package switchfive.project.domain.userStory;

import switchfive.project.domain.*;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.sprint.SprintID;

import java.util.Objects;

/**
 * UserStory class describes the data and the methods of its objects.
 *
 * @author Maurício Pinto Barros
 * @version 0
 * @since Meeting 28-12-2021
 */

public class UserStory implements Entity<UserStory> {

    //Attributes

    /**
     * Unique ID_UserStory that identifies a user story
     */
    private final UserStoryID userStoryID;
    /**
     * Unique project code.
     */
    private final ProjectCode projectCode;
    /**
     * Unique user story identification code.
     */
    private final UserStoryCode userStoryCode;
    /**
     * This parameter tells us, if a user story is not in any sprint (null) or placed in any of them (1, 2, 3...).
     */
    private SprintID sprintID;
    /**
     * User story effort.
     */
    private EffortEstimate effort;
    /**
     * User story priority
     */
    private Priority priority;
    /**
     * User story description.
     */
    private final UserStoryDescription description;
    /**
     * User story status.
     */
    private Status status;

    /**
     * Possible status of a user story
     */
    private enum Status {Planned, Running, Finished, Blocked}

    //Constructor
    /**
     * User story constructor which takes three parameters.
     *
     * @param userStoryID final ID_UserStory userStoryID
     * @param projectCode final ProjectCode projectCode
     * @param userStoryCode final UserStoryCode userStoryCode
     * @param priority final Priority priority
     * @param description final Description description
     */
    public UserStory(final UserStoryID userStoryID,
                     final ProjectCode projectCode,
                     final UserStoryCode userStoryCode,
                     final Priority priority,
                     final UserStoryDescription description) {
        this.userStoryID = userStoryID;
        this.projectCode = projectCode;
        this.userStoryCode = userStoryCode;
        this.sprintID = null;
        this.effort = null;
        this.priority = priority;
        this.description = description;
        this.status = Status.Planned;
    }

    /**
     * A method that modifies the status of a user story.
     *
     * @param status Status status
     */
    public void updateStatus(String status) {
        this.status = Status.valueOf(status);
    }

    /**
     * A get method that returns the user story code.
     */
    public String getUserStoryCode() {
        return userStoryCode.getIdentity();
    }
    /**
     * A get method that returns the project code.
     */
    public ProjectCode getProjectCodeOfUserStory() {
        return projectCode;
    }

    /**
     * A get method that returns the user story priority
     */
    public int getPriority() {
        return priority.priority;
    }

    /**
     * A Set method that changes the atrribute priority.
     *
     * @param priority int priority
     */
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    /**
     * A get method that returns the user story status.
     *
     * @return the status as a Status
     */
    public String getStatus() {
        return this.status.toString();
    }

    /**
     * @param effort is the attribute to be change
     * @author Tânia Mota
     * Method with the objective of changing the effort of a user story
     */

    public boolean changeEffort(Integer effort) {
       EffortEstimate effortEstimate = EffortEstimate.createEffortEstimate(effort);
        this.effort = effortEstimate;
        return true;
    }

    /**
     * Returns the effort estimate of the UserStory.
     * author: mpc
     *
     * @return EffortEstimate object
     */
    public Integer getEffort() {
        return effort.getEffort();
    }


    /**
     * Entities compare by identity, not by attributes.
     *
     * @param other The other entity.
     * @return true if the identities are the same, regardless of other
     * attributes.
     */
    @Override
    public boolean sameIdentityAs(UserStory other) {
        return Objects.equals(userStoryCode, other.userStoryCode) && Objects.equals(projectCode, other.projectCode);
    }

    /**
     * Check if two objects have the same data, as the classes in Java are
     * inherited from the object classes only.
     *
     * @param other final Object other
     * @return true if two objects have the same data; otherwise, returns false.
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (!(other instanceof UserStory)) return false;
        UserStory userStory = (UserStory) other;
        return sameIdentityAs(userStory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userStoryID, projectCode, userStoryCode, sprintID, effort, priority, description, status);
    }
}

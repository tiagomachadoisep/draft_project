package switchfive.project.domain.factory;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.user.User;

import java.security.NoSuchAlgorithmException;

public interface AccountFactory {

    User createAccount(int id,
                       String userName,
                       String email,
                       String function,
                       String password,
                       ProfileID profile) throws NoSuchAlgorithmException;
}

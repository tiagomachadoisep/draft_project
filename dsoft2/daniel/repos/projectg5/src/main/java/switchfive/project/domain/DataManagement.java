package switchfive.project.domain;

import switchfive.project.domain.factory.*;
import switchfive.project.domain.store.*;
import switchfive.project.domain.typology.TypologyDescription;

/**
 * Singleton class.
 */
public final class DataManagement {
    /**
     * customers : CustomerStore.
     */
    private static CustomerStore customerStore = new CustomerStore();
    /**
     * profiles : ProfileStore.
     */
    private static ProfileStore profileStore =
            new ProfileStore(ProfileFactoryImplementation.getInstance());
/*    *//**
     * projects : ProjectStore.
     *//*
    private static ProjectStore projectStore =
            new ProjectStore(ConcreteProjectFactory.getInstance());*/
    /**
     * requests : RequestProfileStore.
     */
    private static RequestProfileStore requestStore =
            new RequestProfileStore(RequestFactoryImplementation.getInstance());
    /**
     * typologies : TypologyStore.
     */
    private static TypologyStore typologyStore =
            new TypologyStore(TypologyFactoryImplementation.getInstance());
    /**
     * users : UserStore.
     */
    private static UserStore userStore =
            new UserStore(AccountFactoryImplementation.getInstance());
    /**
     * instance : DataManagement.
     */
    private static DataManagement instance = null;

    /**
     * Private constructor. Singleton.
     */
    private DataManagement() {
    }

    /**
     * Get instance of DataManagement class. Singleton.
     * Adds default values to typology and profiles
     *
     * @return instance of DataManagement class.
     */
    public static DataManagement getInstance() {
        if (instance == null) {
            instance = new DataManagement();
            profileStore.defaultProfileList();
            typologyStore.createAndAdd(TypologyDescription
                    .create("Fixed cost"));
            typologyStore.createAndAdd(TypologyDescription
                    .create("Time and materials"));
        }
        return instance;
    }

    /**
     * Method that resets all stores
     */
    public static void cleanDataManagement() {
        customerStore = new CustomerStore();
        profileStore =
                new ProfileStore(ProfileFactoryImplementation.getInstance());
        profileStore.defaultProfileList();
        /*projectStore =
                new ProjectStore(ConcreteProjectFactory.getInstance());*/
        requestStore = new RequestProfileStore(
                RequestFactoryImplementation.getInstance());
        typologyStore =
                new TypologyStore(TypologyFactoryImplementation.getInstance());
        userStore = new UserStore(AccountFactoryImplementation.getInstance());
    }


    // GETTER

    /**
     * profiles getter.
     *
     * @return profiles
     */
    public ProfileStore getProfiles() {
        return profileStore;
    }

    /**
     * projects getter.
     *
     * @return projects.
     */
    /*public ProjectStore getProjectStore() {
        return projectStore;
    }*/

    /**
     * requests getter.
     *
     * @return requests
     */
    public RequestProfileStore getRequests() {
        return requestStore;
    }


    /**
     * typologies getter.
     *
     * @return typologies
     */
    public TypologyStore getTypologies() {
        return typologyStore;
    }

    /**
     * users getter.
     *
     * @return users
     */
    public UserStore getUsers() {
        return userStore;
    }
}

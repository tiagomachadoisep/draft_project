package switchfive.project.dto;

import java.util.Objects;

public class ProjectDTO {
    /**
     * Project code, as string.
     */
    private String code;
    /**
     * Project name, as String.
     */
    private String name;
    /**
     * Project description, as String.
     */
    private String description;
    /**
     * Project business sector, as String.
     */
    private String businessSector;
    /**
     * Typology description, as String.
     */
    private String typology;
    /**
     * Project number of planned sprints, as int.
     */
    private int numberOfPlannedSprints;
    /**
     * Project sprint duration, as int.
     */
    private int sprintDuration;
    /**
     * Project budget, as double.
     */
    private double budget;
    /**
     * Project customer, as a CustomerDTO.
     */
    private CustomerDTO customer;
    /**
     * Project time, as a TimeDTO.
     */
    private TimeDTO time;

    /**
     * To carry data between :UI and :Controller.
     *
     * @param codeInput                   final String codeInput
     * @param nameInput                   final String nameInput
     * @param descriptionInput            final String descriptionInput
     * @param businessSectorInput         final String businessSectorInput
     * @param typologyInput               final String typologyInput
     * @param numberOfPlannedSprintsInput final int numberOfPlannedSprintsInput
     * @param sprintDurationInput         final int sprintDurationInput
     * @param budgetInput                 final double budgetInput
     * @param customerInput               final CustomerDTO customerInput
     * @param timeInput                   final TimeDTO timeInput
     */
    public ProjectDTO(final String codeInput, final String nameInput,
                      final String descriptionInput,
                      final String businessSectorInput,
                      final String typologyInput,
                      final int numberOfPlannedSprintsInput,
                      final int sprintDurationInput, final double budgetInput,
                      final CustomerDTO customerInput,
                      final TimeDTO timeInput) {
        this.code = codeInput;
        this.name = nameInput;
        this.description = descriptionInput;
        this.businessSector = businessSectorInput;
        this.typology = typologyInput;
        this.numberOfPlannedSprints = numberOfPlannedSprintsInput;
        this.sprintDuration = sprintDurationInput;
        this.budget = budgetInput;
        this.customer = customerInput;
        this.time = timeInput;
    }

    /**
     * @param other final Object other
     * @return true if instance "other" is an instance of ProjectDTO;
     * otherwise, returns false;
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        ProjectDTO that = (ProjectDTO) other;
        return numberOfPlannedSprints == that.numberOfPlannedSprints
                && sprintDuration == that.sprintDuration
                && Double.compare(that.budget, budget) == 0
                && Objects.equals(code, that.code)
                && Objects.equals(name, that.name)
                && Objects.equals(description, that.description)
                && Objects.equals(businessSector, that.businessSector)
                && Objects.equals(typology, that.typology)
                && Objects.equals(customer, that.customer)
                && Objects.equals(time, that.time);
    }

    /**
     * @return hashCode of an ProjectDTO instance.
     */

    @Override
    public int hashCode() {
        return Objects.hash(code, name, description, businessSector, typology,
                numberOfPlannedSprints, sprintDuration, budget, customer, time);
    }

}

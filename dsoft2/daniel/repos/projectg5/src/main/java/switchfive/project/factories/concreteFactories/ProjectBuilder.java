package switchfive.project.factories.concreteFactories;

import org.springframework.stereotype.Component;
import switchfive.project.domain.Time;
import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.project.*;
import switchfive.project.factories.abstractFactories.IResourceFactory;
import switchfive.project.domain.typology.TypologyID;
import switchfive.project.factories.abstractFactories.IProjectBuilder;

@Component("projectBuilder")
public class ProjectBuilder implements IProjectBuilder {
    private final Project project;

    public ProjectBuilder() {
        this.project = new Project();
    }

    public void initialize(final ProjectCode codeInput,
                           final ProjectName nameInput,
                           final ProjectDescription descriptionInput,
                           final CustomerID customerIdInput,
                           final IResourceFactory factory) {
        this.project.initialize(codeInput, nameInput, descriptionInput,
                customerIdInput, factory);
    }

    public void addBusinessSector(
            final ProjectBusinessSector businessSectorInput) {
        this.project.addBusinessSector(businessSectorInput);
    }

    public void addBudget(final ProjectBudget budgetInput) {
        this.project.addBudget(budgetInput);
    }

    public void addTime(final Time datesInput) {
        this.project.addDates(datesInput);
    }

    public void addSprintDuration(
            final ProjectSprintDuration sprintDurationInput) {
        this.project.addSprintDuration(sprintDurationInput);
    }

    public void addNumberOfPlannedSprints(
            final ProjectNumberOfPlannedSprints numberOfPlannedSprintsInput) {
        this.project.addNumberOfPlannedSprints(numberOfPlannedSprintsInput);
    }

    public void addTypologyID(final TypologyID selectedTypology) {
        this.project.addTypologyID(selectedTypology);
    }

    public Project getProject() {
        return this.project;
    }
}

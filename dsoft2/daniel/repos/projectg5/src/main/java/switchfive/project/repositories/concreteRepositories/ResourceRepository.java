package switchfive.project.repositories.concreteRepositories;

import org.springframework.stereotype.Repository;
import switchfive.project.applicationServices.abstractRepositories.IResourceRepository;
import switchfive.project.domain.resource.Resource;

import java.util.HashSet;
import java.util.Set;

@Repository
public class ResourceRepository implements IResourceRepository {
    private final Set<Resource> resourceRepository;

    public ResourceRepository() {
        this.resourceRepository = new HashSet<>();
    }

    public boolean saveNew(final Resource newResource) {
        return this.resourceRepository.add(newResource);
    }
}

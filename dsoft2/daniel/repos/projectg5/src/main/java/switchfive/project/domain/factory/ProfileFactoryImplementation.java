package switchfive.project.domain.factory;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.profile.Profile;

public class ProfileFactoryImplementation implements ProfileFactory {

    private static ProfileFactoryImplementation instance = null;

    private ProfileFactoryImplementation() {
    }

    /**
     * Get instance of ProfileFactoryImplementation class. Singleton.
     *
     * @return instance of ProfileFactoryImplementation class.
     */
    public static ProfileFactoryImplementation getInstance() {
        if (instance == null) {
            instance = new ProfileFactoryImplementation();
        }
        return instance;
    }

    @Override
    public Profile createProfile(String profileDescription, ProfileID identity) {
        return new Profile(profileDescription,identity);
    }
}

package switchfive.project.controllers.abstractControllers;

import switchfive.project.dto.ProjectCreationDTO;

import java.text.ParseException;

public interface IAddProjectController {
    boolean addProject(ProjectCreationDTO dto)
            throws ParseException;
}

package switchfive.project.dto;

import java.util.List;

/**
 * This DTO carries data (inserted by SM/PM) between UI and Domain.
 */
public class TaskCreationDTO {
    /**
     * The name of the task.
     */
    public String nameDto;
    /**
     * The task description.
     */
    public String descriptionDto;
    /**
     * Task start date.
     */
    public String startDateDto;
    /**
     * Task end date.
     */
    public String endDateDto;
    /**
     * Precedence (precendence task codes).
     */
    public List<Integer> precedenceDto;
    /**
     * Email of the task's responsible.
     */
    public String responsibleEmailDto;
    /**
     * Type of task (Meeting, Documentation, Design, Implementation, Testing,
     * Deployment, etc.).
     */
    public String typeOfTaskDto;
    /**
     * Code of project which the task is part of.
     */
    public String projectCodeDto;
    /**
     * Number of sprint which the task is part of.
     */
    public int sprintNumberDto;
    /**
     * Effort estimate (start by having the initial estimate, but can be
     * updated several times throughout the project; uses Fibonacci series
     * for duration in hours.).
     */
    public int effortDto;

    /**
     * @param nameInput             final String nameInput
     * @param descriptionInput      final String descriptionInput
     * @param startDateInput        final String startDateInput
     * @param endDateInput          final String endDateInput
     * @param precedenceListInput   final List<String> precedenceListInput
     * @param responsibleEmailInput final String responsibleEmailInput
     * @param typeOfTaskInput       final String typeOfTaskInput
     * @param projectCodeInput      final String projectCodeInput
     * @param sprintNumberInput     final int sprintnumberInput
     * @param effortEstimateInput   final int effortEstimateInput
     */
    public TaskCreationDTO(final String nameInput,
                           final String descriptionInput,
                           final String startDateInput,
                           final String endDateInput,
                           final List<Integer> precedenceListInput,
                           final String responsibleEmailInput,
                           final String typeOfTaskInput,
                           final String projectCodeInput,
                           final int sprintNumberInput,
                           final int effortEstimateInput) {
        this.nameDto = nameInput;
        this.descriptionDto = descriptionInput;
        this.startDateDto = startDateInput;
        this.endDateDto = endDateInput;
        this.precedenceDto = precedenceListInput;
        this.responsibleEmailDto = responsibleEmailInput;
        this.typeOfTaskDto = typeOfTaskInput;
        this.projectCodeDto = projectCodeInput;
        this.sprintNumberDto = sprintNumberInput;
        this.effortDto = effortEstimateInput;
    }
}

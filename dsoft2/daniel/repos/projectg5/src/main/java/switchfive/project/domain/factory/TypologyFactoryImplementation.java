package switchfive.project.domain.factory;

import switchfive.project.domain.typology.TypologyID;
import switchfive.project.domain.typology.Typology;
import switchfive.project.domain.typology.TypologyDescription;

public class TypologyFactoryImplementation implements TypologyFactory {
    static TypologyFactoryImplementation instance;

    private TypologyFactoryImplementation() {
    }
    public static TypologyFactoryImplementation getInstance() {
        if (instance == null) {
            instance = new TypologyFactoryImplementation();
        }
        return instance;
    }

    @Override
    public Typology createTypology(TypologyID identity,
                                   TypologyDescription description) {
        return new Typology(identity, description);
    }

}

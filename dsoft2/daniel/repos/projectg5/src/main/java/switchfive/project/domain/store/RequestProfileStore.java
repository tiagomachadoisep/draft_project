package switchfive.project.domain.store;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.request.RequestID;
import switchfive.project.domain.user.UserID;
import switchfive.project.domain.request.Request;
import switchfive.project.domain.factory.RequestFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Class responsible for the storage of all Request in a List
 */
public class RequestProfileStore {

    private RequestFactory requestFactory;

    private Set<Request> requestList;

    public RequestProfileStore(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
        this.requestList = new HashSet<>();
    }


    /**
     * Private method that adds the new profile Request to the store.
     *
     * @param newRequest Request object
     * @return true if the Request is successfully added to the store
     */
    private boolean addNewRequestToRequestStore(Request newRequest) {
        return requestList.add(newRequest);
    }

    /**
     * Method responsible for the creation of a new Request for a Profile. This method creates, validates
     * and adds the new Request object to the DataManagement list
     *
     * @param requestedUserID   User_ID that requests a new Profile
     * @param selectedProfileID Profile_ID object requested
     * @return true if Request object is successfully created, validated and added.
     */
    public boolean createNewRequest(final UserID requestedUserID,
                                    final ProfileID selectedProfileID) {
        RequestID requestIdentity = RequestID.createRequestID(this.requestList.size() + 1);
        Request newRequest = requestFactory.createNewRequest(requestIdentity,
                requestedUserID, selectedProfileID);
        return addNewRequestToRequestStore(newRequest);
    }

    /**
     * Searches the store and retrieves the Request with the selected User and
     * requested Profile. Null if no Request is found.
     * <p>
     * author: mpc
     *
     * @param selectedUser User in Request
     * @param requestedProfile Profile in Request
     * @return Request object if found
     */
    public Request getRequest(UserID selectedUser, ProfileID requestedProfile) {
        Request selectedRequest = null;

        for (Request request : this.requestList) {
            if (request.compareUserAndProfileOfRequest(selectedUser, requestedProfile))
                selectedRequest = request;
        }

        return selectedRequest;
    }


    /**
     * Approves a Request in the Request Store, after an update was done
     * by the admin.
     * author: mpc
     *
     * @param selectedUser User in Request
     * @param requestedProfile Profile in Request
     */
    public boolean approveRequestIfRequestInStore(UserID selectedUser, ProfileID requestedProfile) {
        boolean isRequestApproved = false;
        Request requestInStore = getRequest(selectedUser, requestedProfile);

        if (requestInStore != null) {
            isRequestApproved = requestInStore.approveRequest();
        }

        return isRequestApproved;
    }


    /**
     * If a Request of a selected Profile that was updated by the admin exists in Store,
     * deletes it from the Request Store.
     * <p>
     * author: mpc
     *
     * @param selectedUser User in Request
     * @param actualProfile Profile in Request
     */
    public boolean deleteRequestOfOldProfileIfInStore(UserID selectedUser, ProfileID actualProfile) {
        boolean wasRequestDeleted = false;

        Request requestInStore = getRequest(selectedUser, actualProfile);

        if (requestInStore != null) {
            wasRequestDeleted = this.requestList.remove(requestInStore);
        }

        return wasRequestDeleted;
    }
}

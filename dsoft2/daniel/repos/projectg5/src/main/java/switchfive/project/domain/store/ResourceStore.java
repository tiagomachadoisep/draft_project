package switchfive.project.domain.store;


import switchfive.project.factories.abstractFactories.IResourceFactory;
import switchfive.project.domain.resource.Resource;

import java.util.HashSet;
import java.util.Set;

public class ResourceStore {
    /**
     * Store of resources.
     */
    private final Set<Resource> resources;
    /**
     * Factory pattern - create object without exposing the creation logic to
     * the client and refer to newly created object using a common.
     */
    private final IResourceFactory IResourceFactory;

    /**
     * Constructor with factory input.
     *
     * @param factory ResourceFactory
     *                factory
     */
    public ResourceStore(final IResourceFactory factory) {
        this.resources = new HashSet<>();
        this.IResourceFactory = factory;
    }
}

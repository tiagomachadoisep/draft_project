package switchfive.project.dto;

import java.util.Objects;

public class CustomerDTO {
    /**
     * Tax Payer Identification Number, as a String.
     */
    public String taxpayerIdentificationNumber;
    /**
     * Customer designation, as a String.
     */
    public String designation;

    public CustomerDTO(String taxpayerIdentificationNumber,
                       String designation) {
        this.taxpayerIdentificationNumber = taxpayerIdentificationNumber;
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerDTO that = (CustomerDTO) o;
        return Objects.equals(taxpayerIdentificationNumber, that.taxpayerIdentificationNumber) && Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taxpayerIdentificationNumber, designation);
    }
}

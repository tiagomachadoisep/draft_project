package switchfive.project.domain.factory;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.user.User;

import java.security.NoSuchAlgorithmException;

public class AccountFactoryImplementation implements AccountFactory{

    static AccountFactoryImplementation instance;

    private AccountFactoryImplementation (){

    }

    public static  AccountFactoryImplementation getInstance(){

        if (instance == null){
            instance = new AccountFactoryImplementation();
        }
        return instance;
    }

    @Override
    public User createAccount(int id,
                              String userName,
                              String email,
                              String function,
                              String password,
                              ProfileID profile) throws NoSuchAlgorithmException {
        return new User(id,userName,email,function,password,profile);
    }
}

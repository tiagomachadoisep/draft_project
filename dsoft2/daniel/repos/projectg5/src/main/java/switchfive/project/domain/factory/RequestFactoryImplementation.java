package switchfive.project.domain.factory;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.request.RequestID;
import switchfive.project.domain.user.UserID;
import switchfive.project.domain.request.Request;

public class RequestFactoryImplementation implements RequestFactory {

    private static RequestFactoryImplementation instance = null;

    private RequestFactoryImplementation() {
    }

    /**
     * Get instance of RequestFactoryImplementation class. Singleton.
     *
     * @return instance of RequestFactoryImplementation class.
     */
    public static RequestFactoryImplementation getInstance() {
        if (instance == null) {
            instance = new RequestFactoryImplementation();
        }
        return instance;
    }

    @Override
    public Request createNewRequest(RequestID requestIdentity, UserID requestedUserID,
                                    ProfileID selectedProfileID) {
        return new Request(requestIdentity, requestedUserID, selectedProfileID);
    }
}

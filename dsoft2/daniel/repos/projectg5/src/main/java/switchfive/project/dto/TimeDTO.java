package switchfive.project.dto;

import java.util.Objects;

public class TimeDTO {
    /**
     * Start Date, as a String.
     */
    public String startDate;
    /**
     * End Date, as a String.
     */
    public String endDate;

    public TimeDTO(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeDTO timeDTO = (TimeDTO) o;
        return Objects.equals(startDate, timeDTO.startDate) && Objects.equals(endDate, timeDTO.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDate, endDate);
    }
}

package switchfive.project.domain.store;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.profile.Profile;
import switchfive.project.domain.factory.ProfileFactory;

import java.util.HashSet;
import java.util.Set;

public class ProfileStore {

    private ProfileFactory profileFactory;

    private final Set<Profile> profileList;

    public ProfileStore(ProfileFactory profileFactory) {
        this.profileList = new HashSet<>();
        this.profileFactory = profileFactory;
    }

    /**
     * Default profile list created at dataManagement creation time.
     */
    public void defaultProfileList() {
        createProfile("Visitor", ProfileID.createProfileID(1));
        createProfile("Administrator", ProfileID.createProfileID(2));
        createProfile("Director", ProfileID.createProfileID(3));
        createProfile("User", ProfileID.createProfileID(4));
    }

    /**
     * Method responsible for the creation of a new Profile. This method creates, validates
     * and adds the new Profile object to the DataManagement list
     *
     * @param profileDesignation Requested Profile
     * @return true if Profile object is successfully created, validated and added
     */
    public boolean createProfile(final String profileDesignation, ProfileID identity) {
        Profile newProfile = this.profileFactory.createProfile(profileDesignation,identity);
        return addProfileToProfileStore(newProfile);
    }

    /**
     * Private method that adds the new Profile to the system
     *
     * @param newProfile New Profile object to be validated and added to DataManagement System
     * @return true if the Profile is successfully added to the system
     */
    private boolean addProfileToProfileStore(final Profile newProfile) {
        return this.profileList.add(newProfile);
    }

    public Set<Profile> getProfileList() {
        return profileList;
    }

    /**
     * If in Store, returns the Profile with the visitor description.
     * @return Profile object
     */
    public Profile getVisitorProfile() {
        for (Profile selectedProfile : getProfileList()) {
            if (selectedProfile.compareProfileDesignation("visitor")) {
                return selectedProfile;
            }
        }
        return null;
    }

    /**
     * Checks if a Profile exists in the Store.
     *
     * @param profile Profile object to be found.
     * @return true if Profile exists.
     */
    public boolean profileExists(Profile profile) {
        boolean result = false;

        for (Profile selectedProfile : this.profileList) {
            if (selectedProfile.equals(profile)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Looks for a specific profile description and returns an object
     *
     * @param profileDescription that will be searched
     * @return an profile (model domain object)
     */
    public Profile getProfileByDescription(String profileDescription) {
        for (Profile selectedProfile : getProfileList()) {
            if (selectedProfile.compareProfileDesignation(profileDescription)) {
                return selectedProfile;
            }
        }
        return null;
    }

    /**
     * Looks for a specific profile description and returns an object
     *
     * @param profileID that will be searched
     * @return an profile (model domain object)
     */
    public Profile getProfileByID(ProfileID profileID) {
        for (Profile selectedProfile : getProfileList()) {
            if (selectedProfile.compareProfileID(profileID)) {
                return selectedProfile;
            }
        }
        return null;
    }

    /**
     * Compares two profiles and checks if they are equal.
     *
     * @param profileOne the first profile.
     * @param profileTwo the second profile.
     * @return false if both profiles are different.
     */
    public boolean areProfilesDifferent(final Profile profileOne, final Profile profileTwo) {
        return !profileOne.equals(profileTwo);
    }
}

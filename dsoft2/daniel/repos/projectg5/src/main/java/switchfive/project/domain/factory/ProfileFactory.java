package switchfive.project.domain.factory;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.profile.Profile;

public interface ProfileFactory {
    public Profile createProfile(String profileDescription, ProfileID identity);
}

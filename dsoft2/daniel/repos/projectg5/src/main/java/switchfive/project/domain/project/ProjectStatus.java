package switchfive.project.domain.project;

public enum ProjectStatus {
    /**
     * Default status for new projects.
     */
    Planned,
    /**
     * This status represents a project ongoing.
     */
    Running,
    /**
     * Represents a completed project.
     */
    Finished,
    /**
     * Represents a break in the project (which will continue).
     */
    Blocked
}


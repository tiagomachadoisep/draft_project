package switchfive.project.factories.abstractFactories;

import switchfive.project.domain.resource.ResourceID;
import switchfive.project.domain.user.UserID;
import switchfive.project.domain.Time;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.resource.Resource;
import switchfive.project.domain.resource.ResourceCostPerHour;
import switchfive.project.domain.resource.ResourcePercentageOfAllocation;
import switchfive.project.domain.resource.Role;

import java.text.ParseException;

public interface IResourceFactory {
    Resource createResource(ResourceID identityInput,
                            UserID userIdInput,
                            ProjectCode projectCodeInput,
                            Time datesInput,
                            ResourceCostPerHour costPerHourInput,
                            ResourcePercentageOfAllocation allocationInput,
                            Role roleInput) throws ParseException;
}

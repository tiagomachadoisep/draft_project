package switchfive.project.dto;

import java.util.List;
import java.util.Objects;

public class UserDTO {

    public String email;

    public List<String> userProfileList;

    public boolean activation;

    public String userName;

    public String function;

    public UserDTO(String email, List<String> userProfileList, boolean activation, String userName, String function) {
        this.email = email;
        this.userProfileList = userProfileList;
        this.activation = activation;
        this.userName = userName;
        this.function = function;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return activation == userDTO.activation && Objects.equals(email, userDTO.email) && Objects.equals(userProfileList, userDTO.userProfileList) && Objects.equals(userName, userDTO.userName) && Objects.equals(function, userDTO.function);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, userProfileList, activation, userName, function);
    }
}

package switchfive.project.domain.user;

import switchfive.project.domain.ValueObject;

import java.util.Objects;

public class UserID implements ValueObject<UserID> {

    private final int identity;

    private UserID(int identity) {
        this.identity = identity;
    }

    public static UserID createUserID(int identity) {
        return new UserID(identity);
    }

    /**
     * Value objects compare by the values of their attributes, they don't have
     * an identity.
     *
     * @param other The other value object.
     * @return <code>true</code> if the given value object's and this value
     * object's attributes are the same.
     */
    @Override
    public boolean sameValueAs(UserID other) {
        return this.identity == other.identity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserID _userID = (UserID) o;
        return sameValueAs(_userID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identity);
    }
}

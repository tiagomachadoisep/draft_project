package switchfive.project.dto;

public class UserStoryDTO {
    /**
     * The unique code of user story.
     */
    public final String code;
    /**
     * The estimated effort of a user story, always null in product backlog.
     */
    public final Integer effort;
    /**
     * A int priority of the user story.
     */
    public final int priority;
    /**
     * The user story description.
     */
    public final String description;
    /**
     * The user story status.
     */
    public final String status;

    /**
     * @param code        final String code
     * @param effort      final Integer effort
     * @param priority    final int priority
     * @param description final String description
     * @param status      final String status
     */
    public UserStoryDTO(final String code, final Integer effort, final int priority,
                        final String description, final String status) {
        this.code = code;
        this.effort = effort;
        this.priority = priority;
        this.description = description;
        this.status = status;
    }
}

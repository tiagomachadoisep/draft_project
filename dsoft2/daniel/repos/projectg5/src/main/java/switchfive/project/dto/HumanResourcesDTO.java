package switchfive.project.dto;


import java.util.List;


public class HumanResourcesDTO {

    public final ResourceDTO scrumMaster;

    public final ResourceDTO projectManager;

    public final List<ResourceDTO> projectTeam;

    public final ResourceDTO productOwner;

    public HumanResourcesDTO(ResourceDTO scrumMaster, ResourceDTO projectManager, ResourceDTO productOwner, List<ResourceDTO> projectTeam) {
        this.scrumMaster = scrumMaster;
        this.productOwner = productOwner;
        this.projectManager = projectManager;
        this.projectTeam = projectTeam;
    }
}

package switchfive.project.domain.sprint;

import switchfive.project.domain.Entity;
import switchfive.project.domain.Time;
import switchfive.project.domain.project.ProjectCode;

import java.util.Objects;

public class Sprint implements Entity<Sprint> {
    /**
     * Sprint identifier.
     */
    private SprintID sprintID;
    /**
     * Project code identifier.
     */
    private ProjectCode projectCode;
    /**
     * Sprint time.
     */
    private Time dates;
    /**
     * Sprint description.
     */
    private SprintDescription description;

    /**
     * @param sprintIdInput    sprint identifier (projectCodeInput : String &
     *                        sprintNumber
     *                         : int)
     * @param projectCodeInput alphanumerical identifier
     * @param datesInput       Time (value object)
     * @param descriptionInput descriptionInput of the sprint, inserted by actor
     */
    public Sprint(final SprintID sprintIdInput,
                  final ProjectCode projectCodeInput, final Time datesInput,
                  final SprintDescription descriptionInput) {
        this.sprintID = sprintIdInput;
        this.projectCode = projectCodeInput;
        this.dates = datesInput;
        this.description = descriptionInput;
    }

    /**
     * Entities compare by identity, not by attributes.
     *
     * @param other The other entity.
     * @return true if the identities are the same, regardless of other
     * attributes.
     */
    @Override public boolean sameIdentityAs(final Sprint other) {
        return other != null && this.sprintID.sameValueAs(other.sprintID);
    }

    /**
     * @param other instance to compare
     * @return true if compared objects are equals; otherwise, returns false.
     */
    @Override public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Sprint)) {
            return false;
        }
        Sprint sprint = (Sprint) other;
        return sameIdentityAs(sprint);
    }

    /**
     * @return hashcode.
     */
    @Override public int hashCode() {
        return Objects.hash(sprintID);
    }
}

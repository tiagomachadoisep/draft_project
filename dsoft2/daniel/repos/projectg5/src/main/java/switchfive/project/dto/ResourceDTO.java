package switchfive.project.dto;

public class ResourceDTO {

    /**
     * Unique email associated with the Resource.
     */
    public final String email;
    /**
     * The name of the resource.
     */
    public final String name;

    public ResourceDTO(final String email, final String name) {
        this.email = email;
        this.name = name;
    }
}

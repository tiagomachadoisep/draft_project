package switchfive.project.domain.store;

public enum TaskStatus {
    PLANNED,
    RUNNING,
    BLOCKED,
    FINISHED
}

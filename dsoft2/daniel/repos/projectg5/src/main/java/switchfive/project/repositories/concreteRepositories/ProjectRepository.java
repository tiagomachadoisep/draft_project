package switchfive.project.repositories.concreteRepositories;

import org.springframework.stereotype.Repository;
import switchfive.project.domain.project.Project;
import switchfive.project.applicationServices.abstractRepositories.IProjectRepository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class ProjectRepository implements IProjectRepository {
    private final Set<Project> projectRepository;

    public ProjectRepository() {
        this.projectRepository = new HashSet<>();
    }

    public boolean projectAlreadyExist(final Project project) {
        if (this.projectRepository.isEmpty()) {
            return false;
        }

        return this.projectRepository.contains(project);
    }

    public void saveNew(Project newProject) {
        this.projectRepository.add(newProject);
    }
}

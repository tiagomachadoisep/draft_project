package switchfive.project.applicationServices.abstractRepositories;

import org.springframework.stereotype.Repository;
import switchfive.project.domain.resource.Resource;

public interface IResourceRepository {
    boolean saveNew(Resource newResource);
}

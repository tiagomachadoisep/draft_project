package switchfive.project.dto;

public class ResourceCreationDTO {

    /**
     * Email that identifies the user who takes the action.
     */
    public String emailDto;

    /**
     * Project code in which the user story will be modified.
     */
    public String projectCodeDto;

    /**
     * Resource time, as a TimeDTO.
     */
    public TimeDTO datesDto;

    /**
     * Resource cost per hour, as double.
     */
    public double costPerHourDto;

    /**
     * Resource percentage of allocation, as double.
     */
    public double percentageOfAllocationDto;

    public ResourceCreationDTO() {
    }

    public ResourceCreationDTO(String email, String projectCode, TimeDTO dates, double costPerHour, double percentageOfAllocation) {
        this.emailDto = email;
        this.projectCodeDto = projectCode;
        this.datesDto = dates;
        this.costPerHourDto = costPerHour;
        this.percentageOfAllocationDto = percentageOfAllocation;
    }
}


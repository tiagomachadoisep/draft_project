package switchfive.project.domain.store;

import switchfive.project.domain.userStory.UserStoryID;
import switchfive.project.domain.userStory.Priority;
import switchfive.project.domain.userStory.UserStoryCode;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.userStory.UserStory;
import switchfive.project.domain.factory.UserStoryFactory;
import switchfive.project.domain.userStory.UserStoryDescription;

import java.util.*;

/**
 * ProductBacklog class describes the data and the methods of its objects.
 *
 * @author Maurício Pinto Barros
 * @version 0
 * @since Meeting 28-12-2021
 */

public class UserStoryStore {

    //Attributes
    private final UserStoryFactory userStoryFactory;
    /**
     * List of user stories.
     */
    private final List<UserStory> userStoryList;

    //Constructor

    /**
     * Empty ProductBacklog constructor.
     */
    public UserStoryStore(UserStoryFactory userStoryFactory) {
        this.userStoryList = new ArrayList<>();
        this.userStoryFactory = userStoryFactory;
    }

    /**
     * Public method create and add user story to userStoryList.
     *
     * @param description String description
     * @return true if user story was successfully created and added to userStoryList
     */
    public boolean createAndAddUserStory(String description, ProjectCode projectCode) {
        boolean userStoryCreationStatus = false;

        //TODO instaciar estes objectos no controlador
        UserStoryCode code = generatorCode(projectCode);
        Priority defaultPriority = Priority.createPriority(defaultPriority(projectCode));
        UserStoryID userStoryID = UserStoryID.createUserStoryID(projectCode.toString(), code.toString());
        UserStoryDescription userStoryDescription = UserStoryDescription.createUserStoryDescription(description);

        if (UserStoryDescription.isValidDescriptionForUserStory(description)) {
            UserStory newUserStory = this.userStoryFactory.createUserStory(userStoryID,
                    projectCode, code, defaultPriority, userStoryDescription);
            this.userStoryList.add(newUserStory);
            userStoryCreationStatus = true;
        }
        return userStoryCreationStatus;
    }

    /**
     * Method to calculate the default priority of an UserStory when created.
     * When created, the userStory will go to the bottom of the list of priorities.
     *
     * @return an incremental priority
     */
    public int defaultPriority(ProjectCode projectCode) {
        int index = 0;
        for (UserStory userStory : userStoryList){
            if (userStory.getProjectCodeOfUserStory().equals(projectCode)){
                index++;
            }
        }
        return index + 1;
    }

    /**
     * Private method «Generate Code».
     *
     * @return an incremetal ID_UserStory that match with the created UserStory, e.g. "US1", "US2", "US3"
     */
    private UserStoryCode generatorCode(ProjectCode projectCode) {
        int index = 0;
        for (UserStory userStory : userStoryList){
            if (userStory.getProjectCodeOfUserStory().equals(projectCode)){
                index++;
            }
        }
        String code = "US" + (index + 1);
        return UserStoryCode.createUserStoryCode(code);
    }

    /**
     * Returns the User Story in the Product Backlog given an identifying code.
     *
     * @param code String code.
     * @return UserStory object. Null if User Story is not found.
     */
    public UserStory getUserStory(final String code) {
        UserStory userStory = null;

        for (UserStory selectedUserStory : userStoryList) {
            if (selectedUserStory.getUserStoryCode().toLowerCase(Locale.ROOT).trim().equals(code.toLowerCase(Locale.ROOT).trim())) {
                userStory = selectedUserStory;
            }
        }
        return userStory;
    }

    /**
     * Method that checks if a User Story exists the Product Backlog given an
     * identifying code.
     *
     * @param userStoryCode String that identifies the User Story in
     *                      ProductBacklog to refine.
     * @return boolean
     */
    public boolean doesUserStoryExistInProductBacklog(final String userStoryCode) {
        boolean doesUserStoryExistInProductBacklog = false;
        int iterator = 0;

        while (iterator < userStoryList.size()
                && !doesUserStoryExistInProductBacklog) {
            UserStory currentUserStory = userStoryList.get(iterator);
            if (userStoryCode.toLowerCase(Locale.ROOT).trim()
                    .equals(currentUserStory.getUserStoryCode()
                            .toLowerCase(Locale.ROOT).trim())) {
                doesUserStoryExistInProductBacklog = true;
            }
            iterator++;
        }

        return doesUserStoryExistInProductBacklog;
    }

    /**
     * Method responsible for the refinement of a User Story in the Product
     * Backlog. The User story code exist in the Product Backlog. The original
     * User Story is removed and new ones are created according to the
     * description list.
     *
     * @param requestedUserStoryCode      Original User Story code
     * @param newUserStoryDescriptionList new User Stories description
     * @return boolean
     */
    public boolean refineUserStory(String requestedUserStoryCode,
                                   List<String> newUserStoryDescriptionList) {
        boolean refineUserStory = true;

        UserStory userStoryToRemove = getUserStory(requestedUserStoryCode);
        ProjectCode projectCode = userStoryToRemove.getProjectCodeOfUserStory();
        removeUserStory(userStoryToRemove);

        int iterator = 0;

        while (iterator < newUserStoryDescriptionList.size() && refineUserStory) {
            String description = newUserStoryDescriptionList.get(iterator);
            refineUserStory = createAndAddUserStory(description, projectCode);
            iterator++;
        }

        return refineUserStory;
    }

    /**
     * Removes a given UserStory and updates the US priorities.
     *
     * @param userStoryToRemove User Story to remove from List.
     * @return boolean
     */
    public boolean removeUserStory(UserStory userStoryToRemove) {
        boolean wasUserStorySuccessfullyRemoved = this.userStoryList
                .remove(userStoryToRemove);
        updatesAllUserStoriesPriorities();
        return wasUserStorySuccessfullyRemoved;
    }

    /**
     * Removes a given UserStory and updates the US priorities.
     *
     * @param userStoryCode User Story identifier.
     * @return true if it was removed, false otherwise.
     */
    public boolean removeUserStory(String userStoryCode) {
        UserStory us = getUserStory(userStoryCode);
        boolean removed = this.userStoryList.remove(us);
        updatesAllUserStoriesPriorities();
        return removed;
    }

    /**
     * Method to set a new priority to a certain UserStory.
     *
     * @param code        String code
     * @param newPriority int priority
     * @return
     */
    public boolean setPriority(String code, int newPriority) {
        boolean priorityChanged = false;

        UserStory userStoryToSetPriority = getUserStory(code);

        if (userStoryToSetPriority != null && validPriority(newPriority)) {
            userStoryList.remove(userStoryToSetPriority);
            userStoryList.add(newPriority - 1, userStoryToSetPriority);
            updatesAllUserStoriesPriorities();
            priorityChanged = true;
        }


        return priorityChanged;
    }

    /**
     * Updates the priority of all the user stories in the userStoryList
     * if a new priority was established in the list.
     */
    private void updatesAllUserStoriesPriorities() {
        for (int i = 0; i < userStoryList.size(); i++) {
            UserStory userStory = userStoryList.get(i);
            // TODO Priority method with reference to Project
            Priority wrongPriority = Priority.createPriority(1);
            userStory.setPriority(wrongPriority);
        }
    }

    /**
     * validates priority of UserStory.
     *
     * @param newPriority int priority
     * @return true if number is between 0 and 6.
     */
    private boolean validPriority(int newPriority) {

        boolean priorityValidated = true;

        int max = userStoryList.size();
        int min = 1;

        if (newPriority > max || newPriority < min) {
            priorityValidated = false;
        }

        return priorityValidated;
    }

    /**
     * Get method for User Story List.
     *
     * @return a list of user stories.
     * @author Gisela Araújo
     */
    public List<UserStory> getUserStoryList() {
        return new ArrayList<>(userStoryList);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserStoryStore that = (UserStoryStore) o;
        return Objects.equals(userStoryList, that.userStoryList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userStoryList);
    }

}

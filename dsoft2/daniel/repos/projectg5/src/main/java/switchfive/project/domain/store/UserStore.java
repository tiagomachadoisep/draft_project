package switchfive.project.domain.store;


import switchfive.project.domain.*;
import switchfive.project.domain.factory.AccountFactory;
import switchfive.project.domain.profile.Profile;
import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.user.User;
import switchfive.project.domain.user.UserID;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UserStore {

    private final AccountFactory accountFactory;
    /**
     * Attribute UserList.
     */
    private final List<User> userList;

    /**
     * Empty Constructor.
     */
    public UserStore(AccountFactory accountFactory) {
        this.userList = new ArrayList<>();
        this.accountFactory = accountFactory;
    }

    /**
     * Gets the userList.
     *
     * @return the userList.
     */
    public List<User> getUserList() {
        return userList;
    }

    /**
     * Method that validates if email is duplicated.
     *
     * @param email
     * @return true if email already exists.
     */
    public boolean isEmailDuplicated(final String email) {
        boolean duplicatedEmail = false;
        for (User eachUser : userList) {
            if (eachUser.compareEmail(email)) {
                duplicatedEmail = true;
            }
        }
        return duplicatedEmail;
    }

    /**
     * Adds a new Account to userList.
     *
     * @param newUser the user to be added.
     * @return true if the user was added, false otherwise.
     */
    private boolean addAccount(final User newUser) {
        return userList.add(newUser);
    }

    public boolean createAccount(int id, String userName,
                                 String email,
                                 String function,
                                 String password) throws NoSuchAlgorithmException {

        boolean userAdded = false;

        if (!isEmailDuplicated(email)) {
            ProfileStore profileStore = DataManagement.getInstance().getProfiles();
            // TODO ID_Profile
            ProfileID newProfile = ProfileID.createProfileID(1);
            User newUser = accountFactory.createAccount(id, userName,email,function,password,newProfile);
            addAccount(newUser);
            userAdded = true;
        }
        return userAdded;

    }

    /**
     * /**
     * A method that loops through a users list until it finds a given email.
     *
     * @param email that will be searched
     * @return a list with the user with a given email
     */

    public List<User> getUserByEmail(final String email) {
        ArrayList<User> resultList = new ArrayList<>();
        if (email.equals("")) {
            return resultList;
        } else {
            for (User selectedUser : this.userList) {
                if (selectedUser.compareEmail(email)) {
                    resultList.add(selectedUser);
                }
            }
            return resultList;
        }
    }

    /**
     * A method that loops through a users list and finds a user with a given
     * profile description adding it to a result list.
     *
     * @param profileName that will be searched.
     * @return a list with all the users with a given profileName.
     */
    public List<User> getUserByProfileDescription(final String profileName) {
        ArrayList<User> resultList = new ArrayList<>();
        DataManagement dataManagement = DataManagement.getInstance();
        ProfileStore profileStore = dataManagement.getProfiles();

        if (profileName.equals("")) {
            return resultList;
        } else {
            for (User selectedUser : this.userList) {
                for (ProfileID selectedProfile : selectedUser.getUserProfileList()) {
                    Profile profileFound = profileStore.getProfileByID(selectedProfile);

                    if (profileFound.compareProfileDesignation(profileName)) {
                        resultList.add(selectedUser);
                    }
                }
            }
            return resultList;
        }
    }

    /**
     * A method that will search for a user depending on the type of search
     * (by email or profile).
     *
     * @param typeOfSearch should accept 2 inputs. TypeOfSearch == "email"
     *                     will search the user
     *                     by email. typeOfSearch == "profile" will search
     *                     the user by profile.
     * @param searchField  is the word that will be searched.
     * @return a list with the result.
     */
    public List<User> searchUser(final String typeOfSearch, final String searchField) {
        List<User> resultList = new ArrayList<>();

        if (typeOfSearch.toLowerCase(Locale.ROOT).trim().equals("email")) {
            resultList = getUserByEmail(searchField);
        }
        if (typeOfSearch.toLowerCase(Locale.ROOT).trim().equals("profile")) {
            resultList = getUserByProfileDescription(searchField);
        }
        return resultList;
    }

    /**
     * The purpose of this method is to update a user's profile with a new one.
     * This method will validate that the users exists, the user has the
     * profile to be changed, the new profile exists
     * and the profile to be changed and pretended profile are different.
     *
     * @param selectedEmail       of the user that will have its profile
     *                            updated.
     * @param profileToBeChanged  actual profile of the user.
     * @param newPretendedProfile new pretended profile that the user will
     *                            receive.
     * @return true if the profile is successfully updated.
     */
    public boolean updateUserProfile(String selectedEmail, int profileToBeChanged, int newPretendedProfile) {

        boolean profileUpdated = false;

        // TODO - Restruturação chamada DataManagement
//        ProfileStore profileStore = DataManagement.getInstance().getProfiles();
//
//        ID_Profile actualProfile = ID_Profile.createProfileID(profileToBeChanged);
//        ID_Profile pretendedProfile = ID_Profile.createProfileID(newPretendedProfile);
//
//        if ((isEmailDuplicated(selectedEmail)) && (findUser(selectedEmail).hasProfile(actualProfile)) &&
//                profileStore.profileExists(pretendedProfile) && (User.areProfilesDifferent(actualProfile, pretendedProfile))) {
//            findUser(selectedEmail).updateProfile(actualProfile, pretendedProfile);
//            profileUpdated = true;
//        }
        return profileUpdated;
    }

    /**
     * @param email that will be searched
     * @return the User (as an object) with that given email
     */

    public User findUser(final String email) {
        User result = null;
        for (User selectedUser : this.userList) {
            if (selectedUser.compareEmail(email)) {
                result = selectedUser;
            }
        }
        return result;
    }

    /**
     * Method that allows to search for a user using a user ID object.
     *
     * @param user_ID User identifier to be searched
     * @return The User associated with the given email
     */
    public User findUserByID(final UserID user_ID) {
        User result = null;
        for (User selectedUser : this.userList) {
            //TODO Iplemenentation of User ID
            if (selectedUser.equals(user_ID)) {
                result = selectedUser;
            }
        }
        return result;
    }


    /**
     * Checks if a User exists in the Store.
     *
     * @param selectedUser User object to be found.
     * @return true if User exists.
     */
    public boolean userExistsInStore(final User selectedUser) {
        boolean result = false;

        for (User user : this.userList) {
            if (selectedUser.equals(user) && !result) {
                result = true;
            }
        }
        return result;
    }
}

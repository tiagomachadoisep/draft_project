package switchfive.project.domain.task;

import switchfive.project.domain.ValueObject;

import java.util.Objects;

public class TaskID implements ValueObject<TaskID> {

    private int identity;

    TaskContainer taskContainer;

    private TaskID(int identity, TaskContainer taskContainer) {
        this.identity = identity;
        this.taskContainer = taskContainer;
    }

    public static TaskID createIDTask (int identity, TaskContainer taskContainer) {
        return new TaskID(identity,taskContainer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identity, taskContainer);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof TaskID)) {
            return false;
        }
        TaskID that = (TaskID) object;
        return sameValueAs(that);
    }

    @Override
    public boolean sameValueAs(TaskID other) {
        return other != null && identity == other.identity && taskContainer.equals(other.taskContainer);
    }
}

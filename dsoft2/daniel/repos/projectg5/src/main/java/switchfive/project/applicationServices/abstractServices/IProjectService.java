package switchfive.project.applicationServices.abstractServices;

import switchfive.project.domain.Time;
import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.project.ProjectDescription;
import switchfive.project.domain.project.ProjectName;
import switchfive.project.domain.resource.ResourceCostPerHour;
import switchfive.project.domain.resource.ResourcePercentageOfAllocation;
import switchfive.project.domain.user.UserID;

import java.text.ParseException;

public interface IProjectService {
    boolean addProject(ProjectCode code, ProjectName name,
                       ProjectDescription description, CustomerID customerID,
                       Time time);

    boolean setProjectManager(UserID userIdInput,
                              ProjectCode projectCodeInput, Time time,
                              ResourceCostPerHour costPerHourostPerHour,
                              ResourcePercentageOfAllocation percentageOfAllocation)
            throws ParseException;
}

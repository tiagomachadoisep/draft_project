package switchfive.project.domain.user;

import switchfive.project.domain.DataManagement;
import switchfive.project.domain.Entity;
import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.profile.Profile;
import switchfive.project.domain.store.ProfileStore;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * User class describes the data and the methods of its objects.
 *
 * @author Daniel dos Santos Torres
 * @version 0
 * @since 30-12-2021
 */

public class User implements Entity<User> {
    private final UserID id;
    /**
     * User email.
     */
    private final Email email;
    /**
     * User profile list.
     */
    private final List<ProfileID> userProfileList = new ArrayList<>();
    /**
     * User activation.
     */
    private final Activation activation;
    /**
     * User password.
     */
    private Password password;
    /**
     * User name.
     */
    private UserName userName;
    /**
     * User function.
     */
    private Function function;

    /**
     * User constructor with name, email, photo, function, password.
     *
     * @param userName final UserName userNameInput
     * @param email    final Email emailInput
     * @param function final Function functionInput
     * @param password final Password passwordInput
     * @param profile  final Profile profileInput
     */
    public User(final int id, final String userName, final String email,
                final String function,
                final String password, ProfileID profile) throws NoSuchAlgorithmException {
        this.id = UserID.createUserID(id);
        this.userName = UserName.createUsername(userName);
        this.email = Email.createEmail(email);
        this.function = Function.createFunction(function);
        this.password = Password.createPassword(password);
        userProfileList.add(profile);
        this.activation = Activation.createActivation();
    }

    /**
     * User constructor with name, email, photo, function, password.
     *
     * @param id        final User identifier
     * @param userName  final UserName userNameInput
     * @param email     final Email emailInput
     * @param function  final Function functionInput
     * @param password  final Password passwordInput
     * @param profileID final Profile profileInput
     */
    public User(UserID id,
                Email email,
                Password password,
                UserName userName,
                Function function,
                ProfileID profileID) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.activation = Activation.createActivation();
        this.userName = userName;
        this.function = function;
        userProfileList.add(profileID);
    }

    /**
     * A method to compare two profiles if they are different return true;
     *
     * @param actualProfile    the first profile.
     * @param pretendedProfile the second profile.
     * @return true if both profile are different, false if not.
     */
    public static boolean areProfilesDifferent(Profile actualProfile,
                                               Profile pretendedProfile) {
        return !actualProfile.equals(pretendedProfile);
    }

    /**
     * @return the email of a specific user in a string format.
     */
    public String getEmail() {

        return this.email.getUserEmail();
    }

    /**
     * @param oldPassword is the old user password
     * @param newPassword is the new user password
     * @return true if password is updated, false otherwise.
     * @author Tânia Mota
     * public method which updates password.
     */
    public boolean isPasswordChanged(String oldPassword,
                                     String newPassword) throws NoSuchAlgorithmException {
        boolean updated = false;

        if (newPassword != null) {

            if (areTheSameHash(oldPassword)) {
                changePassword(newPassword);
                updated = true;
            }
        }
        return updated;
    }

    /**
     * A get method for the User account password
     */

    public String getPassword() {
        return password.toString();
    }

    /**
     * A method with the objective of changing the User account password.
     *
     * @param newPassword the new changed password in string format.
     */
    public void changePassword(String newPassword) throws NoSuchAlgorithmException {
        Password password = Password.createPassword(newPassword);
        this.password = password;
    }

    /**
     * Auxiliary method for comparing the input old password from the user
     * and the old password saved in the system;
     *
     * @param inputOldPasswordString is the input password from the user
     * @return true if old input password from user is the same as the one in
     * system,
     * false otherwise
     */
    public boolean areTheSameHash(String inputOldPasswordString) throws NoSuchAlgorithmException {
        Password inputOldPassword = Password.createPassword(inputOldPasswordString);
        return inputOldPassword.sameValueAs(this.password);
    }

    /**
     * A get method for the User list of profiles password.
     *
     * @return a List made up of the user profiles
     */
    public List<ProfileID> getUserProfileList() {
        return userProfileList;
    }

    /**
     * @param email that you want to validate
     * @return true if the user's email is the same as the given parameter,
     * false if not
     */
    public boolean compareEmail(final String email) {
        return getEmail().toLowerCase(Locale.ROOT).contains(email.toLowerCase(Locale.ROOT).trim());
    }

    public String getUserName() {
        return userName.getUserName();
    }

    private void setUserName(UserName newUserName) {
        this.userName = newUserName;
    }

    /**
     * Method for activating user account.
     *
     * @param inputCode the code inserted by the user.
     * @return the user account is activated.
     */
    public boolean activateAccount(String inputCode) {
        return this.activation.validateActivation(inputCode);
    }

    /**
     * Get method for activating an account.
     *
     * @return activation status.
     */
    public Activation getActivation() {
        return activation;
    }

    /**
     * Set method for activating an account.
     *
     * @param code in string format which will be set to activate
     *             a just registered user.
     */
    public void setActivation(String code) {
        getActivation().setCode(code);
    }

    /**
     * if user status is active, sets it to inactive and also sets the
     * deactivation date
     * to the current date.
     *
     * @return true is it was set to inactive, false otherwise.
     */
    public boolean setToInactive() {
        boolean result = false;
        if (userActive()) {
            setActivationStatusToInactive();
            setInactivationDate();
            result = true;
        }
        return result;
    }

    /**
     * gets activation object and sets its inactivationDate attribute to false.
     */
    private void setInactivationDate() {
        getActivation().setInactivationDate();
    }

    /**
     * gets activation object and sets his isActivated attribute to false.
     */
    private void setActivationStatusToInactive() {
        getActivation().setToInactive();
    }

    /**
     * checks if the user isActivated attribute is true.
     *
     * @return true if it is.
     */
    private boolean userActive() {
        return getActivation().isActivated();
    }

    /**
     * if user status is inactive, sets it to active and also sets the
     * activation date
     * to the current date.
     *
     * @return true is it was set to active, false otherwise.
     */
    public boolean setToActive() {
        boolean result = false;
        if (!userActive()) {
            setActivationStatusToActive();
            setActivationDate();
            result = true;
        }
        return result;
    }

    /**
     * gets activation object and sets his activation date.
     */
    private void setActivationDate() {
        getActivation().setActivationDate();
    }

    /**
     * gets activation object and sets his isActivated status to true.
     */
    private void setActivationStatusToActive() {
        getActivation().setToActive();
    }


    /**
     * This method will delete a specific userProfile and add a new one. It
     * will only add a pretendedProfile if the actualProfile exists.
     *
     * @param actualProfile    profile that will be deleted
     * @param pretendedProfile profile that will be added
     * @return true if change is successfully done, false if not.
     */
    public boolean changeProfile(ProfileID actualProfile,
                                 ProfileID pretendedProfile) {
        return userProfileList.remove(actualProfile) && userProfileList.add(pretendedProfile);
    }

    /**
     * This method will iterate through a Users profile list, look for a
     * actualProfile, delete it and add a pretendedProfile.
     *
     * @param actualProfile    profile that will be deleted
     * @param pretendedProfile profile that will be added
     * @return true if the actualProfile is deleted and pretendedProfile is
     * added, false if not.
     */
    public boolean updateProfile(final ProfileID actualProfile,
                                 final ProfileID pretendedProfile) {
        boolean result = false;

        for (ProfileID selectedProfile : userProfileList) {
            if (actualProfile.equals(selectedProfile)) {
                result = changeProfile(selectedProfile, pretendedProfile);
            }
        }
        return result;
    }

    /**
     * Checks if a user has a given profile.
     *
     * @param profile the profile to be checked.
     * @return true if it has the profile, false otherwise.
     */
    public boolean hasProfile(ProfileID profile) {
        boolean result = false;
        for (ProfileID selectedProfile : userProfileList) {
            if (selectedProfile.equals(profile) && !result) {
                result = true;
            }
        }
        return result;
    }
/*
    *//**
     * Updates the user account's name, function and photo if a valid photo extension is passed.
     *
     * @param newName       a String to be set as the new name.
     * @param newFunction   a String to be set as the new function.
     * @param inputFilePath a String with the path of the new photo.
     * @return true if the update was successful, false otherwise.
     *//*
    public boolean updateUserInformation(UserName newName,
                                         Function newFunction,
                                         String inputFilePath) throws IOException {
        boolean updated = false;
        String extension = FilenameUtils.getExtension(inputFilePath);
        int VALID_EXTENSION_SIZE = 3;
        if (extension.length() == VALID_EXTENSION_SIZE) {
            setUserName(newName);
            setFunction(newFunction);
            updated = true;
        }
        return updated;
    }*/

    /**
     * Updates the user account's name, function and photo.
     *
     * @param newName     a String to be set as the new name.
     * @param newFunction a String to be set as the new function.
     */
    public void updateUserInformation(UserName newName,
                                      Function newFunction) {
        setUserName(newName);
        setFunction(newFunction);
    }

    /**
     * Public method that checks if a User has the Visitor Profile.
     *
     * @return true if User already has the visitor Profile.
     */
    public boolean doesUserHaveVisitorProfile() {
        // TODO Implementar - encontrar Profile Visitor através do ID para comparar

        boolean doesUserHaveVisitorProfile = false;
        List<ProfileID> userProfilesOfUser = this.getUserProfileList();
        DataManagement dataManagement = DataManagement.getInstance();
        ProfileStore profileStore = dataManagement.getProfiles();

        String visitorProfileDescription = "Visitor";

        int iterator = 0;

        while (!doesUserHaveVisitorProfile && iterator < userProfilesOfUser.size()) {
            doesUserHaveVisitorProfile = profileStore
                    .getProfileByID(userProfilesOfUser.get(iterator))
                    .compareProfileDesignation(visitorProfileDescription);
            iterator++;
        }

        return doesUserHaveVisitorProfile;
    }

    /**
     * Method that checks if a User already has the requested Profile
     * associated to him.
     *
     * @param requestedProfile Profile Identifier object
     * @return true if User already has the requested Profile
     */
    public boolean doesUserAlreadyHaveRequestedProfile(final ProfileID requestedProfile) {
        return this.getUserProfileList().contains(requestedProfile);
    }

    public boolean getAccountStatus() {
        return getActivation().isActivated();
    }

    public Function getFunction() {
        return function;
    }

    private void setFunction(Function newFunction) {
        this.function = newFunction;
    }

    public String getFunctionDescription() {
        return getFunction().getDescription();
    }

    public List<String> getProfileList() {
        // TODO Implementar - obter Descrições dos Profiles através do ID através do ID para comparar
        List<String> profileList = new ArrayList<>();

        DataManagement dataManagement = DataManagement.getInstance();
        ProfileStore profileStore = dataManagement.getProfiles();

        for (ProfileID selectedProfile : getUserProfileList()) {

            String profile = profileStore.getProfileByID(selectedProfile).toString();
            profileList.add(profile);
        }
        return profileList;
    }

    /**
     * Method that determines if a User object is equal. A User object is
     * considered equal if another User
     * object has the same e-mail.
     *
     * @param o Object to compare
     * @return true if object is equal
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return sameIdentityAs(user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean sameIdentityAs(User other) {
        return other != null && this.id.sameValueAs(other.id);
    }
}





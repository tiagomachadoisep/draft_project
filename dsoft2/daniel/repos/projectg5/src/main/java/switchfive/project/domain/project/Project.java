package switchfive.project.domain.project;

import switchfive.project.domain.Entity;
import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.typology.TypologyID;
import switchfive.project.domain.Time;
import switchfive.project.factories.abstractFactories.IResourceFactory;

import java.util.Objects;

/**
 * This is the «PRODUCT» component of builder pattern.
 */
public class Project implements Entity<Project> {
    /**
     * Typology identity designation.
     */
    private TypologyID typologyID;
    /**
     * Customer identity designation.
     */
    private CustomerID customerID;
    /**
     * Unique project identification code.
     */
    private ProjectCode code;
    /**
     * Project name.
     */
    private ProjectName name;
    /**
     * Project description.
     */
    private ProjectDescription description;
    /**
     * Customer businessSector.
     */
    private ProjectBusinessSector businessSector;
    /**
     * Includes start date and end date and validations.
     */
    private Time dates;
    /**
     * Number of planned sprints.
     * Must be a positive integer between 1 and 999.
     */
    private ProjectNumberOfPlannedSprints numberOfPlannedSprints;
    /**
     * Budget - monetary amount available for resource spending.
     * Must be a positive double greater than 0.
     */
    private ProjectBudget budget;
    /**
     * The duration of a certain sprint.
     */
    private ProjectSprintDuration sprintDuration;
    /**
     * Status of the project.
     */
    private ProjectStatus status;
    /**
     * Resource factory interface.
     */
    private IResourceFactory IResourceFactory;

    /**
     * Empty constructor.
     */
    public Project() {
    }

    /**
     * @param codeInput            ProjectCode (Value Object)
     * @param nameInput            ProjectName (Value Object)
     * @param descriptionInput     ProjectDescription (Value Object)
     * @param customerIdInput      CustomerID (Value Object)
     * @param IResourceFactoryInput ResourceFactory(Common Java
     *                             Interface)
     */
    public void initialize(final ProjectCode codeInput,
                           final ProjectName nameInput,
                           final ProjectDescription descriptionInput,
                           final CustomerID customerIdInput,
                           final IResourceFactory IResourceFactoryInput) {
        this.code = codeInput;
        this.name = nameInput;
        this.description = descriptionInput;
        this.customerID = customerIdInput;
        this.IResourceFactory = IResourceFactoryInput;
        this.status = ProjectStatus.Planned;
    }

    /**
     * @param typologyIdInput TypologyID (Value Object)
     */
    public void addTypologyID(final TypologyID typologyIdInput) {
        this.typologyID = typologyIdInput;
    }

    /**
     * @param businessSectorInput ProjectBusinessSector (Value Object)
     */
    public void addBusinessSector(
            final ProjectBusinessSector businessSectorInput) {
        this.businessSector = businessSectorInput;
    }

    /**
     * @param datesInput Time (Value Object)
     */
    public void addDates(final Time datesInput) {
        this.dates = datesInput;
    }

    /**
     * @param plannedSprintsInput ProjectNumberOfPlannedSprints (Value
     *                            Object)
     */
    public void addNumberOfPlannedSprints(
            final ProjectNumberOfPlannedSprints plannedSprintsInput) {
        this.numberOfPlannedSprints = plannedSprintsInput;
    }

    /**
     * @param budgetInput ProjectBudget (Value Object)
     */
    public void addBudget(final ProjectBudget budgetInput) {
        this.budget = budgetInput;
    }

    /**
     * @param sprintDurationInput ProjectSprintDuration (Value Object)
     */
    public void addSprintDuration(
            final ProjectSprintDuration sprintDurationInput) {
        this.sprintDuration = sprintDurationInput;
    }

    public boolean sameTypologyAs(final TypologyID other) {
        return this.typologyID.sameValueAs(other);
    }

    public boolean sameBusinessAs(final ProjectBusinessSector other) {
        return this.businessSector.sameValueAs(other);
    }

    public boolean sameDatesAs(final Time other) {
        return this.dates.sameValueAs(other);
    }

    public boolean sameNumberOfSprintsAs(
            final ProjectNumberOfPlannedSprints other) {
        return this.numberOfPlannedSprints.sameValueAs(other);
    }

    public boolean sameBudgetAs(final ProjectBudget other) {
        return this.budget.sameValueAs(other);
    }

    public boolean sameDurationAs(final ProjectSprintDuration other) {
        return this.sprintDuration.sameValueAs(other);
    }


    /**
     * @param other The other entity.
     * @return true if instance «other» have the same identity as this;
     * otherwise, returns false.
     */
    @Override public boolean sameIdentityAs(final Project other) {
        return other != null && code.sameValueAs(other.code);
    }

    /**
     * @param other object to compare
     * @return true if compared objects are equals; otherwise, returns false.
     */
    @Override public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Project)) {
            return false;
        }
        Project that = (Project) other;
        return sameIdentityAs(that);
    }

    /**
     * @return new hashcode.
     */
    @Override public int hashCode() {
        return Objects.hash(code);
    }
}


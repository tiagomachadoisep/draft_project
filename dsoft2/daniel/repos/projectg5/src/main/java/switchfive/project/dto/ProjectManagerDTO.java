package switchfive.project.dto;

public class ProjectManagerDTO {
    /**
     * Email that identifies the user who takes the action.
     */
    public int userID;
    /**
     * Resource cost per hour, as double.
     */
    public double costPerHour;
    /**
     * Resource percentage of allocation, as double.
     */
    public double percentageOfAllocation;

    public ProjectManagerDTO(int userID, double costPerHour,
                             double percentageOfAllocation) {
        this.userID = userID;
        this.costPerHour = costPerHour;
        this.percentageOfAllocation = percentageOfAllocation;
    }
}

package switchfive.project.domain.user;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Photo class describes the data and the methods of its objects.
 *
 * @author Maurício Pinto Barros
 * @version 0
 * @since Meeting 28-12-2021
 */

public class Photo {
    /**
     * Photo height.
     */
    private final int height;
    /**
     * Photo width.
     */
    private final int width;
    /**
     * Photo file extension.
     */
    private final String extension;
    /**
     * Path of the image.
     */
    private final String imageLocation;

    /**
     * Writes a photo in the output path, given a valid image, that is located in the input image path.
     *
     * @param photoPath a String with the input path.
     * @throws IOException if an error occurs or when not able to create required ImageInputStream.
     */
    public Photo(final String photoPath) throws IOException {
        if (isValidPhoto(photoPath)) {
            String destinationPath = generateValidRelativePath(photoPath);

            writePhoto(photoPath, destinationPath);

            this.height = getBufferedImageHeight(photoPath);
            this.width = getBufferedImageWidth(photoPath);
            this.extension = getFormat(photoPath);
            this.imageLocation = destinationPath;
        } else {
            throw new IllegalArgumentException("INVALID_PHOTO");
        }
    }

    private int getBufferedImageHeight(String imagePath) throws IOException {
        File initialImage = new File(imagePath);
        BufferedImage bImage = ImageIO.read(initialImage);
        return bImage.getHeight();
    }

    private int getBufferedImageWidth(String imagePath) throws IOException {
        File initialImage = new File(imagePath);
        BufferedImage bImage = ImageIO.read(initialImage);
        return bImage.getWidth();
    }
    /**
     * Get the file extension of the path if the extension is valid.
     * Accepted extensions are .png or .jpg.
     *
     * @param imagePath the path of the image.
     * @return .png if the file is png, .jpg if the file is jpg, null otherwise.
     */
    private String getFormat(final String imagePath) {
        String format = null;

        if (imagePath.endsWith(".png")) {
            format = ".png";
        } else if (imagePath.endsWith(".jpg")) {
            format = ".jpg";
        }
        return format;
    }

    /**
     * Checks if the image has a valid size, between 0x0 and 300x300.
     *
     * @param inputImagePath the image path.
     * @return true if it is within the valid size, false otherwise
     */
    private boolean isValidSize(final String inputImagePath) throws IOException {
        boolean validSize = false;

        File imageFile = new File(inputImagePath);
        BufferedImage image = ImageIO.read(imageFile);

        int imageWidth = image.getWidth();
        int imageHeight = image.getHeight();

        if (isValidHeight(imageHeight) && isValidWidth(imageWidth)) {
            validSize = true;
        }
        return validSize;
    }

    private boolean isValidWidth(int imageWidth) {
        final int MIN_WIDTH = 0; // Image must have at least one pixel
        final int MAX_WIDTH = 301; // Image must have 300 pixels at most.

        return MIN_WIDTH < imageWidth && imageWidth < MAX_WIDTH;
    }

    private boolean isValidHeight(int imageHeight) {
        final int MIN_HEIGHT = 0; // Image must have at least one pixel
        final int MAX_HEIGHT = 301; // Image must have 300 pixels at most.

        return MIN_HEIGHT < imageHeight && imageHeight < MAX_HEIGHT;
    }

    private boolean isValidFormat(final String imagePath) {
        String format = getFormat(imagePath);
        return format != null;
    }

    private boolean isValidPhoto(final String imagePath) throws IOException {
        return isValidFormat(imagePath) && isValidSize(imagePath);
    }

    private void writePhoto(final String imagePath, String destinationPath) throws IOException {
        String format = getFormat(imagePath);

        File initialImage = new File(imagePath);
        BufferedImage bImage = ImageIO.read(initialImage);

        ImageIO.write(bImage, format, new File(destinationPath));
    }

    private String generateImageIdentifier() {
        UUID randomCode = UUID.randomUUID();
        return randomCode.toString();
    }

    private String generateValidRelativePath(final String image) {
        final String REPOSITORY_RELATIVE_PATH = "src/main/resources/";
        String uniqueIdentifier = generateImageIdentifier();
        String format = getFormat(image);
        return REPOSITORY_RELATIVE_PATH + uniqueIdentifier + format;
    }
}

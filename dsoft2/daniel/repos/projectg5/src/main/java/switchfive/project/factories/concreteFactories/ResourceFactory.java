package switchfive.project.factories.concreteFactories;

import org.springframework.stereotype.Component;
import switchfive.project.domain.resource.ResourceID;
import switchfive.project.domain.user.UserID;
import switchfive.project.domain.Time;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.resource.Resource;
import switchfive.project.domain.resource.ResourceCostPerHour;
import switchfive.project.domain.resource.ResourcePercentageOfAllocation;
import switchfive.project.domain.resource.Role;
import switchfive.project.factories.abstractFactories.IResourceFactory;

@Component("resourceFactory")
public final class ResourceFactory implements IResourceFactory {
    static ResourceFactory instance;

    private ResourceFactory() {
    }

    public static ResourceFactory getInstance() {
        if (instance == null) {
            instance = new ResourceFactory();
        }
        return instance;
    }

    public Resource createResource(final ResourceID identityInput,
                                   final UserID userIdInput,
                                   final ProjectCode projectCodeInput,
                                   final Time datesInput,
                                   final ResourceCostPerHour costPerHourInput,
                                   final ResourcePercentageOfAllocation
                                           allocationInput,
                                   final Role roleInput) {
        return new Resource(identityInput, userIdInput, projectCodeInput,
                datesInput, costPerHourInput, allocationInput, roleInput);
    }
}

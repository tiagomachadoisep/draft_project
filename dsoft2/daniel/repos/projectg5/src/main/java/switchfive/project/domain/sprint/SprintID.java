package switchfive.project.domain.sprint;

import switchfive.project.domain.ValueObject;
import switchfive.project.domain.task.TaskContainer;

import java.util.Objects;

public class SprintID implements TaskContainer, ValueObject<SprintID> {

    private String projectCode;

    private int sprintID;

    private SprintID(String projectCode, int sprintID) {
        this.projectCode = projectCode;
        this.sprintID = sprintID;
    }

    public static SprintID createSprintID(String projectCode, int sprintID) {
        return new SprintID(projectCode, sprintID);
    }

    @Override
    public boolean sameValueAs(SprintID other) {
        return other != null && projectCode.equals(other.projectCode) && sprintID == (other.sprintID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SprintID _sprintID = (SprintID) o;
        return sameValueAs(_sprintID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectCode, sprintID);
    }
}

package switchfive.project.domain.factory;

import switchfive.project.domain.userStory.UserStoryID;
import switchfive.project.domain.userStory.Priority;
import switchfive.project.domain.userStory.UserStoryCode;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.userStory.UserStory;
import switchfive.project.domain.userStory.UserStoryDescription;

public interface UserStoryFactory {

    UserStory createUserStory(UserStoryID userStoryID, ProjectCode projectCode, UserStoryCode userStoryCode,
                              Priority priority, UserStoryDescription description);
}

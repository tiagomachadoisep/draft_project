package switchfive.project.applicationServices.abstractRepositories;

import switchfive.project.domain.project.Project;

public interface IProjectRepository {
    boolean projectAlreadyExist(Project project);
    void saveNew(Project newProject);
}

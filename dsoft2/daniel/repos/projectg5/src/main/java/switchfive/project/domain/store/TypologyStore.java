package switchfive.project.domain.store;

import switchfive.project.domain.typology.TypologyID;
import switchfive.project.domain.typology.Typology;
import switchfive.project.domain.factory.TypologyFactory;
import switchfive.project.domain.typology.TypologyDescription;

import java.util.HashSet;
import java.util.Set;

/**
 * TypologyStore describes the data and the methods of its objects.
 */
public class TypologyStore {
    /**
     * Store of typologies.
     */
    private Set<Typology> typologies;
    /**
     * Factory pattern - create object without exposing the creation logic to
     * the client and refer to newly created object using a common interface.
     */
    private TypologyFactory typologyFactory;

    /**
     * Constructor with factory input.
     *
     * @param factory final TypologyFactory factory
     */
    public TypologyStore(final TypologyFactory factory) {
        this.typologies = new HashSet<>();
        this.typologyFactory = factory;
    }

    /**
     * @param description typology description.
     * @return true if operation is successful; otherwise, returns
     * false.
     */
    public boolean createAndAdd(final TypologyDescription description) {
        TypologyID identity =
                TypologyID.createTypologyID(typologies.size() + 1);
        Typology newTypology = typologyFactory.createTypology(identity,
                description);

        for (Typology each : typologies) {
            if (each.sameIdentityAs(newTypology)) {
                return false;
            }
        }

        typologies.add(newTypology);

        return true;
    }
}

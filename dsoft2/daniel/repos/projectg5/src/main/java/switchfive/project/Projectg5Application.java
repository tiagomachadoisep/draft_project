package switchfive.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import switchfive.project.controllers.concreteControllers.AddProjectController;
import switchfive.project.dto.ProjectCreationDTO;
import switchfive.project.dto.ProjectManagerDTO;
import switchfive.project.dto.TimeDTO;

import java.text.ParseException;

@SpringBootApplication
public class Projectg5Application {

    public static void main(String[] args) throws ParseException {
        ApplicationContext context =
                SpringApplication.run(Projectg5Application.class,
                        args);

        AddProjectController addProjectController =
                (AddProjectController) context.getBean("addProjectController");

        TimeDTO timeDto = new TimeDTO("18/04/2023","18/05/2024");

        ProjectManagerDTO pmDto = new ProjectManagerDTO(1, 100, 100);

        ProjectCreationDTO dto =
                new ProjectCreationDTO("isep2", "Instituto Superior de " +
                        "Engenharia do Porto", "ISEP/DEI",
                        "Educational", "Free", 1, 1, 0, 1,
                        timeDto, pmDto);

        addProjectController.addProject(dto);

        if (addProjectController.addProject(dto)) {
            System.out.println("Good job!");
        } else {
            System.out.println("Already exists");
        }
    }

}

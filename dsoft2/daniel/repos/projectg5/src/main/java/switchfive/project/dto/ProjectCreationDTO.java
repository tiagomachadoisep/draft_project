package switchfive.project.dto;

import switchfive.project.domain.customer.CustomerID;

/**
 * ProjectDTO class describes the data of its objects.
 */
public class ProjectCreationDTO {
    public String code;
    public String name;
    public String description;
    public String businessSector;
    public String typology;
    public int numberOfPlannedSprints;
    public int sprintDuration;
    public double budget;
    public int customerID;
    public TimeDTO time;
    public ProjectManagerDTO projectManager;

    public ProjectCreationDTO(final String codeInput, final String nameInput,
                              final String descriptionInput,
                              final String businessSectorInput,
                              final String typologyInput,
                              final int numberOfPlannedSprintsInput,
                              final int sprintDurationInput, final double budgetInput,
                              final int customerInput,
                              final TimeDTO timeInput,
                              final ProjectManagerDTO projectManagerInput) {
        this.code = codeInput;
        this.name = nameInput;
        this.description = descriptionInput;
        this.businessSector = businessSectorInput;
        this.typology = typologyInput;
        this.numberOfPlannedSprints = numberOfPlannedSprintsInput;
        this.sprintDuration = sprintDurationInput;
        this.budget = budgetInput;
        this.customerID = customerInput;
        this.time = timeInput;
        this.projectManager = projectManagerInput;
    }

}

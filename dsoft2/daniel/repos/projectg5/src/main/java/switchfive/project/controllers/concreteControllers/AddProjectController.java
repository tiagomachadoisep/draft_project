package switchfive.project.controllers.concreteControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import switchfive.project.applicationServices.abstractServices.IProjectService;
import switchfive.project.controllers.abstractControllers.IAddProjectController;
import switchfive.project.domain.Time;
import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.project.ProjectDescription;
import switchfive.project.domain.project.ProjectName;
import switchfive.project.domain.resource.ResourceCostPerHour;
import switchfive.project.domain.resource.ResourcePercentageOfAllocation;
import switchfive.project.domain.user.UserID;
import switchfive.project.dto.ProjectCreationDTO;

import java.text.ParseException;

@Controller("addProjectController")
public class AddProjectController implements IAddProjectController {
    private final IProjectService iProjectService;

    //@Autowired
    public AddProjectController(final IProjectService iProjectService) {
        this.iProjectService = iProjectService;
    }

    
    public boolean addProject(ProjectCreationDTO projectDTO)
            throws ParseException {
        CustomerID customerID =
                CustomerID.createIDCustomer(projectDTO.customerID);
        UserID userID = UserID.createUserID(projectDTO.projectManager.userID);

        ProjectCode code = ProjectCode.createProjectCode(projectDTO.code);
        ProjectName name = ProjectName.create(projectDTO.name);
        ProjectDescription description =
                ProjectDescription.create(projectDTO.description);
        Time time =
                Time.create(projectDTO.time.startDate, projectDTO.time.endDate);

        ResourceCostPerHour costPerHour =
                ResourceCostPerHour.create(
                        projectDTO.projectManager.costPerHour);
        ResourcePercentageOfAllocation percentageOfAllocation =
                ResourcePercentageOfAllocation.create(
                        projectDTO.projectManager.percentageOfAllocation);

        if (this.iProjectService.addProject(code, name, description,
                customerID, time)) {
            return this.iProjectService.setProjectManager(
                    userID, code, time, costPerHour, percentageOfAllocation);
        }
        return false;
    }
}

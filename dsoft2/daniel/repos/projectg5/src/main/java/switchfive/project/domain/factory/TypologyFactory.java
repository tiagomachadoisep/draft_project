package switchfive.project.domain.factory;

import switchfive.project.domain.typology.TypologyID;
import switchfive.project.domain.typology.Typology;
import switchfive.project.domain.typology.TypologyDescription;

public interface TypologyFactory {

    Typology createTypology(TypologyID identity,
                            TypologyDescription description);
}

package switchfive.project.domain.customer;

import switchfive.project.domain.Entity;

import java.util.Objects;

/**
 * Customer class describes the data and the methods of its objects.
 * @since Meeting 28-12-2021
 */

public class Customer implements Entity<Customer> {

    /**
     * Number of the Customer. Starts with 1 and it's auto-incremented.
     */
    private CustomerID customerID;

    /**
     * Customer designation.
     */
    private CustomerName customerName;

    public Customer(CustomerID customerID, CustomerName customerName) {
        this.customerID = customerID;
        this.customerName = customerName;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Customer customer = (Customer) object;
        return sameIdentityAs(customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID);
    }

    @Override
    public boolean sameIdentityAs(Customer other) {
        return other != null &&
                this.customerID.equals(other.customerID);
    }
}

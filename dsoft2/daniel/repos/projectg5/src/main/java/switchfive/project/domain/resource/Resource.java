package switchfive.project.domain.resource;

import switchfive.project.domain.Entity;
import switchfive.project.domain.user.UserID;
import switchfive.project.domain.Time;
import switchfive.project.domain.project.ProjectCode;

import java.util.Objects;

/**
 * Resource class describes the data and the methods of its objects.
 */
public class Resource implements Entity<Resource> {
    /**
     * Random value.
     */
    private final ResourceID identity;
    /**
     * User who integrates the resource.
     */
    private final UserID userID;
    /**
     * Project who integrates the resource.
     */
    private final ProjectCode projectCode;
    /**
     * Resource allocation start and end date.
     */
    private final Time date;
    /**
     * Resource cost per hour.
     */
    private final ResourceCostPerHour costPerHour;
    /**
     * Resource percentage of allocation.
     */
    private final ResourcePercentageOfAllocation allocation;
    /**
     * Enum Role is a special "class" that represents a group of constants
     * (unchangeable variables).
     */
    private final Role role;

    /**
     * @param identityInput    final ID_Resource
     *                         identityInput
     * @param userIDInput      final ID_User userIDInput
     * @param projectCodeInput final ID_User projectIdentityInput
     * @param datesInput       final Time datesInput
     * @param costPerHourInput final ResourceCostPerHour
     *                         costPerHourInput
     * @param allocationInput  final
     *                         ResourcePercentageOfAllocation
     *                         allocationInput
     * @param roleInput        final Role roleInput
     */
    public Resource(final ResourceID identityInput,
                    final UserID userIDInput,
                    final ProjectCode projectCodeInput,
                    final Time datesInput,
                    final ResourceCostPerHour costPerHourInput,
                    final ResourcePercentageOfAllocation
                            allocationInput,
                    final Role roleInput) {
        this.identity = identityInput;
        this.userID = userIDInput;
        this.projectCode = projectCodeInput;
        this.date = datesInput;
        this.costPerHour = costPerHourInput;
        this.allocation = allocationInput;
        this.role = roleInput;
    }

    /**
     * @param other The other entity.
     * @return true if other have same identity as this; otherwise, returns
     * false.
     */
    @Override
    public boolean sameIdentityAs(final Resource other) {
        return other != null && this.identity.sameValueAs(other.identity);
    }

    /**
     * @param other other instance
     * @return true id objects are equals; otherwise, returns false.
     */
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Resource)) {
            return false;
        }
        Resource resource = (Resource) other;
        return sameIdentityAs(resource);
    }

    /**
     * @return hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(identity);
    }
}

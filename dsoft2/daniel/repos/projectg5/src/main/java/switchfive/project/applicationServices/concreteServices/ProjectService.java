package switchfive.project.applicationServices.concreteServices;

import org.springframework.stereotype.Service;
import switchfive.project.applicationServices.abstractRepositories.IProjectRepository;
import switchfive.project.applicationServices.abstractRepositories.IResourceRepository;
import switchfive.project.applicationServices.abstractServices.IProjectService;
import switchfive.project.domain.Time;
import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.project.Project;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.project.ProjectDescription;
import switchfive.project.domain.project.ProjectName;
import switchfive.project.domain.resource.Resource;
import switchfive.project.domain.resource.ResourceCostPerHour;
import switchfive.project.domain.resource.ResourceID;
import switchfive.project.domain.resource.ResourcePercentageOfAllocation;
import switchfive.project.domain.resource.Role;
import switchfive.project.domain.user.UserID;
import switchfive.project.factories.abstractFactories.IProjectBuilder;
import switchfive.project.factories.abstractFactories.IResourceFactory;

import java.text.ParseException;

@Service("projectService")
public class ProjectService implements IProjectService {
    private final IProjectRepository iProjectRepository;
    private final IResourceRepository iResourceRepository;
    private final IResourceFactory iResourceFactory;
    private final IProjectBuilder iProjectBuilder;

    public ProjectService(IProjectRepository iProjectRepository,
                          IResourceRepository iResourceRepository,
                          IResourceFactory iResourceFactory,
                          IProjectBuilder iProjectBuilder) {
        this.iProjectRepository = iProjectRepository;
        this.iResourceRepository = iResourceRepository;
        this.iResourceFactory = iResourceFactory;
        this.iProjectBuilder = iProjectBuilder;
    }

    public boolean addProject(final ProjectCode code, final ProjectName name,
                              final ProjectDescription description,
                              final CustomerID customerID, final Time time) {
        this.iProjectBuilder.initialize(code, name, description, customerID,
                iResourceFactory);
        Project project = this.iProjectBuilder.getProject();
        if (!this.iProjectRepository.projectAlreadyExist(project)) {
            this.iProjectRepository.saveNew(project);
            return true;
        }

        return false;
    }

    public boolean setProjectManager(final UserID userId,
                                     final ProjectCode code,
                                     final Time time,
                                     final ResourceCostPerHour costPerHour,
                                     final ResourcePercentageOfAllocation percentageOfAllocation)
            throws ParseException {
        final ResourceID newResourceID = ResourceID.createResourceID();

        Resource projectManager = this.iResourceFactory.createResource(
                newResourceID, userId, code, time, costPerHour,
                percentageOfAllocation, Role.ProjectManager);

        return this.iResourceRepository.saveNew(projectManager);
    }

}

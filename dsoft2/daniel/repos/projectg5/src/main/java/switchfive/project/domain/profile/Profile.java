package switchfive.project.domain.profile;

import switchfive.project.domain.Entity;

import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Profile class describes the data and the methods of its objects.
 *
 * @author Daniel dos Santos Torres
 * @version 0
 * @since 30-12-2021
 */

public class Profile implements Entity<Profile> {

    private ProfileID identity;

    /**
     * Profile designation.
     */
    private String designation;

    /**
     * Profile constructor. The designation describes the Profile.
     *
     * @param designation can't be null.
     */

    public Profile(final String designation, ProfileID identity) {
        if (Stream.of(designation, identity).allMatch(Objects::nonNull) &&
                validateProfileDesignationSize(designation)) {
            this.designation = designation;
            this.identity = identity;
        } else {
            throw new IllegalArgumentException("Invalid input arguments");
        }
    }


    /**
     * Method that compares the Profile object designation with an input String. This comparison is not
     * case-sensitive.
     *
     * @param profileDesignation String to be compared
     * @return true if both are equal
     */
    public boolean compareProfileDesignation(final String profileDesignation) {
        if (Stream.of(profileDesignation).allMatch(Objects::nonNull)) {
            return this.designation.toLowerCase(Locale.ROOT).trim().
                    equals(profileDesignation.toLowerCase(Locale.ROOT).trim());
        }
        return false;
    }


    /**
     * Validates if the input String (regarding the Profile description) is not empty and
     * not above 50 characters.
     *
     * @param profileDesignation input String to be evaluated.
     * @return true if profileDesignation is not empty and not above the limit.
     */
    private boolean validateProfileDesignationSize(final String profileDesignation) {
        if (!profileDesignation.isEmpty() && profileDesignation.length() < 50) {
            return true;
        } else {
            throw new IllegalArgumentException("Profile Description must be within limits");
        }
    }

    /**
     * Checks if unique Profile identifier is the same.
     *
     * @param profileID input ID_Profile
     * @return true if same identifier
     */
    public boolean compareProfileID(ProfileID profileID) {
        return this.identity.sameValueAs(profileID);
    }

    /**
     * Method that determines if a Profile object is equal. A Profile object is considered equal
     * if another Profile object has the same description (using not-case sensitive comparison).
     *
     * @param o Object to compare
     * @return true if object is equal
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profile)) {
            return false;
        }
        Profile profile = (Profile) o;
        return sameIdentityAs(profile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identity);
    }

    @Override
    public boolean sameIdentityAs(Profile other) {
        return this.identity.sameValueAs(other.identity);
    }
}

package switchfive.project.factories.abstractFactories;

import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.project.Project;
import switchfive.project.domain.project.ProjectBudget;
import switchfive.project.domain.project.ProjectBusinessSector;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.project.ProjectDescription;
import switchfive.project.domain.project.ProjectName;
import switchfive.project.domain.project.ProjectNumberOfPlannedSprints;
import switchfive.project.domain.project.ProjectSprintDuration;
import switchfive.project.domain.typology.TypologyID;
import switchfive.project.domain.Time;

import java.text.ParseException;

public interface IProjectBuilder {
    void initialize(ProjectCode codeInput,
                    ProjectName nameInput,
                    ProjectDescription descriptionInput,
                    CustomerID customerIdInput,
                    IResourceFactory factory);

    void addBusinessSector(ProjectBusinessSector businessSectorInput);

    void addBudget(ProjectBudget budgetInput);

    void addTime(Time datesInput) throws ParseException;

    void addSprintDuration(ProjectSprintDuration sprintDurationInput);

    void addNumberOfPlannedSprints(
            ProjectNumberOfPlannedSprints numberOfPlannedSprintsInput);

    void addTypologyID(TypologyID selectedTypology);


    Project getProject();
}

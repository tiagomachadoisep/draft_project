package switchfive.project.domain.store;

import switchfive.project.domain.customer.Customer;

import java.util.HashSet;
import java.util.Set;

public class CustomerStore {
    private final Set<Customer> customerRepository;

    public CustomerStore() {
        this.customerRepository = new HashSet<>();
    }


    public void addCustomerList(Customer name) {
        this.customerRepository.add(name);
    }

}

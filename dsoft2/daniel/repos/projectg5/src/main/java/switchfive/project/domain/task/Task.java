package switchfive.project.domain.task;

import switchfive.project.domain.*;
import switchfive.project.domain.resource.Resource;
import switchfive.project.domain.store.TaskStatus;

import java.util.*;

/**
 * Task class describes the data and the methods of its objects.
 *
 * @author dst
 */
public class Task implements Entity<Task> {
    /**
     * Number of the task in Sprint. Starts with 1 and it's auto-incremented.
     */
    TaskID idTask;
    /**
     * Designation of the task.
     */
    TaskContainer taskContainer;

    TaskName name;
    /**
     * Description of the task.
     */
    TaskDescription description;
    /**
     * Start date, end date.
     */
    Time taskTime;
    /**
     * Hours spent (0 at the time it is created).
     */
    Hour hoursSpent;
    /**
     * Effort estimate (start by having the initial estimate, but can be
     * updated several times throughout the project; uses Fibonacci series
     * for duration in hours.).
     */
    EffortEstimate effortEstimate;
    /**
     * Percentage of execution (0% if it has not yet started, up to 100% when
     * it is completed; value calculated automatically based on the
     * relationship between the hours spent and the estimated effort).
     */
    double percentageOfExecution;
    /**
     * Precedence (optional, list of tasks that must be completed before the
     * start of the task).
     */
    Set<Task> precedenceList;
    /**
     * Type of task (Meeting, Documentation, Design, Implementation, Testing,
     * Deployment, etc.).
     */
    String typeOfTask;
    /**
     * Responsible (human resource responsible for the execution of the task).
     */
    Resource responsible;

    /**
     * List of logs that store the work done on a task.
     */
    List<Log> taskLogs = new ArrayList<>();
    /**
     * List of possible status in a task.
     */
    TaskStatus taskStatus;

    public Task(TaskID idTask, TaskContainer taskContainer, TaskName name, TaskDescription description, Time taskTime, EffortEstimate effortEstimate, Set<Task> precedenceList, String typeOfTask, Resource responsible) {
        this.idTask = idTask;
        this.taskContainer = taskContainer;
        this.name = name;
        this.description = description;
        this.taskTime = taskTime;
        this.hoursSpent = Hour.createHour(0);
        this.effortEstimate = effortEstimate;
        this.percentageOfExecution = 0;
        this.precedenceList = precedenceList;
        this.typeOfTask = typeOfTask;
        this.responsible = responsible;
        this.taskStatus = TaskStatus.PLANNED;
    }

    /**
     * Adds a new log entry to this taskLogs.
     *
     * @param timeSpent       the amount of time in hours spent in a given
     *                        work session.
     * @param workDescription a description of the work done in a given work
     *                        session.
     * @return true if it was added, false otherwise.
     */
    public boolean addTaskLog(final Hour timeSpent,
                              final TaskDescription workDescription) {
        final int logCode = generateLogCode();
        Log newLog = Log.createLog(logCode, timeSpent, workDescription);
        updateHoursSpent(timeSpent);
        return this.taskLogs.add(newLog);
    }

    private void updateHoursSpent(Hour timeSpent) {
        double currentHours = this.hoursSpent.getAmountOfHours();
        double newHours = timeSpent.getAmountOfHours();
        double newTotal = currentHours + newHours;
        this.hoursSpent = Hour.createHour(newTotal);
    }

    private int generateLogCode() {
        return this.taskLogs.size() + 1;
    }


    @Override
    public boolean sameIdentityAs(final Task other) {
        return other != null && this.idTask.equals(other.idTask);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        Task task = (Task) o;
        return sameIdentityAs(task);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTask);
    }
}

package switchfive.project.domain.request;

import switchfive.project.domain.ValueObject;

import java.util.Objects;

public class RequestID implements ValueObject<RequestID> {
    /**
     * Request Identification Number.
     */
    private final int identity;

    /**
     * We use private constructor so that any target class could not
     * instantiate this class directly by calling constructor.
     *
     * @param identityInput final int identityInput
     */
    private RequestID(final int identityInput) {
        this.identity = identityInput;
    }

    /**
     * Call constructor method to instantiate this class.
     *
     * @param identity final int identity
     * @return new instance.
     */
    public static RequestID createRequestID(final int identity) {
        return new RequestID(identity);
    }


    /**
     * Check if two objects have the same data, as the classes in Java are
     * inherited from the object classes only.
     *
     * @param other final Object other
     * @return true if two objects have the same data; otherwise, returns false.
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        RequestID that = (RequestID) other;
        return sameValueAs(that);
    }


    /**
     * Whenever hashcode is invoked on the same object more than once
     * during an execution of a Java application, the hashCode method must
     * consistently return the same integer, provided no information used in
     * equals comparisons on the object is modified.
     *
     * @return true if two objects have the same hashcode; otherwise, returns
     * false.
     */
    @Override
    public int hashCode() {
        return Objects.hash(identity);
    }

    @Override
    public boolean sameValueAs(RequestID other) {
        return other != null &&  this.identity == other.identity ;
    }
}

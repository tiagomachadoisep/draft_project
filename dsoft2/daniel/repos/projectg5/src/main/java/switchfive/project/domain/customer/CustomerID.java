package switchfive.project.domain.customer;

import switchfive.project.domain.ValueObject;

import java.util.Objects;

public class CustomerID implements ValueObject<CustomerID> {

    private int identity;

    private CustomerID(final int identity) {
        this.identity = identity;
    }

    public static CustomerID createIDCustomer (final int identity) {
        return new CustomerID(identity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identity);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CustomerID)) {
            return false;
        }
        CustomerID that = (CustomerID) object;
        return sameValueAs(that);
    }

    @Override
    public boolean sameValueAs(CustomerID other) {
        return other != null && identity == other.identity;
    }
}

package switchfive.project.domain.customer;

import switchfive.project.domain.ValueObject;

import java.util.Locale;
import java.util.Objects;

public class CustomerName implements ValueObject<CustomerName> {

    /**
     * Name inserted by user.
     */
    private String name;

    /**
     * Private constructor for customer name.
     * If customer name inserted has invalid is null or doesn't follow
     * the validation requisites then it throws an exception.
     * @param customerName final String name
     */
    private CustomerName(final String customerName) {
        if (validateCustomerName(customerName)) {
            this.name = customerName;
        } else {
            throw new IllegalArgumentException("The customer name inserted doesn't follow the requirements of 1 to 50 characters.");
        }
   }


    /**
     * Public method which creates a new customerName instance.
     * @param customerName - the customer name.
     * @return new instance of CustomerName
     */
    public static CustomerName createCustomerName(final String customerName) {
        return new CustomerName(customerName);
    }



    private boolean validateCustomerName(final String name) {
        return name != null && isNameBetween1and50characters(name) && !isNameOnlyBlankSpaces(name);
    }

    private boolean isNameOnlyBlankSpaces(String name) {
        return !(name.trim().length() > 0);
    }


    private boolean isNameBetween1and50characters (String name) {
        final int MINIMUM_SIZE = 1;
        final int MAXIMUM_SIZE = 50;
        final int NAME_LENGTH = name.length();

        return NAME_LENGTH >= MINIMUM_SIZE && NAME_LENGTH <= MAXIMUM_SIZE;
    }

    @Override
    public boolean sameValueAs(CustomerName other) {
        return other != null && name.toLowerCase(Locale.ROOT).trim().equals(other.name.toLowerCase(Locale.ROOT).trim());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof CustomerName)) return false;
        CustomerName customerName = (CustomerName) object;
        return sameValueAs(customerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name.toLowerCase(Locale.ROOT).trim());
    }
}


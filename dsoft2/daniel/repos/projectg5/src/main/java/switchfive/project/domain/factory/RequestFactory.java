package switchfive.project.domain.factory;

import switchfive.project.domain.profile.ProfileID;
import switchfive.project.domain.request.RequestID;
import switchfive.project.domain.user.UserID;
import switchfive.project.domain.request.Request;

public interface RequestFactory {
    Request createNewRequest(final RequestID requestIdentity,
                                    final UserID requestedUserID,
                                    final ProfileID selectedProfileID);
}

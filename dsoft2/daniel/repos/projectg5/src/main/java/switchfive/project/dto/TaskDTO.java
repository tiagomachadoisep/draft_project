package switchfive.project.dto;

public class TaskDTO {

    public final int taskNumber;
    public final String taskName;
    public final String taskDescription;
    public final String parentUserStoryCode;
    public final Integer effortEstimate;
    public final String taskType;
    public final String responsibleResource;
    public final String statusOfTask;

    public TaskDTO(int tasknumber, String taskName, String taskDescription
            , String parentUserStoryCode, Integer effortEstimate, String taskType
            , String responsibleResource, String statusOfTask) {
        this.taskNumber = tasknumber;
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.parentUserStoryCode = parentUserStoryCode;
        this.effortEstimate = effortEstimate;
        this.taskType = taskType;
        this.responsibleResource = responsibleResource;
        this.statusOfTask = statusOfTask;
    }
}

package switchfive.project.dto;

/**
 * @author DST
 */
public class SprintCreationDTO {
    /**
     * Sprint code.
     */
    public String projectCodeDto;
    /**
     * Sprint goal.
     */
    public String sprintGoalDto;
    /**
     * Sprint start date.
     */
    public String sprintStartDateDto;
    /**
     * Sprint end date.
     */
    public String sprintEndDateDto;

    /**
     * Constructor.
     *
     * @param codeInput      final String codeInput
     * @param goalInput      final String goalInput
     * @param startDateInput final String startDateInput
     * @param endDateInput   final String endDateInput
     */
    public SprintCreationDTO(final String codeInput, final String goalInput,
                             final String startDateInput,
                             final String endDateInput) {
        this.projectCodeDto = codeInput;
        this.sprintGoalDto = goalInput;
        this.sprintStartDateDto = startDateInput;
        this.sprintEndDateDto = endDateInput;
    }
}

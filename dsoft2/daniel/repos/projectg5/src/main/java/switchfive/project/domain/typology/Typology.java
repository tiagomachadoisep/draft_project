package switchfive.project.domain.typology;

import switchfive.project.domain.Entity;

import java.util.Objects;

/**
 * Typology class describes the data and the methods of its objects.
 */
public class Typology implements Entity<Typology> {
    /**
     * Typology identification number.
     */
    private final TypologyID identity;
    /**
     * Typology description.
     */
    private final TypologyDescription description;

    /**
     * @param identityInput    final ID_Typology identityInput
     * @param descriptionInput final String descriptionInput
     */
    public Typology(final TypologyID identityInput,
                    final TypologyDescription descriptionInput) {
        this.identity = identityInput;
        this.description = descriptionInput;
    }

    /**
     * Entities compare by identity, not by attributes.
     *
     * @param other The other entity.
     * @return true if the identities are the same, regardless of other
     * attributes.
     */
    @Override public boolean sameIdentityAs(final Typology other) {
        return other != null && identity.sameValueAs(other.identity);
    }

    /**
     * Check if two objects have the same data, as the classes in Java are
     * inherited from the object classes only.
     *
     * @param other final Object other
     * @return true if two objects have the same data; otherwise, returns false.
     */
    @Override public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Typology)) {
            return false;
        }
        Typology typology = (Typology) other;
        return sameIdentityAs(typology);
    }

    /**
     * Whenever hashcode is invoked on the same object more than once
     * during an execution of a Java application, the hashCode method must
     * consistently return the same integer, provided no information used in
     * equals comparisons on the object is modified.
     *
     * @return true if two objects have the same hashcode; otherwise, returns
     * false.
     */
    @Override public int hashCode() {
        return Objects.hash(identity);
    }
}

package switchfive.project.controllers.concreteControllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import switchfive.project.applicationServices.abstractServices.IProjectService;
import switchfive.project.domain.Time;
import switchfive.project.domain.customer.CustomerID;
import switchfive.project.domain.project.ProjectCode;
import switchfive.project.domain.project.ProjectDescription;
import switchfive.project.domain.project.ProjectName;
import switchfive.project.dto.ProjectCreationDTO;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AddProjectControllerTest {

    AddProjectController controller;
    IProjectService serviceMock;

    @BeforeEach
    void setUp() {
        serviceMock = mock(IProjectService.class);
        controller = new AddProjectController(serviceMock);
    }

    @Test
    void projectIsSuccessfullyAdded() throws ParseException {
        //Arrange
        ProjectCreationDTO dto =
                new ProjectCreationDTO("isep1", "Instituto Superior de " +
                        "Engenharia do Porto", "ISEP/DEI",
                        "Educational", "Free", 1, 1, 0, 1,
                        null, null);

        when(serviceMock.addProject(any(ProjectCode.class),
                any(ProjectName.class), any(ProjectDescription.class),
                any(CustomerID.class), any(Time.class))).thenReturn(true);

        //Act
        boolean result = controller.addProject(dto);

        //Assert
        assertTrue(result);
    }

    @Test
    void projectFails() throws ParseException {
        //Arrange
        ProjectCreationDTO dto =
                new ProjectCreationDTO("isep1", "Instituto Superior de " +
                        "Engenharia do Porto", "ISEP/DEI",
                        "Educational", "Free", 1, 1, 0, 1,
                        null, null);

        when(serviceMock.addProject(any(ProjectCode.class),
                any(ProjectName.class), any(ProjectDescription.class),
                any(CustomerID.class), any(Time.class))).thenReturn(false);

        //Act
        boolean result = controller.addProject(dto);

        //Assert
        assertFalse(false);
    }
}

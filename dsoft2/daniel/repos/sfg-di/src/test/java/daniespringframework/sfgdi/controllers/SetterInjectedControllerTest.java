package daniespringframework.sfgdi.controllers;

import daniespringframework.sfgdi.controllers.lessSuitableOptions.SetterInjectedController;
import daniespringframework.sfgdi.services.ConstructorInjectedGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SetterInjectedControllerTest {

    SetterInjectedController controller = new SetterInjectedController();

    @BeforeEach
    void setUp() {
        ConstructorInjectedGreetingService
                greetingService = new ConstructorInjectedGreetingService();

        controller.setGreetingService(greetingService);
    }

    @Test
    void getGreeting() {
        System.out.println(controller.getGreeting());
    }
}

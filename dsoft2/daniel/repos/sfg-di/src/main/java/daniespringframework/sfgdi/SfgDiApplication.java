package daniespringframework.sfgdi;

import daniespringframework.sfgdi.controllers.ConstructorInjectedController;
import daniespringframework.sfgdi.controllers.LanguageController;
import daniespringframework.sfgdi.controllers.lessSuitableOptions.MyController;
import daniespringframework.sfgdi.controllers.lessSuitableOptions.PropertyInjectedController;
import daniespringframework.sfgdi.controllers.lessSuitableOptions.SetterInjectedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SfgDiApplication {

    public static void main(String[] args) {

        /*
         * Spring ApplicationContext interface
         */
        ApplicationContext context =
                SpringApplication.run(SfgDiApplication.class, args);

        /*
         * Inversion of control (IoC)
         */
        MyController myController = (MyController) context.getBean(
                "myController");


        String greeting = myController.sayHello();

        System.out.println(greeting);


        System.out.println("---------- Constructor injected controller");

        ConstructorInjectedController constructorInjectedController =
                (ConstructorInjectedController) context.getBean(
                        "constructorInjectedController");

        System.out.println(constructorInjectedController.getGreeting());


        System.out.println("---------- Setter injected controller");

        SetterInjectedController setterInjectedController =
                (SetterInjectedController) context.getBean(
                        "setterInjectedController");

        System.out.println(setterInjectedController.getGreeting());


        System.out.println("---------- Property injected controller");

        PropertyInjectedController propertyInjectedController =
                (PropertyInjectedController) context.getBean(
                        "propertyInjectedController");

        System.out.println(propertyInjectedController.getGreeting());


        System.out.println("---------- Language controller");

        LanguageController languageController =
                (LanguageController) context.getBean("languageController");

        System.out.println(languageController.getGreeting());

    }
}

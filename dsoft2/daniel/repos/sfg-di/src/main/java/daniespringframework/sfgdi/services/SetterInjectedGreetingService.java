package daniespringframework.sfgdi.services;

import org.springframework.stereotype.Service;

@Service ("setterInjectedGreetingService")
public class SetterInjectedGreetingService implements GreetingService {

    public String sayGreeting() {
        return "Hello world, by constructor!!";
    }
}

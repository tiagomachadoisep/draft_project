package daniespringframework.sfgdi.services;

import org.springframework.stereotype.Service;

@Service ("propertyInjectedGreetingService")
public class PropertyInjectedGreetingService implements GreetingService{

    public String sayGreeting() {
        return "Hello world, by property!!";
    }
}

package daniespringframework.sfgdi.services;

import org.springframework.stereotype.Service;

@Service("constructorInjectedGreetingService")
public class ConstructorInjectedGreetingService implements GreetingService {

    public String sayGreeting() {
        return "Hello world, by constructor!";
    }
}

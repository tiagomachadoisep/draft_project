package daniespringframework.sfgdi.services.languages;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"pt","default"})
@Service("i18nService")
public class PtIn18Service implements I18nService {
    public String sayHello() {
        return "Olá!";
    }
}

package daniespringframework.sfgdi.services.languages;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("en")
@Service("i18nService")
public class EnIn18Service implements I18nService{
    public String sayHello() {
        return "Hello!";
    }
}

package daniespringframework.sfgdi.controllers.lessSuitableOptions;

import org.springframework.stereotype.Controller;

/**
 * Controller is used in Spring MVC to define controller, which are first
 * Spring bean and then controller.
 */
@Controller
public class MyController {
    /**
     * @return "Hey!"
     */
    public String sayHello() {
        System.out.println("Hello world!");
        return "Hey!";
    }
}

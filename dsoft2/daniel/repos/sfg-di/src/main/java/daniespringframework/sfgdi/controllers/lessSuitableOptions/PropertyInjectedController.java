package daniespringframework.sfgdi.controllers.lessSuitableOptions;

import daniespringframework.sfgdi.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller("propertyInjectedController")
public class PropertyInjectedController {
    /**
     * Property must be public, to get access from outside.
     */
    @Qualifier("propertyInjectedGreetingService")
    @Autowired
    public GreetingService greetingService;

    public PropertyInjectedController() {
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }

}

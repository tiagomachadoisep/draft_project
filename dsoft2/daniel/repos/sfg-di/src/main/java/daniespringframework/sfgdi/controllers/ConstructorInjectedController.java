package daniespringframework.sfgdi.controllers;

import daniespringframework.sfgdi.services.GreetingService;
import org.springframework.stereotype.Controller;

/**
 * Much better alternative!
 */
@Controller ("constructorInjectedController")
public class ConstructorInjectedController {
    private final GreetingService greetingService;

    public ConstructorInjectedController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}

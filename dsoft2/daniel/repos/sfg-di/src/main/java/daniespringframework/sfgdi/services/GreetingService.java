package daniespringframework.sfgdi.services;

import org.springframework.beans.factory.annotation.Autowired;

public interface GreetingService {

    String sayGreeting();

}

package daniespringframework.sfgdi.controllers.lessSuitableOptions;

import daniespringframework.sfgdi.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller ("setterInjectedController")
public class SetterInjectedController {
    private GreetingService greetingService;

    public SetterInjectedController() {
    }

    /**
     * Private property, but public setter (which is almost the same).
     *
     * @param greetingService
     */
    @Qualifier("setterInjectedGreetingService")
    @Autowired
    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting() {
        return greetingService.sayGreeting();
    }
}

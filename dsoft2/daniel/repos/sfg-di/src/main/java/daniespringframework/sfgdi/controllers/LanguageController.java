package daniespringframework.sfgdi.controllers;

import daniespringframework.sfgdi.services.languages.I18nService;
import org.springframework.stereotype.Controller;

@Controller ("languageController")
public class LanguageController {
    private final I18nService service;

    public LanguageController(
            I18nService service) {
        this.service = service;
    }

    public String getGreeting() {
        return service.sayHello();
    }
}

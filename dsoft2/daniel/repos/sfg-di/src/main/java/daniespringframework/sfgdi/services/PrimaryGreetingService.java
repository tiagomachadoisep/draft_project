package daniespringframework.sfgdi.services;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service("primaryGreetingService")
public class PrimaryGreetingService implements GreetingService {
    public String sayGreeting() {
        return "Hello world, by PRIMARY bean!!";
    }
}

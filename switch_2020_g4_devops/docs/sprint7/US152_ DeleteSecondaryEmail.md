```plantuml
title \n\nUS152 Delete A Secondary Email From My Profile\n\n\n

participant ":IDeleteSecondaryEmailController" as C
participant "assemblerToDTO\n:AssemblerToDTO" as M
participant "emailDTO :EmailDTO" as ED
participant "outputDTO :EmailDTO" as OD
participant ":IPersonService" as PS
participant "ownerEmail\n:Email" as OE
participant "emailToRemove\n:Email" as E
participant ":PersonID" as ID

participant ":IPersonRepository" as PR
participant ":IPersonAssemblerJPA" as AJPA
participant "person\n:Person" as P
participant "PersonJPA" as PJPA

participant "EmailJPA" as EJPA
participant ":IPersonRepositoryJPA" as PRJPA

autonumber
[o-> C : deleteSecondaryEmailFromProfile(mainEmail, emailToRemove)
activate C
C -> M : toEmailDTO(mainEmail, emailToRemove)
M -->ED *: create
C -> PS : deleteSecondaryEmail(emailDTO)
activate PS
PS --> OE * : create(emailDTO.getMainEmail())
PS --> E * : create(emailDTO.getEmail())

PS -> ID *: create(ownerID)
PS -> PR : findByID(personID)
activate PR
PR -> PRJPA : findByID(personID)
activate PRJPA
PRJPA --> PR : optionalPersonJPA
deactivate PRJPA
PR -> PR : personJPAOptional.get()
deactivate AJPA
PR --> PS : optionalPerson
deactivate PR

PS --> PS : person.get()
PS -> PS : person.getOtherEmails()\n.remove(emailToRemove)

PS -> PR : deleteByPersonID(personID)
activate PR

PR -> PRJPA: deleteByPersonID(personID)
activate PRJPA
deactivate PRJPA
PS -> PR : saveNew(person)

PR -> PRJPA: toJPA(person)
deactivate PR
activate PRJPA
PRJPA -> PRJPA : save(personJPA)
deactivate PRJPA

PS --> C : emailList
C -> M : toEmailDTO(mainEmail, emailToRemove)
M --> OD *: create
C -> PS : deleteSecondaryEmail(emailDTO)
C -> C : outputDTOO.setEmailsList(emailList)
[<--o C : result
deactivate PS
deactivate C

```
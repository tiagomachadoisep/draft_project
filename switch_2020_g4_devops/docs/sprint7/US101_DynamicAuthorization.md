```plantuml
@startuml
autonumber
title US101 : add family members

participant " :IAddFamilyMemberController" as ctrl

participant " :IAuthenticationService" as auth
participant " :ISecurityContextProvider" as provider
participant " :IPersonService" as PS



[o-> ctrl : addMember\n(email, familyId, ..., token)
activate ctrl
ctrl -> auth : isCurrentUserAllowedInFamily(familyId)
activate auth
auth -> provider : currentUserEmail = getUsername()
auth -> PS : check = isUserInFamily(currentUserEmail, familyId)
auth --> ctrl : check
deactivate auth
opt check==false
[<--o ctrl : 403 Unauthorized
end
ctrl -> PS : addMember(data)

[<--o ctrl : 201 Created
deactivate ctrl


@enduml
```
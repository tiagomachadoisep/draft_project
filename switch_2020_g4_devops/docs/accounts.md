````plantuml
@startuml

interface Account{
boolean isDescription(String description)
double getBalance()
void addFunds()
void removeFunds()
boolean isCash()
List<Transaction> getListOfTransactionsBetweenDates\n(Date dateOne, Date dateTwo)
}

abstract class FinancialEntityAccount{
description : String
balance : double
entity : FinancialEntity
boolean isEntityName(String name)
}

class FinancialEntity{
name : String
FinancialEntity(String name)
boolean isName(String name)
}

class CashAccount{
description : String
balance : double
CashAccount(double balance)
}

class BankAccount{
BankAccount(int id, String bankName,\ndouble balance)
}

class CreditCardAccount{
interest : double

CreditCardAccount(int id, String entityName,\n double balance, double interest)
void addFractionedPayment(double value, int fractions)
}

class BankSavingsAccount{
interes : double 
BankSavingsAccount(int id, \nString entityName, double balance, double interest)
}

interface Transaction{

boolean isBetweenDates(Date dateOne, Date dateTwo)
String getDescription()
double getValue()
boolean isTransfer()
boolean isMovement()
boolean isOfCategory(Category cat)
}

abstract CashTransaction {
value : double
transactionDate : Date
registrationDate : Date
description : String
}

abstract FinancialEntityAccountTransaction {
value : double
transactionDate : Date
registrationDate : Date
description : String
}


class CashTransfer{
CashTransaction(double value, Date date, String description)
boolean isFromAccount(Account account)
boolean isToAccount(Account account)
}

class CashMovement{
CashMovement(double value,Date date,String description)
boolean isOnAccount(Account account)
boolean isPayment()
boolean isDeposit()
}

class FinancialEntityAccountTransfer{
FinancialEntityAccountTransaction(double value, Date date, String description)
boolean isFromAccount(Account account)
boolean isToAccount(Account account)
}

class FinancialEntityAccountMovement{
FinancialEntityAccountMovement(double value,Date date,String description)
boolean isOnAccount(Account account)
boolean isPayment()
boolean isDeposit()
}


FinancialEntityAccount ..|> Account

BankSavingsAccount --|> FinancialEntityAccount
CreditCardAccount --|> FinancialEntityAccount

CashAccount ..|> Account

BankAccount --|> FinancialEntityAccount

CashTransfer -- Account : from
CashTransfer -- Account : to

CashMovement -- Account : on

FinancialEntity - FinancialEntityAccount

CashTransfer -up-|> CashTransaction
CashMovement -up-|> CashTransaction

FinancialEntityAccountTransfer -right-|> FinancialEntityAccountTransaction
FinancialEntityAccountMovement -left-|> FinancialEntityAccountTransaction

CashTransaction .|> Transaction
FinancialEntityAccountTransaction ..|> Transaction

@enduml
````
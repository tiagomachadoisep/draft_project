```plantuml
@startuml

title Implementation View

package "Family Finance Management System" as app {
package "Controllers" as ctrls {}

package "Domain" as Dom {
package "Services" as S{}
package "Model" as M{}
}
package "Utilities" as U{}
}
note bottom of ctrls : Correlates with Controllers \n component of LogicView FFM Business Logic
note bottom of S : Correlates with Services \n component of LogicView FFM Business Logic
note bottom of M : Correlates with Model \n component of LogicView FFM Business Logic


ctrls  .right> Dom
U .> ctrls

M .> S
@enduml
```
```plantuml
@startuml
title Implementation View - Zoom on Domain

package "Domain" as Dom {
package "Model" as M{
package "ValueObjects" as VO{}
package "DTO"{}
package "Interfaces" as I{}
}
package "Services" as S{
}
}
M .> S
I .> DTO
DTO .> VO
@enduml
```
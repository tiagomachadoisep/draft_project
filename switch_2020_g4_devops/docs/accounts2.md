````plantuml
@startuml

interface Account{
+boolean isDescription\n(String description)
+MoneyValue getBalance()
+boolean isCash()
+List<TransactionDTO> getListOf\nTransactionsBetweenDates\n(Date dateOne, Date dateTwo)
+AccountDTO toDTO()
}

abstract class FinancialEntityAccount{
-description : String
-balance : MoneyValue
+boolean isEntityName(String name)
}

class FinancialEntity{
-name : String
+FinancialEntity(String name)
+boolean isName(String name)
}

class CashAccount{
-description : String
-balance : double
+CashAccount(double balance)
+boolean registerPayment\n(transactionDTO)
+boolean registerDeposit\n(transactionDTO)
+boolean transferMoney\n(transactionDTO)
}

class BankAccount{
+BankAccount(int id,\nFinancialEntity entity,\ndouble balance)
}

class CreditCardAccount{
+CreditCardAccount(int id,\nFinancialEntity entity,\ndouble balance)
}

class BankSavingsAccount{
+BankSavingsAccount(int id,\nFinancialEntity entity,\ndouble balance)
}

interface Transaction{

+boolean isBetweenDates\n(Date dateOne, Date dateTwo)
+String getDescription()
+double getValue()
+boolean isCash()
+boolean isTransfer()
+boolean isMovement()
+boolean isOfCategory(Category cat)
+TransactionDTO toDTO()
}

abstract CashTransaction {
-value : double
-transactionDate : Date
-registrationDate : Date
-description : String
-category : Category
}

class CashTransfer{
+CashTransfer(Account from,\nAccount to,double value,\nDate date, String description)
+boolean isFromAccount\n(Account account)
+boolean isToAccount\n(Account account)
}

class CashMovement{
+CashMovement(Account account,\ndouble value,Date date,\nString description)
+boolean isOnAccount\n(Account account)
+boolean isPayment()
+boolean isDeposit()
}


FinancialEntityAccount ..|> Account

BankSavingsAccount --|> FinancialEntityAccount
CreditCardAccount --|> FinancialEntityAccount

CashAccount .|> Account

BankAccount --|> FinancialEntityAccount

CashTransfer -- CashAccount : from
CashTransfer -- CashAccount : to

CashMovement -- CashAccount : on

FinancialEntityAccount -r- FinancialEntity

CashTransfer -up-|> CashTransaction
CashMovement -up-|> CashTransaction

CashTransaction .|> Transaction

@enduml
````

````plantuml
class "AccountTypeService" as ats{
+getAccountTypeByName(String name)
+boolean createNewAccountType(String name)
}

class "AccountType" as at{
name : String
+Account createAccount(String description, double balance)
}

interface "Account" as a{
}

class "CashAccount" as ca{
}

abstract "FinancialEntityAccount" as fa{
}

class "BankAccount" as ba{
}

class "CreditCardAccount" as cca{

}

class "BankSavingsAccount" as bsa{
}

class "AccountList" as al{
+boolean addAccount(Account acc)
}

class "FFMApplication" as app{
+FamilyService getFamilyService()
}

class "FamilyService" as fserv{
+addCashAccountToPerson\n(EmailAddress mainEmail,\nString description,\ndouble balance)
}

class "Family" as fam{
+boolean addAccountToPerson(mainEmail, Account acc)
}

class "Person" as per{
+boolean addAccount(Account acc)
}


fserv"1" - "1"ats : accTypeServ
ats "1" - "*" at : listOfAccTypes
al "1" --- "*" a : listOfAccounts
bsa --|> fa
ba --|> fa
cca --|> fa
fa ..|> a
ca ..|> a

fa --- at : type
ca --- at : type

app"1" - "1" fserv : familyServ
fam"1" -- "*"fserv : listOfFamilies
fam "1" - "*" per : members
per "1" -r- "1" al
fam "1" -- "1"ca : familyCashAccount


````

````plantuml
autonumber
title create cash account

participant ":AddAccount\nController" as ctrl
participant ":FFM\nApplication" as app
participant "famServ:\nFamilyService" as fserv
participant "accTypeServ:\nAccountTypeService" as aserv
participant "accType:\nAccountType" as acctype
participant "cashAcc:Account" as acc
participant "fam:\nFamily" as fam
participant "members:\nPersonList" as members
participant "member:\nPerson" as member
participant "accList:\nAccountList" as acclist
participant "listOfAccounts:\nList<Account>" as list

[o-> ctrl : addCashAccount\n(mainEmail,description,balance) 
activate ctrl
ctrl -> app : getFamilyService()
activate app
app-->ctrl : famServ
deactivate app
ctrl -> fserv : addCashAccountToPerson\n(mainEmail,description,balance)
activate fserv
fserv -> aserv : createCashAccount(description,balance) 
activate aserv
aserv -> aserv : accType=getAccountType\nByName(cash)
aserv -> acctype : createAccount(description,balance)
activate acctype
acctype --> acc* : create(description,balance)
acctype --> aserv : cashAcc
deactivate acctype
aserv ->fserv : cashAcc
deactivate aserv
fserv -> fserv : fam=getFamilyBy\nMemberMainEmail(mainEmail)
fserv -> fam : addAccountToPerson(mainEmail,cashAcc)
activate fam
fam -> members : addAccountToPerson(mainEmail,cashAcc)
activate members
members -> members : member=getPersonBy\nMainEmail(mainEmail)
members -> member : addAccount(cashAcc)
activate member
member-> acclist : addAccount(cashAcc)
activate acclist
acclist -> acclist : hasCashAcc()
acclist -> acclist : hasAnyAccWithSame\nDescription(cashAcc)
acclist -> list : add(cashAcc)
acclist --> member: true
deactivate acclist
member --> members : true 
deactivate member
members --> fam : true
deactivate members
fam --> fserv : true
deactivate fam
fserv -> ctrl : true 
deactivate fserv
[<-- ctrl : true
deactivate ctrl

````

AccountTypeService knows all AccountTypes, ask him for the correct one: IE

AccountType instance has information to create an Account of that type : Creator

An Account knows its own description, so we should ask it if it is the same: IE

AccountList aggregates all accounts, it can ask them if they have the description: IE




método createAccount da class AccountType (com atributo name):
````
public Account createAccount(String description, double balance){
    if(name=="cash")
        return new CashAccount(description,balance);
    if(name=="bank savings")
        return new BankSavingsAccount(description,balance);
    return null;
}

public Account createAccount(String description){
    if(name=="credit card")
        return new CreditCardAccount(description);
    return null;
}
````

````plantuml
autonumber
title create cash account

participant ":AddAccount\nController" as ctrl
participant ":FFM\nApplication" as app
participant "accServ:\nAccountService" as fserv
participant "accTypeServ:\nAccountTypeService" as aserv
participant "accType:\nAccountType" as acctype
participant "cashAcc:Account" as acc
participant "listOfAccounts:\nList<Account>" as list

[o-> ctrl : addCashAccount\n(mainEmail,description,balance) 
activate ctrl
ctrl -> app : getAccountService()
activate app
app-->ctrl : accServ
deactivate app
ctrl -> fserv : addCashAccount\n(mainEmail,description,balance)
activate fserv
fserv -> aserv : createCashAccount(mainEmail,description,balance) 
activate aserv
aserv -> aserv : accType=getAccountType\nByName(cash)
aserv -> acctype : createAccount(mainEmail,description,balance)
activate acctype
acctype --> acc* : create(description,balance)
acctype --> aserv : cashAcc
deactivate acctype
aserv ->fserv : cashAcc
deactivate aserv

fserv -> list : add(cashAcc)

fserv -> ctrl : true 
deactivate fserv
[<-- ctrl : true
deactivate ctrl

````

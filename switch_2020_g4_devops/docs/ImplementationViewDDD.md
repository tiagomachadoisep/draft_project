```plantuml
@startuml

title Implementation View

package "Family Finance Management System" as app {
package "Controllers" as ctrls {}
package "Repositories" as rep {}
package "Model" as M {}
}

ctrls ..> rep
rep ..> M




@enduml
```
```plantuml
@startuml
title Implementation View - Zoom on Model

package "Model" as M {
package person as p{}
package family as f{}
package invoice as i{}
package ledger as l{}
package contract as c{}
package account as a{}
package category as cat{}
package shared as s{}
package futureTransaction as ft{}

p .>f
l .> i
l ..> a
i ..> c
cat ...> f
a ...> p
a ...> f
i .> cat
ft ...> p
l ...>p
c...>p
ft.>c

}


@enduml
```
````plantuml
@startuml

interface Account{
+boolean isDescription\n(String description)
+MoneyValue getBalance()
+boolean isCash()
+List<TransactionDTO> getListOf\nTransactionsBetweenDates\n(Date dateOne, Date dateTwo)
+AccountDTO toDTO()
}

interface Transaction{

+boolean isBetweenDates\n(Date dateOne, Date dateTwo)
+String getDescription()
+double getValue()
+boolean isCash()
+boolean isTransfer()
+boolean isMovement()
+boolean isOfCategory(Category cat)
+TransactionDTO toDTO()
}

abstract CashTransaction {
-value : double
-transactionDate : Date
-registrationDate : Date
-description : String
-category : Category
}

class CashTransfer{
+CashTransfer(Account from,\nAccount to,double value,\nDate date, String description)
+boolean isFromAccount\n(Account account)
+boolean isToAccount\n(Account account)
}

class CashMovement{
+CashMovement(Account account,\ndouble value,Date date,\nString description)
+boolean isOnAccount\n(Account account)
+boolean isPayment()
+boolean isDeposit()
}

class AccountService{
-getAccountByMainEmailAndDescription\n(String mainEmail, String description)

}

AccountService"1" - "*"Account : listOfAccounts
AccountService"1" -- "*"Transaction : listOfTransactions


CashTransfer -- Account : from
CashTransfer -- Account : to

CashMovement -- Account : on

CashTransfer -|> CashTransaction
CashMovement -|> CashTransaction

CashTransaction .|> Transaction

@enduml
````

````plantuml
autonumber
title create transaction

participant ":CreateTransaction\nController" as ctrl
participant ":FFM\nApplication" as app
participant "famServ:\nFamilyService" as fserv
participant "accTypeServ:\nAccountTypeService" as aserv
participant "accType:\nAccountType" as acctype
participant "cashAcc:Account" as acc
participant "fam:\nFamily" as fam
participant "members:\nPersonList" as members
participant "member:\nPerson" as member
participant "accList:\nAccountList" as acclist
participant "listOfAccounts:\nList<Account>" as list

[o-> ctrl : addTransfer(mainEmailFrom,mainEmailTo,\ndescription,balance,category) 
activate ctrl
ctrl -> app : getAccountService()
activate app
app-->ctrl : famServ
deactivate app
ctrl -> fserv : addCashAccountToPerson\n(mainEmail,description,balance)
activate fserv
fserv -> aserv : createCashAccount(description,balance) 
activate aserv
aserv -> aserv : accType=getAccountType\nByName(cash)
aserv -> acctype : createAccount(description,balance)
activate acctype
acctype -> acc* : create(description,balance)
acctype --> aserv : cashAcc
deactivate acctype
aserv ->fserv : cashAcc
deactivate aserv
fserv -> fserv : fam=getFamilyBy\nMemberMainEmail(mainEmail)
fserv -> fam : addAccountToPerson(mainEmail,cashAcc)
activate fam
fam -> members : addAccountToPerson(mainEmail,cashAcc)
activate members
members -> members : member=getPersonBy\nMainEmail(mainEmail)
members -> member : addAccount(cashAcc)
activate member
member-> acclist : addAccount(cashAcc)
activate acclist
acclist -> acclist : hasCashAcc()
acclist -> acclist : hasAnyAccWithSame\nDescription(cashAcc)
acclist -> list : add(cashAcc)
acclist --> member: true
deactivate acclist
member --> members : true 
deactivate member
members --> fam : true
deactivate members
fam --> fserv : true
deactivate fam
fserv -> ctrl : true 
deactivate fserv
[<-- ctrl : true
deactivate ctrl

````


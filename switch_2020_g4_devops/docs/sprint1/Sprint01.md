# Family Finance Management Project SWitCH

# 1. Developing Team

The students identified on table below represent the developing team of this project.

| Student Nr.    | Name               | 
|----------------|--------------------|
| **[1201763]**  | Filipa Santos      | 
| **[1201764]**  | Gonçalo Reis       | 
| **[1201769]**  | Luís Rodrigues     | 
| **[1201921]**  | Luís Silva         |
| **[1201773]**  | Marta Inácio       |
| **[1201774]**  | Miguel Oliveira    |
| **[1201776]**  | Pedro Santos       |
| **[1201782]**  | Thayane Marcelino  |

# 2. Functionalities Distribution ###

The distribution of  the requirements throughout the project's development by the members of developing team was 
carried out as described in the following table.

| Student Nr.   | Sprint 1                         | 
|---------------|----------------------------------|
| **[1201763]** |[US110], [US010], [US011]         |
| **[1201764]** |[US150], [US010], [US011]         |
| **[1201769]** |[US120], [US010], [US011]         |
| **[1201921]** |[US002], [US010], [US011]         |
| **[1201773]** |[US001], [US010], [US011]         |
| **[1201774]** |[US105], [US101], [US010], [US011]|
| **[1201776]** |[US104], [US010], [US011]         |
| **[1201782]** |[US151], [US010], [US011]         |


[1201763]: ../students/1201763.md
[1201764]: ../students/1201764.md
[1201769]: ../students/1201769.md
[1201921]: ../students/1201921.md
[1201773]: ../students/1201773.md
[1201774]: ../students/1201774.md
[1201776]: ../students/1201776.md
[1201782]: ../students/1201782.md
[US001]: user_stories/US001_AddStandardCategory.md
[US002]: user_stories/US002_GetStandardCategoriesTree.md
[US010]: user_stories/US010_CreateFamily.md
[US011]: user_stories/US011_AddFamilyAdministrator.md
[US101]: user_stories/US101_AddFamilyMember.md
[US104]: user_stories/US104_ListOfMembersRelations.md
[US105]: user_stories/US105_CreateNewRelation.md

[US110]: user_stories/US110_GetListOfCategoriesOnFamilyCategoryTree.md

[US120]: user_stories/US120_CreateCashAccount.md
[US150]: user_stories/US150_GetProfileInfo.md
[US151]: user_stories/US151_AddEmailAccountToProfile.md
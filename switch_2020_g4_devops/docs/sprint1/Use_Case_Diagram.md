# Use Case Diagram - First Sprint

To better show the interaction between the requirements and the actors, next you can see the use case diagram: 


```plantuml
@startuml
title Use Case Diagram - First Sprint

left to right direction

actor "SystemManager" as SM
actor "FamilyAdministrator" as FA
actor "FamilyMember" as FM

rectangle FFMApp{
SM --> (US001 - create a standard category) 
SM --> (US002 - get the standard categories tree)
SM --> (US010 - create a family)
SM --> (US011 - add a family administrator)

FA --> (US101 - add family members)
FA --> (US104 - get the list of family members and their relations)
FA --> (US105 - create a relation between two family members)
FA --> (US110 - get the list of the categories on the family’s category tree)
FA --> (US120 - create a family cash account)

FM --> (US150 - get my profile’s information)
FM --> (US151 - add an email account to my profile)
}
@enduml
# US010
=======================================

# 1. Requirements

*As a System Manager, I want to create a family.*

In this US, the goal is to create a new family that will use the FFM (Family Finance Management) AppMembers.

## 1.1 Client Notes

The following note were acquired through information provided by the PO:

- The family will have a name and will be registered at a given date.

It is possible that in specific cases the date is not the system date at the time of registry, for example there could
be an order to register the family at a moment when the SM is not available. In this case, he/she will register it later
with the registration date of the request.

## 1.2 System Sequence Diagram

Next we present the System Sequence Diagram.

```plantuml
autonumber
hide footbox
title SSD Create a family
actor "System Manager" as SM
activate SM

Participant System
activate System
SM -> System : create new Family
System --> SM : ask data (regDate, name)
deactivate System
SM -> System : inputs required data
activate System
System --> SM : inform success
deactivate System
deactivate SM
```

## 1.3 Process View

```plantuml
@startuml
autonumber
actor "System" as SM
participant ":FFM_UI" as UI
participant ":FFM_BusinessLogic" as BL

activate SM
SM -> UI : asks to create new family
activate UI
UI -> SM : asks for data
deactivate UI
SM -> UI : informs data
activate UI
UI -> BL : add data 
activate BL
BL -> UI : result
deactivate BL
UI -> SM : shows result
deactivate UI
deactivate SM
@enduml
```

## 1.4 Connection with other User Stories

This US doesn't depend on any other US. All the US from this sprint depend on this, except
for [US001] and [US002],
because a family is the main entity of the FFM Application. In different US other aspects are included for each family,
such as family members and their relations.

# 2. Analysis

The SM will have to create a family in the system, therefore he/she will need to ask the family some input data that is
required to create a new family. According to the Product Owner, each family has a name, and a registration date. After
providing this data, the family should be added to the application and should be informed about its success.

In order to distinguish between families, a unique identifier was created, an integer number called familyId. This
number is attributed to the family by the system at the time of its creation through a random number generator, after
verification that the number is indeed unique within the list of registered families.

## 2.1 Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **Name**                    | Mandatory, text, not empty or null, maximum of 20 characters.      |
| **Registration date**       | Mandatory, date, not after current date, not empty or null.        |
| **Family ID**               | Mandatory, unique, integer with 5 random digits                    |            

## 2.2 Relevant excerpt of the Domain Model

Next we present the relevant excerpt of the Domain Model for this US.

```plantuml 
@startuml Domain Model
object "Family" as fam {
name
id
}
object Date {
year
day
month
}
fam "1" - "1" Date : registered >

@enduml
```

# 3. Design

## 3.1. Functionality

The creation of a family in our AppMembers starts with the SM asking the UI to create a family, for which the UI needs some
data in case:

- registration date
- name

If the registration date is not provided, the system uses its current date.

After providing the requested data, the UI asks the createFamilyController to create a family, which, in its turn, will
ask the application (FFMApplication) to do it. The FFMApplication is then responsible for creating a new object family of the Family
class. The Family class validates the inputs name and registration date, and if they're valid the family is created.
Then, the application adds this family to the list of families using the method addFamily(family). A message will be
delivered to the SM by the UI informing of the success of this process.

````plantuml
@startuml
autonumber
title **US010** - Add Family
actor "SystemManager" as SM
participant ":UI" as UI
participant ":AddFamilyController" as AFC
participant ":FFMApplication" as AppMembers
participant "familyService:FamilyService" as service
participant "aFamily:Family" as fF 

activate SM
activate UI
SM -> UI : create new Family
UI --> SM : ask data (regDate, name)
deactivate UI

SM -> UI : inputs required data
activate UI
UI -> AFC : createFamily (regDate, name)
activate AFC
AFC -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> AFC : familyService
deactivate AppMembers
AFC -> service : addFamily(name, regDate) 

activate service
service -> service : generateID()
service -> fF ** : create (id, name, regDate)
activate fF
fF -> fF : validateFamilyName(name)
fF -> fF : validateFamilyRegistrationDate(regDate)
deactivate fF

service -> service : addFamily(aFamily)

service --> AFC : ok
deactivate service

deactivate AppMembers
AFC --> UI : ok
deactivate AFC
UI --> SM : inform success
deactivate UI
deactivate SM
@enduml
````

## 3.2. Class Diagram

```plantuml

class "AddFamilyController" as AFC {
- app : FFMApp
+ boolean createFamily(regDate,name)
}

class "FFMApp" as AppMembers {
-familyService : FamilyService
-categoryService : CategoryService
+FamilyService getFamilyService()
+CategoryService getCategoryService()
}

class "Family" as F {
- name : String
- regDate : Date
- familyId : int
- categoryList : CategoryService
+ Family(name,familyId,regDate,categoryService)
- validateFamilyName(name)
- validateFamilyDate(regDate)
}

class "FamilyService" as serv{
listOfFamilies : List<Family>
-generateID()
+addFamily(String name,Date regDate,categoryService)
}


AFC - AppMembers : asks >
serv -- F : add family >
AppMembers -- serv : has >
@enduml
```

## 3.3. Applied Patterns

The following patterns were applied in this US:

**Controller**: Concerning to CreateFamilyController, this is responsible to receive and coordinate the system
operations necessary to fulfill this process.

**Information Expert**: The FFMApplication knows who have the information about the families, and the Family Service Class has
the responsibility of storing that information.

**Creator**: The FamilyService class is responsible to create a new family.

**Pure Fabrication** : The FFMApplication, FamilyService and CategoryService classes are not part of the concepts of the problem, but are created
to coordinate the program.

**Low Coupling and High Cohesion**:

## 3.4. Tests

**Test 1** Check if the family's constructor that has a name, id and registration date works with valid arguments

**Test 2** Check if the family's constructor that has a name, id and registration date doesn't work with an invalid name

**Test 3** Check if the family's constructor that has a name, id and registration date doesn't work with a null name

**Test 4** Check if the family's constructor that has a name, id and registration date doesn't work with an empty name

**Test 5** Check if the family's constructor that has a name, id and registration date works with an invalid date

**Test 6** Check if the family's constructor that has a name and id works with valid arguments

**Test 7** Check if the family's constructor that has a name and id doesn't work with an invalid name

**Test 8** Check if the family's constructor that has a name and id doesn't work with a null name

**Test 9** Check if the family's constructor that has a name and id doesn't work with an empty name

**Test 10** Check if the family's constructor that has a name, id, registration date and category service works with
valid arguments

**Test 11** Check if the family's constructor that has a name, id, registration date and category service doesn't work
with an invalid category service

# 4. Implementation

With the data given by the UI, the AddFamilyController asks the AppMembers for the class responsible for storing the families:

    public Family addFamily(String name, Date regDate) {
    FamilyService familyService = this.app.getFamilyService();
    CategoryService categoryService = this.app.getCategoryService();
        return familyService.addFamily(name, regDate, categoryService);
    }

Since it's the FamilyService class responsible for storing the families, we implemented a method to addFamilies in the
List o Families, this method generate a unique ID, and only add the family if it's already in the list.

    public Family addFamily(String name, Date regDate, CategoryService categoryService) {
    int id = this.generateUniqueId();
    Family family = new Family(name, regDate, id, categoryService);
    addFamilyIfNotExists(family);
        return family;
    }

Also, when we instantiate a Family, we validate the family name and registration date.

To validate a name:

    private void validateName(String name) {
    if (!checkNameNotNullOrEmpty(name) || !validateNameFormat(name))
        throw new IllegalArgumentException("Name not valid.");
    }

To validate date:

    private boolean validateDate(Date registrationDate) {
        return !registrationDate.after(new Date());
    }

# 5. Integration/Demonstration

Since the Family is one of the keys elements of our app, creating one family is crucial. In the present sprint all the
user stories, at some point, instantiate an object of the class Family.

# 6. Observations

In different US, other attributes of the class Family will be added such as the family administrator, the list of
members, among others.

In the case of the family id, a method ( generateID() ) will be used to automatically assign an id to the family. As
said before, this is an integer unique attribute, composed by 5 random digits.


[US001]: US001_AddStandardCategory.md
[US002]: US002_GetStandardCategoriesTree.md
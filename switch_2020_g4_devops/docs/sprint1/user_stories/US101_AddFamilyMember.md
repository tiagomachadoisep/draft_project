# US101
=======================================


# 1. Requirements

User Story: as a family administrator, I want to add family members.

In this US, we will model the way the family administrator can add
family members to his family.

According to the information provided by the PO, a person 
is defined by an ID, and can have a number of other different 
attributes, which are: a name, a VAT number, a birthdate, an address, one
or more phone numbers and one or more email addresses.

The FA is responsible for adding the family's members to the app, and that includes
providing the necessary data, which is outlined in the diagram below.


##1.1. Client Notes

Through discussions with the PO, it was concluded that in order for a person to use the app, they need to have a valid main email address,
and it is unique.
A person can be a member of multiple families, but in each one they must have
a different main email address.
Obviously, inside a given family, only one instance of a given person can be present. In other words, the same
person (by id) can have many accounts (with different emails), but not in the same family as one another.

##1.2. System Sequence Diagram

```plantuml
autonumber
title SSD Add family member
actor "Family Admin" as FA

Participant System
activate System
activate FA
FA -> System : add family member
System --> FA : ask data (name, id, birth date, vat, email, address)
deactivate System
FA -> System : inputs required data
activate System
System --> FA : inform success
deactivate System
deactivate FA
```

##1.3. Process View

```plantuml
title Process View
actor "Family Admin" as FM
activate FM

Participant UI
Participant "FFM_BusinessLogic" as BL 
FM -> UI : add family member
activate UI
UI--> FM : ask the data
deactivate UI
FM -> UI : input data
activate UI
UI -> BL : addFamilyMember(data)
activate BL
BL --> UI : result
deactivate BL
UI-> FM : shows result
deactivate UI
deactivate FM

```
##1.4. Connection with other User Stories

This US is not directly related to another US so far, but the methods used are
general enough to be used when the need for them arises. It is related to the
[US150](US150_GetProfileInfo.md) and [US151](US151_AddEmailAccountToProfile.md) 
in the sense that the family members are the actors in these
US, and they are created and added to the family here.

The actor of this US is the FA, which is created in the [US011](US011_AddFamilyAdministrator.md).

# 2. Analysis

In order to analyze this problem, we start by defining the relevant concepts
for this user story.

A new person will need to be created, which is the new member, and added to
the Family, which aggregates all the persons belonging to it as its members.

There are some consistency checks that will need to be performed throughout
this, firstly we have to check whether the person to be added is already 
present in the app, and we do this by comparing their main Email addresses. 
This is what is used to identify a person within the app, so it must be unique.

If there is already another person with that same Email address, the task should
immediately fail, since it doesn't make sense to add a person that already 
exists.
There is a caveat here, a person can be created without a mainEmail (for
example a baby or an elderly person). In this case, this check is bypassed.

After this we can proceed to the family (which the current user is the 
Admin of), and we have to again check if the person is indeed a new addition.
This is because we could in principle be adding a person already existing 
in this family, but with a different Email address, and that would be 
fine with the AppMembers, but not with the family. We don't want to add the same
person twice to the family, even with different Email addresses. We can
perform this check by comparing a unique attribute to each person, like their
ID or VAT numbers.

Finally, once all these checks have been passed, we add the new Person to 
the list of members of the family. 
This fulfills the request of the user story.

##2.1 Relevant excerpt of the Domain Model

```plantuml 
@startuml Domain Model
object "Family" as fam {
name
id
}
object Date {
year
day
month
}

object "Person" as p{
name
id
}
object "Email" as e{
email address
}

p "1" - "1" e : has >
p "*" - "1" fam : member >
p "1" - "1" Date : born >

@enduml
```
##2.2. Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **Name**                    | Mandatory, text, not empty or null, maximum of 20 characters.      |
| **ID**                      | Mandatory, integer with 8 digits, unique to each person.        |
| **Birth Date**              | Mandatory, date, not after current date, not empty or null.                    |            
| **VAT**                     | Mandatory, Unique, integer 8 digits, obeys specific rules given by the government.               |            
| **Email Address**           | Optional, if the member will use the app, unique in the system, EmailAddress, needs to be valid (x@y.z)                 |            
| **Address**                 | Optional, Address, contains street name (text), as well as postal code(integer with 4 digits - 3 digits)                    |            
| **PhoneNumber**             | Optional, PhoneNumber, string with 9 digits, has to start with 2 or 9               |            
| **Gender**                  | Mandatory, text, can be male or female only.

# 3. Design



## 3.1. Functionality

The family administrator, logged into the system, starts by sending a 
request through the UI to add a new family member to his family.
Then he inputs the data asked by the UI to create a new person (ID, name,
birthdate, VAT number, main Email address, phone number, address). Not all
these are mandatory, in principle he would just need to introduce the name, VAT, birth date and
ID number. If the person would also want to use the app, however, a main Email
address is also mandatory.

The UI then communicates with the controller tasked with telling the AppMembers
to add a new family member, and then in the app points him to the class responsible for managing
the families in our system, the FamilyService class. Here, all the families are stored, and 
we access the correct family through the mainEmail of the current user, which is its admin. This class
can access all the families and check their members, and it does that in this case in order to guarantee
the user being created is not yet registered in the app, as we discussed, this is done through the 
attribute mainEmail, which is what distinguishes users of the app.

Each family has, as an attribute, an object of the class responsible for performing
operations with Person objects, the PersonService class. This class contains a list of 
all the members of the family, and is tasked with creating
a new object newMember of the class Person, with the data inputted by the FA.

After the object newMember is created, we want to add it to the list of members of the family,
but first we have to perform another check,
since even if the Person we are trying to add is new to the AppMembers, we don't want
to add the same person (even if with a different Email address) to the family.
So in this check we use their ID to compare. If this check is also successful,
we finally add the person to the list of members of the family, informing the current user of the
success of 
the task.



```plantuml
@startuml
autonumber
title US101 : add family members
actor "Family Administrator" as Admin
participant " : UI" as UI
participant " : AddFamilyMemberController" as Controller
participant " : FFMApp" as AppMembers
participant " familyService \n: FamilyService" as service
participant " fam : Family" as fam
participant " perServ : PersonService" as perServ

activate Admin
Admin->UI:Add family member
activate UI
UI-->Admin: Ask for member data\n(name, ID, VAT,\nbirthdate, emailAddress)

deactivate UI
Admin->UI:Input member data
activate UI
UI->Controller: addMemberToFamily(data)
activate Controller
Controller -> AppMembers : getFamilyService()
Activate AppMembers
AppMembers-->Controller : familyService
deactivate AppMembers
Controller -> service : isMainEmailRegistered(emailAddress)
activate service
service --> Controller : result
deactivate service
Controller->service:getFamilyByAdminMainEmail(userMainEmail)
activate service
service-->Controller : fam
deactivate service
Controller ->fam : getPersonService()
activate fam
fam --> Controller : perServ
deactivate fam
Controller -> perServ : addMember(data)
activate perServ
ref over perServ: addMember(data)
perServ-->Controller : success
deactivate perServ
Controller-->UI : success
deactivate Controller
deactivate UI
UI-->Admin : Member added to family
deactivate Admin
@enduml
```

```plantuml
@startuml
autonumber
title : isMainEmailRegistered(mainEmail)
participant " : FamilyService" as service
participant "persons : List<Person>" as persons
participant "person : Person" as person
activate service
[o-> service : isMainEmailRegistered(newMember)
service -> persons : getListOfPersons()
loop for each person in persons
service->person : result = isMainEmail(mainEmail)
person-->service : boolean result
opt result==true
[<--o service : failure
end
end
[<--o service : success
deactivate service
@enduml
```

```plantuml
@startuml
autonumber
title : addMember(data)
participant "perServ : PersonService" as service
participant "newMember : person" as member
participant "members : List<Person>" as members
activate service
[o-> service : addMemberToFamily(data)
service --> member **: create(data)
ref over service : isPersonIdInFamily(newMember)
service ->members : add(newMember)
[<--o service :success
deactivate service
@enduml
```
```plantuml
@startuml
autonumber
title : isPersonIdInFamily(newMember)
participant " perServ: PersonService" as fam
participant "members : List<Person>" as persons
participant "person : Person" as person
[o-> fam : isPersonIdInFamily(newMember)
activate fam
loop for each person in persons
fam->person : result = hasSameId(newMember)
activate person
person-->fam : boolean result
deactivate person
opt result==true
[<--o fam : failure
end
end
[<--o fam : success
deactivate fam

@enduml
```
## 3.2. Class Diagram

```plantuml
@startuml
class Person{
name : String
ID : int
birthDate : Date
vat : VAT
mainEmail : Email
Person(name,id,birthdate,vat,mainEmail)
boolean hasSameId(Person other)
boolean isMainEmail(Email userMainEmail)
}
class Family{
personList : PersonService
PersonService getPersonService()
}
class FFMApp{
families : FamilyService
FamilyService getFamilyService()
}
class AddFamilyMemberController{
app : FFMApp
AddFamilyMemberController(FFMApp app)
addMemberToFamily(Email userMainEmail, data)
}
class FamilyService{
familyList : List<Family>
boolean isMainEmailRregistered(Email userMainEmail)
Family getFamilyByAdminMainEmail(userMainEmail)
}
class PersonService{
members : List<Person>
boolean isPersonIdInFamily(int id)
boolean addMember(data)
}
@enduml
```

## 3.3. Applied Patterns

The following patterns were applied in this US:

- Information Expert: we store the information needed to fulfill a given task
in the class responsible for that task, for example, the class Family is the one
  who has the method to determine whether a person is a member of that family
  
- Pure fabrication: we created a class, FFMApp, that doesn't represent a physical 
concept in our problem, but serves the purpose to help the flow of the program, 
  keeping relevant information and directing requests to the classes responsible 
  for the relevant information
  
- Controller: we use a controller class to make the communication between the UI,
that interacts directly with the user, and the system. Each controller is 
  responsible for treating a different user case, in this US the AddFamilyMemberController
  class is responsible for coordinating the operation of adding new family members.

- Creator: the class responsible for storing the Person class objects is 
also the one who creates them (PersonService)

## 3.4. Tests 

In this section some tests will be presented that guarantee the requisites
of the US are applied.

**Test1** : add a member that already exists in the app
```
@Test
    @DisplayName("Fail to Add Family Member Controller - already in the family, diferent ID, same Email")
    void addMemberToFamilyTestFailMainEmailAlreadyRegistered() {

        EmailAddress mainEmail = new EmailAddress("123@isep.ipp.pt");
        EmailAddress memberEmail = new EmailAddress("member@isep.ipp.pt");
        FFMApplication app = new FFMApplication();

        AddFamilyController ctrlFamily = new AddFamilyController(app);
        AddFamilyAdministratorController ctrlFamilyAdministrator = new AddFamilyAdministratorController(app);
        AddFamilyMemberController ctrlAddMember = new AddFamilyMemberController(app);

        Family family = ctrlFamily.addFamily("Rodrigues", new Date());
        int idFamily = family.getId();
        Family family1 = ctrlFamily.addFamily("Santos", new Date());
        int idFamily1 = family1.getId();
        ctrlFamilyAdministrator.addFamilyAdmin("Miguel", 20, null, new Date(), mainEmail, null, "male", idFamily);
        ctrlFamilyAdministrator.addFamilyAdmin("Ricardo", 12, null, new Date(), memberEmail, null, "male", idFamily1);

        boolean result = ctrlAddMember.addMemberToFamilyNew(mainEmail, "Martim", 987654321, null, new Date(), memberEmail, null, "male");

        assertFalse(result);
    }

```
**Test2** : add a member already in the family with a different main email
```
@Test
    @DisplayName("Fail to Add Family Member - already in the family - same ID, different email")
    void addMemberToFamilyTestFailAlreadyInFamilyDifferentEmil() {

        EmailAddress mainEmail = new EmailAddress("123@isep.ipp.pt");
        EmailAddress memberEmail = new EmailAddress("member@isep.ipp.pt");
        FFMApplication app = new FFMApplication();

        AddFamilyController ctrlFamily = new AddFamilyController(app);
        AddFamilyAdministratorController ctrlFamilyAdministrator = new AddFamilyAdministratorController(app);
        AddFamilyMemberController ctrlAddMember = new AddFamilyMemberController(app);

        Family family = ctrlFamily.addFamily("Rodrigues", new Date());
        int idFamily = family.getId();
        ctrlFamilyAdministrator.addFamilyAdmin("Miguel", 20, null, new Date(), mainEmail, null, "male", idFamily);
        ctrlAddMember.addMemberToFamilyNew(mainEmail, "Margarida", 123456789, new VAT(241670446), new Date(), memberEmail, null, "female");

        boolean result = ctrlAddMember.addMemberToFamilyNew(mainEmail, "Margarida", 123456789, new VAT(241670446), new Date(), new EmailAddress("fail@isep.ipp.pt"), null, "female");

        assertFalse(result);
    }

```
**Test3** : add a member that already exists in the app but not in the family,
with a different main email
```
@Test
    @DisplayName("Add Family Member Controller - Success")
    void addMemberToFamilyTestSuccessMemberAlreadyRegisteredDifferentMainEmail() {

        EmailAddress mainEmail = new EmailAddress("123@isep.ipp.pt");
        EmailAddress memberEmail = new EmailAddress("member@isep.ipp.pt");
        EmailAddress otherEmail = new EmailAddress("member2@isep.ipp.pt");
        FFMApplication app = new FFMApplication();

        AddFamilyController ctrlFamily = new AddFamilyController(app);
        AddFamilyAdministratorController ctrlFamilyAdministrator = new AddFamilyAdministratorController(app);
        AddFamilyMemberController ctrlAddMember = new AddFamilyMemberController(app);

        Family family = ctrlFamily.addFamily("Rodrigues", new Date());
        int idFamily = family.getId();
        Family family1 = ctrlFamily.addFamily("Santos", new Date());
        int idFamily1 = family1.getId();

        ctrlFamilyAdministrator.addFamilyAdmin("Miguel", 20, null, new Date(), mainEmail, null, "male", idFamily);
        ctrlFamilyAdministrator.addFamilyAdmin("Ricardo", 12, null, new Date(), memberEmail, null, "male", idFamily1);

        boolean result = ctrlAddMember.addMemberToFamilyNew(mainEmail, "Ricardo", 12, null, new Date(), otherEmail, null, "male");

        assertTrue(result);
    }
```
**Test4** : add a member that is new to the app
```
@Test
    @DisplayName("Add Family Member Controller - Success")
    void addMemberToFamilyTestSuccess() {

        EmailAddress mainEmail = new EmailAddress("123@isep.ipp.pt");
        EmailAddress memberEmail = new EmailAddress("member@isep.ipp.pt");
        FFMApplication app = new FFMApplication();

        AddFamilyController ctrlFamily = new AddFamilyController(app);
        AddFamilyAdministratorController ctrlFamilyAdministrator = new AddFamilyAdministratorController(app);
        AddFamilyMemberController ctrlAddMember = new AddFamilyMemberController(app);

        Family family = ctrlFamily.addFamily("Rodrigues", new Date());
        int idFamily = family.getId();
        ctrlFamilyAdministrator.addFamilyAdmin("Miguel", 20, null, new Date(), mainEmail, null, "male", idFamily);

        boolean result = ctrlAddMember.addMemberToFamilyNew(mainEmail, "Martim", 123456789, null, new Date(), memberEmail, null, "male");

        assertTrue(result);
    }
```
**Test5** : add a member with invalid attribute name (empty)
```
@Test
    @DisplayName("Add Family Member Controller - Fail")
    void addMemberToFamilyTestFailInvalidAttributes() {

        EmailAddress mainEmail = new EmailAddress("123@isep.ipp.pt");
        EmailAddress memberEmail = new EmailAddress("member@isep.ipp.pt");
        FFMApplication app = new FFMApplication();

        AddFamilyController ctrlFamily = new AddFamilyController(app);
        AddFamilyAdministratorController ctrlFamilyAdministrator = new AddFamilyAdministratorController(app);
        AddFamilyMemberController ctrlAddMember = new AddFamilyMemberController(app);

        Family family = ctrlFamily.addFamily("Rodrigues", new Date());
        int idFamily = family.getId();

        ctrlFamilyAdministrator.addFamilyAdmin("Miguel", 20, null, new Date(), mainEmail, null, "male", idFamily);

        assertThrows(NullPointerException.class,()->{boolean result = ctrlAddMember.addMemberToFamilyNew(mainEmail, "", 12, null, new Date(), memberEmail, null, "male");});

    }
```
# 4. Implementation

In this US we developed a way to add family members to the families created in [US010](US010_CreateFamily.md). In order to do 
this, we obviously use the class Person as a concept, since the family members will be objects of this class. 
```
public class Person {

    private String name;
    private int id;
    private Date birthDate;
    private VAT vat;
    private EmailAddress mainEmail;
    private List<EmailAddress> otherEmails;
    private List<PhoneNumber> phoneNumbers;
    private Address address;
    private String gender;

    public Person(String name, int id, VAT vat, Date birthDate, EmailAddress mainEmail, Address address,
                  String gender) {
        setName(name);
        setBirthdate(birthDate);
        this.id = id;
        this.vat = vat;
        this.mainEmail = mainEmail;
        this.otherEmails = new ArrayList<EmailAddress>();
        this.phoneNumbers = new ArrayList<PhoneNumber>();
        this.address = address;
        setGender(gender);
    }
    
    public boolean hasSameId(Person other) {
        return this.id == other.id;
    }
}
```
The Controller class responsible for the realization of this US is AddFamilyMemberController, and the service class responsible
for storing, and thus creating, family members is the PersonService class. Each family has an object of this type as an 
attribute, and this class is responsible for all operations regarding management of persons of that family.
```
public class AddFamilyMemberController {

    private FFMApplication app;

    public AddFamilyMemberController(FFMApplication app) {
        if (app == null)
            throw new IllegalArgumentException("Application cannot be null.");
        this.app = app;
    }

    public boolean addMemberToFamily(EmailAddress adminMainEmail, String name, int id, VAT vat, Date birthDate, EmailAddress emailAddress, Address address, String gender) {
        FamilyService familyService = app.getFamilyService();
        if (familyService.isMainEmailRegistered(emailAddress))
            return false; //email already exists
        Family family = familyService.getFamilyByAdminMainEmail(adminMainEmail);
        PersonService personList = family.getPersonService();
        return personList.addMember(name, id, vat, birthDate, emailAddress, address, gender);
    }

}
```
```
public class PersonService {
    private List<Person> members;
    private EmailAddress mainEmailOfAdmin;

    public PersonService() {
        this.members = new ArrayList<>();
        this.mainEmailOfAdmin = null;
    }
    
    public boolean addMember(String name, int id, VAT vat, Date birthDate, EmailAddress emailAddress, Address address, String gender) {
        Person member = createPerson(name, id, vat, birthDate, emailAddress, address, gender);
        if (this.isIdInList(member))
            return false;
        members.add(member);
        return true;
    }
    
    public boolean isIdInList(Person newMember) {
        for (Person person : members) {
            if (person.hasSameId(newMember))
                return true;
        }
        return false;
    }
}
```

# 5. Integration/Demonstration

The methods developed in this US are essential in the general system, since a lot of operations are performed in the members
of the families registered in the app. In that sense, [US104](US104_ListOfMembersRelations.md), [US150](US150_GetProfileInfo.md), and
[US151](US151_AddEmailAccountToProfile.md) are directly related to this, since they use the objects created and stored here.

# 6. Observations

This US led to a productive discussion within the group, about the best design structure to create and store the members of
the family. Initially all the persons were stored in a list present in the FFMApplication class, and then the concept of Service class
was introduced. This compelled the group to change the placement of that list to the PersonService class, as an attribute of
the FFMApplication class; parallel to that, each family had its own list of members.

After careful consideration, we concluded it made a lot more sense to attribute to each family a PersonService object, containing only 
its members. This not only eliminates the redundancy in having two lists with repeated attributes, but also delegates the 
responsibility to each family of pointing to the right instance of PersonService, in accordance with the Single Responsibility principle.



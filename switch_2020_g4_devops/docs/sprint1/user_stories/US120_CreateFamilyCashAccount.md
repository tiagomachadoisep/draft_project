# US120
=======================================


# 1. Requirements


*As a Family Administrator, I want to create a cash account for my family.*

In this user story our team produced a method that permit the family administrator to build a single a cash account
for his family.

The only requirement to this method is that the family in question don't have any cash account yet

##1.1 Client Notes 

Our client informed us that would be interesting to start the cash account with some value.  

##1.2 System Sequence Diagram

```plantuml
autonumber
title SSD Create Cash Account
actor "FamilyAdmin" as FA
activate FA

Participant System
activate System
FA -> System : create new Family
System --> FA : ask data (balance)
deactivate System
FA -> System : inputs required data
activate System
System --> FA : inform success
deactivate System
deactivate FA

```

##1.3 Process View

```plantuml
@startuml
autonumber
title Process View
actor "FamilyAdmin" as FA
participant ":FFM_UI" as UI
participant ":FFM_BusinessLogic" as BL

activate FA
FA -> UI : asks to create cashAccount
activate UI
UI -> FA : asks for data
deactivate UI
FA -> UI : informs data
activate UI
UI -> BL : add data 
activate BL
BL -> UI : result
deactivate BL
UI -> FA : shows result
deactivate UI
deactivate FA
@enduml
```


##1.4 Connection with other User Stories

This US is dependent on ([US010](../docs/US010_createFamily.md))and ([US011](../docs/US011_createFamilyAdmin.md)) in the way what we need to have a family
and the respective family administrator to be able to create a cash account.   

# 2. Analysis

This process permit that a cash account is created for the family to handle cash movements.

We considered that for now we only need to know the cashAccount balance because there's only one for each family.

###2.1 Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **Balance**                 | Mandatory, double with two decimal points, not null               |

###2.2 Relevant excerpt of the Domain Model

Next we can see a fraction of our domain diagram with the needed entities for this user story 

```plantuml
@startuml

object Family
object CashAccount
Family : name
Family : id
CashAccount : balance

Family "1" - "1" CashAccount : create > 

@enduml
```

# 3. Design

## 3.1. Functionality

First the Family Administrator (FA) ask the User Interface (UI) to create a cash account,
and the UI asks back for the data needed to the operation.
We considered that to create a cash account the FA need to input one argument, balance, but if no input is given
the cash account is created with balance 0.

Considering that everything went well in the first step, throughout a controller class we communicate with the FFMApp 
to get the service responsible for create cash accounts, FamilyService.


With the FA main email we get the is respective Family and for the CashAccount creation.

Then family verify if already has a cash account, then two situations can occur, they are:

- family already has a cash account, thus, a failure message is sent to the FA

- is the first request for this family, thus create one cash account, and a success message is sent to the FA

Next you may find the sequence diagram for this user story:

```plantuml 
@startuml
autonumber
title **US120** - Create Cash Account
actor "FamilyAdmin" as FA
participant ":UI" as UI
participant ":createCashAccountController" as CAC
participant ":FFM\nApp" as AppMembers
participant "accountService:\nAccountService" as aS 
participant "accountFactory:\nAccountFactory" as aF


activate FA
FA -> UI : createCashAccount
activate UI
UI -> CAC : createCashAccount\n(description)
activate CAC
CAC -> AppMembers : getAccountService()
activate AppMembers
AppMembers --> CAC : accountService
deactivate AppMembers
CAC -> aS : addCashAccount(description)
activate aS
aS -> aS : id =generateAccountId()
aS -> aF : fabricateAccount\n(description, id)
activate aF
return cashAccount
aS -> aS : add(cashAccount)

return : result
deactivate aS
deactivate AppMembers
CAC --> UI : result
deactivate CAC
UI --> FA : result
deactivate UI
deactivate FA
@enduml
```


## 3.2. Class Diagrams

```plantuml
@startuml
skinparam linetype polyline
createCashAccountController - FFMApp : application
FFMApp -- AccountService : accountService
AccountService -- Account : listOfAccounts
AccountService -left- AccountFactory: depends
CashAccount -|> Account 
AccountConcreteFactory -|> AccountFactory
AccountConcreteFactory --> CashAccount : creates 



class createCashAccountController{
+ controllerCashAccount(description)
}

class FFMApp{
+ getFamilyService()
}

class AccountService{
+ addCashAccount(description)
- generateId()
}

interface AccountFactory{
+ fabricateAccount()
}

class AccountConcreteFactory{
+ fabricateAccount()
}

interface Account {
'- accountFactory : accountConcreteFactory

}

class CashAccount {
- description : String
+ CashAccount(description) 
}
@enduml
```


## 3.3. Applied Patterns

**Controller pattern** - We create a class (CreateCashAccountController) whose only purpose is to build a "bridge" between the UI and the AppMembers

**Information Expert** - It's the Family who knows if has or has not a CashAccount, 
it's an attribute of the Family so should be Family responsibility to create a cashAccount.

**Low Coupling** - There's almost no dependencies to create a cashAccount, 
the Class Family only need another class (CashAccount) to instantiate the object.

**High Cohesion** - We create a _FamilyService_ to delegate responsibility, this way FFMApp just need to know that class
responsible for create a cashAccount, in this US120 it's _FamilyService_. 
FamilyService after invoke Family for the cashAccount creation. This way we're not overwhelming any class.

 

## 3.4. Tests 

Implemented tests for the class ***CashAccount*** related to this US:

**For constructors**

**Test 1:** Constructor with given balance test:

    @Test
    @DisplayName("Constructor with given balance test")
    void cashAccountConstructerTest() {
    CashAccount newCashAccount = new CashAccount(200);
    assertNotNull(newCashAccount);
    }

**Test 2:** Constructor with no input test

*For method getBalance()*

**Test 3:** Get expected balance

**Test 4:** Get wrong balance

**Test 5:** Get rounded down balance

**Test 6:** Get rounded up balance

**Test 7:** Get balance when account is created with no starting value



Implemented tests for the class ***Family*** related to this US:

*For method addCashAccount(cashAccount)*

**Test 8:** Fail to add a valid account - family already has one

    @Test
    @DisplayName("Fail to add a valid account - family already has one")
    void addCashAccountTestWithCashAccount() {
    Family family = new Family("Rodrigues", 0);
    family.addCashAccount(200); 
    boolean result = family.addCashAccount(500);
    assertFalse(result);
    }


**Test 9** Fail to add a valid account with no balance - family already has one

**Test 10:** Add a valid cashAccount - positive balance

**Test 11:** Add a valid cashAccount - negative balance

**Test 12:** Add a valid cashAccount - zero balance

**Test 13:** Add a valid cashAccount - no input value




Implemented tests for the class ***FamilyService*** related to this US:

**Test 14:** Fail to create cashAccount - Family already has one

**Test 15:** Add cashAccount - Success



Implemented tests for the class ***CreateCashAccountController*** related to this US:

**Test 16:** Fail to create cashAccount - AppMembers is null

**Test 17:** Creates Cash Account in family with no Cash Account
    
    @Test
    @DisplayName("Creates Cash Account in family with no Cash Account")
    void createCashAccountTestTrue() {
    EmailAddress mainEmail = new EmailAddress("123@isep.ipp.pt");
    FFMApplication app = new FFMApplication();
    double balance = 100;

    AddFamilyController ctrlFamily = new AddFamilyController(app);
    AddFamilyAdministratorController ctrlFamilyAdministrator = new AddFamilyAdministratorController(app);

    Family family = ctrlFamily.addFamily("Rodrigues", new Date());
    int idFamily = family.getId();

    ctrlFamilyAdministrator.addFamilyAdmin("Miguel", 20, null, new Date(), mainEmail, null, "male", idFamily);

    CreateCashAccountController ctrlCashAccount = new CreateCashAccountController(app);
    boolean result = ctrlCashAccount.createCashAccount(balance, mainEmail);

    assertTrue(result);
    }

**Test 18:** Fail to createCashAccount - family already has one


# 4. Implementation

There was not much complexity developing methods related to this US, 
we consider that  the starting cash account value can negative, positive or zero, 
we didn't need any specific validation. Next is the constructor for the class:


    public CashAccount(double balance){

        double formatedBalance = Utilities.roundToDecimals(balance);

        this.balance=formatedBalance;
    }


# 5. Integration/Demonstration

For now cashAccount is not a mandatory attribute to any other operator in the app,
so the creation of a cash is an independent process, and no other is dependent on it.


# 6. Observations

In this US120 we considered that should be possible to create a cashAccount with value 0, positive or negative.

It's clear why we need the option to create an account with value zero or positive, 
but we also think that it's important to have the possibility to start a cashAccount with for example -50.00€, because 
the family can have some kind of debt that want to be represented.




# US151
=======================================


# 1. Requirements

*As a family member, I want to add an email to my profile.*

In this user story a family member will add an email to your profile. This new email address will not be the FM's main email. This is because at [US101](US101_AddFamilyMember.md), the FA will be responsible for creating the family members and assigning the main email to each person.
This user story will serve to add new emails to the profile, but a FM doesn't need to have one.

##1.1. Client notes

There are no PO's observations regarding the development of this US.

##1.2. System Sequence Diagram

Next is the system sequence diagram realized for this US.

```plantuml
title SSD Add Email Account To Profile
actor "Family Member" as FM
activate FM

Participant System
FM -> System : add email account to profile
System --> FM : result
deactivate FM
```
##1.3.Process View

```plantuml
title Process View
actor "Family Member" as FM
activate FM

Participant UI
Participant "FFM_BusinessLogic" as BL 
FM -> UI : add email account to profile
activate UI
UI--> FM : ask the data
deactivate UI
FM -> UI : input data
activate UI
UI -> BL : add email account to profile(email)
activate BL
BL --> UI : result
deactivate BL
UI-> FM : shows result
deactivate UI
deactivate FM

```
##1.4. Connection with other User Stories

For a FM to be able to add a new email, it is necessary that the SM has already created the family ([US010](US010_CreateFamily.md)) and has already created the FA ([US101](US101_AddFamilyMember.md)).

With the FA created, he can add family members and create their profiles ([US101](US101_AddFamilyMember.md)). So with the FM created, he can access your account and add a new email to your profile.

# 2. Analysis

The FM may have zero or many emails registered, but only one main email.

The main email will be created by the FA along with the other profile information of family members.

## 2.1. Attributes

| **_Attributes_**              | **_Business Rules_**                                                                                           |
| :--------------------------   | :--------------------------------------------------------------------------------------------------------------|
| **Email **                    | Mandatory, alphanumeric, not empty or null, follows validation rules. This is the email the person wants to add.       |
| **MainEmail **                | Mandatory, alphanumeric, not empty or null, follows validation rules. This is the email register as main email in the app.        |

## 2.2. Relevant excerpt of the Domain Model
The following diagram contains an excerpt of the Domain Model that illustrates the concepts related to this US.

```plantuml
@startuml
object Person
Person : otherEmails

object Email
Email : email
Person "1" -- "0..*" Email : has >
@enduml
```

# 3. Design

## 3.1. Functionality

The team has designed a SD that represents the functionality required by this US.

Through unit testing we call the controler and ask it to add the email to the person's profile, both data are passed by parameter. Then, the controller will forward the request to the PersonRepository, which is where the people in the application are located.

In the Repository, we check if the person exists in the application and if it exists, the object Person is selected. If not, send an error message.

Upon finding the Person, the request to add an email to the profile is forwarded and it is at the Person that the Email is created. If the email is valid, it is checked if it has already been added, if it has not been inserted, it is added to the list.

Finally, having success in all stages, the object created before return to controller.

```plantuml
@startuml
autonumber
title US151 - Add email to profile\n

participant "emailController\n : EmailController" as EC
participant "people : PersonRepository" as PR
participant "thePerson : Person" as P
participant ": Email" as E

[o-> EC : addEmailToProfile(email, mainEmail)
activate EC
EC -> PR : addEmailToProfile(email, mainEmail)
activate PR
PR -> PR : thePerson = getPersonByID(mainEmail)
PR -> P : addEmailToProfile(email, mainEmail)
activate P
P  -> E ** : create(email)
activate E
E -> E : validateEmail(email)
deactivate E
P -> P : addEmail(email)
P -->PR : email
deactivate P
PR --> EC : email
deactivate PR
[o<-- EC : email 
deactivate EC

@enduml
```
Going more details in getPersonByID() method:
```puml
autonumber
title US151 - getPersonByID(mainEmail)\n
participant ":PersonRepository" as PR
participant "personList" as P
[o-> PR : getPersonByID(mainEmail)
loop foreach person
opt id==mainEmail
PR -> P : isID(mainEmail)
P --> PR : person
end
end
[o<--PR : person

@enduml
```
## 3.2. Class Diagram

In this US four classes interact with each other:

- EmailController
- PersonRepository
- Person
- Email

The following is a class diagram related to this US.

```plantuml
@startuml

class PersonRepository 
class Email <<Value Object>> 
class PersonID <<Value Object>> <<ID>>

package "Aggregate"  {
class Person <<Root>><<Entity>>

PersonRepository "1"-left--> "0..*" Person : contains
Person "1" ---> "0..*" Email : has
Person "1" ---> "1" PersonID : has
}
@enduml
```

## 3.3. Applied Patterns

In this US we can find the patterns below. 

**Controller** - to manipulate and coordinate system events for a class we use the "EmailController". 

**Information expert** - we found this concept in PersonRepository, for exemple, because it knows everything about Person. 

**Creator** - we can found this concept in PersonRepository, they are responsible to create people.

## 3.4. Tests 

Below we find the main tests in the controller that allow a correct assessment of compliance with the requirements.

**Test 1:** A valid Email address

    @Test
    @DisplayName("Test add email - success")
    void addEmailAccountToProfile() {
        FamilyRepository familyRepository = new FamilyRepository();
        familyRepository.createFamily("Castro", "srCastro@gmail.com");

            PersonRepository personRepository = new PersonRepository();
            personRepository.addPerson("srCastro@gmail.com", "Sr.Castro", 237179202, "Rua dos bobos, 0");
            personRepository.addPerson("sraCastro@gmail.com", "Sra.Castro", 228808790, "Rua dos bobos, 0");

            int familyID = 1;
            familyRepository.addMemberToFamily(familyID, new PersonID(new Email("srCastro@gmail.com")));

            AddEmailController emailController = new AddEmailController(personRepository);
            Email result = emailController.addEmailAccountToProfile("sraCastro1970@hotmail.com", "sraCastro@gmail.com");
            Email expected = new Email("sraCastro1970@hotmail.com");
            assertEquals(result, expected);
    }
	
        
**Test 2:** An invalid email address 

    @Test
    @DisplayName("Invalid email - Fail")
    void invalidEmail() {
    FamilyRepository familyRepository = new FamilyRepository();
    familyRepository.createFamily("Castro", "srCastro@gmail.com");

        PersonRepository personRepository = new PersonRepository();
        personRepository.addPerson("srCastro@gmail.com", "Sr.Castro", 237179202, "Rua dos bobos, 0");
        personRepository.addPerson("sraCastro@gmail.com", "Sra.Castro", 228808790, "Rua dos bobos, 0");

        int familyID = 1;
        familyRepository.addMemberToFamily(familyID, new PersonID(new Email("srCastro@gmail.com")));

        AddEmailController emailController = new AddEmailController(personRepository);
        Email result = emailController.addEmailAccountToProfile("sraCastro1970@hotmail", "sraCastro@gmail.com");
        Email expected = new Email("sraCastro1970@hotmail.com");
        assertNotEquals(result, expected);
    }
    
    
**Test 3:** Empty email

    @Test
    @DisplayName("Empty email - Fail")
    void EmptyEmail() {
    FamilyRepository familyRepository = new FamilyRepository();
    familyRepository.createFamily("Castro", "srCastro@gmail.com");

        PersonRepository personRepository = new PersonRepository();
        personRepository.addPerson("srCastro@gmail.com", "Sr.Castro", 237179202, "Rua dos bobos, 0");
        personRepository.addPerson("sraCastro@gmail.com", "Sra.Castro", 228808790, "Rua dos bobos, 0");

        int familyID = 1;
        familyRepository.addMemberToFamily(familyID, new PersonID(new Email("srCastro@gmail.com")));

        AddEmailController emailController = new AddEmailController(personRepository);
        Email result = emailController.addEmailAccountToProfile("", "sraCastro@gmail.com");
        Email expected = new Email("sraCastro1970@hotmail.com");
        assertNotEquals(result, expected);
    }

**Test 4:** The email already exists

    @Test
    @DisplayName("Email already exists - Fail")
    void emailAlreadyExists() {
    FamilyRepository familyRepository = new FamilyRepository();
    familyRepository.createFamily("Castro", "srCastro@gmail.com");

        PersonRepository personRepository = new PersonRepository();
        personRepository.addPerson("srCastro@gmail.com", "Sr.Castro", 237179202, "Rua dos bobos, 0");
        personRepository.addPerson("sraCastro@gmail.com", "Sra.Castro", 228808790, "Rua dos bobos, 0");

        int familyID = 1;
        familyRepository.addMemberToFamily(familyID, new PersonID(new Email("srCastro@gmail.com")));

        AddEmailController emailController = new AddEmailController(personRepository);

        assertThrows(IllegalArgumentException.class, () -> {
            emailController.addEmailAccountToProfile("sraCastro@gmail.com", "sraCastro@gmail.com");
        });
    }

# 4. Implementation

There was no great complexity when developing this functionality. We did essential validations such as the email format using a regular expression and another one that the email would not be accepted if it was not filled out by the user.
 
For exemple, if the user enters a valid email, but forgets and places a space between the letters. Then, the program returns an error.

# 5. Integration/Demonstration

There are no direct dependencies or integrations with this Health Unit. However, it can only exist after a member of the family has been inserted.

Below we can check a method that goes to a person's email list to check if it is inserted in the repository. If it is, the person returns, if not, it returns an error message.
    
    public Person getPersonByID(ID mainEmail) {
    for (Person person : personList) {
    if (person.isID(mainEmail))
    return person;
    }
    throw new IllegalArgumentException("The person could not be found.");
    }

# 6. Observations

Overall we managed to get the expected for this US. There are no relavant comments to do.
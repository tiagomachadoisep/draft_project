# US150
=======================================


# 1. Requirements

*As a family member, I want to get my profile’s information.*

## 1.1 Client Notes

Taking into account the information provided, and the answers received about this user story, it's certain that:

- A registered FM must be able to access the personal information contained in his/hers profile.

    - A FM can only be registered in the system if he/she has a main Email.
    
- Only the FM personal information should be retrieved, so any information about potential transactions and accounts of the user should be left out. 

## 1.2 System Sequence Diagram

Next, we present the System Sequence Diagram.

```plantuml
autonumber
title SSD Get Profile Information
actor "Family Member" as FM
activate FM
Participant System
activate System
FM -> System : ask profile information
System --> FM : return profile information
deactivate System
deactivate FM
``` 

## 1.3 Process view

Below it's presented the process view diagram:

```plantuml
autonumber
title SSD Get Profile Information
actor "Family Member" as FM
Participant UI
Participant FFM_BusinessLogic as AppMembers
activate FM
FM -> UI : ask profile information
UI -> AppMembers : getProfileInfo(mainEmail)
activate AppMembers
AppMembers --> UI : profileInfo
deactivate AppMembers
UI --> FM : return profile information
deactivate UI
deactivate FM
``` 

## 1.4 Connection with other User Stories

This US is dependent on the [US010](US010_CreateFamily.md) to create families where members can be grouped and on the [US101](US101_AddFamilyMember.md), which adds a family member and his/hers respective profile information.

# 2. Analysis

The family member asks for his/hers profile information to be displayed. The system redirects that requests to the correct class, the requested information is retrieved and displayed to the user.

## 2.1 Attributes

This US does not create any instance of a class.

## 2.2 Relevant excerpt of the Domain Model

Below it's presented a partial Domain Model with the relevant information: 

```plantuml
@startuml
object Person {
name
ID
birthDate
vat
mainEmail
otherEmails
phoneNumbers
address
}
object EmailAddress {
emailAddress
}

Person "1" - "1..*" EmailAddress : can have secondary >
Person "1" - "1" EmailAddress : has main email >
@enduml
```

On one hand, Person contains information about the user's name, ID, birth date, VAT number, main Email, secondary emails, phone numbers, address, and gender.

On the other hand, EmailAddress contains the email addresses of the users in the system.

   - Each person can have more than one email, but must have only one main email.


# 3. Design

## 3.1. Functionality

The FM requests access to his/hers profile information, providing his/hers main email. The getProfileInfoController receives the requests and, first, needs to interact with the FFMApplication to request the familyService. After the Controller receives the familyService, it interacts with it in order to identify the family which member is identified by the main email provided. Once that match is found, the correspondent family object is returned, and the controller accesses the personList inside that family, and, searches within the personList (that holds the list of the members of the family) for the person object whose main email matches the one provided. Once the relevant person has been identified, his/hers profile information is passed to String format through the toString(person) method, and that is retrieved all the way to the UI and the FM. 

```plantuml 
@startuml
autonumber
title **US150** Get the Family Member's profile information
actor "Family Member" as FM
participant ": UI" as UI
participant ": getProfileInfoController" as GPI
participant ": FFMApplication" as AppMembers
participant "familyService:\nFamilyService" as FS 
participant "family:\nFamily" as F
participant "personList:\nPersonList" as PS 
participant "person:\nPerson" as P

activate FM
FM -> UI : Get profile information (Main Email)
activate UI
UI -> GPI : getProfileInfo(mainEmail)
activate GPI
GPI -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> GPI : familyService
deactivate AppMembers
GPI -> FS : getFamilyByMemberMainEmail(mainEmail)
activate FS
ref over FS
getFamilyByMemberMainEmail(mainEmail)
end ref
FS --> GPI : family
deactivate FS
GPI -> F : getPersonList()
activate F
F --> GPI : personList
deactivate F
GPI -> PS : getPersonByMainEmail(mainEmail)
activate PS
ref over PS
getPersonByMainEmail(mainEmail)
end ref
PS --> GPI : person
deactivate PS
GPI -> P : toString()
activate P
P --> GPI : profileInfo
deactivate P
GPI --> UI : profileInfo
deactivate GPI
UI --> FM : profile information
deactivate UI
deactivate FM

@enduml
```

Diving deeper into the getFamilyByMainEmail(mainEmail) method:

The familyService starts a loop where it checks which family has a member whose main email matches the one provided. When that correspondence is detected, the appropriate family object is returned.

```plantuml
@startuml
autonumber
title //getFamilyByMemberMainEmail(mainEmail)//
participant "familyService:\nFamilyService" as FS
participant "families:\nList<Family>" as LF
participant "family:\nFamily" as F

[o-> FS : getFamilyByMemberMainEmail(mainEmail)
activate FS
loop for each family in families
FS -> F : isMainEmailOfAnyMember(mainEmail)
activate F
F --> FS : result
deactivate F
[<-- FS : family
end
deactivate FS
@enduml
````

Going deeper once again, the getPersonByMainEmail(mainEmail) works in the following way:

The personList starts a loop where it checks which person in that person list has a main email that matches the one provided. When that correspondence is detected, the appropriate person object is returned.

```plantuml
@startuml
autonumber
title //getPersonByMainEmail(mainEmail)//
participant "personList:\nPersonService" as PS
participant "personList:\nList<Person>" as LP
participant "person:\nPerson" as P

[o-> PS : getPersonByMainEmail(mainEmail)
activate PS
loop for each person in personList
PS -> P : isMainEmail(mainEmail)
activate P
P --> PS : result
deactivate P
[<-- PS : person
end
deactivate PS
@enduml
````

## 3.2. Class Diagrams

On this US, four classes interact with eachother as follows:

```plantuml
@startuml
title **US150** Get Profile Information

class "getProfileInfo \n Controller" as GPI {
- FFM Application : app
+ getPersonService()
+ String getProfileInfo(mainEmail)
 } 

class "FFMApplication" as AppMembers {
- families : List<Family>
- people : List<Person>
+ String getProfileInfo(mainEmail)
+ boolean login(mainEmail)
}

class "PersonService" as PS{
personList : List<Person>
+ getPersonByMainEmail(Person person)
- login(mainEmail)
}

class "Person" as P{
- name : String
- ID : int
- birthDate : Date
- vat : VAT
- mainEmail : Email
- otherEmails : List<Email>
- phoneNumbers : List<PhoneNumber>
- address : Address
+ Person getProfileInfo()
- String toString(person)
}

GPI - AppMembers : asks >
AppMembers - PS : asks >
PS - P : asks >
@enduml
```

## 3.3. Applied Patterns


**Controller**: Concerning to getProfileInfoController, this is responsible to receive and coordinate the system operations necessary to fulfill this process.

**Information Expert**: The Information Expert pattern can be seen in action, too, when the system interacts with the Person class, not requesting the information needed and transforming it into String format, but indicating the information needed and allowing the Person class to retrieve it and transform it into String format itself.

**Creator**: The PersonService class is responsible to create a new person.

**Pure Fabrication** : The FFMApplication and PersonService classes are not part of the concepts of the problem, but are created to coordinate the program.

## 3.4. Tests 

Below are the tests needed to guarantee that the requisites of the US are applied.

**Test1** : Check if profile with all information fields filled in is successfully retrieved.

**Test2** : Check if profile with only essential information fields filled in is successfully retrieved.

Below, it's presented the test 2, for illustration purposes:

    @Test
    @DisplayName("Check if, having a profile filled only with mandatory information, that information is successfully retrieved.")
    public void getProfileInfo_mandatoryInformation() {

        FFMApplication app = new FFMApplication();
        Family family = new Family("Henriques", 1);
        app.getFamilyService().addFamilyIfNotExists(family);
        String personName = "Ana";
        int id = 1;
        Date birthDate = new GregorianCalendar(2002, Calendar.JULY, 12).getTime();
        EmailAddress mainEmail = new EmailAddress("ana@hotmail.com");
        String gender = "female";

        family.getPersonService().addMember(personName, id, null,birthDate, mainEmail,null, gender);

        String expected = "Name: Ana\n" +
                          "ID: 1\n" +
                          "Birth Date: 12/07/2002\n" +
                          "Email Address: ana@hotmail.com\n" +
                          "Gender: female";

        GetProfileInfoController ctrl = new GetProfileInfoController(app);
        EmailAddress email = new EmailAddress("ana@hotmail.com");
        String result = ctrl.getProfileInfo(email);

        assertEquals(expected, result);
    }

# 4. Implementation

The implementation of this US was relatively straight forward and did not arise any problems. Something we had to take into account, though, was that we had to get the family of the person whose profile information we wanted retrieved using the main email, and then match the main email to the correct member of that family. 

# 5. Integration/Demonstration

While no other US is dependent on this one, it is dependent on others that create families [US010](US010_CreateFamily.md) and add family members to those families [US101](US101_AddFamilyMember.md). The method addMember was, then, particularly relevant to ensure the correct interactions between these.

    public boolean addMember(String name, int id, VAT vat, Date birthDate, EmailAddress emailAddress, Address address, String gender) {
        Person member = createPerson(name, id, vat, birthDate, emailAddress, address, gender);
        if (this.isIdInList(member))
            return false;
        members.add(member);
        return true;
    }

# 6. Observations

The main observation that sparked discussion between the group was whether there should be a person list of all people registered in the system or if it should only be present a list of families, each with their own list of its members. The team chose the last option, because we agreed that we should view this system as a true family finances manager, and the programming should also function as so.  



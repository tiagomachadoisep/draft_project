# US002
=======================================


# 1. Requirements
*As a System Manager, I want to get the standard categories tree.*

In this user story our group produced a method that enable the SM to visualize all the standard categories already created by himself as required in the [US001](US001_AddStandardCategory.md).

##1.1 Client Notes
For this US there is not relevant notes from the client.

##1.2 System Sequence Diagram

Next is the system sequence diagram realized for this US.

```plantuml
title SSD Get Standard Categories Tree
autonumber

hide footbox
actor "System Manager" as SM
activate SM

Participant System
SM -> System : get standard category tree
activate System
System --> SM : shows standard categories tree
deactivate System

deactivate SM
```

##1.3. Process view

```plantuml
title Process View
autonumber

hide footbox
actor "System Manager" as SM
activate SM

Participant UI
Participant "FFM_BusinessLogic" as BL 

SM -> UI : get standard category tree
activate UI

UI -> BL : getStandardCategoriesTree()
activate BL
BL --> UI : result
deactivate BL
UI-> SM : shows standard categories tree
deactivate UI

deactivate SM

```

##1.4. Connections with other US
The result of this US depends on the [US001](US001_AddStandardCategory.md), since without the creation of standard
categories there is no possibility to show the expected list, as it would show a blank objet.

On the other hand, [US110](US110_GetListOfCategoriesOnFamilyCategoryTree.md) is connected to this US and US001,
eventually others in the future, because the family's category tree includes all the standard categories.

# 2. Analysis
Through this US the SM pretends to acquire a list as a tree of the Standard Categories already created. This list is stored on the Category Service.

According to US001, these type of categories will have an ID, Name, Parent ID, and will be the basis to the creation of each family category tree.


# 3. Design

## 3.1. Functionality 
The SM sends the information to the UI that wants to see all the existing standard categories. As these are standard for all users there is no need to include arguments on the operation.

As said before, the responsibility to store the list of all standard categories is ascribed to the Category Service. 

The UI asks the controller for this information and, in its turn, this one inquires the FFMApplication about who has the information needed. After getting this knowledge, the controller asks the Category Service for the list of Standard Categories already created.

Finally, this object travels back to the SM following the reverse path.


```plantuml
@startuml
autonumber   

title **US002** - Get Standard Categories Tree

actor "System Manager" as SM
activate SM

SM -> ": UI" as UI : ask for standard categories tree

activate UI

UI -> ": Standard Categories Controller" as SCC : getStandardCategoriesTree()
activate SCC

SCC -> ": FFM Application" as AppMembers: getCategoryService()
activate AppMembers
AppMembers --> SCC : categoryService
deactivate AppMembers

SCC -> "categoryService : CategoryService" as CS: getStandardCategoriesListAsTree()
activate CS

ref over CS : getStandardCategoriesListAsTree()

CS --> SCC: listOfStandardCategoryDTO
deactivate CS

SCC --> UI : listOfStandardCategoryDTO
deactivate SCC

UI --> SM : show all Standard Categories as Tree
deactivate UI

deactivate SM
@enduml
```
Next we present in detail the method getStandardCategoriesListAsTree.

[**sub-diagrama » getStandardCategoriesListAsTree()**]
## 3.2. Class Diagram

In the following we present the classes involved in this US and the way they interact through the exhibition of a class diagram.
The classes implicated are:
- StandardCategoriesController
- FFMApp
- CategoryService

```plantuml
@startuml
title **US002** - Get Standard Categories Tree
class "StandardCategoriesTreeController" as controller {
- app : FFMApplication
+ getStandardCategoriesListAsTree()
}

class "FFMApplication" as app {
- categoryService : CategoryService
+ getCategoryService()
}

class "CategoryService" as CS {
- categoryList : List <Category>
+ getStandardCategoryListAsTree()
 - String getChildList(Category category)
 - String getChildTree (Category category)
 - boolean hasChildren(Category category)
}

controller - app : gives information <
app - controller : ask the information <
controller -- CS : ask >


@enduml
```

## 3.3. Applied Patterns
The patterns applied on this US are: 

**Controller** - concerning to the Standard Categories Tree Controller. It has the responsibility of handling and forward the request.

**Information Expert** - The FFM AppMembers knows who have the information we need, and the CategoryService class has the responsibility of storing the list of Standard categories.

**Low Coupling and High Cohesion** - By creating the CategoryService class we are lowering the dependencies and, therefore, increasing the cohesion.


## 3.4. Tests
Next we present tests we performed to guarantee the requirements of this US are achieved.

**Test 1**: Retrieving Standard Categories Tree successfully

    @DisplayName("Create Standard Categories Tree successfully as String")
    @Test
    void createStandardCategoriesTreeSuccessfullyAsString(){
        FFMApplication app = new FFMApplication();
        StandardCategoriesTreeController ctrl = new StandardCategoriesTreeController(app);
        CategoryService standardCategories = app.getCategoryService();

        standardCategories.addStandardCategory("Health");
        standardCategories.addStandardCategory("Insurances");
        standardCategories.addStandardSubcategory("Dentist", "1");
        standardCategories.addStandardSubcategory("Pharmacy", "1");
        standardCategories.addStandardSubcategory("Car", "2");
        standardCategories.addStandardSubcategory("House", "2");
        standardCategories.addStandardSubcategory("Honda", "2.1");
        standardCategories.addStandardSubcategory("Renault", "2.1");
        standardCategories.addStandardCategory("Sports");
        standardCategories.addStandardSubcategory("Husband", "1.1");

        String expected =
                "1.Health\n" +
                "1.1.Dentist\n" +
                "1.1.1.Husband\n" +
                "1.2.Pharmacy\n" +
                "2.Insurances\n" +
                "2.1.Car\n" +
                "2.1.1.Honda\n" +
                "2.1.2.Renault\n" +
                "2.2.House\n" +
                "3.Sports\n";

        //Act
        String result = ctrl.getStandardCategoriesListAsTree();

        //Assert
        assertEquals(expected, result);
    }

**Test 2**: Retrieving Standard Categories Tree with null Application


**Test 3**: Retrieving Standard Categories Tree without creating Standard Categories


# 4. Implementation
The biggest challenge during this functionality implementation was to capture the sub-categories of "level" three and subsequente sub-levels, i.e., categories that are at least grandchild of primary categories. 

In order to overcome this problem we used recursion. We start to go through the Category List searching for the main categories, verify if that has a child. We did this by comparing the parentId. After that, we recursively continue searching for other child categories.


# 5. Integration/Demonstration
At this moment, as referred before, there are no other US depending directly on this US002, so, no efforts were made to integrate the developed functionality.


# 6. Observations
Another way to implement this functionality is through the use of DTOs. Because this concept is not completely internalized the group chose not to use it.



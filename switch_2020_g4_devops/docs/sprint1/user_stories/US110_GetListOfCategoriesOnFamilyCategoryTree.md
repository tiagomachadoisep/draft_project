# US110
=======================================


# 1. Requirements

*As a family administrator, I want to get the list of the categories on the family's category tree.*

## 1.1 Client notes

This US states that a FA should be able to get the list of categories on the family's category tree.

The requirements specified by the PO which are relevant to US110 are stated as follows:

- The family's category tree refers to set of categories that are used to typify the transactions undertaken by FM, so
  that expenses may be analyzed by category or set of categories;
- SWS provides a standard category tree which can be customized by the family through the creation of new branches and
  leaves, or by creating new categories at the root level;
- There is only one FA per family;
- The same person may belong to more than one family, and therefore can be the family administrator of more than one family;
- A FA has to use distinct emails to access the different families he/she manages.

Therefore, in this US the FA has to be able to access the list of categories related to the family to which he/she is
associated through the email he/she has used to authenticate in the system.

## 1.2 Connection with other User Stories

A family's category list derives from the standard category tree provided by SWS. Therefore, this US is dependent
on [US001](US001_AddStandardCategory.md), in which the SM is able to create a standard category. It is also dependent
on [US010](US010_CreateFamily.md), where a SM creates a family, therefore initializing the family's list of categories
with the standard categories known by FFM APP.

This US contains the method getFamilyByAdminMainEmail(mainEmail), which is also used in other US that require the
validation of the FA ([US101](US101_AddFamilyMember.md), [US104](US104_ListOfMembersRelations.md)
, [US105](US105_CreateNewRelation.md) and [US120](US120_CreateFamilyCashAccount.md)).

# 2. Analysis

The requirements that were stated and analysed in the previous section have made it clear that some kind of validation
needs to take place in the designed solution. This validation will have to verify whether the user that accesses SWS is
the FA of the family he/she wishes to retrieve the category list from.

Considering that a user may be the family FA of more than one family, the solution will also have to find the family
that the FA wishes to analyze. Once such family is identified, the system will only have to retrieve the list of
categories of the family's category tree.
A family's category tree may contain one or more categories. On the other hand, a certain category may appear in many family category trees.

## 2.1 Relevant excerpt of the Domain Model

The following diagram contains an excerpt of the DM that illustrates the concepts related to this US.

```plantuml
@startuml
object "Family" as F
F : id
F : name
object "Category" as C
C : id
C : name
object "Person" as P
P : name
P : identification
P : vat
P : address
P : phoneNumberList
P : emailAddressList

C "*"--"*" F : used by 
C "*"-"1" C : child of 
F "*" *-  "1" P : administered by
F "*" *- "*" P : has members

@enduml
```

The diagram states that a category has an id and a name that is its designation. The association "is child of" means
that a category may be a subcategory, i.e. that it may have a parent category (e.g.: "
Dentist" can be a subcategory of a category named "Health"). This also means that a category may not have a parent
category, whether it's a standard or a customized category.

A family is characterized by an id and a name.

The analysis of this US could not be completed without defining what type of categories would belong to each family's
category tree. Since, according to the requirements, a family may customize the standard category tree by adding new "
branches" and "leaves", as well as root-level categories, both standard and non-standard categories should belong to the
family's category tree.

## 2.2 System Sequence Diagram

```plantuml
autonumber
title SSD Get List Of Categories On Family's Category Tree
actor "Family Administrator" as FA
activate FA
Participant System
activate System

FA -> System : get the list of categories
System --> FA : list of categories
deactivate System
deactivate FA
```

## 2.3 Process View Diagram

```plantuml
autonumber
title Process View of Get List Of Categories On Family's Category Tree
actor "Family Administrator" as FA
activate FA
Participant FFM_UI
Participant FFM_BusinessLogic
activate FFM_UI

FA -> FFM_UI : get the list of categories
FFM_UI -> FFM_BusinessLogic : getListOfCategories(userEmail)
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI: familyCategoryList
deactivate FFM_BusinessLogic
FFM_UI --> FA : list of categories
deactivate FFM_UI
deactivate FA
```

# 3. Design

## 3.1. Class Diagram

```plantuml
class "GetFamilyListOfCategories\n Controller" as CC {
- app: FFMApplication
 + GetFamilyListOfCategoriesController(FFMApplication app)
 + String getFamilyListOfCategories(EmailAddress userEmail)
 } 
class "FFMApplication" as APP {
- familyService: FamilyService
 + FFMApplication()
 + FamilyService getFamilyService()
}
class "FamilyService" as FS {
- families: List<Family>
 + FamilyService()
 + Family getFamilyByAdminMainEmail(EmailAddress mainEmail)
}
class "PersonService" as PS {
- mainEmailOfAdmin: EmailAddress
  + PersonService()
  + isMainEmailOfAdmin(EmailAddress mainEmail)
}
class "CategoryService" as CS {
- categoryList: List<Category>
 + CategoryService()
 + CategoryService(List<Category> standardCategoryList)
 + String getFamilyCategoryListAsString()
 - String getChildList(Category category)
 - String getChildTree (Category category)
 - boolean hasChildren(Category category)
}
class "Family" as F {
  - name: String
  - ID: int
  - registrationDate: Date
  - members: PersonService
  - familyCategoryService: CategoryService
  - cashAccount: CashAccount
  - familyRelationshipList: FamilyRelationService
   + Family(String name, Date registrationDate, int id, CategoryService categoryService)
   + PersonService getPersonService()
   + CategoryService getCategoryService()
}
class "EmailAddress" as EA {
  - email: String
   + EmailAddress(String email)
   + boolean equals(o)
}

CC - APP : asks for family service >
APP -- FS : selects service >
FS - F : asks for person service >
F -- PS : selects service
PS - EA : checks email address >
CC - F : asks for category service
F - CS : selects service > 
CC - CS : gets family's category list >

@enduml
```

## 3.2. Functionality

The SD in this section represents the functionality required by this US.

The FA interacts with the UI by requesting his/her family's category list (1). The UI sends this request to the
GetFamilyListOfCategoriesController (2). First it is necessary to find the family which is associated with the email of
the user. In order to do this, the controller class needs to connect to the family service which is responsible for
dealing with all families in the system. Therefore, the GetFamilyListOfCategoriesController asks FFM APP for the family
service (3) and FFM APP retrieves it (4).

Then the retrieved family service is responsible for finding the family related to the user's email, through the method
getFamilyByAdminMainEmail(mainEmail) (5). Once the family is found, the GetFamilyListOfCategoriesController is able to
ask that family its category service, which is responsible for managing the family's category list (10). This category
service is returned (11), and the GetFamilyListOfCategoriesController then retrieves the family's category list using
the method getFamilyListOfCategories(EmailAddress userEmail) (12).

The SD represents both the successful and scenarios of this US. If the getFamilyByAdminMainEmail(mainEmail) method is
able to retrieve a family, the next step consists in running the getFamilyListOfCategories(EmailAddress userEmail)
method and returning the category list (steps 13 to 15). If not, an error message will be transmitted to the FA through
the UI (steps 6 to 9).

```plantuml 
@startuml
autonumber
title **US110** - //Get List Of Categories On Family's Category Tree//
actor "Family Administrator" as FA
participant ":UI" as UI
participant "controller\n:GetFamilyListOfCategoriesController" as LCC
participant "app\n:FFMApplication" as AppMembers
participant "familyService\n:FamilyService" as FS
participant "categoryService\n:CategoryService" as CS

activate FA
FA -> UI : get the family's list of categories
activate UI
UI -> LCC : getFamilyListOfCategories(mainEmail)
activate LCC
LCC -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> LCC : familyService
deactivate AppMembers
LCC -> FS : getFamilyByAdminMainEmail(mainEmail)
activate FS
ref over FS
getFamilyByAdminMainEmail(mainEmail)
end ref
opt family not found
FS --> LCC : error message
deactivate FS
LCC --> UI : error message
UI --> FA : error message
else
LCC -> AppMembers : getCategoryService()
activate AppMembers
AppMembers --> LCC : categoryService
deactivate AppMembers
LCC -> LCC : getList(family, categoryService)
LCC -> CS : getFamilyCategoryDTOList(family)
ref over CS
getFamilyCategoryDTOList(family)
end ref
activate CS
opt exception is thrown
CS --> LCC : exception
LCC --> UI : empty list
UI --> FA : error message
else
CS --> LCC : familyCategoryDTOList
deactivate CS
LCC --> UI : familyCategoryDTOList
deactivate LCC
UI --> FA : family's list of categories 
deactivate UI
deactivate FA
end
@enduml
```

Two detail SD were designed to illustrate how the application internally processes the flow depicted in the macro SD.

The family service contains information about all the families in the system, therefore it is responsible for finding
the family that corresponds to the email used by the FA to authenticate in the system. This is possible since, as was
mentioned in the requirements section, the FA has to use different emails to manage the finances of different families.
Therefore, an email is uniquely associated with the combination of a family and a FA. This part of the process is
accomplished by the method getFamilyByAdminMainEmail(mainEmail), which is detailed below.

```plantuml
@startuml
autonumber
title //getFamilyByAdminMainEmail(mainEmail)//
participant "familyService:\nFamilyService" as FS
participant "families:\nList<Family>" as LF
participant "family:\nFamily" as F
participant "personList:\nPersonList" as PL
participant "emailAddress:\nEmailAddress" as EA

[o-> FS : getFamilyByAdminMainEmail(mainEmail)
activate FS
loop for each family in families
FS -> F : getPersonList()
activate F
F -> PL : isMainEmailOfAdmin(mainEmail)
activate PL
PL -> EA : sameEmail(emailAddress)
activate EA
EA --> PL : result
deactivate EA
opt result == true
PL --> F : result
deactivate PL
F --> FS : result
deactivate F
[<-- FS : family
end
end
[<-- FS : exception
deactivate FS
@enduml
````

This method is used in any US in which it is necessary to validate if the authenticated user is the FA of any family, so
that the corresponding family is returned.

Since the FFM APP holds information about all the families registered in the system, it is responsible for finding the
family related to the FA. Therefore, a loop cycle iterates through each family in the family service, listed in the
families array list (families:List<Family>). This is accomplished by asking each family in the list of families their
corresponding person service(2), which is responsible for managing the list of members of that family. The person
service has an attribute called mainEmailOfAdmin, which is the main email address of the FA of that family. Next, the
retrieved person service (3) is used to apply the method isMainEmailOfAdmin(emailAddress) (4), which in turn checks if
the provided email address is equal to the attribute mainEmailOfAdmin. This verification is made with an equals method
applied in the EmailAddress class (5). If the equals method returns true, the loop ends and the found family is returned
to LCC (steps 7 to 9). However, if by the end of the loop no true result is returned, an error message should be
returned to LCC (steps 10 to 12).

Once the family object has been identified, it is possible to obtain the list of categories belonging to that family's
category tree. The method getFamilyListOfCategories(EmailAddress userEmail) is called by LCC and applied in the family's
category service, as was explained in the macro SD.

This detail is represented in the following SD of the getCategoryList() method:

```plantuml
@startuml
autonumber
title //getFamilyCategoryDTOList(family)//
participant "categoryService:\nCategoryService" as CS
participant "categoryList:\nList<Category>" as CL
participant "familyCategoryDTOList:\nList<CategoryDTO>" as FCDL
participant "category:\nCategory" as C
participant "categoryDTO:\nCategoryDTO" as CDTO

[o-> CS: getFamilyCategoryDTOList(family)
activate CS
loop for each category in categoryList
CS -> C : isAtRootLevel() && canBeUsedByFamily(family)
activate C
C --> CS : result
deactivate C
opt result == true
CS -> C : toDTO()
activate C
C -> CDTO ** : create(id, name)
activate CDTO
CDTO --> C : categoryDTO
deactivate CDTO
C --> CS : categoryDTO
deactivate C
CS --> FCDL ** : add(categoryDTO)
activate FCDL
ref over CS
getChildDTOList(category, family)
end
opt child list is not empty
CS -> FCDL : addAll(getChildDTOList(category, family))
deactivate FCDL
end
end
end
[<-- CS : familyCategoryDTOList
@enduml
````

In the category service of each family, for each category at the root level (i.e., category that has no parent id), its
name is retrieved (steps 2 and 3)  and it is verified if it has subcategories (4). If the result (5) is true, then
another loop is performed for each child in the category's children list. At each iteration of the loop, the name of the
child's category is returned (steps 6 and 7). In case this category has subcategories (8), the method getChildTree(
category) is applied in order to retrieve that category's child tree (10). This last method (10) is implemented in a
recursive way, as long as there are subcategories to retrieve.

## 3.3. Applied Patterns

Different software development patterns were applied in the analysis and design steps of this US:

**Pure Fabrication:** the FFMApplication class resulted from this principle, and helped in providing a high cohesion and
low coupling.

**Controller:** the GetFamilyListOfCategoriesController class follows this design pattern, in which an intermediate
entity establishes a connection between the UI and the internal processes.

**High Cohesion and Low Coupling:** we have tried to respect this principle by creating a category service and person
service for each family, so that each family has their own dedicated services instead of accessing those services
directly from FFM AppMembers. This way, FFM APP was able to delegate these responsibilities to each family.

**Information Expert:** by stating that FFM APP has the information about all families in the system, and that each
family has their own CategoryService that knows and manages the categories on the family's category tree, as well as
their own PersonService that manages the information about the family's members such as the FA's main email, which is
used to validate his/her request to access the family's category list.

**Ask, Don't Tell:** within the method getFamilyByAdminMainEmail(mainEmail), instead of asking directly the person
service for the FA's main email, it is asked whether the provided email address is equal to the FA's main email (step 4
of detail diagram getFamilyByAdminMainEmail(mainEmail)).

## 3.4. Tests

In this section some tests will be presented that guarantee the requirements of the US are applied. The tests are
explained for each class involved in this US.

These tests were applied to the controller GetFamilyListOfCategoriesController. They assess whether it's possible to
initialize the controller with a valid and an invalid initial state (a not null and null application, respectively). The
other tests cover two successful scenarios and two unsuccessful scenarios. The successful scenarios refer to a FA being
able to retrieve the family category list when it only has standard categories and subcategories, and when it also has
customized categories. The unsuccessful cases pertain to an empty list being returned when errors are caught, related to
the list of categories actually being empty and the user trying to access this list being a Family Member instead of a
FA.

**Test 1** : Test fails because app is null.

**Test 2** : Test succeeds because app is not null.

**Test 3** : Check if only standard categories and subcategories are retrieved in the family's category list when no
customized categories were inserted.

**Test 4** : Check if both standard and customized categories and subcategories are returned to two different families.

**Test 5** : Check if empty list is returned when the family's category list is empty.

**Test 6** : Check if an empty list is returned when a family member tries to access the family's category list.

In FamilyService, the following tests wer made to check successful and unsuccessful cases in finding a family using the
user's main email:

**Test 7** : Check if it is possible to retrieve a family based on the user's email by comparing it to the main email of
that family's FA.

**Test 8** : Check if it is possible to retrieve a family based on the user's email by comparing it to the main email of
that family's FA, when the user is the FA of more than one family.

**Test 9** : Check if it is not possible to retrieve a family based on the user's email by comparing it to the main
email of that family's FA, because the user is not a FA of any family.

Tests from 10 to 16 refer to class CategoryService, in which tests 13 and 14 relate to an auxiliary method called
getFamilyByCategoryId, and tests 15 to 16 to another auxiliary method named getCategoryArrayList.

**Test 10** : Check if only standard categories and subcategories are retrieved in the family's category list when no
customized categories were inserted.

**Test 11** : Check if standard and customized categories and subcategories are retrieved in the family's category list.

**Test 12** : Check if error is thrown when the family's category list is empty.

**Test 13** : Check if it is possible to return a category in a family's category tree by its id, whether it's a
standard or a customized category/subcategory.

**Test 14** : Check if errors are thrown when trying to return a non-existing category in a family's category tree by
its id, whether it's a standard or a customized category/subcategory.

**Test 15** : Check if category list in CategoryService is returned.

**Test 16** : Check if empty list is returned when the category list in CategoryService is empty.

In class Category the methods canBeUsedByFamily and toDTO had to be tested:

**Test 17** : Checks that a standard category can be used by a family.

**Test 18** : Checks that a family category can be used by that family.

**Test 19** : Checks that a family category cannot be used by another family.

**Test 20** : Check if a standard category converts to DTO.

**Test 21** : Check if a standard subcategory converts to DTO.

**Test 22** : Check if a customized category converts to DTO.

**Test 23** : Check if a customized subcategory converts to DTO.

# 4. Implementation

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade
com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração)
relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integration/Demonstration

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*


# 6. Observations

This US led to debate among the team members due to some uncertainties that were felt at the beginning of the project.
For instance, at this point of the project it is not yet clearly defined how the family will be able to customize the standard category tree provided by SWS.
It is also not clear yet if it is possible to create new categories that are at the same level of the base categories.
It is only known that the base categories cannot be changed by the family and that it will be possible to add new "branches" and "leaves" of categories.

For this sprint, considering the known US so far, it was determined that the category tree of each family would comprise standard categories as well as new additions to the family, and that it would be accessible as an array list from the Family class, as it is specific of each family.
The inclusion of both standard and non-standard categories in the same list implies that, at some moment, the standard categories known and listed by FFM APP as standardCategories (according to the CD) will have to be known and listed as well by the Family class through its listOfCategories.

In order to achieve this, it was determined that each time a SM created a family, its category list would be initialized with the standard categories known by the application. *inicialização: criar uma cópia ou remeter para a lista original?*
In case the SM creates a standard category after the family is created, this new standard category is automatically added to each family's category tree.

Later, whenever a family wishes to add a new category, it will be added to that family's category tree. This is, however, a hypothetical scenario that may appear in future sprints.
Other possible future scenarios include the need to remove or change the name of a standard category by a SM, or to remove or change the name of a customized category by the family, in which case such alterations should be mirrored in the family's category tree.

*alternativas?*

In conclusion, we observe that in this US we had to employ and develop brainstorming skills while choosing the most adequate solution considering all known requirements and possible future scenarios.
We have also identified a method that is used by different US (getFamily(userEmail)), and realized that it should be included in the utilities package as well as other methods.




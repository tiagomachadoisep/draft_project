# US104
=======================================

# 1. Requirements

*As a family administrator, I want to get the list of family members and their relations.*

## 1.1 Client notes

The following notes were acquired through meetings with the client:

- The client wants the list of family members to be retrieved as well as their relationships, if they exist.

- It is not mandatory that a family member has a relationship.

- The client suggested that the retrieved list could only show the relationship that the members have with the
family administrator but that would force the relationships to be transformed in relation to the family
  administrator.

As we interpret, the relationships retrieved should be the ones that the family members have in relation with the family administrator.
Therefore, we expect the final list to be a matrix of two columns maximum. One to list the family members, and the
other to list their relationship with the family administrator.

The family administrator should have the default relationship assigned to him as "No Relationship" on the retrieved list, 
because it is not possible to create a relationship with itself.

## 1.2 System Sequence Diagram

```plantuml 
autonumber
title **US104** Get the list of members and their relations
actor "Family Administrator" as FA
participant "System" as Sys
FA -> Sys : get the list of members and their relations
activate Sys
Sys --> FA : returns list
deactivate Sys
```

## 1.3 Process View

```plantuml 
autonumber
title **US104** Process View
actor "Family Administrator" as FA
participant ": FFM_UI" as UI
participant ": FFM_BusinessLogic" as BL
FA -> UI : get list of members and their relations
activate UI
UI -> BL : getMatrixOfMembersAndRelations(mainEmail)
activate BL
BL --> UI : matrixOfMembersAndRelations
deactivate BL
UI --> FA : returns list
deactivate UI
```

## 1.4 Connections with other User Stories

The creation of a family member and the creation of a relationship between family members are described in 
[US101](\US101_AddFamilyMember.md) and [US105](\US105_CreateNewRelation.md) respectively, being US104 partly
dependent on them since without the possibility of creating family members and relationships, the retrieved list would 
be empty. Nevertheless, the list can only be retrieved when a family with a family administrator exists, so the
order for the list retrieval can be issued. Also, if there are no members in the family except for the family administrator, 
only him will appear in the retrieved list.

[US105](\US105_CreateNewRelation.md) mentions the creation of a relationship between two family members, but this relationship will not be represented 
on the expected list retrieved in US104 as long as it defines a relationship where the family administrator does not take part in.
If a member does not have a relationship with the family administrator, the default relationship designation "No Relationship" will
be present following the family member name.

For [US011](\US011_createFamilyAdmin.md), the client wants one family administrator per family. Although a user can be a family administrator of different families,
 since he needs to be logged in with a different email for each family, the list retrieved will belong to the family on 
which the family administrator is logged on.

# 2. Analysis

## 2.1 List retrieval

As the actor is the family administrator, a list with family members and their relations will always be retrieved,
since a family administrator can only exist if a family exists, even if the only member of the family is the family administrator.

## 2.2 Relevant excerpt of the domain model for US104

```plantuml 
@startuml Domain Model
object "Family" as fam {
id
name
}
object Person {
id
name

}
object "FamilyRelationships" as FR {
designation
}
fam "1" - "*" Person : has members >
fam "1" - "1" Person : has admin >
Person "1" ---- "*" FR : to admin
Person "1" ---- "*" FR : of admin
fam "1" - "*" FR : exists


@enduml
```

# 3. Design

## 3.1. Functionality Realization

1. The ListMembersAndRelationsController will be responsible for calling the correct service present in the FFM AppMembers Class;
2. This service, called FamilyService, holds a list of families within the FFM AppMembers. This service will then use the email of the user
which requested the list of members and relations and correctly identify to which family he belongs to. 
   After finding the correct family, the request is forwarded to that family.  
3. The Family class will be responsible for creating a temporary list (matrix) where the family member names and relationships
will be stored and returned; 
4. The Family Class will call its PersonList which holds all family members of the family and extract the name of each one.
5. For every iteration on each family member, the Family Class will call its FamilyRelationshipList, which holds a list of 
   relationships between family members of that family. Here, a loop searches for relations between each member of the family 
   and the family administrator.
6. If a relationship is found, the name of the relation is extracted and added next to the family member name in the matrix to be returned.
   If a relationship is not found between a family member and the family administrator, the default relation "No Relationship"
   will be added to the matrix next to the family member name.
7. The Family class will return this temporary matrix to the ListMembersAndRelationsController to return it to the UI.

NOTE: This is not a good implementation. Check the Observations section for details.
    
Below are the Sequence Diagrams for US104:
       

```plantuml 
@startuml
autonumber
title **US104** Get the list of members and their relations
actor "Family Administrator" as FA
participant ":UI" as UI
participant ":ListMembersAndRelations\nController" as MRC
participant "app :\nFFMApplication" as AppMembers
participant "familyService :\nFamilyService" as FS
participant "family :\nFamily" as F
activate FA
FA -> UI : Get member list and relations
activate UI
UI -> MRC : getMatrixOfMembersAndRelations(mainEmail)
activate MRC
MRC -> AppMembers : getfamilyService()
activate AppMembers
AppMembers --> MRC : familyService
deactivate AppMembers
MRC -> FS : getFamilyByAdminMainEmail(mainEmail)
activate FS
FS -> MRC : family
deactivate FS
MRC -> F : getMatrixOfMembersAndRelations()
activate F
F --> MRC : matrixOfMembersAndRelations
deactivate F
MRC --> UI : matrixOfMembersAndRelations
deactivate MRC
UI --> FA : Member list and relations
deactivate UI
@enduml
```

Going into more detail in the getMatrixOfMembersAndRelations() method of the Family Class:

```plantuml
@startuml
autonumber

participant "family :\nFamily" as F
participant "members :\nPersonList" as PL
participant "member :\nPerson" as P
participant "familyRelationshipList :\nFamilyRelationshipList" as FRL
participant "familyRelationship :\nFamilyRelationship" as FR
participant "familyRelationType :\nFamilyRelationType" as FRT
participant "memberNameAndRelation : String[]" as Array
participant "matrixOfMembersAndRelations : List<String[]>" as TempMatrix

[o-> F : getMatrixOfMembersAndRelations()
activate F
F -> TempMatrix ** : create()

loop for each member in members
F -> P : getName()
activate P
return memberName

    loop for each familyRelationship in familyRelationshipList
        alt if (isThisPersonOne(FA) && isThisPersonTwo(member)
            alt if (member.isGenderMale())
                F -> FRT : getMaleInverseRelationType()
                activate FRT
                FRT -> FRT : getDesignation()
                FRT -> F : relationDesignation
                deactivate FRT
            end
            alt if (member.isGenderFemale())
                F -> FRT : getFemaleInverseRelationType()
                activate FRT
                FRT -> FRT : getDesignation()
                FRT -> F : relationDesignation
                deactivate FRT
            end
        end
        alt if (isThisPersonTwo(FA) && isThisPersonOne(member))
            F -> FRT : getDesignation()
            activate FRT
            FRT -> F : relationDesignation
            deactivate FRT
        end
    end
    
F -> Array ** : create(memberName, relationDesignation)
F -> TempMatrix : add(memberNameAndRelation)
end

[<-- F : matrixOfMembersAndRelations
deactivate F
@enduml
```



## 3.2. Class Diagram

```plantuml
class "ListMembersAndRelations \n Controller" as LMRC {
 - app : FFMApplication;
 + List<String[]> getMatrixOfMembersAndRelations(String mainEmail)
 } 
class "FFMApplication" as APP {
- familyService : FamilyService
 + FamilyService getFamilyService()
}

class "FamilyService" as FS {
 - families : List<Family>
 + Family getFamilyByAdminMailEmail(String mainEmail)
}

class "Family" as F {
  - members : PersonList
  - familyRelationshipList : FamilyRelationshipList
   + List<String[]> getMatrixOfMembersAndRelations()
}
class "PersonList" as PL {
- members : List<Person>
- mainEmailOfAdmin : EmailAddress
+ List<Person> getMembers()
+ EmailAddress getMailEmailOfAdmin()
}

class "Person" as P {
- name : String
+ boolean hasMainEmail(String mainEmail)
+ String getName()
+ boolean isGenderMale()
+ boolean isGenderFemale()
+ boolean equals(Object o)
}

class "FamilyRelationshipList" as FRL {
- listOfRelationships : List<FamilyRelationship>
 + List<FamilyRelationship> getListOfRelationships()
}

class "FamilyRelationship" as FR {
- personOne : Person
- personTwo : Person
- relationType : FamilyRelationType
+ boolean isThisPersonOne()
+ boolean isThisPersonTwo()
+ FamilyRelationType getRelationType()
}

class "FamilyRelationType" as FRT {
- String designation
- FamilyRelationType maleInverseRelationType
- FamilyRelationType femaleInverseRelationType
+ String getDesignation()
+ FamilyRelationType getMaleInverseRelationType()
+ FamilyRelationType getFemaleInverseRelationType()
}

LMRC -- APP : selects familyService >
APP -- FS : selects family >
FS -- F : selects personList, familyRelationshipList >
F -- PL : selects person >
PL -- P: asks for name >
F -- FRL : selects familyRelationship >
FRL -- FR : asks for personOne, personTwo, relationType >
FR -- FRT : has > 
FR - P : has >
@enduml
```



## 3.3. Applied Patterns

**Information Expert:** We assigned responsibility to the information expert. The Family Class is responsible for 
creating the temporary matrix, search its own list of members and their relationships within
its PersonList and FamilyRelationshipList and place each member and its relationship in the temporary matrix, 
which will then be returned to the ListMembersAndRelationsController.

**Controller:** we created the ListMembersAndRelationsController whose only purpose is to build a "bridge" between
the UI and the FFMApplication, with each task needed being the responsibility of the
controller, since each class should have one responsibility **(Single Responsibility Principle)**.

**Pure Fabrication:** this pattern is applied through the creation of the class FFMApplication, not part of the
concepts of the domain model but essential to coordinate the data and functions of it.

**High Coesion & Low Coupling:** Through the creation and assignment of single responsibilities to each class
by applying the above mentioned GRASP, a high coesion and low coupling between classes is achieved.

## 3.4. Tests

### 3.4.1 ListMembersAndRelationsController class tests:
#### 3.4.1.1 Tests for the getMatrixOfMembersAndRelations() method:

**Test 1:** Check the retrieval of the list of members and relations of a family, called by a FA.
    
    @Test
    @DisplayName("Successful retrieval of the list of Members and Relations")
    void getMatrixOfMembersAndRelations_FA_callOne() {
        FFMApplication ffmApplication = new FFMApplication();
        ListMembersAndRelationsController listMembersAndRelationsController = new ListMembersAndRelationsController(ffmApplication);

        Family familySantos = new Family("Santos", 0);
        Family familyGuedes = new Family("Guedes", 1);
        ffmApplication.getFamilyService().addFamilyIfNotExists(familySantos);
        ffmApplication.getFamilyService().addFamilyIfNotExists(familyGuedes);
        String pedroName = "Pedro";
        String ruiName = "Rui";
        String joanaName = "Joana";
        String pedroEmailAddress ="epe1@hotmail.com";
        String joanaEmailAddress = "epe3@hotmail.com";
        String ruiEmail = "epe2@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date ruiBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        Date joanaBirthdate = new GregorianCalendar(1991, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName, "19741658 6ZV2", 0, pedroBirthdate, pedroEmailAddress, null, genderMale);

        familyGuedes.getPersonList().addAdministrator(ruiName, "19913200 3ZY1", 0, ruiBirthdate, ruiEmail, null, genderMale);
        familyGuedes.getPersonList().addMember(joanaName, "18480344 6ZY2", 0, joanaBirthdate, joanaEmailAddress, null, genderFemale);
        FamilyRelationType husband = new FamilyRelationType("husband");
        FamilyRelationType wife = new FamilyRelationType("wife");
        husband.setFemaleInverseRelationType(wife);
        wife.setMaleInverseRelationType(husband);
        familyGuedes.getFamilyRelationshipList().addRelation(familyGuedes.getPersonList().getMembers().get(0),
                familyGuedes.getPersonList().getMembers().get(1), husband);

        String[] relationOne = {"Rui", "No Relationship"};
        String[] relationTwo = {"Joana", "wife"};
        List<String[]> expected = new ArrayList<>();
        expected.add(relationOne);
        expected.add(relationTwo);

        //act
        List<String[]> result = listMembersAndRelationsController.getMatrixOfMembersAndRelations(ruiEmail);

        //assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertNotSame(expected, result);


    }

**Test 2:** Check the retrieval of the list of members and relations of a family, called by a FA, 
when he belongs to different families.

**Test 3:** Check the unsuccessful retrieval of the list of members and relations of a family, called by a FM.

### 3.4.2 Family class tests:
#### 3.4.2.1 Test for the getMatrixOfMembersAndRelations() method:

**Test 1:** This test checks the retrieval of the intended list for the following cases:

- The FA has relationships with other FMs where the FA is the personTwo in the relation. The relation will appear.
- The FA has relationships with other FMs where the FA is the personOne in the relation. The relation will appear.
- There is a relationship between two FMs. The output relation will be "No Relationship".
- There is a FM without any relationship. The output relation will be "No Relationship".
- The FA has no relationship with himself. The output relation will be "No Relationship".

      @Test
      @DisplayName("List of Members and Relations")
      void getMatrixOfMembersAndRelations_successForEveryPossibleRelationSituation() {
            //arrange
            Family family = new Family("Santos", 0);
    
            String pedroName = "Pedro";
            String joaoName = "Joao";
            String mariaName = "Maria";
            String joanaName = "Joana";
            String ritaName = "Rita";
            String filipaName = "Filipa";
            String ricardoName = "Ricardo";
            String joseName = "Jose";
            Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
            Date joaoBirthdate = new GregorianCalendar(1956, Calendar.AUGUST, 28).getTime();
            Date mariaBirthdate = new GregorianCalendar(1954, Calendar.AUGUST, 28).getTime();
            Date joanaBirthdate = new GregorianCalendar(1992, Calendar.AUGUST, 28).getTime();
            Date ritaBirthdate = new GregorianCalendar(1995, Calendar.AUGUST, 28).getTime();
            Date filipaBirthdate = new GregorianCalendar(1995, Calendar.AUGUST, 28).getTime();
            Date ricardoBirthdate = new GregorianCalendar(1995, Calendar.AUGUST, 28).getTime();
            Date joseBirthdate = new GregorianCalendar(1995, Calendar.AUGUST, 28).getTime();
            String pedroEmailAddress = "reer1@hotmail.com";
            String joaoEmailAddress = "reer2@hotmail.com";
            String mariaEmailAddress = "reer3@hotmail.com";
            String joanaEmailAddress = "reer4@hotmail.com";
            String ritaEmailAddress = "reer5@hotmail.com";
            String filipaEmailAddress = "reer6@hotmail.com";
            String ricardoEmailAddress = "reer7@hotmail.com";
            String joseEmailAddress = "reer8@hotmail.com";
            String genderMale = "male";
            String genderFemale = "female";
            family.getPersonList().addAdministrator(pedroName, "19741658 6ZV2", 0, pedroBirthdate, pedroEmailAddress, null, genderMale);
            family.getPersonList().addMember(joaoName, "11054247 9ZV0", 0, joaoBirthdate, joaoEmailAddress, null, genderMale);
            family.getPersonList().addMember(mariaName, "19913200 3ZY1", 0, mariaBirthdate, mariaEmailAddress, null, genderFemale);
            family.getPersonList().addMember(joanaName, "14175618 7ZY9", 0, joanaBirthdate, joanaEmailAddress, null, genderFemale);
            family.getPersonList().addMember(ritaName, "19474601 1ZV9", 0, ritaBirthdate, ritaEmailAddress, null, genderFemale);
            family.getPersonList().addMember(filipaName, "15665777 5ZW3", 0, filipaBirthdate, filipaEmailAddress, null, genderFemale);
            family.getPersonList().addMember(ricardoName, "13652209 2ZZ2", 0, ricardoBirthdate, ricardoEmailAddress, null, genderMale);
            family.getPersonList().addMember(joseName, "19579142 8ZX2", 0, joseBirthdate, joseEmailAddress, null, genderMale);
            FamilyRelationType son = new FamilyRelationType("son");
            FamilyRelationType father = new FamilyRelationType("father");
            FamilyRelationType brother = new FamilyRelationType("brother");
            FamilyRelationType mother = new FamilyRelationType("mother");
            FamilyRelationType sister = new FamilyRelationType("sister");
            son.setMaleInverseRelationType(father);
            son.setFemaleInverseRelationType(mother);
            mother.setMaleInverseRelationType(son);
            brother.setFemaleInverseRelationType(sister);
            sister.setMaleInverseRelationType(brother);
    
            Person pedro = family.getPersonList().getPersonByMainEmail(pedroEmailAddress);
            Person joao = family.getPersonList().getPersonByMainEmail(joaoEmailAddress);
            Person maria = family.getPersonList().getPersonByMainEmail(mariaEmailAddress);
            Person joana = family.getPersonList().getPersonByMainEmail(joanaEmailAddress);
            Person rita = family.getPersonList().getPersonByMainEmail(ritaEmailAddress);
            Person filipa = family.getPersonList().getPersonByMainEmail(filipaEmailAddress);
            Person ricardo = family.getPersonList().getPersonByMainEmail(ricardoEmailAddress);
            Person jose = family.getPersonList().getPersonByMainEmail(joseEmailAddress);
    
            family.getFamilyRelationshipList().addRelation(pedro, joao, son);
            family.getFamilyRelationshipList().addRelation(pedro, rita, brother);
            family.getFamilyRelationshipList().addRelation(maria, pedro, mother);
            family.getFamilyRelationshipList().addRelation(joana, pedro, sister);
            family.getFamilyRelationshipList().addRelation(jose, filipa, brother);
    
            String[] relationOne = {"Pedro", "No Relationship"};
            String[] relationTwo = {"Joao", "father"};
            String[] relationThree = {"Maria", "mother"};
            String[] relationFour = {"Joana", "sister"};
            String[] relationFive = {"Rita", "sister"};
            String[] relationSix = {"Filipa", "No Relationship"};
            String[] relationSeven = {"Ricardo", "No Relationship"};
            String[] relationEight = {"Jose", "No Relationship"};
            List<String[]> expected = new ArrayList<>();
            expected.add(relationOne);
            expected.add(relationTwo);
            expected.add(relationThree);
            expected.add(relationFour);
            expected.add(relationFive);
            expected.add(relationSix);
            expected.add(relationSeven);
            expected.add(relationEight);
    
            //act
            List<String[]> result = family.getMatrixOfMembersAndRelations();
    
            //assert
            assertArrayEquals(expected.toArray(), result.toArray());
            assertNotSame(expected, result);

        }

# 4. Implementation

The implementation of this User Story was dependent on how the class Family, Person,
 FamilyRelationship, and FamilyRelationType were implemented, since it needed to access information in each one and know how to retrieve it.

During Sprint01, changes on the above mentioned classes would impact the methods and respective tests 
developed for this User Story, which demanded our constant attention to correct possible changes without negatively impacting the 
already established code.

During Sprint02, structural changes were made, especially on how the relation types are defined.
Before, we had strings representing relation types as an attribute, but that attribute was promoted to its own class. 
This facilitated
relationships construction and information retrieval. Check [US105](\US105_CreateNewRelation.md) for more details.

On the Observations section, a problem regarding a method is described.


# 5. Integration/Demonstration

The implementation of this User Story makes use of the not yet developed authentication procedure that validates the acting user
to make an allowed call for this functionality. Thus, this functionality needs the email address of the user that is currently
logged in to verify if the user is the family administrator of the family to which his email address is linked to. 
If the user is not the family administrator of the family to which he belongs, he cannot call this functionality.


The functionality developed is further enhanced by how the family relations are created. As described in [US105](\US105_CreateNewRelation.md),
upon creation of a family relationship between two family members, it is possible to know the inverse relationship between 
members.
This ensures that if the family administrator has a relationship with a member, then the relationship designation that
the member has with the family administrator is also known. This facilitates the lookup for a valid existing relationship
while facilitating the family relationships construction by the family administrator, as he only needs to define a
relation between two members in one way.

The following test checks for the retrieval of the list of members and relations of the correct family
when a user is the family administrator in two different families but is only currently logged in of them. 
The test starts since the controller creation:

      /**
     * This test checks the retrieval of the list of members and relations of a family, called by a FA, when he belongs to different families.
     */
    @Test
    @DisplayName("Successful retrieval of the list of Members and Relations - FA has two families but is logged in FamilyTwo")
    void getMatrixOfMembersAndRelations_FA_callTwo() {
        FFMApplication ffmApplication = new FFMApplication();
        ListMembersAndRelationsController listMembersAndRelationsController = new ListMembersAndRelationsController(ffmApplication);

        Family familySantos = new Family("Santos", 0);
        Family familyGuedes = new Family("Guedes", 1);
        ffmApplication.getFamilyService().addFamilyIfNotExists(familySantos);
        ffmApplication.getFamilyService().addFamilyIfNotExists(familyGuedes);
        String pedroName = "Pedro";
        String joanaName = "Joana";
        EmailAddress pedroEmailAddressOne = new EmailAddress("epe1@hotmail.com");
        EmailAddress pedroEmailAddressTwo = new EmailAddress("epe2@hotmail.com");
        EmailAddress joanaEmailAddress = new EmailAddress("epe3@hotmail.com");
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date joanaBirthdate = new GregorianCalendar(1991, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName, 1, null, pedroBirthdate, pedroEmailAddressOne, null, genderMale);
        familyGuedes.getPersonList().addAdministrator(pedroName, 1, null, pedroBirthdate, pedroEmailAddressTwo, null, genderMale);

        familyGuedes.getPersonList().addMember(joanaName, 2, null, joanaBirthdate, joanaEmailAddress, null, genderFemale);
        FamilyRelationType husband = new FamilyRelationType("husband");
        FamilyRelationType wife = new FamilyRelationType("wife");
        husband.setFemaleInverseRelationType(wife);
        wife.setMaleInverseRelationType(husband);
        familyGuedes.getFamilyRelationshipList().addRelation(familyGuedes.getPersonList().getMembers().get(0),
                familyGuedes.getPersonList().getMembers().get(1), husband);

        String[] relationOne = {"Pedro", "No Relationship"};
        String[] relationTwo = {"Joana", "wife"};
        List<String[]> expected = new ArrayList<>();
        expected.add(relationOne);
        expected.add(relationTwo);

        //act
        List<String[]> result = listMembersAndRelationsController.getMatrixOfMembersAndRelations(pedroEmailAddressTwo);

        //assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertNotSame(expected, result);


    }

# 6. Observations

Further functionalities will need to be implemented to ensure that the actor of US104 is the only one authorized
to call for the functionality developed, such as the user authentication procedure mentioned in the above section.

During Sprint02, it was necessary to change how the relation types are defined (from Strings to objects of its own class).
Thus, some corrections needed to be made to the methods developed for this US104 and respective tests.

**IMPORTANT:** Later on sprint02, it was acknowledged that the method getMatrixOfMembersAndRelations() in 
the Family class is not well implemented. It has an overly complex logic that is a result of not respecting the 
"*Tell, Don't Ask*" principle. It must be revised and reworked as soon as possible. This was the result of 
adapting an old method to the structural changes that were implemented during sprint02 without proper RDD
validation.




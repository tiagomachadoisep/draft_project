```plantuml
@startuml
autonumber
title US080 - Login
actor "User" as user
participant " UI (React)" as ui
participant ": AuthenticationController" as ctrl
participant "response\n: JWTResponse" as response
participant ": JwtService" as serv
participant ": IPersonRepositoryJPA" as repo
participant ": AuthorizationServer" as auth

activate user
user -> ui : introduce login data\n(username, password)
activate ui
ui -> ctrl : POST /authenticate\n{username,password}
activate ctrl
ctrl -> serv : loadUserByUsername(username)
activate serv
serv -> repo : findById(username)
activate repo
repo --> serv : user
deactivate repo
serv --> ctrl : user
deactivate serv
ctrl -> ctrl : authenticate(username,password)
ctrl -> ctrl : token=generateJWTtoken(user) 
ctrl --> response* : create(token)
ctrl --> ui : 200 {response}
deactivate ctrl
ui --> user : login successful
deactivate ui
user -> ui : get list of families
activate ui
ui -> auth: GET /families\n{Authorization: "Bearer "+token}
activate auth
auth -> auth : check=validateToken(token)
alt check==true
auth -> : executeRequest(userDetails,request)
auth --> ui : 200 listOfFamilies
else
auth --> ui : 401 Unauthorized
deactivate auth
end
ui --> user : response
deactivate ui
deactivate user

@enduml
```
```plantuml
@startuml
autonumber
title US003 - Import Categories
actor "System Manager" as SM
participant ": ImportCategoriesController" as ctrl
participant ": CategoryService" as serv
participant ": ICategoryRepositoryHttp" as repo
participant "ExternalAPIURL" as api
participant ": AssemblerJSONtoDTO" as assembler

activate SM
SM -> ctrl : import categories
activate ctrl
ctrl -> serv : importCategories()
activate serv
serv -> repo : importCategories()
activate repo
repo -> api : GET request
activate api
api --> repo : categoriesJSON
deactivate api
repo -> assembler : jsonToDTO(categoriesJSON)
activate assembler
assembler --> repo : categoriesDTO
deactivate assembler
repo --> serv : categoriesDTO
deactivate repo
serv --> ctrl : categoriesDTO
deactivate serv
ctrl --> SM : categoriesDTO
deactivate ctrl
deactivate SM
@enduml
```
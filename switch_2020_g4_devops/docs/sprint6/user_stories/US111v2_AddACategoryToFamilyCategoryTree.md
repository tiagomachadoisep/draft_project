# 3. Design

## 3.1. Class Diagram

```plantuml
@startuml
skinparam linetype polyline

title US111v2 - Add a Custom Category To the Family's Category Tree Diagram

interface "IAddCustomCategoryControllerV2" as ICTRL {

}

class "AddCustomCategoryControllerV2" as CTRL {
+ addCustomSubcategoryV2(id, inputCategoryDTO)
+ isSelectedParentCategoryAvailable(inputCategoryDTO)
+ getStandardCategoriesUsingRESTAPI()
+ createAndAddLinksToDTO(categoryDTOBeforeLinks)
+ createAndAddLinksToExceptionDTO(exceptionDTOBeforeLinks, familyId)
}

interface "ICategoryService" as ICS {

}

class "CategoryService" as CS {
+ addCustomSubCategory(categoryDTO)
+ existsCategory(name, parent, isStandard)
}

class "AssemblerToDTO" as ADTO {
+ inputToCustomCategoryDTO(familyId, inputCategoryDTO)
+ toCategoryDTO(newSubcategory)
}

interface "ICategoryRepositoryHttp" as ICRHTTP {

}

class "CategoriesFromGroupOne" as CGROUP1 {
+ importCategories()
+ jsonToDTOList(object)
}

class "CategoriesFromGroupTwo" as CGROUP2 {
+ importCategories()
+ jsonToDTOList(object)
}

class "CategoriesFromGroupThree" as CGROUP3 {
+ importCategories()
+ jsonToDTOList(object)
}

class "CategoryDTO" as CDTO {
+ CategoryDTO()
+ setName(name)
+ setId(id)
+ setParentId(parentId)
+ setFamilyId(familyId)
+ getName()
+ getParentId()
}

interface "ICategoryAssembler" as ICA {

}

class "CategoryAssembler" as CA {
+ assembleCategoryId()
+ assembleCategoryName(categoryDTO)
+ assembleParentCategoryId(categoryDTO)
+ assembleFamilyId(categoryDTO)
}

class "CustomCategory" as CC {
+ Builder(id)
+ setName(name)
+ setParent(parent)
+ setFamily(familyId)
+ build()
}

interface "ICategoryRepository" as ICR {

}

class "CategoryRepository" as CR {
+ saveNew(newSubcategory)
}

interface "ICategoryAssemblerJPA" as ICAJPA {

}

class "CategoryAssemblerJPA" as CAJPA {
+ toJPA(newCategory)
}

interface "ICategoryRepositoryJPA" as ICRJPA {
+ save(newCategoryJPA)
}

class "ExceptionDTO" as EDTO {
+ ExceptionDTO(message)
}


CTRL -.up|> ICTRL
CTRL --> "1" ICS : categoryService
CTRL --> "1" ADTO : assemblerToDTO
CTRL --> "1" ICRHTTP : categoryRepositoryHTTP
CS -.|> ICS
CS --> "1" ICR : categoryRepository
CS --> "1" ICA : assembler
CS --> "1" ADTO : assemblerToDTO
CGROUP1 -.|> ICRHTTP
CGROUP2 -.|> ICRHTTP
CGROUP3 -.|> ICRHTTP
CR -.|> ICR 
CR --> "1" ICAJPA : categoryAssemblerJPA
CR --> "1" ICRJPA : categoryRepositoryJPA
CAJPA -.|> ICAJPA
CS -.> CC : builds




@enduml
```

## 3.2. Functionality

```plantuml
autonumber
title US111 - Add A Category To The Family's Category Tree
actor "Family Administrator" as FA
participant "controller\n:AddCustom\nCategoryController" as ACCC
participant "categoryRepository\n:CategoryRepository" as CR
participant "categories\n:Categories" as CS
participant "customCategory\n:CustomCategory" as CC
participant "familyID\n:FamilyID" as FID
participant "categoryName\n:CategoryName" as CN
participant "parentName\n:ParentName" as PN
participant "categoryID\n:CategoryID" as CID

activate FA
FA -> ACCC : addCustomCategory\n(familyID, categoryName, parentCategoryId)
activate ACCC
ACCC -> CR : addCustomCategory\n(familyID, categoryName, parentCategoryId)
activate CR
CR -> CS : doesCategoryExistAtLevel\n(categoryName, level)
activate CS
CS --> CR : boolean
deactivate CS
CR -> CS : generateCategoryId()
activate CS
CS --> CR : categoryID
deactivate CS
CR -> CC * : addCustomCategory\n(familyID, name, parentName, categoryID)
CC -> FID * : create(familyID)
CC -> CN * : create(categoryName)
CC -> PN * : create(parentName)
CC -> CID * : create(categoryID)
CR -> CS : addCategory(customCategory)
activate CS
CS -> CS : add(customCategory)
deactivate CS
CR -> ACCC : categoryId
deactivate CR
ACCC -> FA : informs success
deactivate ACCC
deactivate FA

@enduml
```

Glossary
======================

*Contains all abbreviations used on the documentation.*

| *_Abbreviations_* | *_Designation_*   |  *_Description_*   |
| :-----------------| :-----------------| :------------------|
| **US**            | User Story        | Description of a software system feature that developing team has to build.|
| **SM**            | System Manager    | The System Administrator. Configuration and administration of the application are his/her responsibilities.|
| **PO**            | Product Owner     | Member of developing team that represents the client.                        |
| **FA**            | Family Administrator | A person responsible for carrying out the administration of their family application.                   |
| **SSD**           | System Sequence Diagram | Diagram that shows the interactions between user and the system developed.|
| **SD**            | Sequence Diagram | Diagram that captures all interactions between objects by order.|
| **CD**            | Class Diagram    | Diagram that describes the structure of the application, by showing the classes involved, their methods, attributes and the relationships among them.|
| **DM**            | Domain Model     | Conceptual model of Application being developed, showing the definitions and relations between the business terms.  
| **UI**            | User Interface   | The space where humans interact and communicate with computer/device.|
| **FFM APP**       | Family Finance Management Application  | Application that is being developed. Also we named a Class FFMApplication that delegates the requests between controller and all classes involved.|
| **FFM_BL**        | Family Finance Management Business Logic  | Layer in the program responsible for the  business functions and rules. 
| **FM**            | Family Member | Person that has a family relation.                    |
| **SWS**           | SWitCH Soft   | Team responsible to develop this application.|
| **GRASP**         | General Responsibility assignment software patterns | Set of patterns used on Object-Oriented Design.| 
| **DTO**           | Data Transfer Objects | Used to transport data between different system components. |      
| **RDD**           | Responsibility-Driven Design | Design technique in object-oriented programming. |
| **SRP**           | Single Responsibility Principle | Computer-programming principle that states that a class should have only one reason to change.
| **STUPID**        | STUPID | Acronym that describes some bad coding practices.
| **SOLID**         | SOLID | Acronym that describes good coding design patterns and principles.
| **UML**           | Unified Modeling Language | General-purpose, developmental, modeling language in the field of software engineering that is intended to provide a standard way to visualize the design of a system.
| **DDD**           | Domain-Driven Design | The concept that the structure and language of software code (class names, class methods, class variables) should match the business domain.
| **JPA**           | Java Persistence API | The Java ORM standard for storing, accessing, and managing Java objects in a relational database.
| **HATEOAS**       | Hypermedia as the Engine of Application State | Constraint of the REST application architecture that distinguishes it from other network application architectures.
| **REST**          | Representational state transfer | A software architectural style that was created to guide the design and development of the architecture for the World Wide Web.
| **API**           | Application Programming Interface | Software intermediary that allows two applications to talk to each other.
| **SPA**           | Single Page Application |  Web application or website that interacts with the user by dynamically rewriting the current web page with new data from the web server, instead of the default method of a web browser loading entire new pages.

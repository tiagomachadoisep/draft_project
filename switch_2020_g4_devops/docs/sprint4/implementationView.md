```plantuml

title Implementation View

 skinparam linetype polyline
 
package domain {
package shared
package aggregates
package domainservices
}

package "application services" {
package iappservices
package implappservices

package irepositories
}


package "interface adapters" {

package controllers {
package icontrollers
package implcontrollers
}
package repositories
}

package iassemblers
package implassemblers

package dto

package persistence

implappservices .up.^ iappservices
implassemblers .up.^iassemblers
repositories .right.^irepositories
implassemblers ..> dto
implassemblers ..> shared
implassemblers ..> aggregates
implcontrollers .> iappservices
implcontrollers .up.^ icontrollers
implappservices ..> iassemblers
implappservices ..> irepositories
implappservices ..> shared
implappservices ..> domainservices
implappservices ..> aggregates
repositories .up.> persistence

``` 

```plantuml
@startuml
title Implementation View - Zoom on Aggregates

package "Aggregates" as A {
package person as p{}
package family as f{}
package invoice as i{}
package ledger as l{}
package contract as c{}
package account as a{}
package category as cat{}
package futureTransaction as ft{}

p .>f
l .> i
l ..> a
i ..> c
cat ...> f
a ...> p
a ...> f
i .right.> cat
ft ...> p
l ...>p
c...>p
ft.>c

}


@enduml
```

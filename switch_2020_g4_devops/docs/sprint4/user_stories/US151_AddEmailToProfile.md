```plantuml
title \n\nUS151 Add Email to profile\n\n\n

participant ":IAddEmailController" as C
participant "mapper:MapperToDTO" as M
participant ":EmailDTO" as ED
participant ":IPersonService" as PS
participant ":PersonID" as ID
participant ":IPersonRepository" as PR
participant ":Person" as P
participant ":Email" as E
autonumber
[o-> C : addEmailToProfile(email, mainEmail)
activate C
C -> M : toEmailDTO(email, mainEmail)
M -->ED *: create
C -> PS : addEmailToProfile(emailDTO)
activate PS
PS --> ID * : create(new Email(emailDTO.getMainEmail()))
PS -> PR : addEmailToProfile(emailDTO.getEmail(), personID)
activate PR
PR -> PR : person = findByID(personID)
opt person.isPresent() == false
[<--o PR : result = false
else 
PR --> P : person = person.get()
PR -> P : addEmailToProfile(emailDTO.getEmail())
activate P
P  -> E ** : create(email)
activate E
E -> E : validateEmail(email)
deactivate E
P -> P : addEmail(email)

P -->PR : result = true
deactivate P
PR --> PS : result = true
deactivate PR
PS --> C : result = true
deactivate PS
[<--o C : result = true
end
```
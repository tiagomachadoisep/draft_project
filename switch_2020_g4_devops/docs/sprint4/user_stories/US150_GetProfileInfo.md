
```plantuml 
@startuml
autonumber
title \n**US150** Get the Family Member's profile information\n\n

participant "ctrl:\nIGetProfileInfoController" as C
participant "mapper:\nMapperToDTO" as M
participant "personDTO :\nPersonDTO" as DTO
participant "iPersonService:\nIPersonService" as PS 
participant "id:\nPersonID" as PI
participant "repository:\nIPersonRepository" as PR
participant "person:\nPerson" as P

[o-> C : getProfileInfo(mainEmail)
activate C
C -> M : toPersonDTO(person)
M --> DTO *: create
C-> PS : getProfileInfo(personDTO)
activate PS
PS --> PI * : create
PS -> PR : findByID(personID)
activate PR
PR -> P : person.get()
activate P
P --> PR : person
deactivate P
PR --> PS : person
deactivate PR
|||
PS -> PS : person.toString()
PS --> C : person
deactivate PS
[<--o C : person
deactivate C
@enduml
```
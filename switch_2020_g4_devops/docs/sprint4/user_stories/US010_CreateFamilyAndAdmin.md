Pre-requisites:

- 

Post-conditions:

- FamilyID generated
- Family object created
- Person created and associated with family

````plantuml
title US010 - Create Family and set Administrator
autonumber
actor "System Manager" as sm
participant ": UI" as UI
participant ": AddFamilyAndAdminController" as ctrl
participant ": IFamilyService" as serv
participant ": AssemblerDTO" as map
participant ": FamilyFactory" as fac
participant "family : Family" as fam
participant "familyID : FamilyID" as famid
participant "admin : Person" as admin
participant ": IFamilyRepository" as frepo
participant ": IPersonRepository" as prepo
participant ": AssemblerJPA" as jpa
participant "famJPA : FamilyJPA" as famjpa
participant "adminPA : PersonJPA" as adminjpa
participant ":IFamilyRepositoryJPA" as frepojpa
participant ":IPersonRepositoryJPA" as prepojpa
activate sm
sm -> UI : addFamilyAndAdmin(dto)
activate UI
UI -> ctrl : addFamilyAndAdmin(dto)
activate ctrl
ctrl -> serv : createAndAddFamily(dto)
activate serv
serv -> map : toFamilyName(dto.getFamilyName())
activate map
map --> serv : famName
deactivate map
serv -> map : toPersonName(dto.getAdminName())
activate map
map --> serv : adminName
deactivate map
serv -> map : toPersonID(dto.getAdminEmail())
activate map
map --> serv : adminID
deactivate map
serv -> map : toVAT(dto.getAdminVAT())
activate map
map --> serv : adminVAT
deactivate map
serv -> fac : createFamilyAndAdmin(famName,adminName,adminID,adminVAT)
activate fac

fac -> fam* : create(famName,adminID)
fam -> famid* : create()
fac -> admin* : create(adminID,adminName,adminVAT)
fac --> serv : familyPersonPair
deactivate fac
opt Transactional
serv -> frepo: save(family)
activate frepo
frepo -> jpa : assembleJPA(family)
activate jpa 
jpa -> famjpa*
jpa--> frepo : famJPA
deactivate jpa
frepo -> frepojpa : save(famJPA)
deactivate frepo
serv -> prepo: save(admin)
activate prepo
prepo -> jpa : assembleJPA(admin)
activate jpa 
jpa -> adminjpa*
jpa--> prepo : adminJPA
deactivate jpa
prepo -> prepojpa : save(adminJPA)
deactivate prepo
end
serv --> ctrl : familyID
deactivate serv
ctrl --> UI : familyID
deactivate ctrl
UI --> sm : familyID
deactivate UI
deactivate sm
````
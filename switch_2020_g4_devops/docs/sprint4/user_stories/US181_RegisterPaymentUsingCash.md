Pre-requisites :

- Member is registered in the app
- Member has cash account
- Category registered in the app
- Balance is enough (?)
- Invoice registered (?)

Post-conditions:

- Transaction object created and associated with member's Ledger object
- Balance of the account involved is changed
- Debit Movement created and associated with Transaction object

````plantuml
autonumber
title US181 - Register payment using cash account
actor "Family Member" as FM
participant ": UI" as UI
participant ": RegisterPaymentController" as ctrl
participant ": MapperToDTO" as mapper
participant ": LedgerService" as serv
participant ": AccountRepository" as arepo
participant ": LedgerRepository" as lrepo
participant ": AssemblerJPA" as jpa
participant "memberLedger : Ledger" as led
participant "payment : Payment" as payment
participant "debit : Movement" as deb

activate FM
FM -> UI : registerPayment\n(memberEmail, amount, categoryId)
activate UI
UI -> ctrl : registerPayment\n(memberEmail, amount, categoryId)
activate ctrl
ctrl -> mapper : toTransactionDTO(memberEmail, amount, categoryId)
activate mapper
mapper --> ctrl : transactionDTO
deactivate mapper
ctrl -> serv : registerPayment(transactionDTO)  
activate serv
serv -> arepo : hasPersonalCashAccount(personID)
serv -> arepo : findCashByOwnerId(personID)
activate arepo
arepo --> serv : account
deactivate arepo
serv -> lrepo : findByOwnerId(memberEmail)
activate lrepo
lrepo --> serv : ledgerJPA
deactivate lrepo
serv -> jpa : memberLedger=assemble(ledgerJPA)
serv -> led : addPayment(accountId, moneyAmount, categoryId)
activate led
led -> led : id = generateTransactionId()
led --> payment* : create(id,accountId,moneyAmount,categoryId)
activate payment
payment -> deb* : create(account,-moneyAmount)
deactivate payment
led -> led : add(payment)
led --> serv : id
deactivate led
serv -> jpa : ledgerJPA=toLedgerJPA(memberLedger)
serv-> lrepo : save(ledgerJPA)
serv --> ctrl : id
deactivate serv
ctrl -> UI : id
deactivate ctrl
UI -> FM : id
deactivate UI
deactivate FM
````

Tests:

- Register a payment for a user without cash account
- Register a payment with not enough balance
- Register a valid payment

````plantuml
autonumber
title US181 - Register payment using cash account
actor "Family Member" as FM
participant ": UI" as UI
participant ": RegisterPaymentController" as ctrl
participant ": MapperToDTO" as mapper
participant ": LedgerService" as serv
participant ": AccountRepository" as arepo
participant ": AccountRepositoryJPA" as arepojpa
participant ": LedgerRepository" as lrepo
participant ": AssemblerJPA" as jpa
participant ": LedgerRepositoryJPA" as lrepojpa
participant ": LedgerAccountService" as domserv

participant "memberLedger : Ledger" as led
participant "payment : Payment" as payment
participant "debit : Movement" as deb
participant "account : Account" as acc

activate FM
FM -> UI : registerPayment\n(memberEmail,\namount, categoryId)
activate UI
UI -> ctrl : registerPayment\n(memberEmail, amount, categoryId)
activate ctrl
ctrl -> mapper : toTransactionDTO\n(memberEmail, amount, categoryId)
activate mapper
mapper --> ctrl : transactionDTO
deactivate mapper
ctrl -> serv : registerPayment(transactionDTO)  
activate serv
serv -> arepo : hasPersonalCashAccount(personID)
serv -> arepo : findCashByOwnerId(personID)
activate arepo
arepo -> arepojpa : findCashAccountBy\nOwnerId(personID)
activate arepojpa
arepojpa --> arepo : personalCashAccountJPA
deactivate arepojpa
arepo -> jpa : account=assemble(personalCashAccountJPA)
arepo --> serv : account
deactivate arepo
serv -> lrepo : findByOwnerId(memberEmail)
activate lrepo
lrepo -> lrepojpa : findByOwnerId(memberEmail)
activate lrepojpa
lrepojpa --> lrepo : ledgerJPA
deactivate lrepojpa
lrepo -> jpa : memberLedger=\nassemble(ledgerJPA)
lrepo --> serv : ledger
deactivate lrepo
serv -> domserv : addPayment(memberLedger, account, moneyAmount, categoryId)
activate domserv
domserv -> acc : hasFunds(moneyAmount)
domserv -> led : addPayment(account.getId(), moneyAmount, categoryId)
activate led
led -> led : id = generateTransactionId()
led --> payment* : create(id,accountId,\nmoneyAmount,categoryId)
activate payment
payment -> deb* : create(account,\n-moneyAmount)
deactivate payment
led -> led : add(payment)
led --> domserv : id
deactivate led
domserv -> acc : removeFunds(moneyAmount)
domserv --> serv : id
deactivate domserv
serv -> lrepo : save(memberLedger)
activate lrepo
lrepo -> jpa : ledgerJPA=\ntoLedgerJPA(memberLedger)
lrepo-> lrepojpa : save(ledgerJPA)
deactivate lrepo
serv -> arepo : save(account)
activate arepo
arepo -> jpa : personalCashAccountJPA=toAccountJPA(account)
arepo -> arepojpa : save(personalCashAccountJPA)
deactivate arepo
serv --> ctrl : id
deactivate serv
ctrl -> UI : id
deactivate ctrl
UI -> FM : id
deactivate UI
deactivate FM
````

````plantuml
@startuml
autonumber
title US181 - Register payment using cash account
actor "Family Member" as FM
participant ": UI" as UI
participant ": RegisterPaymentController" as ctrl
participant ": MapperToDTO" as mapper
participant ": LedgerService" as serv
participant ": AccountRepository" as arepo
participant ": AccountRepositoryJPA" as arepojpa
participant ": LedgerRepository" as lrepo
participant ": AssemblerJPA" as jpa
participant ": LedgerRepositoryJPA" as lrepojpa
participant ": LedgerAccountService" as domserv

participant "memberLedger : Ledger" as led
participant "payment : Payment" as payment
participant "debit : Movement" as deb
participant "account : Account" as acc
participant "ledgerJPA : LedgerJPA" as ljpa

activate FM
FM -> UI : registerPayment\n(memberEmail,\namount, categoryId)
activate UI
UI -> ctrl : registerPayment\n(memberEmail, amount, categoryId)
activate ctrl
ctrl -> mapper : toTransactionDTO\n(memberEmail, amount, categoryId)
activate mapper
mapper --> ctrl : transactionDTO
deactivate mapper
ctrl -> serv : registerPayment(transactionDTO)  
activate serv
serv -> arepo : hasPersonalCashAccount(personID)
serv -> arepo : findCashByOwnerId(personID)
activate arepo
arepo -> arepojpa : findCashAccountBy\nOwnerId(personID)
activate arepojpa
arepojpa --> arepo : personalCashAccountJPA
deactivate arepojpa
arepo -> jpa : account=assemble(personalCashAccountJPA)
arepo --> serv : account
deactivate arepo
serv -> lrepo : findByOwnerId(memberEmail)
activate lrepo
lrepo -> lrepojpa : findByOwnerId(memberEmail)
activate lrepojpa
lrepojpa --> lrepo : ledgerJPA
deactivate lrepojpa
lrepo -> jpa : memberLedger=\nassemble(ledgerJPA)
lrepo --> serv : ledger
deactivate lrepo
serv -> domserv : addPayment(memberLedger, account, moneyAmount, categoryId)
activate domserv
domserv -> acc : hasFunds(moneyAmount)
domserv -> led : addPayment(accountId, moneyAmount, categoryId)
activate led
led -> led : id = generateTransactionId()
led --> payment* : create(id,accountId,\nmoneyAmount,categoryId)
activate payment
payment -> deb* : create(account,\n-moneyAmount)
deactivate payment
led -> led : add(payment)
led --> domserv : id
deactivate led
domserv -> acc : removeFunds(moneyAmount)
domserv --> serv : id
deactivate domserv
serv -> lrepo : addPayment(id, accountId, moneyAmount, categoryId)
activate lrepo
lrepo -> ljpa : addPayment(id, accountId, moneyAmount, categoryId)
lrepo -> lrepojpa : save(ledgerJPA)
deactivate lrepo
serv -> arepo : save(account)
activate arepo
arepo -> jpa : personalCashAccountJPA=toAccountJPA(account)
arepo -> arepojpa : save(personalCashAccountJPA)
deactivate arepo
serv --> ctrl : id
deactivate serv
ctrl -> UI : id
deactivate ctrl
UI -> FM : id
deactivate UI
deactivate FM
@enduml
````

Domain Service is responsible for enforcing business rules. Operation is only saved to database if all
tasks are successful.
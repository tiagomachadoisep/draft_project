Pre-requisites :

- Family has cash account
- Member has cash account
- Member is registered in the app with family's id
- Category registered in the app

Post-conditions: 

- Transaction object created and associated with member and family Ledger objects
- Balances of the accounts involved are changed
- Debit and Credit Movements created and associated with Transaction object

````plantuml
@startuml
title US130 - Transfer Cash From Family To Member
autonumber
actor "Family Admin" as FA
participant ": UI" as UI
participant ": TransferCashFromFamilyToMemberController" as ctrl
participant ": MapperToDTO" as mapper
participant ": LedgerService" as serv
participant ": PersonRepository" as prepo
participant "member : Person" as member
participant ": AccountRepository" as arepo
participant ": LedgerRepository" as lrepo
participant "familyLedger : Ledger" as fled
participant "transfer : Transfer" as transfer
participant "debit : Movement" as deb
participant "credit : Movement" as cred
participant "memberLedger : Ledger" as mled
activate FA
FA -> UI : transferCashFromFamilyToMember\n(familyId ,memberEmail, amount, categoryId)
activate UI
UI -> ctrl : transferCashFromFamilyToMember\n(familyId ,memberEmail, amount, categoryId)
activate ctrl
ctrl -> mapper : toTransferDTO(familyId ,memberEmail, amount, categoryId)
activate mapper
mapper --> ctrl : transferDTO
deactivate mapper
ctrl -> serv : transferCashFromFamilyToMember\n(transferDTO)    
activate serv
serv -> prepo : member = findById(memberEmail)
serv -> member : isFamilyId(familyId)
serv -> arepo : findByOwnerId(familyId)
activate arepo
arepo --> serv : familyCashAccount
deactivate arepo
serv -> arepo : findByOwnerId(memberEmail)
activate arepo
arepo --> serv : memberCashAccount
deactivate arepo
serv -> lrepo : findByOwnerId(familyId)
activate lrepo
lrepo --> serv : familyLedger
deactivate lrepo
serv -> lrepo : findByOwnerId(memberEmail)
activate lrepo
lrepo --> serv : memberLedger
deactivate lrepo
serv -> fled : addTransfer(familyCashAccountId, memberCashAccountId, amount, categoryId)
activate fled
fled -> fled : id = generateTransactionId()
fled --> transfer* : create(familyCashAccountId,\nmemberCashAccountId, amount, categoryId)
activate transfer
transfer --> deb* : create(familyCashAccountId,-amount)
transfer --> cred* : create(memberCashAccountId,amount)
deactivate transfer
fled -> fled : add(transfer)
fled --> serv : transfer
deactivate fled
serv -> mled : add(transfer)
serv --> ctrl : transactionId
deactivate serv
ctrl --> UI : transactionId
deactivate UI
UI -> FA : transactionId
deactivate FA
@enduml
````

````plantuml
@startuml
title US130 - Transfer Cash From Family To Member
autonumber
actor "Family Admin" as FA
participant ": UI" as UI
participant ": TransferCashFromFamilyToMemberController" as ctrl
participant ": LedgerService" as serv
participant " : MemberTransferService" as domserv
participant ": PersonRepository" as prepo
participant ": AccountRepository" as arepo
participant ": LedgerRepository" as lrepo
participant "familyLedger : Ledger" as fled
participant "transfer : Transfer" as transfer
participant "debit : Movement" as deb
participant "credit : Movement" as cred
participant "memberLedger : Ledger" as mled
activate FA
FA -> UI : transferCashFromFamilyToMember\n(familyId ,memberEmail, amount, categoryId)
activate UI
UI -> ctrl : transferCashFromFamilyToMember\n(familyId ,memberEmail, amount, categoryId)
activate ctrl
ctrl -> serv : transferCashFromFamilyToMember\n(familyId ,memberEmail, amount, categoryId)
activate serv
serv -> prepo : member = findById(memberEmail)
domserv -> "member : Person" : isFamilyId(familyId)
serv -> arepo : findByOwnerId(familyId)
activate arepo
arepo --> serv : familyCashAccount
deactivate arepo
serv -> arepo : findByOwnerId(memberEmail)
activate arepo
arepo --> serv : memberCashAccount
deactivate arepo
serv -> lrepo : findByOwnerId(familyId)
activate lrepo
lrepo --> serv : familyLedger
deactivate lrepo
serv -> lrepo : findByOwnerId(memberEmail)
activate lrepo
lrepo --> serv : memberLedger
deactivate lrepo
serv -> domserv : addTransfer(familyCashAccount, memberCashAccount, moneyAmount, category, memberLedger, familyLedger)
domserv -> famcash : hasAmount(moneyAmount)
domserv -> fled : addTransfer(familyCashAccountId, memberCashAccountId, amount, categoryId, memberLedger)
activate fled
fled -> fled : id = generateTransactionId()
fled --> transfer* : create(familyCashAccountId,\nmemberCashAccountId, amount, categoryId)
activate transfer
transfer --> deb* : create(familyCashAccountId,-amount)
transfer --> cred* : create(memberCashAccountId,amount)
deactivate transfer
fled -> fled : add(transfer)
fled -> mled : add(transfer)
fled --> serv : transactionId
deactivate fled
serv --> ctrl : transactionId
deactivate serv
ctrl --> UI : transactionId
deactivate UI
UI -> FA : transactionId
deactivate FA
@enduml
````

````plantuml
@startuml
autonumber
title US130 - Transfer Cash From Family To Member
actor "Family Admin" as FM
participant ": UI" as UI
participant ": TransferCashFromFamilyController" as ctrl
participant ": MapperToDTO" as mapper
participant ": LedgerService" as serv
participant ": PersonRepository" as prepo
participant ": AccountRepository" as arepo
participant ": AccountRepositoryJPA" as arepojpa
participant ": LedgerRepository" as lrepo
participant ": AssemblerJPA" as jpa
participant ": LedgerRepositoryJPA" as lrepojpa
participant ": LedgerAccountService" as domserv

participant "memberLedger : Ledger" as led
participant "payment : Payment" as payment
participant "debit : Movement" as deb
participant "accountFam : Account" as acc
participant "famLedger : Ledger" as famled
participant "ledgerJPA : LedgerJPA" as ljpa

activate FM
FM -> UI : transferMoneyFromFamily\n(adminEmail, memberEmail,\namount, categoryId)
activate UI
UI -> ctrl : transferMoneyFromFamily\n(adminEmail, memberEmail,\namount, categoryId)
activate ctrl
ctrl -> mapper : toTransactionDTO\n(adminEmail, memberEmail, amount, categoryId)
activate mapper
mapper --> ctrl : transactionDTO
deactivate mapper
ctrl -> serv : transferMoneyFromFamily(transactionDTO)  
activate serv
serv -> prepo : isMemberInFamily(memberEmail,adminEmail)
serv -> arepo : hasCashAccount(personID)
serv -> arepo : findCashByOwnerId(personID)
activate arepo
arepo -> arepojpa : findCashAccountBy\nOwnerId(personID)
activate arepojpa
arepojpa --> arepo : personalCashAccountJPA
deactivate arepojpa
serv -> arepo : hasCashAccount(familyID)
serv -> arepo : findCashByOwnerId(familyID)
activate arepo
arepo -> arepojpa : findCashAccountBy\nOwnerId(familyID)
activate arepojpa
arepojpa --> arepo : familyCashAccountJPA
deactivate arepojpa
arepo -> jpa : accountFam=assemble(familyCashAccountJPA)
arepo --> serv : accountFam
deactivate arepo
serv -> lrepo : findByOwnerId(memberEmail)
activate lrepo
lrepo -> lrepojpa : findByOwnerId(memberEmail)
activate lrepojpa
lrepojpa --> lrepo : ledgerJPA
deactivate lrepojpa
lrepo -> jpa : memberLedger=\nassemble(ledgerJPA)
lrepo --> serv : ledger
deactivate lrepo
serv -> lrepo : findByOwnerId(familyID)
activate lrepo
lrepo -> lrepojpa : findByOwnerId(familyID)
activate lrepojpa
lrepojpa --> lrepo : ledgerJPA
deactivate lrepojpa
lrepo -> jpa : familyLedger=\nassemble(ledgerJPA)
lrepo --> serv : ledgerFam
deactivate lrepo
serv -> domserv : addTransfer(ledgerFam, memberLedger, account,\naccountFam, moneyAmount, categoryId)
activate domserv
domserv -> acc : hasFunds(moneyAmount)
domserv -> led : addTransfer(accountId, moneyAmount, categoryId)
activate led
led --> payment* : create(id,accountId,\nmoneyAmount,categoryId)
activate payment
payment -> deb* : create(account,\n-moneyAmount)
deactivate payment
led -> led : add(payment)
led --> domserv : id
deactivate led
domserv -> acc : removeFunds(moneyAmount)
domserv --> serv : id
deactivate domserv
serv -> lrepo : addPayment(id, accountId, moneyAmount, categoryId)
activate lrepo
lrepo -> ljpa : addPayment(id, accountId, moneyAmount, categoryId)
lrepo -> lrepojpa : save(ledgerJPA)
deactivate lrepo
serv -> arepo : save(account)
activate arepo
arepo -> jpa : personalCashAccountJPA=toAccountJPA(account)
arepo -> arepojpa : save(personalCashAccountJPA)
deactivate arepo
serv --> ctrl : id
deactivate serv
ctrl -> UI : id
deactivate ctrl
UI -> FM : id
deactivate UI
deactivate FM
@enduml
````
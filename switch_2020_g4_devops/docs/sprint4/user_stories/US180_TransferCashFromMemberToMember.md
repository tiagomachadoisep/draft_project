Pre-requisites :

- Both members have cash account
- Members are registered in the app with same family
- Category registered in the app

Post-conditions:

- Transaction object created and associated with both members Ledger objects
- Balances of the accounts involved are changed
- Debit and Credit Movements created and associated with Transaction object


````plantuml
@startuml
title US180 - Transfer Cash From Member To Member
autonumber
actor "Family Member" as FM
participant ": UI" as UI
participant ": TransferCashFromMemberToMemberController" as ctrl
participant ": MapperToDTO" as mapper
participant ": LedgerService" as serv
participant ": PersonRepository" as prepo
participant "sender : Person" as member
participant ": AccountRepository" as arepo
participant ": LedgerRepository" as lrepo
participant "familyLedger : Ledger" as fled
participant "transfer : Transfer" as transfer
participant "debit : Movement" as deb
participant "credit : Movement" as cred
participant "memberLedger : Ledger" as mled
activate FM
FM -> UI : transferCashFromMemberToMember\n(memberEmailSender ,memberEmailReceiver, amount, categoryId)
activate UI
UI -> ctrl : transferCashFromMemberToMember\n(memberEmailSender ,memberEmailReceiver, amount, categoryId)
activate ctrl
ctrl -> mapper : toTransferDTO(memberEmailSender ,memberEmailReceiver, amount, categoryId)
activate mapper
mapper --> ctrl : transferDTO
deactivate mapper
ctrl -> serv : transferCashFromMemberToMember\n(transferDTO)    
activate serv
serv -> prepo : sender = findById(memberEmailSender)
serv -> prepo : receiver = findById(memberEmailReceiver)
serv -> member : hasSameFamily(receiver)
serv -> arepo : findByOwnerId(memberEmailSender)
activate arepo
arepo --> serv : cashAccountSend
deactivate arepo
serv -> arepo : findByOwnerId(memberEmailReceiver)
activate arepo
arepo --> serv : cashAccountReceive
deactivate arepo
serv -> lrepo : findByOwnerId(memberEmailSender)
activate lrepo
lrepo --> serv : senderLedger
deactivate lrepo
serv -> lrepo : findByOwnerId(memberEmailReceiver)
activate lrepo
lrepo --> serv : receiverLedger
deactivate lrepo
serv -> fled : addTransfer(cashAccountSendId, cashAccountReceiveId, amount, categoryId)
activate fled
fled -> fled : id = generateTransactionId()
fled --> transfer* : create(id,cashAccountSendId,\ncashAccountReceiveId, amount, categoryId)
activate transfer
transfer --> deb* : create(cashAccountSendId,-amount)
transfer --> cred* : create(cashAccountReceiveId,amount)
deactivate transfer
fled -> fled : add(transfer)
fled --> serv : transfer
deactivate fled
serv -> mled : add(transfer)
serv --> ctrl : transactionId
deactivate serv
ctrl --> UI : transactionId
deactivate UI
UI -> FM : transactionId
deactivate FM
@enduml
````
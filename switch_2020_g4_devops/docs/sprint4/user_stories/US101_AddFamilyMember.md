# SEQUENCE DIAGRAMS

```plantuml
@startuml
autonumber
title US101 : add family members

participant " :IAddFamilyMemberController" as Ctrl
participant " :MapperToDTO" as TPDTO
participant " personDTO:\nPersonDTO" as PDTO
participant " :IPersonService" as PS
participant " :IPersonAssembler" as PA
participant " personRepository:\nIPersonRepository" as PR
participant " person\n:Person" as P
participant " assemblerJPA\n:AssemblerJPA" as AJPA
participant " :IPersonRepositoryJPA" as RJPA



[o-> Ctrl : addMember\n(name,email,vatNumber)
activate Ctrl
Ctrl -> TPDTO  : toPersonDTO(name,email,vatNumber)
activate TPDTO
TPDTO --> PDTO ** : create(name,email,vatNumber)
deactivate TPDTO
Ctrl -> PS : addMember(personDTO)
activate PS
PS -> PA : assemble(personDTO)
activate PA
PA --> PS : person
deactivate PA
PS -> PR : saveNew(person)
activate PR
PR -> P : getID()
activate P 
P --> PR : personID
deactivate P
PR -> PR : existsID(personID)
opt exists
PR --> Ctrl : exception ""Member already registered"
else
PR -> AJPA : personToJpa(person)
activate AJPA
AJPA --> PR : personJPA
deactivate AJPA
PR -> RJPA : save(personJPA)

deactivate PR
PS -> Ctrl : personID
deactivate PS
[<--o Ctrl : personID
deactivate Ctrl
end opt

@enduml
```

```plantuml
@startuml
autonumber
title US101 : add family members (SPRING)

participant " :IAddFamilyMemberController" as Ctrl
participant " :MapperToDTO" as TPDTO
participant " personDTO:\nPersonDTO" as PDTO
participant " :IPersonService" as PS
participant " person\n:Person" as P
participant " personRepository:\nIPersonRepository" as PR



[o-> Ctrl : addMember\n(name,email,vatNumber)
activate Ctrl
Ctrl -> TPDTO  : toPersonDTO(name,email,vatNumber)
activate TPDTO
TPDTO --> PDTO ** : create(name,email,vatNumber)
deactivate TPDTO
Ctrl -> PS : addMember(personDTO)
activate PS
PS --> P** : create(personDTO)
PS -> PR : saveMember(person)
activate PR
PR -> PR : isMember(personID)
PR -> PR : addPerson(person)
return personID
deactivate PR
PS -> Ctrl : personID
deactivate PS
[<--o Ctrl : personID
deactivate Ctrl
@enduml
```

**************************************************************************



```plantuml
@startuml
autonumber
title US101 : add family members
actor "Family Administrator" as Admin
participant " : UI" as UI
participant " : AddFamilyMemberController" as Ctrl
participant " : personRepository:\nPersonRepository" as PR


activate Admin
Admin->UI:Add family member
activate UI
UI->Ctrl: addMemberToFamily(familyID,\npersonID,name,vatNumber,address,familyID)
activate Ctrl
Ctrl -> PR : addPerson(personID,\nname,vatNumber,address,familyID)
Activate PR
ref over PR : addPerson(personID,\nname,vatNumber,address,familyID)
return personID
deactivate PR

Ctrl --> UI : personID
deactivate Ctrl
UI --> Admin : Member added to Family
deactivate UI 
deactivate Admin
@enduml
```
```plantuml
@startuml
autonumber
title : addPerson
participant " : PersonRepository" as PR
participant "members : Members" as members
participant "personList : List<Person>" as PL
participant "person : Person" as person
activate PR
[o-> PR : addPerson(personID,\nname,vatNumber,address)
PR -> members : isMember(personID)
activate members
loop for each person in personList
members -> person : boolean = isID(personID)

opt result==true
members -> PR: failure
[<--o PR : exception
end

deactivate
end
[<--o PR : personID
deactivate PR
@enduml
```

```plantuml
@startuml
autonumber
title US101 : add family members

participant " :IAddFamilyMemberController" as Ctrl
participant " :MapperToDTO" as TPDTO
participant " personDTO:\nPersonDTO" as PDTO
participant " :IPersonService" as PS
participant " personAssembler : \nIPersonAssembler" as assembler
participant " person\n:Person" as P
participant " personRepository:\nIPersonRepository" as PR
participant " : IAssemblerJPA" as jpassembler
participant "personJPA : \nPersonJPA" as personjpa
participant " personRepositoryJPA:\nIPersonRepositoryJPA" as JPA


[o-> Ctrl : addMember\n(familyID, personID,\n name, vatNumber, address)
activate Ctrl
Ctrl -> TPDTO  : toPersonDTO(familyID, personID,\n name, vatNumber, address)
activate TPDTO
TPDTO --> PDTO ** : create(familyID, personID,\n name, vatNumber, address)
TPDTO --> Ctrl : personDTO
deactivate TPDTO
Ctrl -> PS : addMember(personDTO)
activate PS
PS -> assembler : assemble(personDTO)
activate assembler
assembler --> P** : build()
assembler --> PS : person
deactivate assembler
PS -> PR : saveNew(person)
activate PR
PR -> JPA : check = existsById(personID)
PR -> jpassembler : personToJPA(person)
activate jpassembler
jpassembler --> personjpa : create()
jpassembler --> PR : personJPA
deactivate jpassembler
PR -> JPA : save(personJPA)
deactivate PR
PS -> Ctrl : personID
deactivate PS
[<--o Ctrl : personID
deactivate Ctrl
@enduml
```

## Class Diagram

```plantuml
@startuml
skinparam linetype polyline

AddFamilyMemberController -.left|> IAddFamilyMemberController
AddFamilyMemberController -right-> "1"  MapperToDTO : mapper
MapperToDTO  ..right> PersonDTO :  creates
AddFamilyMemberController --> "1" IPersonService : personService

PersonService -.up|> IPersonService
PersonService -> "1" IPersonRepository : personRepository
PersonService -left> "1" IPersonAssembler : personAssembler
PersonAssembler -up.|> IPersonAssembler
PersonAssembler .-right-> Person : builds 
AssemblerJPA -.right|> IPersonAssemblerJPA 
AssemblerJPA ..> PersonJPA : creates

PersonRepository -.|> IPersonRepository
PersonRepository -> "1" IPersonAssemblerJPA : personAssemblerJPA
PersonRepository -> "1" IPersonRepositoryJPA : personRepositoryJPA


interface IAddFamilyMemberController{}

class AddFamilyMemberController {
+addFamilyMember(int familyID, String personID,\n String name, int vatNumber, String address)
}

class MapperToDTO{
+toPersonDTO(int familyID,String personID,\nString name, int vatNumber, String address)
}

class PersonDTO{
+PersonDTO(int familyID, String personID,\n String name, int vatNumber, String address)
}

interface IPersonService{

}

interface IPersonAssembler{}

class Person{
+build()
}

interface IPersonRepository{

}

interface IPersonAssemblerJPA{
}

class PersonJPA{
+ PersonJPA(String personID, int familyID,\n String name, int vat, String address)
}

interface IPersonRepositoryJPA{

}

class PersonAssembler{
+assemble(PersonDTO dto)
}

class PersonRepository{
+saveNew(Person person)
+existsID(PersonID id)
}

class PersonService{
+addMember(PersonDTO personDTO)
}

class AssemblerJPA{
+personToJpa(Person person)
}

@enduml
```

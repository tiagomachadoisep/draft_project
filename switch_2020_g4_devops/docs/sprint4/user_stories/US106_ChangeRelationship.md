# 3. Design

## 3.1. Functionality

````plantuml

title US106 - Change Family Relationship
autonumber
actor "Family Administrator" as FA
participant ": UI" as UI
participant ": ChangeRelationshipController" as ctrl
participant ": FamilyRelationshipService" as frs
participant ": FamilyRepository" as repo
participant "family : Family" as fam
participant "relations : FamilyRelationships" as rels
participant "relation : FamilyRelationship" as ship

FA -> UI : changeRelationship(familyId,\nemailOne,emailTwo, newRelation)
activate FA
activate UI
UI -> ctrl : changeRelationship(familyId,\nemailOne,emailTwo, newRelation)
activate ctrl
ctrl -> frs : changeRelationship(familyId,\nemailOne,emailTwo, newRelation)
activate frs
frs -> repo : getFamilyById(familyId)
activate repo
repo --> frs : family
deactivate repo
frs -> fam : changeRelationship(personOneID, personTwoID, newRelation)
activate fam
fam -> rels : findAndChangeRelationship\n(personOneID, personTwoID, newRelation)
activate rels
loop for relation in relations
rels -> ship : isID(FamilyRelationshipID\n(personOneID, personTwoID))
activate ship
ship --> rels : boolean
opt boolean == true
rels -> ship : setRelationType(newRelation)
deactivate ship
end
end 
rels --> fam : boolean
deactivate rels
fam --> frs : boolean
deactivate fam
frs --> ctrl : boolean
deactivate frs
ctrl --> UI : result
deactivate ctrl
UI --> FA : result
deactivate UI
deactivate FA

````
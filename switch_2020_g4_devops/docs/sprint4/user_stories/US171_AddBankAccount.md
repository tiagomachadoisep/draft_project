##Sequence Diagram

```plantuml 
@startuml
autonumber
title **US171** - Add Bank Account (SPRING)

participant ":IAddBankAccountController" as BAC
participant ":toAccountDTO\n:MapperToDTO" as AMDTO
participant "accountDTO\n:AccountDTO" as ADTO
participant ":IAccountService" as AS
participant "account\n:Account" as A
participant ":IAccount\nRepository" as AR



[o-> BAC : addBankAccount\n(description)
activate BAC
BAC -> AMDTO : toAccountDTO(data)
activate AMDTO
AMDTO -> ADTO**:create(description)
deactivate AMDTO
BAC -> AS: addBankAccount(accountDTO)
activate AS
AS -> A**: create(accountDTO)
AS -> AR: save(account)
activate AR
AR -> AR: addAccount(account)
return accountID
return accountID
deactivate AR
deactivate BAC
@enduml
```
```plantuml
title US001 AddStandardCategory
actor "Sistem Manager" as sm
participant ":IGetStandardCategoriesTree" as ctrl
participant "mapper:Mapper" as mapper
participant ":ICategoryService" as aps
participant ":ICategoryRepository" as cr
participant "category\n: Categorable" as cat
autonumber

sm -> ctrl : request standard categories tree
activate sm
activate ctrl
ctrl -> aps : getStandardCategoriesTree()
activate aps
aps -> cr : getStandardCategoriesTree()
activate cr
cr --> "categoryTree : List <Categorable>" ** : create()
loop category in categories
cr -> cat : isAtRootLevel() && isStandard()
activate cat
cat --> cr : boolean
opt boolean == true
deactivate cat
cr -> "categoryTree : List <Categorable>" : add(category)
activate "categoryTree : List <Categorable>" 
ref over cr : getChildTree(parent)
deactivate "categoryTree : List <Categorable>" 
end opt
end loop

cr --> aps : categoryTree
deactivate cr
ref over aps : categoryToDto()
aps -> aps : categoryToDto()
aps --> ctrl : categoryTreeDto
deactivate aps
ctrl --> sm : categoryTreeDto
deactivate ctrl
deactivate sm
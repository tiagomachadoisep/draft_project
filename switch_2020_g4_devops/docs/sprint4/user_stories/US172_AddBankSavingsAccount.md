##Sequence Diagram

```plantuml 
@startuml
autonumber
title **US171** - Add Bank Savings Account (SPRING)

participant ":IAddBankSavingsAccountController" as BSAC
participant ":toAccountDTO\n:MapperToDTO" as AMDTO
participant "accountDTO\n:AccountDTO" as ADTO
participant ":IAccountService" as AS
participant "account\n:Account" as A
participant ":IAccount\nRepository" as AR



[o-> BSAC : addBankSavingsAccount\n(description)
activate BSAC
BSAC -> AMDTO : toAccountDTO(data)
activate AMDTO
AMDTO -> ADTO**:create(description)
deactivate AMDTO
BSAC -> AS: addBankSavingsAccount(accountDTO)
activate AS
AS -> A**: create(accountDTO)
AS -> AR: save(account)
activate AR
AR -> AR: addAccount(account)
return accountID
return accountID
deactivate AR
deactivate BSAC
@enduml
```
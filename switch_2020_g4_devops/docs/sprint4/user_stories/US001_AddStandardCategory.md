# US001
=======================================


# 1. Requirements

User Story: *As a System Manager, I want to add a standard category.*

In this US, we built this structure that allows us to create the standard categories.
These categories will identify every money transaction made on each family in the application to analyze monthly or annual spendings per category.


## 1.1 Client notes
According to the PO, this category type cannot be change or deleted by families, but families will be able to add and customize new categories.


## 1.2 Connection with others US
The US does not depend on other US. Although, [US002] and [US110]
depend on the functionality of this US.


# 2. Analysis

As a standard category, we assumed that SM has to fill some attributes that characterize it, such as:

| **_Attributes_**            | **_Business Rules_**                                                                                                                                    |
| :-------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **ID**                      | Mandatory, String type, unique for each category. The system generates automatically.                                                                   |
| **Name**                    | Mandatory, String type, cannot be empty or null.                                                                                                        |
| **ParentCategory**          | Category type. Identifies a subcategory related to the respectively parent one.                                                                         |

When category doesn't have a parent category, we assumed that the parent ID would be **-1**.

After creating the Standard Category, it will be store in Category Repository.

## 2.1 System Sequence Diagram

```plantuml 
autonumber
title **US001** SSD
actor "System Manager" as SM
participant "System" as Sys
SM -> Sys : create standard category
activate SM
activate Sys
Sys --> SM : asks data (i.e name)
deactivate Sys
SM -> Sys : inputs data (name)
activate Sys
Sys --> SM : informs result
deactivate Sys
deactivate SM
```

The result represents case of success or fail.

## 2.2 Process View

```plantuml 
autonumber
title **US001** Create Standard Category\n Process View
actor "System Manager" as SM
participant "FFM_UI" as UI
participant "FFM_BusinessLogic" as FL
SM -> UI : create standard category
activate SM
activate UI
UI --> SM : asks data (i.e name)
deactivate UI
SM -> UI : inputs data (name)
activate UI
UI -> FL : addStandardCategory(name)
activate FL
FL --> UI : result
deactivate FL
UI --> SM : shows result
deactivate UI
deactivate SM
```

# 3. Design

## 3.1. Class Diagrams

```plantuml
title **US001** Class Diagram

class "AddStandardCategoryController" as ctrl {
+ addStandardCategory(name) 
+ addStandardSubcategory(name, parentId)
}

class "MapperToDto" as mapper {
+ categoryToDto(name, parentId)
}

class "CategoryDTO" as dto {
}

class "CategoryService" as cs {
+ addStandardCategory(categoryDto)
+ addStandardSubcategory(categoryDto)
}

interface "ICategoryRepository" as irepo {
}

class "CategoryRepository" as repo {
+ hasSameNameAtSameLevel(parentId, name, isStandard)
+ generateCategoryId()
+ generateSubcategoryId(parentId)
+ save(category)
+ findByID(categoryId)
}

abstract "BasicCategory" as basic {
}
class "StandardCategory" as cat {
}

interface "Categorable" as able {
}

ctrl --> "1" mapper : mapper
ctrl --> "1" cs : categoryService
repo ..> able
basic ..|> able 
basic <|-- cat
cs --> "1" irepo : categoryRepository
repo ..|> irepo
mapper --> dto 
```
## 3.2. Functionality

The following Sequence Diagram represents the functionality required by this US.

```plantuml
title US001 AddStandardCategory


participant ":IAddStandardCategoryController" as ctrl
participant "mapper:Mapper" as mapper
participant ":ICategoryService" as aps
participant ":ICategoryRepository" as cr
autonumber
[o-> ctrl : addStandard\nCategory(name, parent)
activate ctrl
ctrl -> mapper : toCategoryDto(name, parent)
activate mapper
mapper --> "catDto : CategoryDto" ** : create(name, parent)
deactivate mapper
ctrl -> aps : addStandardCategory(catDto)
activate aps
aps -> "catDto : CategoryDto" : getName()
activate "catDto : CategoryDto"
"catDto : CategoryDto" --> aps : name
aps -> "catDto : CategoryDto" : getParentId()
"catDto : CategoryDto" --> aps : parentId
deactivate "catDto : CategoryDto"
aps -> cr : generateCategoryId()
activate cr
cr --> aps : id
aps -> cr : hasSameNameAtSameLevel(parentId, name, isStandard)
cr --> aps : boolean
deactivate cr
aps --> "newCategory : Category " ** : create(catDto)
aps -> cr : save(newCategory)
activate cr
deactivate cr
aps --> ctrl : id
deactivate aps
[<--o ctrl : id
deactivate ctrl
```


The SM requests to UI to create a new standard category.
UI replies asking for the name he wants to give to his new category.
After getting the attribute from SM, UI sends the request to the AddCategoryController.
This controller class has the responsibility to communicate with the FFMApplication, which returns the CategoryService that has the responsibility to create the new category.

Similar to this SD, we also have a method to addStandardSubcategory(). The only difference is that we need
to add a parentCategory to identify this type of category.

Going more detailed in addStandardCategory(name) method:


## 3.3. Applied Patterns

Different **GRASP** patterns were applied to better construct this US and to keep the information encapsulated on each class:

**The Controller Pattern:** AddStandardCategoryController Class was created only to build a "bridge" between the UI and the AppMembers.  For every actor's request, the controller has the responsibility of handling and forward the request.

**Information Expert:** Class Category has all information about categories and what they need to be created. This class follows this principle.

**Low Coupling and High Cohesion:** we built CategoryService regarding these two important principles.

**Pure Fabrication:** was used to create of class FFMApplication and CategoryService. These classes were
not part of the concepts of the problem but essential to coordinate the data and functions
of it.

## 3.4. Tests

In this section some tests will be presented that guarantee the requisites
of the US are applied.

**Test 1** : Creates a valid standard category.

    @Test
    @DisplayName("Creates a valid category that does not exist on list.")
    public void successAddingStandardCategoryUnitTest() {
        //ARRANGE
        String categoryName = "Health";
        String idExpected = "1";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setParentId("-1");
        Mockito.when(mapper.toCategoryDTO(categoryName, "-1")).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardCategory(categoryCreated)).thenReturn(idExpected);

        //ACT
        String id = controller.addStandardCategory(categoryName);

        //ASSERT
        assertEquals(idExpected, id);

    }
**Test 2** : The standard category is not created due to name with numbers.

    @Test
    @DisplayName("Testing try catch with category name wrong.")
    public void failAddingStandardCategoryDueToInvalidNameUnitTest() {
        //ARRANGE
        String categoryName = "123kgjrur";
        String invalidName = "Invalid argument";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setParentId("-1");

        Mockito.when(mapper.toCategoryDTO(categoryName, "-1")).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardCategory(categoryCreated)).thenReturn(invalidName);

        //ACT
        String id = controller.addStandardCategory(categoryName);

        //ASSERT
        assertEquals(invalidName, id);

    }

**Test 3** : The standard category is not created because already exists.

    @Test
    @DisplayName("Category already exists on list.")
        public void failAddingCategoryAlreadyExistsUnitTest() {
            //ARRANGE
            String categoryName = "Health";
            String errorExpected = "Category already exists";
            CategoryDTO categoryCreated = new CategoryDTO();
            categoryCreated.setName(categoryName);
            categoryCreated.setParentId("-1");
            Mockito.when(mapper.toCategoryDTO(categoryName, "-1")).thenReturn(categoryCreated);
            Mockito.when(categoryServiceMock.addStandardCategory(categoryCreated)).thenReturn(errorExpected);

            //ACT
            controller.addStandardCategory(categoryName);
            String realError = controller.addStandardCategory(categoryName);

            //ASSERT
            assertEquals(errorExpected, realError);
    
        }

Regarding Class Category and name validation, some tests were applied, such as:

**Test 1** : The standard category is not created due to name empty.

     @Test
     @DisplayName("Exception for creating an Empty Name Category")
     public void creatingCategoryWithNoName()
     {
        Throwable exception =
               assertThrows(IllegalArgumentException.class, () -> {
               Category noNameCategory = new Category(" ");});
     }


**Test 2** : The standard category is not created due to null name.

     @Test
     @DisplayName("Exception for creating a null Category")
        public void creatingCategoryWithNullName() {
        assertThrows(IllegalArgumentException.class, () -> {
        Category badCategory = new Category(null);
        });
        }

**Test 3** : Checks if category can be created with only two characters in name.

**Test 4** : Checks if category can be created with blank name.

**Test 5** : Checks if category can be created with numbers in name.

Also in Class Category we tested the method hasSameName(name):

**Test 1** : Checks when two categories have different names.

**Test 2** : Checks when two categories have the same name with upper case.

Not all are represented, but the most relevant are to assure this US is completely functional.

# 4. Implementation

Following the design, the AddStandardCategoryController gets the CategoryService from FFMApplication,
and it's ready to create a new standard category.

    public boolean createStandardCategory(String name) {

    CategoryService categoryService = this.app.getCategoryService();
    return categoryService.addStandardCategory(name, isStandard);
    }


In terms of category name validation, we used a **REGEX** validation to prevent
of Category's name having numbers and other special characters.
Also, to avoid repeated ID on categories list, we developed a method to check if id is already
on list, and it generates automatically when category is created:

    private String generateCategoryId() {

        int count = 0;
        for (Category category : categoryList) {
            if (category.getParentID().equals("-1")) { //only counts base categories
                count += 1;
            }
        }
        int lastDigit = count + 1;
        return "" + lastDigit;

    }


# 5. Observations

Overall, we managed to do this US without any relevant issue. We thought about Customized Category already, just to
differentiate them from Standard Category by using the CreatedByFamily attribute.


[US002]: /docs/sprint1/user_stories/US002_GetStandardCategoriesTree.md

[US110]: /docs/sprint1/user_stories/US110_GetListOfCategoriesOnFamilyCategoryTree.md

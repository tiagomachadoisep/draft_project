# 3. Design

## 3.1. Class Diagram

## 3.2. Functionality

```plantuml
autonumber
title US170 - Create Personal Cash Account Sequence Diagram

participant "controller\n:CreatePersonal\nCashAccountController" as CPCAC
participant "mapper\n:PrimitiveToDTO" as PDTO
participant ":IPersonal\nCashAccountService" as IPCAS
participant ":IPersonService" as IPS
participant ":IAccountService" as IAS
participant "personalCashAccountDTO\n:PersonalCashAccountDTO" as PCADTO
participant "email\n:Email" as E
participant "personID\n:PersonID" as PID
participant ":IAccountRepository" as IAR
participant "mapper2\n:DomainObjectToDatabase" as DODB
participant "accountData\n:AccountData" as AD

[o-> CPCAC : createPersonalCashAccount\n(email, description, balance)
activate CPCAC
CPCAC -> PDTO : toPersonalCashAccountDTO\n(email, description, balance)
activate PDTO
PDTO --> CPCAC : personalCashAccountDTO
deactivate PDTO
CPCAC -> IPCAS : createPersonalCashAccount\n(personalCashAccountDTO)
activate IPCAS
IPCAS -> IPS : checkIfEmailExists\n(personalCashAccountDTO)
activate IPS
ref over IPS
checkIfEmailExists(personalCashAccountDTO)
end ref
opt boolean == false
IPS --> IPCAS : false
IPCAS --> CPCAC : -1
[<--o CPCAC : -1
else
IPS --> IPCAS : true
deactivate IPS
IPCAS -> IAS : createPersonalCashAccount\n(personalCashAccountDTO)
activate IAS
IAS -> PCADTO : getEmail()
activate PCADTO
PCADTO --> IAS : emailString
deactivate PCADTO
IAS -> E ** : create(emailString)
IAS -> PID ** : create(email)
IAS -> IAR : hasPersonACashAccount(personID)
activate IAR
ref over IAR
hasPersonACashAccount(personID)
end ref
opt boolean == true
IAR --> IAS : true
IAS --> IPCAS : -1
IPCAS --> CPCAC : -1
[<--o CPCAC : -1
else
IAR --> IAS : false
deactivate IAR
IAS -> IAS : accountID=generateAccountID()
IAS -> IAS : personalCashAccount=\ntoPersonalCashAccount(personalCashAccountDTO, accountID)
ref over IAS
toPersonalCashAccount(personalCashAccountDTO, accountID)
end ref
IAS -> IAR : save(personalCashAccount)
activate IAR
IAR -> DODB : mapToData\n(personalCashAccount)
activate DODB
DODB -> AD ** : create\n(personalCashAccount)
deactivate DODB
deactivate IAR
IAS --> IPCAS : accountID
deactivate IAS
IPCAS --> CPCAC : accountID
deactivate IPCAS
deactivate AD
[<--o CPCAC : accountID

end opt
end opt

```


# US181
=======================================


# 1. Requirements

*As a family member, I want to register a payment that I have made
using my cash account.*

##1.1. Client Notes

Transactions of the electronic type (banks and credit/debit cards) will be automatically inputted in the system,
but payments made using cash will have to be manually introduced by the users.
Since we would like to establish a history of user's payments, and allow him to obtain a list of account
movements, these transactions will be registered by the system.

User's cash accounts can go to negative values, if the users submit payments of higher value than their current
cash account balance.

##1.2. System Sequence Diagram

```plantuml
autonumber
title SSD Register cash account payment
actor "Family Member" as FM
Participant System as S

FM -> S : register cash account payment
activate FM
activate S
S --> FM : show list of categories
deactivate S

FM -> S : choose one category
activate S
S --> FM : ask for data(date, value, description)
deactivate S

FM -> S : input data
activate S
S --> FM : payment registered
deactivate S
deactivate FM
```

##1.3. Process View

```plantuml
title Process View
actor "Family Member" as FM

Participant UI
Participant "FFM_BusinessLogic" as BL 
activate UI
activate FM
FM -> UI : register cash account payment
UI ->BL: checkUserHasCashAccount
activate BL
BL -->UI : true
deactivate BL
UI -> BL : getFamilyListOfCategories
activate BL
BL --> UI : listOfCategories
deactivate BL
UI --> FM : choose a category from list
deactivate UI
FM ->UI : choose from list
activate UI
UI --> FM : ask data (date, value,\ndescription)
deactivate UI
FM -> UI : inputs required data
activate UI

UI -> BL : registerCashPayment(transactionDTO)
activate BL
BL --> UI : result
deactivate BL
UI-> FM : shows result
deactivate UI
deactivate FM

```
##1.4. Connection with other User Stories

This US is related to [US170](US170_CreatePersonalCashAccount.md), where the user (Family Member) creates its own personal cash account, which he 
can use money from to perform payments. These payments will then be registered in the application using the method
described in this US.

# 2. Analysis

The basic flow we want to implement in this US goes through the following steps:
1. The family member (authenticated in the app) asks to register a payment he made using cash
2. The system asks for the relevant data (date of the payment, value, a brief description and a category)
3. The family member introduces the data, for example (21/01/2021, 10€, cinema ticket, entertainment)
4. The system validates the data (date not in the future, valid description, category can be used by the family, etc.) and registers the payment,  
   adding it to the list of payments made by that account and subtracting the value of the payment from the user's cash account funds
5. The family member is informed of the success of the operation

Possible occurrences:

If any of the required fields is invalid (step 4), the app will notify the user, the transaction will not be registered, and the balance of the 
account will not be changed. This can be because: the description is invalid, the value is negative or zero,
or the category specified is not in the list of categories of this family.

If the user doesn't have a cash account (step 1), the app will notify the user he can't perform this action
for that reason. (the app could also ask the user to create one if this happens)

If the payment has already been registered (same date, same value, same description), the app should warn the 
user of this fact and invalidate the registration.

##2.1 Relevant excerpt of the Domain Model

```plantuml 
@startuml Domain Model

object Family{
name
}

object Person{
id
name
}

object CashAccount{
balance
}

object CashPayment{
transactionDate
value
description
}

object Category{
id
name
}


Family "1" - "*" Person : has >
Person "1" - "1"CashAccount : owns >
CashAccount"1" - "*"CashPayment : registers >
CashPayment"*" - "1"Category : has >

@enduml
```
##2.2. Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **transactionDate**         | Mandatory, date of the transaction      |
| **value**                   | Mandatory, double, positive, value of the transaction (amount paid)       |
| **description**             | Mandatory, String, brief description of the transaction    |
| **category**                | Optional, Category, category which the transaction belongs to   |


# 3. Design



## 3.1. Functionality

The user Family Member, authenticated in the system, asks to register a cash payment he made using his personal
cash account. This request is done to the UI, who then asks the user to input the necessary data, date of the payment,
value of the payment and a brief description, as well as choose the category to which the transaction
belongs. To do this last part a list of categories of the user's family is presented, and he is asked
to choose one if he wants, otherwise this field is left empty.

The list of categories is obtained through the CategoryService of the application, a Service which
stores all the categories in the system, and knows which ones can be used by which families. The standard
ones (created by the SM) can be used by everyone, and the custom ones (created by the FA) can be used
only by a given family. The user will choose one available category or none and fill the rest with the 
necessary data outlined above and send the request back to the UI.

The UI then communicates this request to the controller through its registerCashPayment() method.
This method starts by getting the FamilyService, the class responsible for handling all families in our system,
and then obtains the correct family of the current user through its mainEmail attribute. In the object family
a list of all members is stored, and we access the correct instance of Person corresponding to the current user
again through its mainEmail. This instance of Person has an attribute of the type AccountList, a class 
responsible for storing and handling all operations related to the different types of accounts. The interface
Account is the template which all types of accounts follow, and CashAccount is an example of such.

If the user does not already have a CashAccount registered, at this point the system will stop and notify the user
of this problem (it can be implemented to ask the user to create one here). If he does have the account, the system
will access it and use the method registerPayment(), outlined below, to register the transaction made. This is
done by seeing if such a transaction already exists, indicating a possible error of double introduction of the
same transaction. If it does not, an instance of CashTransaction is created, this class implements the interface
Transaction, which sets the template for the transactions to be registered in our system from all different 
possible origins.

The instance payment of the class CashTransaction is finally added to the list of transactions stored in the class
that created it. 

If everything went well, the user is then informed of the success of his request.

```plantuml
@startuml
autonumber
title US181 : register cash payment
actor "Family Member" as FM
participant UI
participant ": RegisterCash\nPaymentController" as ctrl
participant ": FFM\nApplication" as app
participant "familyService :\nFamilyService" as serv
participant "categoryService :\nCategoryService" as cat
participant "family :\nFamily" as fam
participant "members :\nPersonList" as members
participant "member :\nPerson" as member
participant "accounts :\nAccountList" as accs
participant "cashAccount :\nCashAccount" as ca

activate FM
FM -> UI : register cash payment
activate UI
UI -> ctrl : checkUser\nHasCashAccount\n(userMainEmail)
activate ctrl
ctrl -> app : familyService=\ngetFamilyService()
ctrl -> serv : family=getFamilyByMemberMainEmail(userMainEmail)
ctrl -> fam : members=getMembers()
ctrl -> members : member=getPersonByMainEmail(userMainEmail)
ctrl -> member : accounts=getAccountList()
ctrl -> accs : check=hasCashAccount()


ctrl --> UI : check
opt check==false
deactivate ctrl
UI --> FM : user has no cash\naccount defined
end


UI -> ctrl : getListOfCategories\nOfFamily(userMainEmail)
activate ctrl

ctrl -> app : categoryService=\ngetCategoryService()
ctrl -> cat : categoryListDTO=getFamilyCategoryDTOList(family)
ctrl --> UI: categoryListDTO
deactivate ctrl
UI -->FM : show list of \ncategories
deactivate UI
FM -> UI : choose one 
activate UI
UI --> FM : ask for data \n(date, value,\ndescription)
deactivate UI
FM -> UI : input data
activate UI
UI -> ctrl : registerCashPayment\n(transactionDTO)
activate ctrl
ctrl -> accs : cashAccount=getCashAccount()

ctrl -> ca : result=registerPayment(transactionDTO)
activate ca
ref over ca : registerPayment\n(transactionDTO)
ca --> ctrl : result
deactivate ca
ctrl --> UI : result
deactivate ctrl
UI-->FM : payment registered
deactivate UI
deactivate FM


@enduml
```

```plantuml
@startuml
autonumber
title : registerPayment(transactionDTO)
participant ":CashAccount" as ca
participant "payment : CashMovement" as payment
participant "transactions : List<Transaction>" as list
participant " transaction : Transaction" as transaction
activate ca
[o-> ca : registerPayment\n(transactionDTO)
ca --> payment* : create (transactionDTO)

payment -> payment : validateDescription
activate payment
deactivate payment
loop for each transaction in transactions
ca -> transaction : result=equals(payment)
activate transaction
transaction --> ca : result
deactivate transaction
opt result==true
[<--o ca : failure
end
end
ca -> ca : addTransaction(payment)
[<--o ca : success
deactivate ca
@enduml
```

```plantuml
@startuml
autonumber
title US181 : register cash payment
actor "Family Member" as FM
participant UI
participant ": RegisterCash\nPaymentController" as ctrl
participant ": FFM\nApplication" as app
participant "familyService :\nFamilyService" as serv
participant "categoryService :\nCategoryService" as cat
participant "family :\nFamily" as fam
participant "members :\nPersonList" as members
participant "member :\nPerson" as member
participant "accounts :\nAccountList" as accs
participant "cashAccount :\nCashAccount" as ca

activate FM
FM -> UI : register cash payment
activate UI



UI -> ctrl : getListOfCategories\nOfFamily(userMainEmail)
activate ctrl

ctrl -> app : categoryService=\ngetCategoryService()
ctrl -> cat : categoryListDTO=getFamilyCategoryDTOList(family)
ctrl --> UI: categoryListDTO
deactivate ctrl
UI -->FM : show list of \ncategories
deactivate UI
FM -> UI : choose one 
activate UI
UI --> FM : ask for data \n(date, value,\ndescription)
deactivate UI
FM -> UI : input data
activate UI
UI -> ctrl : registerCashPayment\n(transactionDTO)
activate ctrl
ctrl -> app : familyService=\ngetFamilyService()
ctrl -> serv : family=getFamilyByMemberMainEmail(userMainEmail)
ctrl -> fam : members=getMembers()
ctrl -> members : member=getPersonByMainEmail(userMainEmail)
ctrl -> member : accounts=getAccountList()
ctrl -> accs : check=hasCashAccount()
opt check==false
ctrl --> UI : fail
UI --> FM : user has no cash\naccount defined
end
ctrl -> accs : cashAccount=getCashAccount()
ctrl -> ca : result=registerPayment(transactionDTO)
activate ca
ref over ca : registerPayment\n(transactionDTO)
ca --> ctrl : result
deactivate ca
ctrl --> UI : result
deactivate ctrl
UI-->FM : payment registered
deactivate UI
deactivate FM


@enduml
```
## 3.2. Class Diagram

```plantuml
@startuml

class RegisterCashPayment\nController{
+checkUserHasCashAccount\n(EmailAddress mainEmail)
+registerCashPayment\n(EmailAddress mainEmail,\nTransactionDTO transactionDTO)
}

class FFMApplication{
+getFamilyService()
+getCategoryService()
}

class FamilyService{
+getFamilyByUserMainEmail\n(EmailAddress mainEmail)
}

class Family{
+getMembers()
}

class PersonList{
+getPersonByMainEmail\n(EmailAddress mainEmail)
}

class Person{
-mainEmail : EmailAddress
+getAccountList()
}

class AccountList{
-listOfAccounts : List<Account>
+getCashAccount()
+hasCashAccount()
}

class CashAccount{
-balance : MoneyValue
+registerPayment(double value,\nDate date, String description)
}

class CashMovement{
+CashTransaction(double value,\nDate date, String description)
}

abstract class CashTransaction{
-value : MoneyValue
-transactionDate : Date
-description : String
+ validateDescription()
}

interface Transaction{
+isMovement()
}

interface Account{
+isCash()
}

class Category{
-name : String
-id : String
+canBeUsedByFamily\n(Family family)
}

class CategoryService{
+getCategoryById(String id)
+getFamilyCategoryListDTO\n(Family family)
}

RegisterCashPayment\nController "1" - "1" FFMApplication : app
FFMApplication"1" *- "1"FamilyService : famServ
FamilyService"1" o-- "*" Family : listOfFamilies
Family "1" *- "1" PersonList : personList
PersonList "1" o- "*" Person : listOfMembers

Person "1" *-- "1" AccountList : listOfAccounts
AccountList "1" o- "*" Account : accounts

Account <|.. CashAccount

CashMovement -|> CashTransaction

CashTransaction .|> Transaction

CashAccount "1" o-- "*" CashMovement : from 

CategoryService "1" o- "*" Category : listOfCategories 
FFMApplication "1" *-- "1" CategoryService : catServ
Category "*" - "*"Family : ownedBy
Category "1" -- "*"CashMovement : category <
@enduml
```

## 3.3. Applied Patterns

In the design process of this US we applied the patterns

- Information Expert: we assign the responsibilities related to objects
  to the class that has the information necessary to fulfil them. For example,
  the class CashAccount stores instances of CashTransaction, so it also
  responsible for telling us information about the transactions
  
- Creator: the class CashAccount aggregates instances of CashTransaction,
so it makes sense that it is responsible for creating them as well.
  
- Pure Fabrication: we introduce classes that do not correspond to any of
the concepts of the problem domain, but are useful to achieve the goals
  of the other design patterns (for example, the Controller and the Service
  classes).

- Controller: the class RegisterCashPaymentController is a bridge between
the UI and our system, and has the responsibility of dealing with system 
  events, in this case registering a payment made using the user's cash account.
  
- Low Coupling/High Cohesion: this pattern consists of keeping the classes
focused, with responsibilities that are related. This is accomplished 
  by keeping classes weakly coupled, that is, weakly connected to other 
  classes, limiting the knowledge and reliance the class has with other
  elements of the system. In this case, we split the relationships so that,
  for example, the operations related to the accounts are dealt with by the class
  AccountList, leaving the class Person with a more focused set of responsibilities,
  one of which is directing to the AccountList class.
  
- Protected Variation/Polymorphism: since we will be able to, in this application,
import transactions from different systems (for example, e-fatura or specific
  bank systems), we want to make a sort of template for the transactions. 
  That is, we require each class responsible for handling transactions to 
  provide a set of business methods that allow us to extract information
  we need for a specific use case scenario. This is done through the introduction
  of an interface, which all the classes handling the different types of transactions
  will implement. The details on how each of them handles each method are
  irrelevant to us, as long as the information is being provided according to 
  the template. Although the methods may have the same name, their implementation
  will be different in each of the types of transaction.

## 3.4. Tests 

In order to perform the following tests, we initialize an application with this data: one family with two members,
one of which is the admin and has no cash account defined, and the other are members, one with a cash account with 
positive balance, and the other with a cash account with negative balance. We also provide 4 different 
categories, two of which are standard (available for all possible families), and two are from the family
created before. These are the *car* and *house* standard categories, and *others* and *renault* as 
customized categories, where the latter is a subcategory of *car*.

````
@BeforeAll
    static void initializeApplication(){
        app = new FFMApplication();
        FamilyService familyService = app.getFamilyService();
        Family santos=familyService.addFamily("Santos",new Date());
        CategoryService categoryService = app.getCategoryService();

        casa = categoryService.addStandardCategory("Casa");
        carro = categoryService.addStandardCategory("Carro");
        outros = categoryService.addCustomizedCategory("Outros",santos);
        renault = categoryService.addCustomizedSubcategory("Renault",carro,santos);

        PersonList personList = santos.getPersonList();
        joseEmail = "ze@isep.pt";
        Person memberJose= new Person("Jose","19913200 3ZY1",new Date(), joseEmail,"male");
        adminEmail = "carlos@isep.pt";
        Person adminCarlos = new Person("Carlos","19741658 6ZV2",new Date(),adminEmail,"male");
        anaEmail ="ana@isep.pt";
        Person memberAna=new Person("Ana","19579142 8ZX2",new Date(), anaEmail,"female");
        personList.addMember(memberJose);
        personList.addMember(adminCarlos);
        personList.addMember(memberAna);

        AccountList accountListJose = memberJose.getPersonAccountList();
        CashAccount cashAccount = new CashAccount(100);
        accountListJose.addAccount(cashAccount);

        AccountList accountListAna = memberAna.getPersonAccountList();
        CashAccount cashAccountThree = new CashAccount(-20);
        accountListAna.addAccount(cashAccountThree);
    }
````
**Test 1**: Register a cash payment of a given value (50€) made by a family member with a cash account with a balance of 1000€, described by *groceries*, on the system date and with a category *Home* - ***pass***
````
@Test
    void registerPaymentUsingCashAccountTest(){

        //Arrange
        RegisterMemberPaymentUsingCashAccountController ctrl = new RegisterMemberPaymentUsingCashAccountController(app);
        //Act
        boolean result = ctrl.registerCashPayment(joseEmail,new Date(),50,"pagamento multa",renault);
        //Assert
        assertTrue(result);
    }
````
**Test 2**: Register a cash payment made by a family member without a cash account defined - ***fail***
````
@Test
    void registerPaymentNoCashAccountTest(){
        //Arrange
        RegisterMemberPaymentUsingCashAccountController ctrl = new RegisterMemberPaymentUsingCashAccountController(app);
        //Act
        boolean result = ctrl.registerCashPayment(adminEmail,new Date(),10,"mercearia",casa);
        //Assert
        assertFalse(result);
    }
````
**Test 3**: Register a cash payment with an invalid description (empty) - ***fail***
````
@Test
    void registerPaymentUsingCashAccountInvalidDescriptionTest(){

        //Arrange
        RegisterMemberPaymentUsingCashAccountController ctrl = new RegisterMemberPaymentUsingCashAccountController(app);
        //Act
        boolean result = ctrl.registerCashPayment(joseEmail,new Date(),50,"",outros);
        //Assert
        assertFalse(result);
    }
````
**Test 4**: Register a cash payment of a value higher than the current balance of the user cash account (the balance will become negative) - ***pass***
````
@Test
    void registerPaymentUsingCashAccountValueHigherThanBalanceTest(){
        //Arrange
        RegisterMemberPaymentUsingCashAccountController ctrl = new RegisterMemberPaymentUsingCashAccountController(app);
        //Act
        boolean result = ctrl.registerCashPayment(joseEmail,new Date(),500,"new tv",casa);
        //Assert
        assertTrue(result);
    }
````
**Test 5**: Register a cash payment with a negative value - ***fail***
````
@Test
    void registerPaymentUsingCashAccountNegativeValueTest(){
        //Arrange
        RegisterMemberPaymentUsingCashAccountController ctrl = new RegisterMemberPaymentUsingCashAccountController(app);
        //Act
        boolean result = ctrl.registerCashPayment(joseEmail,new Date(),-20,"encontrei 20euros no chao",outros);
        //Assert
        assertFalse(result);
    }
````
**Test 6**: Register a cash payment with the exact same values as an existing one for the same user - ***fail***
````
@Test
    void registerPaymentUsingCashAccountRepeatedTest(){
        //Arrange
        RegisterMemberPaymentUsingCashAccountController ctrl = new RegisterMemberPaymentUsingCashAccountController(app);
        //Act
        ctrl.registerCashPayment(joseEmail,new Date(2020,1,1),20,"pagamento multa",renault);
        boolean result = ctrl.registerCashPayment(joseEmail,new Date(2020,1,1),20,"pagamento multa",renault);
        //Assert
        assertFalse(result);
    }
````
# 4. Implementation

In this project, we implement the payments using the interface Transaction. This interface serves as a sort of 'template class'
for all transactions in the system, both manually inserted by the users and imported from external sources like financial entities.
Interfaces generally answer the question of what the objects are supposed to do, leaving how to do it to the implementation of each 
specific class.

In order to do that, we establish some methods (just their signature) in the interface Transaction, and they will have to be 
implemented in all the classes responsible for transactions, each in their own way of course.
We will show the main classes involved in this US as well as the most relevant methods.
````
public interface Transaction {
    double getValue();
    Date getDate();
    boolean isMovement();
    boolean isTransfer();
    String getDescription();
    boolean isDateBetween(Date dateOne, Date dateTwo);
    boolean isOfCategory(Category category);
    Category getCategory();
    boolean isCashTransaction();
    boolean isFinancialEntityTransaction();
    TransactionDTO toDTO();
}
````
As we can see, the methods describe what kind of information the classes will have to provide when they are called.

In this US we are dealing with cash transactions only, and they can be of two types: a transfer, which corresponds to a transfer 
of money between two accounts registered in the system with defined cash accounts; and a movement, which is only within one account, 
and corresponds to the deposit or withdrawal of money in that account, for example for an external payment. Since these two transactions,
despite their differences, share some similarities, we decided to create an abstract class to implement the common methods define the common
attributes, entitled CashTransaction, which is a superclass of both the classes responsible for cash transactions.
````
public abstract class CashTransaction implements Transaction {

    protected MoneyValue value;
    protected Date transactionDate;
    protected String description;
    protected Category category;

    @Override
    public double getValue() {
        return this.value.getValue();
    }

    @Override
    public boolean isDateBetween(Date dateOne, Date dateTwo) {
        return transactionDate.after(dateOne) && transactionDate.before(dateTwo);
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Date getDate() {
        return (Date) this.transactionDate.clone();
    }


    public boolean isOfCategory(Category cat) {
        return Objects.equals(this.category, cat);
    }

    @Override
    public Category getCategory() {
        return this.category;
    }

    protected void validateDate(Date transactionDate) {
        if (transactionDate == null)
            throw new IllegalArgumentException("Date can't be null");
        if (transactionDate.after(new Date()))
            throw new IllegalArgumentException("Date cannot be in the future");
    }

    @Override
    public boolean isCashTransaction() {
        return true;
    }

    @Override
    public boolean isFinancialEntityTransaction() {
        return false;
    }

}
````
This class implements some methods of the interface Transaction, while leaving (more specific) others to be implemented
by its subclasses. Next the class CashMovement is presented, corresponding to the kind of transaction treated here.
````
public class CashMovement extends CashTransaction {

    private final Account account;


    public CashMovement(Account account, MoneyValue value, Date paymentDate, String description, Category category){
        validateAccount(account);
        this.account = account;
        this.value = value;
        this.transactionDate = (Date) paymentDate.clone();
        validateDescription(description);
        this.description = description;
        this.category = category;
    }

    @Override
    public boolean isMovement() {
        return true;
    }

    @Override
    public boolean isTransfer() {
        return false;
    }

    @Override
    public TransactionDTO toDTO() {
        return new TransactionDTO(account.toDTO(), null, transactionDate, description, value.getValue(), category.toDTO());
    }

    public boolean isPayment(){
        return value.getValue()<=0;
    }

    public boolean isDeposit(){
        return value.getValue()>0;
    }
}
````
This overrides the remaining methods and implements new ones, necessary for this class, which corresponds to the concept present in this US.

In order to communicate with this domain class, we use a controller class, to perform the actions requested by the US. This serves
as a bridge between the UI and the system.
````
public class RegisterMemberPaymentUsingCashAccountController {

    private final FFMApplication app;

    public RegisterMemberPaymentUsingCashAccountController(FFMApplication app){
        if(app==null)
            throw new IllegalArgumentException("AppMembers cannot be null");
        this.app=app;
    }

    public boolean registerCashPayment(String userMainEmail, TransactionDTO){
        FamilyService familyService = this.app.getFamilyService();
        Family family = familyService.getFamilyByMemberMainEmail(userMainEmail);
        PersonList members = family.getPersonList();
        Person member = members.getPersonByMainEmail(userMainEmail);
        AccountList accountList = member.getPersonAccountList();
        try{
        CashAccount cashAccount = accountList.getCashAccount();
        return cashAccount.registerPayment(TransactionDTO);
        }
        catch (Exception e){return false;}
    }
}
````
The account where the action is being taken is responsible for both instancing and storing the transactions, and that is 
the cash account of the current user, stored in the instance of AccountList associated with him. We saw the controller following 
the steps necessary to get to this class, asking the family for the person, the person for the account list, and the account list
for the correct account.
````
public class CashAccount implements Account {

    private MoneyValue moneyBalance;
    private final String description;
    private final List<Transaction> transactionList;

    private void addFunds(MoneyValue value) {
        this.moneyBalance=this.moneyBalance.sum(value);
    }
    
    public boolean registerPayment(Date transactionDate, double amount, String description) {
        if(amount<0)
            return false;
        CashMovement payment = new CashMovement(this, new MoneyValue(-amount), transactionDate, description);
        for (Transaction t : transactionList) {
            if (t.equals(payment))
                return false;
        }
        MoneyValue value = new MoneyValue(amount);
        removeFunds(value);
        transactionList.add(payment);
        return true;
    }
````
With this, we have all the necessary framework to create and store a payment, with the data provided by the user.
# 5. Integration/Demonstration

This US is connected with several of the USs of this sprint, [US180](US180_TransferMoneyFromCashAccountToCashAccount.md) is similar to this one but concerning transfers instead
of movements, in [US170](US170_CreatePersonalCashAccount.md) the cash account of the users, necessary to perform transactions, are created, and on [US186](US186_GetTransactionsBetweenDates.md) the user
is presented with a list of transactions made by one of his accounts, for example his cash account.

# 6. Observations
In this sprint the focus was largely on accounts and transactions involving those accounts. With the introduction of the
patterns of inheritance and polymorphism, we saw this is a great opportunity to implement those patterns in our design. In this 
US the focus was on transactions.

We started by grouping all transactions in the same class, implementing the interface Transaction, but then realized it would
be better to separate them into transfers and movements, indicating the difference between transactions involving only the user's
account, and the ones involving two accounts, one sending and one receiving. Despite their differences they share a
lot of common attributes and methods, so we created an abstract superclass that both extend, and grouped the common ground 
there, to avoid the duplication of code.

After this, there was a decision as to where the instances of each transaction should be created, and then stored. Following
the design patterns, we decided to attribute this responsibility to the account involved, since it will know the details of the transaction
and when, in other US, it is asked for a list of transactions of one account, it is stored where it was created.
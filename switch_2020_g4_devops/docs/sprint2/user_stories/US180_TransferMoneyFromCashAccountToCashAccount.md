# US180
=======================================


# 1. Requirements
*As a Family Member, I want to transfer money from my cash account to another family member's cash account.*
 

##1.1 Client Notes
The following note were acquired through information provided by the PO:
- Each family member may have a cash account to represent the amount of cash he/she has.
- Each cash account must have a description.
- The actor may introduce a date that don't correspond to the exact registry date.

##1.2 System Sequence Diagram

Next is the system sequence diagram realized for this US.

```plantuml
title SSD Transfer money from my cash account\n to another family member's cash account

autonumber
hide footbox
actor "Family Member" as FM
activate FM

Participant System

FM -> System : transfer money from my cash account \nto another family member's cash account
activate System

System --> FM : ask for data (value, transferDate, \nmemberToMainEmail, description)
deactivate System

FM -> System : input data
activate System
System --> FM : inform result
deactivate System

deactivate FM
```

##1.3. Process view

```plantuml
title Process View

autonumber
hide footbox
actor "Family Member" as FM
activate FM

Participant UI

FM -> UI : transfer money from my cash account \nto another family member's cash account
activate UI

Participant FFM_BusinessLogic as FFMBL

UI --> FM : ask for data (value, transferDate, \ndescription, memberToMainEmail)
deactivate UI

FM -> UI : input data
activate UI
UI -> FFMBL : transferMoneyFromMyCashAccount ToAnotherFamilyMember'sCashAccount \n(value, transferDate, memberToMainEmail, memberFromMainEmail, description)
activate FFMBL
FFMBL --> UI : result
deactivate FFMBL
UI --> FM : inform result
deactivate UI

deactivate FM
```


##1.4. Connections with other US
This US relies on several USs. For this to work fist a family has to be created ([US010](../../sprint1/user_stories/US010_CreateFamily.md)), then, family members could be added ([US101](../../sprint1/user_stories/US101_AddFamilyMember.md)). As the only actor that, so far, can do this is the administrator, actual US depend on [US011](../../sprint1/user_stories/US011_AddFamilyAdministrator.md) to add family administrator. For last, each involved member has to have a cash account, so, to create them the [US170](US170_CreatePersonalCashAccount.md) is needed.

On the other hand, there are other USs that are or may be reliant on this. It is the case of [US185](US185_CheckAccountBalance.md) in the case of checking a cash account, [US186](US186_GetTransactionsBetweenDates.md) in the same case, [US188](US188_AsParentCheckBalanceOfOneChildrenCashAccount.md) and [US135](US135_CheckBalanceOfFamilyOrMemberCashAccount.md), because the result of each referred US will vary if a cash transfer is performed.



# 2. Analysis
Through this US the FM pretends to transfer an amount of money from his cash account to another family member cash account.

According to the PO each cash transfer has a date of transfer that might, or might not, be the same as transfer registration date. For example, nothing prohibits the member could register the transfer with a date in the past.

In order to distinguish between family members a unique person identifier must be introduced. Also, to facilitate the user's job, when creating a cash transfer a transaction should be automatically added to the transaction's list of the sender and money recipient.

Other aspect that had to be considered was where to store individual cash account and respective transaction registries. The team decided to store a list of accounts in the Person class, which may have each kind of account, namely, the cash account that is the account that matters the most to this US. By doing so, the account is dependent on the Person to whom it belongs. In other US the squad decided to hold the Person responsible for keeping the list of transactions. The same reason is applied here.

For last, the group developed two validations to prevent strange, erroneous or misleading information. With that being said, we added the verification of the existence of the cash accounts and  validation of the amount transferred, i.e., amount can not be smaller or equals than null, or greater than the cash account balance.




## 2.1. Attributes
| **_Attributes_**          | **_Business Rules_**                                               |
| :-------------------------| :------------------------------------------------------------------|
| **Value**                 | Mandatory, double, money value bigger than zero, not empty or null     |
| **TransferDate**          | Mandatory, date, not empty or null     |
| **MemberToMainEmail** | Mandatory, text, not empty or null |
| **MemberFromMainEmail** | Mandatory, text, not empty or null |
| **Description** | Mandatory, text, not empty or null |

## 2.2. Relevant excerpt of the Domain Model
In this subchapter we present the relevant excerpt of the Domain Model for this US.

```plantuml

title Domain Model excerpt
'skinparam linetype ortho
object "CashTransfer" as T{
date
value
description
}

object Account {
}

object "Cash Account" as CA {
moneyBalance
transactionList
}

object "Family" as fam {
}

object Person {
mainEmail
listOfAccounts
}

fam "1" - "1..*" Person : has list >
Person "1" -- "0..*" Account
T "*" - "1" Account : accountFrom
T "*" - "0..1" Account : accountTo
CA -left- Account : contains <



```


# 3. Design

## 3.1. Functionality
The FM, authenticated in the system, sends the information to the UI that wants to transfer a certain amount of money from his cash account to another family member's cash account. As referred before, there is some data required to operate this process, thus, the UI asks the user the transfer date, the amount to be transferred, the family member's main email and a description of the transfer.

This information is, then, passed to the "Transfer money from cash account controller" along with the user's main email.

Then the controller starts to operate a series of requests, they are:

- to the FFMApplication asks for the family service, the class responsible for handling all families in our system;
- to the family service asks for the user's family, that it is searched by the user's main email;
- in the user's family is stored the list of members for that specific family;
- through this list reaches the two person involved;
- in each one there is a corresponding accounts list, then, each cash account. Here is performed the first validation. If any of the participants does not have a cash account que operation fails, and the result is sent throughout the program till it reaches the user;

Now the controller has all the information required and initiates the request to transfer money from an individual cash account. Thus, it instructs the user's cash account to do so, passing the amount, description, toCashAccount and transferDate. With the value it is created a money value, then validated. If the result returns true the operations continues, else it is stopped. Next the amount is subtracted from the user's cash account, and added to the family member's. Lastly, a cash transaction is added to each participant's transactions list, with information about the description, value, accountTo, accountFrom and transfer date.

Finally, the result travels back to the FM following the reverse path, being shown to the user.

The group designed the next SD that sums up the previously explanation. It is noteworthy this diagram only represents the success case, otherwise, it would become too large and difficult to interpret.


```plantuml
title US181 - Transfer money from my cash account\n to another family member's cash account

autonumber

actor "Family Member" as FM
activate FM
Participant ":UI" as UI
Participant ":TransferMoney\nFromCash\nAccountController" as Controller
Participant ":FFM\nAplication" as AppMembers
Participant "family\nService :\nFamily\nService" as FS
Participant "family : \nFamily" as Fam
Participant "family\nMembers \n: Pernson\nList" as PL
Participant "user : \nPerson" as P
Participant "user\nAccountsList \n: AccountList" as AL
Participant "family\nMember \n: Person" as FP
Participant "familyMember\nAccountsList \n : AccountList" as FAL
'Participant "familyMember\nCashAccount \n: CashAccount" as FCA
Participant "userCash\nAccount \n: CashAccount" as CA


FM -> UI : transfer money from my cash account \nto another family member's cash account
activate UI
UI --> FM : ask for data (value, transferDate, \ndescription, memberToMainEmail)
deactivate UI

FM -> UI : inputs data
activate UI

UI -> Controller : transferMoneyFromMyCashAccount ToAnotherFamily\nMember'sCashAccount (value, transferDate, memberTo\nMainEmail, memberFromMainEmail, description)
activate Controller 

Controller -> AppMembers : familyService =\ngetFamilyService( )
'activate AppMembers
'AppMembers --> Controller : familyService
'deactivate AppMembers

Controller -> FS : family = getFamilyByMainEmail\n(memberToMainEmail)
'activate FS
'FS --> Controller : family
'deactivate FS

Controller -> Fam : familyMembers = getPersonList()
note left : From 7 to 13 must desappear \nAsk family to give the account based \n\ton the email of each person 
'activate Fam
'Fam -> Controller : familyMembers
'deactivate Fam

Controller -> PL : user = getPersonByMainEmail(memberFromMainEmail)
'activate PL
'PL --> Controller : user
'deactivate PL

Controller -> P : userAccountsList = getPersonAccountsList()
'activate P
'P -> Controller : accountsList
'deactivate P

Controller -> AL : userCashAccount = getCashAccount( )
'activate AL
'AL --> Controller : userCashAccount
'deactivate AL

Controller -> PL : familyMember = getPersonByMainEmail(memberToMainEmail)
'activate PL
'PL --> Controller : familyMember
'deactivate PL

Controller -> FP : familyMemberAccountsList = getPersonAccountList()
'activate FP
'FP --> Controller : familyMemberAccountList
'deactivate FP

Controller -> FAL : familyMemberCashAccount = getCashAccount()
'activate FAL
'FAL --> Controller : familyMemberCashAccount
'deactivate FAL

Controller -> CA : transferMoneyFromCashAccount(value, description, userCashAccount, familyMemberCashAccount, transferDate)
activate CA
ref over CA : transferMoneyFromCashAccount(value,\ndescription, selfCashAccount, family\nMemberCashAccount, transferDate)

CA --> Controller : result
deactivate CA

Controller --> UI : result
deactivate Controller

UI --> FM : inform result
deactivate UI

deactivate FM
```
Going more detailed in transferMoneyFromCashAccount method. This diagram already represents failure cases.

```plantuml
hide footbox
autonumber
title transferMoneyFromCashAccount(value, description, \nselfCashAccount, familyMemberCashAccount, transferDate)

Participant "cashAccount :\nCashAccount" as CA
Participant "familyMemberCashAccount\n: CashAccount" as FCA
Participant "moneyValue \n: MoneyValue" as MV
Participant "cashTransaction\n: CashTransfer" as T
'Participant "newUserMoneyValue \n: MoneyValue"  as UMV
'Participant "newFamilyMemberMoneyValue \n: MoneyValue"  as FMV

[o->CA : transferMoneyFromMyCashAccount(value, description, \nuserCashAccount, familyMemberCashAccount, transferDate)

activate CA

CA -> MV ** : moneyValue = create (value)

CA -> CA : result = validateTransfer\n            (moneyValue)
activate CA
deactivate CA
opt result == false
[<--o CA : failure
end

CA -> T ** : cashTransaction = create (userCashAccount, familyMemberCash\nAccount, moneyValue, transferDate, description)

CA -> CA : addTransaction(cashTransaction)
activate CA
deactivate CA
CA -> FCA : addTransaction(cashTransaction)

activate FCA
FCA --> CA : boolean
deactivate FCA

[<--o CA : succes

deactivate CA

```
Next we present a more detailed view of addTransaction method. As the last one, this diagram represents failure cases.

```plantuml
autonumber
title addTransaction (cashTransaction)

Participant ": CashAccount" as UCA
participant "cashTransaction \n: Transaction" as T

participant "transactionList :\nList<Transaction>" as list
[o->UCA : addTransaction (cashTransaction)
activate UCA

UCA -> T : isCashTransaction(cashTransaction)
activate T
T -> UCA : result
deactivate T
opt result==false

[<--o UCA : false
end

UCA -> T : isTransfer(cashTransaction)
activate T
T --> UCA : result
deactivate T

UCA -> UCA : addTransactionTransfer(cashTransaction)
activate UCA #motivation

opt  result==true

UCA -> T : getMoneyValue()
activate T

T --> UCA : value
deactivate T

UCA -> T : isOrigin = isAccountFrom()
activate T
T --> UCA : result
deactivate T

UCA -> T : isDestination = isAccountTo()
activate T
T-->  UCA : result
deactivate T

alt isOrigin

UCA -> UCA : removeFunds(value)
UCA -> list : add(cashTransaction)
activate list
list --> UCA : true
deactivate list
[<--o UCA : true

else isDestination

UCA -> UCA : addFunds(value)
UCA -> list : add(cashTransaction)
activate list
list --> UCA : true
deactivate list

deactivate UCA

[<--o UCA : true

end
[<--o UCA : false

end
deactivate UCA
```


## 3.2. Class Diagram

In the following we present the classes involved in this US and the way they interact through the exhibition of a class diagram.


```plantuml
skinparam linetype ortho
title **US180** - Transfer money from my cash account \nto another family member's cash account

class "TransferMoneyFromCashAccountController" as Controller {
- app : FFMApplication
+ transferMoneyFromMyCashAccountTo Another\nFamilyMembersCashAccount(double value, Date\ntransferDate, String memberToMainEmail, String\nmemberFromMainEmail, String description) : boolean
}

class "FFMApplication" as app {
- familyService : FamilyService
+ getFamilyService() : FamilyService
}

class "FamilyService" as FS {
- families : List <Family>
+getFamilyByMemberMainEmail (String\nmemberFromMainEmail) :  Family
}

class "Family" as F {
-members : PersonList
+getPersonList() : Person List
}

class "PersonList" as PL {
-members : List<Person>
+getPersonByMainEmail(memberFromMainEmail) : Person
+getPersonByMainEmail(memberToMainEmail) : Person
}

class "Person" as P{
-listOfAccounts : List<Account>
+getPersonAccountList() : List<Account>
}

class "AccountsList" as AL{
-listOfAccounts : List<Account>
+getCashAccount() : CassAccount
}

interface "Account" as A{

}

class "CashAccount" as CA{
-transactionList : List<Transaction>
+getCashAccount() : CashAccount
+isCash() : boolean
+transferMoneyFromCashAccount(double\namount, String description, CashAccount\nfromCashAccount,CashAccount toCash\nAccount, Date transferDate) : boolean
-validateTransfer(value)
-removeFunds(value)
-addValues(value)
+add(cashTransaction) : boolean
}

interface "Transaction" as T

abstract "CashTransaction" as CTN{
#transactionDate : Date
#description : String
#category : Category
}

class "CashTransfer" as CT{

}

class "MoneyValue" as MV{
value : double
currency : Currency
}

A "*" -* "1" AL

P "1" *-left- "1" AL

PL "1" *-left- "*" P

F "1" *-- "1" PL

FS "1" o-right- "*" F

app "1" *-right- "1" FS

Controller "1" -right- "1" app

A <|.. CA

CA "1" *-down- "1" MV

CA "1" -right "*" CT : from

CA "1" -right "*" CT : to

CT -down-|> CTN

CTN "1" *-left- "1" MV

CTN ..|> T

```

## 3.3. Applied Patterns
The patterns applied on this US are:

**Controller** - concerning to the Transfer money from cash account controller. It has the responsibility of handling and forward the request of the UI. 

**Information Expert** - we store the information needed to fulfill the task in the same class responsible for that task, in this case, cash account class stores information about the account and operates the method to accomplish the functionality.

**Low Coupling and High Cohesion** - by maintaining the information encapsulated, limiting the knowledge and reliance the class has with other elements of the system, we are also maintaining low coupling and high cohesion, encouraging class definitions that are easier to understand and maintain.

**Pure fabrication** - by creating classes that does not directly represent concepts of the problem, but exist to help coordinate the program. In this case, the ones used are FFMApplication and FamilyService.

**Creator** - the class responsible to store cash account is also the responsible to create. For example, CashAccount stores instances of CashTransaction, so, is responsible to create them as well.

**Protected Variation and Polymorphism** - in order to protect the system against variations, we encapsulate the behavior in a generic interface, and the system deal with it without the need to know  who, how or where that is made. We create interfaces Transaction and Account. Each one have classes that implement them differently, depending on the requirements.


## 3.4. Tests
Next we present tests we performed to guarantee the requirements of this US are achieved.

**Test 1**: Transfer money from my cash account with null Application.

**Test 2**: Transfer money unsuccessfully to non-existing family member cash account

**Test 3**: Transfer money unsuccessfully due to non-existing own cash account

**Test 4**: Transfer money unsuccessfully due to bigger value than balance

    @DisplayName("Transfer money unsuccessfully due to bigger value than balance")
    @Test
    void transferMoreThanAvailableMoneyFromMyCashAccount() {
        //ARRANGE
        List<Person> members = new ArrayList<>();

        String luisEmail = "luis@abc.pt";

        Person luis = new Person("Luis","13686553 4ZZ8", new Date(1991 + 1900), luisEmail, "male");
        AccountList luisAccountsList = luis.getPersonAccountList();
        CashAccount luisCashAccount = new CashAccount("Luis cash account", 100);
        luisAccountsList.addAccount(luisCashAccount);

        String saraEmail = "sara@abc.pt";
        Person sara = new Person("Sara", "13686553 4ZZ8", new Date(1994 + 1900), saraEmail, "female");
        AccountList saraAccountsList = sara.getPersonAccountList();
        CashAccount saraCashAccount = new CashAccount("Sara cash account", 300);
        saraAccountsList.addAccount(saraCashAccount);

        members.add(luis);
        members.add(sara);

        PersonList familyMembers = new PersonList (members, luisEmail);
        Family newFamily = new Family("Silva", new Date(), 1, familyMembers);
        List<Family> families = new ArrayList<>();
        families.add(newFamily);
        FamilyService familyService = new FamilyService(families);
        FFMApplication app = new FFMApplication(familyService);
        TransferMoneyFromCashAccountController transferMoney = new TransferMoneyFromCashAccountController(app);

            //expected values
        double expectedLuisBalance = 100.0;
        double expectedSaraBalance = 300.0;

        //ACT
            //Transferência do sara para luis
        boolean result = transferMoney.transferMoneyFromMyCashAccountToAnotherFamilyMembersCashAccount(500, new Date() , luisEmail, saraEmail, "Pharmacy payment");
        double saraBalance = saraAccountsList.getCashAccount().getMoneyBalance().getValue();
        double luisBalance = luisAccountsList.getCashAccount().getMoneyBalance().getValue();

        //ASSERT
        assertFalse(result);
        assertEquals(expectedLuisBalance, luisBalance);
        assertEquals(expectedSaraBalance, saraBalance);
        assertTrue(luisAccountsList.getCashAccount().getTransactionsList().size() == 0);
        assertTrue(saraAccountsList.getCashAccount().getTransactionsList().size() == 0);
    }

**Test 5**: Transfer money unsuccessfully due to invalid value

    @DisplayName("Transfer money from member cash account unsuccessfully due to invalid value")
    @Test
    void transferInvalidValueFromMyCashAccount() {
        List<Person> members = new ArrayList<>();

        String luisEmail = "luis@abc.pt";
        Person luis = new Person("Luis", "18480344 6ZY2", new Date(1991 + 1900), luisEmail, "male");
        AccountList luisAccountsList = luis.getPersonAccountList();
        CashAccount luisCashAccount = new CashAccount("Luis cash account", 100);
        luisAccountsList.addAccount(luisCashAccount);

        String saraEmail = "sara@abc.pt";

        Person sara = new Person("Sara", "13686553 4ZZ8", new Date(1994 + 1900), saraEmail, "female");
        AccountList saraAccountsList = sara.getPersonAccountList();
        CashAccount saraCashAccount = new CashAccount("Sara cash account", 300);
        saraAccountsList.addAccount(saraCashAccount);

        members.add(luis);
        members.add(sara);

        PersonList familyMembers = new PersonList (members, luisEmail);
        Family newFamily = new Family("Silva", new Date(), 1, familyMembers);
        List<Family> families = new ArrayList<>();
        families.add(newFamily);
        FamilyService familyService = new FamilyService(families);
        FFMApplication app = new FFMApplication(familyService);
        TransferMoneyFromCashAccountController transferMoney = new TransferMoneyFromCashAccountController(app);

            //expected values
        double expectedLuisBalance = 100.0;
        double expectedSaraBalance = 300.0;

        //ACT
            //Transferência do sara para luis
        boolean result = transferMoney.transferMoneyFromMyCashAccountToAnotherFamilyMembersCashAccount(-100, new Date() , luisEmail, saraEmail, "Pharmacy payment");
        double saraBalance = saraAccountsList.getCashAccount().getMoneyBalance().getValue();
        double luisBalance = luisAccountsList.getCashAccount().getMoneyBalance().getValue();

        //ASSERT
        assertFalse(result);
        assertEquals(expectedLuisBalance, luisBalance);
        assertEquals(expectedSaraBalance, saraBalance);
        assertTrue(luisAccountsList.getCashAccount().getTransactionsList().size() == 0);
        assertTrue(saraAccountsList.getCashAccount().getTransactionsList().size() == 0);
    }

**Test 6**: Transfer money successfully

    @DisplayName("Transfer money from personal cash account successfully")
    @Test
    void transferMoneyFromMyCashAccountToAnotherFamilyMembersCashAccountSuccessfully() {
        //ARRANGE
        List<Person> members = new ArrayList<>();

        String luisEmail = "luis@abc.pt";

        Person luis = new Person("Luis", "13686553 4ZZ8", new Date(1991 + 1900), luisEmail, "male");
        AccountList luisAccountsList =luis.getPersonAccountList();
        CashAccount luisCashAccount = new CashAccount("Luis cash account", 100);
        luisAccountsList.addAccount(luisCashAccount);

        String saraEmail = "sara@abc.pt";
        Person sara = new Person("Sara","18480344 6ZY2", new Date(1994 + 1900), saraEmail, "female");
        AccountList saraAccountsList = sara.getPersonAccountList();
        CashAccount saraCashAccount = new CashAccount("Sara cash account", 300);
        saraAccountsList.addAccount(saraCashAccount);

        members.add(luis);
        members.add(sara);

        PersonList familyMembers = new PersonList (members, luisEmail);
        Family newFamily = new Family("Silva", new Date(), 1, familyMembers);
        List<Family> families = new ArrayList<>();
        families.add(newFamily);
        FamilyService familyService = new FamilyService(families);
        FFMApplication app = new FFMApplication(familyService);
        TransferMoneyFromCashAccountController transferMoney = new TransferMoneyFromCashAccountController(app);

            //expected values
        double expectedLuisBalance = 200.0;
        double expectedSaraBalance = 200.0;

        //ACT
            //Transferência do sara para luis
        boolean result = transferMoney.transferMoneyFromMyCashAccountToAnotherFamilyMembersCashAccount(100, new Date() , luisEmail, saraEmail, "Pharmacy payment");
        double saraBalance = saraAccountsList.getCashAccount().getMoneyBalance().getValue();
        double luisBalance = luisAccountsList.getCashAccount().getMoneyBalance().getValue();

        //ASSERT
        assertTrue(result);
        assertEquals(expectedLuisBalance, luisBalance);
        assertEquals(expectedSaraBalance, saraBalance);
        assertTrue(luisAccountsList.getCashAccount().getTransactionsList().size() == 1);
        assertTrue(saraAccountsList.getCashAccount().getTransactionsList().size() == 1);
    }

# 4. Implementation

At the beginning of the present US elaboration we thought the arguments to be passed to the UI should be the user's Account, family member's Account and  amount. Soon we realized the actor should not introduce an object part of business logic like account in to the program, so, we changed method signature to receive two email objects. After a while we understand the program should receive a String instead of the object EmailAddress. Mead sprint we introduced the class MoneyValue, and some changes had to be made.

# 5. Integration/Demonstration
Most of the methods used were developed in past US, and for that reason the group thinks this proves functionality integration with other USs. At the same time, the method that execute the transfer uses methods from other class, and other US rely on this use case.

# 6. Observations
During the development of this US the group decided that individual cash accounts should be stored in account list, this means that each person has his accountList attribute, in which may have four types of account: cash, bank, credit and savings account. We followed the same idea as for families because felt much more logical than, for example, have cash account with a list of people. This information could be stored repeatedly in each kind of account. 

Know the work is done, we acknowledge that at least one modification could be made, that is, rather than ask the actor for the family member's email address, we could retrieve a list of possible family money recipients. The user would, then, choose one, enabling the operation without the need of email address. Probably this would be more user-friendly.



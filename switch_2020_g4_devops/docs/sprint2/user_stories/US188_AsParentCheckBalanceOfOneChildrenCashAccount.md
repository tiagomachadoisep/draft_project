# US188
=======================================

# 1. Requirements

*As a parent, I want to check the balance of one of my children’s cash account.*

## 1.1. Client Notes

## 1.2. System Sequence Diagram

```plantuml
autonumber
title SSD check the balance of one of my children’s cash account
actor "Parent" as P

Participant System
activate System
activate P
P -> System : checkChildCashBalance
System --> P : listOfChilds
deactivate System

P -> System : select child
activate System
System --> P : inform balance

deactivate System
deactivate P
```
1
## 1.3. Process View

```plantuml
title Process View
actor "Parent" as P

Participant UI
Participant "FFM_BusinessLogic" as BL 
activate P
activate UI

P -> UI : checkChildCash\nAccountBalance
UI -> BL : getChildrenList()
activate BL
BL --> UI : childrenList
deactivate BL
UI --> P : childrenList
deactivate UI

P -> UI : selectedChild(Child)
activate UI
UI -> BL : getCashAccountBalance(Child)
activate BL
BL --> UI : balance
deactivate BL
UI-> P : shows balance
deactivate UI
deactivate P
```

## 1.4. Connection with other User Stories

This US is related with the [US104](docs/sprint1/user_stories/US104_ListOfMembersRelations.md) (ListOfMembersRelations) and with [105](docs/sprint1/user_stories/US105_CreateNewRelation.md) (CreateNewRelation) , because this US 188 is almost a specification of this two US.

First the relation between parent and child need to be created, then get the list of family members related to the user,
but specifically for children.

This US is also related with [US170](docs/sprint2/user_stories/US170_CreatePersonalCashAccount.md) (I want to create a personal cash account), to check the cash account
balance first we need to add/create one in the child accountList.

# 2. Analysis

The basic flow we want to implement in this US goes through the following steps:

1. A User asks to check a child cash account balance.

2. Our FFMApplication shows to the parent his/her children list.

3. The user select a specific child.

4. The cash account balance is presented to the parent, if the child has no cash account it shows balance 0.



## 2.1 Relevant excerpt of the Domain Model

```plantuml 
@startuml Domain Model

object Family{
name
id
}


object Person{
id
name
}

object CashAccount{
balance
}

object FamilyRelationship{

}

object FamilyRelationshipType{

}


Person "1" *-right- "1" CashAccount : has 

Family "1" *-right- "*" Person : has members 
Person "1" -down- "*" FamilyRelationship : from
Person "1" -- "*" FamilyRelationship : to
Family "1" -- "*" FamilyRelationship : listOf
FamilyRelationship "*" -- "1" FamilyRelationshipType


@enduml
```

## 2.2. Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **balance**                 | Current balance of the Cash Account                                |
| **PersonDTO**               | DTO for Person                                                     |
# 3. Design

## 3.1. Functionality

To check a child cash account balance we choose to separate the process in
two fases.

Each family has a list of relations that is filtered to contain
only children related to the user:


```plantuml
@startuml
autonumber
title US188 : getChildrenList
actor "Parent" as P
participant UI
participant ":CheckChildCashBalance\nController" as ctrl
participant ":FFMApplication" as app
participant ":FamilyService" as serv
participant ":Family" as fam
participant ":FamilyRelationshipList" as FRL
participant ":PersonList" as pl


activate P
P -> UI : checkChild\nCashBalance
activate UI
UI -> ctrl : getChildrenList\n(email)
activate ctrl

ctrl -> app : getFamilyService()
activate app
app --> ctrl : familyService
deactivate app

ctrl -> serv : getFamilyByMemberMainEmail()
activate serv
serv --> ctrl : family
deactivate serv

ctrl -> fam : getPersonList
activate fam
fam --> ctrl : personList
deactivate fam

ctrl -> pl : getPersonByMainEmail(mainEmail)
activate pl
pl -> ctrl : person
deactivate pl

 
ctrl -> fam : getFamilyRelationsList()
activate fam
fam --> ctrl : familyRelationList
deactivate fam

ctrl -> FRL : getMembersWithRelationsWithMember()
activate FRL 
FRL --> ctrl : childrenList
deactivate FRL
ctrl --> UI : childrenListDTO
deactivate ctrl
UI --> P : childrenListDTO
deactivate UI
deactivate P
@enduml
```

This children List is passed to the user, so he can select the child relative to the account that he wants to check:

Each Person(child) has a list of accounts, and for now each member can only have one cashAccount.
We select the cash and get the balance and show it to the user.

```plantuml
@startuml
autonumber
title US188 : checkChildCashBalance
actor "Parent" as P
participant UI
participant ":CheckChildCashBalance\nController" as ctrl
participant ":FFMApplication" as app
participant "::PersonDTO" as PDTO
participant ":FamilyService" as serv
participant ":Family" as fam
participant ":PersonList" as pl
participant ":Person" as Person
participant ":AccountList" as al
participant ":CashAccount" as CA

activate P
P -> UI : choosed Child
activate UI
UI -> ctrl : CheckChildCash\nBalanceController\n(PersonDTO)
activate ctrl
ctrl -> PDTO : getEmailAddress()
activate PDTO
PDTO --> ctrl : childEmail
deactivate PDTO

ctrl -> app : getFamilyService()
activate app
app --> ctrl : familyService
deactivate app

ctrl -> serv : getFamilyByMemberMainEmail()
activate serv
serv --> ctrl : family
deactivate serv

ctrl -> fam : getPersonList
activate fam
fam --> ctrl : personList
deactivate fam

ctrl -> pl : getPersonByMainEmail(mainEmail)
activate pl
pl -> ctrl : person
deactivate pl

ctrl -> Person : getPersonAccountList()
activate Person
Person --> ctrl : accountList
deactivate Person

ctrl -> al : getCashAccount()
activate al
al --> ctrl : cash Account
deactivate al

ctrl -> CA : getBalance()
activate CA
CA --> ctrl : balance
deactivate CA

ctrl --> UI : balance
deactivate ctrl

UI --> P : balance
deactivate UI

@enduml
```

## 3.2. Class Diagram

```plantuml
@startuml

class "CheckChildCashBalance\nController" as ctrl{
- FFMApplication : app
+ CheckChildCashBalanceController(FFMApplication app)
+ getChildrenList(String email)
+ checkChildCashBalance(PersonDTO childDTO) 
}

class FFMApplication{
- FamilyService : familyService
+ getFamilyService()
}

class FamilyService{

+ getFamilyByMemberMainEmail(email)
}

class Family{
+ getPersonList()
+ getFamilyRelationshipList()
}

class PersonList{

+ getPersonByMainEmail\n(String mainEmail)
}

class Person{

+ getAccountList()
}

class AccountList{

+ getCashAccount()
}

class CashAccount{
- moneyBalance : MoneyValue
+ getBalance()
}

abstract class FinancialEntityAccount {
-description : String
-id : int
+hasSameDescription(String description)
}

interface Account{
+isCash()
}

ctrl "1" -- "*" FFMApplication
FFMApplication"1" -right- "1"FamilyService
FamilyService"1" *-- "*" Family
Family "1" *- "1" PersonList
PersonList "1" *-- "*" Person
Person "1" *-- "1" AccountList
AccountList "1" *-left- "1" CashAccount
CashAccount -left-|> FinancialEntityAccount
FinancialEntityAccount .up.|> Account


@enduml
```

## 3.3. Applied Patterns

In the design process of this US we applied the patterns

- Information Expert: we assign the responsibilities related to object to the class that has the information necessary
  to fulfil them. For example, the class FamilyRelationList stores information about a family relations, so it is also
  responsible for telling us information about who is or is not child of the user.

- Pure Fabrication: we introduce classes that do not correspond to any of the concepts of the problem domain, but are
  useful to achieve the goals of the other design patterns (for example, the Controller and the Service classes).

- Controller: the class CheckChildCashBalanceController is a bridge between the UI and our system, and has the
  responsibility of dealing with system events, in this case check the balance of a user child.

- Low Coupling/High Cohesion: this pattern consists of keeping the classes focused, with responsibilities that are
  related. This is accomplished by keeping classes weakly coupled, that is, weakly connected to other classes, limiting
  the knowledge and reliance the class has with other elements of the system. 


## 3.4. Tests

**Test1:** Exception CheckChildrenCashBalanceController - app is null

**Test2:** Get children list - Success

**Test3:** CheckChildCashBalance - Success

**Test4:** Get children list - Fail, miss one member

**Test5:** checkChildCashBalance - Fail wrong value

**Test6:** checkChildCashBalance - child has no cash account



# 4. Implementation

As said before this is US is like a specification of some already implemented methods, the most challenging part
was to filter the user relations to only show his/her children.

For that we start by creating a method a standard method that can get a list of all members related with the user,
and then we choose only 'son' and 'daughter'.

After the user choice of which child he wants to check the account, we just get the respective cash account balance. 

# 5. Integration/Demonstration

No special integration was necessary for the US to be functional, other than the necessity
of the accounts being already created when the FM does this request.

# 6. Observations

This US is totally dependent on how our app works manage relations and that is something that will probably
be rearranged in future sprints, still all the requirements we're accomplished.  
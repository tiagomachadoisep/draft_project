# US170
=======================================

# 1. Requirements

*As a family member, I want to create a personal cash account.*

## 1.1 Client notes

In this US it is stated that a FM has to be able to create a personal cash account.

According to the project requirements and the feedback from the PO, regarding personal cash accounts, the following
should be considered:

- A FM can only have one personal cash account;
- A FA is also a FM;
- While creating a cash account, an initial balance value should be inserted;
- The initial balance value can be positive, zero or negative (the latter in case there is an initial amount in debt);
- Cash transactions have to be manually inputted by the FM.

## 1.2 Connection with other User Stories

This US is related to [US120](../../sprint1/user_stories/US120_CreateFamilyCashAccount.md), in which a FA creates a
family's cash account. It is closely related to:

- [US130](\US130_TransferMoney.md), in which money is transferred from a family cash account to a FM's cash account;
- [US135](\US135_CheckBalanceOfFamilyOrMemberCashAccount.md), in which a FA checks the balance of his/her cash account;
- [US180](\US180_TransferMoneyFromCashAccountToCashAccount.md), where a FM transfer money from his/her cash account to
  another FM's cash account;
- [US181](\US181_RegisterMemberPaymentUsingCashAccount.md), in which a FM register a payment using his/her personal cash
  account;
- [US185](\US185_CheckAccountBalance.md), where a FM checks the balance of his/her personal cash account;
- [US186](\US186_GetTransactionsBetweenDates.md), in which a FM gets the list of movements on one of his/her accounts
  between two dates;
- [US188](\US188_AsParentCheckBalanceOfOneChildrenCashAccount.md), where a parent (as a FA or FM) checks the balance of
  one of his children's (FM) personal cash account.

This US contains the method getPersonByMainEmail(mainEmail), which is also used in other US that require the validation
of the FM ([US150](../../sprint1/user_stories/US150_GetProfileInfo.md)
, [US151](../../sprint1/user_stories/US151_AddEmailAccountToProfile.md),
[US171](\US171_AddBankAccount.md), [US172](\US172_AddBankSavingsAccount.md), [US173](\US173_AddCreditCardAccount.md),
[US180](\US180_TransferMoneyFromCashAccountToCashAccount.md), [US181](\US181_RegisterMemberPaymentUsingCashAccount.md),
[US185](\US185_CheckAccountBalance.md) and [US186](\US186_GetTransactionsBetweenDates.md)).


# 2. Analysis

The requirements that were stated and analysed in the previous section have made it clear that some kind of validation
needs to take place in the designed solution. This validation will have to verify whether the user that accesses SWS is
a FM of any family.

A person can be a FM of more than one family, however the main email used to authenticate in SWS has to be different for
each family. Each family has its own person list that contains information about its members. Once such family is
identified, so is the person list and consequently the FM. Associated with a FM is his/her list of accounts of any
type (bank accounts, cash account, among others).

The personal cash account will then be created and added to this list of accounts of the FM.

## 2.1 Relevant excerpt of the Domain Model

```plantuml
title Excerpt of Domain Model

object "Family" as F {
id
name
}

object "Person" as P {
name
identification
vat
address
phoneNumberList
emailAddressList
}

object "Cash Account" as CA{
balance
description
}

F "*" *- "1" P : is administered by
F "*" *- "*" P : has members
P "1" *- "0..1" CA : has

@enduml
```

In this excerpt of the domain model we can see that each family has its members (one of them being the FA), and that
each member may have a personal cash account. This personal cash account is a type of account that has two attributes:
initial balance and description.

## 2.2 Attributes

For a personal cash account, the following parameters have to be inserted by the FM through the UI:

| **_Attributes_**            | **_Business Rules_**    |
| :-------------------------- | :----------------------|
| **
MainEmail** | Mandatory, String type, refers to the main email of the FM which will be used to identify his/her family |
| **Description** | Mandatory, String, cannot be empty or null|
| **Balance** | Mandatory, double with two decimal points, not null, can be negative in case of initial debt |

This will then enable the creation of the personal cash account with the attributes Description and Balance described in
the previous table.

## 2.3 System Sequence Diagram

```plantuml
autonumber
title SSD Create Personal Cash Account
actor "FamilyMember" as FM
activate FM

Participant System
activate System
FM -> System : create personal cash account
System --> FM : ask data (main email, description, balance)
deactivate System
FM -> System : inputs data (main email, description, balance)
activate System
System --> FM : informs result
deactivate System
deactivate FM
```

## 2.4 Process View Diagram

```plantuml
title Process View of Create Personal Cash Account
actor "Family Member" as FM
Participant FFM_UI
Participant FFM_BusinessLogic

FM -> FFM_UI : create personal cash account
activate FM
activate FFM_UI
FFM_UI --> FM : asks data \n(main email, description, balance)
deactivate FFM_UI
FM -> FFM_UI : inputs data \n(main email, description, balance)
activate FFM_UI
FFM_UI -> FFM_BusinessLogic : createPersonalCashAccount\n(mainEmail, description, balance)
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI : result
deactivate FFM_BusinessLogic
FFM_UI --> FM : informs result
deactivate FFM_UI
deactivate FM
```

# 3. Design

## 3.1. Class Diagram

```plantuml

title **US170** Create Personal Cash Account

class "Family" as F{
   + Family(String name, int id, Date registrationDate)
   + Family(String name, int id)
   + Family(String name, Date registrationDate, int id)
   + Family(String name, Date registrationDate, int id, PersonList personList)
   + Family(String name, Date registrationDate, int id, PersonList personList,CashAccount cashAccount) 
   + PersonList getPersonList()
}

class "FamilyService" as FS {
 + FamilyService()
 + Family getFamilyByMemberMainEmail(mainEmail)
}

class "CreatePersonalCashAccountController" as CPCA {
 + CreatePersonalCashAccountController(app)
 + createPersonalCashAccount(mainEmail, description, balance)
}

class "FFMApplication" as APP {
 + FFMApplication()
 + FamilyService getFamilyService()
}

interface "Account" as A {
 + boolean hasSameDescription(String description)
 + boolean validateDescription(String description)
 + boolean isCash()
}

class "AccountList" as AL {
 - accountList : List<Account>
 + boolean createPersonalCashAccount(description, balance)
}

class "CashAccount" as CA {
 - description : String
 + CashAccount(String description, double balance)
 + boolean isCash()
 + boolean hasSameDescription(String description)
 + boolean validateDescription(String description)
}

class "MoneyValue" as MV {
 - value : double
 - currency : Currency
 + MoneyValue(double value)
}

class "PersonList" as PL {
 + PersonList(List<Person> members, String mainEmailOfAdmin)
 + Person getPersonByMainEmail(String mainEmail)
}

class "Person" as P{
 + Person(name, id, vat, birthDate, mainEmail, address, gender)
 + Person(String name, String id, Date birthDate, String mainEmail, String gender)
 + AccountList getPersonAccountList()
}

class "EmailAddress" as EA {
  - email: String
   + EmailAddress(String email)
   + boolean sameEmail(String anEmail)
   + boolean equals(Object o)
}

CPCA "1" -> "*" APP: app
APP "1" *--> "1" FS: familyService
FS "1" o-- "*" F
F "1" *- "1" PL
PL "1" o-- "*" P
PL "1" - "1" EA
P "1" *-- "1" AL
AL "1" *- "1" CA
CA "1" *- "1" MV
A <|.. CA

@enduml
```

## 3.2. Functionality

The FM interacts with the UI by asking to create a personal cash account (1). The UI requests the necessary data (2)
which is then inputted by the user:
the description and the initial balance (3). As was mentioned, this initial balance can have any value (positive, zero
or negative). Then, the UI invokes the method of the controller createPersonalCashAccount with the inputted data and the
main email of the FM (4).

The controller then validates the user asking to create the personal cash account by verifying if his email address (
mainEmail) is the main email of any family member. In order to do so, the controller needs to get the family service
through the app (steps 5 and 6), to get the family to which the user accesses using their main email (steps 7 and 8),
and then get the members of that family so as to ask each one if their main email is equal to the one used to
authenticate in the system (steps 9 to 12).

Once the person object is found, the controller needs to acces the list of accounts associated with that person which is
an attribute of the person object. This list is found and retrieved (steps 13 and 14) and only then can the controller
create a personal cash account for the person. Since the account list holds the information about all the accounts, it
should be the entity responsible for creating and guarding a new account. This is represented in step 15 and detailed in
the next SD.

If no errors occur, a successful message is delivered back to the FA (steps 19 to 21). Otherwise, an error message is
returned (steps 16 to 18).

```plantuml
autonumber
title US170 - Create Personal Cash Account
actor "Family Member" as FM
participant ":UI" as UI
participant "controller\n:CreatePersonal\nCashAccountController" as ctrl
participant "app\n:FFMApplication" as app
participant "familyService\n:FamilyService" as FS
participant "family\n:Family" as F
participant "personList\n:PersonList" as PL
participant "person\n:Person" as P
participant "accountList\n:AccountList" as AL

activate FM
FM -> UI : create personal cash account
activate UI
UI --> FM : asks data
FM -> UI : inputs data (mainEmail, \ndescription, balance)
UI -> ctrl : createPersonalCashAccount\n(mainEmail, description, balance)
activate ctrl
ctrl -> app : getFamilyService()
activate app
app --> ctrl : familyService
deactivate app
ctrl -> FS : getFamilyByMemberMainEmail(mainEmail)
activate FS
FS --> ctrl : family
deactivate FS
ctrl -> F : getPersonList()
activate F
F --> ctrl : membersList
deactivate F
ctrl -> PL : getPersonByMainEmail(mainEmail)
activate PL
PL --> ctrl : person
deactivate PL
ctrl -> P : getAccountList()
activate P
P --> ctrl : accountList
deactivate P
ctrl -> AL : createPersonalCashAccount(description, balance)
activate AL
ref over AL : createPersonalCashAccount\n(description, balance)
opt not possible to create personal cash account
AL --> ctrl : error
ctrl --> UI : error
UI --> FM : error message
else
AL --> ctrl : result
deactivate AL
ctrl --> UI : result
deactivate ctrl
UI --> FM : informs result
deactivate UI
deactivate FM
end
@enduml
```

The method createPersonalCashAccount is detailed in the SD below. The first part of the method consists in checking if
the person already has a cash account. Therefore, a loop for each account in the person's account list is made by asking
each account if it is a cash account (steps 2 and 3). If this result is true, then an error is returned to the account
list and the controller (4).

Otherwise, the next step consists in assessing if the account has the same description of any other account in the
person's account list (5). This method validates first if the description is valid (i.e., not null or empty). If the
description is not valid or if it is the same as another account's description, an error is returned (7).

If there is no problem, then the next step consists in creating the account (8) and adding it to the person's account
list (9). Then, a success message is delivered back to the FM (10).

```plantuml
title //createPersonalCashAccount(description,balance)//
autonumber
participant "accountList\n:AccountList" as AL
participant "account\n:Account" as A
[o-> AL :createPersonalCashAccount(description,balance)
activate AL
loop for each account in accountList
AL -> A : isCash()
activate A
A --> AL : result
deactivate A
opt result == true
[<-- AL : error
end opt
end loop
loop for each account in accountList
AL -> A : hasSameDescription(description)
activate A
A --> AL : result
deactivate A
opt result == true
[<-- AL : error
end opt
end loop
AL -->  "newPersonalCashAccount:\n Account" ** : create(description, balance)
AL -> AL : add(newPersonalCashAccount)
[<-- AL : success
deactivate AL

@enduml
```

## 3.3. Applied Patterns

Different software development patterns were applied in the analysis and design steps of this US:

**Pure Fabrication:** the FFMApplication class resulted from this principle, as well as the service classes (in this US,
the FamilyService), as they do not represent a concept of the problem domain.

**Controller:** the AddPersonalCashController class follows this design pattern, in which an intermediate entity
establishes a connection between the UI and the internal processes.

**Creator:** this pattern means that a class should have the responsibility of creating objects if it's also responsible
for aggregating them. This happens in AccountList, which has a list of all the accounts of a person as an attribute (
accounts of all types - cash, bank, bank savings and credit card), therefore it's also the one who creates cash account
objects.

**Information Expert:** by stating that certain classes should have a certain responsibility since they hold the
information required to do it. In this US, this is achieved with AccountList which is an attribute of the Person class.
The AccountList class knows all the information about all the accounts belonging to a person (as was explained in the
previous point), therefore it should have responsibilities such as creating a new account, adding it to the list, and
checking if it contains a specific account.

**High Cohesion and Low Coupling:** this principle states that a class should be focused on specific responsibilities (
high cohesion), so that it depends the less possible on other classes (low coupling). This principle is reflected in our
choice of assigning a list of accounts for each person object instead of creating a kind of account service in the FFM
APP. This way, each person has their own account list, and this list is responsible for managing all the accounts it
contains.

**Polymorphism/Protected Variation** this is evident through the creation of an interface named Account which the class
CashAccount implements. The interface acts as a blueprint for all types of accounts by defining the methods signatures
that all the accounts that implement it will implement. This means that, irrespective of the type of account, the method
implemented in each type (cash, credit card, bank and bank savings) will have to follow that "template" even if the
methods will have their own specifications and variations.

## 3.4. Tests

Different tests were conceived for the classes involved in this US (CreatePersonalCashAccountController, AccountList,
CashAccount). In CreatePersonalCashAccountController, the following tests were made:

**Test 1** : Check that it is not possible to initiate this controller with a null app.

**Test 2** : Check if it is possible to create a personal cash account with valid arguments.

**Test 3** : Check if it is impossible to create a personal cash account with invalid argument.

**Test 4** : Check if it is impossible to create a personal cash account when a person already has one.

As for Account List, the tests below were performed:

**Test 5** : Check if adding a personal cash account is possible because the person hasn't got one.

**Test 6** : Check if adding a personal cash account is impossible because the person already has one.

**Test 7** : Check if adding a personal cash account is impossible because the person already has an account with the
same description.

**Test 8** : Check if adding a personal cash account is impossible because the person inserted a null description.

**Test 9** : Check if adding a personal cash account is impossible because the person inserted an empty description.

**Test 10** : Checks if an account is successfully added to a person's account list.

**Test 11** : Checks if an account is unsuccessfully added to a person's account list.

Finally, in CashAccount, we tested the constructor for successful and not successful scenarios, and also the methods
isCash, validateDescription and hasSameDescription:

**Test 12** : Test constructor for personal cash account.

**Test 13** : Test constructor for personal cash account - fails because description is empty.

**Test 14** : Test constructor for personal cash account - fails because description is invalid.

**Test 15** : Checks if it is a cash account.

**Test 16** : Checks that a cash account description is valid.

**Test 17** : Checks that a cash account description cannot be null.

**Test 18** : Checks that a cash account description cannot be empty.

**Test 19** : Checks that two cash account descriptions are the same.

**Test 20** : Checks that two cash account descriptions aren't the same.

# 4. Implementation

In order to implement this US the group had to debate about the most reasonable way to structure the different types of
accounts a user may have. According to the PO and the project requirements, there may exist the following type of
accounts: cash, bank, credit card and bank savings.

By analyzing each account two different types were identified: the ones associated with a financial entity (bank
account, credit card account and bank savings account) and the cash account (of a family or of a person). However, these
two types have some methods in common, which are represented in the Account interface. The accounts associated with a
financial entity share their attributes, and their methods can be represented in an abstract way as was explained in
section 3.3 of the documentation. Therefore, these accounts extend the abstract class FinancialEntityAccount which in
turn implements the Account interface. The CashAccount also implements the Account interface.

Below is the code that was implemented in the controller for this US:

    public boolean createPersonalCashAccount(String mainEmail, String description, double balance) {

        //Find family member and his/her account list
        FamilyService familyService = app.getFamilyService();
        Family family = familyService.getFamilyByMemberMainEmail(mainEmail);
        PersonList personList = family.getPersonList();
        Person familyMember = personList.getPersonByMainEmail(mainEmail);
        AccountList accountList = familyMember.getPersonAccountList();

        try {
            accountList.createPersonalCashAccount(description, balance);
            return true;
        } catch (IllegalArgumentException exception) {
            return false;
        }

    }

In the AccountList class the code of the method createPersonalCashAccount is detailed:

    public boolean createPersonalCashAccount(String description, double balance) {
        for (Account account :
                this.listOfAccounts) {
            if (account.isCash()) {
                return false;
            }

        }

        for (Account account : this.listOfAccounts) {
            try {
                if (account.hasSameDescription(description)) {
                    return false;
                }
            } catch (IllegalArgumentException exception) {
                return false;
            }

        }

        try {
            Account newPersonalCashAccount = new CashAccount(description, balance);
            this.listOfAccounts.add(newPersonalCashAccount);
            return true;
        } catch (IllegalArgumentException exception) {
            return false;
        }

    }


# 5. Integration/Demonstration

Given the fact that this US is connected to other US (as mentioned in section 1.4), some efforts had to be made in order
to integrate its functionality with the functionality of other US. The integration that had to take place was due to the
choice of structuring accounts according to the Domain Model, in which an Interface named Account is the interface of
any type of account (cash or financial entity account). In the previous section of the documentation it is explained how
this was implemented.

On the other hand, while designing this US we have decided that a cash account should have a mandatory description, as
was referred in section 2 of the documentation. Therefore, the previous constructor for a family cash
account ([US120](../../sprint1/user_stories/US120_CreateFamilyCashAccount.md)) had to be changed as it only accepted an
initial balance as a parameter, and not a description. Moreover, the constructors for both family and personal cash
accounts have to convert the initial balance value to a MoneyValue object, which had not been contemplated for
([US120](../../sprint1/user_stories/US120_CreateFamilyCashAccount.md)). This method is now the constructor for a cash
account, either for the family or for a FM:

    public CashAccount(String description, double moneyBalance) {
        if (!this.validateDescription(description)) {
            throw new IllegalArgumentException("Description cannot be null or empty.");
        }

        this.description = description;
        this.moneyBalance = new MoneyValue(moneyBalance);
        this.transactionList = new ArrayList<>();
    }

# 6. Observations

In this US, as well as with other US for this sprint review, the group has opted for a structure in which the accounts
are kept and known by each person. Therefore, a personal cash account is guarded in a person's account list alongside
other accounts the person may have
(associated with a financial entity).

A possible alternative could have been keeping all the accounts in SWS in a class like an account service which would
keep and manage all the accounts of all families. However, this solution had a higher coupling and lower cohesion.

There may be a future requirement or US in which a FM can have more than a personal cash account. If that happens, the
implementation will have to be updated.

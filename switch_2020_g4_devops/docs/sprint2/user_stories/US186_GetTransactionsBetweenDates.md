# US186
=======================================


# 1. Requirements

*As a family member, I want to get the list of transactions on one of my
accounts between two dates*

With this US, the FM should gain the ability to get the list of transactions of any of his/hers accounts between
two specified dates.

## 1.1 Client Notes

Nothing significant was added until the time of writing.

## 1.2 System Sequence Diagram

Next, we present the System Sequence Diagram.

```plantuml
autonumber
title SSD Get transactions between dates 
actor "Family Member" as FM
activate FM
Participant System
activate System
FM -> System : get transactions between dates
System --> FM : shows account descriptions
deactivate System
FM -> System : inputs required data (description, start date, end date)
activate System
System --> FM : shows transactions between dates
deactivate System
deactivate FM
``` 

## 1.3 Process view

Below it's presented the process view diagram:

```plantuml
autonumber
title Get transactions between dates
actor "Family Member" as FM
Participant UI
Participant FFM_BusinessLogic as APP

activate FM
FM -> UI : get transactions between dates
activate UI
UI --> FM : shows account descriptions
deactivate UI
FM -> UI : input required data\n(description, start date, end date)
activate UI
UI -> APP : getTransactionsBetweenDates(description, startDate, endDate)
activate APP
APP --> UI : transactionsBetweenDates
deactivate APP
UI --> FM : show transactions between dates
deactivate UI
deactivate FM
``` 

## 1.4 Connection with other User Stories

This US can be directly dependent on [US170](US170_CreatePersonalCashAccount.md),
[US171](US171_AddBankAccount.md), [US172](US172_AddBankSavingsAccount.md),
[US173](US173_AddCreditCardAccount.md), depending on the type of account the FM
chooses to get the list of movements from.


# 2. Analysis

Next, it is presented the transactionsBetweenTwoDates list of attributes, and a relevant
excerpt of the domain model applicable to this US.

## 2.1 Attributes

The list of the movements between two dates that will be created will have the
following attributes:

| **_Attributes_**            | **_Business Rules_**                                                                                                                  |
| :-------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------|
| **transactionDTOList**      | Mandatory, List type. Describes the account transactions on a specified period given by the FM.                                 |

## 2.2 Relevant excerpt of the Domain Model

Below it's presented a partial Domain Model with the relevant information:

```plantuml

title Excerpt of Domain Model

object Family {
members
}

object Person {
mainEmail
accountList
}

object FinancialEntityAccount {
balance
description
entity
transactionList
}

object BankSavingsAccount {
id
}

object BankAccount {
id
}

object CreditCardAccount {
id
}



Family "1"-"1" Person : has admin
Family "1"--"1..*" Person : has members
Person "1"-"1..*" FinancialEntityAccount : has
FinancialEntityAccount --|> BankAccount
FinancialEntityAccount --|> BankSavingsAccount
FinancialEntityAccount --|> CreditCardAccount

@enduml
```

Above we can see the family and person objects, which are related in the sense that
each family has one or more members and always exactly one family administrator.
Also, each person can have one or more financial entity accounts, which have their own list of movements, and can be of
implementations: bank savings account, bank account, and credit card account.

# 3. Design

## 3.1. Functionality

The FM asks to get the transactions list of one of his/hers account during a specified period. The UI, then, passes the requests to the 
getTransactionsBetweenDatesController, which begins a process to reach the accounts of
the FM: first it asks for the family service to the FFMApplication,
then searches the family of the FM by ID through the familyService, afterwards it asks
the family object its personList, and founds the intended person object (that of the
FM) through its main email, which leads to the accountList of that person object. There, it gets all account descriptions
and retrieves a list of account descriptions (through a DTO).

```plantuml
autonumber
title US186 - Get Transactions Between Dates
actor "Family Member" as FM
participant ":UI" as ui
participant ":GetTransactionsBetweenDatesController" as ctrl
participant ":FFMApplication" as app
participant ":FamilyService" as fs
participant ":Family" as f
participant ":PersonList" as pl
participant ":Person" as p
participant ":AccountList" as la

activate FM
FM -> ui : get transactions between dates
activate ui
ui -> ctrl : getAllAccountsDescription(mainEmail)
activate ctrl
ctrl -> app : getFamilyService()
activate app
app --> ctrl : familyService
deactivate app
ctrl -> fs : getFamilyByMemberMainEmail(mainEmail)
activate fs
fs --> ctrl : family
deactivate fs
ctrl -> f : getPersonList()
activate f
f --> ctrl : membersList
deactivate f
ctrl -> pl : getPersonByMainEmail(mainEmail)
activate pl
pl --> ctrl : person
deactivate pl
ctrl -> p : getAccountList()
activate p
p --> ctrl : accountList
deactivate p
ctrl -> la : getAllAccountsDescription()
activate la
ref over la : getAllAccountsDescription()
la --> ctrl : accountDescriptionDTO
deactivate la
ctrl --> ui: accountDescriptionDTO
deactivate ctrl
ui --> FM : shows descriptions
deactivate ui
deactivate FM
@enduml
```

Going deeper on getAllAccountsDescription() method, we can see that all descriptions are saved on
a temporary ArrayList and then  converted to a DTO to show to the FM.

```plantuml
autonumber
title getAllAccountsDescription()
participant ":AccountList" as la
participant ":Account" as acc
[o-> la : getAllAccountsDescription()
activate la
la --> "dtoList : List<TransactionDTO>" ** : create() 
activate "dtoList : List<TransactionDTO>"
loop for each account in accountList
la --> "dto : TransactionDTO" ** : create()
activate "dto : TransactionDTO"
la -> acc : getDescription()
activate acc
acc --> la : description
deactivate acc
la -> "dto : TransactionDTO" : setDescription(description)
deactivate "dto : TransactionDTO"
la -> "dtoList : List<TransactionDTO>" : add(aDto)
end loop
"dtoList : List<TransactionDTO>" --> la : dtoList
deactivate "dtoList : List<TransactionDTO>"
[<-- la : dtoList
deactivate la
@enduml
```

Diving more deep into the getAccountByDescription(description), a loop is started
in the accountList that goes through every account in that list. If any of the
descriptions of them match the given description, that account is returned. Else, 
a message stating that no accounts with the provided description have been found is
sent back.

```plantuml
autonumber
title US186 - Get Transactions Between Dates
actor "Family Member" as FM
participant ":UI" as ui
participant ":GetTransactionsBetweenDatesController" as ctrl
participant ":AccountList" as la
participant ":Account" as acc
FM -> ui : Get transactions of an\n account between dates
activate FM
activate ui
ui -> ctrl : getTransactionsBetweenDates(description)
activate ctrl
ctrl -> la : getAccountByDescription(description)
activate la
loop for account in accountList
la -> acc : hasSameDescription(description)
activate acc
acc --> la : account
deactivate acc
end loop
la --> ctrl : account
deactivate la
ctrl -> acc : getTransactionsBetweenDates()
activate acc
acc --> ctrl : balance
deactivate acc
ctrl --> ui : balance
deactivate ctrl
ui --> FM : balance
deactivate ui
deactivate FM
@enduml
```

Finally, getting a closer look to the getTransactionsBetweenDates
(startDate, endDate), the account creates a new list to contain any possible transactions
that are within the period of time specified. Then, a loop through each movement
of that account transactionList starts, and any transactions that match the required time
period are added to the newly created list. At last, that list is returned.

``` plantuml
@startuml
title getTransactionsBetweenDates(startDate, endDate)
participant ":Account" as A
participant ":MovementList" as ML
participant ":Movement" as M
participant "transactionsBetweenDates:\nList<TransactionDTO>" as MBD

[o-> A : getTransactionssBetweenDates(startDate, endDate)
activate A
A -> MBD ** : create
activate MBD
A -> ML : getTransactionsBetweenDates(startDate, endDate)
activate ML
loop for each transaction in transactionList
ML -> M : result = istransactionbetweenDates\n(startDate, endDate)
deactivate ML
activate M
opt result == true
M -> MBD : addToTransactionBetweenDates(transaction.toDTO())
deactivate M
end opt
end loop
MBD --> A : transactionsBetweenDatesDTO
deactivate MBD
[<--o A : transactionsBetweenDatesDTO
deactivate A
@enduml
```

## 3.2. Class Diagrams

The class diagram is represented below:

```plantuml
@startuml
GetTransactionsBetweenDatesController - FFMApplication : asks >
FFMApplication -- FamilyService : gets >
FamilyService - Family : gets >
Family -- PersonList : has a >
PersonList - Person : gets >
Person -- AccountList : has an >
AccountList - Account : gets >
Account -- TransactionList : creates a >

class GetTransactionsBetweenDatesController{
- FFMApplication : app
+ getMovementsBetweenTwoDates(description, startDate, endDate)
}

class FFMApplication{
- familyService : FamilyService
+ getFamilyService()
}

class FamilyService{
- families : List <Family>
+ getFamilyById(familyId)
}

class Family {
+ getPersonList()
}

class PersonList {
- members : List <Person>
+ getMemberByMainEmail(mainEmail)
}

class Person {
- name : String
- id : int
- vat : VAT
- phoneNumber : PhoneNumber
- address : Address
- birthDate : Date
- emailAddress : EmailAddress
- gender : String
+ Person(name, id, birthDate, VAT, emailAddress, phoneNumber, address, gender)
}

class AccountList {
- accounts : List<Account>
+ getAccountByDescription(description)
}

interface Account {
+ getMovementList()
+ createMovementList()
}

class TransactionList {
- transactionList : List<Transaction>
+ getTransactionBetweenDates(startDate, endDate)
}
@enduml
```

## 3.3. Applied Patterns

**Controller**: Concerning to getMovementsBetweenTwoDatesController, this is
responsible for receiving and coordinating the system operations necessary to
fulfill this process.

**Information Expert**: The Information Expert pattern can be seen in action,
too, when the system interacts with the MovementList class, not requesting the
information needed and manipulating it itself, but indicating the information
needed and allowing the Person class to create it and manipulate it in the
necessary ways.

**Creator**: The MovementList class is responsible for creating the new
movementsbetweenTwoDatesList.

**Pure Fabrication** : The FFMApplication and FamilyService classes are not
part of the concepts of the problem, but are created to coordinate the process.

## 3.4. Tests

Below are the tests needed to ensure the requisites of the US are correctly
applied.

**Test 1**: Check if the getAllAccountsDescription is successful.

**Test 2**: Check if the getAllAccountsDescriptions handles correctly the
case when there are no accounts to retrieve the description from.

**Test 3**: Getting transactions between two dates: some transactions are
within the specified period, and others are not.

**Test 4**: Getting transactions between two dates: all transactions are
within the specified period.

**Test 5**: Getting transactions between two dates: no transactions are
within the specified period.

**Test 6**: Getting transactions between two dates: no transactions were made.

**Test 7**: Getting transactions between two dates: all transactions are movements.

**Test 8**: Getting transactions between two dates: some transactions are movements,
and others are transfers.

**Test 9**: Getting transactions between two dates: asking from a credit card account.

**Test 10**: Getting transactions between two dates: asking from a bank savings account.

**Test 11**: Getting transactions between two dates: asking from a cash account.

**Test 12**: Getting transactions between two dates: start date and end date incorrectly reversed.

# 4. Implementation

The implementation of this US was relatively straight forward and did not arise any problems. Something we had to take into account, though, was that we had to get the family of the person whose profile information we wanted retrieved using the main email, and then match the main email to the correct member of that family.

# 5. Integration/Demonstration

No special integration was necessary for the US to be functional, other than the necessity
of the accounts being already created when the FM does this request.

# 6. Observations

There was not any significant debate that sparked between the group regarding this US,
as it is relatively straight forward.

# US106
=======================================

# 1. Requirements

*As a family administrator, I want to change the relation between two family members.*

## 1.1 Client notes

There were no aditional notes acquired from the client.

The client simply wants the family administrator to change an existing relationship between two members of the
family.

## 1.2 System Sequence Diagram

```plantuml 
autonumber
title **US106** Change relation between members
actor "Family Administrator" as FA
participant "System" as Sys
activate FA
FA -> Sys : change relationship between members
activate Sys
Sys --> FA : returns list of existing relationships
deactivate Sys
FA -> Sys: input the two members data and intended relation
activate Sys
Sys --> FA: informs result
deactivate Sys
deactivate FA
```

## 1.3 Process View

```plantuml 
autonumber
title **US106** Process View
actor "Family Administrator" as FA
participant ": FFM_UI" as UI
participant ": FFM_BusinessLogic" as BL
activate FA
FA -> UI : Change relation between members
activate UI
UI -> BL : getListOfExistingRelationships(adminEmail)
activate BL
BL --> UI : listofExistingRelationships
deactivate BL
UI --> FA : returns list
deactivate UI
FA -> UI: input data
activate UI
UI -> BL: changeRelationship(adminEmail, emailPersonOne,\n emailPersonTwo, newRelation)
activate BL
BL --> UI: result
deactivate BL
UI --> FA: informs result
deactivate UI
deactivate FA
```

## 1.4 Connections with other User Stories

This US106 is tightly connected with [US105](\US105_CreateNewRelation.md) on how relationships are formed
between family members. As relationships involve two object Persons and one object of FamilyRelationType, the only
thing needed is to change the object FamilyRelationType for another that has the string designation that the family
administrator wants.

# 2. Analysis

## 2.1 List retrieval

At first, a list of existing relationships within the family must be returned to the family administrator, so it can
choose which relationship is to be changed.

## 2.2 Relevant excerpt of the domain model for US106

```plantuml 
@startuml Domain Model
object "Family" as fam {
id
name
}
object Person {
id
name

}
object "FamilyRelationships" as FR {
designation
}
fam "1" - "*" Person : has members >
fam "1" - "1" Person : has admin >
Person "1" ---- "*" FR : to 
Person "1" ---- "*" FR : of 
fam "1" - "*" FR : exists


@enduml
```

# 3. Design

## 3.1. Functionality Realization

1. The ChangeRelationshipController will be responsible for calling the correct service present in the FFM Application Class;
   
2. This service, called FamilyService, holds a list of families within the FFM Application. This service will then use the email of the user
   which requested the change of a relationship and correctly identify to which family he belongs to.
   After finding the correct family, the family's familyRelationshipList is retrieved.
   
3. The FamilyRelationshipList will be responsible for returning a list of string arrays which contain the name of the members
   of a relationship and the relation designation of that relationship.
   
4. The family administrator will then choose which existing relationship he wants to change and input the new relation
   designation he wants.
   
5. The UI will send the correct data to the ChangeRelationshipController. This controller takes the email of the persons
   of the relationship, and the new designation as strings passed as arguments. It will get the Person objects 
   associated with those emails, and the FamilyRelationType object associated with the designation passed, if it exists.
   
6. If there is a FamilyRelationType object with the same designation as the new designation passed, that object will be
   set as the familyRelationType attribute of the FamilyRelationship object that represents the relationship between
   the members.
   
7. The controller then sends a result to the UI, reporting the success or failure of changing the relationship.

Below are the Sequence Diagrams for US106:

```plantuml 
@startuml
autonumber
title **US106** Change relation between members
actor "Family Administrator" as FA
participant ":UI" as UI
participant ":ChangeRelation\nController" as CRC
participant "app :\nFFMApplication" as AppMembers
participant "familyService :\nFamilyService" as FS
participant "family :\nFamily" as F
participant "familyRelationshipList :\nFamilyRelationshipList" as FRL
participant "personList :\nPersonList" as PL
participant "familyRelationTypeService :\nFamilyRelationTypeService" as FRTS
participant "familyRelationship :\nFamilyRelationship" as FR

activate FA
FA -> UI : Change relation between members
activate UI
UI -> CRC : getListOfRelationships(adminEmail)
activate CRC
ref over CRC
getListOfRelationships(adminEmail)
end ref
CRC --> UI : listMembersAndRelations
deactivate CRC
UI --> FA : listOfExistingRelationships
deactivate UI
FA -> UI : input the two members \ndata and intended relation
activate UI
UI -> CRC : changeRelation(adminEmail, mailPersonOne, \nemailPersonTwo, newRelation)
activate CRC
CRC -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> CRC : familyService
deactivate AppMembers
CRC -> FS : getFamilyByAdminMainEmail(adminEmail)
activate FS
FS -> CRC : family
deactivate FS
CRC -> F : getPersonList()
activate F
F --> CRC : personList
deactivate F
CRC -> PL : getPersonByMainEmail(emailPersonOne)
deactivate F
activate PL
PL --> CRC : personOne
deactivate PL
CRC -> PL : getPersonByMainEmail(emailPersonTwo)
activate PL
PL --> CRC : personTwo
deactivate PL
CRC -> F : getFamilyRelationshipList()
activate F
F --> CRC : familyRelationshipList
deactivate F
CRC -> FRL : getRelationshipOfMembers\n(personOne, personTwo)
deactivate F
activate FRL
FRL --> CRC : familyRelationship
deactivate FRL
CRC -> AppMembers : getFamilyRelationTypeService()
activate AppMembers
AppMembers --> CRC : familyRelationTypeService
deactivate AppMembers
CRC -> FRTS : getTypeFromListByDesignation(newRelation)
activate FRTS
FRTS --> CRC : RelationType
deactivate FRTS
CRC -> FR : setRelationType(RelationType)
activate FR
deactivate FR
CRC --> UI : result
deactivate CRC
UI --> FA : informs result
deactivate UI
deactivate FA
@enduml
```

Going into more detail in the getListOfRelationships(adminEmail) method of the ChangeRelationController class:

```plantuml 
@startuml
autonumber
title **US106** Change relation between members
participant ":ChangeRelation\nController" as CRC
participant "app :\nFFMApplication" as AppMembers
participant "familyService :\nFamilyService" as FS
participant "family :\nFamily" as F
participant "familyRelationshipList :\nFamilyRelationshipList" as FRL

[o-> CRC : getListOfRelationships(adminEmail)
activate CRC
CRC -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> CRC : familyService
deactivate AppMembers
CRC -> FS : getFamilyByAdminMainEmail(adminEmail)
activate FS
FS -> CRC : family
deactivate FS
CRC -> F : getFamilyRelationshipList()
activate F
F --> CRC : familyRelationshipList
deactivate F
CRC -> FRL : getRelationshipListDescriptions()
activate FRL
FRL --> CRC : listOfExistingRelationships
deactivate FRL
[<-- CRC : listOfExistingRelationships
deactivate CRC
@enduml
```

## 3.2. Class Diagram

```plantuml
@startuml

class "ChangeRelation \n Controller" as CRC {

 + List<String[]> getListOfRelationships(String adminEmail)
 + boolean changeRelation(String adminEmail, String emailPersonOne, String emailPersonTwo, String newRelation)
 } 
 
class "FFMApplication" as APP {

 + FamilyService getFamilyService()
 + FamilyRelationTypeService getFamilyRelationTypeService()
}

class "FamilyService" as FS {
 + Family getFamilyByAdminMailEmail(EmailAddress mainEmail)
}

class "Family" as F {
  + PersonList getPersonList()
  + FamilyRelationshipList getFamilyRelationshipList()
}

class "PersonList" as PL {
  - mainEmailOfAdmin : EmailAddress
  + Person getPersonByMainEmail(String mainEmail)
}

class "Person" as P {
 - mainEmail : EmailAddress
 + boolean hasMainEmail(String emailAddress)
}

class "FamilyRelationTypeService" as FRTS {
  + FamilyRelationType getTypeFromListByDesignation(String designation)

}

class "FamilyRelationType" as FRT {
   - designation : String
   + boolean isSameDesignation(String designation)
}

class "FamilyRelationship" as FR {
  + void setRelationType(FamilyRelationType familyRelationType)
}

class "FamilyRelationshipList" as FRL {
 + FamilyRelationship getRelationshipOfMembers(Person personOne, Person personTwo)
 + List<String[]> getRelationshipListDescriptions()
}

CRC "1" o-- "1" APP : asks >
APP "1" *- "1" FS : has >
FS "1" *-- "*" F : holds >
F "1" *-- "1" PL : has >
F "1" *-left- "1" FRL : has >
FR "*" o-- "2" P : has >
FR "*" o-- "1" FRT : has >
PL "1" *-- "*" P : holds >
APP "1" *-left- "1" FRTS : has >
FRTS "1" *-- "*" FRT : holds >
FRL "1" *-- "*" FR : holds >
FRT "2" o-- "2" FRT : has gender-based inverse type 
@enduml
```



## 3.3. Applied Patterns

**Information Expert:** We assigned responsibility to the information expert. The FamilyRelationship class 
holds the two members of the relationship and the familyRelationType that is to be changed.
Thus, this class is responsible for changing the current familyRelationType object by another one passed as
argument to the proper method.

**Controller:** we created the ChangeRelationController whose only purpose is to build a "bridge" between
the UI and the FFMApplication, with each task needed being directed by the
controller to the other known classes, since each class should 
have one responsibility **(Single Responsability Principle)**.

**Pure Fabrication:** this pattern is applied through the creation of the class FFMApplication, not part of the
concepts of the domain model but essential to coordinate the data and functions of it.

**High Coesion & Low Coupling:** Through the creation and assignment of single responsabilities to each class
by applying the above mentioned GRASP, a high coesion and low coupling between classes is achieved.

## 3.4. Tests
### 3.4.1 ChangeRelationController Class tests
#### 3.4.1.1 Tests for getListOfRelationships() method:

**Test 1:** Check the successful list retrieval when is the FA calling the method and there are family
relationships present.

**Test 2:** Check the failure of the list retrieval when the user calling the method is not a family administrator.

#### 3.4.1.2 Tests for changeRelation() method:

**Test 1:** Check the success case of changing an existing relationship between two members.

**Test 2:** Check the failure of changing a relation when there is no prior relation defined between the two members.

**Test 3:** Check the failure of changing the relation between members when the new relation name is not valid.

**Test 4:** Check the failure of changing a relation when one member does not exist.

**Test 5:** Check the failure of changing a relation when the user issuing the order is not a family administrator.

### 3.4.2 FamilyRelationTypeService Class tests
#### 3.4.2.1 Tests for getTypeFromListByDesignation() method:

**Test 1:** Check that the correct object is returned when an existing object has the same relation designation
as some String passed as argument.

**Test 2:** Check that a null object is returned when there is no object with the same relation designation
as some String passed as argument.

**Test 3:** Check that a null object is returned when a null String is passed as argument, as there are no
objects with a null designation.

### 3.4.3 FamilyRelationType Class tests
#### 3.4.3.1 Tests for isSameDesignation() method:

**Test 1:** Check that a boolean True is returned when a passed String designation is equal to the object's
String designation.

**Test 2:** Check that a boolean False is returned when a passed String designation is not equal to the object's
String designation.

**Test 3:** Check the returned boolean is False when a null String is to be compared to the object's
String designation.

### 3.4.4 FamilyRelationshipList Class tests
#### 3.4.4.1 Tests for getRelationshipListDescriptions() method:

**Test 1:** Check the successful list retrieval when there are relationships in the family.

**Test 2:** Check the successful list retrieval even when there are not relationships present in the family.

### 3.4.5 FamilyRelationship Class tests
#### 3.4.4.1 Tests for setRelationType() method (this is tested in the constructor):

**Test 3:** Check that the creation of a relationship with a null familyRelationType object is not possible.


# 4. Implementation

The implementation of this User Story was dependent on how the class FamilyRelationTypeService, FamilyRelationship 
and FamilyRelationType were implemented, since it needed to access information in each one and know how to retrieve it.

During Sprint02, structural changes were made, especially on how the relation types are defined.
Before, we had strings representing relation types, but that was changed to objects. This facilitated
relationships construction and information retrieval. Check [US105](\US105_CreateNewRelation.md) for more details.


# 5. Integration/Demonstration

The implementation of this User Story makes use of the not yet developed authentication procedure that validates the acting user
to make an allowed call for this functionality. Thus, this functionality needs the email address of the user that is currently
logged in to verify if the user is the family administrator of the family to which his email address is linked to.
If the user is not the family administrator of the family to which he belongs, he cannot call this functionality.


The functionality developed is further enhanced by how the family relations are created. As described in 
[US105](\US105_CreateNewRelation.md), the familyRelationship object contains a familyRelationType object that holds
the relation designation. For the relationship to change, the only thing needed is to change the familyRelationType
object defined for the familyRelationship.

# 6. Observations

Further functionalities will need to be implemented to ensure that the actor of US106 is the only one authorized
to call for the functionality developed, such as the user authentication procedure mentioned in the above section.

During Sprint02, it was necessary to change how the relation types are defined (from Strings to Objects).
Thus, some corrections needed to be made to the methods developed for [US105](\US105_CreateNewRelation.md),
[US104](../../sprint1/user_stories/US104_ListOfMembersRelations.md) and respective tests.





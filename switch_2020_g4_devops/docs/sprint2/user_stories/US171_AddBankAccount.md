# US171
=======================================


# 1. Requirements

*As a Family Member, I want to add a bank account I have.*

In this US, we create the ability to add a bank account to a family member of the family.
This FM can have one or more bank accounts and all accounts will be saved in an account list connected to the FM, respectively.


## 1.1 Client notes

According to the PO, more than one family member can share the same bank account and also, the balance of the bank account can have negative values.
Since PO said the balance will be added by a financial entity, the user introduces only the account's description, and the balance starts always with 0.


# 2. Analysis

The diagrams built explain how this US works.

The only occurrences that can block the account's creation is if description is null or empty or
if there is already an account with same description.

## 2.1. Relevant excerpt of the Domain Model


```plantuml

title Excerpt of Domain Model

object "Family" as fam {
id
name
}


object Person {
name
identification
vat
address
phoneNumberList
mainEmail
emailAddressList
}


object Account {
description
balance
}

object "Bank Account" as BA{
id
}

fam "1"--"1" Person : has admin
fam "1"--"1..*" Person : has members
Person "1"-"1..*" BA : has
Account <|.. BA

@enduml
```

# 2.2 Attributes

As a bank account, we assumed that FM has to complete some information that characterizes it, such as:

| **_Attributes_**            | **_Business Rules_**                                                                                                                  |
| :-------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------|
| **Description**             | Mandatory, String type, unique for each bank account. Describes the account and it's given by the FM.                                 |
| **ID**                      | Mandatory, int. Generated automatically by the system to better identify each account.                                                |

On BankAccount class we also added the balance as an attribute but, it will only be added by a financial entity. So, when FM creates the bank account, the balance will always be zero.

## 2.3 System Sequence Diagram


```plantuml
title SSD Add Bank Account
actor "Family Member" as SM
Participant System
SM -> System : add bank account
activate SM
activate System
System --> SM : asks data
SM -> System : input data
System --> SM : success
deactivate System
deactivate SM
```

## 2.4. Process View Diagram

```plantuml
title Process View of Add Bank Account
actor "Family Member" as SM
Participant FFM_UI
Participant FFM_BusinessLogic
SM -> FFM_UI : add bank account
activate SM
activate FFM_UI
FFM_UI --> SM : asks data 
deactivate FFM_UI
SM -> FFM_UI : input data
activate FFM_UI
FFM_UI -> FFM_BusinessLogic : addBankAccount(mainEmail, description)
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI : success
deactivate FFM_BusinessLogic
FFM_UI --> SM : success
deactivate FFM_UI
deactivate SM
```

# 3. Design

## 3.1. Class Diagram

The following class diagram shows the relationship between the classes involved in this US.

```plantuml

title **US171** Add Bank Account

class AddBankAccountController{
+addBankAccount(String description)
}

class FFMApplication{
+getFamilyService()
}

class FamilyService{
-families : List<Family>
+getFamilyByMemberMainEmail(EmailAddress mainEmail)
}

class Family{
+getMembers()
}

class PersonList{
-members : List<Person>
+getPersonByMainEmail(EmailAddress mainEmail)
}

class Person{
-mainEmail : EmailAddress
+getAccountList()
}

class AccountList{
-listOfAccounts : List<Account>
+addBankSavingsAccount()
}

class BankAccount{
+BankAccount(String description, int id)
}

abstract class FinancialEntityAccount {
-description : String
-id : int
+hasSameDescription(String description)
}

interface Account{
}

AddBankAccountController "1" -- "*" FFMApplication
FFMApplication"1" *-right- "1"FamilyService
FamilyService"1" o-- "*" Family
Family "1" *- "1" PersonList
PersonList "1" o-- "*" Person
Person "1" *-right- "1" AccountList
AccountList "1" *-right- "*" BankAccount
BankSavingsAccount -up-|> FinancialEntityAccount
FinancialEntityAccount .up.|> Account

@enduml
```

## 3.1. Functionality

The following SD represents the US functionality.

```plantuml
autonumber
title US171 - Add Bank Account
actor "Family Member" as FM
participant ":UI" as ui
participant ":AddBankAccountController" as ctrl
participant ":FFMApplication" as app
participant "familyService \n:FamilyService" as fs
participant "family \n:Family" as f
participant "personList \n:PersonList" as pl
participant "person \n:Person" as p
participant "accountList \n:AccountList" as al
activate FM
FM -> ui : add bank account
activate ui
ui --> FM : asks data
FM -> ui : inputs data \n(mainEmail, description)
ui -> ctrl : addBankAccount(mainEmail, description)
activate ctrl
ctrl -> app : getFamilyService()
activate app
app --> ctrl : familyService
deactivate app
ctrl -> fs : getFamilyByMemberMainEmail(mainEmail)
activate fs
fs --> ctrl : family
deactivate fs
ctrl -> f : getPersonList()
activate f
f --> ctrl : membersList
deactivate f
ctrl -> pl : getPersonByMainEmail(mainEmail)
activate pl
pl --> ctrl : person
deactivate pl
ctrl -> p : getAccountList()
activate p
p --> ctrl : accountList
deactivate p
ctrl -> al : addBankAccount(description)
activate al
ref over al : addBankAccount(description)
al --> ctrl : result
deactivate al
ctrl --> ui : result
deactivate ctrl
ui --> FM : result
deactivate ui
deactivate FM

@enduml
```

The FM requires to UI to add a bank account. UI passes information to AddBankAccountController that has to send
the request to the FFMApplication. After that, it is important to locate the person who is requesting inside the family respectively,
which is represented between step 5 and 12.

Found the FM, we call the addBankAccount(description) method to create the account.

Going deeper on addBankAccount(description)

```plantuml
autonumber
participant "accountList \n:AccountList" as p
participant ":Account" as la
[o-> p : addBankAccount(description)
activate p
loop for each account in accountList
p -> la : result = hasSameDescription(description)
activate la
la --> p : boolean
deactivate la
opt result == true
[<-- p : account already exists
end opt
end loop
p -> p : generateAccountId()
p -->  "newBankAccount:\n BankAccount" ** : create(description, accountId)
p -> p : add(newBankAccount)
[<-- p : success
deactivate p

@enduml
```

First we compare if already exists the same description on other created accounts in the Person's accountList.
If the answer is false, the accountList generates an id automatically and calls the BankAccount constructor. After creating the account, we add it to the list of accounts and answer to the system **success**.


## 3.3. Applied Patterns

**Controller pattern** - We create a class (AddBankAccountController) whose only purpose is to build a "bridge"
between the UI and the AppMembers.

**Information Expert** - Each Person in our app has a list of accounts, responsible for saving every person account. It also
has the responsibility to manage and create new accounts. It happens with FamilyService that deals with every *service* related to family.

**Low Coupling** - Creating an interface Account lows our class bank account coupling.

**High Cohesion** - AccountList was created to save all accounts, and to deal with accounts operations, leaving fewer responsibilities 
to accounts classes. 


## 3.4. Tests

In this section some tests will be presented that guarantee the requisites
of the US are applied.

A BeforeEach was created to all tests of AddBankAccountController:

    private static FFMApplication app;
    private static String emailLuis;

    @BeforeEach
    void createsFamilyWithMembers() {
        app = new FFMApplication();
        FamilyService familyService = app.getFamilyService();
        Family family = new Family("Rodrigues", new Date(), 20);
        familyService.addFamilyIfNotExists(family);
        PersonList personList = family.getPersonList();

        emailLuis = "luis@isep.ipp.pt";
        String emailMargarida = "margarida@isep.ipp.pt";
        String emailMarta = "marta@isep.ipp.pt";
        Person personLuis = new Person("Luis", "19913200 3ZY1", 0, new Date(), emailLuis, null, "male");
        Person personMargarida = new Person("Margarida", "19741658 6ZV2", 0, new Date(), emailMargarida, null, "female");
        Person personMarta = new Person("Marta", "18480344 6ZY2", 0, new Date(), emailMarta, null, "female");
        personList.addMember(personLuis);
        personList.addMember(personMargarida);
        personList.addMember(personMarta);

    }

**Test 1** : Creates a valid bank account.

    @Test
    @DisplayName("Creates a valid Bank Account")
    public void validBankAccountCreated() {
        String bankDescription = "Revolut";

        AddBankAccountController ctrlBank = new AddBankAccountController(app);
        boolean result = ctrlBank.addBankAccount("luis@isep.ipp.pt",bankDescription);

        assertTrue(result);
    }

**Test 2** : Does not create a bank account due to empty description

    @Test
    @DisplayName("Does not create a bank account due to empty description")
    public void bankAccountNotCreatedDueToEmptyDescription() {
        String description = "";

        AddBankAccountController ctrlBank = new AddBankAccountController(app);
        boolean result = ctrlBank.addBankAccount("luis@isep.ipp.pt",description);

        assertFalse(result);

    }

Regarding Bank Account class, the following tests were made:

**Test 1** : Does not create a bank account due to null description.

**Test 2** : Does not create a bank account due to empty description

**Test 3** : Bank account with same description

**Test 4**: Bank account with different description that others

**Test 5** : Creates a valid bank account

Looking to AccountList class we also performed some relevant tests:

**Test 1** : Description of bank account already exists on list.

**Test 2** : Does not add the bank account due to null description.

**Test 3** : Adds a valid Bank Account.

Not all tests are represented, but the most relevant are to assure this US is completely functional.

# 4. Implementation

The implementation of this US took a little longer than expected. There was some discussion related to abstract class and interface. Our main
goal was to avoid repeating code on all basic bank accounts, so we used an abstract class where all
the repeating attributes and methods were implemented. 
Also, to represent all accounts in system, we created an Account Interface that acts as a blueprint of all accounts.

Regarding the method of adding a bank account, was simple to construct.

    public boolean addBankAccount(String description) {
        for (Account account : this.listOfAccounts) {
            if (account.hasSameDescription(description)) {
            return false;
            }
            }
        int accountId = generateAccountId();
        Account newBankAccount = new BankAccount(description, accountId);
        this.listOfAccounts.add(newBankAccount);
        return true;
    }

# 5. Integration/Demonstration

The US does not depend on any other US, but others are dependent on this one, such as [US185] and the [US186].
By adding a bank account, it will be possible to see the balance and gets account's transactions.


# 6. Observations

We've found some doubts on where to save all members, because reaching each person to add and save the account
created, we have to do several steps like getting first the family service, family, then the member itself and finally the account list.
The alternative we thought about was, instead of saving all family members on family class, we would save on family service. 
For now, we will maintain the members on family class but, it is a valid alternative if in the future we find some issues related to the app performance.

[US185]: US185_CheckAccountBalance.md
[US186]: US186_GetTransactionsBetweenDates.md

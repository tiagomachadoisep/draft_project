# US135
=======================================


# 1. Requirements

*As a family administrator, I want to check the balance of the family’s cash account or
of a given family member's cash account.*

##1.1. Client Notes

Families registered in the app can have one cash account, and each user 
can also have one cash account. They are introduced manually by the FA, in the casa
of a family cash account, and by the user itself, in the case of a cash account
of a family member.

Payments or transfers can be registered with the different cash accounts, 
and they will alter their balance, by the value defined in the transaction.
Here, the FA wants to check the current balance of any given cash account
related to his family, be it the family's or any user's account.

##1.2. System Sequence Diagram

```plantuml
autonumber
title SSD Check cash account balance
actor "Family Member" as FM

Participant System
activate System
activate FM
FM -> System : check cash account balance
System --> FM : select: family or member
deactivate System
opt family
FM -> System : select family
activate System
System --> FM : inform balance
end
deactivate System
opt member
FM -> System : select member
activate System
System --> FM : show list of members, ask one
deactivate System
FM->System : select one
activate System
System --> FM : inform balance
deactivate System
end
deactivate FM
```

##1.3. Process View

```plantuml
title Process View
actor "Family Member" as FM

Participant UI
Participant "FFM_BusinessLogic" as BL 
activate UI
activate FM
FM -> UI : check cash account balance
UI --> FM : select: family or member
deactivate UI
opt
FM -> UI : select family
activate UI
UI -> BL : getCashAccountBalance(familyId)
activate BL
BL --> UI : result
deactivate BL
UI-> FM : shows result
deactivate UI
end
opt 
FM -> UI : select member
activate UI
UI-->FM : show list of members, ask one
deactivate UI
FM->UI : select one
activate UI
UI -> BL : getCashAccountBalance(mainEmail)
activate BL
BL --> UI : result
deactivate BL
UI-> FM : shows result
deactivate UI
end
deactivate FM

```
##1.4. Connection with other User Stories

This US is related to US120, where the family cash account is created, and to 
US170, where a user's cash account is created, since without it, we obviously 
can't check the balance of an account that doesn't exist.

It is also related to US130, US180 and US181, since these US contain operations
that would change the balance of a cash account.

Finally, US185 will use similar methods, since the actor of that US (the family member)
wants to check the balance of his own cash account.

# 2. Analysis

The basic flow we want to implement in this US goes through the following steps:
1. The family administrator, authenticated in the app, asks to check the balance of a
cash account
   
2. The system asks if he wants the family's cash account, or a family member's cash account

At this point the flow is divided into two paths:

a)
3. The FA selects the family
4. The system shows the current balance of the family's cash account

b)
3. The FA selects family member
4. The system shows a list of family members and asks the FA to select one (can also show only members with CA)
5. The FA selects one member
6. The system shows the current balance of the selecte family member's cash account


Possible occurrences:

If the family or the selected member has no cash account created yet, the system
should tell the FA this, and in case of the family, ask him if he wants to
create one.

If the family has no members, at step b4 the list will have just one element, the FA himself.



##2.1 Relevant excerpt of the Domain Model

```plantuml 
@startuml Domain Model

object Family{
name
id
}


object Person{
id
name
}

object CashAccount{
balance
}

Family "1" *- "1" CashAccount : has >
Person "1" *- "1" CashAccount : has >
Family "1" -- "*" Person : has members >
Family "1" - "1" Person  : has admin >

@enduml
```
##2.2. Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **balance**                 | Current balance of the Cash Account      |


# 3. Design



## 3.1. Functionality

The user Family Administrator, authenticated in the app, sends a request through the UI to check the 
cash account balance of his family. The UI communicates this request to an instance of the Controller responsible for this 
action, the CheckCashAccountBalanceController. 

The controller executes the following steps, starting by asking the application to refer him to the
instance of the service responsible for storing families, the FamilyService class. He obtains the 
correct instance of Family and uses a method to check if the family has a cash account defined. If it
doesn't, the UI is informed of the failure and reports this to the user, and the execution stops here.
If it does, the Controller asks for the balance of that cash account and returns this information to the UI,
who then transmits it to the user.

```plantuml
@startuml
autonumber
title US181 : a) check cash account balance of family
actor "Family Admin" as FA
participant UI
participant ": CheckCashAccountBalance\nController" as ctrl
participant ": FFMApplication" as app
participant "famServ : FamilyService" as serv
participant "family : Family" as fam
participant "cashAcc : CashAccount" as ca

activate FA
FA -> UI : check family cash \naccount balance
activate UI
UI -> ctrl : checkCashAccountBalance\nOfFamily(userMainEmail)
activate ctrl
ctrl -> app : famServ=getFamilyService()
ctrl -> serv : family=getFamilyByAdmin\nMainEmail(userMainEmail)

ctrl -> fam : result=hasCashAccount()
opt result==false
ctrl --> UI : fail
UI --> FA : family has no cash\naccount defined
end
ctrl -> fam : cashAcc=getCashAccount()
ctrl -> ca : balance=getBalance()
activate ca
ca -> ctrl : balance
deactivate ca
ctrl -> UI : balance
deactivate ctrl
UI->FA : show balance
deactivate UI
deactivate FA


@enduml
```

In the case where the FA wants to check the balance of another family member's cash account, the flow
is similar to the case presented before, however there is an extra step consisting of determining the user
to which the FA is referring to. In order to do that, the Controller passes to the UI a list of 
family members of the user's family (through a DTO), and the user chooses the one he wants.
After this, the process is very similar to the one of the family, only we have to get the correct instance
of Person, corresponding to the member in question. This is stored in the PersonList class, as indicated in the diagram below.
Again, a problem can occur where the member has no cash account defined, and the same thing will happen
here, the UI will get notified, and transmit to the user that the request had failed
for this reason. Otherwise, the final outcome will be the value of the balance of the selected member's
cash account.

```plantuml
@startuml
autonumber
title US181 : b) check cash account balance of family member
actor "Family Admin" as FA
participant UI
participant ": CheckCashAccountBalance\nController" as ctrl
participant ": FFMApplication" as app
participant "famServ : FamilyService" as serv
participant "family : Family" as fam
participant "members : PersonList" as members
participant "member : Person" as member
participant "accounts: AccountList" as list
participant "cashAcc : CashAccount" as ca

activate FA
FA -> UI : check member cash \naccount balance
activate UI
UI -> ctrl : getListOfFamilyMembers\nOfAdmin(userMainEmail)
activate ctrl
ctrl -> app : famServ:getFamilyService()
ctrl -> serv : family=getFamilyByAdmin\nMainEmail(userMainEmail)
ctrl -> fam : members=getMembers()
ctrl -> members : listOfMembers=getListOfMembers()
ctrl --> UI : listOfMembers
deactivate ctrl
UI--> FA : show list of members
deactivate UI
FA ->UI : select one
activate UI
UI -> ctrl : checkCashAccountBalanceOf\nMember(userMainEmail,mainEmail)
activate ctrl
ctrl -> app : famServ=getFamilyService()
ctrl -> serv : family=getFamilyByAdmin\nMainEmail(userMainEmail)
ctrl -> fam : members=getMembers()
ctrl -> members : member=getPersonByMainEmail(mainEmail)
ctrl -> member : getAccountList()
ctrl -> list : result=hasCashAccount()
opt result==false
ctrl --> UI : fail
UI --> FA : member has no cash\naccount defined
end
ctrl -> list : cashAcc=getCashAccount()
ctrl -> ca : balance=getBalance()
activate ca
ca -> ctrl : balance
deactivate ca
ctrl -> UI : balance
deactivate ctrl
UI->FA : show balance
deactivate UI
deactivate FA


@enduml
```



## 3.2. Class Diagram

```plantuml
@startuml

class "CheckCashAccountBalance\nController" as ctrl{
checkCashAccountBalanceOfFamily(Email userMainEmail)
checkCashAccountBalanceOfMember\n(Email userMainEmail,\Email membermainEmail)
}

class FFMApplication{
getFamilyService()
}

class FamilyService{
getFamilyByAdminMainEmail\n(EmailAddress mainEmail)
}

class Family{
PersonList getMembers()
boolean hasCashAccount()
CashAccount getCashAccount()
}

class PersonList{
getPersonByMainEmail\n(EmailAddress mainEmail)
}

class Person{
mainEmail : EmailAddress
AccountList getAccountList()
}

class AccountList{
listOfAccounts : List<Account>
bolean hasCashAccount()
CashAccount getCashAccount()
}

class CashAccount{
balance : double
double getBalance()
}



interface Account{
}

ctrl "1" -- "*" FFMApplication
FFMApplication"1" *- "1"FamilyService
FamilyService"1" *-- "*" Family
Family "1" *- "1" PersonList
PersonList "1" *-- "*" Person

Person "1" *-- "1" AccountList
AccountList "1" *- "1" CashAccount
Family "1" *- "1" CashAccount

CashAccount -|> Account

@enduml
```

## 3.3. Applied Patterns

In the design process of this US we applied the patterns

- Information Expert: we assign the responsibilities related to objects
  to the class that has the information necessary to fulfil them. For example,
  the class AccountList stores the accounts of a given member, so it is responsible for informing
  whether he has a cash account or not
  
- Creator: the class Family has an attribute CashTransaction,
so it makes sense that it is responsible for creating instances of this class as well.
  
- Pure Fabrication: we introduce classes that do not correspond to any of
the concepts of the problem domain, but are useful to achieve the goals
  of the other design patterns (for example, the Controller and the Service
  classes, as well as the FFMApplication).

- Controller: the class CheckCashAccountBalanceController is a bridge between
the UI and our system, and has the responsibility of dealing with system 
  events, in this case checking the balance of a given person or family cash account.
  
- Low Coupling/High Cohesion: this pattern consists of keeping the classes
focused, with responsibilities that are related. This is accomplished 
  by keeping classes weakly coupled, that is, weakly connected to other 
  classes, limiting the knowledge and reliance the class has with other
  elements of the system. In this case, we split the relationships so that,
  for example, the operations related to the accounts are dealt with by the class
  AccountList, leaving the class Person with a more focused set of responsibilities,
  one of which is directing to the AccountList class.
  
- Protected Variation/Polymorphism: since we will be able to, in this application,
import transactions from different systems (for example, e-fatura or specific
  bank systems), we want to make a sort of template for the transactions. 
  That is, we require each class responsible for handling transactions to 
  provide a set of business methods that allow us to extract information
  we need for a specific use case scenario. This is done through the introduction
  of an interface, which all the classes handling the different types of transactions
  will implement. The details on how each of them handles each method are
  irrelevant to us, as long as the information is being provided according to 
  the template.

- Polymorphism: since we have different classes implementing the same general behaviour, namely the 
Account interface, the responsibility to define the behaviour will lay on the class implementing it,
  and each class can have a different way of doing it, leading to the existence of methods with the 
  same name but different implementations.

- Protected Variation: this pattern protects elements from the variation of other elements, by wrapping
the possible variations in an interface (Account in this case) and using polymorphic methods to implement different objects corresponding
  to this interface.

## 3.4. Tests 
In order to test this US, we use the following setup to initialize instances of the relevant classes, namely 
two different families with administrators, where one has a cash account and the other does not, and in the first family four 
members, of which three have cash accounts.
````
private static FFMApplication app;
private static Date date;
    
@BeforeAll
    static void initializeApplication(){
        app=new FFMApplication();

        Family family = new Family("Silva",1);
        Family familyTwo = new Family("Costa",2);

        FamilyService familyService = app.getFamilyService();
        familyService.addFamilyIfNotExists(family);
        familyService.addFamilyIfNotExists(familyTwo);

        date = new Date();

        String adminEmailAddress = "admin@isep.ipp.pt";
        Person admin = new Person("Rui","18480344 6ZY2",date,adminEmailAddress,"male");

        String emailAddressOne = "ze@isep.ipp.pt";
        Person personOne = new Person("Ze","19741658 6ZV2",date,emailAddressOne,"male");

        String emailAddressTwo= "ana@isep.ipp.pt";
        Person personTwo = new Person("Ana","19913200 3ZY1",date,emailAddressTwo,"female");

        String emailAddressThree = "anaSS@isep.ipp.pt";
        Person personThree = new Person("Ana","13686553 4ZZ8",date,emailAddressThree,"female");

        PersonList personList = family.getPersonList();
        personList.addAdmin(admin);
        personList.addMember(personOne);
        personList.addMember(personTwo);
        personList.addMember(personThree);


        String adminEmailAddressTwo = "admin2@isep.ipp.pt";
        Person adminTwo = new Person("Ana","13686553 4ZZ8",date,adminEmailAddressTwo,"female");

        PersonList personListTwo = familyTwo.getPersonList();
        personListTwo.addAdministratorUsingDTO(adminTwo.toDTO());

        CashAccount personalCashAccount = new CashAccount(350.2);
        AccountList listOfAccounts = admin.getPersonAccountList();
        listOfAccounts.addAccount(personalCashAccount);

        family.addCashAccount(2000.65);

        CashAccount personalCashAccountOne = new CashAccount(100);
        AccountList listOfAccountsOne = personOne.getPersonAccountList();
        listOfAccountsOne.addAccount(personalCashAccountOne);

        CashAccount cashAccountThree = new CashAccount(0);
        AccountList listOfAccountsThree = personThree.getPersonAccountList();
        listOfAccountsThree.addAccount(cashAccountThree);
    }
````
**Test 1**: Check the cash account balance of the family having a cash account - ***pass***
````
@Test
    void checkCashAccountBalanceOfFamily() {
        //Arrange
        CheckCashAccountBalanceController ctrl = new CheckCashAccountBalanceController(app);
        String adminEmail = "admin@isep.ipp.pt";
        double expected = 2000.65;
        //Act
        double result = ctrl.checkCashAccountBalanceOfFamily(adminEmail).getContent();
        //Assert
        assertEquals(expected,result);
    }
````
**Test 2**: Check the cash account balance of the family not having a cash account - ***fail***
````
 @Test
    void checkCashAccountBalanceOfFamilyDoesntHave() {
        //Arrange
        CheckCashAccountBalanceController ctrl = new CheckCashAccountBalanceController(app);
        String adminEmail = "admin2@isep.ipp.pt";
        //Act & Assert
        assertFalse(ctrl.checkCashAccountBalanceOfFamily(adminEmail).hasContent());
    }
````
**Test 3**: Check the cash account balance of a family member having a cash account - ***pass***
````
@Test
    void checkCashAccountBalanceOfFamilyMember() {
        //Arrange
        String adminEmail = "admin@isep.ipp.pt";
        String memberEmail = "ze@isep.ipp.pt";
        CheckCashAccountBalanceController ctrl = new CheckCashAccountBalanceController(app);
        double expected= 100;
        //Act
        double result=ctrl.checkCashAccountBalanceOfFamilyMember(adminEmail,memberEmail).getContent();
        //Assert
        assertEquals(result,expected);
    }
````
**Test 4**: Check the cash account balance of a family member not having a cash account - ***fail***
````
@Test
    void checkCashAccountBalanceOfFamilyMemberDoesntHave() {
        //Arrange
        String adminEmail = "admin@isep.ipp.pt";
        String memberEmail = "ana@isep.ipp.pt";
        CheckCashAccountBalanceController ctrl = new CheckCashAccountBalanceController(app);
        //Act & Assert
        assertFalse(ctrl.checkCashAccountBalanceOfFamilyMember(adminEmail,memberEmail).hasContent());
    }
````
# 4. Implementation
In this US we want to get the value of the attribute value of an object of the class CashAccount. We start by
presenting the interface Account, which the class implements.
````
public interface Account {
    double getBalance();
    String getDescription();
    boolean hasSameDescription(String description);
    boolean isCash();
    AccountDTO toDTO();
}
````
This describes a contract all the classes implementing this interface must follow.
It is then the job of each individual class to implement the methods in the correct 
way for it.
In the case of CashAccount, we are just interested in obtaining the balance,
so we present it below.
````
public class CashAccount implements Account {

    private MoneyValue moneyBalance;
    private final String description;
    private final List<Transaction> transactionList;

  public double getBalance() {
        return this.moneyBalance.getValue();
    }
 }
````
This contains the value the user is interested in. Regarding the organization
inside the system, while in the case of families (where the only type of account possible
is a cash account) the account is stored within the class Family, in the case of
members we have a special class with the responsibilities regarding accounts (creating,
storing and operating on accounts) called AccountList.
````
public class AccountList {

    private final List<Account> listOfAccounts;
    
    public CashAccount getCashAccount(){
        for (Account account : listOfAccounts) {
            if (account.isCash())
                return (CashAccount) account;
        }
        throw new IllegalArgumentException("The member do not have a cash account");
    }
}
````
As we can see, if the owner of an AccountList has no cash account, an exception will
be thrown and caught by the Controller, who will report this to the UI.
````
public class CheckCashAccountBalanceController {

    private final FFMApplication app;


    CheckCashAccountBalanceController(FFMApplication app){
        if(app==null)
            throw new IllegalArgumentException("AppMembers cannot be null");
        this.app=app;
    }

    public Result<Double> checkCashAccountBalanceOfFamily(String userMainEmail){
        FamilyService familyService =app.getFamilyService();
        Family family = familyService.getFamilyByAdminMainEmail(userMainEmail);
        if (!family.hasCashAccount())
            return Result.createFail();
        CashAccount cashAccount = family.getCashAccount();
        return Result.createSuccess(cashAccount.getBalance());
    }

    public List<PersonDTO> getListOfFamilyMembers(String userMainEmail){
        FamilyService familyService =app.getFamilyService();
        Family family = familyService.getFamilyByAdminMainEmail(userMainEmail);
        PersonList members = family.getPersonList();
        return members.getMembersAsDTO();
    }

    public Result<Double> checkCashAccountBalanceOfFamilyMember(String userMainEmail, String memberMainEmail){
        FamilyService familyService =app.getFamilyService();
        Family family = familyService.getFamilyByAdminMainEmail(userMainEmail);
        PersonList members = family.getPersonList();
        Person member = members.getPersonByMainEmail(memberMainEmail);
        AccountList accountList = member.getPersonAccountList();
        try {
            CashAccount cashAccount = accountList.getCashAccount();
            return Result.createSuccess(cashAccount.getBalance());
        }catch(IllegalArgumentException e) {
            return Result.createFail();
        }
    }
}
````
The Result class was created in order to return to the UI information
about the success or failure of the operation, along with the value of the 
balance in the case of success.
````
public class Result<T> {
    private final boolean success;
    private final T content;

    private Result(boolean success, T content) {
        this.success = success;
        this.content = content;
    }

    public boolean hasContent() {
        return this.success;
    }

    public T getContent() {
        return this.content;
    }

    public static<T>Result<T> createSuccess(T value) {
        return new Result(true, value);
    }

    public static<T>Result<T> createFail() {
        return new Result(false,null);
    }

}
````
# 5. Integration/Demonstration
This US is integrated in the system through its actor, the FA created in [US011](/docs/sprint1/user_stories/US011_AddFamilyAdministrator.md), and the information he requests
was introduced in [US120](/docs/sprint1/user_stories/US120_CreateFamilyCashAccount.md) for the family cash account, and [US170](US170_CreatePersonalCashAccount.md) for a personal cash account.

# 6. Observations

This US didn't present many difficulties, since its goal is very straightforward. Some questions that were
discussed however were what would happen when a member or family had no cash account defined.

The first idea was to only show a list of members with cash accounts, but that could lead to confusion by the user (FA)
since he would see an incomplete list of members, so we decided to show the whole list and then notify him
with an error if the member he selected didn't have a cash account.
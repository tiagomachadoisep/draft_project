# US173
=======================================

# 1. Requirements

*As a Family Member, I want to add a credit card account.*

The only requirement we have for this US173 is that the FM input a description for his new credit card.

## 1.1 Client Notes

Our client informed us that one person can have more than one CreditCardAccount, and all movements will be charged
afterwards throughout a connection in our app.

## 1.2 System Sequence Diagram

```plantuml
autonumber
title SSD Add Credit Card Account
actor "FamilyMember" as FM
activate FM

Participant System
activate System
FM -> System : addCreditCardAccount
System --> FM : ask data (description)
deactivate System
FM -> System : inputs required data
activate System
System --> FM : inform success
deactivate System
deactivate FM

```

## 1.3 Process View

```plantuml
@startuml
autonumber
title Process View
actor "FamilyMember" as FM
participant ":FFM_UI" as UI
participant ":FFM_BusinessLogic" as BL

activate FM
FM -> UI : asks to addCreditCardAccount
activate UI
UI -> FM : asks for data
deactivate UI
FM -> UI : informs data
activate UI
UI -> BL : add data 
activate BL
BL -> UI : result
deactivate BL
UI -> FM : shows result
deactivate UI
deactivate FM
@enduml
```

## 1.4 Connection with other User Stories

This US does not dependent in any other US, and there not other US dependent on this one.

# 2. Analysis

This process permit that a credit card account is created for one person.

### 2.1 Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **Id**                      | Mandatory, int. Generated automatically by the system to better identify each account.               |
| **Description**             | Mandatory, brief description to improve user experience            |

### 2.2 Relevant excerpt of the Domain Model

Next we can see a fraction of our domain diagram with the needed entities for this user story

```plantuml
skinparam linetype polyline
object "Family" as fam {
name
familyId
}

object Person {
id
name
}

object Account {
}

object "CreditCardAccount" as CCA{
description
balance
}

fam "1"---"1" Person : has admin
fam "1"--"1..*" Person : has members
Person "1"--"1..*" CCA : has
Account --|> CCA

@enduml
```

# 3. Design

## 3.1. Functionality

First the Family Member (FM) ask the User Interface (UI) to add a credit card account, and the UI asks back for
the data needed to the operation. We considered that to add a credit card account the FM need to input two arguments:

- balance: represents the value contracted with some financial entity to be available for the FM
- description: brief description choose by the FM to identify their account

Considering that everything went well in the first step, throughout a controller class we communicate with the FFMApplication to
get the service responsible for create cash accounts, FamilyService.

We choose to create an account list in each Person to save their accounts, with the FM main email we reach the account
list and save in that list the new CreditCardAccount.

Since each Person can have more than one CreditCarAccount, we verify if the given description don't repeat any of the ones
list, and if it's a new description we add the new Account, and a success message is sent to the FM.

Next you may find the sequence diagram for this user story:

```plantuml 
@startuml
autonumber
title **US173** - Add CreditCardAccount
actor "FamilyMember" as FM
participant ":UI" as UI
participant ":AddCreditCardAccountController" as CCAC
participant ":FFM\nApplication" as AppMembers
participant "accountService:\nAccountService" as aS 
participant "accountFactory:\nAccountFactory" as aF

activate FM
FM -> UI : addCreditCardAccount(description)
activate UI
UI -> CCAC : addCreditCardAccount\n(mainEmail, description)
activate CCAC
CCAC -> AppMembers : getAccountService()
activate AppMembers
AppMembers --> CCAC : accountService
deactivate AppMembers
CCAC -> aS : addCreditCardAccount()
activate aS
aS -> aF : addCreditCardAccount
activate aF
aF -> aS : Account
deactivate aF
aS -> aS : addAccount
aS -> CCAC : result
deactivate aS

CCAC --> UI : result
deactivate CCAC
UI --> FM : result
deactivate UI
deactivate FM
@enduml
```


## 3.2. Class Diagrams

```plantuml
@startuml
AddCreditCardAccountController - FFMApplication : asks >
FFMApplication -- FamilyService : asks >
FamilyService - Family : asks >
Family -- PersonList : asks >
PersonList - Person : asks >
Person -- AccountList : has >
AccountList -- CreditCardAccount : has >

class AddCreditCardAccountController{
- FFMApplication : application
+ addCreditCardAccountController\n(balance,financialEntity)
}

class FFMApplication{
- familyService : FamilyService
+ getFamilyService()
}

class FamilyService{
- families : List <Family>
+ getFamilyByMemberMainEmail(mainEmail)
}

class Family {

+ getPersonService()
}

class PersonList {
- members : List <Person>
+ addCreditCardAccountController\n(balance,financialEntity)
}

class Person {
- creditCardAccount : CreditCardAccount
+ addCreditCardAccountController\n(balance,financialEntity)
}

class AccountList{
- accountList : AccountList
+ generateAccountId()
+ addCreditCardAccount()
}

class CreditCardAccount {
- description : String
- id : int
+ CreditCardAccount(description,id) 
}
@enduml
```

## 3.3. Applied Patterns

**Controller pattern** - We create a class (CreditCardAccountController) whose only purpose is to build a "bridge"
between the UI and the AppMembers

**Information Expert** - Each Person in our app has a list of accounts, responsible for saving every person account, so
is that account list who is responsible for creating new credit card accounts.

**Low Coupling** - There's almost no dependencies to add a credit card account, the Class AQccount List only need
another class (CreditCardAccount) to instantiate the object.

**High Cohesion** - We create a accountList to delegate responsibility, this way FFMApplication just need to know the
class responsible to add a creditCardAccount, in this US173 as in other similar US, we save accounts in a list that
every Person has as an attribute.

## 3.4. Tests

Implemented tests related to this US:

_addCreditCardController_

**Test1:** Fail to addCreditCardAccountController - app is null

**Test2:** AddCreditCardAccountController - Success

**Test3:** AddCreditCardAccountController - Success with different description

**Test4:** Fail to AddCreditCardAccountController - same description

_CreditCardAccount constructor_

**Test5:** Constructor with given description - Success

    @Test
    @DisplayName("Constructor with given description - Success")
    void CreditCardAccountConstructorTestWithDescription() {
        CreditCardAccount newCreditCardAccount = new CreditCardAccount("CC Maria");
        assertNotNull(newCreditCardAccount);
    }

**Test6:** Constructor with given description and id - Success

**Test7:** Exception on creating a CreditCardAccount with null description

**Test8:** Exception on creating a CreditCardAccount with blank description

_hasSameDescription(String description)_

**Test9:** CreditCardAccount - Same Description True

**Test10:** CreditCardAccount - Same Description False

_AccountList - addCreditCardAccount_

**Test11:** AddCreditCardAccount - Success

**Test12:** AddCreditCardAccount Fail - Same Description


# 4. Implementation

There was not much complexity developing methods related to this US, we consider that to start a creditCardAccount 
the only mandatory attribute should be the description, used by the users to identify their accounts.

    public boolean addCreditCardAccount(String description) {
        for (Account account : this.accountList) {
            if (account.hasSameDescription(description))
                return false;
        }

        int newAccountId = generateAccountId();

        CreditCardAccount newCreditCardAccount = new CreditCardAccount(description, newAccountId);

        return accountList.add(newCreditCardAccount);
    }


# 5. Integration/Demonstration

The US does not depend on any other US, but others are dependent on this one, such as [US185](US185_CheckAccountBalance.md) and the [US186](US186_GetTransactionsBetweenDates.md).
Adding a new credit card account to the FM account's list,is mandatory to be able to perform
the actions demonstrated in these other US.

# 6. Observations

In this US 173 we considered that the attribute balance is not mandatory since all movements will be charge afterwards,
but if the user for some reason want to start the account with some value, it's still possible. 





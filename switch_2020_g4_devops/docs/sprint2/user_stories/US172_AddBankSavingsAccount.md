# US172
=======================================


# 1. Requirements

*As a family member, I want to add a bank savings account I have.*

With this US, the FM should gain the ability to add a bank savings account to his/hers profile.
The initial balance (and subsequent changes) of this account should not be entered manually by the FM, but retrieved from
a bank extract.

## 1.1 Client Notes

According to the PO, more than one family member can share the same bank savings account.

## 1.2 System Sequence Diagram

Next, we present the System Sequence Diagram.

```plantuml
autonumber
title SSD Add Banking Savings Account
actor "Family Member" as FM
activate FM
Participant System
FM -> System : add bank savings account
activate System
System --> FM : ask data
deactivate System
FM -> System : inputs required data (description)
activate System
System --> FM : informs result
deactivate System
deactivate FM
``` 

## 1.3 Process view

Below it's presented the process view diagram:

```plantuml
autonumber
title SSD Add Banking Savings Account
actor "Family Member" as FM
Participant UI
Participant FFM_BusinessLogic as AppMembers
activate FM
FM -> UI : add bank savings account
activate UI
UI --> FM : ask data
deactivate UI
FM -> UI : input required data (description)
activate UI
UI -> AppMembers : addBankSavingsAccount(description)
activate AppMembers
AppMembers --> UI : result
deactivate AppMembers
UI --> FM : show result
deactivate UI
deactivate FM
``` 

## 1.4 Connection with other User Stories

This US is not directly dependent on any other. However, the [US185](US185_CheckAccountsBalance.md) and the
[US186](US186_GetTransactionsBetweenDates.md) does depend on this US if, respectively, the FM requests to see the balance
of his/hers bank savings account or if the FM asks for the transactions of a bank savings account to be retrieved.

# 2. Analysis

Next, it is presented the BankSavingsAccount list of attributes, and a relevant excerpt of the domain model
applicable to this US.

## 2.1 Attributes

The bank savings account that will be created will have the following attributes:

| **_Attributes_**            | **_Business Rules_**                                                                                             |
| :-------------------------- | :----------------------------------------------------------------------------------------------------------------|
| **balance**                 | Mandatory, int type, can have negative values and the balance introduced can be an estimated value.              |
| **description**             | Mandatory, String type, unique for each bank savings account. Describes the account and it's given by the FM.    |
| **entity**                  | Mandatory, String type, gives information regarding the financial entity associated to the account.              |
| **transactionList**         | Mandatory, List<Transaction> type, saves all the transactions related to the account.                            |
| **id**                      | Mandatory, int type, unique identifier to distinguish from other accounts of the FM.                             |


## 2.2 Relevant excerpt of the Domain Model

Below it's presented a partial Domain Model with the relevant information:

```plantuml

title Excerpt of Domain Model

object Family {
members
}

object Person {
mainEmail
accountList
}

object BankSavingsAccount {
description
id
}

Family "1"--"1" Person : has admin
Family "1"--"1..*" Person : has members
Person "1"-"1..*" BankSavingsAccount : has

@enduml
```

Above we can see the family and person objects, which are related in the sense that
each family has one or more members and always exactly one family administrator.
Also, each person can have a bank savings account, which is an implementation of a FinancialEntityAccount. 

# 3. Design

## 3.1. Functionality

The FM asks to add a bank savings account to his/hers profile, and the UI requests
the user to insert the description intended for that new account. The main email is supposed to
transfer from the authentication process. The data requested is input, and the UI
passes the requests to the AddBankSavingsAccountController which, in its turn,
requests to the FFMApplication request to its FamilyService. The familyService
is then reached by the Controller for it to get the family object of the family member doing the request.
Afterwards, the Controller accesses the personList inside that specific family, and finds the intended
person object by matching its main email, that corresponds to the FM. Finally, the controller asks that
person object to add a bank savings account to its accountList, which it does and then returns the result
of that process all the way until the UI, that shows that information to the FM.

```plantuml 
@startuml
autonumber
title **US172** - Add Bank Savings Account
actor "FamilyMember" as FM
participant ":UI" as UI
participant ":AddBankSavingsAccountController" as CTRL
participant ":FFMApplication" as APP
participant "familyService:\nFamilyService" as FS
participant "family:\nFamily" as F
participant "personList:\nPersonList" as PL
participant "person:\nPerson" as P
participant "accountList \n:AccountList" as al

activate FM
FM -> UI : add bank savings account
activate UI
UI --> FM : asks data (description)
deactivate UI
FM -> UI : inputs data required
activate UI
UI -> CTRL : addBankSavingsAccount\n(description)
activate CTRL
CTRL -> APP : getFamilyService()
activate APP
APP --> CTRL : familyService
deactivate APP
CTRL -> FS : getFamilyByMemberMainEmail(mainEmail)
activate FS
FS --> CTRL : family
deactivate FS
CTRL -> F : getPersonList()
activate F
F --> CTRL : personList
deactivate F
CTRL -> PL: getPersonByMainEmail(mainEmail)
activate PL
PL --> CTRL : person
CTRL -> P : getAccountList()
deactivate PL
activate P
P --> CTRL : accountList
deactivate P
CTRL -> al : addBankSavingsAccount\n(description)
activate al
ref over al
addBankSavingsAccount
(description)
end ref
al --> CTRL : result
deactivate al
CTRL --> UI : result
deactivate CTRL
UI --> FM : show result
deactivate UI
deactivate FM
@enduml
```

Diving more deep into the addBankSavingsAccount(description) method, it's possible to see
that a loop over all the accounts present in the accountList of the person object
is executed. This serves to verify that no other account in the user's list of accounts
has that same description, as it is not allowed. If a match is found, the process
aborts, and the information that the account already exists is passed outside the method.
If no match is found, the AccountList generates a unique id to use in the creation
of the new bankSavingsAccount object. That object is then, immediately, added to the list
of accounts in question. Information of the successful operation is then passed to outside
the method.

```plantuml
autonumber
title addBankSavingsAccount(description)
participant "accountList \n:AccountList" as p
participant ":Account" as la
[o-> p : addBankSavingsAccount(description)
activate p
loop for each account in accountList
p -> la : result = hasSameDescription(description)
activate la
la --> p : result
deactivate la
opt result == true
[<-- p : failure
end opt
end loop
p -> p : generateAccountId()
p -->  "bankSavingsAccount:\n BankSavingsAccount" ** : create(description, \naccountId)
p -> p : add(bankSavingsAccount)
[<-- p : success
deactivate p

@enduml
```

## 3.2. Class Diagrams

The class diagram is represented below:

```plantuml
@startuml

class AddBankSavingsAccountController{
+addBankSavingsAccount(String description)
}

class FFMApplication{
+getFamilyService()
}

class FamilyService{
-families : List<Family>
+getFamilyByMemberMainEmail(EmailAddress mainEmail)
}

class Family{
+getMembers()
}

class PersonList{
-members : List<Person>
+getPersonByMainEmail(EmailAddress mainEmail)
}

class Person{
-mainEmail : EmailAddress
+getAccountList()
}

class AccountList{
-listOfAccounts : List<Account>
+addBankSavingsAccount()
}

class BankSavingsAccount{
+BankSavingsAccount(String description, int id)
}

abstract class FinancialEntityAccount {
-description : String
-id : int
+hasSameDescription(String description)
}

interface Account{
}

AddBankSavingsAccountController "1" -- "*" FFMApplication
FFMApplication"1" *-right- "1"FamilyService
FamilyService"1" o-- "*" Family
Family "1" *- "1" PersonList
PersonList "1" o-- "*" Person
Person "1" *-right- "1" AccountList
AccountList "1" *-right- "*" BankSavingsAccount
BankSavingsAccount -up-|> FinancialEntityAccount
FinancialEntityAccount .up.|> Account
@enduml
```


## 3.3. Applied Patterns

- Information Expert: we assign the object-related responsibilities
  to the class that has the information necessary to fulfil them. For instance,
  the class AccountList is the responsible for storing and telling
  information about the accounts.

- Creator: the class AccountList aggregates instances of 
  different implementations of the Account class,
  so it makes sense that it is responsible for creating them as well.

- Pure Fabrication: we introduce classes that do not correspond to any of
  the concepts of the problem domain, but are useful to achieve the goals
  of the other design patterns (for example, the Controller and the Service
  classes).

- Controller: the class AddBankSavingsAccountController is a bridge between
  the UI and our system, and has the responsibility of dealing with system
  events, in this case creating and adding a bank savings account to the accountList
  of an instance of a Person.

- Low Coupling/High Cohesion: this pattern consists of keeping the classes
  focused, with responsibilities that are related. This is accomplished
  by keeping classes weakly coupled, that is, weakly connected to other
  classes, limiting the knowledge and reliance the class has with other
  elements of the system. In this case, we split the relationships so that,
  for example, the operations related to the accounts are dealt with by the class
  AccountList, leaving the class Person with a more focused set of responsibilities,
  one of which is directing to the AccountList class.

- Protected Variation/Polymorphism: since we will be able to, in this application,
  model accounts of different types (bank account, bank savings account, credit card account),
  we made a sort of template for the accounts.
  That is, we require each class responsible for handling accounts to
  provide a set of business methods that allow us to extract information
  we need for a specific use case scenario. This is done through the introduction
  of an interface, which all the classes handling the different types of accounts
  will implement. The details on how each of them handles each method are
  irrelevant to us, as long as the information is being provided according to
  the template. Although the methods may have the same name, their implementation
  will be different in each of the types of account.

## 3.4. Tests

In order to perform the following tests, we initialize an application with this data:
one family with one member already included.

    @BeforeAll
    static void createsFamilyWithMembers() {
        app = new FFMApplication();
        FamilyService familyService = app.getFamilyService();
        Family family = new Family("Sousa", new Date(), 20);
        familyService.addFamilyIfNotExists(family);
        emailAna = "ana@isep.ipp.pt";
        Person personAna = new Person("Ana", "18480344 6ZY2", 0, new Date(), emailAna, null, "female");
        PersonList plist = family.getPersonList();
        plist.addMember(personAna);
    }

**Test 1**: Adding a bank savings account to Ana's accountList: fail because app is null.

    void appIsNullFailure() {
        assertThrows(IllegalArgumentException.class, () -> {
            AddBankSavingsAccountController ctrl = new AddBankSavingsAccountController(null);
        });
    }

**Test 2**: Adding a bank savings account to Ana's accountList: success with empty accountList.

    void addBankSavingsAccountSuccessEmptyAccountList() {
        String description = "Poupanças";
        AddBankSavingsAccountController ctrlBankSavingsAccountController = new AddBankSavingsAccountController(app);
        boolean result = ctrlBankSavingsAccountController.addBankSavingsAccount(emailAna, description);
        assertTrue(result);
    }

**Test 3**: Adding a bank savings account to Ana's accountList: success with non-repeated description.

    void addBankSavingsAccountSuccessNonRepeatedDescription() {
        AddBankSavingsAccountController ctrlBankSavingsAccountController = new AddBankSavingsAccountController(app);
        ctrlBankSavingsAccountController.addBankSavingsAccount(emailAna, "Fundo Emergência");
        boolean result = ctrlBankSavingsAccountController.addBankSavingsAccount(emailAna, "Férias");
        assertTrue(result);
    }

**Test 4**: Adding a bank savings account to Ana's accountList: fail because repeated descriptions are not allowed.

    void addBankSavingsAccountTestFailureRepeatedDescription() {
        AddBankSavingsAccountController ctrlBankSavingsAccountController = new AddBankSavingsAccountController(app);
        ctrlBankSavingsAccountController.addBankSavingsAccount(emailAna, "Fundo Emergência");
        boolean result = ctrlBankSavingsAccountController.addBankSavingsAccount(emailAna, "Fundo Emergência");
        assertFalse(result);
    }

**Test 5**: Adding a bank savings account to Ana's accountList: fail because description cannot be null.

    void addBankSavingsAccountFailureNullDescription() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount("BPI", 1);
        assertThrows(IllegalArgumentException.class, () -> {
            boolean result = bankSavingsAccount.hasSameDescription(null);
        });
    }

**Test 6**: Adding a bank savings account to Ana's accountList: fail because description cannot be empty.

    void addBankSavingsAccountFailureEmptyDescription() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount("BPI", 1);
        assertThrows(IllegalArgumentException.class, () -> {
            boolean result = bankSavingsAccount.hasSameDescription("");
        });
    }

# 4. Implementation

In this project, we implement the accounts using the interface Account. This interface serves as a sort of 'template class'
for all accounts in the system. Interfaces generally answer the question of what the objects are supposed to do,
leaving how to do it to the implementation of each specific class.

In order to do that, we established some methods (just their signature) in the interface Account, and they will have to be
implemented in all the classes responsible for accounts, each in their own way.
Below is the interface constructed, that served as a framework for the implementation of this and other US.

```
public interface Account {
  
  double getBalance();
  String getDescription();
  boolean validateDescription(String description);
  boolean hasSameDescription(String description);
  boolean isCash();
  boolean isFinancialEntityAccount();
  List<Transaction> getTransactionList();
  List<TransactionDTO> getTransactionsBetweenDates(Date dateOne, Date dateTwo);
  AccountDTO toDTO();
}
```

# 5. Integration/Demonstration

While no other US is dependent on this one, it is dependent on others, namely
the [US185](US185_CheckAccountBalance.md) and the [US186](US186_GetTransactionsBetweenDates.md).
Adding a new account to the FM account's list, was, then, instrumental for him/her to be able to perform
the actions demonstrated in these other US.

# 6. Observations

There was not any significant debate that sparked between the group regarding this US,
as it is relatively straight forward.



# US111
=======================================

# 1. Requirements

*As a family administrator, I want to add a category to the family’s category tree.*

## 1.1 Client notes

This US states that a FA should be able to add a category to the list of categories on the family's category tree.

The requirements specified by the PO which are relevant to US111 were divided in two groups, the first one relating to
the FA:

There is only one FA per family;

- The same person may belong to more than one family, and therefore can be the family administrator of more than one
  family;
- A FA has to use distinct emails to access the different families he/she manages.

Concerning the family's category tree, the following was stated:

- The family's category tree refers to set of categories that are used to typify the transactions undertaken by FM, so
  that expenses may be analyzed by category or set of categories;
- SWS provides a standard category tree which can be customized by the family through the creation of new branches and
  leaves, or by creating new categories at the root level;
- A FA can create customized categories and subcategories, and a subcategory may have a standard category as its "
  parent";
- A category should have a unique name at least within its "ramification" (for a specific family's category tree).

Therefore, in this US the FA has to be able to add a customized category to the list of categories related to the family
to which he/she is associated through the email he/she has used to authenticate in the system.

## 1.2 Connection with other User Stories

A family's category list derives from the standard category tree provided by SWS. Therefore, this US is dependent
on [US001](../../sprint1/user_stories/US001_AddStandardCategory.md), in which the SM is able to create a standard
category. It is also dependent on [US010](../../sprint1/user_stories/US010_CreateFamily.md), where a SM creates a
family, therefore initializing the family's list of categories with the standard categories known by FFM APP.

This US contains the method getFamilyByAdminMainEmail(mainEmail), which is also used in other US that require the
validation of the FA ([US101](../../sprint1/user_stories/US101_AddFamilyMember.md)
, [US104](../../sprint1/user_stories/US104_ListOfMembersRelations.md)
, [US105](../../sprint1/user_stories/US105_CreateNewRelation.md)
and [US120](../../sprint1/user_stories/US120_CreateFamilyCashAccount.md)).

Finally, this US is closely related
to [US110](../../sprint1/user_stories/US110_GetListOfCategoriesOnFamilyCategoryTree.md), where a FA is able to retrieve
the list of categories on his/her family's category tree. If the present US is successful, then
in [US110](../../sprint1/user_stories/US110_GetListOfCategoriesOnFamilyCategoryTree.md) the FA will also be successful.

# 2. Analysis

The requirements that were stated and analysed in the previous section have made it clear that some kind of validation
needs to take place in the designed solution. This validation will have to verify whether the user that accesses SWS is
the FA of the family he/she wishes to retrieve the category list from.

Considering that a user may be the family FA of more than one family, the solution will also have to find the family
that the FA wishes to analyze. Once such family is identified, the system will only have to retrieve the list of
categories of the family's category tree. A family's category tree may contain one or more categories. On the other
hand, a certain category may appear in many family category trees.

## 2.1 Relevant excerpt of the Domain Model

```plantuml
@startuml
object "Family" as F
F : id
F : name
object "Category" as C
C : id
C : name
object "Person" as P
P : name
P : identification
P : vat
P : address
P : phoneNumberList
P : emailAddressList

C "*"--"*" F : used by
C "*"-"1" C : child of 
F "*" *-  "1" P : administered by
F "*" *- "*" P : has members
@enduml
```

The diagram states that a category has an id and a name that is its designation. The association "is child of" means
that a category may be a subcategory, i.e. that it may have a parent category (e.g.: "
Dentist" can be a subcategory of a category named "Health"). This also means that a category may not have a parent
category, whether it's a standard or a customized category.

A family is characterized by an id and a name.

According to the PO, a FA creates a customized category that may be at the "root level" or below some other category (
standard or customized).

## 2.2 Attributes

For a customized category or subcategory, the following parameters have to be inserted by the FA:

| **_Attributes_**            | **_Business Rules_**  |
| :-------------------------- | :---------------------|
| **
MainEmail** | Mandatory, String type, refers to the main email of the FA which will be used to identify his/her family.|
| **Name** | Mandatory, String type, cannot be empty or null, refers to the new category name.|
| **
ParentCategory** | Mandatory (for subcategories), CategoryDTO type, the system then will identify the Category object from this CategoryDTO.|

This will then enable the creation of a new category with the following attributes:

| **_Attributes_**            | **_Business Rules_** |
| :-------------------------- | :------------------------ |
| **ID** | Mandatory, String type, unique for each category. The system generates automatically.|
| **
Name** | Mandatory, String type, cannot be empty or null and has to be unique within the same ramification. Also a category/subcategory pair cannot have the same name.|
| **ParentCategory** | Mandatory (for subcategories), Category type.|
| **
Family** | Mandatory (for customized categories), refers to the family that owns the customized category/subcategory. |

## 2.3 System Sequence Diagrams

In these system sequence diagrams we go into more detail with the two possible cases, add a customized category or a
customized subcategory.

```plantuml
autonumber
title SSD Add a Category to the Family's Category Tree
actor "Family Administrator" as FA
activate FA
Participant System
activate System

FA -> System : add a customized category 
System --> FA : requests data (name, main email)
deactivate System
FA -> System : provides data (name, main email)
activate System
System --> FA : informs result
deactivate System
deactivate FA
```

```plantuml
autonumber
title SSD Add a Subcategory to the Family's Category Tree
actor "Family Administrator" as FA
activate FA
Participant System
activate System

FA -> System : add a customized subcategory 
System --> FA : shows family categories list\nand requests data (name, parent category)
deactivate System
FA -> System : provides data (name, parent category)
activate System
System --> FA : informs result
deactivate System
deactivate FA
```

## 2.4 Process View Diagram

In these process view diagrams we go into more detail with the two possible cases, add a customized category or a
customized subcategory.

```plantuml
autonumber
title Process View of Add a Category to the Family's Category Tree
actor "Family Administrator" as FA
activate FA
Participant FFM_UI
Participant FFM_BusinessLogic
activate FFM_UI

FA -> FFM_UI : add a customized category
FFM_UI --> FA : requests data (name, mainEmail)
deactivate FFM_UI
FA -> FFM_UI : inputs data (name, mainEmail)
activate FFM_UI
FFM_UI -> FFM_BusinessLogic : addCustomizedCategory\n(name, mainEmail)
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI: result
deactivate FFM_BusinessLogic
FFM_UI --> FA : informs result
deactivate FFM_UI
deactivate FA
```

```plantuml
autonumber
title Process View of Add a Subcategory to the Family's Category Tree
actor "Family Administrator" as FA
activate FA
Participant FFM_UI
Participant FFM_BusinessLogic
activate FFM_UI

FA -> FFM_UI : add a customized category
FFM_UI -> FFM_BusinessLogic : getFamilyCategoriesDTOList\n(mainEmail)
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI : familyCategoriesList
deactivate FFM_BusinessLogic
FFM_UI --> FA : shows family categories list\nand requests data
deactivate FFM_UI
FA -> FFM_UI : inputs data
activate FFM_UI
FFM_UI -> FFM_BusinessLogic : addCustomizedCategory\n(name, parentCategory)
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI: result
deactivate FFM_BusinessLogic
FFM_UI --> FA : informs result
deactivate FFM_UI
deactivate FA
```

# 3. Design

## 3.1. Class Diagram

```plantuml
class "AddCustomizedCategory\n Controller" as ACC {
- family: Family
 + AddCustomizedCategoryController(FFMApplication app)
 + boolean addCustomizedCategory(String name, String mainEmail)
 + List<CategoryDTO> getFamilyCategoriesDTOList(String mainEmail)
 + boolean addCustomizedCategory(String name, CategoryDTO parentCategory, String mainEmail)
 } 
class "FFMApplication" as APP {
 + FFMApplication()
 + FamilyService getFamilyService()
 + CategoryService getCategoryService()
}
class "FamilyService" as FS {
 + FamilyService()
 + Family getFamilyByAdminMainEmail(EmailAddress mainEmail)
}
class "CategoryService" as CS {
 + CategoryService()
 + CategoryService(List<Category> standardCategoryList)
 - boolean existsAtRootLevelInFamilyCategoryTree(String name, Family family)
 - boolean existsAtSameLevelInFamilyCategoryTree(String name, Category parentCategory, Family family
 - List<Category> getListOfCategoriesWithSameParentCategory(Category parentCategory, Family family)
 - Category getFamilyCategoryById(String id, Family family)
 - String generateCategoryId()
 - String generateSubcategoryId(Category parentCategory)
 + CategoryDTO addCustomizedCategory(String name, Family family)
 + List<CategoryDTO> getFamilyCategoryDTOList(Family family)
 - List<CategoryDTO> getChildDTOList(Category category, Family family)
 + CategoryDTO addCustomizedSubcategory(String name, CategoryDTO parent, Family family)
 - Category addCustomizedSubcategory(String name, Family family, Category parentCategory)

}
class "Family" as F {
   + Family(String name, int id, Date registrationDate)
   + Family(String name, int id)
   + Family(String name, Date registrationDate, int id)
   + Family(String name, Date registrationDate, int id, PersonList personList)
   + Family(String name, Date registrationDate, int id, PersonList personList,CashAccount cashAccount) 
   + PersonList getPersonList()
}
class "Category" as C {
  - name: String
  - id: String
  - parentCategory: Category
  - createdByFamily: Family
  + Category(String name, String id, Family family)
  + Category(String name, Category parentCategory, String id, Family family)
  + boolean hasSameName(String name)
  + String getCategoryId()
  + Category getParentCategory()
  + boolean isAtRootLevel()
  + boolean canBeUsedByFamily(Family family)
  + CategoryDTO toDTO()
  + boolean hasSameCategoryId(Category category)
  + boolean hasParentCategory(Category parentCategory)
  + boolean equals(Object o)
}
class "CategoryDTO" as CDTO {
 + id: String
 + name: Sring
 + CategoryDTO(String id, String name)

}
class "PersonList" as PL {
  + PersonList()
  + boolean isMainEmailOfAdmin(String mainEmail)
}
class "EmailAddress" as EA {
  - email: String
   + EmailAddress(String email)
   + boolean sameEmail(String anEmail)
   + boolean equals(Object o)
}

ACC "1" - "*" APP
APP "1" *-- "1" FS
APP "1" *-- "1" CS
FS "1" o-- "*" F
CS "1" o-- "*" C
F "1" *- "1" PL
PL "1" - "1" EA

@enduml
```

## 3.2. Functionality

The two macro SD in this section represent the functionality required by this US. The first refers to the creation of a
customized category at "root level", the second to the creation of a customized subcategory.

Within the first SD, the FA interacts with the UI by asking to add a customized category (1). The UI requests the
necessary data (2), which the FA then inserts (3). The UI sends this data to the AddCustomizedCategoryController (4).
First it is necessary to find the family which is associated with the email of the user. In order to do this, the
controller class needs to connect to the family service which is responsible for dealing with all families in the
system. Therefore, the AddCustomizedCategoryController asks FFM APP for the family service (5) and FFM APP retrieves
it (6). Then the retrieved family service is responsible for finding the family related to the user's email, through the
method getFamilyByAdminMainEmail(mainEmail) (7).

Once the family is found, the AddCustomizedController is able to ask FFM APP (12) for the category service that manages
all the categories in the system (standard and customized). This category service is returned (13), and the category
service is then responsible for adding a customized category using the method addCustomizedCategory (name, family) (14).
This method returns a Category that needs to be converted to a CategoryDTO that is then retrieved to the FA (steps 18 to
20).

This SD represents both the successful and scenarios of this US. If the getFamilyByAdminMainEmail(mainEmail) method is
able to retrieve a family, the next step consists in running the getCategoryService() method and adding the customized
category (step 14). If not, an error message will be transmitted to the FA through the UI (steps 8 to 10).

On the other hand, if an error is thrown and caught while trying to add a customized category, an error message will be
delivered back to the FA (steps 15 to 17).

```plantuml 
autonumber
title **US111** Add a Category to the Family's Category Tree
actor "FamilyAdministrator" as FA
participant ":UI" as UI
participant "controller\n:AddCustomizedCategory\nController" as ACC
participant "app\n:FFMApplication" as AppMembers
participant "familyService\n:FamilyService" as FS
participant "categoryService:\nCategoryService" as CS

FA -> UI : add a customized category
activate FA
activate UI
UI --> FA : requests data
deactivate UI
FA -> UI : inputs data
activate UI
UI ->  ACC : addCustomizedCategory\n(name, mainEmail)
activate ACC
ACC -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> ACC : familyService 
deactivate AppMembers
ACC -> FS : getFamilyByAdminMainEmail\n(mainEmail)
activate FS
opt family not found
FS --> ACC : error
ACC --> UI : error
UI --> FA : error message
else
FS --> ACC : family
deactivate FS
ACC -> AppMembers : getCategoryService()
activate AppMembers
AppMembers --> ACC : categoryService
deactivate AppMembers
ACC -> CS : addCustomizedCategory\n(name, family)
activate CS
ref over CS
addCustomizedCategory(name, family)
end ref
opt error is caught
CS --> ACC : error
ACC --> UI : error
UI --> FA : error message
else 
CS --> ACC : categoryDTO
deactivate CS
ACC --> UI : categoryDTO
deactivate ACC
UI --> FA : informs about category
deactivate UI
deactivate FA
end
end
```

The following is SD detailing the method addCustomizedCategory(name, family). Within the
listOfCategoriesAtRootLevelOfFamilyCategoryTree, a loop is run to check if, for each category in this list, its name is
equal to the name of the category the FA intends to insert. If the result (3) is true, then the category cannot be
created, and an error is returned (4). This means that at the root level no two categories can have the same name.

If, by the end of the loop, no other category with the same name is found, the id of the category is generated
automatically (5) and the category is created (6). Then, the newly created category is added to the family's category
tree (7) and it is created a DTO of it (steps 8 to 10). This new category DTO is returned (steps 11 and 12).

```plantuml 
autonumber
title //addCustomizedCategory(name, family)//
participant "categoryService:\nCategoryService" as CS
participant "listOfCategoriesAtRootLevelOfFamilyCategoryTree\n:List<Category>" as CL
participant "category:\nCategory" as C

[o-> CS : addCustomizedCategory\n(name, family)
activate CS
loop for each category in categoryList
CS -> C : hasSameName(name)
activate C
return result
deactivate C
opt result == true
[<-- CS : error
end
end
CS -> CS : generateCategoryId()
CS -> "newCategory:\n Category" as NC ** : create(name, id, family)
CS -> CS : add(newCategory)
CS -> NC : toDTO()
activate NC
NC -> "newCategoryDTO: \nCDTO" as NCDTO ** : create(id, name)
activate NCDTO
NCDTO --> NC : categoryDTO
deactivate NCDTO
NC --> CS : categoryDTO
deactivate NC
[<-- CS : categoryDTO
deactivate CS
```

The following is the SD designed for the case when the FA wishes to add a subcategory to his/her family's category tree.

There are two differences between this SD and the first one. First, the method getFamilyCategoriesDTOList(String
mainEmail) is used by the controller to give back to the FA a list of his/her family's list of categories in a DTO
format, so that the FA is able to choose the parent category of the subcategory that is going to be created (steps 2 to
4). The other difference is the method used for adding a customized subcategory (9). If it is successful, a categoryDTO
of the new category is created and returned to the FA (steps 13 to 15); if not, an error message is returned to the FA (
steps 10 to 12).

```plantuml 
autonumber
title **US111** Add a Subcategory to the Family's Category Tree
actor "FamilyAdministrator" as FA
participant ":UI" as UI
participant "controller\n:AddCustomizedCategory\nController" as ACC
participant "app\n:FFMApplication" as AppMembers
participant "familyService\n:FamilyService" as FS
participant "categoryService:\nCategoryService" as CS

FA -> UI : add a customized subcategory
activate FA
activate UI
UI -> ACC : getFamilyCategoriesDTOList\n(mainEmail)
activate ACC
ref over ACC
getFamilyCategoriesDTOList(mainEmail)
end ref
ACC --> UI : familyCategoriesList
deactivate ACC
UI --> FA : shows family categories list\nand requests data
deactivate UI
FA -> UI : inputs data
activate UI
UI -> ACC : addCustomizedSubcategory\n(name, parentCategory)
activate ACC
ACC -> AppMembers : getCategoryService()
activate AppMembers
AppMembers --> ACC : categoryService
deactivate AppMembers
ACC -> CS : addCustomizedSubcategory\n(name, parentCategory, family)
ref over CS
addCustomizedSubcategory(name, parentCategory, family)
end ref
opt error is caught
activate CS
CS --> ACC : error
ACC --> UI : error
UI --> FA : error message
else
CS --> ACC : categoryDTO
deactivate CS
ACC --> UI : categoryDTO
deactivate ACC
UI --> FA : informs about category
end
```

We now go into more detail with the method getFamilyCategoriesDTOList(mainEmail). First the controller tries to find the
FA's family and initializes the category service by using the same methods detailed in the SD for creating a customized
category (steps 5 to 13 in that SD).

Then, the category service searches inside its category list for categories that can be used by the family (steps 11 and
12) and adds them to a temporary list that is the familyCategoryDTOList, while converting each category to a CategoryDTO
with two parameters (id and name) (13). This list is sent back to the UI (steps 14 and 15).

```plantuml 
autonumber
title //getFamilyCategoriesDTOList(mainEmail)//
participant "controller\n:AddCustomizedCategory\nController" as ACC
participant "app\n:FFMApplication" as AppMembers
participant "familyService\n:FamilyService" as FS
participant "categoryService:\nCategoryService" as CS
participant "categoryList:\nList<Category>" as CL
participant "familyCategoryDTOList:\nList<CategoryDTO>" as FCL
participant "category:\nCategory" as C

[o-> ACC : getFamilyCategoriesDTOList\n(mainEmail)
activate ACC
ACC -> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> ACC : familyService 
deactivate AppMembers
ACC -> FS : getFamilyByAdminMainEmail\n(mainEmail)
activate FS
opt family not found
FS --> ACC : error
[<-- ACC : error
else
FS --> ACC : family
deactivate FS
ACC -> AppMembers : getCategoryService()
activate AppMembers
AppMembers --> ACC : categoryService
deactivate AppMembers
ACC -> CS : getFamilyCategoryDTOList(family)
activate CS
loop for each category in categoryList
CS -> C : canBeUsedByFamily(family)
activate C
C --> CS : result
deactivate C
opt result == true
CS -> FCL ** : add(category.toDTO())
end
end
CS --> ACC : familyCategoryDTOList
deactivate CS
[<-- ACC : familyCategoryDTOList
deactivate ACC
end

```

Now we go into more detail with method addCustomizedSubcategory.

The first step consists in finding the parentCategory object of the category the FA is trying to create by providing the
parentCategoryId. The parentCategory object is used to create the new category. In case the category is not found, an
error message is delivered back to the FA (2).

If the category is found, then the method addCustomizedSubcategory(name, family, parentCategory) is called. The
parentCategory is the object found by the getCategoryById method.

In case an error is caught, it is not possible to add the subcategory and an error is sent back (3). Otherwise, the
created category is converted to a DTO (steps 4 and 5) and sent back to the UI (steps 6 to 8).

```plantuml 
autonumber
title //addCustomizedSubcategory(name, parentCategory, family)//
participant "categoryService:\nCategoryService" as CS
participant "category:\nCategory" as C

[o-> CS : addCustomizedCategory\n(name, parentCategory, family)
activate CS
ref over CS
getCategoryById(parentCategory.getCategoryId())
end ref
opt category not found
[<-- CS : error
else
ref over CS
addCustomizedSubcategory(name, family, parentCategory)
end ref
end
opt not possible to add subcategory
[<-- CS : error
else 
CS -> C : toDTO()
activate C
C -> "newCategoryDTO:\nCategoryDTO" as NCDTO ** : create(id, name)
activate NCDTO
NCDTO --> C : categoryDTO
deactivate NCDTO
C --> CS : categoryDTO
deactivate C
[<-- CS : categoryDTO
end
deactivate CS
```

A SD describing the method getCategoryById(parentCategory.getCategoryId()) is detailed below. A loop is executed in
which every category in the family's category list is accessed in order to find its id (2). If the id is equal to the
parentCategoryId that is being searched, the corresponding category is returned (steps 3 and 4). If, by the end of the
loop, no matching id was found, an error is returned (5).

```plantuml 
autonumber
title //getCategoryById(parentCategory.getCategoryId())//
participant "categoryService:\nCategoryService" as CS
participant "categoryList:\nList<Category>" as CL
participant "category:\nCategory" as C

[o-> CS : getCategoryById(parentCategory.getCategoryId())
activate CS
loop for each category in categoryList
CS -> C : getCategoryId()
activate C
C --> CS : id

opt id == parentCategoryId
C --> CS : category
deactivate C
[<-- CS : category
end
end
[<-- CS : error
deactivate CS
```

The following SD details the method addCustomizedSubcategory(name, parentCategory, family). Comparing to the method
addCustomizedCategory, this the loop cycles through the list listOfCategoriesWithSameParentCategoryOfFamilyCategoryTree,
since it's possible for categories to have the same name as long as they don't share the same parent category.

Step 6 is also different because an additional parameter is included (the parentCategory).

```plantuml 
autonumber
title //addCustomizedSubcategory(name, family, parentCategory)//
participant "categoryService:\nCategoryService" as CS
participant "listOfCategoriesWithSameParentCategoryOfFamilyCategoryTree\n:List<Category>" as CL
participant "category:\nCategory" as C

[o-> CS : addCustomizedCategory\n(name, family, parentCategory)
activate CS
loop for each category in categoryList
CS -> C : hasSameName(name)
activate C
return result
deactivate C
opt result == true
[<-- CS : error
end
end
CS -> CS : generateSubcategoryId()
CS -> "newCategory:\n Category" ** : create(name, parentCategory, id, family)
CS -> CS : add(newCategory)
[<-- CS : success
deactivate CS
```

## 3.3. Applied Patterns

Different software development patterns were applied in the analysis and design steps of this US:

**Pure Fabrication:** the FFMApplication class resulted from this principle, as well as the service classes (
CategoryService and FamilyService), as they do not represent a concept of the problem domain.

**Controller:** the AddCustomizedCategoryController class follows this design pattern, in which an intermediate entity
establishes a connection between the UI and the internal processes.

**Creator:** this pattern means that a class should have the responsibility of creating objects if it's also responsible
for aggregating them. This happens in CategoryService, which has a list of all categories as an attribute (of all kinds
- customized, standard, categories and subcategories), therefore it's also the one who creates category objects.

**Information Expert:** by stating that certain classes should have a certain responsibility since they hold the
information required to do it. In this US, this is achieved with CategoryService, as it knows the information about all
the categories in SWS, it's the one responsible for creating categories for any family, and also for retrieving the list
of all categories in a family's category tree when the FA wishes to select a parent category of the subcategory he/she
intends to create.

## 3.4. Tests

Different tests were devised for the classes involved in this US (AddCustomizedCategoryController, CategoryService,
Category). In AddCustomizedCategoryController, the following tests were made in order to test a success and failure
scenario for each type of customized category (category or subcategory). The other tests were to assess whether it was
possible to create the controller with different states of the application, if a member who is not the FA can add a
customized category, and both the success and failure cases for retrieving the family's list of category DTO.

**Test 1** : Test fails because the services in the app are null.

**Test 2** : Test fails because app is null.

**Test 3** : Test succeeds because app is not null.

**Test 4** : Add a customized category - success.

**Test 5** : Add a customized category - failure.

**Test 6** : Add a customized subcategory - success.

**Test 7** : Add a customized subcategory - failure.

**Test 8** : Impossible to add a customized category because the user is not a FA.

**Test 9** : Check that it is possible to successfully retrieve an array list of category DTO of a family.

**Test 10** : Check that an empty array list of family category DTO is retrieved when an error is caught.

In CategoryService, the many possible causes for throwing errors were tested, and for each type of category. Two new
public methods created for this US were also tested in CategoryService (getFamilyCategoryById and
getFamilyCategoryDTOList).

**Test 11** : Add a customized category - success.

**Test 12** : Add a customized category - failure because category already exists at root level.

**Test 13** : Add a customized category - failure because category name is invalid.

**Test 14** : Add customized subcategory in which parent category is standard - success.

**Test 15** : Add customized category in which parent category is customized - success.

**Test 16** : Add customized category in which parent category is a customized subcategory- success.

**Test 17** : Add a customized subcategory with a name that already exists at the same level.

**Test 18** : Add a customized subcategory with a name that already exists at the same level (parent category is a
subcategory).

**Test 19** : Check that it is possible to successfully retrieve an array list of category DTO of a family.

**Test 20** : Check that an empty array list of family category DTO is retrieved when an error is caught.

**Test 21** : Check if it is possible to return a category in a family's category tree by its id, whether it's a
standard or a customized category/subcategory.

**Test 22** : Check if errors are thrown when trying to return a non-existing category in a family's category tree by
its id, whether it's a standard or a customized category/subcategory.

In class Category, the tests are related to methods already used, tested and documented in US110.

The methods related to the validation of a FA and finding his/her family using the main email were already tested and
documented in many US of the previous sprint (check, for instance, the documentation of US110).


# 4. Implementation

In order to implement this US it was first necessary to restructure how our application stores standard and customized
categories, according to feedback received in the sprint 1 review. These changes can be summarized below:

- Each category has a parent category that is a category object instead of a string that refers to the parent category
  id;
- Each category has a family object associated with it, which is null in case the category is standard;
- The category service is used to store and manage all categories, standard or from each family;
- The family no longer has its own category service;
- DTO will be used to transfer information about categories between layers.

After this restructure, it was relatively straightforward to implement the code for this US. In the controller the
following methods were implemented:

    public boolean addCustomizedCategory(String name, String mainEmail) {

        try {
            FamilyService familyService = this.app.getFamilyService();
            Family familyF = familyService.getFamilyByAdminMainEmail(mainEmail);
            CategoryService categoryService = this.app.getCategoryService();
            categoryService.addCustomizedCategory(name, familyF);
            return true;
        } catch (IllegalArgumentException exception) {
            return false;
        }

    }

    public List<CategoryDTO> getFamilyCategoriesDTOList(String mainEmail) {

        try {
            FamilyService familyService = this.app.getFamilyService();
            this.family = familyService.getFamilyByAdminMainEmail(mainEmail);
            CategoryService categoryService = this.app.getCategoryService();
            return categoryService.getFamilyCategoryDTOList(family);
        } catch (IllegalArgumentException exception) {
            return Collections.emptyList();
        }

    }

    public boolean addCustomizedSubcategory(String name, CategoryDTO parentCategory) {

        try {
            CategoryService categoryService = this.app.getCategoryService();
            categoryService.addCustomizedSubcategory(name, parentCategory, this.family);
            return true;
        } catch (IllegalArgumentException exception) {
            return false;
        }

    }

# 5. Integration/Demonstration

As was explained in the previous section, in order to implement this US we first had to restructure other classes
related to this US, and which had been developed for [US001](../../sprint1/user_stories/US001_AddStandardCategory.md) in
Sprint 1.

For instance, the method addStandardSubcategory in AddStandardCategoryController had to be changed in order to accept a
CategoryDTO as the parent category of a subcategory:

    public boolean addStandardSubcategory(String name, CategoryDTO parentCategory) {
        CategoryService categoryService = this.app.getCategoryService();

        try {
            categoryService.addStandardSubcategory(name, parentCategory);
            return true;
        } catch (IllegalArgumentException exception) {
            return false;
        }

    }

Also, the method canBeUsedByFamily in CategoryService which was developed for this US is now also used by methods
getFamilyCategoryListAsString and getChildList in CategoryService, related
to [US110](../../sprint1/user_stories/US110_GetListOfCategoriesOnFamilyCategoryTree.md).

    public String getFamilyCategoryListAsString(Family family) {
        if (this.getCategoriesAsArrayList().isEmpty()) {
            throw new IllegalStateException("The family's category list is empty.");
        }
        String familyCategoryList = "";

        for (Category category : this.categoryList) {
            if (category.getParentCategory() == null && (category.canBeUsedByFamily(family))) {
                familyCategoryList += category.toString();
                familyCategoryList += " " + getChildList(category, family);
            }
        }

        return familyCategoryList;
    }

    private String getChildList(Category category, Family family) {
        String childList = "";
        if (!hasChildren(category))
            return childList;

        for (Category cat : this.categoryList) {
            if (cat.getParentCategory() != null && cat.canBeUsedByFamily(family) && cat.getParentCategory().getCategoryId().equals(category.getCategoryId())) {
                childList += cat.toString() + " " + getChildList(cat, family);
            }
        }

        return childList;
    }

# 6. Observations

This US involved some brainstorm work between the members of the team as we had to figure out who should have the
responsibility of guarding and managing the family's categories.

It was also discussed how the FA should indicate the parent category of the subcategory he/she wants to create. It was
decided the best approach was to provide the FA's a list of the family's categories and that then he/she should choose
the parent category from that list, which is then inserted as a category DTO in the method of the controller.

In future US and sprints it may be necessary to improve this step of the process in order to integrate it with the UI.

Also, in future sprints the method getFamilyCategoriesDTOList in this US controller will have to be improved so that the
items in the DTO list are presented in an ordered way.

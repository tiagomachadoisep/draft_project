# US130
=======================================


# 1. Requirements

*As a family administrator, I want to transfer money from family's cash account to another family member's cash account.*

In this US we have developed a feature so that the FA can transfer amounts from the family's cash account to the cash account of a member of their family.
From the data inserts as the transaction date, description, amount to be transferred and email of the person who will receive the money, the system will quite simply and quickly perform the operation.

##1.1. Client notes

A PO observation was that a brief description of the transaction should be included in the transaction.

##1.2. System Sequence Diagram

Next is the system sequence diagram realized for this US.

```plantuml
title System Sequence Diagram 
actor "Family Administrator" as FA
activate FA

Participant System
FA -> System : transfer money
System --> FA : asks data
|||
FA-> System : input data
System ->FA: result
deactivate FA
```
##1.3.Process View

```plantuml
title Process View
actor "Family Administrator" as FA
activate FA

Participant UI
Participant "FFM_BusinessLogic" as BL 
FA -> UI : tranfer money
activate UI
UI--> FA : ask the data
deactivate UI
FA -> UI : input data
activate UI
UI -> BL : transferMoney(date, description, value,memberMainEmail)
activate BL
BL --> UI : result
deactivate BL
UI-> FA : shows result
deactivate UI
deactivate FA

```
##1.4. Connection with other User Stories

In relation to this Sprint, this US is only linked to [US170](/docs/sprint2/user_stories/US170_CreatePersonalCashAccount.md), which creates a member's cash account.
Before this US can perform its functionality, it is necessary that the member's account has been created. After a transfer between cash accounts is created, 
there is a connection with [US181](/docs/sprint2/user_stories/US181_RegisterMemberPaymentUsingCashAccount.md) then it creates the transaction is created and recorded by the same.

# 2. Analysis

## 2.1. Attributes
| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **Date**                    | Mandatory, Date, date of the transaction               |
| **Description**             | Mandatory, String, small transaction's description             |
| **Value**                   | Mandatory, double, positive number with two decimal numbers, not null               |
| **Member Main Email**       | Mandatory, String, not null               |


## 2.2. Relevant excerpt of the Domain Model

The following diagram contains an excerpt of the Domain Model that illustrates the concepts related to this US.

```plantuml
@startuml
object Person{
CashAccount 
}
object CashAccount{
balance
}
Person "1" *-- "0..1" CashAccount : has >
@enduml
```

# 3. Design

## 3.1. Functionality

In the sequence diagram below we detail how the application was built.

The first step is when the FA asks the UI for the desire to transfer a cash amount from the family account to another family member. The UI therefore requests the data so that the operation can take place in the system. They are: date of the transaction, brief description, amount you want to transfer and the email of the member who will receive the money.

Then the information goes to our TransferMoneyController which will search for the necessary information for each class responsible for each feature.

In the Application, the controller searches for the FamilyService, which returns the service class of the families. From the family services, we use the getFamilybyMemberMainEmail method to extract the family.

Knowing which family, the controller goes to that family to get their cash account and its members. From the list of members, take the person who will receive the cash amount and then select your cash account.

The Cash account validates the data, transfers the amount and records the transaction. If everything is correct, it sends a message of the transaction completed.

```puml
@startuml

autonumber
title US130 - Transfer money from family's cash account \nto member's account
actor "Family Administrator" as FA
participant ":UI" as UI
participant ":TransferMoneyController" as C
participant ":FFMApplication" as AppMembers
participant ":FamilyService" as FS
participant ":Family" as F
participant ":PersonList" as PL
participant ":Person" as P
participant ":AccountList" as AL
participant ":CashAccount" as CA
FA -> UI : Transfer money
activate FA
activate UI
UI -> FA : ask data
FA -> UI : input data\n (date, description, value, memberEmail)
UI -> C : transferMoney\n(date, description, value,memberMainEmail)

activate C
C-> AppMembers : getFamilyService()
activate AppMembers
AppMembers --> C: familyService
deactivate AppMembers
|||
C->FS : getFamilyByAdminMainEmail\n(adminMainEmail)
activate FS
FS-->C : family
deactivate FS
|||
C->F :getCashAccount()
activate F
F--> C: familyCashAccount
deactivate F
C -> F : getPersonList()
activate F

F--> C : memberList
deactivate F
|||
C ->PL : getPersonByMainEmail(memberMainEmail)
activate PL
PL-->C : person
|||
deactivate PL
C -> P : getAccountList()
activate P
P --> C : accountList
deactivate P
|||
C->AL : getCashAccount()
activate AL
AL--> C : personCashAccount
deactivate AL
|||
C-> CA : transferMoney(date, description, familyCashAccount, value, memberCashAccount)
activate CA
ref over CA : transferMoney(date, description,familyCashAccount, value, memberCashAccount)
CA--> C :result
deactivate CA
C-->UI : result
deactivate C
UI-->FA : result
deactivate UI
deactivate FA
@enduml
```
Going more detailed in transferMoney method:

```plantuml
hide footbox
autonumber
title transferMoney(date, description, familyCashAccount,\nvalue, memberCashAccount)

Participant "familyCashAccount :\nCashAccount" as CA
Participant "transaction : Transaction" as T
Participant "moneyValue : MoneyValue" as MV
Participant "memberCashAccount : \nCashAccount" as MCA
|||
[o->CA : transferMoneyCashAccount(date, description,\nfamilyCashAccount, value, memberCashAccount)
|||
alt result == false
activate CA
CA -> MV ** : create(value)
CA -> CA : result = validateTransfer(value)
[<--o CA : failure
else
CA -> T ** : create (date, description,\n familyCashAccount,\nvalue, memberCashAccount)

CA -> CA : removeFunds(value)

CA -> CA : addToTransactionList(transaction)

CA -> MCA : addFunds(value)

CA->MCA : addToTransactionList(transaction)


[<--o CA : sucess
deactivate CA
end

```
## 3.2. Class Diagram

In this US four classes interact with each other:

- TransferMoneyController
- FFMApplication
- FamilyService
- Family  
- PersonList
- Person
- AccountList
- CashAccount
- CashTransaction

The following is a class diagram related to this US.

```plantuml
@startuml

class "TransferMoney \n Controller" as C {
 - app: FFMApplication
 - TranferMoneycontroller(FFMApplication app)
 - tranferMoney(date, description,adminMainEmail,\nvalue, personCashAccount) : boolean 
 } 
class "FFMApplication" as AppMembers {
- familyService : FamilyService
- FFMAplication(FamilyService familySevice)
- getFamilyService()
}
class "FamilyService" as FS {
- families : List<Family> 
- getFamilyByAdminMainEmail(adminMainEmail)
}
class "Family" as F{
- cashAccount : CashAccount
- memberlist : PersonList
- getPersonList() :PersonList
- getCashAccount() :CashAccount
}
class "PersonList" as PL{
- members : PersonList
- getMemberByMainEmail(memberMainEmail) : boolean
}
class "Person" as P{
- accountList : AccountList
- getCashList() : boolean
}
class "AccountList" as AL{
- cashAccount : CashAccount
- getCashAccount() : boolean
}
class "CashAccount" as CA{
- moneyBalance: MoneyValue
- description :String
-transactions : List<Transaction> 
- validateTransfer(value)
- removeFund(value) : void
- addFunds(value) : void
- transferMoney(familyCashAccount, value, memberCashAccount) : boolean
- validateTransaction() : boolean
}
interface "Account" as IA{
+ isDescription(String description) : boolean 
+ getBalance() : double 
+ addFunds() : void 
+ removeFunds() :void 
+ isCash() : boolean 
+ getListOfTransactionsBetweenDates\n(Date dateOne, Date dateTwo) : List<Transaction> 
}

C -- AppMembers : asks >
AppMembers - FS : has >
FS - F : has >
F - PL : has >
PL - P : has >
P -- AL : has >
AL -- CA : has >
CA -|> IA: implements >

@enduml
```

## 3.3. Applied Patterns

In this US we can find the patterns below.

**Controller** - We have the "TransferMoneyController" following this pattern. It is responsible to receive and coordenate
the events/operations of the system.

**Information Expert** - This pattern is present into the class "FFMApplication", this class knows what classes has responsibility for something, then delegates responsibilities.

**Pure Fabrication** - We can identify this concept, for example, in the service class called FamilyService. This class was "invented" to support high cohesion, low coupling and reuse.

**Low Coupling and High Cohesion** - We can find these patterns throughout the application. We try to delegate the responsibilities in order not to concentrate on a single class to increase Cohesion. We also try as much as possible to create methods so that the code can be reused to maintain a low coupling.

## 3.4. Tests

Below we find the main tests that allow a correct assessment of compliance with the requirements.

**Test 1:** Transfer money from family's cash account to a member's cash account 

**Test 2:** Try to transfer a negative value   

**Test 3:** Try to transfer with not enough balance   

**Test 4:** Try to do a transaction with value = 0 

# 4. Implementation

There was not much complexity to realize this functionality. It was necessary that the family member's account was created and thus it was possible to the FA transfer money from family's cash account.

# 5. Integration/Demonstration

This functionality was carried out in conjunction with the [US180](/docs/sprint2/user_stories/US180_TransferMoneyFromCashAccountToCashAccount.md), the same Controller and method were used to carry out the transfer between cash accounts.
This US also had integration with the [US170](/docs/sprint2/user_stories/US170_CreatePersonalCashAccount.md), so that in order for money to be transferred to a family member's cash account, the person's cash account should have been created.
There was also integration with [US181](/docs/sprint2/user_stories/US181_RegisterMemberPaymentUsingCashAccount.md), where when carrying out the transfer operation, we also registered this operation.

Below we can see a method that exemplifies this last integration mentioned:

    public boolean transferMoneyFromCashAccount(MoneyValue value, String description, CashAccount cashAccount,
        CashAccount otherCashAccount, Date transferDate) {
        [...]
        MoneyValue newValue = cashAccount.getMoneyBalance().subtraction(value);
        setMoneyValue(newValue);

        MoneyValue newValue2 = otherCashAccount.getMoneyBalance().sum(value);
        otherCashAccount.setMoneyValue(newValue2);

        Transaction cashTransaction = new CashTransfer(cashAccount, otherCashAccount, value, transferDate,
                description);
        cashAccount.transactionList.add(cashTransaction);
        otherCashAccount.transactionList.add(cashTransaction);
        return stateOfTransfer;
    }


# 6. Observations

Overall we managed to get the expected for this US. There are no relavant comments to do.
# US185
=======================================


# 1. Requirements

*As a Family Member, I want to check the balance of one of my accounts.*

A FM can have several accounts of multiple kinds like, bank account, bank savings, credit card and cash account.
Since we're not dealing with financial entity yet, we start a bank account with balance value of zero. 
The user when adds a new account he/she only can introduce the account's description.

## 1.1 Client notes

The information obtained from PO indicates the better way to introduced the balance on SWS is through a financial entity.
So, when the account is added by an actor (FA or FM), the balance should be zero, except for cash account.


# 2. Analysis

## 2.1. Relevant excerpt of the Domain Model

```plantuml
title Excerpt of Domain Model

object "Family" as fam {
id
name
}


object Person {
name
identification
vat
address
phoneNumberList
emailAddressList
}


object "Cash \nAccount" as CA {
balance
description
}

object "Bank \nAccount" as BA{
id
balance
description
}

object "Credit Card \nAccount" as CCA{
id
balance
description
}

object "Bank Savings \nAccount" as BSA{
id
balance
description
}


fam "*" *-left- "*" Person : has members
fam "*" *-  "1" Person : administered by


Person "1" -- "0..1" CA : has
Person "1" -- "0..*" BA : has
Person "1" -- "0..*" BSA : has
Person "1" -- "0..*" CCA : has

```

## 2.2 Attributes

To check the balance of a selected account, we need the member's e-mail to identify the family and member
and the account's description to search in account's list and then retrieve the respective balance.

## 2.3 System Sequence Diagram

```plantuml
title SSD Check Account Balance
actor "Family Member" as SM
Participant System
SM -> System : get account balance
activate SM
activate System
System --> SM : which account
SM -> System : chooses bank/cash/savings
System --> SM : balance
deactivate System
deactivate SM
```

## 2.4. Process View Diagram

```plantuml
title Process View of Check Account Balance
actor "Family Member" as SM
Participant FFM_UI
Participant FFM_BusinessLogic
SM -> FFM_UI : get account balance
activate SM
activate FFM_UI
FFM_UI -> FFM_BusinessLogic : getAllAccountsDescriptions()
activate FFM_BusinessLogic
FFM_BusinessLogic --> FFM_UI : accounts description
FFM_UI --> SM : shows accounts 
SM -> FFM_UI : chooses account
FFM_UI -> FFM_BusinessLogic : getAccountBalance(description, mainEmail)
FFM_BusinessLogic --> FFM_UI : balance
deactivate FFM_BusinessLogic
FFM_UI --> SM : balance
deactivate FFM_UI
deactivate SM
```
# 3. Design

## 3.1. Class Diagram

The following class diagram shows the relationship between the classes involved in this US.


```plantuml

class "Family" as F{
- name : String
- id : int
+ Family()
+ getPersonList()

}

class "FamilyService" as FS {
+ FamilyService()
+ getFamilyByMemberMainEmail(mainEmail)
}

class "CheckAccountsBalanceController" as ctrl {
+ CheckAccountsBalanceController()
+ getAllAccountsDescription(mainEmail)
+ checkAccountsBalance(description)
}

class "FFMApplication" as APP {
+ FFMApplication()
+ getFamilyService()
}

interface "Account" as A {
+ getDescription()
+ getBalance()
}

class "AccountList" as AL {
+ AccountList()
+ getAllAccountsDescription()
+ getAccountByDescription(description)

}

class "PersonList" as PL {
+ PersonList()
+ getPersonByMainEmail(mainEmail)
}

class "Person" as P{
- name : String
- email : EmailAddress
+ Person()
+ getAccountList()

}

class "AccountDTO" as dto {
- description : String
+ setDescription(description)
+ equals()

}

ctrl "1" -- "1" APP : asks 
APP - FS : gets 
FS "1"--"1..*" F : gets 
F "1"*-"1" PL : has 
PL "1"*--"1..*" P : contains 
P "1"*-"1" AL : has an 
AL "1"-"0..*" A : gets balance 
ctrl - dto : returns information converted to 
  
```
## 3.2. Functionality

The analysis of this US is well represented on the following SD.
We expect that the system shows the information about the accounts existent on the account list of the user through
a DTO.
After the user chooses the account, the system will retrieve the account's balance.

There are some possible occurrences we predicted on building this US. If the user inputs a wrong account
description, the system will return an empty DTO.

Also, when the account's description is wrong received by the Controller, the system cannot find the account and answers with a false
value, which means there is no balance.

```plantuml
autonumber
title US185 - Check Account's Balance
actor "Family Member" as FM
participant ":UI" as ui
participant ":CheckAccountBalanceController" as ctrl
participant ":FFMApplication" as app
participant ":FamilyService" as fs
participant ":Family" as f
participant ":PersonList" as pl
participant ":Person" as p
participant ":AccountList" as la

activate FM
FM -> ui : get my account balance
activate ui
ui -> ctrl : getAllAccountsDescription(mainEmail)
activate ctrl
ctrl -> app : getFamilyService()
activate app
app --> ctrl : familyService
deactivate app
ctrl -> fs : getFamilyByMemberMainEmail(mainEmail)
activate fs
fs --> ctrl : family
deactivate fs
ctrl -> f : getPersonList()
activate f
f --> ctrl : membersList
deactivate f
ctrl -> pl : getPersonByMainEmail(mainEmail)
activate pl
pl --> ctrl : person
deactivate pl
ctrl -> p : getAccountList()
activate p
p --> ctrl : accountList
deactivate p
ctrl -> la : getAllAccountsDescription()
activate la
ref over la : getAllAccountsDescription()
la --> ctrl : dtoList
deactivate la
ctrl --> ui: dtoList
deactivate ctrl
ui --> FM : which account \n(shows descriptions)
deactivate ui
deactivate FM
@enduml
```
After FM requesting to see the balance account, the UI needs to show the description of each account owned by FM so that he/she can choose which one wants to check.
In order to obtain it, the system needs to take several steps (from 3 to 14) until it reaches the AccountList class which has all user's accounts.

Going deeper on getAllAccountsDescription() method

```plantuml
autonumber
title getAllAccountsDescription()
participant ":AccountList" as la
participant ":Account" as acc
[o-> la : getAllAccountsDescription()
activate la
la --> "dtoList : List<AccountDTO>" ** : create() 
loop for each account in accountList
la --> "aDto : AccountDTO" ** : create()
la -> acc : getDescription()
activate acc
acc --> la : description
activate "aDto : AccountDTO"
la -> "aDto : AccountDTO" : setDescription(description)
deactivate "aDto : AccountDTO"
la -> "dtoList : List<AccountDTO>" : add(aDto)
activate "dtoList : List<AccountDTO>"
end loop
deactivate "dtoList : List<AccountDTO>"
deactivate acc
[<-- la : dtoList
deactivate la
@enduml
```
All descriptions are saved on a temporary DTO List to show to the user.


When user chooses the account, the following steps taken by the system are represented on the next SD.

```plantuml
autonumber
title US185 - Check Account's Balance
actor "Family Member" as FM
participant ":UI" as ui
participant ":CheckAccountBalanceController" as ctrl
participant ":AccountList" as la
participant ":Account" as acc
FM -> ui : bank/cash/savings
activate FM
activate ui
ui -> ctrl : checkAccountsBalance(description)
activate ctrl
ctrl -> la : getAccountBalanceByDescription(description)
activate la
loop for account in accountList
la -> acc : hasSameDescription(description)
activate acc
acc --> la : account
end loop
la --> ctrl : account
deactivate la
ctrl -> acc : getBalance()
acc --> ctrl : balance
deactivate acc
ctrl --> ui : balance
deactivate ctrl
ui --> FM : balance
deactivate ui
deactivate FM
@enduml
```
Since the system already knows the FM AccountList, it only needs the description that FM has chosen. So the controller asks directly to AccountList
about the balance of the selected account.
Finally, the account returns the balance to FM.

## 3.3. Applied Patterns

In this User Story we can find the patterns below:

**Controller** - A class *CheckAccountsBalanceController* 
was created and, its only purpose is to build a "bridge" between the UI and the AppMembers.

**Information Expert** - Each Person in our app has a list of accounts, responsible for saving every person account. It also
has the responsibility to manage and create new accounts. The FamilyService happens the same, since it deals with every *service* related to family.

**Low Coupling** - Creating an interface Account lows our class bank account coupling.

**High Cohesion** - AccountList was created to save all accounts, and to deal with accounts operations, leaving fewer responsibilities
to accounts classes.


## 3.4. Tests

In this section some tests will be presented that guarantee the requisites
of the US are applied.

A BeforeAll was created to all tests of AddBankAccountController:

    private static FFMApplication app;
    private static String emailLuis;
    private static String emailMarta;
    private static String bankAccountDescription;
    private static String creditCardAccountDescription;

    @BeforeAll
    static void createsFamilyWithMembers() {
        app = new FFMApplication();
        FamilyService appFamilyService = app.getFamilyService();
        Family family = new Family("Rodrigues", new Date(), 20);
        appFamilyService.addFamilyIfNotExists(family);
        PersonList personList = family.getPersonList();


        emailLuis = "luis@isep.ipp.pt";
        String emailMargarida = "margarida@isep.ipp.pt";
        emailMarta = "marta@isep.ipp.pt";
        Person personLuis = new Person("Luis", "19913200 3ZY1", 0, new Date(), emailLuis, null, "male");
        Person personMargarida = new Person("Margarida", "19741658 6ZV2", 0, new Date(), emailMargarida, null, "female");
        Person personMarta = new Person("Marta", "18480344 6ZY2", 0, new Date(), emailMarta, null, "female");
        personList.addMember(personLuis);
        personList.addMember(personMargarida);
        personList.addMember(personMarta);

        AccountList listOfAccounts = personLuis.getPersonAccountList();
        bankAccountDescription = "Conta BCP";
        creditCardAccountDescription = "Revolut";
        BankAccount bankAccount = new BankAccount(bankAccountDescription,1);
        CreditCardAccount creditCardAccount = new CreditCardAccount(creditCardAccountDescription,1);
        listOfAccounts.addAccount(bankAccount);
        listOfAccounts.addAccount(creditCardAccount);
       

    }
**Test 1** : Success returning all accounts description.

    @Test
    @DisplayName("Getting all descriptions of account list")
    public void successReturningAllAccountsDescription() {
        //Arrange
        CheckAccountBalanceController balanceCtrl = new CheckAccountBalanceController(app);
        List<AccountDTO> expected = new ArrayList<>();
        AccountDTO aDTO = new AccountDTO(bankAccountDescription);
        AccountDTO otherDto = new AccountDTO(creditCardAccountDescription);
        expected.add(aDTO);
        expected.add(otherDto);

        //Act
        List<AccountDTO> result = balanceCtrl.getAllAccountsDescription("luis@isep.ipp.pt");

        //Assert
        assertEquals(expected, result);

    }

**Test 2** : Member doesn't have an account. Returns an empty string.

**Test 3** : Gets the balance of bank account, having only that account.

**Test 4** : Gets the balance of credit card account, having three accounts on list.

    @Test
    @DisplayName("gets the balance of credit card account.")
    public void balanceOfCreditCardAccount() {
       //Arrange
        AccountList accountList = new AccountList();
        String bankAccountDescription = "Crédito Agricola";
        String creditCardAccountDescription = "Revolut";
        String savingsAccountDescription = "Saving for house";
        Account newBankAccount = new BankAccount(bankAccountDescription,1);
        Account newCreditCardAccount = new CreditCardAccount(creditCardAccountDescription, 100.80);
        Account bankSavingsAccount = new BankSavingsAccount(savingsAccountDescription,1);
        accountList.addAccount(newBankAccount);
        accountList.addAccount(newCreditCardAccount);
        accountList.addAccount(bankSavingsAccount);

        CheckAccountBalanceController balanceCtrl = new CheckAccountBalanceController(app,accountList);
        double expected = 100.80;

        //Act
        Result<Double> result = balanceCtrl.checkAccountsBalance(creditCardAccountDescription);
        Double resultBalance = result.getContent();
        //Assert
        assertEquals(expected, resultBalance);
        assertTrue(result.hasContent());

    }

**Test 5** : Description doesn't match to any account.

    @Test
    @DisplayName("description doesn't match to any account.")
    public void doesNotFindAccountSoBalanceReturnsFailMessage() {
        //Arrange
        AccountList accountList = new AccountList();
        String bankAccountDescription = "Crédito Agricola";
        String creditCardAccountDescription = "Revolut";
        String savingsAccountDescription = "Saving for house";
        String badDescription = "Daughter Account";
        Account newBankAccount = new BankAccount(bankAccountDescription,1);
        Account newCreditCardAccount = new CreditCardAccount(creditCardAccountDescription, 100.80);
        Account bankSavingsAccount = new BankSavingsAccount(savingsAccountDescription,1);
        accountList.addAccount(newBankAccount);
        accountList.addAccount(newCreditCardAccount);
        accountList.addAccount(bankSavingsAccount);

        CheckAccountBalanceController balanceCtrl = new CheckAccountBalanceController(app,accountList);

        //Act
        Result<Double> result = balanceCtrl.checkAccountsBalance(badDescription);

        //Assert
        assertNull(result.getContent());
        assertFalse(result.hasContent());

    }


# 4. Implementation

For this sprint, we started to implement DTO classes to help us to return to the user some information stored on system.

    public List<AccountDTO> getAllAccountsDescription(String mainEmail) {

        FamilyService familyService = app.getFamilyService();
        Family family = familyService.getFamilyByMemberMainEmail(mainEmail);
        PersonList personList = family.getPersonList();
        Person familyMember = personList.getPersonByMainEmail(mainEmail);
        this.accountList = familyMember.getPersonAccountList();
        return accountList.getAllAccountsDescription();
    }

It was a bit confusing at the beginning to implement DTO, but after some explanations we managed to do it.   

# 5. Integration/Demonstration

The US depends on [US170], [US171], [US172] and [US173]. The user only can check the account's balance if
he/she has an existent account in system.

# 6. Observations

We discussed how to get the right sequence of occurrences, since getting all descriptions and then the balance of the account,
we always need the account list. The resolution we thought about was that the Controller, once it gets the descriptions, 
the account list is already known after the user chooses the account. Therefore, we avoid doing the same steps twice, and it is
faster to get the balance.

In the future, we hope that will improve our app's performance.

[US170]: US170_CreatePersonalCashAccount.md
[US171]: US171_AddBankAccount.md
[US172]: US172_AddBankSavingsAccount.md
[US173]: US173_AddCreditCardAccount.md
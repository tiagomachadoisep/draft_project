# Use Case Diagram - Second Sprint


To better show the interaction between the requirements and the actors, next you can see the use case diagram: 


```plantuml
@startuml
title Use Case Diagram - Second Sprint

left to right direction

actor "FamilyAdministrator" as FA
actor "FamilyMember" as FM
actor "Parent" as P

rectangle FFMApplication{
FA --> (US106 - change relation between two family members) 
FA --> (US111 - add a category to the family's category tree)
FA --> (US130 - transfer money from the family's cash account to another family member's cash account)
FA --> (US135 - check the balance of the family´s cash account or of a given family member)

FM --> (US170 - create a personal cash account)
FM --> (US171 - add a bank account I have)
FM --> (US172 - add a bank savings account I have)
FM --> (US173 - add a credit card account I have)
FM --> (US180 - transfer money from my cash account to another family member's cash account)
FM --> (US181 - register a payment that I have made using one of my cash accounts)
FM --> (US185 - check the balance of one of my accounts)
FM --> (US186 - get the list of movements on one of my accounts between two dates)

P --> (US188 - check the balance of one of my children's cash account)
}
@enduml
# Family Finance Management Project SWitCH

# 1. Developing Team

The students identified on table below represent the developing team of this project.

| Student Nr.    | Name               | 
|----------------|--------------------|
| **[1201763]**  | Filipa Santos      | 
| **[1201764]**  | Gonçalo Reis       | 
| **[1201769]**  | Luís Rodrigues     | 
| **[1201921]**  | Luís Silva         |
| **[1201773]**  | Marta Inácio       |
| **[1201774]**  | Miguel Oliveira    |
| **[1201776]**  | Pedro Santos       |
| **[1201782]**  | Thayane Marcelino  |

# 2. Functionalities Distribution ###

The distribution of  the requirements throughout the project's development by the members of developing team was 
carried out as described in the following table.

| Student Nr.   | Sprint 2                         | 
|---------------|----------------------------------|
| **[1201763]** |[US111], [US170]         |
| **[1201764]** |[US172], [US186]                  |
| **[1201769]** |[US173], [US188]         |
| **[1201921]** |[US180]                  |
| **[1201773]** |[US171], [US185]         |
| **[1201774]** |[US181], [US135]         |
| **[1201776]** |[US106], [US188]         |
| **[1201782]** |[US130]                  |


[1201763]: ../students/1201763.md
[1201764]: ../students/1201764.md
[1201769]: ../students/1201769.md
[1201921]: ../students/1201921.md
[1201773]: ../students/1201773.md
[1201774]: ../students/1201774.md
[1201776]: ../students/1201776.md
[1201782]: ../students/1201782.md

[US106]: ../sprint2/user_stories/US106_ChangeRelationship.md
[US111]: ../sprint2/user_stories/US111_AddACategoryToFamilyCategoryTree.md
[US130]: ../sprint2/user_stories/US130_TransferMoney.md
[US135]: ../sprint2/user_stories/US135_CheckBalanceOfFamilyOrMemberCashAccount.md
[US170]: ../sprint2/user_stories/US170_CreatePersonalCashAccount.md
[US171]: ../sprint2/user_stories/US171_AddBankAccount.md
[US172]: ../sprint2/user_stories/US172_AddBankSavingsAccount.md
[US173]: ../sprint2/user_stories/US173_AddCreditCardAccount.md
[US180]: ../sprint2/user_stories/US180_TransferMoneyFromCashAccountToCashAccount.md
[US181]: ../sprint2/user_stories/US181_RegisterMemberPaymentUsingCashAccount.md
[US185]: ../sprint2/user_stories/US185_CheckAccountBalance.md
[US186]: ../sprint2/user_stories/US186_GetTransactionsBetweenDates.md
[US188]: user_stories/US188_AsParentCheckBalanceOfOneChildrenCashAccount.md

```plantuml
title \n\nUS151 Add Email to profile\n\n\n

participant ":IAddEmailController" as C
participant "assemblerToDTO\n:AssemblerToDTO" as M
participant ":EmailDTO" as ED
participant ":IPersonService" as PS
participant ":PersonID" as ID
participant ":IPersonRepository" as PR
participant ":IPersonAssemblerJPA" as AJPA
participant "person\n:Person" as P
participant "PersonJPA" as PJPA
participant "email\n:Email" as E
participant "EmailJPA" as EJPA
participant ":IPersonRepositoryJPA" as PRJPA

autonumber
[o-> C : addEmailToProfile(email, mainEmail)
activate C
C -> M : toEmailDTO(email, mainEmail)
M -->ED *: create
C -> PS : addEmailToProfile(emailDTO)
activate PS
PS --> ID * : create(new Email(email))
PS -> PR : findByID(personID)
activate PR
PR -> PRJPA : findByID(personID)
activate PRJPA
PRJPA --> PR : optionalPersonJPA
deactivate PRJPA
PR --> PR : personJPAOptional.get()
PR -> P *: create(personjpa)
deactivate AJPA
PR --> PS : optionalPerson
deactivate PR
PS -->PS : person.get()
PS -> P : addEmailToProfile(emailDTO)
activate P
P  -> E ** : create(email)
activate E
E -> E : validateEmail(email)
deactivate E
P -> P : addEmail(email)
P --> PS : true
deactivate P
PS -> PR : updatePerson(person)
activate PR
PR -> AJPA : toJPA(person)
activate AJPA
AJPA -> PJPA *:create(person)
PJPA -> EJPA *: create(email)
AJPA --> PR : personjpa
deactivate AJPA
PR -> PRJPA : save(personjpa)
deactivate PR
PS --> C : result = true
[<--o C : result = true
deactivate PS
deactivate C

```
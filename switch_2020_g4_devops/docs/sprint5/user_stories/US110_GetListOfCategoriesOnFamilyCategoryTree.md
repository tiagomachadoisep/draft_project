```plantuml
autonumber
title US110 - Get the list of categories on family's categorylist

participant "ctrl\n:getFamilyCategory\nListController" as C
participant ":ICategoryService" as CS
participant ":AssemblerToDTO" as A
participant ":CategoryDTO" as CDTO
participant ":ICategoryRepository" as CR
participant ":ICategoryRepositoryJPA" as CRJPA

[o-> C : getListOfCategories(familyID)
activate C
C -> CS : getListOfCategories(familyID)
CS -> CR : getListOfCategoriesByFamilyid(familyID)
CR -> CRJPA : findAllByFamilyID(familyID)
CRJPA --> CR : familyCategoriesJPA
CR -> CR : categoryJPAListToCategorableList(familyCategoriesJPA)
CR -> CRJPA : findAllByFamilyID(null)
CR -> CR : categoryJPAListToCategorableList(standardCategoriesJPA)
CR -> CR : addAll(standardCategories)
CR --> CS : categorableList
CS -> A : toCategoryDTO(categorableList)
A -> CDTO *: create(categoryListJPA)
A --> CS : categoryDTOList

CS --> C : familyCategoryListDTO

[<--o C : familyCategoryListDTO
deactivate C
@enduml
```

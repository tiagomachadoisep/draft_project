```plantuml 
@startuml
autonumber
title **US185** As a Family Member check the balance of one of my accounts(SPRING + JPA)

participant ":ICheckAccountBalanceController" as CABC
participant ":IAccountService" as AS
participant ":IAccountRepository" as AR
participant ":IAccountRepositoryJPA" as ARJPA


[o-> CABC : findAccountsByID\n(personID)
activate CABC
CABC -> AS : findAccountsByID\n(personID)
activate AS
AS -> AR : findAccountsByID\n(personID)
activate AR
ref over AR : findAllAccountsByID\n(PersonID personID)
return accountList
return accountList
return accountListDTO

@enduml
```

```plantuml 
@startuml
autonumber
title **US185** findAllAccountsByID


participant ":IAccountRepository" as AR
participant ":List<Account>" as AL
participant ":ICreditCard\nAccountRepositoryJPA" as CCRJPA
participant ":IPersonalCash\nAccountRepositoryJPA" as PCRJPA
participant ":IAccountAccount\nAssemblerJPA" as AAJPA



[o-> AR : findAccountsByID\n(personID)
activate AR
AR -> AL** : create 
AR -> CCRJPA : findAllByPersonID\n(personID)
activate CCRJPA
return allCreditCardAccount
AR -> PCRJPA : findAccountsByID\n(personID)
activate PCRJPA
return allPersonalCashAccounts
AR -> AAJPA : assembleToCreditCardAccountList(allCreditCardAccounts)
activate AAJPA
return ccAccounts
AR -> AAJPA : assembleToPersonalCashAccountList(allPersonalCashAccounts)
activate AAJPA
return personalCashAccounts
AR -> AL : add(ccAccounts)
AR -> AL : add(personalCashAccounts)
deactivate AR


@enduml
```

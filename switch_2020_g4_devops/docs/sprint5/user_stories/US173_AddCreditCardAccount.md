## Sequence Diagrams

```plantuml 
@startuml
autonumber
title **US173** - Add CreditCardAccount (SPRING + JPA)

participant ":IAddCreditCardAccountController" as CCAC
participant ":MapperToDTO" as AMDTO
participant "accountDTO\n:AccountDTO" as ADTO
participant ":IAccountService" as AS
participant ":IAccountAssembler" as AA
participant "account\n:Account" as A
participant ":IAccount\nRepository" as AR
participant ":IAssembleJPA" as AAJPA
participant ":personalCashAccountJPA:\nAccountJPA" as AJPA
participant ":IAccountRepositoryJPA" as ARJPA


[o-> CCAC : addCreditCardAccount\n(description)
activate CCAC
CCAC -> AMDTO : toAccountDTO(data)
activate AMDTO
AMDTO -> ADTO**:create(description)
deactivate AMDTO
CCAC -> AS: addCreditCardAccount(accountDTO)
activate AS
AS -> AA : assemble(accountDTO)
activate AA
AA -> A** : build() 
return : account
AS -> AR: saveNew(account)
activate AR
AR -> AAJPA : accountToJPA(account)
activate AAJPA
AAJPA -> AJPA**: create
return personalCashAccountJPA
AR -> ARJPA: save(personalCashAccountJPA)
return accountID
return accountID
deactivate AR
deactivate CCAC
@enduml
```



## Class Diagram

```plantuml
@startuml
skinparam linetype polyline
AddCreditCardAccountController - AccountRepository 
AccountRepository - ElectronicAccountFactory : uses
AccountConcreteFactory -.up|> ElectronicAccountFactory
CreditCardAccount -.up|> Account
AccountRepository -> Account : listOf
AccountConcreteFactory -left> CreditCardAccount : creates


class AddCreditCardAccountController{
+ addCreditCardAccountController()
}

interface Account{

}

interface ElectronicAccountFactory{

+ fabricateElectronicAccount\n(EAccountType,PersonID,Description,AccountId)
}

class AccountConcreteFactory{

+ fabricateElectronicAccount\n(EAccountType,PersonID,Description,AccountId)
}

class CreditCardAccount{
- description
- personID
- accountID
- accountType
+ CreditCardAccount\n(PersonID, Description, AccountId)
}

class AccountRepository{

+ addCreditCardAccount(PersonID, Description)
}

@enduml
```


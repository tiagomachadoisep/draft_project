# US105
=======================================

# 1. Requirements

*As a family administrator, I want to create a relation between two family members.*

In this US, we design a solution that allows the family administrator to create a relationship between two members of
his family.

## 1.1. Client Notes

According to the information provided by the PO, a relationship is defined between two distinct members of the same
family by the administrator himself, independently of the creation of new family members. Which means, it's possible to
have members without defined relationships. These relationships will have a designation corresponding to the degree of
the relationship (e.g. father, son, cousin, etc.). Obviously, a pair of family members can only have one relationship
defined between them. Regarding this, the PO also stated that inverse relationships should not be created automatically.
If person A is something to person B, the administrator can then create the relationship in what B is to A. This means
that a relationship is identified not only by the members that constitute the relationship but also the degree of the
relationship between them.

Here we want to respond to a request of the family administrator by creating an object of the class FamilyRelationship,
containing all the relevant information.

# 1.2. System Sequence Diagram

OBSOLETE

```plantuml
autonumber
title SSD Create Family Relation
actor "Family Admin" as FA

Participant System

activate FA
FA -> System : create family relation
activate System
System --> FA : show list of members
deactivate System
FA -> System : select first member
activate System
System --> FA : show a list of members\n with no relation with this member
deactivate System
FA -> System : choose one member from the list
activate System
System -->FA : ask for relationship designation
deactivate System
FA -> System : input relationship designation
activate System
System-->FA : success
deactivate System


deactivate FA
```

## 1.3. Process View

OBSOLETE

```plantuml
title Process View
actor "Family Admin" as FA
activate FA
Participant UI
Participant "FFM_BusinessLogic" as BL 
FA -> UI : create family relationship
activate UI
UI-> BL : getFamilyMembers(adminMainEmail)
activate BL
BL-->UI : listOfMembers
deactivate BL
UI--> FA : show list of members of family
deactivate UI
FA -> UI : choose one member
activate UI
UI -> BL : getMembersWithoutRelationshipToMember(adminMainEmail, memberEmail)
activate BL
BL --> UI : listOfMembersWithoutRelationshipToMember
deactivate BL
UI--> FA : shows list of members without relationship
deactivate UI
FA -> UI :choose one member
activate UI
UI--> FA: ask for relationship designation
deactivate UI
FA->UI : input relationship designation
activate UI
UI->BL : createRelationship(adminMainEmail, memberOneEmail, memberTwoEmail, relationDesignation)
activate BL
BL-->UI : result
deactivate BL
UI --> FA : inform success
deactivate UI
deactivate FA
```

## 1.4. Connection with other User Stories

This US is related to the [US104](US104_ListOfMembersRelations.md), where the family administrator wants to get a list
of family members and their relationships with him. The relationships are created through this US.

In [US106](../../sprint2/user_stories/US106_ChangeRelationship.md) where the family administrator changes an existing
relationship between two family members, the object of class FamilyRelationType that is associated with the object
FamilyRelationship is changed to another one.

# 2. Analysis

We define a relationship through the two persons (members of the family) involved and the relation designation. This
designation obviously can't be arbitrary. To solve this, default family relation designations are set by the
FamilyRelationTypeService within the FFM Application. These designations will be string type attributes of objects from
class FamilyRelationType, present in a list of objects hold by the FamilyRelationTypeService. Optionally, these default
relation types could be set by an external configuration file, but that funcionatily is not implemented yet.

Thus, family relationships are represented in objects of class FamilyRelationship, which hold two Person objects
corresponding to the members in question and a FamilyRelationType object which holds the relation designation between
the two members.

The FamilyRelationType object will hold the inverse FamilyRelationType objects that are associated with it, by gender.
This means that if a FamilyRelationType object has "father" as the relation designation attribute, that same object will
hold an object of the same type which has the relation designation "son", and hold another object which has the relation
designation "daughter".

After the family administrator indicates that he wishes to create a new relationship in his family, a list of members is
provided to him, where he will choose one member to whom he wished to add a relationship. Then, another list will be
shown with all the members which still don't have defined relationships with the first member. After he chooses the
second member of the relationship, the administrator chooses the relation designation from the list of available names (
determined in the system). He is then greeted by the success screen for this action.

## 2.1 Relevant excerpt of the Domain Model

The following diagram contains an excerpt of the DM that illustrates the concepts related to this US.

OBSOLETE

```plantuml 
@startuml Domain Model
object "Family" as fam {
id
name
}
object Person {
id
name
}
object "FamilyRelationships" as FR {
designation
}
fam "1" - "*" Person : has members >
fam "1" - "1" Person : has admin >
Person "1" ---- "*" FR : to
Person "1" ---- "*" FR : of
fam "1" - "*" FR : exists


@enduml
```

The diagram states that each Family has a set of members defined in that family, and each member may or may not be a
part of a family relationship, or more, with other members of the family. Two members, however, can not have more than
one relationship created between them.

## 2.2. Attributes

| **_Attributes_**            | **_Business Rules_**                                               |
| :-------------------------- | :------------------------------------------------------------------|
| **
personOne**                 | Mandatory, Person object, not null, has to be a member of the family of the admin     |
| **
personTwo**                 | Mandatory, Person object, not equal to personOne or null, has to be a member of the family of the admin with no defined relation with personOne     |
| **designation**                    | Mandatory, name (text) in the list of possible relations      |

List of possible relations: husband, wife, son, daughter, brother, sister, father, mother, cousin, aunt, uncle, nephew,
niece, grandfather, grandmother, grandson, granddaughter, boyfriend, girlfriend, friend.

# 3. Design

## 3.1. Functionality

The family administrator, logged into the system, sends a request through the UI to create a new relationship between
members of his family. The class Family has an attribute familyRelationshipList where it stores all defined
relationships (objects of the class FamilyRelationship) within. The UI asks for the relevant data, namely the two
persons (given through a unique attribute, the mainEmail) and the designation of the relationship, which is validated.

The UI then sends the request through a controller class, AddFamilyRelationshipController, who in turn communicates with
the FFMApplication. The app connects to the FamilyService object which has information about all the families in our
system, and finds the one corresponding to the current user (for which he is the admin). The family has an attribute of
the class PersonList where the list of members of the family is stored, and this list is returned to the user, so he can
choose the first member for the relationship he wished to add.

After this, the Controller uses the class FamilyRelationshipList, where the list of all the relationships of the family
are stored, through a method dedicated to find the members of the family with whom the first member (chosen by the
Admin) doesn't have a defined relation yet. This list is then returned to the FA, and he can choose the second member
for the relationship.

The only remaining step is to decide the designation of the relationship, and this is done by presenting the FA with a
list of all possible relation designations in our system (defined in the FamilyRelationTypeService class), so that he
can choose a valid designation. When all this is done, a new FamilyRelationship object is created, and appended to the
list of family relationships, which is an attribute of the object of the class FamilyRelationshipList of the given
family.

UPDATED

```plantuml
@startuml
autonumber
title US105 As a family administrator, I want to create a relation between two family members.
participant " CreateFamilyRelationshipController\n : ICreateFamilyRelationshipController" as Controller
participant " FamilyRelationshipService\n : IFamilyRelationshipService" as RelationshipService
participant " : IAssembler" as Assembler
participant " relationshipDTO :\n RelationshipDTO" as RelationshipDTO
participant " familyID : FamilyID" as FamilyID
participant " personOneID : PersonID" as PersonOneID
participant " personTwoID : PersonID" as PersonTwoID
participant " familyRelationType :\n FamilyRelationType" as FamilyRelationType
participant " relationshipID :\n FamilyRelationshipID" as FamilyRelationshipID
participant " familyRepository\n : IFamilyRepository" as FamilyRepository
participant " personRepository\n : IPersonRepository" as PersonRepository
participant " family\n : Family" as Family
participant " : FamilyRelationships" as FamilyRelationships
participant " familyRelationship :\n FamilyRelationship" as FamilyRelationship
participant " : IAssemblerJPA" as AssemblerJPA
participant " familyRepositoryJPA :\n IFamilyRepositoryJPA" as FamilyRepositoryJPA


[o-> Controller : createRelationship\n(relationshipDTO, familyId)
activate Controller
Controller -> Assembler : toRelationshipDTO(personOneEmail,\n personTwoEmail, familyId, designation)
activate Assembler
Assembler --> RelationshipDTO ** : create
deactivate Assembler
Controller -> RelationshipService : createAndSaveRelationship(relationshipDTO)
activate RelationshipService
RelationshipService -> Assembler : toFamilyID(famId)
activate Assembler
Assembler --> FamilyID ** : create(famId)
deactivate Assembler
RelationshipService -> Assembler : toPersonID(personOneEmail)
activate Assembler
Assembler --> PersonOneID ** : create(personOneEmail)
deactivate Assembler
RelationshipService -> Assembler : toPersonID(personTwoEmail)
activate Assembler
Assembler --> PersonTwoID ** : create(personTwoEmail)
deactivate Assembler
RelationshipService -> Assembler : toFamilyRelationType
activate Assembler
Assembler --> FamilyRelationType ** : create(designation)
deactivate Assembler
RelationshipService -> FamilyRepository : findByID(familyID)
activate FamilyRepository
FamilyRepository -> FamilyRepositoryJPA : findById(familyID)
activate FamilyRepositoryJPA
FamilyRepositoryJPA --> FamilyRepository : optionalFamilyJPA
deactivate FamilyRepositoryJPA
FamilyRepository -> FamilyRepository : familyJPAToDomain(optionalFamilyJPA)

FamilyRepository --> RelationshipService : optionalFamily
deactivate FamilyRepository
RelationshipService -> PersonRepository : findByID(personOneID)
activate PersonRepository
PersonRepository --> RelationshipService : optionalPersonOne
deactivate PersonRepository
RelationshipService -> PersonRepository : findByID(personTwoID)
activate PersonRepository
PersonRepository --> RelationshipService : optionalPersonTwo
deactivate PersonRepository
RelationshipService -> RelationshipService : id = generateID()
RelationshipService -> Assembler : toFamilyRelationshipID(id)
activate Assembler
Assembler --> FamilyRelationshipID ** : create(id)
deactivate Assembler
RelationshipService -> Family : createRelationship(personOneID, personTwoID, familyRelationType, relationshipID)
activate Family
Family -> FamilyRelationships : createRelationship(personOneID, personTwoID,\n familyRelationType, relationshipID)
activate FamilyRelationships
FamilyRelationships -> FamilyRelationship ** : create(personOneID, personTwoID,\n familyRelationType, relationshipID)
FamilyRelationships -> FamilyRelationships : addRelationship(familyRelationship)
deactivate FamilyRelationships
deactivate Family
RelationshipService -> FamilyRepository : saveNew(family)
activate FamilyRepository
FamilyRepository -> AssemblerJPA : toJPA(family)
activate AssemblerJPA
AssemblerJPA --> FamilyRepository : familyJPA
deactivate AssemblerJPA
FamilyRepository -> FamilyRepositoryJPA : save(familyJPA)
deactivate FamilyRepository

RelationshipService --> Controller : result
deactivate RelationshipService
[<--o Controller  : result
deactivate Controller
@enduml
```

## 3.2. Class Diagram

OBSOLETE

```plantuml
@startuml
class "FFMApplication" as app{
+ FamilyService getFamilyService()
+ FamilyRelationTypeService getFamilyRelationTypeService() 
}
class "Family" as fam{
+ PersonList getPersonList()
+ FamilyRelationshipList getFamilyRelationshipList()
}
class "PersonList" as PL{
+ List<Person> getMembers()
+ List<PersonDTO> getMembersAsDTO()
+ Person getPersonByMainEmail(String mainEmail)
}
class "FamilyService" as fs{
+ List<Family> getFamilies()
+ Family getFamilyByAdminMainEmail(String adminMainEmail)
}
class "FamilyRelationshipList" as frs{
+ List<Person> getMembersWithoutRelationTo(Person personOne, List<Person> listOfMembers)
+ boolean addRelation(Person personOne, Person personTwo, FamilyRelationType familyRelationType)
- boolean arePersonsRelated(Person personOne, Person personTwo)
}
class "FamilyRelationship" as fr{
+ void setRelationType(FamilyRelationType familyRelationType)
+ boolean isRelationBetweenPersons(Person personOne,Person personTwo)
- void setPersonsOfRelationship(Person personOne, Person personTwo)
}
class "AddFamilyRelationshipController" as crc{
+ boolean createRelationship(RelationshipDTO relationshipDTO, String familyId)
}
class "FamilyRelationTypeService" as FRTS {
+ FamilyRelationType getTypeFromListByDesignation(String designation)
+ List<String> getRelationTypeDesignations()
}
class "FamilyRelationType" as FRT {
- designation : String
+ boolean isSameDesignation(String designation)
}

class "Person" as P {
}

crc "1" o- "1" app : asks >
app "1" *-- "1" fs : has >
fs "1" *- "*" fam : holds >
fam "1" *- "1" PL : has >
fam "1" *-- "1" frs : has >
frs "1" *-- "*" fr : holds >
app "1" *--- "1" FRTS : has >
FRTS "1" *- "*" FRT : holds >
fr "*" o-- "1" FRT : has >
FRT "2" o- "2" FRT : has gender-based inverse type
PL "1" *-- "*" P : holds >
fr "*" o- "2" P : has >

@enduml
```

**********************************************************************************************************


UPDATE LUIS BELO

```plantuml
'skinparam linetype polyline
interface "ICreateFamilyRelationshipController" as Ictrl

class "CreateFamilyRelationshipController" as ctrl {
+createFamilyRelationship(RelationshipDTO relationshipDTO\n, String familyId)
}

class "Assembler" as dto {
+toRelationshipDto(String personOneEmail, String personTwoEmail,\n String familyId, String description)
}

class RelationshipDTO{
+RelationshipDTO(String personOneEmail, String personTwoEmail,\n String description, String familyId)
}

interface "IFamilyRelationshipService" as IFRS 

interface "IFamilyRepository" as IFR{}

interface "IPersonRepository" as IPR{

}

interface IFamilyRepositoryJPA{
}

class PersonJPA{
+ PersonJPA(String personID, String familyID,\n String name, int vat, String address)
}

class FamilyJPA{
+ PersonJPA(String familyID, String adminEmail,\n String familyName, String registrationDate)
}

class FamilyRelationshipJPA{
FamilyRelationshipJPA(String familyRelationshipID,\n FamilyJPA familyJPA, String personOneID,\n String personTwoID, String designation)
}

interface "IPersonRepositoryJPA" as IPRJPA{

}

class "FamilyRepository" as FR {
+savenew(Family family
+findByID(FamilyID id)
}

interface IFamilyAssemblerJPA{
}

class FamilyAssemblerJPA{
+toJPA(Family family)
+toFamilyName(String familyName)
+toRegistrationDate(String date)
+toPersonID(String personEmail)
+toFamilyRelationType(String designation)
+toFamilyRelationshipID(String relationshipID)
}

class "PersonRepository" as PR {
+findByID(PersonID personID)
+toEntity(PersonJPA personJPA)
}

class "FamilyRelationshipService" as FRS {
+createAndSaveRelationship(RelationshipDTO relationshipDTO)
}

class PersonAssemblerJPA{
}

class Family{
-FamilyRelationships membersRelationships
+createRelationship(PersonID personOneID,\nPersonID personTwoID,\nFamilyRelationType familyRelationType,
FamilyRelationshipID familyRelationshipID)
}

interface IPersonAssemblerJPA

ctrl .up.|> Ictrl
ctrl -right> "1" dto : assembler
dto .right.> RelationshipDTO : assembles
ctrl -down-> "1" IFRS : familyRelationshipService
FRS .left.|> IFRS
FRS -left> dto : assembler
FRS --> "1" IPR : personRepository
FRS -right-> IFR : familyRepository
PR .up.|> IPR
FR .up.|> IFR
FRS --> Family
FR --> IFamilyAssemblerJPA : familyAssemblerJPA
FamilyAssemblerJPA .up.|> IFamilyAssemblerJPA
FamilyAssemblerJPA .right.> FamilyJPA : assembles
FamilyJPA --> "*" FamilyRelationshipJPA : familyRelationshipJPAList
FR -> IFamilyRepositoryJPA : familyRepositoryJPA
PR --> IPRJPA : personRepositoryJPA

PR --> IPersonAssemblerJPA : personAssemblerJPA
PersonAssemblerJPA .up.|> IPersonAssemblerJPA
PersonAssemblerJPA .> PersonJPA : assembles


```

## 3.3. Applied Patterns

The OO concept of abstraction is applied by hiding the working details of each task from the user, only presenting him
with a UI, with whom he interacts and inputs commands and data. The functions themselves that handle these requests and
data are contained or encapsulated in a given class, without being able to be accessed from outside that class.

The pattern Information Expert is applied for example when we want to know if there is already a relation between two
persons, we ask the class responsible for the objects, FamilyRelationship. We don't ask for the names of the people in
all relations, we provide the persons and *ask* if any of the relationships are between them.

The Controller pattern is also applied, we create a class whose only purpose is to build a "bridge" between the UI and
the FFMApplication, with each task needed being the responsibility of a given controller, since each class should have
one responsibility.

The Pure Fabrication pattern is applied for example through the creation of the class FFMApplication, FamilyService,
PersonList and FamilyRelationshipList for example. These classes were not part of the concepts of the problem but
essential to coordinate the data and functions of it.

The Creator pattern is used by giving the responsibility of instancing objects of a given class to the class tasked with
storing them. For example, we create the object of class FamilyRelationship in the class FamilyRelationshipList, which
is where the list of existing relationships of a family is stored.

## 3.4. Tests

In this section some tests will be presented that guarantee the requisites of the US are applied.

### 3.4.1 AddFamilyRelationshipController Class tests:

#### 3.4.1.1 Tests for the addRelationshipToFamily() method:

**Test 1** : Create a relationship between the same person - failure.

```
    /**
     * This tests the creation failure of a relationship when the relationship to be created is between the same person.
     */
    @Test
    void addRelationshipToFamily_failCase_SamePersonRelationship(){
        FFMApplication app = new FFMApplication();
        AddFamilyRelationshipController familyRelationshipController = new AddFamilyRelationshipController(app);

        Family familySantos = new Family("Santos",1);
        app.getFamilyService().addFamilyIfNotExists(familySantos);

        String pedroName = "Pedro";
        String pedroEmail = "rrer1@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        familySantos.getPersonList().addAdministrator(pedroName,"18480344 6ZY2",0,pedroBirthdate,pedroEmail,null, genderMale);

        boolean result = familyRelationshipController.addRelationshipToFamily(pedroEmail, pedroEmail, pedroEmail, "husband");

        assertFalse(result);
    }
```

**Test 2** : Create a relationship with an invalid designation (for ex. husbansd) - failure

```
    /**
     * This tests the failure of creating a relation which type does not exist in the FamilyRelationTypeService.
     */
    @Test
    void addRelationshipToFamily_Failure_RelationTypeDoesNotExistInTheSystem(){
        FFMApplication app = new FFMApplication();
        AddFamilyRelationshipController familyRelationshipController = new AddFamilyRelationshipController(app);

        Family familySantos = new Family("Santos",1);
        app.getFamilyService().addFamilyIfNotExists(familySantos);

        String pedroName = "Pedro";
        String ritaName = "Rita";
        String pedroEmail = "rrer1@hotmail.com";
        String ritaEmail = "rrer2@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date ritaBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName,"18484062 7ZV8",0,pedroBirthdate,pedroEmail,null, genderMale);
        familySantos.getPersonList().addMember(ritaName,"17958641 6ZW2",0,ritaBirthdate,ritaEmail,null,genderFemale);

        boolean result = familyRelationshipController.addRelationshipToFamily(pedroEmail,pedroEmail,ritaEmail, "husbansd");

        assertFalse(result);
    }
```

**Test 3** : Create a relationship between two persons when one person is of a different family - failure

```
    /**
     * This tests the creation failure of a relationship when one of the members belongs to another family.
     */
    @Test
    void addRelationshipToFamily_failCase_memberDoesNotBelongToThisFamily(){
        FFMApplication app = new FFMApplication();
        AddFamilyRelationshipController familyRelationshipController = new AddFamilyRelationshipController(app);

        Family familySantos = new Family("Santos",1);
        Family familyGuedes = new Family("Guedes",2);
        app.getFamilyService().addFamilyIfNotExists(familySantos);
        app.getFamilyService().addFamilyIfNotExists(familyGuedes);

        String pedroName = "Pedro";
        String ritaName = "Rita";
        String joaoName = "Joao";
        String pedroEmail = "rrer1@hotmail.com";
        String ritaEmail = "rrer2@hotmail.com";
        String joaoEmail = "rrer3@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date ritaBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        Date joaoBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName,"19741658 6ZV2",0,pedroBirthdate,pedroEmail,null, genderMale);
        familySantos.getPersonList().addMember(ritaName,"18480344 6ZY2",0,ritaBirthdate,ritaEmail,null,genderFemale);
        familyGuedes.getPersonList().addMember(joaoName,"19913200 3ZY1",0,joaoBirthdate,joaoEmail,null,genderMale);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            boolean result = familyRelationshipController.addRelationshipToFamily(pedroEmail,joaoEmail,ritaEmail, "husband");
        });
    }
```

**Test 4** : Create a relationship when the order is not issued by a family administrator - failure

```
    /**
     * This tests the creation failure of a relationship when the member calling the method is not a family
     * administrator.
     */
    @Test
    void addRelationshipToFamily_failCase_NotAnAdminCall(){
        FFMApplication app = new FFMApplication();
        AddFamilyRelationshipController familyRelationshipController = new AddFamilyRelationshipController(app);

        Family familySantos = new Family("Santos",1);
        app.getFamilyService().addFamilyIfNotExists(familySantos);

        String pedroName = "Pedro";
        String ritaName = "Rita";
        String pedroEmail = "rrer1@hotmail.com";
        String ritaEmail = "rrer2@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date ritaBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName,"18480344 6ZY2",0,pedroBirthdate,pedroEmail,null, genderMale);
        familySantos.getPersonList().addMember(ritaName,"19741658 6ZV2",0,ritaBirthdate,ritaEmail,null,genderFemale);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            boolean result = familyRelationshipController.addRelationshipToFamily(ritaEmail, pedroEmail, ritaEmail, "husband");
        });
    }
```

**Test 5** : Create a relationship between two persons already related - failure

```
    /**
     * This tests the creation failure of a relationship when a relationship already exists between the members.
     */
    @Test
    void addRelationshipToFamily_failCase_RelationAlreadyExists(){
        FFMApplication app = new FFMApplication();
        AddFamilyRelationshipController familyRelationshipController = new AddFamilyRelationshipController(app);

        Family familySantos = new Family("Santos",1);
        app.getFamilyService().addFamilyIfNotExists(familySantos);

        String pedroName = "Pedro";
        String ritaName = "Rita";
        String pedroEmail = "rrer1@hotmail.com";
        String ritaEmail = "rrer2@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date ritaBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName,"19741658 6ZV2",0,pedroBirthdate,pedroEmail,null, genderMale);
        familySantos.getPersonList().addMember(ritaName,"18480344 6ZY2",0,ritaBirthdate,ritaEmail,null,genderFemale);
        FamilyRelationType husband = new FamilyRelationType("husband");
        familySantos.getFamilyRelationshipList().addRelation(familySantos.getPersonList().getPersonByMainEmail(pedroEmail),
                familySantos.getPersonList().getPersonByMainEmail(ritaEmail), husband);

        boolean result = familyRelationshipController.addRelationshipToFamily(pedroEmail, pedroEmail, ritaEmail, "husband");

        assertFalse(result);
    }
```

**Test 6** : Create a relationship between two persons in the same family with no prior relationship - success

```
    /**
     * This tests the creation and addition of a relationship between two family members.
     */
    @Test
    void addRelationshipToFamily_successCase(){
        FFMApplication app = new FFMApplication();
        AddFamilyRelationshipController familyRelationshipController = new AddFamilyRelationshipController(app);

        Family familySantos = new Family("Santos",1);
        app.getFamilyService().addFamilyIfNotExists(familySantos);

        String pedroName = "Pedro";
        String ritaName = "Rita";
        String pedroEmail = "rrer1@hotmail.com";
        String ritaEmail = "rrer2@hotmail.com";
        Date pedroBirthdate = new GregorianCalendar(1993, Calendar.JUNE, 25).getTime();
        Date ritaBirthdate = new GregorianCalendar(1992, Calendar.JUNE, 25).getTime();
        String genderMale = "male";
        String genderFemale = "female";
        familySantos.getPersonList().addAdministrator(pedroName,"19913200 3ZY1",0,pedroBirthdate,pedroEmail,null, genderMale);
        familySantos.getPersonList().addMember(ritaName,"19741658 6ZV2",0,ritaBirthdate,ritaEmail,null,genderFemale);

        boolean result = familyRelationshipController.addRelationshipToFamily(pedroEmail,pedroEmail,ritaEmail, "husband");

        assertTrue(result);
    }
```

#### 3.4.1.2 Tests for the getFamilyMembers() method:

**Test 1:** Check the successful retrieval of the member list.

**Test 2:** Check the failure of the list retrieval when the member calling the method is not a family administrator.

#### 3.4.1.3 Tests for the getMembersWithoutRelationshipToMember() method:

**Test 1:** Check the member list retrieval when there are members without relationships with the member passed as
argument.

**Test 2:** Check the member list retrieval even when there are no members without relationship with the member passed
as argument.

**Test 3:** Check the failure of the list retrieval when the user calling the method is not a family administrator.

**Test 4:** Check the failure of the list retrieval when the member passed as argument is not a registered user.

**Test 5:** Check the failure of the list retrieval when the member passed as argument does not belong to the family of
the user calling the method.

#### 3.4.1.4 Tests for the getRelationTypeDesignations() method:

**Test 1:** Check that a list of relation type designations is returned successfully.

### 3.4.2 FamilyRelationshipList Class tests:

#### 3.4.2.1 Tests for the addRelation() method:

**Test 1:** Check the failure of a relationship creation when a relationship between the members already exists.

**Test 2:** Check the failure of a relationship creation when an invalid relation type object is passed as argument.

**Test 3:** Check the creation of a relation between members.

#### 3.4.2.2 Tests for the getMembersWithoutRelationTo() method:

**Test 1:** Check the returned list of members without relationship to a certain member passed as argument when those
members in that situation exist.

**Test 2:** Checks the returned list of members without relationship to a certain member passed as argument when there
are no members without relationship with the member passed as argument.

### 3.4.3 FamilyRelationship Class tests:

#### 3.4.3.1 Tests for the setPersonsOfRelationship() method:

**Test 1:** Check that the creation of a self-relation is not possible.

**Test 2:** Check that the creation of a relationship when the first person is null fails.

**Test 3:** Check that the creation of a relationship when the second person is null fails.

**Test 4:** Check that the creation of a relationship when the familyRelationType object is null fails.

#### 3.4.3.2 Tests for the setRelationType() method:

**Test 1:** Check that the creation of a relationship fails when a null familyRelationType is passed as argument.

#### 3.4.3.3 Tests for the isRelationBetweenPersons() method:

**Test 1:** Check that it returns true when a relationship between the passed members exists.

**Test 2:** Check that it returns false when a relationship between the passed members does not exist.

### 3.4.4 FamilyRelationTypeService Class tests:

#### 3.4.4.1 Tests for the getTypeFromListByDesignation() method:

**Test 1:** Check that the correct object is returned when an existing object has the same relation designation as some
String passed as argument.

**Test 2:** Check that a null object is returned when there is no object with the same relation designation as some
String passed as argument.

**Test 3:** Check that a null object is returned when a null String is passed as argument, as there are no objects with
a null designation.

#### 3.4.4.2 Tests for the getRelationTypeDesignations() method:

**Test 1:** Check that a list of relation type designations is returned.

# 4. Implementation

In the case of this US, we will present the main aspects of the class FamilyRelationship, which is the class of the main
concept involved, and of the Controller class, as well as the FamilyRelationshipList class, responsible for creating and
storing FamilyRelationship type objects for a given family.

The FamilyRelationship is the concept, and it assures that a relation has a valid FamilyRelationType object and is
between two persons.

```
public class FamilyRelationship {

    private Person personOne;
    private Person personTwo;
    private FamilyRelationType relationType;

    /**
     * Constructor of FamilyRelationship.
     *
     * @param personOne          First Person passed as argument.
     * @param personTwo          Second Person passed as argument.
     * @param familyRelationType FamilyRelationType to be set as the relation designation between the two members.
     */
    public FamilyRelationship(Person personOne, Person personTwo, FamilyRelationType familyRelationType) {
        setPersonsOfRelationship(personOne, personTwo);
        setRelationType(familyRelationType);
    }
}
```

In the controller, we perform the actions requested in the US, go to the FA respective family and check if both the
persons proposed are members of that family, then try to add a relation between them with the given designation.

```
/**
     * This method adds a relation between two persons of the family.
     * @param adminMainEmail String email format of the family administrator.
     * @param memberOneEmail String email format of the first family member.
     * @param memberTwoEmail String email format of the second family member.
     * @param relationDesignation String name designation of the relation between the two family members.
     * @return Boolean: True if the relationship is added. False if it failed.
     */
    public boolean addRelationshipToFamily(String adminMainEmail, String memberOneEmail, String memberTwoEmail, String relationDesignation) {
        FamilyService familyService = app.getFamilyService();
        Family family = familyService.getFamilyByAdminMainEmail(adminMainEmail);
        PersonList personList = family.getPersonList();
        Person personOne = personList.getPersonByMainEmail(memberOneEmail);
        Person personTwo = personList.getPersonByMainEmail(memberTwoEmail);
        FamilyRelationTypeService familyRelationTypeService = app.getFamilyRelationTypeService();
        FamilyRelationType familyRelationType = familyRelationTypeService.getTypeFromListByDesignation(relationDesignation);
        FamilyRelationshipList familyRelationshipList = family.getFamilyRelationshipList();
        return familyRelationshipList.addRelation(personOne,  personTwo, familyRelationType);
    }
```

In the FamilyRelationshipList class, the relationships are created and stored, each family will have its own object of
this type, with these responsibilities.

```
public class FamilyRelationshipList {

    private final List<FamilyRelationship> listOfRelationships;
    
    /**
     * This method creates a relationship between two members after proper validation and adds that relationship
     * to the familyRelationshipList.
     *
     * @param personOne          First Person of the relationship.
     * @param personTwo          Second Person of the relationship.
     * @param familyRelationType Relation type which defines the relation of what personOne is to personTwo.
     * @return Boolean: True if the relationship was successfully added. False if there was some error.
     */
    public boolean addRelation(Person personOne, Person personTwo, FamilyRelationType familyRelationType) {
        if (arePersonsRelated(personOne, personTwo)) {
            return false;
        }
        try {
            FamilyRelationship relationship = new FamilyRelationship(personOne, personTwo, familyRelationType);
            this.listOfRelationships.add(relationship);

            return true;
        } catch (Exception e) {
            return false;
        }
    }
```

# 5. Integration/Demonstration

The methods in this US are integrated in [US104](US104_ListOfMembersRelations.md), where the list of relationships with
the FA in a family are presented to the user (FA). These relationships are created by the process outlined here. For
more information about how they will be shown refer to that US.

The process for changing an existing relationship is detailed
in [US106](../../sprint2/user_stories/US106_ChangeRelationship.md)
where the FamilyRelationType object which is an attribute of a FamilyRelationship is changed for another one containing
the new desired relation designation.

# 6. Observations

This US along with US104 presented an ambiguous formulation, where several possibilities arose from the initial
description. The other US asked for a list of all members and relationships of the family, and in this US it was asked
to develop a way to create those relationships.

The ambiguity comes from the different possibilities for relationships, for example do we just need to define them with
respect to the main user, and then derive the others from those? That description is not unique, since for example we
could have two members of the family who are nephews of the main user, and among themselves they could be either
brothers or cousins. Another possibility is to list all relationships between all family members, but this as pointed
out by the PO can lead to an enormous list with superfluous information.

We discussed this and worked with these two interpretations, the one presented before creates a unique object (of the
class FamilyRelationship)
to represent the relationship, containing the two persons involved and an object FamilyRelationType that contains the
relation designation between the persons.

The other possibility is to add an attribute to each person that defines its relationship with the family administrator,
and then derive other possible relationships from that through another method.

For sprint 01, the relation designation of a relationship was simply a string attribute, but that attribute was promoted
to a class of its own.

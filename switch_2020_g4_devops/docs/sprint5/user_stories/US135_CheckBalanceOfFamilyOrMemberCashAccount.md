```plantuml 
@startuml
autonumber
title **US135** Check the balance of family or member cash account

participant ":IcheckFamilyOrMemberCash\nAccountBalanceController" as C
participant ":PersonID" as P
participant ":IAccountService" as S
participant ":AssemblerToDTO" as A
participant ":IAccountRepository" as R
participant ":IAccountRepositoryJPA" as RJPA

[o-> C : checkFamilyOrMember\nCashAccountBalance(String ownerID)
activate C
C -> P *: create
C -> S : getFamilyOrMemberCashAccount(personId)
S -> R : getFamilyOrMemberCashAccount(personID)
R -> RJPA : findAllByPersonID(personId)
RJPA --> R : accountJPA
R -> R : toEntity(accountJPA)
R --> S : account
S -> A : getBalanceDTO(account)
A --> S : accountDTO
S --> C : accountDTO
[<--o C : accountDTOOUT

@enduml
```
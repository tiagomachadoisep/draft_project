# 3. Design

## 3.1. Class Diagram

```plantuml
@startuml
skinparam linetype polyline

title US170 - Create Personal Cash Account Class Diagram

interface ICreatePersonalCashAccountControllerREST {}

class CreatePersonalCashAccountControllerREST {
+ createPersonalCashAccount\n(id, inputPersonalCashAccountDTO)
+ getPersonalCashAccountByAccountId\n(id, accountId)
}

class AssemblerToDTO {
+ inputToPersonalCashAccountDTO\n(id, inputPersonalCashAccountDTO)
+ toPersonalCashAccountDTO\n(personalCashAccount)
+ toAccountDTO(personalCashAccount)
}

interface IPersonalCashAccountService {

}

class PersonalCashAccountService {
+ createPersonalCashAccount\n(personalCashAccountDTO)
+ getPersonalCashAccountByID\n(accountId)
}

interface IPersonService {}

interface IAccountService {}

class PersonService {
+ checkIfEmailExists\n(personalCashAccountDTO)
}

interface IPersonRepository {}

class PersonRepository {
+ existsID(id)
}

interface IPersonRepositoryJPA {}

class AccountService {
+ createPersonalCashAccount\n(personalCashAccountDTO)
+ getAccountByID\n(String accountID)
}

interface IAccountRepository {}

interface IAccountAssembler {}

class AccountRepository {
+ hasPersonACashAccount\n(personID)
+ saveNew\n(personalCashAccount)
+ findByID(accountID)
+ toEntity(accountJPA)
}

interface IAccountRepositoryJPA {}

class AccountAssembler {
+ assemble\n(personalCashAccountDTO)
+ getDescriptionFromAccountDTO\n(personalCashAccountDTO)
+ getBalanceFromAccountDTO\n(personalCashAccountDTO)
}

class PersonalCashAccount {
+ build()
}

class AccountDTO {
+ setOwnerId(id)
+ AccountDTO\n(ownerId, description, balance, accountID, accountType)
}

CreatePersonalCashAccountControllerREST -.up|> ICreatePersonalCashAccountControllerREST
CreatePersonalCashAccountControllerREST --> "1" AssemblerToDTO : assemblerToDTO
CreatePersonalCashAccountControllerREST -up> "1" IPersonalCashAccountService : iPersonalCashAccountService
PersonalCashAccountService -.up|> "1" IPersonalCashAccountService
PersonalCashAccountService -> "1" IPersonService : iPersonService
PersonalCashAccountService --> "1" IAccountService : iAccountService
PersonService -.up|> IPersonService
PersonService -> "1" IPersonRepository : personRepository
PersonRepository -.up|> IPersonRepository
PersonRepository -> "1" IPersonRepositoryJPA : iPersonRepositoryJPA
AccountService -.up|> IAccountService
AccountService --> "1" IAccountRepository : iAccountRepository
AccountService -> "1" IAccountAssembler : iAccountAssembler
AccountService -left> "1" AssemblerToDTO : assemblerToDTO
AccountService -.> PersonalCashAccount : builds
AccountAssembler -.up|> IAccountAssembler
AccountRepository -.up|> IAccountRepository
AccountRepository -> "1" IAccountRepositoryJPA : iAccountRepositoryJPA
AssemblerToDTO -.> AccountDTO : creates

@enduml
```

## 3.2. Functionality

```plantuml
autonumber
title US170 - Create Personal Cash Account Sequence Diagram

participant "controller\n:CreatePersonal\nCashAccountControllerREST" as CPCAC
participant "assemblerToDTO\n:AssemblerToDTO" as ATDTO
participant "accountDTO\n:AccountDTO" as ADTO
participant ":IPersonalCashAccountService" as IPCAS
participant ":IPersonService" as IPS
participant "email\n:Email" as E
participant "personID\n:PersonID" as PID
participant ":IPersonRepository" as IPR
participant ":IPersonRepositoryJPA" as IPRJPA
participant ":IAccountService" as IAS
participant ":IAccountRepository" as IAR
participant ":IAccountRepositoryJPA" as IARJPA
participant "accountJPA\n:AccountJPA" as AJPA
participant "accountID\n:AccountID" as AID
participant ":IAccountAssembler" as IAA
participant "builder\n:PersonalCashAccountBuilder" as B
participant ":IAccountAssemblerJPA" as IAAJPA

[o-> CPCAC : createPersonalCashAccount\n(id, inputPersonalCashAccountDTO)
activate CPCAC
CPCAC -> ATDTO : inputToPersonalCashAccountDTO\n(id, inputPersonalCashAccountDTO)
activate ATDTO
opt id == null or inputPersonalCashAccountDTO == null
ATDTO --> CPCAC : IllegalArgumentException
[<--o CPCAC : "Wrong info"
else
ATDTO -> ADTO : setOwnerId(id)
ATDTO --> CPCAC : assembledDTO
deactivate ATDTO
CPCAC -> IPCAS : createPersonalCashAccount(assembledDTO)
activate IPCAS
IPCAS -> IPS : checkIfEmailExists\n(personalCashAccountDTO)
activate IPS
IPS -> ADTO : getOwnerId()
activate ADTO
ADTO --> IPS : emailAddress
deactivate ADTO
IPS -> E : create(emailAddress)
activate E
E --> IPS : email
deactivate E
IPS -> PID : create(email)
activate PID
PID --> IPS : personID
deactivate PID
IPS -> IPR : existsID(personID)
activate IPR
IPR -> IPRJPA : existsById(id)
activate IPRJPA
IPRJPA --> IPR : check
deactivate IPRJPA
IPR --> IPS : check
deactivate IPR
opt check == false
IPS --> IPCAS : check
IPCAS --> CPCAC : NonExistentPersonException
[<--o CPCAC : "Person does not exist"
else
IPS --> IPCAS : check
deactivate IPS
IPCAS -> IAS : createPersonalCashAccount\n(personalCashAccountDTO)
activate IAS
IAS -> ADTO : getOwnerId()
activate ADTO
ADTO --> IAS : emailAddress
deactivate ADTO
IAS -> E : create\n(emailAddress)
activate E
E --> IAS : email
deactivate E
IAS -> PID : create(email)
activate PID
PID --> IAS : personID
deactivate PID
IAS -> IAR : hasPersonACashAccount(personID)
activate IAR
IAR -> IARJPA : findAllByPersonID(personID)
activate IARJPA
IARJPA --> IAR : accounts
deactivate IARJPA
loop foreach accountJPA in accounts
IAR -> AJPA : getAccountType()
activate AJPA
AJPA --> IAR : accountType
deactivate AJPA
opt accountType == personal cash account
IAR --> IAS : true
IAS --> IPCAS : PersonAlreadyHasCashAccountException
IPCAS --> CPCAC : PersonAlreadyHasCashAccountException
[<--o CPCAC : "Person already has a cash account"
end
else
end
IAR --> IAS : false
deactivate IAR
IAS -> AID : create(GenerateUniqueID.generateID())
activate AID
AID --> IAS : accountID
deactivate AID
IAS -> IAA : getDescriptionFromAccountDTO(personalCashAccountDTO)
activate IAA
IAA --> IAS : description
deactivate IAA
IAS -> IAA : getBalanceFromAccountDTO(personalCashAccountDTO)
activate IAA
IAA --> IAS : balance
deactivate IAA
IAS -> B : create(accountID)
activate B
B --> IAS : personalCashAccount
deactivate B
IAS -> IAR : saveNew(personalCashAccount)
activate IAR
IAR -> IAAJPA : accountToAccountJPA(personalCashAccount)
activate IAAJPA
IAAJPA --> IAR : accountJPA
deactivate IAAJPA
IAR -> IARJPA : save(accountJPA)
IAS -> ATDTO : toPersonalCashAccountDTO(personalCashAccount)
activate ATDTO
ATDTO --> IAS : personalCashAccountDTO
deactivate ATDTO
IAS --> IPCAS : personalCashAccountDTO
deactivate IAS
IPCAS --> CPCAC : personalCashAccountDTO
deactivate IPCAS
[<--o CPCAC : personalCashAccountDTO
deactivate CPCAC
end opt
end opt


```

```plantuml
autonumber
title US170 - Detail over checkIfEmailExists(personalCashAccountDTO)

participant ":IPersonService" as IPS
participant "accountDTO\n:AccountDTO" as ADTO
participant "email\n:Email" as E
participant "personID\n:PersonID" as PID
participant ":IPersonRepository" as IPR
participant ":IPersonRepositoryJPA" as IPRJPA

[o-> IPS : checkIfEmailExists\n(personalCashAccountDTO)
activate IPS
IPS -> ADTO : getOwnerId()
activate ADTO
ADTO --> IPS : emailAddress
deactivate ADTO
IPS -> E : create\n(emailAddress)
activate E
E --> IPS : email
deactivate E
IPS -> PID : create\n(email)
activate PID
PID --> IPS : personID
deactivate PID
IPS -> IPR : existsID(personID)
activate IPR
IPR -> IPRJPA : existsById(id)
activate IPRJPA
IPRJPA --> IPR : check
deactivate IPRJPA
IPR --> IPS : check
deactivate IPR
[<--o IPS : check
deactivate IPS

```

```plantuml
autonumber
title US170 - Detail over assemble(personalCashAccountDTO)

participant ":IAccountAssembler" as IAA
participant "accountDTO\n:AccountDTO" as ADTO
participant "accountID\n:AccountID" as AID
participant "email\n:Email" as E
participant "personID\n:PersonID" as PID
participant "description\n:Description" as D
participant "balance\n:MoneyValue" as MV
participant "personalCashAccount\n:PersonalCashAccount" as PCA

[o-> IAA : assemble(personalCashAccountDTO)
activate IAA
IAA -> ADTO : getAccountId()
activate ADTO
ADTO --> IAA : accountID
deactivate ADTO
IAA -> AID : create(accountID)
activate AID
AID --> IAA : accountID
deactivate AID
IAA -> ADTO : getOwnerId()
activate ADTO
ADTO --> IAA : ownerID
deactivate ADTO
IAA -> E : create(ownerID)
activate E
E --> IAA : email
deactivate E
IAA -> PID : create(email)
activate PID
PID --> IAA : personID
deactivate PID
IAA -> ADTO : getDescription()
activate ADTO
ADTO --> IAA : descriptionString
deactivate ADTO
IAA -> D : create(descriptionString)
activate D
D --> IAA : description
deactivate D
IAA -> ADTO : getBalance()
activate ADTO
ADTO --> IAA : balanceDouble
deactivate ADTO
IAA -> MV : create(balanceDouble)
activate MV
MV --> IAA : balance
deactivate MV
IAA -> PCA ** : build()
[<--o IAA : account
deactivate IAA

```

```plantuml
autonumber
title US170 - Detail over saveNewJPA(personalCashAccount)

participant ":IAccountRepository" as IAR
participant "iAccountRepositoryJPA" as IARJPA
participant "iAccountAssemblerJPA" as IAAJPA
participant "personalCashAccountJPA\n:AccountJPA" as AJPA
participant ":Account" as A


[o-> IAR : saveNewJPA(personalCashAccount)
activate IAR
IAR -> IAAJPA : accountToAccountJPA(personalCashAccount)
activate IAAJPA
IAAJPA -> AJPA : create()
IAAJPA -> A : getID()
activate A
A --> IAAJPA : accountID
deactivate A
IAAJPA -> AJPA : setAccountID(accountID)
IAAJPA -> A : getOwnerID()
activate A
A --> IAAJPA : ownerID
deactivate A
IAAJPA -> AJPA : setOwnerID((PersonID) ownerID))
IAAJPA -> A : getDescription()
activate A
A --> IAAJPA : description
deactivate A
IAAJPA -> AJPA : setDescription(description.toString())
IAAJPA -> A : getBalance()
activate A
A --> IAAJPA : balance
deactivate A
IAAJPA -> AJPA : setBalance(balance.getValue())
IAAJPA -> AJPA : setAccountType\n(EAccountType.CASHPERSONAL)
IAAJPA --> IAR : personalCashAccountJPA
deactivate IAAJPA
IAR -> IARJPA : save(personalCashAccountJPA)
deactivate IAR

```

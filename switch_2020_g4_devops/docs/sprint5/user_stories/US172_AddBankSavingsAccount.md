## Sequence Diagrams

```plantuml 
@startuml
autonumber
title **US172** - Add BankSavingsAccount (SPRING + JPA)

participant ":IAddBankSavingsAccountController" as CCAC
participant ":MapperToDTO" as AMDTO
participant "accountDTO\n:AccountDTO" as ADTO
participant ":IAccountService" as AS
participant ":IAccountAssembler" as AA
participant "account\n:Account" as A
participant ":IAccount\nRepository" as AR
participant ":IAssembleJPA" as AAJPA
participant ":BankSavingsAccountJPA:\nAccountJPA" as AJPA
participant ":IAccountRepositoryJPA" as ARJPA


[o-> CCAC : addBankSavingsAccount\n(description)
activate CCAC
CCAC -> AMDTO : toAccountDTO(data)
activate AMDTO
AMDTO -> ADTO**:create(description)
deactivate AMDTO
CCAC -> AS: addBankSavingsAccount(accountDTO)
activate AS
AS -> AA : assemble(accountDTO)
activate AA
AA -> A** : build() 
return : account
AS -> AR: saveNew(account)
activate AR
AR -> AAJPA : accountToJPA(account)
activate AAJPA
AAJPA -> AJPA**: create
return BankAccountJPA
AR -> ARJPA: save(BankSavingsAccountJPA)
return accountID
return accountID
deactivate AR
deactivate CCAC
@enduml
```

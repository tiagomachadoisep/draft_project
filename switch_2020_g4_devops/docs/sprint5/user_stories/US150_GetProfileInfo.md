
```plantuml 
@startuml
autonumber
title \n**US150** Get the Family Member's profile information\n\n

participant "ctrl:\nIGetProfileInfoController" as C
participant "iPersonService:\nIPersonService" as PS 
participant "repository:\nIPersonRepository" as PR
participant "person:\nPerson" as P
participant "assemblerDTO\n:AssemblerToDTO" as ADTO
participant ":PersonDTO" as PDTO
participant "repositoryJPA:IPersonRepositoryJPA"  as PRJPA

[o-> C : getProfileInfo(mainEmail)
activate C

C-> PS : getPersonDTO(mainEmail)
activate PS

PS -> PR : findByID(personID)
activate PR
PR -> PRJPA : findByID(personID)
activate PRJPA
PRJPA --> PR : optionalPersonJPA
deactivate PRJPA
PR -> PR : personJPAOptional.get()
PR -> P * : create(personJPA) 
PR --> PS : optionalPerson
deactivate PR
PS -> ADTO *: create()
activate ADTO
PS -> ADTO : toPersonDTO(optionalPerson.get())
ADTO -> PDTO *: create
ADTO --> PS : personDTO
deactivate ADTO
PS --> C : personDTO
deactivate PS
[<--o C : personDTO
deactivate C
@enduml
```
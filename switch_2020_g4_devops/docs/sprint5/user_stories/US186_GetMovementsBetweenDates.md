```plantuml 
@startuml
autonumber
title Find Accounts By Owner ID

participant ":IGetAccountMovementsBetweenDatesController" as CTRL
participant ":IAccountService" as AS
participant ":IAccountRepository" as AR

[o-> CTRL : Find accounts by owner ID\n(personID)
activate CTRL
CTRL -> AS : findAccountsByOwnerID\n(personID)
activate AS
AS -> AR : findAccountsByOwnerID\n(personID)
activate AR
ref over AR : findAccountsByOwnerID\n(personID)
return accounts
return accountsDTO
return accountsDTO
@enduml
```

```plantuml 
@startuml
autonumber
title findAccountsByOwnerID

participant ":IAccountRepository" as AR
participant ":IAccountRepositoryJPA" as ARJPA
participant ":IAccount\nAssemblerJPA" as AAJPA

[o-> AR : findAccountsByOwnerID\n(personID)
activate AR
AR -> ARJPA : findAllByPersonID\n(personID)
activate ARJPA
return accountsJPA
AR -> AAJPA : assembleToAccountList(accountsJPA)
activate AAJPA
return accounts
return accounts
@enduml
```

```plantuml 
@startuml
autonumber
title Get Account Movements Between Dates

participant ":IGetAccountMovementsBetweenDatesController" as CTRL
participant ":ILedgerService" as AS
participant ":ILedgerRepository" as AR

[o-> CTRL : Get account movements \nbetween dates(personID, accountID, \nstartDate, endDate)
activate CTRL
CTRL -> AS : GetAccountMovementsBetweenDates\n(personID, accountID, startDate, endDate)
activate AS
AS -> AR : GetAccountMovementsBetweenDates\n(personID, accountID, startDate, endDate)
activate AR
ref over AR : GetAccountMovementsBetweenDates\n(personID, accountID, startDate, endDate)
return movements
return movementsDTO
return movementsDTO
@enduml
```

```plantuml 
@startuml
autonumber
title getAccountMovementsBetweenDates

participant ":ILedgerRepository" as LR
participant ":ILedgerRepositoryJPA" as LRJPA
participant ":LedgerJPA" as LJPA
participant ":ILedger\nAssemblerJPA" as LAJPA

[o-> LR : getAccountMovementsBetweenDates\n(personID, accountID, startDate, endDate)
activate LR
LR -> LRJPA : getLedgerByOwnerID(personID)
activate LRJPA
LRJPA --> LR : ledgerJPA
deactivate LRJPA
LR -> LJPA : findMovementsBetweenDatesByAccountID\n(accountID, startDate, endDate)
activate LJPA
LJPA -> LJPA : transactionsJPA = \nfindTransactionsByAccountID\n(accountID)
LJPA -> LJPA : transactionsBetweenDatesJPA = \nfindTransactionsBetweenDates\n(transactionsJPA, startDate, endDate)
LJPA -> LJPA : movementsJPA = \ngetMovements\n(transactionsBetweenDatesJPA)
LJPA --> LR : movementsJPA 
deactivate LJPA
LR -> LAJPA : assembleToMovements(movementsJPA)
activate LAJPA
return movements
return movements
@enduml
```
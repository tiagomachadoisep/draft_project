```plantuml
title US010 - Create Family and Set Administrator
autonumber
actor "System Manager" as sm
participant ":UI" as ui
participant ":CreateFamilyController" as ctrl
participant ":FamilyRepository" as fr
participant "families:Families" as f
sm -> ui : create family
activate sm
activate ui
ui --> sm : asks data
sm -> ui: createFamily(familyName,\nadminEmail, name, ...)
ui -> ctrl : createFamily(familyName,\nadminEmail, name, ...)
activate ctrl
ctrl -> fr : createFamily(name, adminEmail)
activate fr
fr -> f : generateId()
activate f
f --> fr : id
deactivate f
fr --> "family :\nFamily" as fam * :create(name, \nid, adminEmail)
activate fam
fam --> "name :\nFamilyName" *
fam --> "id : ID" * 
fam --> "adminEmail :\n Email" *
deactivate fam
fr -> f : addFamily(family)
activate f
f -> f : add(family)
deactivate f
fr --> ctrl : familyId
deactivate fr
ref over ctrl : addPerson(name, email, address,...)
ctrl --> ui : familyId 
deactivate ctrl
ui --> sm : informs result
deactivate ui
deactivate sm
```

addPerson(name, email, address, vat, familyId)

```plantuml
title addPerson(name, email, address, vat, familyId)
autonumber
participant ":CreateFamilyController" as ctrl
participant ":PersonRepository" as pr
participant "members:Members"
ctrl -> pr : addPerson(email, name, address,\n vatNumber, familyId)
activate ctrl
activate pr

```
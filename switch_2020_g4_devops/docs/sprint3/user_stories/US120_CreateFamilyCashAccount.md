(...)
## 3.1. Class Diagrams

On this US, only four classes interact together as in the Class Diagram below.

```plantuml

skinparam linetype ortho
title **US120** - Create Family Cash Account

class "CreateFamilyCashAccountController" as ctrl {
+ createFamilyCashAccount(String description, String mainEmail, double balance) : ID
}

class "FamilyRepository" as FR {
+getFamilyIdByAdminMainEmail(mainEmail)
}

class "Families" as F {

}

class "Family" as Fam {
+isAdmin(mainEmail)
+getID()
}

class "AccountRepository" as AR {
+addFamilyCashAccount(description, familyID, balance)
}

class "Accounts" as A {
-hasCashAccount(familyID)
-addAccount(newCashAccount)
}

ctrl --> "1" FR : familyRepository
FR --> "1" F : families
F --> "1" Fam : familyList
FR ..> "1" Fam

ctrl --> "1" AR : accountRepository
AR --> "1" A : accounts

```


## 3.2. Functionality

Relatively to this US, the group designed the next SD:

```plantuml 
@startuml
autonumber
title **US120** - Create Cash Account
actor "FamilyAdmin" as FA
participant ":UI" as UI
participant ":createFamilyCash\nAccountController" as CAC
participant ":Account\nRepository" as AR
participant "accounts : Accounts" as A 
'participant "accountFactory:\nAccountFactory" as aF


activate FA
FA -> UI : create Family Cash Account
activate UI
UI --> FA : ask data \n(description, familyID, balance)
deactivate UI

FA -> UI : inputs data
activate UI 
UI -> CAC : createFamilyCashAccount\n(description, familyID, balance)
activate CAC
CAC -> AR : addFamilyCashAccount\n(description,familyID,balance)
activate AR

AR -> A : hasACashAccount\n(familyID)
activate A
A --> AR : result
deactivate

AR -> A : id=generateAccountID
activate A
deactivate A

'AR -> aF : createFamilyCashAccount\n(description, familyID, balance, id)
'activate aF

AR --> "newCashAccount \n: CashAccount" ** : create
"newCashAccount \n: CashAccount" --> "balance :\nMoneyValue" **: create
"newCashAccount \n: CashAccount" --> "familyID \n: OwnerID" ** : create
"newCashAccount \n: CashAccount" --> "id : \nCashAccountID" ** : create
"newCashAccount \n: CashAccount" --> "description \n: Description" ** : create

AR -> A : addAccount(newCashAccount)
activate A
A -> A : add(newCashAccount)
A --> AR : boolean
deactivate A

AR --> CAC : boolean
deactivate A
deactivate AR
CAC --> UI : boolean
deactivate CAC
UI --> FA : inform success
deactivate UI
deactivate FA
@enduml
```
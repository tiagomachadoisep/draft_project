
```plantuml
@startuml
autonumber
title US151 - Add email to profile\n

participant "emailController\n : EmailController" as EC
participant "people : PersonRepository" as PR
participant "thePerson : Person" as P
participant ": Email" as E

[o-> EC : addEmailToProfile(email, mainEmail)
activate EC
EC -> PR : addEmailToProfile(email, mainEmail)
activate PR
PR -> PR : thePerson = getPersonByID(mainEmail)
PR -> P : addEmailToProfile(email, mainEmail)
activate P
P  -> E ** : create(email)
activate E
E -> E : validateEmail(email)
deactivate E
P -> P : addEmail(email)
P -->PR : email
deactivate P
PR --> EC : email
deactivate PR
[o<-- EC : email 
deactivate EC

@enduml
```
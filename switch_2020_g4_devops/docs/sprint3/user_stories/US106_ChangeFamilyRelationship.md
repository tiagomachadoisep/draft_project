````plantuml
@startuml
title US106 - Change Family Relationship
autonumber
actor "Family Admin" as FA
participant ": UI" as UI
participant ": ChangeRelationshipController" as ctrl
participant ": FamilyRepository" as repo
participant "families : Families" as fams
participant "family : Family" as fam
participant "relations : Relations" as rels
participant "relation : FamilyRelationship" as rel
activate FA
FA -> UI : changeRelationship(familyId,\nemailOne,emailTwo,newDescription)
activate UI
UI -> ctrl : changeRelationship(familyId,\nemailOne,emailTwo,newDescription)
activate ctrl
ctrl -> repo : changeRelationship(familyId,\nemailOne,emailTwo,newDescription)
activate repo
repo -> fams : getFamilyById(familyId)
activate fams
fams --> repo : family
deactivate fams
repo -> fam : changeRelation(emailOne,emailTwo,newDescription)
activate fam
fam -> rels : getRelationById(emailOne,emailTwo)
activate rels
rels --> fam : relation
deactivate rels
fam -> rel : setDescription(newDescription)
fam -->repo : result
deactivate fam
repo --> ctrl : result
deactivate repo
ctrl --> UI : result
deactivate ctrl
UI --> FA : result
deactivate UI
deactivate FA
@enduml
````
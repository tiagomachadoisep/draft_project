```plantuml
@startuml
autonumber
title US101 : add family members
actor "Family Administrator" as Admin
participant " : UI" as UI
participant " : AddFamilyMemberController" as Ctrl
participant " : personRepository:\nPersonRepository" as PR


activate Admin
Admin->UI:Add family member
activate UI
UI->Ctrl: addMemberToFamily(familyID,\npersonID,name,vatNumber,address,familyID)
activate Ctrl
Ctrl -> PR : addPerson(personID,\nname,vatNumber,address,familyID)
Activate PR
ref over PR : addPerson(personID,\nname,vatNumber,address,familyID)
return personID
deactivate PR

Ctrl --> UI : personID
deactivate Ctrl
UI --> Admin : Member added to Family
deactivate UI 
deactivate Admin
@enduml
```
```plantuml
@startuml
autonumber
title : addPerson
participant " : PersonRepository" as PR
participant "members : Members" as members
participant "personList : List<Person>" as PL
participant "person : Person" as person
activate PR
[o-> PR : addPerson(personID,\nname,vatNumber,address)
PR -> members : isMember(personID)
activate members
loop for each person in personList
members -> person : boolean = isID(personID)

opt result==true
members -> PR: failure
[<--o PR : exception
end

deactivate
end
[<--o PR : personID
deactivate PR
@enduml
```


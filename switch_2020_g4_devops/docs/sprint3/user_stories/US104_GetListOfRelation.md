````plantuml
@startuml
title US104 - Get List Of Family Relations
autonumber
actor "Family Admin" as FA
participant ": UI" as UI
participant ": GetListOfRelationsController" as ctrl
participant ": FamilyRepository" as repo
participant "families : Families" as fams
participant "family : Family" as fam
participant "relations : FamilyRelations" as rels
activate FA
FA -> UI : getListOfRelations(familyId)
activate UI
UI -> ctrl : getListOfRelations(familyId)
activate ctrl
ctrl -> repo : getListOfRelations(familyId)
activate repo
repo -> fams : getFamilyById(familyId)
activate fams
fams --> repo : family
deactivate fams
repo -> fam : getListOfRelations()
activate fam
fam -> rels :getListOfRelations()
activate rels
rels --> fam : listOfRelations
deactivate rels
fam --> repo : listOfRelations
deactivate fam
repo --> ctrl : listOfRelations
deactivate repo
ctrl --> UI : listOfRelations
deactivate ctrl
UI --> FA : listOfRelations
deactivate UI
deactivate FA
@enduml
````
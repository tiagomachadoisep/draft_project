```plantuml 
@startuml
autonumber
title **US188** As a parent check the balance of one of my children’s cash account.
actor "Parent" as P
participant ":UI" as UI
participant ":CheckChildCashAccountController" as CCCA
participant ":FamilyRepository" as FR
participant ":AccountRepository" as AR



activate P
P -> UI : CheckChildCashAccount\n(personID)
activate UI
UI -> CCCA : getChildren\n(personID)
activate CCCA
CCCA -> FR : getChildren\n(personID)
activate FR
ref over FR : getChildren(familyID, PersonID)
return personIDListDTO
return personIDListDTO
return children
P -> UI : choose child
activate UI
UI -> CCCA : checkPersonCashBalance\n(personID)
activate CCCA
CCCA -> AR : checkPersonCashBalance(personID) 
activate AR
ref over AR : checkPersonCashBalance(personID)
return value
return value
return value
deactivate P
@enduml
```

```plantuml
@startuml
autonumber
title : getChildren(familyID, personID)
participant " : FamilyRepository" as FR
participant " : Families" as F
participant " : Family" as fam
participant " : FamilyRelationList" as FRL

[o-> FR : getChildren\n(personID,familyID)
activate FR
FR -> F : getFamilyByID(familyID)
activate F
return family
FR -> fam : childrenList(personID)
activate fam
fam -> FRL : findChildren\n(personID) 
activate FRL
return childrenList
return childrenList
[<--o FR : childrenList
deactivate FR
@enduml
```



```plantuml
@startuml
autonumber
title : checkPersonCashBalance
participant " : AccountRepository" as AR
participant " : Accounts" as A


[o-> AR : checkPersonCashAccount\n(personID)
activate AR
AR -> A : hasCashAccount(personID)
activate A
return accountID
AR -> A : getBalance(accountID)
activate A
return value
[<--o AR : value
deactivate AR
@enduml
```

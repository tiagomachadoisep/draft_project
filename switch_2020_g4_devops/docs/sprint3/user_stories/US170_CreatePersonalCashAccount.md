# 3. Design

## 3.1. Class Diagram

```plantuml

title **US170** Create Personal Cash Account Class Diagram

class "CreatePersonalCashAccountController" as CTRL {
   + createPersonalCashAccount(email, description, balance)
}

class "PersonRepository" as PR {
   + getPersonIDByStringEmail(email)
}

class "Members" as MS {
    + getPersonIDByStringEmail(email)
}

class "Person" as P {
    + isThisStringEmailThisPersonID(email)
    + getID()
}

class "PersonID" as PID {
    + isThisStringEmailThePersonID(email)
}

class "Email" as E {
    + sameEmail(anEmail)
}

class "AccountRepository" as AR {
   + createPersonalCashAccount(personID, description, balance)
   
}

class "Accounts" as AS {
   - hasACashAccount(personID)
   - checkTypeCashAndAccountOwner(personID, account)
   + generateAccountId()
   - addAccount(personalCashAccount)
   
}

interface "Account" as A {
    + isCashAccount()
    + hasAccountOwner(personID)
}

abstract class "CashAccount" as CA {

}

class "PersonalCashAccount" as PCA {
    + PersonalCashAccount(personID, description, balance, accountId)
}

class "AccountId" as AID {

}

enum "EAccountType" as EAT {

}

class "Description" as D {

}

class "MoneyValue" as MV {

}

CTRL --> "1" AR: accountRepository
CTRL --> "1" PR: personRepository
AR --> "1" AS : accounts
AR ..> A 
AS -> "0..*" A : accountList
CA .up.|> A 
CA --> "1" AID : accountId
CA --> "1" EAT : accountType
CA --> "1" D : description
CA --> "1" MV : balance
PCA -up-|> CA
PCA -left-> PID : ownerID


PR --> "1" MS : members
PR ..> P
MS -> "0..*" P : personList
P --> "1" PID : mainEmail
PID -> "1" E : personID

@enduml
```

## 3.2. Functionality

```plantuml
autonumber
title US170 - Create Personal Cash Account Sequence Diagram
actor "Family Member" as FM
participant ":UI" as UI
participant "controller\n:CreatePersonal\nCashAccountController" as CPCAC
participant "personRepository\n:PersonRepository" as PR
participant "id\n:Optional<ID>" as OPT
participant "personID\n:PersonID" as PID
participant "accountRepository\n:AccountRepository" as AR
participant "accounts\n:Accounts" as AS
participant "personalCashAccount\n:PersonalCashAccount" as A
participant "description\n:Description" as D
participant "balance\n:MoneyValue" as MV
participant "accoundId\n:AccountId" as AID
participant "accountType\n:EAccountType" as EAT

activate FM
FM -> UI : create a personal \ncash account
activate UI
UI --> FM : asks data \n(email, description, balance)
deactivate UI
FM -> UI : inserts data \n(email, description, balance)
activate UI
UI -> CPCAC : createPersonalCashAccount\n(email, description, balance)
activate CPCAC
CPCAC -> PR : getPersonIDByStringEmail(email)
activate PR
ref over PR 
getPersonIDByStringEmail(email)
end ref
PR --> CPCAC : personOptionalID
deactivate PR
opt personOptionalID is present
CPCAC -> OPT : personOptionalID.get()
activate OPT
OPT --> CPCAC : id
deactivate OPT
CPCAC -> PID : (PersonID) id
activate PID
PID --> CPCAC : personID
deactivate PID
deactivate OPT
CPCAC -> AR : createPersonalCashAccount\n(personID, description, balance)
activate AR
AR -> AS : personAlreadyHasACashAccount(personID)
activate AS
ref over AS
personAlreadyHasACashAccount(personID)
end ref
AS --> AR : boolean
deactivate AS
opt boolean == true 
AR --> CPCAC : empty optional
CPCAC --> UI : empty optional
UI --> FM: person already has \na cash account
else 
AR -> AS : generateAccountId()
activate AS
AS --> AR: accountId
deactivate AS
AR -> A * : create(personID, description, balance, accountId)
activate A
A -> A : this.ownerId = personID
A -> D * : create(description)
A -> A : this.description = description
A -> MV * : create(balance)
A -> A : this.balance = balance
A -> AID * : create(accountId)
A -> A : this.accountId = accountId
A -> EAT : EAccountType.CASHPERSONAL
activate EAT
EAT --> A : accountType
deactivate EAT
A -> A : this.accountType = accountType
deactivate A
AR -> AS : addAccount\n(personalCashAccount)
activate AS
AS -> AS : add\n(personalCashAccount)
deactivate AS
AR --> CPCAC : id
deactivate AR
CPCAC --> UI : id
UI --> FM : informs success
end opt
else
CPCAC --> UI : empty optional
deactivate CPCAC
UI --> FM : person not found
deactivate UI
deactivate FM
end opt
@enduml
```

```plantuml
autonumber
title US170 Sequence Diagram Detail - getPersonIDByStringEmail(email)

participant "personRepository\n:PersonRepository" as PR
participant "members:Members" as M
participant "personList\n:List<Person>" as PL
participant "person\n:Person" as P
participant "personID\n:PersonID" as PID
participant "email\n:Email" as E
participant "id\n:ID" as ID
participant "id\n:Optional<ID>" as OPT

[o-> PR : getPersonIDByStringEmail(email)
activate PR
PR -> M : getPersonIDByStringEmail(email)
activate M
loop for each person in personList && if emailsThePersonID == false
M -> P : isThisStringEmailThisPersonID(email)
activate P
P -> PID : isThisStringEmailThisPersonID(email)
activate PID
PID -> E : sameEmail(anEmail)
activate E
E --> PID : result
deactivate E
PID --> P : emailIsThePersonID
deactivate PID
P --> M : emailIsThePersonID
deactivate P

end loop
opt emailIsThePersonID == true
M -> P : get()
activate P
P -> ID : getID()
activate ID
deactivate P
ID --> M : personID 
deactivate ID
M -> OPT : Optional.of(personID)
activate OPT
OPT --> M : personOptionalID
deactivate OPT
M --> PR : personOptionalID
[<-- PR : personOptionalID
else
M -> OPT : Optional.empty()
activate OPT
OPT --> M : personOptionalID
deactivate OPT
M --> PR : personOptionalID
deactivate M
[<-- PR : personOptionalID
deactivate PR
end

@enduml
```

```plantuml
autonumber
title US170 Sequence Diagram Detail - personAlreadyHasACashAccount(personID)

participant "accounts\n:Accounts" as AS
participant "accounts:\nList<Account>" as LA
participant "account\n:Account" as A

[o-> AS : personAlreadyHasACashAccount(personID)
activate AS
loop for each account in accountList
AS -> A : isCashAccount
activate A
A --> AS : isCashAccount
deactivate A
AS -> A : hasAccountOwner(personID)
activate A
A --> AS : hasAccountOwner
deactivate A
opt isCashAccount == true && hasAccountOwner == true
[<-- AS : true
end
end
[<-- AS : false
deactivate AS

@enduml
```
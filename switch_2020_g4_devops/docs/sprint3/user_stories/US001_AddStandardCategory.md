```plantuml
title US001 - Add Standard Category
autonumber
actor "System Manager" as sm
participant ":UI" as ui
participant ":AddStandardCategory\nController" as ctrl
participant ":CategoryRepository" as cr
participant ":Categories" as cs
participant ":CategoryFactory" as cf

sm -> ui : add category
activate sm
activate ui
ui --> sm : asks for data
sm -> ui : introduces name
ui -> ctrl : addStandardCategory(name\nparentCategory)
activate ctrl
ctrl -> cr : addStandardCategory(name\nparentCategory)
activate cr
loop for category in Categories
cs -> cs : isAtSameLevel(parent)
activate cs
cs -> cs : hasSameName(name)
deactivate cs
opt result == false
cr -> cs : id = generateId()
cr -> cf : createStandardCategory(name, id,parentCategory)
activate cf
end 
end
cf --> "newCategory:\nStandardCategory" as new *
activate new 
new --> "name: CategoryName" *
new --> "id : CategoryId" *
deactivate new
cf --> cr : newCategory
deactivate cf
cr -> cs : add(newCategory)
activate cs
cs -> cs : add(newCategory)
deactivate cs
cr --> ctrl : boolean (?)
deactivate cr
ctrl --> ui : boolean 
deactivate ctrl
ui --> sm : informs result
deactivate ui
deactivate sm
```


**US001, US010 (CORRIGIR ALGUMA COISA), US106.**
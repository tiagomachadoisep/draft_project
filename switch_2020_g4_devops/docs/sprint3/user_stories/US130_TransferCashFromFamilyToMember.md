````plantuml
@startuml
title US130 - Transfer Cash From Family To Member
autonumber
actor "Family Admin" as FA
participant ": UI" as UI
participant ": TransferCashFromFamilyToMemberController" as ctrl
participant ": AccountRepository" as arepo
participant ": LedgerRepository" as lrepo
participant "ledgers : Ledgers" as leds
participant "familyLedger : Ledger" as fled
participant "transfer : Transfer" as transfer
participant "debit : Movement" as deb
participant "credit : Movement" as cred
participant "transactionList : Transactions" as trlist
participant "memberLedger : Ledger" as mled
activate FA
FA -> UI : transferCashFromFamilyToMember\n(familyId,memberMainEmail,amount,categoryId)
activate UI
UI->ctrl : transferCashFromFamilyToMember\n(familyId,memberMainEmail,amount,categoryId)
activate ctrl
ctrl -> arepo : getCashAccountId(familyId)
activate arepo
arepo --> ctrl : familyCashId
deactivate arepo
ctrl -> arepo : getCashAccountId(memberMainEmail)
activate arepo
arepo --> ctrl : memberCashId
deactivate arepo

ctrl -> lrepo : addTransfer(familyId,memberMainEmail,\nfamilyCashId,memberCashId,amount,categoryId)
activate lrepo
lrepo -> leds : getLedgerByOwner(familyId)
activate leds
leds --> lrepo : familyLedger
deactivate leds
lrepo -> fled : addTransfer(familyCashId,memberCashId,amount,categoryId)
activate fled
fled -> trlist : generateTransactionId()
activate trlist
trlist --> fled : transactionId
deactivate trlist
fled --> transfer* : create(familyCashId,memberCashId,\namount,categoryId,transactionId)
transfer --> deb* : create(familyCashId,-amount)
transfer --> cred* : create(memberCashId,amount)
fled -> trlist : add(transfer)
fled -->lrepo : transfer
deactivate fled
lrepo -> leds : getLedgerByOwner(memberMainEmail)
activate leds
leds --> lrepo : memberLedger
deactivate leds
lrepo -> mled : addExistingTransfer(transfer)
activate mled
mled -> mled : add(transfer)
mled --> lrepo 
deactivate mled
lrepo --> ctrl 
deactivate ctrl
ctrl --> UI
deactivate UI
UI --> FA
deactivate FA
@enduml
````
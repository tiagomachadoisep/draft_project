## Sequence Diagrams

```plantuml 
@startuml
autonumber
title **US173** - Add CreditCardAccount
actor "FamilyMember" as FM
participant ":UI" as UI
participant ":AddCreditCardAccountController" as CCAC
participant ":AccountRepository" as AR


activate FM
FM -> UI : addCreditCardAccount\n(accountDesciption)
activate UI
UI -> CCAC : addCreditCardAccount\n(emailOwner, accountDescription)
activate CCAC
CCAC -> AR : addCreditCardAccount\n(personID, accountDescription)
activate AR
ref over AR : addCreditCardAccount\n(personID, accountDescription)
return accountID
deactivate AR
CCAC -> UI : accountID
deactivate CCAC
UI -> FM : account created
deactivate UI
deactivate FM
@enduml
```

```plantuml
@startuml
autonumber
title : addCreditCardAccount
participant " : AccountRepository" as AR
participant " : Accounts" as A
participant " : ElectronicAccountFactory" as EAF


[o-> AR : addCredit(personID,\nname,vatNumber,address)
activate AR
AR -> A : generateAccountID
activate A
return accountID
AR -> EAF : fabricateElectronicAccount\n(personID, description,accpountID)
activate EAF
return account
AR -> A : addAccount(account)
activate A
return true
[<--o AR : accountID
deactivate AR
@enduml
```


## Class Diagram

```plantuml
@startuml
skinparam linetype polyline
AddCreditCardAccountController - AccountRepository 
AccountRepository - ElectronicAccountFactory : uses
AccountConcreteFactory -.up|> ElectronicAccountFactory
CreditCardAccount -.up|> Account
AccountRepository -> Account : listOf
AccountConcreteFactory -left> CreditCardAccount : creates


class AddCreditCardAccountController{
+ addCreditCardAccountController()
}

interface Account{

}

interface ElectronicAccountFactory{

+ fabricateElectronicAccount\n(EAccountType,PersonID,Description,AccountId)
}

class AccountConcreteFactory{

+ fabricateElectronicAccount\n(EAccountType,PersonID,Description,AccountId)
}

class CreditCardAccount{
- description
- personID
- accountID
- accountType
+ CreditCardAccount\n(PersonID, Description, AccountId)
}

class AccountRepository{

+ addCreditCardAccount(PersonID, Description)
}

@enduml
```


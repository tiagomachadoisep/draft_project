```plantuml 
@startuml
autonumber
title **US188** As a Family Member check the balance of one of my accounts
actor "Family Member" as FM
participant ":UI" as UI
participant ":CheckAccountBalanceController" as CABC
participant ":CheckFundsService" as CFS
participant ":AccountRepository" as AR



activate FM
FM -> UI : CheckAccountBalance\n(personID)
activate UI
UI -> CABC : findAccounts\n(personID)
activate CABC
CABC -> AR : findAccounts\n(personID)
activate AR
ref over AR : findAccounts(personID)
return accountIDList
return accountIDList
return accounts
FM -> UI : choose account
activate UI
UI -> CABC : checkAccountBalance\n(personID)
activate CABC
CABC -> AR : checkAccountBalance(personID) 
activate AR
ref over AR : checkAccountBalance(personID)
return value
return value
return value
deactivate FM
@enduml
```


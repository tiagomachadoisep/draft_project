````plantuml
@startuml
title US150 - Get Profile Information
autonumber
actor "Family Member" as FM
participant ": UI" as UI
participant ": GetProfileInformationController" as ctrl
participant ": PersonRepository" as repo
participant "persons : Persons" as pers
participant "person : Person" as p
activate FM
FM -> UI : getProfileInformation(mainEmail)
activate UI
UI -> ctrl : getProfileInformation(mainEmail)
activate ctrl
ctrl -> repo : getProfileInformation(mainEmail)
activate repo
repo -> pers : getPersonById(mainEmail)
activate pers
pers --> repo : person
deactivate pers
repo -> p : getProfileInformation()
activate p
p --> repo : profileInfo
deactivate p
repo --> ctrl : profileInfo
deactivate repo
ctrl --> UI : profileInfo
deactivate ctrl
UI --> FM : profileInfo
deactivate UI
deactivate FM
@enduml
````
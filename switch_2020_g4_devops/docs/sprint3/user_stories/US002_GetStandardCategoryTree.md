`````plantuml
@startuml
title US002 - Get Standard Category Tree
autonumber
actor "System Manager" as SM
participant ": UI" as UI
participant ": GetStandardCategoryTreeController" as ctrl
participant ": CategoryRepository" as repo
participant "categories : Categories" as cats
participant "standardCategoryList : List<Category>" as catl
participant "category : Category" as cat
activate SM
SM -> UI : getStandardCategoryTree()
activate UI
UI -> ctrl : getStandardCategoryTree()
activate ctrl
ctrl -> repo : getStandardCategoryTree()
activate repo
repo -> cats : getStandardCategoryTree()
activate cats
cats --> catl*
loop for category in categories
cats -> cat : isStandard()
activate cat
cat --> cats : checkStandard
deactivate cat
opt checkStandard==true
cats--> catl : add(category)
end
end
cats -> cats : standardTree=\ntoTree(standardCategoryList)
cats --> repo : standardTree
deactivate cats
repo --> ctrl : standardTree
deactivate repo
ctrl --> UI : standardTree
deactivate ctrl
UI --> SM : standardTree
deactivate UI
deactivate SM
@enduml
````
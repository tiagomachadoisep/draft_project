# 3. Design

## 3.1. Class Diagram

## 3.2. Functionality

```plantuml
autonumber
title US111 - Add A Category To The Family's Category Tree
actor "Family Administrator" as FA
participant "controller\n:AddCustom\nCategoryController" as ACCC
participant "categoryRepository\n:CategoryRepository" as CR
participant "categories\n:Categories" as CS
participant "customCategory\n:CustomCategory" as CC
participant "familyID\n:FamilyID" as FID
participant "categoryName\n:CategoryName" as CN
participant "parentName\n:ParentName" as PN
participant "categoryID\n:CategoryID" as CID

activate FA
FA -> ACCC : addCustomCategory\n(familyID, categoryName, parentCategoryId)
activate ACCC
ACCC -> CR : addCustomCategory\n(familyID, categoryName, parentCategoryId)
activate CR
CR -> CS : doesCategoryExistAtLevel\n(categoryName, level)
activate CS
CS --> CR : boolean
deactivate CS
CR -> CS : generateCategoryId()
activate CS
CS --> CR : categoryID
deactivate CS
CR -> CC * : addCustomCategory\n(familyID, name, parentName, categoryID)
CC -> FID * : create(familyID)
CC -> CN * : create(categoryName)
CC -> PN * : create(parentName)
CC -> CID * : create(categoryID)
CR -> CS : addCategory(customCategory)
activate CS
CS -> CS : add(customCategory)
deactivate CS
CR -> ACCC : categoryId
deactivate CR
ACCC -> FA : informs success
deactivate ACCC
deactivate FA

@enduml
```

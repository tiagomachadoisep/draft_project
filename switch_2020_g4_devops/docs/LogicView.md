# Logic View - First Sprint

Next we present the Logic View of our work.

````plantuml
@startuml

() UI
Component "Family Finance Management System" as FFMS {
[Family Finance Management Business Logic] as FFMBL
[Family Finance Management UI] as FFMUI
FFMUI -(() BL_API
BL_API - FFMBL
}

FFMS #-left- UI
FFMUI - FFMS

@enduml
````

Going more detailed on the Family Finance Management Business Logic component

````plantuml
@startuml

() BL_API

Component "Family Finance Management Business Logic" as FFMBL{
component Controller

component Model
component Services
'component DTO
'component Interfaces
'Controller -( () Interfaces_API
'Interfaces_API - Interfaces
Controller -(() Model_API
Controller --(()Service_API
'Controller -(() DTO_API
'DTO_API - DTO
'DTO -( Model_API
Model_API - Model
Service_API - Services
Services -( Model_API
}


 FFMBL #-left- BL_API
 FFMBL - Controller

@enduml
````

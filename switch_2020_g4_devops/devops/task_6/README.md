# Track 6


We start by adapting our project to Gradle, with the help of Spring
initializr (https://start.spring.io/) we create the necessary build.gradle
file for our Spring application with the required dependencies and run it successfully.

In order to make Spring use an H2 server as database, we need to perform
some changes, they are detailed in CA3/Part2, and we do those here too. We also want to generate a WAR file
containing our backend application.

In the application.properties file we set the address of the database to
be the one we will choose for our VM containing the H2 server.

## Vagrant

Vagrant is a software for running and managing VMs, it can use Virtualbox, 
Hyper-V and many others, in this case we will use Virtualbox. We will create
two VMs using the Vagrantfile in this folder.

We can test by launching only the "db" machine with vagrant and 
running the app in our local computer, and it works. Next, we will deploy
the app in another VM "web", with Tomcat, with the correct port forwarded,
so we can access it through localhost as well. By using

``
vagrant up``

![vagrant up](vagrant%20up.PNG)

both VMs are created and provisioned, and we can access them through the ports forwarded
in the Vagrantfile, as we can see below. We can use localhost or the IPs of the VMs

![h2console local](h2console%20local%20vm.PNG)

![h2console local vm ip](h2console%20local%20vm%20ip.PNG)

![sws frontend local](sws%20frontend%20local%20vm.PNG)

![sws frontend local vm ip](sws%20frontend%20local%20vm%20ip.PNG)

To use the frontend we developed in React, we assemble the js files with
the gradle command assembleFrontend and copy them to
the Tomcat server where the application war is located.

As a smoke test, we can try to use the functionalities of our app (for example adding an email to a member),
and verifying the success of the operation in the database

![sws frontend local vm add email](sws%20frontend%20local%20vm%20add%20email.PNG)
![h2console vm add email](h2console%20local%20vm%20add%20email.PNG)

## Docker

Another option to run the FFM application in our local machine is using Docker containers. We will use docker-compose, a
tool for defining and running multiple containers using a single configuration file, docker-compose.yml. With the simple
commands

``docker-compose build``

``docker-compose up``

we can create and start all services from our configuration. The Dockerfiles for each machine, web and db, are similar to
the Vagrantfile, for the db we need to have an H2 server, so we need java as well

![dockerfiledb](dockerfiledb.PNG)

and for our web machine, we will also need java, and tomcat, nodejs and npm, as well as git to download the files from our repository

![dockerfileweb](dockerfileweb.PNG)

We also need to expose the correct ports and make adjustments to our application.properties. Since we chose the same
IPs as for Vagrant, we don't need to change that part again. Our web machine will compile the application and put the
WAR file in the tomcat folder, so it can be deployed, and the compiled assets for the frontend will be placed there as
well.

Once we run the build command Docker starts building our images through the Dockerfiles and performing the required tasks,
and when it is finished they should be present in the Docker Desktop dashboard, as we can see below

![dockerdashboard](dockerdashboard.PNG)

And we can run these containers to access our application with the ``docker-compose up`` command

![docker frontend](docker%20frontend.PNG)
![docker frontend login](docker%20frontend%20login.PNG)
![docker db](docker%20db.PNG)
![docker console](docker%20console.PNG)

Our app is fully running in the containers, and we can access through our local machine, this concludes the smoke test
using Docker containers!

To publish our images to Docker Hub we need their ID, and first we have to Login, using ``docker login`` 

![dockerlogin](dockerlogin.PNG)

Having the id, we tag the images

``docker tag 0cc20f381928 1201774isep/task_6_web:latest``

``docker tag 7c1baeaf4c24 1201774isep/task_6_db:latest``

and push them

``docker push 1201774isep/tasl_6_web:latest``

``docker push 1201774isep/tasl_6_db:latest``

and they are in our Docker Hub

![dockerhub](dockerhub.PNG)

## Jenkins

We will now use Jenkins to automate the processes described in the previous sections. We start jenkins in our local
machine using

``java -jar jenkins.war --httpPort=8081``

and setup a new pipeline for this project

![jenkinspipeline](jenkinspipeline.PNG)

In the pipeline we will want to include the following stages: assemble the application; generate and publish
javadoc in Jenkins; execute tests and publish its results in
Jenkins, including code coverage; publish the distributable artifacts in Jenkins (WAR file); build the 2 docker images (app
and db) and publish them in Docker Hub.

To do this we will create a Jenkinsfile. In order to test it, we can start by defining the script directly in Jenkins, and then 
writing to a Jenkinsfile in the repository. We'll start with the steps up to when we use Docker to build the images

![jenkins script](jenkins%20script.PNG)

After a few tries we get the pipeline to pass successfully with all the required steps before the Docker images

![jenkins pass](jenkins%20pass.PNG)

The WAR file is archived as intended, and as we can see on the left, all our reports are ready to be analysed, clicking on "Jacoco report" we can see the code coverage
of our application

![jenkins jacoco](jenkins%20jacoco.PNG)

and on "JavaDoc" we can see its documentation

![jenkins javadoc](jenkins%20javadoc.PNG)

selecting any given method will take us to its javadoc page

![jenkins javadoc example](jenkins%20javadoc%20example.PNG)

And if we go to the current build (#5 in this case) we can select "Test Results" to see al the tests passing

![jenkins tests](jenkins%20tests.PNG)

Now let's add the next task, generating and publishing the Docker images. To do this, we add steps to work in the directory
where the Dockerfile is located (for web and db, respectively), and build the images using the script

![jenkins script docker](jenkins%20script%20docker.PNG)

this will build the images, tag them, and push them to Docker Hub using the credentials I defined in Jenkins previously.

![jenkins pipeline docker](jenkins%20pipeline%20docker.PNG)
![jenkins docker hub](jenkins%20docker%20hub.PNG)

The pipeline ran successfully and the images are present in Docker Hub. This concludes the requested stages of the pipeline
for this track.

Now we want to, instead of executing the script directly in Jenkins, put it in a Jenkinsfile and tell Jenkins where to find it.
This is easy to achieve, in the page where we had the script (Jenkins - Manage Pipeline)

![jenkins file](jenkins%20file.PNG)

and building the pipeline again we see that everything works the same

![jenkins jenkinsfile](jenkins%20jenkinsfile.PNG)

If we want we can even make the builds run everytime a commit is made to our repository.

## Deploying to cloud servers

We start by creating two servers in the ISEP DEI cloud (https://vs-ctl.dei.isep.ipp.pt),
one with H2Server for the database, and one with Ubuntu where we will install and run
Tomcat for the web application.

![h2vs](h2vs.PNG)
![ubuntuvs](ubuntuvs.PNG)
![h2vsdetails](h2vsdetails.PNG)

We configure the H2Server to allow access to databases and copy the url to our application.properties file,
so that our Spring application will use this server as a database

![datasourcevs](datasourcevs.PNG)

As a note, we enable the option to auto create databases for the first time we use it, and once it is created we disable
this mode.

We also need to change our frontend files, to access the correct URL API, which is changed from "localhost" when
we were working in our machine to the address of our VS, where the backend app will be deployed

![urlapivs](urlapivs.PNG)

Next, we have to configure our web virtual server. We start by installing some needed packages

````
sudo apt-get update -y
sudo apt-get install python3 --yes
sudo apt-get install openjdk-8-jdk-headless -y
sudo apt-get install tomcat9 -y
sudo apt-get install tomcat9-admin -y
````

(tomcat8 is not available for Ubuntu 20, so we have to use 9). When it is configured, we will need
to put the WAR file and the static files for the frontend in this server. We do it using ssh 

![sstransfer](sshtransfer.PNG)

and then move this file to the webapps folder of Tomcat

``
mv /root/switch_2020_g4_devops-0.0.1-SNAPSHOT.war /var/lib/tomcat9/webapps/
``

and do the same thing for the frontend files (in src/main/js/my-app/build). We have to add a step here
to make the user "tomcat" owner of the folder we are importing, so it can be accessed

``
chown -R tomcat:tomcat /var/lib/tomcat9/webapps/sws
``

And, once all this is done, we can access our app in the URL http://vs448.dei.isep.ipp.pt:8080/sws/

![appvs](appvs.PNG)

the database frontend (h2-console) can be accessed through http://vs447.dei.isep.ipp.pt:2226/ and there
we can see the modifications we make in the previous link, for instance let's add a new member

![appvsnew](appvsnew.PNG)

and check if he is in the database

![dbvsnew](dbvsnew.PNG)

We can also perform requests to the backend directly using Postman, for example below we will
send a GET request to obtain the list of categories saved in the database

![postman](postman.PNG)

or authenticate and receive a token using a POST request

![postman](postmantoken.PNG)

This concludes the deployment of our app to the cloud.

## Conclusion

We have updated our FFM application to connect to an external database (H2 in this case)
and deployed it locally using both Virtualbox VMs with Vagrant, and Docker. 

We performed some smoke
tests by accessing the app and logging in with an existing user. 

We used Jenkins to automate the processes
of compiling our app, executing the tests, analyzing code coverage, publish Javadoc and test results,
and the generated WAR file. This can be triggered periodically, or after commits to the repository.

Finally, we manually executed the app using ISEP DEI cloud containers, with an H2 server containing 
the database and another server running Ubuntu with tomcat to deploy the web application. We tested
this setup by accessing the app through the server URL and performing actions (authenticating, 
adding a new family member), as well as using Postman.

This project helped us understand how to deploy a real application and make it "usable",
after developing it during the course of the year we would just work with an
"in-memory" H2 database. But here, we have connected it to an external database, and this allows
us for example to take down the web application server (for maintenance, or to deploy a new version
of the app) and keep all the data safely stored.

We also used automation to perform the repetitive tasks, such as executing tests, generating 
documentation and compiling the application. This was incorporated in a pipeline which can then be used
to automatically provide our web application server with the newest version of the FFM app when it is
updated.

We used the cloud servers of ISEP to access and use the application with success.
- [Task 1](task_1/README.md)
- [Task 2](task_2/README.md)
- [Task 3](task_3/README.md)
- [Task 4](task_4/README.md)
- [Task 5](task_5/README)
- [Task 6](task_6/README.md)
- [Task 7](task_7/README.md)
- [Task 8](task_8/README.md)

# Analysis of the Alternatives


We worked with different alternatives to achieve the goal of implementing a pipeline to automate the deployment of our 
Personal Finance Application. The main goal was to implement Infrastructure as Code, allowing the different alternatives
to be reproducible just by using the scripts and code provided in our repository.

## 1. Build Tools

The first thing we worked with were the build tools, where we used both Maven and Gradle to compile and test our code.
They both did their job, but we ultimately considered Gradle to be more intuitive and easy to work with, since the syntax of the build file
is much more simple and straightforward.

We were able to execute the frontend tasks during the *assemble* stage of gradle by including it on its build file. In Maven,
however, we need to specify the tasks related to the frontend on the Jenkinsfile (build the war file and run the command to
build the frontend and generate the frontend static files).

## 2. Infrastructure as Code - Ansible 

In terms of deployment, we used Ansible to deploy our application locally to VMs using Vagrant. Ansible allowed us to 
control and provision the necessary VMs with a couple of files, and this made it straightforward to perform the changes we needed 
in a single place, allowing us to easily configure our setup. We used 3 VMs, one with Ansible and Jenkins (control), one
with the application (frontend and backend) and one with the H2 database.

An alternative was to use Ansible to control the deployment of our application to virtual servers provided by
ISEP DEI (cloud). In this case, we provide ansible to the URL of the servers we create in this cloud, and they are managed
from our local machine as well. Both these options were interesting, since they allowed us to explore the potential of 
Ansible, and both have their advantages and disadvantages. 

In the case of local deployment, since we have everything in our machine it makes it easier to work, we don't need VPN,
we can directly access the servers and test them. However, sometimes it was hard to deal with three machines at the same time due to 
our computer memory. 

With the cloud, we have to be connected to an ISEP VPN to access the 
servers, and sometimes it takes a little more time to perform the commands. However, they are accessible from anywhere (
with the aforementioned VPN), and we can communicate with the servers developed by our colleagues easily.

## 3. Docker

Another option was to use Docker containers to deploy the app locally in our machines, and perform some usability tests,
which was also done successfully. We noticed Docker to be somewhat faster than Vagrant in terms of creating and provisioning
the machines, but the overall functionality was similar. An advantage of Docker is the existence of Docker Hub, where we
can store the images we built for our project (one with the web application and one with the database) and update them with
new versions anytime they are developed.

In these tracks, we also deployed the application to cloud servers, this time manually. We used Ubuntu VSs with Tomcat, and
H2 Servers for the database. Although they were fully functional, this approach has the disadvantage of not being easily automated
like with Ansible, where we can for example setup a trigger when a new commit is made to our repository, and the application
sitting on our VS is automatically updated to the newest version, as opposed to manually deploying the updated version in the 
server.

## 4. Final Acknowledgments

In conclusion, we learned a lot about how to build and deploy a web application. During the year we always worked with 
our local machine only, using an in-memory database to temporarily store our data while we used and tested the application.

This approach is a lot better in the sense that now we can perform operations in our application leading to changes (write,
update, delete) in our database, and they are stored permanently, we can shut down the app and turn it back on and keep
working with our data.

The integration tools used also proved to be very helpful, allowing us to automate the execution of tasks that are repetitive
and always needed during development and production, such as compiling the code, executing tests and publishing documentation.


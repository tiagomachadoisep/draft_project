# Project Personal Finance - DevOps

# Track 1 Maven; local deployment of App with Ansible to Vagrant VMs

Track 1 aims to deploy our web app locally, by creating three virtual machines, one with database H2, one with Tomcat to show our
backend and frontend working and another one with ansible to control the deployment. 

### 1. Maven WAR plugin and Javax API

At the beginning, Maven was not identifying the **@Transactional** annotation because it was missing a dependency in *pom.xml*:

      <dependency>
            <groupId>javax.interceptor</groupId>
            <artifactId>javax.interceptor-api</artifactId>
            <version>1.2.2</version>
      </dependency>

Also, our project did not have the step to generate the war file for the app deployment. So, in *pom.xml* the packaging section was added:

    <groupId>switch2020</groupId>
    <artifactId>project</artifactId>
    <packaging>war</packaging>
    <version>1.0-SNAPSHOT</version>
    <name>project</name>


### 2. Vagrantfile Configuration

On Vagrantfile, three machines were configured: db, web and control. 

The db holds the H2 database and its configuration and provision was practically the same used in ca3-part2.   

The web virtual machine was also very similar to ca3-part2, except the deployed part of the war file that in this project 
is configured on ansible's playbook.

The control VM was used to configured Ansible and Jenkins: 

    sudo apt-add-repository --yes --u ppa:ansible/ansible
    sudo apt-get install ansible --yes
    sudo wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
    sudo java -jar jenkins.war

To access the remote repository through Jenkins, the git was also installed on control machine:

    sudo apt-get install -y nano git

The first attempt to run Jenkinsfile triggered a time out error due to the run time.
To overcome the issue, a 2048 memory was added to control machine: 

    control.vm.provider "virtualbox" do |v2|
      v2.memory = 2048
    end

The file was saved on task_1 folder.


### 3. Jenkins Configuration

The first stages on Jenkinsfile were the ones used on ca5-part2 (but here using maven), except the last two.

On Test With Coverage stage, a new line was added to include Jacoco on the report. **Jacoco** gives the coverage report on each
class implemented.

The command was generated using Pipeline Syntax: 

    jacoco exclusionPattern: '**/*Test*.class', execPattern: '**/jacoco.exec', inclusionPattern: '**/*.class'

The stage *Enable Frontend* was used to install npm to generate the frontend files needed to run on web VM.

Since the Node wasn't available on the control VM, the NodeJS plugin was installed and configured on **Manage Jenkins ->
Global Tool Configuration**:

![nodejs_plugin](./images/nodeJS_plugin.png)

![node_configuration](./images/node_api.png)

On the stage:

    stage('Enable Frontend') {
      steps {
        echo 'Frontendiiing...'
        dir('src/main/js/my-app/') {
          nodejs('npm') {
          sh 'npm install'
          sh 'npm run build'
          }
        }
      }
    }

The stage *Ansible* consisted on getting the playbook and run it:

    stage('Ansible') {
             steps {
                 echo 'Ansible...'
                 ansiblePlaybook credentialsId: 'control', inventory: 'devops/task_1/hosts', playbook: 'devops/task_1/playbook1.yml'
                   }
               
                }


The Jenkinsfile was saved also on task_1 folder.

### 4. Ansible

For ansible configuration, three files were added: *playbook1.yml*, *hosts* and *ansible.cfg*

#### 4.1 ansible.cfg

Certain settings in Ansible are adjustable via a configuration file (ansible.cfg). Here only two lines were added:

    [defaults]
    inventory = /vagrant/hosts
    remote_user = vagrant

identifying the user (vagrant) and, the hosts features.

#### 4.2 hosts

The hosts file identifies each host IP, user and password to access each one:

    [servers]
    web ansible_ssh_host=192.168.33.10 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/web/virtualbox/private_key
    db ansible_ssh_host=192.168.33.11 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/db/virtualbox/private_key

#### 4.3 playbook

The main task of the ansible's playbook is to deploy the war file and copy all frontend files to the web virtual machine.

The apache was used to the frontend files, so it was installed through playbook. To make sure that the files were the correct ones on the specific directory, 
a delete step was wrote to leave the directory empty. After that, the war file was copied to */var/lib/tomcat8/webapps/*
and frontend files were copied to */var/www/html/*

Before this solution, I was trying to configure the H2 database also in playbook but, I've noticed that the application was
not running because the H2 console was not initiating automatically. Even if I was initiating manually, it still didn't work.
The database configuration was transferred to the Vagrantfile.

### Frontend URL's and application.properties changes

The changes made on ca3-part2 were also applied here so that, the spring application could use db VM as an H2 server.

    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.jpa.hibernate.ddl-auto=update
    spring.h2.console.enabled=true
    spring.h2.console.settings.web-allow-others=true

The db IP was configured on data source URL:

    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

The context path was inserted to state the path needed to be specified after the server URL in order to be accessed:

    server.servlet.context-path=/project-1.0-SNAPSHOT

The services from frontend files were also updated to the new localhost path:

    export const URL_API = 'http://localhost:8080/project-1.0-SNAPSHOT/api';

### VM Creation and Jenkins pipeline

After all the documents are written, the command line was used to start all the virtual machines:

    vagrant up

All machines are created and, the Jenkins is accessed on *localhost:8081*.

![virtual_box_machines](./images/virtual_box_task_1.png)

Before going to Jenkins, the control VM was accessed to uncomment one line on */etc/ansible/ansible.cfg*:

![host_key](./images/host_key_present.png)
    
*host_key_checking = False*

The changes were saved and control machine was exited.

An admin account was created and, the Jenkins was started with the default plugins. More plugins were installed
to use on the pipeline like: JaCoCo, Ansible, HTML Publisher, NodeJS and Javadoc.

The NodeJS was configured on **Global Tool Configuration** as said before and, the credentials were created on **Manage 
Credentials**: one to access the remote repository and another one to access the control machine to run the ansible.

A pipeline with SCM was created and **Build Now** was hit.

A successful pipeline was obtained:

![success_pipeline](./images/task_1_jenkins_pipeline.png)

All the reports were published:

- Jacoco

![jacoco](./images/jacoco_coverage.png)

- Javadoc

![javadoc](./images/javadoc_.png)

- Tests

![tests](./images/only_tests.png)

- Artifacts

![artifacts](./images/tests_artifacts_.png)

### Frontend Issues

After running successfully on Jenkins, I tried to access the frontend but, it gave me an error.

Trying to solve that, I accessed the web machine through command line, and I've noticed that the war file was not deployed.

So the tomcat was restarted:

    sudo service tomcat8 restart

Finally, the application started to work!

![frontend_](./images/frontend_task_1.png)

A test was done by adding a new member **TEST** to the application.

It was added on the frontend table and on the database:

![h2_database](./images/h2_access_task_1.png)


### Final Thoughts

Well, it took me 3 days to get this successful achievement.
I had to work on a different computer because mine didn't have enough memory to run 3 virtual machines at the same time.

I also have to say that, without my colleagues, specially our genius Miguel, I couldn't make it!

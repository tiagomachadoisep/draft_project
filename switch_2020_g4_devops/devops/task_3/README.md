# Project Personal Finance - DevOps

*Track 3 - Maven; local smoke test of App with Docker containers; publish docker images; manually execute App in local containers*

- **Smoke Test with Docker**

I initially built the docker-compose.yml file to perform the smoke test, which is a check to see if our backend and frontend are running properly. The creation of the containers will be orchestrated by this file.

![](./images/docker-compose.png)

Following that, I made two folders: one for the frontend Dockerfile and another for the database. I borrowed the Dockerfile db from the ca4 project, as well as the Dockerfile web, however I had to make a few changes. As you can see in the screenshots below, I updated the repository path, then the working directory, and finally the name of my war file.

![](./images/docker_web.png)
![](./images/docker_db.png)

I then went to the command prompt and typed **docker-compose build** in the directory where my docker-compose file was located. Like that, I prepared the images, but to put the containers to work, I used the **docker-compose up** command.
Thus, was generated both web and db containers.

![](./images/docker_containers.png)

Then I tried to view my application in the browser, but it didn't function. This is because some complications emerged when my colleagues needed to make changes to the application.properties file. They were, however, easily discovered and altered.
I used localhost:9092 to check if my database configuration was accurate in the Application.properties file (as configured in docker-compose.yml). Then I double-checked that the server was on the right track (/switch 2020 g4 devops-0.0.1-SNAPSHOT), and finally I commented on the source of the conflict, which was the datasource line in this case.

![](./images/app_properties.png)

Finally, to access the app, I navigated to the follwings URL's:

**db**: http://localhost:8082/login.do?jsessionid=0352717b74233936fc24ac423cad49cd

**web**: http://localhost:8080/project-1.0-SNAPSHOT/sws

As can be seen in the screenshot below, the database correctly displays our application's data, and the front end,
when attempting to access the categories, also displays the data as expected. Moreover, i added a member called "Toni" in the frontend, and as we can see in the database screenshot,
that data appeared there as well.

![](./images/app_web.png)
![](./images/app_db.png)

- **Pipeline**

As requested, we had to set up a Jenkins pipeline with the following stages: assemble the application;
generate and publish javadoc in Jenkins; run tests (unit and integration) and publish the results in Jenkins,
including code coverage; publish the distributable artifacts in Jenkins;
and build and publish two Docker images (app + db) on Docker Hub.

We can see below the Jenkinsfile created to the effect:

![](./images/jenkinsfile_1.png)
![](./images/jenkinsfile_2.png)

Because the 8080 was already in use, I needed to redirect to another port to access Jenkins. I used the command **java -jar jenkins.war —httpPort=8081** to accomplish this.

Unfortunately, after building the pipeline (following all the steps outlined in ca5), I was unable to get the "Publish image to Dockerhub" step to work. Below is a screenshot
with everything working except that step:

![](./images/jenkins_pipeline.png)

If we go to the console output of those failed builds, we can see always the same error, captured in this screenshot:

![](./images/jenkins_error.png)

After several tries, I gave up without the jenkins pipeline working properly. However, I believe the most important task has been achieved:
being able to run the app in the docker containers created and perform a smoke test as asked.
pipeline {
    agent any

    stages {

        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'goncalo-bitbucket-credentials-project', url: 'https://bitbucket.org/switchmarta/switch_2020_g4_devops/src/master/'
            }
        }

        stage('Assemble') {
             steps {
                 echo 'Assembling...'
                 sh './mvnw clean compile war:war'
                 sh './mvnw jar:jar'
             }
        }

        stage('Tests With Code Coverage') {
            steps {
                echo 'Testing with code coverage...'
                sh './mvnw test jacoco:report'
                junit  skipPublishingChecks: true, testResults: '*/surefire-reports/*.xml'
                jacoco exclusionPattern: '**/*Test*.class', execPattern: '**/jacoco.exec', inclusionPattern: '**/*.class'
            }
        }

        stage('Javadoc') {
            steps {
                echo 'Creating Javadoc...'
                sh './mvnw javadoc:javadoc'
                javadoc javadocDir: 'target/site/apidocs/', keepAll: true
            }
        }

        stage('Archiving artifacts') {
            steps {
                echo 'Archiving artifacts...'
                archiveArtifacts artifacts: '**/*.jar'
  		        archiveArtifacts artifacts: '**/*.war'
            }
        }

        stage('Docker Images') {
            steps {
                echo 'Publishing web image...'
                dir('devops/task_3/web') {
                    script {
                        bat 'docker login -u 1201764 -p 202020!Apple registry.hub.docker.com/'
                        docker.withRegistry("https://registry.hub.docker.com/", "goncalo-dockerhub-credentials") {
                            def customImage = docker.build("1201764/web:${env.BUILD_ID}")
                            customImage.push()
                        }
                    }
                }
                echo 'Publishing db image...'
                dir('devops/task_3/db') {
                    script {
                        docker.withRegistry("https://registry.hub.docker.com/", "goncalo-dockerhub-credentials") {
                            def customImage = docker.build("1201764/db:${env.BUILD_ID}")
                            customImage.push()
                        }
                    }
                }
            }
        }
    }
}
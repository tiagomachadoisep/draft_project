#Track 7 - Maven; cloud deployment of App with Ansible

For this assignment I've created a Vagrantfile to implement only a local VM called control where besides nano, git, jdk, python
and other utilities, I also installed Jenkins to create a pipeline for the continuous integration and Ansible to manage the other 
two servers that I used for this track.

As cloud, I used two of the available virtual servers templates provided by DEI.

I've created one for the backend (ID - 31) and another with H2 (ID - 33). I created a hosts file to configure the hosts 
used in my ansible playbook.

In the folder with the vagrant file I used the command

    vagrant up

Everything went smoothly, and the *control* VM was created with success.

Now it's time to enter the *control* VM 

    vagrant ssh control

Now the first thing I do is ping both servers and see if they can connect with my VM, my backend server has ssh installed,
and I never got any problem to ping him.

![](images/image1.PNG)

On the other hand my db server don't have ssh installed, and I was never able to ping it, I read some documentation about 
using ansible without ssh, it's possible, but it's seem to me that it was more easy to create another server with the 
same template as the backend, which has ssh already installed.

So in my ansible playbook I needed to created two tasks for the db host to install and start H2

![](images/image2.PNG)

Now I'm able to ping both servers.

I completed my playbook with the tasks for the web server in which I install tomcat9, I copy the .war file to support
the backend of our application, and the build files relative to the frontend.

**Note:**
To get my build folder related to the frontend/react app and to copy the created folder to my web server, 
I needed to use the command

    npm run build

otherwise, frontend wouldn't work, I also needed to update node.js version, I've done this by myself because I focused 
first in make it worked, but if I have the time I'll try to update node.js in Vagrantfile. Also if I can I'll run the 
*npm run build* in playbook.

Now in my Jenkinsfile I've created six jobs, checkout, tests with coverage, javadoc and archiving are very similar to
what we've done in the previous assignment and are self-explanatory, besides using maven commands instead of gradle. 
My job assemble is similar, but I added a command to use an application.properties file that I save in my task_7 folder, 
this way I'm not dependent if any of my colleagues make same change in this file.

![](images/image3.PNG)

I've created a new job called Ansible to read my playbook. This job as 3 arguments:

credentialsId - the username is vagrant, and the password is the one in the 
.vagrant/machines/control/virtualbox/private_key file

inventory - with the path to hosts file

playbook - with the path to the ansible-playbook .yml file

Now I try to build my Jenkins pipeline

![](images/image4.PNG)

At this point I've tried to run the application, but it would not work at this point I was really stuck. 

My colleague Miguel Oliveira helped me because I didn't remember that three files in our react project use URI, 
and I need to change so that I don't use https only http, and the URL to my backend server. 
I checked the error because when I was in the frontend of our app, in the devtools I can the wrong URI.

After this change everything was working:

The web server:

![](images/image5.PNG)

and the db server:

![](images/image6.PNG)


w
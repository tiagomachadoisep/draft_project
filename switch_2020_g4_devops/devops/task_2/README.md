# Devops task 2: Gradle; local deployment of App with Ansible to Vagrant VMs

I had RAM problems while operating the VMs, where the Jenkins pipeline would get stuck. Increasing the VMs dedicated
RAM did not solve the problem. Below is an image of where the pipeline would get stuck:

![Jenkins_stuck](Images/Jenkins_stuck.png)

The **Vagrantfile**, **Jenkinsfile** and the **post-pipeline.yml** playbook are inside this folder so the teacher can check them.
The playbook would transfer the relevant files originated from the `gradle assemble` task and deliver them to the 
appVM virtual machine, deploying our app using tomcat.

Thus, I decided to make the deployment of our app to the DEI cloud servers, using only Buddy. This implementation
is described in the section below.

---

## Alternative deployment: Gradle; cloud deployment of App using Buddy

For the cloud deployment, I made use of DEI virtual servers with the following templates:

![UbuntuServerTemplate](Images/UbuntuServerTemplate.png)
This is the server that will hold the app (backend+frontend) inside the '/var/lib/tomcat9/webapps' folder.
![UbuntuServerInfo](Images/UbuntuServerInfo.png)
As this server is a clean Linux container, the software to be installed will be discussed later in this section.
The public ssh access should be enabled.
***
![h2ServerTemplate](Images/h2ServerTemplate.png)
This is the server that will serve as our database.
![h2ServerInfo](Images/h2ServerInfo.png)
The Auto-create new databases should be enabled for the first time the data from our bootstrapping is written to the 
database. Afterwards, we can disable it.

***

### The Buddy pipeline

Below is an overview of the actions created on a Buddy pipeline, which executed successfully after many tries of 
correcting errors.
I will be explaining every action, as I tried to reproduce the steps that Jenkins and Ansible would perform.
I also made use of the integration of Buddy with Bitbucket and Discord, to push reports. Before every pipeline runs,
Buddy automatically downloads our remote repo to the pipeline filesystem, to be used for every Action, where some of them use
containers where the home directory will be our repository root directory. This facilitates the initial step of cloning
our repository first.

![buddy_pipeline_completion](Images/buddy_pipeline_completion.png)

***

### Prepare AppServer

The AppServer is the name I gave to our Ubuntu Server where the app will be deployed.

On this Action, I am making sure the Ubuntu Server that will deploy the app has the necessary software to be able 
to do it. I am probably installing more than what is enough, but keep in mind that I was debugging various errors
until the build succeeded. A ssh connection is necessary.

![Prepare_AppServer](Images/Prepare_AppServer.png)

**Note:**
This Action is preparing the Ubuntu Server assuming that this server can change. In this case we are making sure
the server has all the necessary software. For the h2 database server, the server does not allow ssh connections so
I can not create an Action preparing the database server. Anyway, if the database servers are always configured 
following available templates, they do not need any extra steps of configuration.

***

### Gradle Tasks

This Action will perform the various gradle tasks needed (gradle assemble, gradle test, gradle javadoc) after
making some adjustments to the files being used. As every one of us needed different settings, I made a copy of the 
necessary files to my devops/task_2 folder and copied them to the respective locations before running the gradle tasks.
The final step was to rename the War file generated. For some reason Buddy transformed our repository name underscores
into hyphens and we do not want that.

![Gradle_Tasks_code](Images/Gradle_Tasks_code.png)

This runs on a clean Ubuntu container, so I also need to install the necessary software, as you can see below on 
the Customize Environment code box:

![Gradle_Tasks_env1](Images/Gradle_Tasks_env1.png)

![Gradle_Task_env2](Images/Gradle_Task_env2.png)

**Note:** Please check the copied files as they are different from the ones my collegues used.
I had to add the hostname and port of the h2 database to the **application.properties** file.
For the **URL_API.js**, **UserRoleService.js** and **LoginSimpleService.js** files, I had 
to change the URL to match the hostname and port of the Ubuntu server where the backend is being 
deployed. As for the **build.gradle** file I just had to add the following code so that the
`gradle javadoc` would compile successfully:

```groovy
javadoc {
    options.encoding = 'UTF-8'
}
```

***

### Transfer WAR file to Appserver

This Action makes use of the rsync software to synchronize files between servers. The ansible step I would reproduce 
in the initial task assignment also makes use of rsync. In this particular case, I will send the **war** file generated
by the gradle assemble task to the '/var/lib/tomcat9/webapps' folder on the Ubuntu server.

![Transfer_war](Images/Transfer_war.png)

***

### Transfer Frontend files to AppServer

This Action will make the same thing as the Action above, but now we are transferring the relevant frontend files
located at 'src/main/js/my-app/build' to '/var/lib/tomcat9/webapps/sws' on the Ubuntu server.

![](Images/Transfer_frontend.png)

***

### Give tomcat permissions to AppServer

This Action will run a **chwon** command on the Ubuntu server so that '/var/lib/tomcat9/webapps/sws' has the 
required permissions to be accessed.

![GiveTomcatPermissions](Images/GiveTomcatPermissions.png)

***

### Copy reports and remove build products

This is an optional Action. Here I am copying the reports generated by the `gradle test` and `gradle javadoc` tasks
to folders inside 'devops/task_2'. After copying them, I am removing the folders generated by the gradle tasks, so
Buddy can push to the repository only the copied files on the next Action. This proved to not be enough, but I only 
used this action once, so I kept it as it is.

![CleanBuildProducts](Images/CleanBuildProducts.png)

This is executed in a separate Ubuntu container, where I made sure it had the necessary software.

![](Images/CopyAndRemoveInfo.png)

***

### Push reports

This is an optional Action, dependent on the above aforementioned Action. My intent here was to make Buddy
push to our repository only the reports I copied to my 'devops/task_2' folder. Unfortunately I needed to make some more
cleaning, but since I only used this Action once, I corrected the mistake directly on the repository and left this 
Action as is, just to show the possible integration of Buddy with Bitbucket.

![PushReports](Images/PushReports.png)

Below is the commit made by Buddy after finishing this Action.

![buddy_bitbucket_integration](Images/buddy_bitbucket_integration.png)

***

### Send notification to Discord

This is an optional Action. Here Buddy will push a notification to Discord after the pipeline finishes, reporting
the success or failure of the execution. This is just to show the integration of Buddy with Discord.

![](Images/SendNotificationDiscord.png)

![buddy_discord_integration](Images/buddy_discord_integration.png)

***
#### This is the end of the Buddy pipeline description.
***

### Accessing our deployed App

Now we are ready to access our App at http://vs453.dei.isep.ipp.pt:8080/sws/ .
Note that we need to be using ISEP's VPN.

![success_serverappLogin](Images/success_serverappLogin.png)

If you want to test our App, log in as a Family Administrator with **Email:** ze@isep.pt **Password:** 218471742 .

![success_server_appAccess](Images/success_server_appAccess.png)

#### End of implementation of task 2.

***

## Personal note

Since I closely followed the deployment of my colleagues for the other tasks using Vagrant, Docker and Ansible, and 
seeing that I could not advance with the development of my attributed task, I decided to experiment with Buddy, which 
I found very easy to use. The fact that I was not dependent on my machine accelerated the debugging of other
errors and problems associated with our App. It would not be possible to successfully deploy our App if I was still
stuck with the Vagrant VMs trying to run pipelines with Jenkins. I am happy that it worked with Buddy.
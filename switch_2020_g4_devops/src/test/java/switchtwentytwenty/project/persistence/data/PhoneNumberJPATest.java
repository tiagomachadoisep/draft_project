package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PhoneNumberJPATest {

    @Test
    void getNumber() {

        //Arrange
        PhoneNumberJPA phoneNumberJPA = new PhoneNumberJPA();
        phoneNumberJPA.setNumber("912191986");

        //Act
        String result = phoneNumberJPA.getNumber();

        //Assert
        assertNotEquals("", result);

    }

    @Test
    void getPersonJPA() {

        //Arrange
        PhoneNumberJPA phoneNumberJPA = new PhoneNumberJPA();
        phoneNumberJPA.setNumber("912191986");
        phoneNumberJPA.setPersonJPA(new PersonJPA());

        //Act
        PersonJPA result = phoneNumberJPA.getPersonJPA();

        //Assert
        assertNotNull(result);

    }

}
package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.category.StandardCategory;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CategoryJPATest {

    @Test
    void comparingTwoStandardCategoriesJPA() {
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);

        assertEquals(jpa, jpa);
        assertEquals(jpa.hashCode(), jpa.hashCode());
        assertNotEquals(null, jpa);
    }

    @Test
    void comparingJPAWithStandardCategory() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA();
        jpa.setId(new CategoryID("1"));
        jpa.setName("Health");
        jpa.setParentCategory(null);
        StandardCategory standard = new StandardCategory.Builder(new CategoryID("1")).setName(new CategoryName("World"))
                .setParent(null).build();
        //ASSERT
        assertNotEquals(standard, jpa);
    }
    @Test
    void comparingTwoJPAButWithDifferentAttributes() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        CategoryJPA jpaToCompare = new CategoryJPA(new CategoryID("1.2"), "Health", "2", null);

        //ASSERT
        assertNotEquals(jpaToCompare, jpa);
        assertNotEquals(jpaToCompare.hashCode(), jpa.hashCode());
    }

    @Test
    void comparingTwoJPAButTotallyDifferent() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Animals", null, null);
        CategoryJPA jpaToCompare = new CategoryJPA(new CategoryID("1.2"), "Health", "2", new FamilyID("3"));

        //ASSERT
        assertNotEquals(jpaToCompare, jpa);
    }

    @Test
    void testingGetterAndSetterOfIDJPA() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        CategoryID idExpected = new CategoryID("3");
        jpa.setId(idExpected);
        //ASSERT
        assertEquals(idExpected,jpa.getId());
    }

    @Test
    void testingGetterAndSetterOfNameJPA() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        String nameExpected = "Travel";
        jpa.setName(nameExpected);
        //ASSERT
        assertEquals(nameExpected,jpa.getName());
    }
    @Test
    void testingGetterAndSetterOfParentIdJPA() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        String parentIdExpected = "1.3";
        jpa.setParentCategory(parentIdExpected);
        //ASSERT
        assertEquals(parentIdExpected,jpa.getParentId());
    }

    @Test
    void testingGetterAndSetterOfFamilyIdJPA() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        String familyIdExpected = "1.3";
        jpa.setFamilyId(new FamilyID(familyIdExpected));
        //ASSERT
        assertEquals(new FamilyID(familyIdExpected), jpa.getFamilyId());
    }

    @Test
    void testEqualsSameClassOneObjectNull() {
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        CategoryJPA jpaTwo = null;

        assertNotEquals(jpa, jpaTwo);
    }

    @Test
    void testEqualsDifferentClass() {
        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "Health", null, null);
        Family family = new Family.Builder(new FamilyID("20")).build();

        assertNotEquals(jpa, family);
    }
}
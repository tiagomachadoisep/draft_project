package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class EmailJPATest {

    @Test
    @DisplayName("Valid email.")
    void getEmail() {
        String expected = "lucicleide66@gmail.com";
        EmailJPA emailJPA = new EmailJPA("lucicleide66@gmail.com");
        String result = emailJPA.getEmail();
        assertEquals(expected,result);
    }

    @Test
    @DisplayName("Null email.")
    void getEmail_null() {
        String expected = null;
        EmailJPA emailJPA = new EmailJPA("lucicleide66@gmail.com");
        String result = emailJPA.getEmail();
        assertNotEquals(expected,result);
    }

    @Test
    @DisplayName("Empty email.")
    void getEmail_empty() {
        String expected = "lucicleide66@gmail.com";
        EmailJPA emailJPA = new EmailJPA("");
        String result = emailJPA.getEmail();
        assertNotEquals(expected,result);
    }

    @Test
    @DisplayName("Empty email.")
    void getEmail_noArgsConst() {
        String expected = "lucicleide66@gmail.com";
        EmailJPA emailJPA = new EmailJPA();
        String result = emailJPA.getEmail();
        assertNotEquals(expected,result);
    }

}
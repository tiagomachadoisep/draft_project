package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TransactionJPATest {

    @Test
    void getLedger() {
        TransactionJPA transactionJPA = new TransactionJPA(new LedgerJPA(1, "ze@isep.pt"), "1", "1", 10, "1",
                new TransactionDate(LocalDate.now()));
        assertEquals(transactionJPA.getLedgerJPA(), new LedgerJPA(1, "ze@isep.pt"));
    }

    @Test
    void equals() {
        TransactionJPA transactionJPA = new TransactionJPA(new LedgerJPA(1, "ze@isep.pt"), "1", "1", 10, "1",
                new TransactionDate(LocalDate.now()));
        TransactionJPA transactionJPA2 = new TransactionJPA(new LedgerJPA(1, "ze@isep.pt"), "1", "1", 10, "1",
                new TransactionDate(LocalDate.now()));
        assertEquals(transactionJPA, transactionJPA2);
        assertEquals(transactionJPA, transactionJPA);
        assertNotEquals(transactionJPA, new TransactionJPA());
        assertNotEquals("ola", transactionJPA);
    }


    @Test
    void equals_NotAnInstanceOfTransactionJPA() {

        //Arrange
        TransactionJPA transactionJPA = new TransactionJPA(new LedgerJPA(1, "ze@isep.pt"), "1", "1", 10, "1",
                new TransactionDate(LocalDate.now()));
        PersonID personID = new PersonID(new Email("carlos@hotmail.com"));

        //Act+Assert
        assertNotEquals(transactionJPA, personID);

    }

    @Test
    void hashCodeTest() {

        //Arrange
        TransactionJPA transactionJPA = new TransactionJPA(new LedgerJPA(1, "ze@isep.pt"), "1", "1", 10, "1",
                new TransactionDate(LocalDate.now()));

        //Act
        int result = transactionJPA.hashCode();

        //Assert
        assertNotEquals(0, result);

    }

    @Test
    void getDestinationID() {

        //Arrange
        LedgerJPA ledgerJPA = new LedgerJPA();
        TransactionJPA transactionJPA = new TransactionJPA(ledgerJPA, new TransactionID("1"), new AccountID("1"),
                new AccountID("2"), 100
                , new CategoryID("1"), new TransactionDate("2021-02-02"));

        //Act
        AccountID result = transactionJPA.getDestinationID();

        //Assert
        assertNotNull(result);

    }

    @Test
    void getTransactionID() {

        //Arrange
        LedgerJPA ledgerJPA = new LedgerJPA();
        TransactionJPA transactionJPA = new TransactionJPA(ledgerJPA, new TransactionID("1"), new AccountID("1"), 100
                , new CategoryID("1"), new TransactionDate("2021-02-02"));

        //Act
        TransactionID result = transactionJPA.getTransactionID();

        //Assert
        assertNotNull(result);

    }

    @Test
    void getTransactionId() {

        //Arrange
        LedgerJPA ledgerJPA = new LedgerJPA();
        TransactionJPA transactionJPA = new TransactionJPA(ledgerJPA, new TransactionID("1"), new AccountID("1"), 100
                , new CategoryID("1"), new TransactionDate("2021-02-02"));

        //Act
        TransactionID result = transactionJPA.getTransactionID();

        //Assert
        assertNotNull(result);

    }

    @Test
    void getCategoryID() {

        //Arrange
        LedgerJPA ledgerJPA = new LedgerJPA();
        TransactionJPA transactionJPA = new TransactionJPA(ledgerJPA, new TransactionID("1"), new AccountID("1"), 100
                , new CategoryID("1"), new TransactionDate("2021-02-02"));

        //Act
        CategoryID result = transactionJPA.getCategoryID();

        //Assert
        assertNotNull(result);

    }
}
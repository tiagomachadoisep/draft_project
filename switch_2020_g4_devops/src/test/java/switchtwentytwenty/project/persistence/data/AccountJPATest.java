package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.BankAccount;
import switchtwentytwenty.project.domain.shared.ids.AccountID;

import static org.junit.jupiter.api.Assertions.*;

class AccountJPATest {

    @Test
    void testEquals_ReturnsTrue_SameObject() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("123"));

        //Act
        boolean result = accountJPA.equals(accountJPA);

        //Assert
        assertTrue(result);
        assertEquals(accountJPA.hashCode(), accountJPA.hashCode());

    }

    @Test
    void testEquals_ReturnsTrue() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("123"));
        AccountJPA accountJPA2 = new AccountJPA();
        accountJPA2.setAccountID(new AccountID("123"));

        //Act
        boolean result = accountJPA.equals(accountJPA2);

        //Assert
        assertTrue(result);
        assertEquals(accountJPA.hashCode(), accountJPA2.hashCode());

    }

    @Test
    void testEquals_ReturnsFalse_DifferentAccountId() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("123"));
        AccountJPA accountJPA2 = new AccountJPA();
        accountJPA2.setAccountID(new AccountID("321"));

        //Act
        boolean result = accountJPA.equals(accountJPA2);

        //Assert
        assertFalse(result);
        assertNotEquals(accountJPA.hashCode(), accountJPA2.hashCode());

    }

    @Test
    void testEquals_ReturnsFalse_ValueIsNull() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("123"));

        //Act
        boolean result = accountJPA.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void testEquals_ReturnsFalse_DifferentObject() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("123"));
        Account account = new BankAccount.Builder(new AccountID("123")).build();

        //Act
        boolean result = accountJPA.equals(account);

        //Assert
        assertFalse(result);

    }

}
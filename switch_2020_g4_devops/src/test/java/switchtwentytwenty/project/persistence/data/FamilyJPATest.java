package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyJPATest {


    @Test
    void addFamilyRelationship_successCase() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "dsfds23";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "brother";

        FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID,familyJPA,personOneID,
                personTwoID,designation);

        List<FamilyRelationshipJPA> expected = new ArrayList<>();
        expected.add(familyRelationshipJPA);

        boolean result = familyJPA.addFamilyRelationship(familyRelationshipID,personOneID,personTwoID,designation);

        assertTrue(result);
        assertArrayEquals(expected.toArray(), familyJPA.getFamilyRelationshipJPAList().toArray());

    }

    @Test
    void addFamilyRelationship_failCase_NullPersonOneIdString() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "dsfds23";
        String personOneID = null;
        String personTwoID = "two@gmail.com";
        String designation = "brother";

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyJPA.addFamilyRelationship(familyRelationshipID,personOneID,personTwoID,designation);
        });

    }

    @Test
    void addFamilyRelationship_failCase_NullPersonTwoIdString() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "dsfds23";
        String personOneID = "one@gmail.com";
        String personTwoID = null;
        String designation = "brother";

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyJPA.addFamilyRelationship(familyRelationshipID,personOneID,personTwoID,designation);
        });

    }

    @Test
    void addFamilyRelationship_failCase_NullDesignationString() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "dsfds23";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyJPA.addFamilyRelationship(familyRelationshipID,personOneID,personTwoID,designation);
        });

    }

    @Test
    void addFamilyRelationship_failCase_NullRelationshipIdString() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = null;
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "brother";

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyJPA.addFamilyRelationship(familyRelationshipID, personOneID, personTwoID, designation);
        });

    }

    @Test
    void EqualsAndHashcode() {

        FamilyID familyID = new FamilyID("123");

        FamilyJPA familyJPA = new FamilyJPA();
        familyJPA.setFamilyID(familyID);

        FamilyJPA sameID = new FamilyJPA();
        sameID.setFamilyID(familyID);

        FamilyJPA differentID = new FamilyJPA();
        differentID.setFamilyID(new FamilyID("456"));

        FamilyJPA nullFamilyJPA = null;


        assertEquals(familyJPA, familyJPA);
        assertEquals(familyJPA.hashCode(), familyJPA.hashCode());

        assertEquals(familyJPA, sameID);
        assertEquals(familyJPA.hashCode(), sameID.hashCode());

        assertNotEquals(familyJPA, differentID);
        assertNotEquals(familyJPA.hashCode(), differentID.hashCode());

        assertNotEquals(familyJPA, familyID);
        assertNotEquals(familyJPA.hashCode(), familyID.hashCode());

        assertNotEquals(familyJPA, nullFamilyJPA);
    }

}
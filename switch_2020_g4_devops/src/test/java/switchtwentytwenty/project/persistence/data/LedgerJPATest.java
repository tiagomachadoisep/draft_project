package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class LedgerJPATest {

    @Test
    void testEqualsSameObject() {
        LedgerJPA ledgerJPA = new LedgerJPA(20, "luis@isep.pt");

        assertEquals(ledgerJPA, ledgerJPA);
        assertEquals(ledgerJPA.hashCode(), ledgerJPA.hashCode());
    }


    @Test
    void testEqualsDifferentObjectSameArguments() {
        LedgerJPA ledgerJPA = new LedgerJPA(20, "luis@isep.pt");
        LedgerJPA ledgerJPATwo = new LedgerJPA(20, "luis@isep.pt");

        assertEquals(ledgerJPA, ledgerJPATwo);
        assertEquals(ledgerJPA.hashCode(), ledgerJPATwo.hashCode());
    }

    @Test
    void testEqualsDifferentClass() {
        LedgerJPA ledgerJPA = new LedgerJPA(20, "luis@isep.pt");
        Family family = new Family.Builder(new FamilyID("20")).build();

        assertNotEquals(ledgerJPA, family);
        assertNotEquals(ledgerJPA.hashCode(), family.hashCode());
    }

    @Test
    void testEqualsSameClassDifferentArguments() {
        LedgerJPA ledgerJPA = new LedgerJPA(20, "luis@isep.pt");
        LedgerJPA ledgerJPATwo = new LedgerJPA(14, "luis@isep.pt");

        assertNotEquals(ledgerJPA, ledgerJPATwo);
        assertNotEquals(ledgerJPA.hashCode(), ledgerJPATwo.hashCode());
    }

    @Test
    void testEqualsSameClassOneNullObject() {
        LedgerJPA ledgerJPA = new LedgerJPA(20, "luis@isep.pt");
        LedgerJPA ledgerJPATwo = null;

        assertNotEquals(ledgerJPA, ledgerJPATwo);
    }

  /*  @Test
    void testToString(){
        LedgerJPA ledgerJPA = new LedgerJPA(20, "luis@isep.pt");
        String expected = "ledgerID=20, personID=luis@isep.pt";
        String result = ledgerJPA.toString();

        assertEquals(expected,result);
    }
*/

}
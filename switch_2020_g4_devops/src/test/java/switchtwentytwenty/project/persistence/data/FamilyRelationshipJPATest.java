package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class FamilyRelationshipJPATest {


    @Test
    void familyRelationshipJPAConstructor_successCase(){

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "brother";

        FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                personTwoID, designation);

        assertNotNull(familyRelationshipJPA);
        assertEquals(familyRelationshipJPA.getFamilyRelationshipID(), familyRelationshipID);
        assertEquals(familyRelationshipJPA.getDesignation(), designation);
        assertEquals(familyRelationshipJPA.getPersonOneID(), personOneID);
        assertEquals(familyRelationshipJPA.getPersonTwoID(), personTwoID);
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_NullFamilyJPA(){

        FamilyJPA familyJPA = null;
        String familyRelationshipID = "hudsu2";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "brother";


        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID,familyJPA,personOneID,
                    personTwoID,designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_NullPersonOneID(){

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = null;
        String personTwoID = "two@gmail.com";
        String designation = "brother";


        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_EmptyPersonOneID() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = "";
        String personTwoID = "two@gmail.com";
        String designation = "brother";


        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_NullPersonTwoID() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = "one@gmail.com";
        String personTwoID = null;
        String designation = "brother";


        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_EmptyPersonTwoID() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = "one@gmail.com";
        String personTwoID = "";
        String designation = "brother";


        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_NullDesignation() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = null;


        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_EmptyDesignation() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "hudsu2";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "";


        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_NullFamilyRelationshipID() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = null;
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "brother";

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void familyRelationshipJPAConstructor_failCase_EmptyFamilyRelationshipID() {

        FamilyJPA familyJPA = new FamilyJPA();
        String familyRelationshipID = "";
        String personOneID = "one@gmail.com";
        String personTwoID = "two@gmail.com";
        String designation = "brother";

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, familyJPA, personOneID,
                    personTwoID, designation);
        });
    }

    @Test
    void EqualsAndHashcode() {
        FamilyJPA familyJPA = new FamilyJPA();
        familyJPA.setFamilyID(new FamilyID("familyId123"));

        FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(
                "id123", familyJPA, "one@gmail.com",
                "two@gmail.com", "sister");

        FamilyRelationshipJPA otherFamilyRelationshipJPA = new FamilyRelationshipJPA(
                "id123", familyJPA, "one@gmail.com",
                "two@gmail.com", "sister");

        FamilyRelationshipJPA differentID = new FamilyRelationshipJPA(
                "id456", familyJPA, "one@gmail.com",
                "two@gmail.com", "sister");

        FamilyRelationshipJPA nullRelationship = null;

        assertEquals(familyRelationshipJPA, familyRelationshipJPA);
        assertEquals(familyRelationshipJPA.hashCode(), familyRelationshipJPA.hashCode());

        assertEquals(familyRelationshipJPA, otherFamilyRelationshipJPA);
        assertEquals(familyRelationshipJPA.hashCode(), otherFamilyRelationshipJPA.hashCode());

        assertNotEquals(familyRelationshipJPA, differentID);
        assertNotEquals(familyRelationshipJPA.hashCode(), differentID.hashCode());

        assertNotEquals(familyRelationshipJPA, familyJPA);
        assertNotEquals(familyRelationshipJPA.hashCode(), familyJPA.hashCode());

        assertNotEquals(familyRelationshipJPA, nullRelationship);

    }

    @Test
    void EqualsAndHashcodeRelationshipIdJpa() {

        FamilyJPA familyJPA = new FamilyJPA();
        familyJPA.setFamilyID(new FamilyID("familyId123"));

        FamilyJPA differentJPA = new FamilyJPA();
        familyJPA.setFamilyID(new FamilyID("familyId456"));

        FamilyRelationshipJPA.FamilyRelationshipIdJPA familyRelationshipIdJPA =
                new FamilyRelationshipJPA.FamilyRelationshipIdJPA("relId123", familyJPA);

        FamilyRelationshipJPA.FamilyRelationshipIdJPA otherRelationshipIdJPA =
                new FamilyRelationshipJPA.FamilyRelationshipIdJPA("relId123", familyJPA);

        FamilyRelationshipJPA.FamilyRelationshipIdJPA differentIdAndFamilyJPA =
                new FamilyRelationshipJPA.FamilyRelationshipIdJPA("relId456", differentJPA);

        FamilyRelationshipJPA.FamilyRelationshipIdJPA differentId =
                new FamilyRelationshipJPA.FamilyRelationshipIdJPA("relId456", familyJPA);

        FamilyRelationshipJPA.FamilyRelationshipIdJPA differentFamilyJPA =
                new FamilyRelationshipJPA.FamilyRelationshipIdJPA("relId123", differentJPA);

        FamilyRelationshipJPA.FamilyRelationshipIdJPA nullIdJPA = null;

        assertEquals(familyRelationshipIdJPA, familyRelationshipIdJPA);
        assertEquals(familyRelationshipIdJPA.hashCode(), familyRelationshipIdJPA.hashCode());

        assertEquals(familyRelationshipIdJPA, otherRelationshipIdJPA);
        assertEquals(familyRelationshipIdJPA.hashCode(), otherRelationshipIdJPA.hashCode());

        assertNotEquals(familyRelationshipIdJPA, differentIdAndFamilyJPA);
        assertNotEquals(familyRelationshipIdJPA.hashCode(), differentIdAndFamilyJPA.hashCode());

        assertNotEquals(familyRelationshipIdJPA, differentId);
        assertNotEquals(familyRelationshipIdJPA.hashCode(), differentId.hashCode());

        assertNotEquals(familyRelationshipIdJPA, differentFamilyJPA);
        assertNotEquals(familyRelationshipIdJPA.hashCode(), differentFamilyJPA.hashCode());

        assertNotEquals(familyRelationshipIdJPA, familyJPA);
        assertNotEquals(familyRelationshipIdJPA.hashCode(), familyJPA.hashCode());

        assertNotEquals(familyRelationshipIdJPA, nullIdJPA);
    }

}
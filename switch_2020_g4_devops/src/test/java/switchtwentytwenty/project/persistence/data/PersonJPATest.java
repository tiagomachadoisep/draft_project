package switchtwentytwenty.project.persistence.data;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonJPATest {

    @Test
    void getVAt() {
        int expected = 123456789;
        PersonJPA jpa = new PersonJPA();
        jpa.setVat(expected);

        //ACT
        int result = jpa.getVat();

        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void getOtherEmails() {
        EmailJPA emailOne = new EmailJPA("switch@isep.pt");
        EmailJPA emailTwo = new EmailJPA("work@inestec.pt");

        List<EmailJPA> otherEmails = new ArrayList<>();
        otherEmails.add(emailOne);
        otherEmails.add(emailTwo);

        PersonJPA jpa = new PersonJPA();
        jpa.setOtherEmails(otherEmails);

        //ACT
        List<EmailJPA> result = jpa.getOtherEmails();

        //ASSERT
        assertEquals(otherEmails, result);
    }

}
package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.EmailJPA;
import switchtwentytwenty.project.persistence.data.PersonJPA;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonAssemblerJPATest {

    @Test
    void toJPATest() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt")))
                .setName(new PersonName("luis"))
                .setAddress(new Address("Rua tal"))
                .setFamilyId(new FamilyID("3"))
                .setVat(new VATNumber(266301851))
                .setBirthDate(null).build();

        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        PersonJPA personJPA = personAssemblerJPA.toJPA(person);

        assertNotNull(personJPA);
        assertNotEquals(0, person.getVatNumber());

    }

    @Test
    void toJPATestAgain() {
        List<Email> emails = new ArrayList<>();
        emails.add(new Email("sarovski@isep.pt"));
        emails.add(new Email("wow@yo.pt"));

        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt")))
                .setAddress(new Address("Rua tal"))
                .setFamilyId(new FamilyID("3"))
                .setName(new PersonName("luis"))
                .setVat(new VATNumber(266301851))
                .setBirthDate(new BirthDate("17-11-1988"))
                .setOtherEmails(emails)
                .setRole(ERole.ADMIN).build();


        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        PersonJPA personJPA = personAssemblerJPA.toJPA(person);

        assertNotNull(personJPA);
    }

    @Test
    void toJPATestWithoutAddress() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt")))
                .setName(new PersonName("luis"))
                .setFamilyId(new FamilyID("3"))
                .setVat(new VATNumber(0))
                .setBirthDate(null).build();

        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        PersonJPA personJPA = personAssemblerJPA.toJPA(person);

        assertNotNull(personJPA);

    }

    @Test
    void toJPATestWithoutBirthDate() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt")))
                .setName(new PersonName("luis"))
                .setFamilyId(new FamilyID("3"))
                .setVat(new VATNumber(0))
                .setAddress(null).build();

        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        PersonJPA personJPA = personAssemblerJPA.toJPA(person);

        assertNotNull(personJPA);

    }

    @Test
    void assemblingEmails() {
        //Arrange
        EmailJPA emailFromWork = new EmailJPA("work@count.pt");
        EmailJPA emailFromHome = new EmailJPA("home@sapo.pt");
        List<EmailJPA> otherEmails = new ArrayList<>();
        otherEmails.add(emailFromWork);
        otherEmails.add(emailFromHome);

        PersonJPA jpa = new PersonJPA();
        jpa.setPersonID(new PersonID(new Email("gomgom@one.piece.pt")));
        jpa.setName("Marta");
        jpa.setOtherEmails(otherEmails);

        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        //Act
        List<Email> emails = personAssemblerJPA.assembleEmail(jpa);
        //Assert
        assertNotNull(emails);
        assertNotEquals(new ArrayList<>(), emails);
    }

    @Test
    void assembleAddress() {
        Address expected = new Address("Rua tal");

        PersonJPA person = new PersonJPA();
        person.setAddress("Rua tal");
        person.setPersonID(new PersonID(new Email("luis@isep.pt")));

        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        //ACT
        Address result = personAssemblerJPA.assembleAddress(person);

        //ASSERT
        assertEquals(expected.toString(), result.toString());

    }

    @Test
    void assembleBirthDate() {
        BirthDate expected = new BirthDate("28-01-2001");

        PersonJPA person = new PersonJPA();
        person.setAddress("Rua tal");
        person.setBirthDate("28-01-2001");
        person.setPersonID(new PersonID(new Email("luis@isep.pt")));

        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();

        //ACT
        BirthDate result = personAssemblerJPA.assembleBirthDate(person);

        //ASSERT
        assertEquals(expected.toString(), result.toString());

    }

}
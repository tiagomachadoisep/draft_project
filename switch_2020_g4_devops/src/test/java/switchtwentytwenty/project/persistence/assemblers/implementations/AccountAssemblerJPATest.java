package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.CreditCardAccount;
import switchtwentytwenty.project.domain.aggregates.account.PersonalCashAccount;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.AccountJPA;

import static org.junit.jupiter.api.Assertions.*;

class AccountAssemblerJPATest {

    //  private final IAccountFactory iAccountFactory = Mockito.mock(IAccountFactory.class);
    // private final AccountAssemblerJPA assemblerJPA = new AccountAssemblerJPA();

    @Test
    void constructorAccountAssembleJPA() {
        AccountAssemblerJPA assemblerJPATwo = new AccountAssemblerJPA();

        assertNotNull(assemblerJPATwo);
    }

    @Test
    void toJPATest() {
        AccountAssemblerJPA accountAssemblerJPA = new AccountAssemblerJPA();
        Account account = new CreditCardAccount.Builder(new AccountID("1"))
                .setOwnerID(new PersonID(new Email("luis@isep.pt")))
                .setDescription(new Description("newAccount"))
                .setBalance(new MoneyValue(200)).setAccountType().build();

        AccountJPA accountJPA = accountAssemblerJPA.toJPA(account);

        assertNotNull(accountJPA);

    }

    @Test
    void toJPA_NullEntity() {

        //Arrange
        AccountAssemblerJPA assemblerJPA = new AccountAssemblerJPA();
        Account entity = null;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> {
            assemblerJPA.toJPA(entity);
        });

    }

    @Test
    void toJPA_NotNullEntity() {

        //Arrange
        AccountAssemblerJPA assemblerJPA = new AccountAssemblerJPA();
        AccountID accountID = new AccountID("123");
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        MoneyValue balance = new MoneyValue(1000);
        Description description = new Description("Cash account");
        Account entity =
                new PersonalCashAccount.Builder(accountID).setAccountType().setBalance(balance).setDescription(description).setOwnerID(personID).build();

        AccountJPA expected = new AccountJPA();
        expected.setAccountID(accountID);
        expected.setAccountType(EAccountType.PERSONAL_CASH);
        expected.setBalance(balance.getValue());
        expected.setDescription(description.toString());
        expected.setPersonID(personID);

        //Act
        AccountJPA result = assemblerJPA.toJPA(entity);

        //Assert
        assertEquals(expected, result);

    }

}
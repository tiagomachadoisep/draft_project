package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.category.CustomCategory;
import switchtwentytwenty.project.domain.aggregates.category.StandardCategory;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ICategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoryAssemblerJPATest {

    ICategoryAssemblerJPA assemblerJPA = new CategoryAssemblerJPA();


    @Test
    void transformsStandardCategoryToACategoryJPA() {
        //ARRANGE
        CategoryJPA expectedCategoryJPA = new CategoryJPA();
        expectedCategoryJPA.setName("Games");
        expectedCategoryJPA.setId(new CategoryID("1.2"));
        expectedCategoryJPA.setParentCategory("1");
        expectedCategoryJPA.setFamilyId(null);

        StandardCategory category = new StandardCategory.Builder(new CategoryID("1.2"))
                .setName(new CategoryName("Games"))
                .setParent(new CategoryID("1")).build();

        //ACT
        CategoryJPA actualCategoryJPA = assemblerJPA.toJPA(category);

        //ASSERT
        assertEquals(expectedCategoryJPA, actualCategoryJPA);
    }

    @Test
    void transformsCustomCategoryToACategoryJPA() {
        //ARRANGE
        CategoryJPA expectedCategoryJPA = new CategoryJPA();
        expectedCategoryJPA.setName("Games");
        expectedCategoryJPA.setId(new CategoryID("1.2"));
        expectedCategoryJPA.setParentCategory("1");
        expectedCategoryJPA.setFamilyId(new FamilyID("1"));

        CustomCategory category = new CustomCategory.Builder(new CategoryID("1.2"))
                .setName(new CategoryName("Games"))
                .setParent(new CategoryID("1")).setFamily(new FamilyID("1")).build();

        //ACT
        CategoryJPA actualCategoryJPA = assemblerJPA.toJPA(category);

        //ASSERT
        assertEquals(expectedCategoryJPA, actualCategoryJPA);
    }

    @Test
    void transformsStandardCategoryToAStandardCategoryJPAWithNullParent() {
        //ARRANGE
        CategoryJPA expectedCategoryJPA = new CategoryJPA();
        expectedCategoryJPA.setName("Games");
        expectedCategoryJPA.setId(new CategoryID("1.2"));
        expectedCategoryJPA.setParentCategory(null);

        StandardCategory category = new StandardCategory.Builder(new CategoryID("1.2"))
                .setName(new CategoryName("Games"))
                .setParent(null).build();

        //ACT
        CategoryJPA actualCategoryJPA = assemblerJPA.toJPA(category);

        //ASSERT
        assertEquals(expectedCategoryJPA, actualCategoryJPA);
    }

    @Test
    void transformsCustomCategoryToACategoryJPAWithNullParent() {
        //ARRANGE
        CategoryJPA expectedCategoryJPA = new CategoryJPA();
        expectedCategoryJPA.setName("Games");
        expectedCategoryJPA.setId(new CategoryID("1.2"));
        expectedCategoryJPA.setParentCategory(null);
        expectedCategoryJPA.setFamilyId(new FamilyID("1"));

        CustomCategory category = new CustomCategory.Builder(new CategoryID("1.2"))
                .setName(new CategoryName("Games"))
                .setParent(null).setFamily(new FamilyID("1")).build();

        //ACT
        CategoryJPA actualCategoryJPA = assemblerJPA.toJPA(category);

        //ASSERT
        assertEquals(expectedCategoryJPA, actualCategoryJPA);
    }

    @Test
    void toCategoryName() {
        //ARRANGE
        String name = "Hotel";
        CategoryName expected = new CategoryName(name);

        //ACT
        CategoryName actual = assemblerJPA.toCategoryName(name);

        //ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void toStandardsubcategory() {
        //ARRANGE
        String id = "19jnf";
        CategoryID expected = new CategoryID(id);

        //ACT
        CategoryID actual = assemblerJPA.toCategoryID(id);

        //ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void toFamilyID() {
        //ARRANGE
        String id = "19jnf";
        FamilyID expected = new FamilyID(id);

        //ACT
        FamilyID actual = assemblerJPA.toFamilyID(id);

        //ASSERT
        assertEquals(expected, actual);
    }


    @Test
    void toJPA_CustomSubCategory() {

        //Arrange
        CategoryJPA categoryJPA = new CategoryJPA();
        CategoryAssemblerJPA categoryAssemblerJPA = new CategoryAssemblerJPA();

        CategoryID categoryID = new CategoryID("sfkd85posa1");
        FamilyID familyID = new FamilyID("hof0er39382");
        CategoryName categoryName = new CategoryName("Wine");
        CategoryID parentCategoryID = new CategoryID("sdf45aefghj");

        CustomCategory customCategory =
                new CustomCategory.Builder(categoryID).setFamily(familyID).setName(categoryName).setParent(parentCategoryID).build();

        CategoryJPA expected = new CategoryJPA();
        expected.setFamilyId(familyID);
        expected.setId(categoryID);
        expected.setName(categoryName.toString());
        expected.setParentCategory(parentCategoryID.toString());

        //Act
        CategoryJPA result = categoryAssemblerJPA.toJPA(customCategory);

        //Assert
        assertEquals(expected.getFamilyId(), result.getFamilyId());
        assertEquals(expected.getId(), result.getId());
        assertEquals(expected.getName(), result.getName());
        assertEquals(expected.getParentId(), result.getParentId());

    }

    @Test
    void toJPA_StandardCategory() {

        //Arrange
        CategoryJPA categoryJPA = new CategoryJPA();
        CategoryAssemblerJPA categoryAssemblerJPA = new CategoryAssemblerJPA();

        CategoryID categoryID = new CategoryID("sfkd85posa1");
        CategoryName categoryName = new CategoryName("Wine");

        CustomCategory standardCategory =
                new CustomCategory.Builder(categoryID).setFamily(null).setName(categoryName).setParent(null).build();

        CategoryJPA expected = new CategoryJPA();
        expected.setFamilyId(null);
        expected.setId(categoryID);
        expected.setName(categoryName.toString());
        expected.setParentCategory(null);

        //Act
        CategoryJPA result = categoryAssemblerJPA.toJPA(standardCategory);

        //Assert
        assertEquals(expected.getFamilyId(), result.getFamilyId());
        assertEquals(expected.getId(), result.getId());
        assertEquals(expected.getName(), result.getName());
        assertEquals(expected.getParentId(), result.getParentId());

    }

}
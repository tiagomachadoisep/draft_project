package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.persistence.data.FamilyJPA;
import switchtwentytwenty.project.persistence.data.FamilyRelationshipJPA;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyAssemblerJPATest {

    private FamilyAssemblerJPA familyAssemblerJPA = new FamilyAssemblerJPA();

    @Test
    void toJPA_successCase() {

        String date = LocalDate.now().toString();


        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate(date))
                .build();
        family.createRelationship(new PersonID(new Email("one@gmail.com")), new PersonID(new Email("two@gmail.com")), new FamilyRelationType("brother")
                , new FamilyRelationshipID("34h3e"));


        List<FamilyRelationshipJPA> familyRelationshipJPAList = new ArrayList<>();

        FamilyJPA expected = new FamilyJPA(new FamilyID("1"), new PersonID(new Email("admin@gmail.com")),
                "Santos", date, familyRelationshipJPAList);

        expected.addFamilyRelationship("34h3e", "one@gmail.com", "two@gmail.com",
                "brother");

        FamilyJPA result = familyAssemblerJPA.toJPA(family);

        assertEquals(expected, result);
        assertNotSame(expected, result);
        assertNotNull(result.getFamilyName());
        assertNotNull(result.getAdminEmail());
        assertNotNull(result.getRegistrationDate());

    }

    @Test
    void toJPA_failCase_nullArgument() {

        Family family = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyJPA result = familyAssemblerJPA.toJPA(family);
        });
    }


    @Test
    void toFamilyName_successCase(){

        String name = "Santos";

        FamilyName expected = new FamilyName(name);

        FamilyName result = familyAssemblerJPA.toFamilyName(name);

        assertEquals(expected, result);

    }

    @Test
    void toFamilyName_failCase_emptyName(){

        String name = "";

        Assertions.assertThrows(Exception.class, () -> {
            FamilyName result = familyAssemblerJPA.toFamilyName(name);
        });

    }

    @Test
    void toRegistrationDate_successCase(){

        String date = "25-04-2003";

        RegistrationDate expected = new RegistrationDate(date);

        RegistrationDate result = familyAssemblerJPA.toRegistrationDate(date);

        assertEquals(expected, result);

    }

    @Test
    void toRegistrationDate_failCase_emptyDate(){

        String date = "";

        Assertions.assertThrows(Exception.class, () -> {
            RegistrationDate result = familyAssemblerJPA.toRegistrationDate(date);
        });

    }

    @Test
    void toPersonID_successCase(){

        String personEmail = "admin@gmail.com";

        PersonID expected = new PersonID(new Email(personEmail));

        PersonID result = familyAssemblerJPA.toPersonID(personEmail);

        assertEquals(expected, result);

    }

    @Test
    void toPersonID_failCase_emptyEmail(){

        String personEmail = "";

        Assertions.assertThrows(Exception.class, () -> {
            PersonID result = familyAssemblerJPA.toPersonID(personEmail);
        });

    }

    @Test
    void toFamilyRelationType_successCase(){

        String designation = "sister";

        FamilyRelationType expected = new FamilyRelationType(designation);

        FamilyRelationType result = familyAssemblerJPA.toFamilyRelationType(designation);

        assertEquals(expected, result);

    }

    @Test
    void toFamilyRelationType_failCase_emptyEmail(){

        String designation = "";

        Assertions.assertThrows(Exception.class, () -> {
            FamilyRelationType result = familyAssemblerJPA.toFamilyRelationType(designation);
        });

    }

    @Test
    void toFamilyRelationshipID_successCase(){

        String relationId = "wewae-2321";

        FamilyRelationshipID expected = new FamilyRelationshipID(relationId);

        FamilyRelationshipID result = familyAssemblerJPA.toFamilyRelationshipID(relationId);

        assertEquals(expected, result);

    }

    @Test
    void toFamilyRelationshipID_failCase_emptyId(){

        String relationId = "";


        Assertions.assertThrows(Exception.class, () -> {
            FamilyRelationshipID result = familyAssemblerJPA.toFamilyRelationshipID(relationId);
        });

    }


}
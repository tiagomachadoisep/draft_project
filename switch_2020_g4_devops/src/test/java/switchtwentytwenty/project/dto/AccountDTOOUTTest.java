package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.CheckAccountBalanceController;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class AccountDTOOUTTest {

    @Test
    void testEqualsSameObject() {
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT("first");

        assertEquals(accountDTOOUT, accountDTOOUT);
        assertEquals(accountDTOOUT.hashCode(), accountDTOOUT.hashCode());
    }

    @Test
    void testEqualsDifferentObject() {
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT("first");
        AccountDTOOUT difAccountDTOOUT = new AccountDTOOUT("different");

        assertNotEquals(accountDTOOUT, difAccountDTOOUT);
        assertNotEquals(accountDTOOUT.hashCode(), difAccountDTOOUT.hashCode());
    }


    @Test
    void testEqualsDifferentClassObject() {
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT("first");
        Family family = new Family.Builder(new FamilyID("20")).build();

        assertNotEquals(accountDTOOUT, family);
        assertNotEquals(accountDTOOUT.hashCode(), family.hashCode());
    }

    @Test
    void testEqualsSameClassNull() {
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT("first");
        AccountDTOOUT accountDTOOUTTwo = null;
        assertNotEquals(accountDTOOUT, accountDTOOUTTwo);
    }

    @Test
    void testEqualsDifferentObjectsSameArguments() {
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT("first");
        AccountDTOOUT difAccountDTOOUT = new AccountDTOOUT("first");

        assertEquals(accountDTOOUT, difAccountDTOOUT);
        assertEquals(accountDTOOUT.hashCode(), difAccountDTOOUT.hashCode());
    }

    @Test
    void ifDescriptionIsNull_throwException() {

        assertThrows(NullPointerException.class, () -> {
            AccountDTOOUT accountDTOOUT = new AccountDTOOUT(null);
        });
    }

    @Test
    void ifDescriptionIsEmpty_throwException() {

        assertThrows(EmptyArgumentException.class, () -> {
            AccountDTOOUT accountDTOOUT = new AccountDTOOUT("");
        });
    }

    @Test
    void testEqualsSuper() {
        AccountDTO accountDTO = new AccountDTO();
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT("first");
        accountDTOOUT.add(linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountByID(accountDTO.getAccountId())).withRel("New Account Info"));

        AccountDTOOUT accountDTOOUTSecond = new AccountDTOOUT("first");

        assertNotEquals(accountDTOOUT,accountDTOOUTSecond);
    }

    @Test
    void testGetDescription(){
        String description = "howl";
        AccountDTOOUT accountDTOOUT = new AccountDTOOUT(description);

        assertEquals(description,accountDTOOUT.getDescription());
    }


}
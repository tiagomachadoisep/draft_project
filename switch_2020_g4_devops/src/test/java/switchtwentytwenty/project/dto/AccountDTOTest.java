package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class AccountDTOTest {

    @Test
    void equals_AllParametersAreEqual() {

        //Arrange
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO_1 = new AccountDTO(ownerID, description, accountType);
        AccountDTO accountDTO_2 = new AccountDTO(ownerID, description, accountType);

        //Act
        boolean result = accountDTO_1.equals(accountDTO_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void equals_AccountIDIsDifferent() {

        //Arrange
        String accountID_1 = "sdfw2d";
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO_1 = new AccountDTO(ownerID, description, accountType);
        accountDTO_1.setAccountId(accountID_1);
        AccountDTO accountDTO_2 = new AccountDTO(ownerID, description, accountType);

        //Act
        boolean result = accountDTO_1.equals(accountDTO_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_OwnerIDIsDifferent() {

        //Arrange
        String description = "Bank account";
        String ownerID_1 = "carla@hotmail.com";
        String ownerID_2 = "carlos@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO_1 = new AccountDTO(ownerID_1, description, accountType);
        AccountDTO accountDTO_2 = new AccountDTO(ownerID_2, description, accountType);

        //Act
        boolean result = accountDTO_1.equals(accountDTO_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_AccountTypeIsDifferent() {

        //Arrange
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType_1 = EAccountType.BANK;
        EAccountType accountType_2 = EAccountType.BANK_SAVINGS;

        AccountDTO accountDTO_1 = new AccountDTO(ownerID, description, accountType_1);
        AccountDTO accountDTO_2 = new AccountDTO(ownerID, description, accountType_2);

        //Act
        boolean result = accountDTO_1.equals(accountDTO_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_NullObject() {

        //Arrange
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO = new AccountDTO(ownerID, description, accountType);

        //Act
        boolean result = accountDTO.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_TheSameObject() {

        //Arrange
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO = new AccountDTO(ownerID, description, accountType);

        //Act
        boolean result = accountDTO.equals(accountDTO);

        //Assert
        assertTrue(result);

    }

    @Test
    void equals_ObjectOfDifferentClass() {

        //Arrange
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType = EAccountType.BANK;
        FamilyID familyID = new FamilyID("15");

        AccountDTO accountDTO = new AccountDTO(ownerID, description, accountType);
        Family family = new Family.Builder(familyID).build();

        //Act
        boolean result = accountDTO.equals(family);

        //Assert
        assertFalse(result);

    }

    @Test
    void hashCode_ReturnsTrue() {

        //Arrange
        String description = "Bank account";
        String ownerID = "carla@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO_1 = new AccountDTO(ownerID, description, accountType);
        AccountDTO accountDTO_2 = new AccountDTO(ownerID, description, accountType);

        //Act
        int result_1 = accountDTO_1.hashCode();
        int result_2 = accountDTO_2.hashCode();

        //Assert
        assertEquals(result_1, result_2);

    }

    @Test
    void hashCode_ReturnsFalse() {

        //Arrange
        String description = "Bank account";
        String ownerID_1 = "carla@hotmail.com";
        String ownerID_2 = "carlos@hotmail.com";
        EAccountType accountType = EAccountType.BANK;

        AccountDTO accountDTO_1 = new AccountDTO(ownerID_1, description, accountType);
        AccountDTO accountDTO_2 = new AccountDTO(ownerID_2, description, accountType);

        //Act
        int result_1 = accountDTO_1.hashCode();
        int result_2 = accountDTO_2.hashCode();

        //Assert
        assertNotEquals(result_1, result_2);

    }

}
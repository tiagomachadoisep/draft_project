package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.BankAccount;
import switchtwentytwenty.project.domain.aggregates.account.CreditCardAccount;
import switchtwentytwenty.project.domain.aggregates.family.FamilyRelationship;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.aggregates.ledger.Transfer;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AssemblerToDTOTest {

    private final AssemblerToDTO assemblerToDTO = new AssemblerToDTO();

    @Test
    @DisplayName("Success - String to EmailDTO")
    void toEmailDTO_success() {
        String mainEmail = "jacinto@gmail.com";
        String otherEmail = "jaja123@hotmail.com";

        EmailDTO expected = new EmailDTO(otherEmail, mainEmail);
        EmailDTO result = assemblerToDTO.toEmailDTO("jaja123@hotmail.com", "jacinto@gmail.com");

        assertEquals(expected, result);
    }

    @Test
    void toRelationshipDTO() {
        String firstPersonEmail = "first@gmail.com";
        String secondPersonEmail = "second@gmail.com";
        String familyId = "id123";
        String description = "sister";

        RelationshipDTO expectedRelationshipDTO = new RelationshipDTO();
        expectedRelationshipDTO.setFamilyId(familyId);
        expectedRelationshipDTO.setDesignation(description);
        expectedRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        expectedRelationshipDTO.setSecondPersonEmail(secondPersonEmail);

        RelationshipDTO resultDTO = assemblerToDTO.toRelationshipDto(
                firstPersonEmail, secondPersonEmail, familyId, description
        );

        assertEquals(expectedRelationshipDTO, resultDTO);
    }

    @Test
    void inputToRelationshipDTO() {
        String familyId = "id123";
        String relationId = "relationId123";
        String newDesignation = "brother";

        RelationshipDTO expected = new RelationshipDTO();
        expected.setFamilyId(familyId);
        expected.setRelationshipId(relationId);
        expected.setDesignation(newDesignation);

        RelationshipDTO result = assemblerToDTO.inputToRelationshipDTO(
                familyId, relationId, newDesignation
        );

        assertEquals(expected, result);
    }

    @Test
    void toPersonID_successCase() {

        String personEmail = "email@gmail.com";

        PersonID expected = new PersonID(new Email(personEmail));

        PersonID result = assemblerToDTO.toPersonID(personEmail);

        assertEquals(expected, result);

    }

    @Test
    void toPersonID_failCase_invalidEmail() {

        String personEmail = "email@@gmail.com";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            PersonID result = assemblerToDTO.toPersonID(personEmail);
        });

    }

    @Test
    void toFamilyID_successCase() {

        String familyId = "id123";

        FamilyID expected = new FamilyID(familyId);

        FamilyID result = assemblerToDTO.toFamilyID(familyId);

        assertEquals(expected, result);

    }

    //TODO:Pedro. Add test to .toFamilyID when the FamilyID instantiation throws an exception.

    @Test
    void toFamilyRelationshipID_successCase() {

        String relationshipId = "relationId_321";

        FamilyRelationshipID expected = new FamilyRelationshipID(relationshipId);

        FamilyRelationshipID result = assemblerToDTO.toFamilyRelationshipID(relationshipId);

        assertEquals(expected, result);
    }

    @Test
    void toFamilyRelationshipID_failCase_invalidRelationshipId() {

        String relationshipId = "";

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationshipID result = assemblerToDTO.toFamilyRelationshipID(relationshipId);
        });
    }

    @Test
    void toFamilyRelationType_successCase() {

        String designation = "sister";

        FamilyRelationType expected = new FamilyRelationType(designation);

        FamilyRelationType result = assemblerToDTO.toFamilyRelationType(designation);

        assertEquals(expected, result);

    }

    @Test
    void toFamilyRelationType_failCase_invalidDesignation() {

        String designation = "Big Sister";

        Assertions.assertThrows(InvalidDesignationException.class, () -> {
            FamilyRelationType result = assemblerToDTO.toFamilyRelationType(designation);
        });

    }

    @Test
    void toRelationshipDtoFromFamilyRelationship_successCase() {
        FamilyRelationship familyRelationship = new FamilyRelationship(
                new PersonID(new Email("one@gmail.com")),
                new PersonID(new Email("two@gmail.com")),
                new FamilyRelationType("sister"),
                new FamilyRelationshipID("id123")
        );

        FamilyID familyID = new FamilyID("fam123");

        RelationshipDTO expected = new RelationshipDTO();
        expected.setRelationshipId("id123");
        expected.setDesignation("sister");
        expected.setFirstPersonEmail("one@gmail.com");
        expected.setSecondPersonEmail("two@gmail.com");
        expected.setFamilyId(familyID.toString());


        RelationshipDTO result = assemblerToDTO.toRelationshipDto(familyRelationship, familyID);

        assertEquals(expected, result);

    }

    @Test
    void toRelationshipDtoFromFamilyRelationship_failCase_nullRelationship() {
        FamilyRelationship familyRelationship = null;
        FamilyID familyID = new FamilyID("fam123");

        Assertions.assertThrows(NullArgumentException.class, () -> {
            RelationshipDTO result = assemblerToDTO.toRelationshipDto(familyRelationship, familyID);
        });

    }

    @Test
    void toRelationshipDtoFromFamilyRelationship_failCase_nullFamilyId() {
        FamilyRelationship familyRelationship = new FamilyRelationship(
                new PersonID(new Email("one@gmail.com")),
                new PersonID(new Email("two@gmail.com")),
                new FamilyRelationType("sister"),
                new FamilyRelationshipID("id123")
        );

        FamilyID familyID = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            RelationshipDTO result = assemblerToDTO.toRelationshipDto(familyRelationship, familyID);
        });

    }

    @Test
    void toRelationshipDto_AllArguments_successCase() {
        String firstPersonEmail = "first@gmail.com";
        String secondPersonEmail = "second@gmail.com";
        String familyId = "fam1";
        String relationshipId = "rel1";
        String designation = "sister";

        RelationshipDTO expected = new RelationshipDTO();
        expected.setFamilyId(familyId);
        expected.setRelationshipId(relationshipId);
        expected.setFirstPersonEmail(firstPersonEmail);
        expected.setSecondPersonEmail(secondPersonEmail);
        expected.setDesignation(designation);


        RelationshipDTO result = assemblerToDTO.toRelationshipDto(
                firstPersonEmail, secondPersonEmail, familyId, designation, relationshipId
        );

        assertEquals(expected, result);

    }

    @Test
    void createFamilyRelationshipID() {
        FamilyRelationshipID familyRelationshipID = assemblerToDTO.createFamilyRelationshipID();

        assertNotNull(familyRelationshipID);
    }

    @Test
    void toAccountDtoOut() {
        AccountDTO accountDTO = new AccountDTO("luis@isep.pt", "CC Luis", EAccountType.CREDIT_CARD);
        AccountDTOOUT expected = new AccountDTOOUT("CC Luis");
        AccountDTOOUT result = assemblerToDTO.toAccountDtoOut(accountDTO);

        assertEquals(expected, result);
    }

    @Test
    void toAccountDTOLIST() {
        List<Account> accountList = new ArrayList<>();
        Account accountCC = new CreditCardAccount.Builder(new AccountID("3")).setDescription(new Description("CC Luis"))
                .setBalance(new MoneyValue(100)).setOwnerID(new PersonID(new Email("luis@isep.pt")))
                .setAccountType().build();
        Account accountBank = new BankAccount.Builder(new AccountID("20")).setDescription(new Description("Bank Luis"))
                .setBalance(new MoneyValue(252)).setOwnerID(new PersonID(new Email("luis@isep.pt")))
                .setAccountType().build();
        accountList.add(accountCC);
        accountList.add(accountBank);

        List<AccountDTO> expected = new ArrayList<>();
        AccountDTO accountDTOCC = assemblerToDTO.toAccountDTO(accountCC);
        AccountDTO accountDTOBank = assemblerToDTO.toAccountDTO(accountBank);
        expected.add(accountDTOCC);
        expected.add(accountDTOBank);

        List<AccountDTO> result = assemblerToDTO.toAccountDTOList(accountList);

        assertEquals(expected, result);
    }

    @Test
    void toInputToPersonalCashAccountDTO_NullID() {
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO();

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            AccountDTO result = assemblerToDTO.inputToPersonalCashAccountDTO(null, inputPersonalCashAccountDTO);
        });
    }


    @Test
    void toInputToPersonalCashAccountDTO_NullAccountDTO() {
        String id = "luis@isep.pt";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            AccountDTO result = assemblerToDTO.inputToPersonalCashAccountDTO(id, null);
        });
    }

    @Test
    void toInputToPersonalCashAccountDTO_EmptyID() {
        String id = "";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO();

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            AccountDTO result = assemblerToDTO.inputToPersonalCashAccountDTO(id, inputPersonalCashAccountDTO);
        });
    }

    @Test
    void toInputToCustomCategoryDTO() {
        String id = "luis@isep.pt";
        CategoryDTO categoryDTO = new CategoryDTO();

        categoryDTO.setId(id);

        CategoryDTO result = assemblerToDTO.inputToCustomCategoryDTO(id, categoryDTO);

        assertEquals(categoryDTO, result);

    }

    @Test
    void toPersonDTOListTest() {
        Person personOne = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setName(new PersonName("luis"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-1986")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        Person personTwo = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).setName(new PersonName("mimi"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-2016")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        List<Person> personList = new ArrayList<>();
        personList.add(personOne);
        personList.add(personTwo);

        PersonDTO personDTOOne = new PersonDTO("2", "luis@isep.pt", "luis"
                , 0, "Rua tal", "01-01-1986");
        PersonDTO personDTOTwo = new PersonDTO("2", "mimi@isep.pt", "mimi"
                , 0, "Rua tal", "01-01-2016");

        List<PersonDTO> expected = new ArrayList<>();
        expected.add(personDTOOne);
        expected.add(personDTOTwo);

        List<PersonDTO> result = assemblerToDTO.toPersonDTOList(personList);

        assertEquals(expected, result);

    }

    @Test
    void toExceptionDto() {
        String message = "dto message";
        ExceptionDTO expected = new ExceptionDTO(message);

        ExceptionDTO result = assemblerToDTO.toExceptionDto(message);

        assertEquals(expected, result);
    }

    @Test
    void assembleRelationshipDTO() {
        String familyId = "20";
        String personOne = "ursula@ess.pt";
        String personTwo = "gervasio@fcup.pt";

        RelationshipDTO expected = new RelationshipDTO();
        expected.setFamilyId(familyId);
        expected.setFirstPersonEmail(personOne);
        expected.setSecondPersonEmail(personTwo);
        //ACT
        RelationshipDTO actual = assemblerToDTO.inputSearchForRelationshipDTO(familyId, personOne, personTwo);
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void assembleCategoryDTO() {
        String name = "Tennis";
        String parentId = "1.1";

        CategoryDTO dto = new CategoryDTO();
        dto.setName(name);
        dto.setParentId(parentId);

        //ACT
        CategoryDTO actual = assemblerToDTO.inputToCategoryDTO(dto);
        //Assert
        assertEquals(dto, actual);
    }

    @Test
    void assembleAccountDTO() {
        String id = "1.1";
        String name = "Tuga";

        AccountDTO dto = new AccountDTO();
        dto.setDescription(name);
        dto.setBalance(200);

        //ACT
        AccountDTO actual = assemblerToDTO.inputToAccountDTO(id, dto);
        //Assert
        assertEquals(dto.getBalance(), actual.getBalance());
        assertEquals(dto.getDescription(), actual.getDescription());
    }

    @Test
    void getAccountBalance() {
        double balance = 200;
        Account account = new BankAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(200))
                .setDescription(new Description("Tuga")).build();

        //act
        AccountDTO actual = assemblerToDTO.getBalanceDTO(account);
        //assert
        assertEquals(balance, actual.getBalance());
    }

    @Test
    void transferDTO() {
        Transactionable transf = new Transfer(new TransactionID("1"), new AccountID("123"), new AccountID("456"),
                new MoneyValue(20), new CategoryID("1"), new TransactionDate(LocalDate.MAX), new Description("BCP"));

        TransferDTO actual = assemblerToDTO.toTransferDTO(transf);
        assertNotNull(actual);
    }

    @Test
    void toAccountDTO_test(){

        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();

        String ownerID = "luis@isep.pt";
        String description = "account";
        EAccountType accountType = EAccountType.CREDIT_CARD;

        AccountDTO expected = new AccountDTO(ownerID,description,accountType);

        AccountDTO result = assemblerToDTO.toAccountDTO(ownerID,description,accountType);

        assertEquals(expected,result);
    }

}
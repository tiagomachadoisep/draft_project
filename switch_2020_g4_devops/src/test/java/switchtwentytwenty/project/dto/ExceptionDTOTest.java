package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class ExceptionDTOTest {

    @Test
    void emptyConstructor() {
        ExceptionDTO exceptionDTO = new ExceptionDTO("some message");

        assertNotNull(exceptionDTO);
    }

    @Test
    void setMessage() {
        String message = "This is a message";
        String newMessage = "new message";
        ExceptionDTO exceptionDTO = new ExceptionDTO(message);


        exceptionDTO.setMessage(newMessage);

        assertEquals(newMessage, exceptionDTO.getMessage());
    }

    @Test
    void equalsAndHashcode() {

        ExceptionDTO dtoMessage = new ExceptionDTO("dto");

        ExceptionDTO dtoSameMessage = new ExceptionDTO("dto");

        ExceptionDTO dtoDifferentMessage = new ExceptionDTO("okok");

        ExceptionDTO nullDto = null;

        RelationshipDTO differentDto = new RelationshipDTO();

        assertEquals(dtoMessage, dtoMessage);
        assertEquals(dtoMessage.hashCode(), dtoMessage.hashCode());

        assertEquals(dtoMessage, dtoSameMessage);
        assertEquals(dtoMessage.hashCode(), dtoSameMessage.hashCode());

        assertNotEquals(dtoMessage, dtoDifferentMessage);
        assertNotEquals(dtoMessage.hashCode(), dtoDifferentMessage.hashCode());

        assertNotEquals(dtoMessage, nullDto);

        assertNotEquals(dtoMessage, differentDto);
        assertNotEquals(dtoMessage.hashCode(), differentDto.hashCode());

    }
    @Test
    void testEqualsSuper(){
        PersonDTO personDTO = new PersonDTO("luis@isep.pt");

        ExceptionDTO exceptionDTO = new ExceptionDTO("dto");
        exceptionDTO.add(linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family"));

        ExceptionDTO exceptionDTONoLinks = new ExceptionDTO("okok");

        assertNotEquals(exceptionDTO,exceptionDTONoLinks);

    }

}
package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import org.springframework.hateoas.Link;
import switchtwentytwenty.project.controllers.implcontrollers.CheckAccountBalanceController;
import switchtwentytwenty.project.controllers.implcontrollers.DeleteSecondaryEmailController;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class EmailDTOTest {

    @Test
    void testEquals_sameObject() { //this == o
        String email = "abc@isep.pt";
        String ownerID = "carlos@isep.pt";

        EmailDTO dto1 = new EmailDTO(email, ownerID);

        //assert
        assertEquals(dto1, dto1); //same object
        assertEquals(dto1.hashCode(), dto1.hashCode());//equal hash
    }

    @Test
    void testEquals_differentClassObject(){ //(o == null || getClass() != o.getClass())
        EmailDTO dto1 = new EmailDTO("abc@isep.pt", "carlos@isep.pt");
        EmailDTO dto2 = null;

        FamilyDTO dto3 = new FamilyDTO();
        Family family = new Family.Builder(new FamilyID("1")).build();

        assertNotEquals(dto2, dto1);//not equal object
        assertNotEquals(family, dto1);//not equal object
        assertFalse(dto1.equals(dto2)); // o == null
        assertFalse(dto1.equals(dto3)); // getClass() != o.getClass()
    }

    @Test
    void testEqualsSuper() { // (!super.equals(o))

        EmailDTO outDTO = new EmailDTO("carlos@isep.pt", "abc@abc.pt");
        outDTO.add(linkTo(methodOn(DeleteSecondaryEmailController.class).deleteSecondaryEmail(outDTO.getEmail(), outDTO.getMainEmail())).withSelfRel());


        EmailDTO outDTO2 = new EmailDTO("carlos@isep.pt");

        assertNotEquals(outDTO,outDTO2);
        assertFalse(outDTO.equals(outDTO2));
    }
    @Test
    void testEquals_ObjectWithSameInfo() { //all equal
        String email = "abc@isep.pt";
        String ownerID = "carlos@isep.pt";

        EmailDTO dto1 = new EmailDTO(email, ownerID);
        EmailDTO dto2 = new EmailDTO(email, ownerID);

        //assert
        assertNotSame(dto1, dto2);//not same objects
        assertEquals(dto1, dto2); //equal objects
        assertEquals(dto1.hashCode(), dto2.hashCode());//equal hashes
    }

    @Test
    void testEquals_ObjectWithDifferentEmailField() { //(!email.equals(emailDTO.email))

        EmailDTO dto1 = new EmailDTO("abc@isep.pt", "carlos@isep.pt");
        EmailDTO dto2 = new EmailDTO("carlitos@isep.pt", "carlos@isep.pt");

        //assert
        assertNotSame(dto1, dto2);//not same objects
        assertFalse(dto1.equals(dto2)); //not equal objects
        assertNotEquals(dto1.hashCode(), dto2.hashCode());//not equal hashes
    }

    @Test
    void testEquals_ObjectWithDifferentOwnerField() { //(!mainEmail.equals(emailDTO.mainEmail))

        EmailDTO dto1 = new EmailDTO("abc@isep.pt", "carlitos@isep.pt");
        EmailDTO dto2 = new EmailDTO("abc@isep.pt", "carlos@isep.pt");

        //assert
        assertNotSame(dto1, dto2);//not same objects
        assertFalse(dto1.equals(dto2)); //not equal objects
        assertNotEquals(dto1.hashCode(), dto2.hashCode());//not equal hashes
    }

    @Test
    void testEquals_ObjectWithDifferentEmailListField(){ // (!emailsList.equals(emailDTO.emailsList))
        EmailDTO dto1 = new EmailDTO("abc@isep.pt", "carlitos@isep.pt");
        EmailDTO dto2 = new EmailDTO("abc@isep.pt", "carlitos@isep.pt");
        List<String> emailList = new ArrayList<>();
        emailList.add("email2@isep.pt");
        dto2.setEmailsList(emailList);

        assertNotSame(dto1, dto2);
        assertFalse(dto1.equals(dto2));
    }
}
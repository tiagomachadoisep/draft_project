package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class TransferDTOTest {

    @Test
    void testEqualsSameObject() {
        TransferDTO transferDTO = new TransferDTO("luis@isep.pt", "mimi@isep.pt"
                , 200, "20", LocalDate.now());

        assertEquals(transferDTO, transferDTO);
        assertEquals(transferDTO.hashCode(), transferDTO.hashCode());
    }

    @Test
    void testEqualsDifferentObjectSameArguments() {
        TransferDTO transferDTO = new TransferDTO("luis@isep.pt", "mimi@isep.pt"
                , 200, "20", LocalDate.now());

        TransferDTO transferDTOTwo = new TransferDTO("luis@isep.pt", "mimi@isep.pt"
                , 200, "20", LocalDate.now());


        assertEquals(transferDTO, transferDTOTwo);
        assertEquals(transferDTO.hashCode(), transferDTOTwo.hashCode());
    }


    @Test
    void testEqualsDifferentClass() {
        TransferDTO transferDTO = new TransferDTO("luis@isep.pt", "mimi@isep.pt"
                , 200, "20", LocalDate.now());
        Family family = new Family.Builder(new FamilyID("20")).build();


        assertNotEquals(transferDTO, family);
        assertNotEquals(transferDTO.hashCode(), family.hashCode());
    }

    @Test
    void testEqualsSameClassOneNull() {

        TransferDTO transferDTO = new TransferDTO("luis@isep.pt", "mimi@isep.pt"
                , 200, "20", LocalDate.now());

        TransferDTO transferDTOTwo = null;


        assertNotEquals(transferDTO, transferDTOTwo);

    }

    @Test
    void getDestinationId() {

        //Arrange
        String destinationId = "1234";
        TransferDTO transferDTO = new TransferDTO("filipa@gmail.com", "ze@isep.pt", 100, "123", "12345",
                destinationId, "123", LocalDate.parse("2020-08-11"));
        ;

        //Act
        String result = transferDTO.getDestinationId();

        //Assert
        assertEquals(destinationId, result);

    }

    @Test
    void testSuperEquals(){
        TransferDTO transferDTO = new TransferDTO("luis@isep.pt", "mimi@isep.pt"
                , 200, "20", LocalDate.now());

        TransferDTO transferDTOTwo = new TransferDTO("martim@isep.pt", "marta@isep.pt"
                , 500, "25", LocalDate.now());

        PersonDTO personDTO = new PersonDTO("luis@isep.pt");

        transferDTO.add(linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family"));

        assertNotEquals(transferDTO,transferDTOTwo);
    }
}
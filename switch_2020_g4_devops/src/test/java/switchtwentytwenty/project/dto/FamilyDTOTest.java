package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.controllers.implcontrollers.CreateFamilyCashAccountController;
import switchtwentytwenty.project.controllers.implcontrollers.CreateFamilyRelationshipController;
import switchtwentytwenty.project.controllers.implcontrollers.GetProfileInfoController;
import switchtwentytwenty.project.dto.visitor.LinkAddVisitor;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class FamilyDTOTest {

    @Test
    void acceptVisitor(){
        String name= "Silva";
        String adminEmail = "silva@isep.pt";
        String familyId = "1";
        FamilyDTO dto = new FamilyDTO(name,adminEmail,familyId);

        dto.accept(new LinkAddVisitor());

        FamilyDTO expected = new FamilyDTO(name,adminEmail,familyId);
        expected.add(linkTo(methodOn(AddFamilyController.class).getFamily(expected.getId())).withSelfRel());
        expected.add(linkTo(AddFamilyController.class).slash("families").slash(expected.getId()).slash("members").withRel("members"));
        expected.add(linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(expected.getAdminEmail())).withRel("admin"));
        expected.add(linkTo(methodOn(CreateFamilyRelationshipController.class).getAllRelationships(expected.getId())).withRel("relations"));
        expected.add(linkTo(CreateFamilyCashAccountController.class).slash("families").slash(expected.getId()).slash("accounts").withRel("account"));

        assertEquals(expected,dto);
    }

    @Test
    void acceptVisitorFalse(){
        String name= "Silva";
        String adminEmail = "silva@isep.pt";
        String familyId = "1";
        FamilyDTO dto = new FamilyDTO(name,adminEmail,familyId);

        dto.accept(new LinkAddVisitor());

        FamilyDTO expected = new FamilyDTO(name,adminEmail,familyId);

        assertNotEquals(expected,dto);
    }

}
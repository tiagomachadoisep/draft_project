package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class RelationshipDTOTest {

    @Test
    void testingAttributesGetterSetter() {
        String emailOne = "marta@hotmail.com";
        String emailTwo = "pretend@ucan.com";
        String description = "brother";
        String familyId = "1";
        String familyRelationshipId ="10";

        RelationshipDTO dto = new RelationshipDTO();
        dto.setFirstPersonEmail(emailOne);
        dto.setSecondPersonEmail(emailTwo);
        dto.setDesignation(description);
        dto.setFamilyId(familyId);
        dto.setRelationshipId(familyRelationshipId);

        assertEquals(emailOne, dto.getFirstPersonEmail());
        assertEquals(emailTwo, dto.getSecondPersonEmail());
        assertEquals(description, dto.getDesignation());
        assertEquals(familyId, dto.getFamilyId());
        assertEquals(familyRelationshipId, dto.getRelationshipId());

    }

    @Test
    void comparingSameObject() {

        //arrange
        String emailOne = "marta@hotmail.com";
        String emailTwo = "pretend@ucan.com";
        String description = "brother";
        String familyId = "1";
        String familyRelationshipId ="10";

        RelationshipDTO dto = new RelationshipDTO();
        dto.setFirstPersonEmail(emailOne);
        dto.setSecondPersonEmail(emailTwo);
        dto.setDesignation(description);
        dto.setFamilyId(familyId);
        dto.setRelationshipId(familyRelationshipId);

        //act
        boolean result = dto.equals(dto);

        //assert
        assertTrue(result);
        assertNotEquals(null, dto);
        assertEquals(dto.hashCode(), dto.hashCode());

    }

    @Test
    void comparingDifferentObject() {

        //arrange
        String emailOne = "marta@hotmail.com";
        String emailTwo = "pretend@ucan.com";
        String description = "brother";
        String familyId = "1";
        String familyRelationshipId ="10";

        RelationshipDTO dto = new RelationshipDTO();
        dto.setFirstPersonEmail(emailOne);
        dto.setSecondPersonEmail(emailTwo);
        dto.setDesignation(description);
        dto.setFamilyId(familyId);
        dto.setRelationshipId(familyRelationshipId);

        RelationshipDTO secondDto = new RelationshipDTO();
        secondDto.setFirstPersonEmail(emailOne);
        secondDto.setSecondPersonEmail(emailTwo);
        secondDto.setDesignation(description);
        secondDto.setFamilyId("3");
        secondDto.setRelationshipId(familyRelationshipId);

        RelationshipDTO thirdDto = new RelationshipDTO();
        thirdDto.setFirstPersonEmail(emailOne);
        thirdDto.setSecondPersonEmail(emailTwo);
        thirdDto.setDesignation("sister");
        thirdDto.setFamilyId(familyId);
        thirdDto.setRelationshipId(familyRelationshipId);

        RelationshipDTO fourthDto = new RelationshipDTO();
        fourthDto.setFirstPersonEmail(emailOne);
        fourthDto.setSecondPersonEmail(emailTwo);
        fourthDto.setDesignation(description);
        fourthDto.setFamilyId(familyId);
        fourthDto.setRelationshipId("20");

        RelationshipDTO fifthDto = new RelationshipDTO();
        fifthDto.setFirstPersonEmail(emailOne);
        fifthDto.setSecondPersonEmail("fail@email.pt");
        fifthDto.setDesignation(description);
        fifthDto.setFamilyId(familyId);
        fifthDto.setRelationshipId(familyRelationshipId);


        RelationshipDTO sixthDto = new RelationshipDTO();
        sixthDto.setFirstPersonEmail("fail@email.pt");
        sixthDto.setSecondPersonEmail(emailTwo);
        sixthDto.setDesignation(description);
        sixthDto.setFamilyId(familyId);
        sixthDto.setRelationshipId(familyRelationshipId);


        //act
        boolean result = dto.equals(secondDto);
        boolean resultDescription = dto.equals(thirdDto);
        boolean resultRelationshipId = dto.equals(fourthDto);
        boolean resultSecondEmail = dto.equals(fifthDto);
        boolean resultFirstEmail = dto.equals(sixthDto);



        //assert
        assertFalse(result);
        assertFalse(resultDescription);
        assertFalse(resultRelationshipId);
        assertFalse(resultSecondEmail);
        assertFalse(resultFirstEmail);
        assertNotEquals(dto.hashCode(), secondDto.hashCode());

    }

    @Test
    void comparingDifferentClassObjects() {

        //arrange
        String emailOne = "marta@hotmail.com";
        String emailTwo = "pretend@ucan.com";
        String description = "brother";
        String familyId = "1";
        String familyRelationshipId ="10";

        RelationshipDTO dto = new RelationshipDTO();
        dto.setFirstPersonEmail(emailOne);
        dto.setSecondPersonEmail(emailTwo);
        dto.setDesignation(description);
        dto.setFamilyId(familyId);
        dto.setRelationshipId(familyRelationshipId);

        RelationshipDTO dtoTwo = new RelationshipDTO();
        dtoTwo.setFirstPersonEmail(emailOne);
        dtoTwo.setSecondPersonEmail(emailTwo);
        dtoTwo.setDesignation(description);
        dtoTwo.setFamilyId("3");
        dtoTwo.setRelationshipId(familyRelationshipId);


        CategoryName name = new CategoryName("something");


        //act
        assertNotEquals(name,dto);
        assertFalse(dto.getClass().equals(name.getClass()));
        assertTrue(dto.getClass().equals(dtoTwo.getClass()));

        System.out.println(dto.getClass());
        System.out.println(name.getClass());
        System.out.println(dtoTwo.getClass());
        assertNotEquals(dto.getClass(),name.getClass());
        assertEquals(dto.getClass(),dtoTwo.getClass());

    }

    @Test
    void testEqualsSuper(){

        //arrange
        PersonDTO personDTO = new PersonDTO("luis@isep.pt");

        String emailOne = "marta@hotmail.com";
        String emailTwo = "pretend@ucan.com";
        String description = "brother";
        String familyId = "1";
        String familyRelationshipId ="10";

        RelationshipDTO dto = new RelationshipDTO();
        dto.setFirstPersonEmail(emailOne);
        dto.setSecondPersonEmail(emailTwo);
        dto.setDesignation(description);
        dto.setFamilyId(familyId);
        dto.setRelationshipId(familyRelationshipId);
        dto.add(linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family"));


        RelationshipDTO secondDto = new RelationshipDTO();
        secondDto.setFirstPersonEmail(emailOne);
        secondDto.setSecondPersonEmail(emailTwo);
        secondDto.setDesignation(description);
        secondDto.setFamilyId("3");
        secondDto.setRelationshipId(familyRelationshipId);

        assertNotEquals(dto, secondDto);

    }

    @Test
    void nullAndDifferentClass() {
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        RelationshipDTO relationshipDTONull = null;
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("123");

        assertNotEquals(relationshipDTO, relationshipDTONull);
        assertNotEquals(relationshipDTO, familyRelationshipID);
    }


}
package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.AddEmailController;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class PersonDTOTest {

    @Test
    void testEquals() {
        PersonDTO personDTO = new PersonDTO("luis@isep.pt");
        PersonDTO personDTOOther = new PersonDTO("mimi@isep.pt");
        Family family = new Family.Builder(new FamilyID("1")).build();

        assertEquals(personDTO, personDTO);
        assertNotEquals(personDTO, personDTOOther);
        assertEquals(personDTO.hashCode(), personDTO.hashCode());
        assertNotEquals(personDTO.hashCode(), personDTOOther.hashCode());
        assertNotEquals(personDTO, family);
        assertNotEquals(null, personDTO);
    }

    @Test
    void testEqualsSuper() {
        PersonDTO personDTO = new PersonDTO("luis@isep.pt");
        PersonDTO personDTOOther = new PersonDTO("mimi@isep.pt");
        personDTO.add(linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family"));

        personDTOOther.add(linkTo((methodOn(AddEmailController.class)
                .showOtherEmails(personDTO.getEmailAddress()))).withRel("Other emails"));

        assertNotEquals(personDTO, personDTOOther);
    }

    @Test
    void testEquals_CompareWithNullObject() {

        //Arrange
        String emailAddress = "filipa@gmail.com";
        PersonDTO personDTO = new PersonDTO();
        personDTO.setEmailAddress(emailAddress);

        //Act
        boolean result = personDTO.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void setEmailAddress() {

        //Arrange
        String emailAddress = "filipa@gmail.com";
        PersonDTO personDTO = new PersonDTO();

        //Act
        personDTO.setEmailAddress(emailAddress);

        //Assert
        assertEquals(emailAddress, personDTO.getEmailAddress());

    }

}
package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class CategoryDTOTest {

    @Test
    void testingAttributesGetterSetter() {

        //arrange
        String categoryId = "1";
        String categoryName = "Drinks";
        String parentId = "3";
        String familyId = "10";

        CategoryDTO dto = new CategoryDTO();
        dto.setId(categoryId);
        dto.setName(categoryName);
        dto.setParentId(parentId);
        dto.setFamilyId(familyId);

        //assert
        assertEquals(categoryId, dto.getId());
        assertEquals(categoryName, dto.getName());
        assertEquals(parentId, dto.getParentId());
        assertEquals(familyId, dto.getFamilyId());

    }

    @Test
    void comparingSameObject() {
        //arrange
        String categoryId = "1";
        String categoryName = "Drinks";
        String parentId = "3";
        String familyId = "10";

        CategoryDTO dto = new CategoryDTO();
        dto.setId(categoryId);
        dto.setName(categoryName);
        dto.setParentId(parentId);
        dto.setFamilyId(familyId);

        //assert
        assertEquals(dto, dto);
        assertNotEquals(null, dto);
        assertEquals(dto.hashCode(), dto.hashCode());
    }

    @Test
    void comparingDifferentObject() {
        //arrange
        String categoryId = "1";
        String categoryName = "Drinks";
        String parentId = "3";
        String familyId = "10";

        CategoryDTO dto = new CategoryDTO();
        dto.setId(categoryId);
        dto.setName(categoryName);
        dto.setParentId(parentId);
        dto.setFamilyId(familyId);

        CategoryDTO secondDto = new CategoryDTO();
        secondDto.setId(categoryId);
        secondDto.setName(categoryName);
        secondDto.setParentId(parentId);
        secondDto.setFamilyId("30");

        //assert
        assertNotEquals(dto, secondDto);
        assertNotEquals(secondDto.hashCode(), dto.hashCode());
    }

    @Test
    void comparingDifferentClassObjects() {
        //arrange
        String categoryId = "1";
        String categoryName = "Drinks";
        String parentId = "3";
        String familyId = "10";

        CategoryDTO dto = new CategoryDTO();
        dto.setId(categoryId);
        dto.setName(categoryName);
        dto.setParentId(parentId);
        dto.setFamilyId(familyId);

        FamilyID id = new FamilyID("1");

        //assert
        assertNotEquals(dto, id);
    }

    @Test
    void categoryDTOToString() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Beer");
        categoryDTO.setFamilyId("123");
        categoryDTO.setId("123456");
        categoryDTO.setParentId("123456789");

        String expected = "{" +
                "name='" + categoryDTO.getName() + '\'' +
                ", id='" + categoryDTO.getId() + '\'' +
                ", parentId='" + categoryDTO.getParentId() + '\'' +
                ", familyId='" + categoryDTO.getFamilyId() + '\'' +
                "}";

        //Act
        String result = categoryDTO.toString();

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void equals_comparingWithNullObject() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Beer");
        categoryDTO.setFamilyId("123");
        categoryDTO.setId("123456");
        categoryDTO.setParentId("123456789");

        //Act
        boolean result = categoryDTO.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_comparingObjectsWithDifferentName() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Wine");
        categoryDTO.setFamilyId("123");
        categoryDTO.setId("123456");
        categoryDTO.setParentId("123456789");
        CategoryDTO categoryDTO2 = new CategoryDTO();
        categoryDTO2.setName("Beer");
        categoryDTO2.setFamilyId("123");
        categoryDTO2.setId("123456");
        categoryDTO2.setParentId("123456789");

        //Act
        boolean result = categoryDTO.equals(categoryDTO2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_comparingObjectsWithDifferentId() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Beer");
        categoryDTO.setFamilyId("123");
        categoryDTO.setId("123456");
        categoryDTO.setParentId("123456789");
        CategoryDTO categoryDTO2 = new CategoryDTO();
        categoryDTO2.setName("Beer");
        categoryDTO2.setFamilyId("123");
        categoryDTO2.setId("1234567");
        categoryDTO2.setParentId("123456789");

        //Act
        boolean result = categoryDTO.equals(categoryDTO2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_comparingObjectsWithDifferentParentId() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName("Beer");
        categoryDTO.setFamilyId("123");
        categoryDTO.setId("123456");
        categoryDTO.setParentId("123456789");
        CategoryDTO categoryDTO2 = new CategoryDTO();
        categoryDTO2.setName("Beer");
        categoryDTO2.setFamilyId("123");
        categoryDTO2.setId("123456");
        categoryDTO2.setParentId("1234567890");

        //Act
        boolean result = categoryDTO.equals(categoryDTO2);

        //Assert
        assertFalse(result);

    }


}
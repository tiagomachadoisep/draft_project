package switchtwentytwenty.project.dto;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.controllers.implcontrollers.AddEmailController;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class PaymentDTOTest {

    @Test
    void testEquals_differentClass() {
        PaymentDTO paymentDTO = new PaymentDTO("luis@isep.pt"
                , 20, "Health");

        Family family = new Family.Builder(new FamilyID("1")).build();

        assertNotEquals(paymentDTO, family);
    }

    @Test
    void testEquals() {
        String memberID = "luis@isep.pt";
        double balance = 200;
        String categoryID = "categoryID";
        String transactionID = "transactionID";
        String accountID = "accountID";
        LocalDate localDate = LocalDate.now();

        PaymentDTO paymentDTO = new PaymentDTO(memberID, balance, categoryID, transactionID, accountID, localDate);
        PaymentDTO paymentDTOSecond = new PaymentDTO(memberID, balance, categoryID, transactionID, accountID, localDate);
        PaymentDTO paymentFail = new PaymentDTO(memberID, balance, categoryID, "failTransaction", accountID, localDate);

        assertNotEquals(paymentDTO, paymentFail);
        assertNotEquals(paymentDTO.hashCode(), paymentFail.hashCode());
        assertEquals(paymentDTO.hashCode(), paymentDTOSecond.hashCode());

    }

    @Test
    void testEqualsSuper() {
        String memberID = "luis@isep.pt";
        double balance = 200;
        String categoryID = "categoryID";
        String transactionID = "transactionID";
        String accountID = "accountID";
        LocalDate localDate = LocalDate.now();

        PersonDTO personDTO = new PersonDTO("familyID", memberID, "Jurema", 123456789,
                "Rua das Cristalinas,99", "01-01-1990");

        PaymentDTO paymentDTO = new PaymentDTO(memberID, balance, categoryID, transactionID, accountID, localDate);
        paymentDTO.add(linkTo((methodOn(AddEmailController.class).showOtherEmails(personDTO.getEmailAddress()))).withRel("Other emails"));

        PaymentDTO paymentDTOSecond = new PaymentDTO(memberID, balance, categoryID, transactionID, accountID, localDate);

        assertNotEquals(paymentDTO,paymentDTOSecond);
    }
}
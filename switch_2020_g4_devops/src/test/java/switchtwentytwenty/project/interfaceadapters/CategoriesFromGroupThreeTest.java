package switchtwentytwenty.project.interfaceadapters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class CategoriesFromGroupThreeTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private CategoriesFromGroupThree adapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }


    /*@Test
    void importCategoriesTest() {


        CategoryDTO catDTO0 = new CategoryDTO("LUZ", "G3/4", "null", "");

        List<CategoryDTO> expected = new ArrayList<>();

        expected.add(catDTO0);

        String categoryJSON = "{\n" +
                "\"outputCategoryDTOList\":\n" +
                "  [\n" +
                "    {\n" +
                "      \"categoryName\":\"LUZ\",\n" +
                "      \"categoryID\":\"4\",\n" +
                "      \"parentID\":\"null\",\n" +
                "      \"familyID\":null\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(categoryJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("http://vs116.dei.isep.ipp.pt:8080/categories", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);


        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected, result);

    }*/

    @Test
    void importCategoriesTest_withNullResponse_ThrowException() {

        String emptyJSON = "";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(emptyJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("http://vs116.dei.isep.ipp.pt:8080/categories", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);

        assertThrows(IllegalArgumentException.class, () -> {
            adapter.importCategories();
        });
    }
}


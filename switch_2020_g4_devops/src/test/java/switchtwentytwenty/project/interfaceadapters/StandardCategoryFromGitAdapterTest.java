package switchtwentytwenty.project.interfaceadapters;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class StandardCategoryFromGitAdapterTest {


    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private CategoryFromGitRepositoryHttp adapter;
    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }



    @Test
    void importCategories() {
        CategoryFromGitRepositoryHttp adapter = new CategoryFromGitRepositoryHttp();

        CategoryDTO dto0 = new CategoryDTO("All", "HTTPGIST/0", null, null);
        CategoryDTO dto1 = new CategoryDTO("Books", "HTTPGIST/1", null, null);
        CategoryDTO dto2 = new CategoryDTO("Hardcover", "HTTPGIST/1", "HTTPGIST/1", null);
        CategoryDTO dto3 = new CategoryDTO("Paperback", "HTTPGIST/2", "HTTPGIST/1", null);
        CategoryDTO dto4 = new CategoryDTO("Electronic", "HTTPGIST/3", "HTTPGIST/1", null);
        CategoryDTO dto5 = new CategoryDTO("Movies", "HTTPGIST/2", null, null);
        CategoryDTO dto6 = new CategoryDTO("DVD", "HTTPGIST/4", "HTTPGIST/2", null);
        CategoryDTO dto7 = new CategoryDTO("BluRay", "HTTPGIST/5", "HTTPGIST/2", null);
        CategoryDTO dto8 = new CategoryDTO("Download", "HTTPGIST/6", "HTTPGIST/2", null);
        CategoryDTO dto9 = new CategoryDTO("Games", "HTTPGIST/3", null, null);
        CategoryDTO dto10 = new CategoryDTO("XBox", "HTTPGIST/7", "HTTPGIST/3", null);
        CategoryDTO dto11 = new CategoryDTO("PC", "HTTPGIST/8", "HTTPGIST/3", null);
        CategoryDTO dto12 = new CategoryDTO("Music", "HTTPGIST/4", null, null);

        List<CategoryDTO> expected = new ArrayList<>();
        expected.add(dto0);
        expected.add(dto1);
        expected.add(dto2);
        expected.add(dto3);
        expected.add(dto4);
        expected.add(dto5);
        expected.add(dto6);
        expected.add(dto7);
        expected.add(dto8);
        expected.add(dto9);
        expected.add(dto10);
        expected.add(dto11);
        expected.add(dto12);

        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected, result);
    }


    @Test
    void importCategories_whenResponseIsNull_throwException() {

        String emptyJSON = "";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(emptyJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("https://gist.githubusercontent.com/dagilleland/4191556/raw/1d1517860ad110bde47fbf0b4702e2c426f1932c/fiddle.response.json", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);


        List<CategoryDTO> expected = Collections.emptyList();
        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected,result);
    }

    @Test
    void importCategories_whenResponseIsInvalid_throwException() {

        String emptyJSON = "";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(emptyJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("https://gist.githubusercontent.com/dagilleland/4191556/raw/1d1517860ad110bde47fbf0b4702e2c426f1932c/fiddle.response.json", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);


        List<CategoryDTO> expected = Collections.emptyList();
        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected,result);
    }

    @Test
    void importCategories_whenResponseHasInvalidObject_throwException() {

        String emptyJSON = "[{\"categoryId\":1,\"name\":\"Books\",\"hahah\":[{\"subCategoryId\":1,\"name\":\"Hardcover\"},{\"subCategoryId\":2,\"name\":\"Paperback\"},{\"subCategoryId\":3,\"name\":\"Electronic\"}]}]";
        CategoryDTO dto = new CategoryDTO("Books","HTTPGIST/1",null,null);

        ResponseEntity<String> myEntity = new ResponseEntity<String>(emptyJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("https://gist.githubusercontent.com/dagilleland/4191556/raw/1d1517860ad110bde47fbf0b4702e2c426f1932c/fiddle.response.json", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);


        List<CategoryDTO> expected = new ArrayList<>();
        expected.add(dto);
        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected,result);
    }
/*
    @Test
    void importCategoriesTest() {
        CategoryFromGitRepositoryHttp adapter = new CategoryFromGitRepositoryHttp();

        List<CategoryDTO> expected = Collections.emptyList();

        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected, result);
    }
*/

}
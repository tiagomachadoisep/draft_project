package switchtwentytwenty.project.interfaceadapters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoriesFromGroupOneTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private CategoriesFromGroupOne adapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void importCategoriesTest() {


        CategoryDTO catDTO0 = new CategoryDTO("TRAVEL", "G1/2070544935", null, null);
        CategoryDTO catDTO1 = new CategoryDTO("VACATION", "G1/-641203292", "G1/2070544935", null);


        List<CategoryDTO> expected = new ArrayList<>();

        expected.add(catDTO0);
        expected.add(catDTO1);

        String categoryJSON = "{\n" +
                "\"categoryDTOs\":\n" +
                "  [\n" +
                "    {\n" +
                "      \"name\":\"TRAVEL\",\n" +
                "      \"id\": 2070544935,\n" +
                "      \"parentId\": null,\n" +
                "      \"idDatabase\": 1,\n" +
                "      \"childCategories\": null\n" +
                "    }\n," +
                "    {\n" +
                "      \"name\":\"VACATION\",\n" +
                "      \"id\": -641203292,\n" +
                "      \"parentId\": 2070544935,\n" +
                "      \"idDatabase\": 2,\n" +
                "      \"childCategories\": null\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(categoryJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("http://vs249.dei.isep.ipp.pt:8080/categories/standard/list", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);


        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected, result);

    }

    @Test
    void importCategoriesTest_withNullResponse_ThrowException() {

        String emptyJSON = "";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(emptyJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("http://vs249.dei.isep.ipp.pt:8080/categories/standard/list", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);

        assertThrows(IllegalArgumentException.class, () -> {
            adapter.importCategories();
        });
    }

}
package switchtwentytwenty.project.interfaceadapters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoriesFromGroupTwoTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private CategoriesFromGroupTwo adapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void importCategoriesTest() {


        CategoryDTO catDTO0 = new CategoryDTO("Food", "G2/1", null, null);
        CategoryDTO catDTO1 = new CategoryDTO("Fruit", "G2/2", "G2/1", null);

        List<CategoryDTO> expected = new ArrayList<>();

        expected.add(catDTO0);
        expected.add(catDTO1);

        String categoryJSON = "  [\n" +
                "    {\n" +
                "      \"designation\":\"Food\",\n" +
                "      \"id\":\"1\",\n" +
                "      \"parentID\": null\n" +
                "    }\n" + ","+
                "    {\n" +
                "      \"designation\":\"Fruit\",\n" +
                "      \"id\":\"2\",\n" +
                "      \"parentID\":\"1\"\n" +
                "    }\n"+
                "  ]";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(categoryJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("http://vs118.dei.isep.ipp.pt:8080/categories/standard", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);


        List<CategoryDTO> result = adapter.importCategories();

        assertEquals(expected, result);

    }

    @Test
    void importCategoriesTest_withNullResponse_ThrowException() {

        String emptyJSON = "";

        ResponseEntity<String> myEntity = new ResponseEntity<String>(emptyJSON, HttpStatus.ACCEPTED);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        Mockito.when(restTemplate.exchange("http://vs118.dei.isep.ipp.pt:8080/categories/standard", HttpMethod.GET, entity, String.class)).thenReturn(myEntity);

        assertThrows(IllegalArgumentException.class, () -> {
            adapter.importCategories();
        });
    }


}
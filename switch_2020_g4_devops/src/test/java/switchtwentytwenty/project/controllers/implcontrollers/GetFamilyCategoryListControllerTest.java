package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

//@SpringBootTest
public class GetFamilyCategoryListControllerTest {
/*
    @Mock
    private ICategoryService categoryService;

    @InjectMocks
    private GetFamilyCategoryListController controller;

    @Test
    @DisplayName("Success - get the list of family's categories")
    void getListOfCategories_success() {
        //Arrange
        String familyId = "12";
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);
        category1.setFamilyId(familyId);

        CategoryDTO category2 = new CategoryDTO();
        category2.setId("2");
        category2.setName("Supermarket");
        category2.setParentId(null);
        category2.setFamilyId(familyId);

        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        categoryDTOList.add(category1);
        categoryDTOList.add(category2);

        Mockito.when(categoryService.getListOfCategories(familyId)).thenReturn(categoryDTOList);

        //Act
        ResponseEntity<Object> result = controller.getListOfCategories(familyId);

        //Assert
        assertEquals(categoryDTOList, result.getBody());
    }

    @Test
    @DisplayName("Failure - the family id is null.")
    void getListOfCategories_failure() {
        //Arrange
        Mockito.when(categoryService.getListOfCategories(null)).thenThrow(NullPointerException.class);
        String expected = "Invalid parameter";
        //Act
        ResponseEntity<Object> result = controller.getListOfCategories(null);
        //Assert
        assertEquals("Invalid parameter.", result.getBody());
    }

    @Test
    @DisplayName("Success - get the information of a category.")
    void getCategoryInfo_success() {
        //Arrange
        String familyId = "12";
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);
        category1.setFamilyId(familyId);

        Mockito.when(categoryService.getCategoryById(category1.getId())).thenReturn(category1);
        //Act
        ResponseEntity<Object> result = controller.getCategoryInfo(category1.getId());
        //Assert
        assertEquals(category1, result.getBody());
    }

    @Test
    @DisplayName("Failure - the category doesn't exist.")
    void getCategoryInfo_failure() {
        //Arrange
        String familyId = "12";
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);
        category1.setFamilyId(familyId);
        Mockito.when(categoryService.getCategoryById(category1.getId())).thenReturn(category1);
        //Act
        ResponseEntity<Object> result = controller.getCategoryInfo("10");
        //Assert
        assertNotEquals("Category does not exist", result.getBody());
    }

    @Test
    @DisplayName("Failure - the service returns an exception.")
    void getCategoryInfo_exception() {
        //Arrange
        String familyId = "12";
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);
        category1.setFamilyId(familyId);
        Mockito.when(categoryService.getCategoryById("1")).thenThrow(NoSuchElementException.class);
        //Act
        ResponseEntity<Object> result = controller.getCategoryInfo("1");
        //Assert
        assertEquals("Category does not exist", result.getBody());
    }

    @Test
    @DisplayName("Failure - the family id is null.")
    void getCategoryInfo_categoryIdNull() {
        //Arrange

        CategoryDTO category1 = new CategoryDTO();
        category1.setId(null);
        category1.setName("Health");
        category1.setParentId(null);
        category1.setFamilyId(null);
        //Act
        ResponseEntity<Object> result = controller.getCategoryInfo(null);
        //Assert
        assertNotEquals("Category does not exist", result.getBody());
    }*/
}
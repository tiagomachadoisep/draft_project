package switchtwentytwenty.project.controllers.implcontrollers;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextImpl;
import switchtwentytwenty.project.applicationservices.iappservices.IAuthenticationService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//@SpringBootTest
class AddMemberControllerTest {

    //@Mock
    private final IPersonService iPersonServiceMock = Mockito.mock(IPersonService.class);

    private final AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);

    private final IPersonAssembler iPersonAssembler = Mockito.mock(PersonAssembler.class);

    private final IAuthenticationService iAuthenticationService = Mockito.mock(IAuthenticationService.class);

    private final ISecurityContextProvider securityContextProvider = Mockito.mock(ISecurityContextProvider.class);
    //@InjectMocks
    private final AddMemberController controller = new AddMemberController(iPersonServiceMock, assemblerToDTOMock, securityContextProvider, iAuthenticationService);

    private String adminEmail;

    @BeforeEach
    void setUpContext() {
        adminEmail = "ze@isep.pt";
        String password = String.valueOf(218471742);
        //SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(adminEmail,password));
        Mockito.when(securityContextProvider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(adminEmail, password)));
        Mockito.when(iAuthenticationService.isCurrentUserInFamily("20")).thenReturn(true);
    }

    @Test
    void whenAddPerson_returnPersonDTOWithAllInfo() {
        //String adminEmail = "ze@isep.pt";
        //String password = String.valueOf(218471742);
        String name = "luis";
        int vat = 123456789;
        String emailAddress = "luis@isep.pt";
        String address = "Rua de cima";
        String familyID = "20";
        String birthDate = "01-01-1990";
        PersonDTO personDTO = new PersonDTO(familyID, emailAddress, name, vat, address, birthDate);
        PersonID personID = new PersonID(new Email(emailAddress));

        Mockito.when(iPersonServiceMock.addMember(personDTO)).thenReturn(personID);

        //SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(adminEmail,password));

        ResponseEntity<Object> result = controller.addPerson(personDTO, familyID);

        assertEquals(personDTO, result.getBody());

    }

    @Test
    void tryToAddPersonWithInvalidName_catchException() {
        String name = "";
        int vat = 123456789;
        String emailAddress = "luis@isep.pt";
        String address = "Rua de cima";
        String familyID = "20";
        String birthDate = "01-01-1990";
        PersonDTO personDTO = new PersonDTO(familyID, emailAddress, name, vat, address, birthDate);
        PersonID personID = new PersonID(new Email(emailAddress));

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Invalid parameters - Invalid name format");

        Link familyLink = linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family");
        expectedExceptionDTO.add(familyLink);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);


        Mockito.when(iPersonServiceMock.addMember(personDTO)).thenThrow(IllegalArgumentException.class);

        Mockito.when(assemblerToDTOMock.toExceptionDto("Invalid parameters - null")).thenReturn(expectedExceptionDTO);

        ResponseEntity<Object> result = controller.addPerson(personDTO, familyID);

        assertEquals(expected, result);
    }

    /*@Test
    void whenTryToAddExistingMember_catchException(){
        String name = "luis";
        int vat = 123456789;
        String emailAddress = "luis@isep.pt";
        String address = "Rua de cima";
        String familyID = "20";
        String birthDate = "01-01-1990";
        PersonDTO personDTO = new PersonDTO(familyID, emailAddress, name, vat, address, birthDate);
        PersonID personID = new PersonID(new Email(emailAddress));





    }*/

    @Test
    void tryToGetAllFamilyMembers() {
        PersonDTO personDTOOne = new PersonDTO("2", "luis@isep.pt", "luis"
                , 0, "Rua tal", "01-01-1986");
        PersonDTO personDTOTwo = new PersonDTO("2", "mimi@isep.pt", "mimi"
                , 0, "Rua tal", "01-01-2016");

        List<PersonDTO> personDTOList = new ArrayList<>();
        personDTOList.add(personDTOOne);
        personDTOList.add(personDTOTwo);

        Mockito.when(iPersonServiceMock.getFamilyMembers("20")).thenReturn(personDTOList);

        ResponseEntity<Object> result = controller.getFamilyMembers("20");

        assertEquals(personDTOList, result.getBody());
    }

    @Test
    void tryToGetAllMembers() {

        PersonDTO personDTOOne = new PersonDTO("2", "luis@isep.pt", "luis"
                , 0, "Rua tal", "01-01-1986");
        PersonDTO personDTOTwo = new PersonDTO("2", "mimi@isep.pt", "mimi"
                , 0, "Rua tal", "01-01-2016");

        List<PersonDTO> personDTOList = new ArrayList<>();
        personDTOList.add(personDTOOne);
        personDTOList.add(personDTOTwo);

        Mockito.when(iPersonServiceMock.getAllMembers()).thenReturn(personDTOList);

        ResponseEntity<Object> result = controller.getAllMembers();

        assertEquals(personDTOList, result.getBody());
    }

    /*
    @Test
    void testSpyMockito(){
        String name = "luis";
        int vat = 123456789;
        String emailAddress ="luis@isep.pt";
        String address = "Rua de cima";
        String familyID = "20";
        String birthDate = "01-01-1990";
        PersonDTO personDTO = new PersonDTO(familyID, emailAddress, name, vat, address, birthDate);

        AddMemberController addMemberControllerSpy = Mockito.spy(new AddMemberController(iPersonServiceMock));

        addMemberControllerSpy.addPerson(personDTO);
        verify(addMemberControllerSpy, times(1)).addLinksToDTO(personDTO);
    }
  */

    @Test
    void whenAddPerson_notInSameFamily() {
        String name = "luis";
        int vat = 123456789;
        String emailAddress = "luis@isep.pt";
        String address = "Rua de cima";
        String familyID = "30";
        String birthDate = "01-01-1990";
        PersonDTO personDTO = new PersonDTO(familyID, emailAddress, name, vat, address, birthDate);
        ExceptionDTO exceptionDTO = new ExceptionDTO("Forbidden - not your family");
        exceptionDTO.add(linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family"));

        Mockito.when(iPersonServiceMock.addMember(personDTO)).thenReturn(new PersonID());
        Mockito.when(assemblerToDTOMock.toExceptionDto("Forbidden - not your family")).thenReturn(new ExceptionDTO("Forbidden - not your family"));

        ResponseEntity<Object> result = controller.addPerson(personDTO, familyID);

        assertEquals(exceptionDTO, result.getBody());

    }

    @Test
    void getMemberByFamilyId() {
        String mainEmail = "zoro@isep.pt";
        String familyID = "123";

        Mockito.when(iPersonServiceMock.getMemberFamilyById(mainEmail)).thenReturn(familyID);

        //ACT
        ResponseEntity<Object> actual = controller.getMemberFamilyById(mainEmail);

        //ASSERT
        assertEquals(familyID, actual.getBody());

    }

}
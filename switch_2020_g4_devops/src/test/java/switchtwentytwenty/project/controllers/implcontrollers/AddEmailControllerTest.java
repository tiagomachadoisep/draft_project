package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.utils.Result;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

//@SpringBootTest
class AddEmailControllerTest {
/*

    @Mock
    private IPersonService iPersonService;
    @Mock
    private AssemblerToDTO assemblerToDTOMock;

    @InjectMocks
    private AddEmailController emailController;

    //--------------------------------------------------Unit Tests--------------------------------------------------

    @Test
    @DisplayName("Success - unit test add valid email")
    void addEmailAccountToProfile_Success() {
        //Arrange
        String mainEmail = "sraCastro1970@hotmail.com";
        String secondaryEmail = "sraCastro@hotmail.com";
        EmailDTO emailDTO = new EmailDTO(secondaryEmail,mainEmail);
        EmailDTO inputDTO = new EmailDTO(secondaryEmail);
        PersonDTO personDTO = new PersonDTO(mainEmail);
        EmailDTO outputDTO = new EmailDTO(secondaryEmail);

        Mockito.when(iPersonService.getPersonDTO(mainEmail)).thenReturn(personDTO);
        Mockito.when(assemblerToDTOMock.toEmailDTO(secondaryEmail, mainEmail)).thenReturn(emailDTO);
        Mockito.when(iPersonService.addEmailToProfile(emailDTO)).thenReturn(Result.success(secondaryEmail));
        Mockito.when(assemblerToDTOMock.toEmailDTO(secondaryEmail)).thenReturn(outputDTO);

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(inputDTO, mainEmail);

        //Assert
        assertThat(result.getBody()).isEqualTo(outputDTO);
    }

    @Test
    @DisplayName("Failure - unit test add the same email")
    void addEmailAccountToProfile_SameEmail() {
        //Arrange
        String mainEmail = "sraCastro1970@hotmail.com";
        String secondaryEmail = "sraCastro1970@hotmail.com";
        EmailDTO emailDTO = new EmailDTO(secondaryEmail, mainEmail);
        String expected = "This email can't be added, because it was defined as your main email.";
        PersonDTO personDTO = new PersonDTO(mainEmail);
        EmailDTO outputDTO = new EmailDTO(secondaryEmail);


        Mockito.when(iPersonService.getPersonDTO(mainEmail)).thenReturn(personDTO);
        Mockito.when(assemblerToDTOMock.toEmailDTO(secondaryEmail, mainEmail)).thenReturn(emailDTO);
        Mockito.when(iPersonService.addEmailToProfile(emailDTO)).thenReturn(Result.failure("This email can't be added, because it was defined as your main email."));

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(outputDTO, mainEmail);

        //Assert
        assertThat(result.getBody()).isEqualTo(expected);
    }

    @Test
    @DisplayName("Failure - try to add an invalid email")
    void addEmailAccountToProfile_InvalidEmail() {
        //Arrange
        String mainEmail = "sraCastro1970@hotmail.com";
        String secondaryEmail = "sraCastro1971@hotmail.com";
        EmailDTO emailDTO = new EmailDTO(secondaryEmail, mainEmail);
        String expected = "This email can't be added.";
        PersonDTO personDTO = new PersonDTO(mainEmail);
        EmailDTO outputDTO = new EmailDTO(secondaryEmail);


        Mockito.when(iPersonService.getPersonDTO(mainEmail)).thenReturn(personDTO);
        Mockito.when(assemblerToDTOMock.toEmailDTO(secondaryEmail, mainEmail)).thenReturn(emailDTO);
        Mockito.when(iPersonService.addEmailToProfile(emailDTO)).thenReturn(Result.failure("Invalid parameters."));

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(outputDTO, mainEmail);

        //Assert
        assertThat(result.getBody()).isNotEqualTo(expected);
    }

    @Test
    @DisplayName("Failure - unit test empty email")
    void addEmailAccountToProfile_EmptyEmail() {
        //Arrange
        String mainEmail = "sraCastro1970@hotmail.com";
        String secondaryEmail = "";
        EmailDTO emailDTO = new EmailDTO(secondaryEmail, mainEmail);
        String expected = "Invalid parameters.";
        PersonDTO personDTO = new PersonDTO(mainEmail);
        EmailDTO outputDTO = new EmailDTO(secondaryEmail);


        Mockito.when(iPersonService.getPersonDTO(mainEmail)).thenReturn(personDTO);
        Mockito.when(assemblerToDTOMock.toEmailDTO(secondaryEmail, mainEmail)).thenReturn(emailDTO);
        Mockito.when(iPersonService.addEmailToProfile(emailDTO)).thenReturn(Result.failure("Invalid parameters."));

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(outputDTO, mainEmail);

        //Assert
        assertThat(result.getBody()).isEqualTo(expected);
    }

    @Test
    @DisplayName("Failure - unit test null email")
    void addEmailAccountToProfile_NullEmail() {
        //Arrange
        String mainEmail = "sraCastro1970@hotmail.com";
        EmailDTO emailDTO = new EmailDTO(null, mainEmail);
        String expected = "Invalid parameters.";

        Mockito.when(iPersonService.getPersonDTO(mainEmail)).thenReturn(null);
        Mockito.when(assemblerToDTOMock.toEmailDTO(null, mainEmail)).thenReturn(null);

        Mockito.when(iPersonService.addEmailToProfile(emailDTO)).thenReturn(Result.failure("Invalid parameters."));

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(null, mainEmail);

        //Assert
        assertThat(result.getBody()).isEqualTo(expected);
    }

    //----------------- show other emails  unit tests --------------------

    @Test
    @DisplayName("Success - shows the list of other emails")
    void showOtherEmails_success() {
        String mainEmail = "clotildes@gmail.com";
        String otherEmail = "clot123@hotmail.com";
        String oneMoreEmail = "clo@gmail.com";

        List<String> otherEmails = new ArrayList<>();
        otherEmails.add(otherEmail);
        otherEmails.add(oneMoreEmail);
        EmailDTO emailDTO = new EmailDTO(mainEmail);
        emailDTO.setEmailsList(otherEmails);

        Mockito.when(iPersonService.getOtherEmailsList(mainEmail)).thenReturn(otherEmails);
        ResponseEntity<Object> result = emailController.showOtherEmails(mainEmail);
        //Assert
        assertNotNull(result.getBody());
    }

    /*
    @Test
    @DisplayName("Success - the person doesn't has other emails ")
    void showOtherEmails_emptyList() {
        String mainEmail = "joselma@gmail.com";
        List<String>expected = new ArrayList<>();
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmailsList(expected);

        Mockito.when(iPersonService.getOtherEmailsList(mainEmail)).thenReturn(new ArrayList<>());

        ResponseEntity<Object> result = emailController.showOtherEmails(mainEmail);
        //Assert
        assertEquals(emailDTO, result.getBody());
    }

    @Test
    @DisplayName("Failure - main email is null ")
    void showOtherEmails_failure() {
        String mainEmail = null;
        Mockito.when(iPersonService.getOtherEmailsList(mainEmail)).thenThrow(IllegalArgumentException.class);
        String expected = "Invalid id";
        ResponseEntity<Object> result = emailController.showOtherEmails(mainEmail);
        assertThat(result.getBody()).isEqualTo(expected);
    }
*/

}



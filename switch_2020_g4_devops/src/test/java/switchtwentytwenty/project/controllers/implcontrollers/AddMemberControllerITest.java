package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.parameters.P;
import switchtwentytwenty.project.applicationservices.iappservices.IAuthenticationService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.applicationservices.implappservices.AuthenticationService;
import switchtwentytwenty.project.applicationservices.implappservices.PersonService;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddMemberControllerITest {

    private final IPersonRepository personRepository = Mockito.mock(IPersonRepository.class);
    private final ISecurityContextProvider provider = Mockito.mock(ISecurityContextProvider.class);

    private final IPersonService service = new PersonService(personRepository,new PersonAssembler());
    private final IAuthenticationService authenticationService = new AuthenticationService(provider,service);

    private final AddMemberController ctrl = new AddMemberController(service,new AssemblerToDTO(),provider,authenticationService);

    @Test
    void addMember(){
        String userEmail = "ze@isep.pt";
        PersonID userID = new PersonID(new Email(userEmail));

        String name = "luis";
        int vat = 123456789;
        String emailAddress = "luis@isep.pt";
        String address = "Rua de cima";
        String familyID = "20";
        String birthDate = "01-01-1990";
        PersonDTO personDTO = new PersonDTO("0", emailAddress, name, vat, address, birthDate);
        PersonID personID = new PersonID(new Email(emailAddress));

        Person user = new Person.Builder(userID).setFamilyId(new FamilyID(familyID)).setName(new PersonName("Ze")).setAddress(new Address("other")).
                setBirthDate(new BirthDate("01-01-1900")).setVat(new VATNumber(0)).setRoleAsFamilyAdministrator().build();

        Mockito.when(personRepository.findByID(userID)).thenReturn(Optional.of(user));
        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.empty());
        Mockito.when(personRepository.findFamilyByPersonId(userEmail)).thenReturn("20");
        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(userEmail,"1234")));


        ResponseEntity response = ctrl.addPerson(personDTO,familyID);

        assertEquals(personDTO,response.getBody());
    }
}

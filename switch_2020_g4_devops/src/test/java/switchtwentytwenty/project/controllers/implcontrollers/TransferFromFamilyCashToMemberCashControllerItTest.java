package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.LedgerService;
import switchtwentytwenty.project.applicationservices.irepositories.*;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.FamilyCashAccount;
import switchtwentytwenty.project.domain.aggregates.account.PersonalCashAccount;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.domainservices.ILedgerAccountService;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.TransferDTO;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//@SpringBootTest
public class TransferFromFamilyCashToMemberCashControllerItTest {
/*
    @Mock
    private ILedgerRepository ledgerRepository;
    @Mock
    private IAccountRepository accountRepository;
    @Mock
    private IPersonRepository personRepository;
    @Mock
    private ICategoryRepository categoryRepository;
    @Mock
    private IFamilyRepository familyRepository;
    @Autowired
    private ILedgerAccountService ledgerAccountService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void transfer(){
        //Arrange
        LedgerService ledgerService=new LedgerService(ledgerRepository,accountRepository,personRepository,categoryRepository,familyRepository,ledgerAccountService,new AssemblerToDTO());

        AssemblerToDTO mapper=new AssemblerToDTO();
        TransferFromFamilyCashToMemberCashController ctrl = new TransferFromFamilyCashToMemberCashController(ledgerService, mapper);

        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));

        String otherEmail = "carlos@isep.pt";
        PersonID otherID = new PersonID(new Email(otherEmail));

        LocalDate date = LocalDate.of(2020, 1, 1);

        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();

        double balance = 20;

        String categoryId = "1";

        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(100))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash do ze")).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("2")).setAccountType().setBalance(new MoneyValue(0))
                .setDescription(new Description("cash do carlos")).setOwnerID(otherID).build();


        Ledger familyLedger = new Ledger(1, mainEmail, ELedgerType.FAMILY);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(familyRepository.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(new FamilyID("1"));
        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepository.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(personRepository.hasSameFamily(personID, otherID)).thenReturn(true);
        Mockito.when(personRepository.hasSameFamily(otherID, personID)).thenReturn(true);

        Mockito.when(accountRepository.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepository.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepository.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(accountRepository.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepository.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerRepository.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(categoryRepository.existsID(new CategoryID(categoryId))).thenReturn(true);

        TransferDTO in = new TransferDTO(mainEmail,otherEmail,balance,categoryId,date);

        //Act
        ResponseEntity<Object> result = ctrl.transferCashFromFamily(in);

        //Assert
        assertNotNull(result.getBody());
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(familyAccount.getBalance(),new MoneyValue(80));
        assertEquals(otherAccount.getBalance(),new MoneyValue(20));
    }

    @Test
    void transferFail(){
        //Arrange
        LedgerService ledgerService = new LedgerService(ledgerRepository, accountRepository, personRepository, categoryRepository, familyRepository, ledgerAccountService, new AssemblerToDTO());

        AssemblerToDTO mapper = new AssemblerToDTO();
        TransferFromFamilyCashToMemberCashController ctrl = new TransferFromFamilyCashToMemberCashController(ledgerService, mapper);

        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));

        String otherEmail = "carlos@isep.pt";
        PersonID otherID = new PersonID(new Email(otherEmail));
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();

        double balance = 2000;

        String categoryId = "1";

        LocalDate date = LocalDate.of(2020, 1, 1);

        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(100))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash do ze")).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("2")).setAccountType().setBalance(new MoneyValue(0))
                .setDescription(new Description("cash do carlos")).setOwnerID(otherID).build();


        Ledger familyLedger = new Ledger(1, mainEmail, ELedgerType.FAMILY);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(familyRepository.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(new FamilyID("1"));
        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepository.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(personRepository.hasSameFamily(personID, otherID)).thenReturn(true);
        Mockito.when(personRepository.hasSameFamily(otherID, personID)).thenReturn(true);

        Mockito.when(accountRepository.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepository.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepository.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(accountRepository.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepository.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerRepository.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(categoryRepository.existsID(new CategoryID(categoryId))).thenReturn(true);

        TransferDTO in = new TransferDTO(mainEmail,otherEmail,balance,categoryId,date);

        //Act
        ResponseEntity<Object> result = ctrl.transferCashFromFamily(in);

        //Assert
        assertEquals("Not enough balance", result.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }*/
}

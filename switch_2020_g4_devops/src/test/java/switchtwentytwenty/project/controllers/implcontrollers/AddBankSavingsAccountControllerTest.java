package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class AddBankSavingsAccountControllerTest {

    private final IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    private final AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);
    private final AddBankSavingsAccountController ctrl = new AddBankSavingsAccountController(assemblerToDTOMock, iAccountServiceMock);

    @Test
    void successByAddingBankSavingsAccount() {
        String id = "marta@yo.com";
        AccountDTO dto = new AccountDTO();
        dto.setOwnerId(id);
        dto.setDescription("Millenium");
        dto.setBalance(9000);

        AccountDTO expected = new AccountDTO();
        expected.setAccountId("1");
        expected.setOwnerId(id);
        expected.setDescription("Millenium");
        expected.setBalance(9000);

        Mockito.when(assemblerToDTOMock.inputToAccountDTO(id, dto)).thenReturn(dto);
        Mockito.when(iAccountServiceMock.addBankSavingsAccount(dto)).thenReturn(expected);

        //ACT
        ResponseEntity<Object> result = ctrl.addBankSavingsAccount(id, dto);

        //ASSERT
        assertEquals(result.getBody(), expected);

    }

    @Test
    void failAddingBankAccount_badDescription() {
        //ARRANGE
        String id = "id@isep.pt";
        AccountDTO dto = new AccountDTO();
        dto.setOwnerId(id);
        dto.setDescription("Mi");
        dto.setBalance(100);

        Link allMemberAccountsLink = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(dto.getOwnerId())).withRel("All Member accounts");

        IllegalArgumentException exception = new IllegalArgumentException();
        String responseMessage = "Cannot add Bank Savings Account - " + exception.getMessage();
        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);
        exceptionDTO.add(allMemberAccountsLink);

        Mockito.when(assemblerToDTOMock.inputToAccountDTO(id, dto)).thenReturn(dto);
        Mockito.when(iAccountServiceMock.addBankSavingsAccount(dto)).thenThrow(exception);
        Mockito.when(assemblerToDTOMock.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);
        //ACT
        ResponseEntity<Object> result = ctrl.addBankSavingsAccount(id, dto);

        //ASSERT
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

}
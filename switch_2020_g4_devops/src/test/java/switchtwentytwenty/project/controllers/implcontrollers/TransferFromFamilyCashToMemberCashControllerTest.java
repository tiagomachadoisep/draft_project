package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.dto.TransferDTO;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class TransferFromFamilyCashToMemberCashControllerTest {

    private ILedgerService ledgerService = Mockito.mock(ILedgerService.class);
    private TransferFromFamilyCashToMemberCashController ctrl = new TransferFromFamilyCashToMemberCashController(ledgerService,null);


    @Test
    void transferCashFromFamily() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        double balance = 20;
        String categoryId = "1";
        LocalDate date = LocalDate.now();

        TransferDTO in = new TransferDTO(mainEmail,otherEmail,balance,categoryId,date);

        TransferDTO out = new TransferDTO(mainEmail,otherEmail,balance,categoryId,"1","2","2",date);
        TransferDTO outT = new TransferDTO(mainEmail,otherEmail,balance,categoryId,"1","2","2",date);

        Mockito.when(ledgerService.transferCashFromFamilyToMember(in)).thenReturn(out);

        Link accountLink = linkTo(methodOn(CheckAccountBalanceController.class).checkAccountBalance(outT.getAccountId()))
                .withRel("Account balance");

        outT.add(accountLink);

        //Act
        ResponseEntity<Object> result = ctrl.transferCashFromFamily(in);

        //Assert

        assertEquals(result.getBody(), outT);
        assertNotSame(outT, result.getBody());

    }

    @Test
    void transferCashFromFamilyFail() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        double balance = 20;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020,1,1);

        TransferDTO in = new TransferDTO(mainEmail,otherEmail,balance,categoryId,date);

        TransferDTO out = new TransferDTO("Not enough balance");

        Mockito.when(ledgerService.transferCashFromFamilyToMember(in)).thenReturn(out);

        //Act
        ResponseEntity<Object> result = ctrl.transferCashFromFamily(in);

        //Assert
        assertEquals("Not enough balance", result.getBody());
    }
}
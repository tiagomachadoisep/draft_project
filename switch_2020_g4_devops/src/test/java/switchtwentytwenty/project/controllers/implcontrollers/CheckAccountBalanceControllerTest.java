package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AccountDTOOUT;
import switchtwentytwenty.project.dto.AssemblerToDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CheckAccountBalanceControllerTest {
    private static String ownerEmail;
    private static PersonID personID;
    private static Ledger ledger;
    private static AccountDTO accountDTOOne;
    private static AccountDTO accountDTOTwo;
    private static AccountDTO accountDTOThree;
    private static List<AccountDTO> accountDTOList;
    private static List<AccountDTOOUT> accountDTOOUTList;
    private static AccountDTOOUT accountDTOOUTOne;
    private static AccountDTOOUT accountDTOOUTTwo;
    private static AccountDTOOUT accountDTOOUTThree;
    private static List<AccountDTO> personAccounts;
    private static final String WRONG_INFORMATION = "Wrong Information";

    private final IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    private final ILedgerService iLedgerServiceMock = Mockito.mock(ILedgerService.class);
    private final AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);
    private final CheckAccountBalanceController controller = new CheckAccountBalanceController(iAccountServiceMock, iLedgerServiceMock, assemblerToDTOMock);

    void setUp() {
        ownerEmail = "luis@isep.pt";
        personID = new PersonID(new Email(ownerEmail));
        LedgerID ledgerID = new LedgerID();
        ledger = new Ledger(ledgerID.getLedgerIdValue(), ownerEmail);
        accountDTOOne = new AccountDTO("luis@isep.pt", "CC Luis", 50, "20", EAccountType.CREDIT_CARD);
        accountDTOTwo = new AccountDTO("mimi@isep.pt", "CC Mimi", 100, "6", EAccountType.CREDIT_CARD);
        accountDTOThree = new AccountDTO("luis@isep.pt", "CC Martim", 80, "14", EAccountType.CREDIT_CARD);

        accountDTOList = new ArrayList<>();
        accountDTOList.add(accountDTOOne);
        accountDTOList.add(accountDTOTwo);
        accountDTOList.add(accountDTOThree);

        personAccounts = new ArrayList<>();
        personAccounts.add(accountDTOOne);
        personAccounts.add(accountDTOThree);

        accountDTOOUTList = new ArrayList<>();
        accountDTOOUTOne = new AccountDTOOUT(accountDTOOne.getDescription());
        accountDTOOUTTwo = new AccountDTOOUT(accountDTOTwo.getDescription());
        accountDTOOUTThree = new AccountDTOOUT(accountDTOThree.getDescription());
    }

    @Test
    void whenTryToGetAllPersonAccount_getAccountsByOwnerID_Success() {
        setUp();

        Mockito.when(iAccountServiceMock.findAllAccountsByOwner(personID))
                .thenReturn(personAccounts);
        Mockito.when(iLedgerServiceMock.findLedgerByOwnerID(personID)).thenReturn(Optional.ofNullable(ledger));
        Mockito.when(assemblerToDTOMock.toAccountDtoOut(accountDTOOne)).thenReturn(accountDTOOUTOne);
        Mockito.when(assemblerToDTOMock.toAccountDtoOut(accountDTOTwo)).thenReturn(accountDTOOUTTwo);
        Mockito.when(assemblerToDTOMock.toAccountDtoOut(accountDTOThree)).thenReturn(accountDTOOUTThree);

        accountDTOOUTList.add(accountDTOOUTOne);
        accountDTOOUTList.add(accountDTOOUTThree);

        ResponseEntity<Object> result = controller.getAccountsByOwnerID(ownerEmail);

        assertEquals(accountDTOOUTList, result.getBody());
    }


    @Test
    void whenTryToGetAllPersonAccounts_getAccountsByOwnerID_FailEmptyID() {

        ResponseEntity<Object> result = controller.getAccountsByOwnerID("");

        assertEquals(WRONG_INFORMATION, result.getBody());
    }

    @Test
    void withAccountID_checkAccountBalance_Success() {
        setUp();

        double expected = accountDTOThree.getBalance();
        Mockito.when(iAccountServiceMock.checkAccountBalance(new AccountID("14"))).thenReturn(new MoneyValue(80));

        ResponseEntity<Object> result = controller.checkAccountBalance("14");

        assertEquals(expected, result.getBody());
    }


    @Test
    void withAccountID_checkAccountBalance_FailEmptyID() {
        setUp();

        ResponseEntity<Object> result = controller.checkAccountBalance("");

        assertEquals(WRONG_INFORMATION, result.getBody());
    }

    @Test
    void withAccountID_checkAccountBalance_FailNullID() {
        setUp();

        ResponseEntity<Object> result = controller.checkAccountBalance(null);

        assertEquals(WRONG_INFORMATION, result.getBody());
    }

    @Test
    void withAccountID_getAccount_Success() {
        setUp();

        Optional<AccountDTO> expected = Optional.of(accountDTOTwo);
        Mockito.when(iAccountServiceMock.getAccountByID("mimi@isep.pt")).thenReturn(expected);

        ResponseEntity<Object> result = controller.getAccountByID("mimi@isep.pt");

        assertEquals(expected, result.getBody());
    }

    @Test
    void withAccountID_getAccount_Fail() {
        setUp();

        Mockito.when(iAccountServiceMock.getAccountByID("")).thenThrow(IllegalArgumentException.class);
        ResponseEntity<Object> result = controller.getAccountByID("");

        assertEquals(WRONG_INFORMATION, result.getBody());
    }

    @Test
    void getAllAccounts() {
        setUp();

        Mockito.when(iAccountServiceMock.getAllAccounts()).thenReturn(accountDTOList);
        ResponseEntity<Object> result = controller.getAllAccounts();

        assertEquals(accountDTOList, result.getBody());
    }

    @Test
    void getAllFamilyAccounts() {
        setUp();

        Mockito.when(iAccountServiceMock.getFamilyAccounts("1")).thenReturn(accountDTOList);
        ResponseEntity<Object> result = controller.getFamilyAccounts("1");

        assertEquals(accountDTOList, result.getBody());
    }

    @Test
    void noLedgerForTheUser() {
        //ARRANGE
        setUp();
        Mockito.when(iLedgerServiceMock.findLedgerByOwnerID(personID)).thenReturn(Optional.empty());

        //ACT + Assert
        assertThrows(NullPointerException.class, () -> {
            controller.findLedgerIDByOwnerID(ownerEmail);
        });
    }

    @Test
    void wrongUserEmail() {
        //ARRANGE
        setUp();

        String expected = "MemberID not valid";

        //ACT
        ResponseEntity<Object> result = controller.findLedgerIDByOwnerID("kdsmodsrhf");

        //Assert
        assertEquals(expected, result.getBody());
    }

    @Test
    void getAllAccounts_test(){
        setUp();

        List<AccountDTO> expected = accountDTOList;

        Mockito.when(iAccountServiceMock.getAllAccounts()).thenReturn(accountDTOList);
        ResponseEntity<Object> result = controller.getAllAccounts();

        assertEquals(expected,result.getBody());


    }
}
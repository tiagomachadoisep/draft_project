package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddBankAccountControllerTest {

    private final IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    private final AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);
    private final AddBankAccountController ctrl = new AddBankAccountController(assemblerToDTOMock, iAccountServiceMock);

    @Test
    void successByAddingBankAccount() {
        //ARRANGE
        String id = "marta@yo.com";
        AccountDTO dto = new AccountDTO();
        dto.setOwnerId(id);
        dto.setDescription("Millenium");
        dto.setBalance(100);

        AccountDTO expected = new AccountDTO();
        expected.setAccountId("1");
        expected.setOwnerId(id);
        expected.setDescription("Millenium");
        expected.setBalance(100);


        Mockito.when(assemblerToDTOMock.inputToAccountDTO(id, dto)).thenReturn(dto);
        Mockito.when(iAccountServiceMock.addBankAccount(dto)).thenReturn(expected);

        //ACT
        ResponseEntity<Object> result = ctrl.addBankAccount(id, dto);

        //assert
        assertEquals(result.getBody(), expected);
    }

    @Test
    void failAddingBankAccount_ownerIdNotPresent() {
        //ARRANGE
        String id = null;
        AccountDTO dto = new AccountDTO();
        dto.setOwnerId(id);
        dto.setDescription("Millenium");
        dto.setBalance(100);

        String expected = "Invalid parameters";

        Mockito.when(assemblerToDTOMock.inputToAccountDTO(id, dto)).thenThrow(IllegalArgumentException.class);

        //ACT
        ResponseEntity<Object> result = ctrl.addBankAccount(id, dto);

        //assert
        assertEquals(result.getBody(), expected);
    }

}
package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.utils.Result;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


class CreateFamilyRelationshipControllerTest {


    private IFamilyRelationshipService familyRelationshipServiceMock = Mockito.mock(IFamilyRelationshipService.class);

    private AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);

    private CreateFamilyRelationshipController controller =
            new CreateFamilyRelationshipController(familyRelationshipServiceMock, assemblerToDTOMock);

    private void addLinksToDto(RelationshipDTO relationshipDTO, String familyId) {

        Link selfLink = linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getRelationship(relationshipDTO.getFamilyId(), relationshipDTO.getRelationshipId()))
                .withSelfRel();

        Link changeRelLink = linkTo(ChangeRelationshipController.class).slash("families").slash(familyId).
                slash("relations").slash(relationshipDTO.getRelationshipId()).withRel("changeRel");

        Link familyLink = linkTo(methodOn(AddFamilyController.class).getFamily(relationshipDTO.getFamilyId())).
                withRel("family");

        Link personOneLink = linkTo(methodOn(GetProfileInfoController.class)
                .getProfileInfo(relationshipDTO.getFirstPersonEmail())).withRel("first person");

        Link personTwoLink = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(relationshipDTO.
                getSecondPersonEmail())).withRel("second person");

        Link relationsLink = linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getAllRelationships(familyId)).withRel("relationships");

        relationshipDTO.add(selfLink);
        relationshipDTO.add(changeRelLink);
        relationshipDTO.add(familyLink);
        relationshipDTO.add(personOneLink);
        relationshipDTO.add(personTwoLink);
        relationshipDTO.add(relationsLink);
    }

    private void addLinksToExceptionDTO(ExceptionDTO exceptionDTO, String familyId) {
        Link familyLink = linkTo(methodOn(AddFamilyController.class).getFamily(familyId)).
                withRel("family");
        Link relationsLink = linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getAllRelationships(familyId)).withRel("relationships");

        exceptionDTO.add(familyLink);
        exceptionDTO.add(relationsLink);
    }


    @Test
    void newFamilyRelationship_successCase() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        RelationshipDTO serviceOutputDTO = new RelationshipDTO();
        serviceOutputDTO.setFirstPersonEmail(firstPersonEmail);
        serviceOutputDTO.setSecondPersonEmail(secondPersonEmail);
        serviceOutputDTO.setFamilyId(id);
        serviceOutputDTO.setRelationshipId("relationshipID_2134221");

        RelationshipDTO expectedDTO = new RelationshipDTO();
        expectedDTO.setFirstPersonEmail(firstPersonEmail);
        expectedDTO.setSecondPersonEmail(secondPersonEmail);
        expectedDTO.setFamilyId(id);
        expectedDTO.setRelationshipId("relationshipID_2134221");

        addLinksToDto(expectedDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedDTO, HttpStatus.CREATED);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.success(serviceOutputDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_relationshipBetweenMembersAlreadyExists() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "A relationship between the members already exists.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "A relationship between the members already exists.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_membersDoNotBelongToTheSameFamily() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The persons do not belong to the same family.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The persons do not belong to the same family.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_membersDoNotExist() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The person(s) do not exist.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The person(s) do not exist.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_TheFamilyDoesNotExist() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "A family with the passed ID does not exist.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "A family with the passed ID does not exist.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_designationIsInvalid() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "fatherOKOK";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The relationship designation '" + designationFather + "' is invalid.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The relationship designation '" + designationFather + "' is invalid.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_designationIsEmpty() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The relationship designation cannot be empty.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The relationship designation cannot be empty.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_designationIsNull() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The relationship designation cannot be null.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The relationship designation cannot be null.");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_InvalidPersonTwoEmail() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@@@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "Invalid Email");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "Invalid Email");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_InvalidPersonOneEmail() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ze@@@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(id);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "Invalid Email");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "Invalid Email");

        addLinksToExceptionDTO(expectedExceptionDTO, id);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, id, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void newFamilyRelationship_failCase_nullFamilyId() {
        //arrange
        String famId = null;
        String firstPersonEmail = "ze@@@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationFather);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(famId);
        relationshipDTO.setDesignation(designationFather);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The family Id cannot be null.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "The family Id cannot be null.");

        addLinksToExceptionDTO(expectedExceptionDTO, famId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assemblerToDTOMock.toRelationshipDto(firstPersonEmail, secondPersonEmail, famId, designationFather)).
                thenReturn(relationshipDTO);
        Mockito.when(familyRelationshipServiceMock.createAndSaveRelationship(relationshipDTO)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, famId);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void getRelationship_successCase() {

        String stringFamId = "fam123";
        String stringRelId = "rel123";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(stringFamId);
        relationshipDTO.setRelationshipId(stringRelId);
        relationshipDTO.setDesignation("sister");
        relationshipDTO.setFirstPersonEmail("one@gmail.com");
        relationshipDTO.setSecondPersonEmail("second@gmail.com");

        RelationshipDTO expectedDTO = new RelationshipDTO();
        expectedDTO.setFirstPersonEmail("one@gmail.com");
        expectedDTO.setSecondPersonEmail("second@gmail.com");
        expectedDTO.setFamilyId(stringFamId);
        expectedDTO.setRelationshipId(stringRelId);
        expectedDTO.setDesignation("sister");

        addLinksToDto(expectedDTO, stringFamId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedDTO, HttpStatus.OK);

        Mockito.when(familyRelationshipServiceMock.getRelationshipDto(stringFamId, stringRelId)).
                thenReturn(Result.success(relationshipDTO));

        ResponseEntity<Object> result = controller.getRelationship(stringFamId, stringRelId);

        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void getRelationship_failCase_RelationshipNotFound() {

        String stringFamId = "fam123";
        String stringRelId = "rel123";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(stringFamId);
        relationshipDTO.setRelationshipId(stringRelId);
        relationshipDTO.setDesignation("sister");
        relationshipDTO.setFirstPersonEmail("one@gmail.com");
        relationshipDTO.setSecondPersonEmail("second@gmail.com");

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "There is no relationship with that id.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "There is no relationship with that id.");

        addLinksToExceptionDTO(expectedExceptionDTO, stringFamId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(familyRelationshipServiceMock.getRelationshipDto(stringFamId, stringRelId)).
                thenReturn(Result.failure(serviceExceptionDTO));


        ResponseEntity result = controller.getRelationship(stringFamId, stringRelId);

        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void getRelationship_failCase_FamilyNotFound() {

        String stringFamId = "fam123";
        String stringRelId = "rel123";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(stringFamId);
        relationshipDTO.setRelationshipId(stringRelId);
        relationshipDTO.setDesignation("sister");
        relationshipDTO.setFirstPersonEmail("one@gmail.com");
        relationshipDTO.setSecondPersonEmail("second@gmail.com");

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "There is no family with that id.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Relationship creation failed! " +
                        "There is no family with that id.");

        addLinksToExceptionDTO(expectedExceptionDTO, stringFamId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(familyRelationshipServiceMock.getRelationshipDto(stringFamId, stringRelId)).
                thenReturn(Result.failure(serviceExceptionDTO));

        ResponseEntity result = controller.getRelationship(stringFamId, stringRelId);

        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void getAllRelations_successCase() {
        //arrange
        String famId = "1";
        String firstPersonEmail = "ze@isep.pt";
        String secondPersonEmail = "ana@isep.pt";
        String designationFather = "father";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(firstPersonEmail);
        relationshipDTO.setSecondPersonEmail(secondPersonEmail);
        relationshipDTO.setFamilyId(famId);
        relationshipDTO.setDesignation(designationFather);

        List<RelationshipDTO> serviceRelationshipDTOList = new ArrayList<>();
        serviceRelationshipDTOList.add(relationshipDTO);

        List<RelationshipDTO> expectedRelationshipDTOList = new ArrayList<>();
        expectedRelationshipDTOList.add(relationshipDTO);

        for (RelationshipDTO relationship :
                expectedRelationshipDTOList) {

            addLinksToDto(relationship, famId);
        }

        ResponseEntity<Object> expected =
                new ResponseEntity<>(expectedRelationshipDTOList, HttpStatus.OK);

        Mockito.when(familyRelationshipServiceMock.getAllRelationshipsDto(famId)).
                thenReturn(Result.success(serviceRelationshipDTOList));

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        List<RelationshipDTO> resultList = (List<RelationshipDTO>) result.getBody();
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());
        assertEquals(expectedRelationshipDTOList, resultList);
    }

    @Test
    void getAllRelations_successCaseButFamilyHasNoRelationships() {
        //arrange
        String famId = "1";

        List<RelationshipDTO> relationshipDTOList = new ArrayList<>();

        List<RelationshipDTO> expectedRelationshipDTOList = new ArrayList<>();

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedRelationshipDTOList, HttpStatus.OK);

        Mockito.when(familyRelationshipServiceMock.getAllRelationshipsDto(famId)).
                thenReturn(Result.success(relationshipDTOList));

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(expected, result);
    }

    @Test
    void getAllRelations_failCase_familyDoesNotExist() {
        //arrange
        String famId = "1";

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Could not get relationships! " +
                        "There is no family with that id.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Could not get relationships! " +
                        "There is no family with that id.");

        addLinksToExceptionDTO(expectedExceptionDTO, famId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(familyRelationshipServiceMock.getAllRelationshipsDto(famId)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());
    }

    @Test
    void getAllRelations_failCase_nullFamilyId() {
        //arrange
        String famId = null;

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Could not get relationships! " +
                        "The family Id cannot be null.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Could not get relationships! " +
                        "The family Id cannot be null.");

        addLinksToExceptionDTO(expectedExceptionDTO, famId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(familyRelationshipServiceMock.getAllRelationshipsDto(famId)).
                thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());
    }
}
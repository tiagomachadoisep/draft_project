package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyCashAccountService;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.exceptions.AdminStateException;
import switchtwentytwenty.project.exceptions.FamilyAlreadyHasCashAccountException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CreateFamilyCashAccountTest {

    private IFamilyCashAccountService iFamilyCashAccountServiceMock =
            Mockito.mock(IFamilyCashAccountService.class);

    private AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);

    private CreateFamilyCashAccountController createFamilyCashAccountController = new CreateFamilyCashAccountController(assemblerToDTOMock, iFamilyCashAccountServiceMock);

    @Test
    @DisplayName("Creating family cash account with success - restController")
    public void createFamilyCashAccountSuccessfullyRestController() {
        //arrange
        String email = "abc@abc.pt";
        String description = "Family cash account";
        double balance = 50;
        AccountDTO input = new AccountDTO(email, description, balance, EAccountType.FAMILY_CASH);

        AccountDTO assembledDTO = new AccountDTO("abc@abc.pt", "Family cash account", 50,
                EAccountType.FAMILY_CASH);

        Mockito.when(assemblerToDTOMock.toFamilyCashAccountDTO(email, description, balance, "-1")).thenReturn(assembledDTO);

        AccountDTO outputDTO = new AccountDTO(email,description,balance, EAccountType.FAMILY_CASH);
        Mockito.when(iFamilyCashAccountServiceMock.createFamilyCashAccount(input)).thenReturn(outputDTO);

        //act
        ResponseEntity<Object> result = createFamilyCashAccountController.createFamilyCashAccount(email, input);

        //assert
        assertEquals(201, result.getStatusCodeValue());
    }

    @Test
    public void createFamilyCashAccountUnsuccessfully_notAdmin(){
        //arrange
        String email = "abc@abc.pt";
        String description = "Family cash account";
        double balance = 50;
        AccountDTO input = new AccountDTO(email, description, balance, EAccountType.FAMILY_CASH);

        AccountDTO familyCashAccountDTO = new AccountDTO(email,description, balance, EAccountType.FAMILY_CASH);

        Mockito.when(assemblerToDTOMock.toFamilyCashAccountDTO(email,description, balance, "-1")).thenReturn(familyCashAccountDTO);
        Mockito.when(iFamilyCashAccountServiceMock.createFamilyCashAccount(familyCashAccountDTO)).thenThrow(AdminStateException.class);

        //act
        ResponseEntity<Object> result = createFamilyCashAccountController.createFamilyCashAccount(email, input);

        //assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals("Person is not the family administrator of this family", result.getBody());
    }

    @Test
    void createFamilyCashAccountUnsuccessfully_alreadyHasCashAccount() {
        //arrange
        String email = "abc@abc.pt";
        String description = "Family cash account";
        double balance = 50;
        AccountDTO input = new AccountDTO(email, description, balance, EAccountType.FAMILY_CASH);

        AccountDTO familyCashAccountDTO = new AccountDTO(email, description, balance, EAccountType.FAMILY_CASH);

        Mockito.when(assemblerToDTOMock.toFamilyCashAccountDTO(email, description, balance, "-1")).thenReturn(familyCashAccountDTO);
        Mockito.when(iFamilyCashAccountServiceMock.createFamilyCashAccount(familyCashAccountDTO)).thenThrow(FamilyAlreadyHasCashAccountException.class);

        //act
        ResponseEntity<Object> result = createFamilyCashAccountController.createFamilyCashAccount(email, input);

        //assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals("Family already has a cash account", result.getBody());
    }

    @Test
    void whenGetsTheAccount() {

        AccountDTO dto = new AccountDTO();
        dto.setBalance(100);
        dto.setAccountId("1");
        dto.setOwnerId("ze@fmail.com");

        Mockito.when(iFamilyCashAccountServiceMock.getFamilyCashAccount("1")).thenReturn(Optional.of(dto));

        //ACT
        ResponseEntity<Object> actual = createFamilyCashAccountController.getAccountByID("ze@fmail.com", "1");
        //ASSERT
        assertEquals(Optional.of(dto), actual.getBody());
    }
}

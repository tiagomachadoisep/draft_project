package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.dto.TransferDTO;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TransferCashBetweenMembersControllerTest {

    private ILedgerService ledgerService = Mockito.mock(ILedgerService.class);
    private TransferCashBetweenMembersController ctrl = new TransferCashBetweenMembersController(ledgerService);

    @Test
    void testSuccess(){
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        double balance = 20;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020,1,1);

        TransferDTO in = new TransferDTO(mainEmail,otherEmail,balance,categoryId,date);

        TransferDTO out = new TransferDTO(mainEmail,otherEmail,balance,categoryId,"1","2","2",date);

        Mockito.when(ledgerService.transferCash(in)).thenReturn(out);

        //Act
        ResponseEntity<Object> result = ctrl.transferCash(in);

        //Assert
        assertNotNull(result.getBody());
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void testFail(){
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        double balance = 20;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020,1,1);

        TransferDTO in = new TransferDTO(mainEmail,otherEmail,balance,categoryId,date);

        TransferDTO out = new TransferDTO(mainEmail,otherEmail,0,categoryId,    date);

        Mockito.when(ledgerService.transferCash(in)).thenReturn(out);

        //Act
        ResponseEntity<Object> result = ctrl.transferCash(in);

        //Assert
        assertEquals("Insufficient funds", result.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }
}
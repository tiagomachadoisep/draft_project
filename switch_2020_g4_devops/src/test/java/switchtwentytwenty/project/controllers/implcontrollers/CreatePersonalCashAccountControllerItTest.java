package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.AccountService;
import switchtwentytwenty.project.applicationservices.implappservices.PersonService;
import switchtwentytwenty.project.applicationservices.implappservices.PersonalCashAccountService;
import switchtwentytwenty.project.applicationservices.implassemblers.AccountAssembler;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.AccountAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.implementations.PersonAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;
import switchtwentytwenty.project.repositories.AccountRepository;
import switchtwentytwenty.project.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreatePersonalCashAccountControllerItTest {

    private static IPersonRepositoryJPA iPersonRepositoryJPAMock;
    private static IAccountRepositoryJPA iAccountRepositoryJPAMock;
    private static IPersonRepository iPersonRepositoryMock;
    private static CreatePersonalCashAccountController createPersonalCashAccountControllerREST;

    @BeforeEach
    private void setUp() {
        iPersonRepositoryMock = Mockito.mock(IPersonRepository.class);
        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        iPersonRepositoryJPAMock = Mockito.mock(IPersonRepositoryJPA.class);
        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();
        PersonRepository personRepository = new PersonRepository(personAssemblerJPA, iPersonRepositoryJPAMock);
        PersonAssembler personAssembler = new PersonAssembler();
        PersonService personService = new PersonService(personRepository, personAssembler);
        AccountAssembler accountAssembler = new AccountAssembler();
        AccountAssemblerJPA accountAssemblerJPA = new AccountAssemblerJPA();
        iAccountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
        AccountRepository accountRepository = new AccountRepository(accountAssemblerJPA, iAccountRepositoryJPAMock);
        AccountService accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO,iPersonRepositoryMock);
        PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(personService,
                accountService);
        createPersonalCashAccountControllerREST =
                new CreatePersonalCashAccountController(assemblerToDTO, personalCashAccountService);
    }

    @Test
    void createPersonalCashAccount_ValidPersonAndAccount() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 13.40,
                EAccountType.PERSONAL_CASH);

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("3"));
        accountJPA.setPersonID(new PersonID(new Email(ownerId)));
        accountJPA.setAccountType(EAccountType.CREDIT_CARD);
        accountJPA.setBalance(0);
        accountJPA.setDescription("My credit card account");
        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iPersonRepositoryJPAMock.existsById(new PersonID(new Email(ownerId)))).thenReturn(true);
        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(new PersonID(new Email(ownerId)))).thenReturn(accounts);

        //Act
        ResponseEntity<Object> result = createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                inputPersonalCashAccountDTO);

        //Assert
        assertThat(result.getStatusCodeValue()).isEqualTo(201);
        assertNotNull(result.getBody());

    }

    @Test
    void createPersonalCashAccount_PersonDoesNotExist() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 13.40,
                EAccountType.PERSONAL_CASH);

        String errorMessage = "Person does not exist";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);


        Mockito.when(iPersonRepositoryJPAMock.existsById(new PersonID(new Email(ownerId)))).thenReturn(false);

        //Act
        ResponseEntity<Object> result = createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                inputPersonalCashAccountDTO);

        //Assert
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
        assertThat(result.getBody()).isEqualTo(expected);

    }

    @Test
    void createPersonalCashAccount_PersonAlreadyHasACashAccount() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        String description = "My cash account";
        double balance = 13.40;
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", balance,
                EAccountType.PERSONAL_CASH);

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("1"));
        accountJPA.setPersonID(new PersonID(new Email(ownerId)));
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(balance);
        accountJPA.setDescription(description);
        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iPersonRepositoryJPAMock.existsById(new PersonID(new Email(ownerId)))).thenReturn(true);
        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(new PersonID(new Email(ownerId)))).thenReturn(accounts);

        String errorMessage = "Person already has a cash account";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);

        //Act
        ResponseEntity<Object> result = createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                inputPersonalCashAccountDTO);

        //Assert
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
        assertThat(result.getBody()).isEqualTo(expected);
    }

    @Test
    void createPersonalCashAccount_WrongInfo_NullInputParameters() {

        //Arrange
        String errorMessage = "Wrong info";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                null);


        //Act
        ResponseEntity<Object> result = createPersonalCashAccountControllerREST.createPersonalCashAccount(null, null);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }


    @Test
    void getPersonalCashAccountByID_AccountExists() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        String accountId = "djaS";
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID(accountId));
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(40);
        accountJPA.setDescription("Cash account");
        accountJPA.setPersonID(new PersonID(new Email(ownerId)));

        Optional<AccountDTO> expected = Optional.of(new AccountDTO(ownerId, "My cash account", 30, accountId,
                EAccountType.PERSONAL_CASH));

        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(new AccountID(accountId))).thenReturn(accountJPA);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.getPersonalCashAccountByAccountId(ownerId, accountId);

        //Assert
        assertEquals(200, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void getPersonalCashAccountByID_AccountDoesNotExist() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        String accountId = "";

        String errorMessage = "Account does not exist";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.getPersonalCashAccountByAccountId(ownerId, accountId);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }


}

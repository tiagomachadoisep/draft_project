package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.FamilyService;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.FamilyDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


public class AddFamilyControllerItTest {


    private final IFamilyRepository familyRepository = Mockito.mock(IFamilyRepository.class);

    private final IPersonRepository personRepository = Mockito.mock(IPersonRepository.class);

    private final AssemblerToDTO as = new AssemblerToDTO();
    private final FamilyService familyService = new FamilyService(familyRepository, personRepository, as);


    @Test
    void addFamily() {
        //Arrange
        AssemblerToDTO assembler = new AssemblerToDTO();
        AddFamilyController ctrl = new AddFamilyController(assembler, familyService);

        String familyName = "Silva";
        String name = "Ze";
        String adminEmail = "ze@isep.pt";
        int adminVat = 0;
        String adminBirthDate = "01-01-1990";
        FamilyDTO expected = new FamilyDTO(familyName, adminEmail);
        FamilyDTO in = new FamilyDTO(familyName, adminEmail,name,adminVat,adminBirthDate);

        //Act
        ResponseEntity dto = ctrl.addFamily(in);
        FamilyDTO result = (FamilyDTO) dto.getBody();
        //Assert
        assertEquals(expected.getAdminEmail(), result.getAdminEmail());
        assertEquals(expected.getName(), result.getName());
        assertEquals(HttpStatus.CREATED, dto.getStatusCode());
    }

    @Test
    void addFamily_InvalidParameters() {
        //Arrange
        AssemblerToDTO assembler = new AssemblerToDTO();
        AddFamilyController ctrl = new AddFamilyController(assembler, familyService);

        String familyName = "";
        String name = "Ze";
        String adminEmail = "ze@isep.pt";
        int adminVat = 0;
        String adminBirthDate = "01-01-1990";
        FamilyDTO in = new FamilyDTO(familyName, adminEmail,name,adminVat,adminBirthDate);

        ExceptionDTO expected = new ExceptionDTO("Invalid name.");
        Link tryAgainLink = linkTo(methodOn(AddFamilyController.class).addFamily(in)).withRel("try again");
        expected.add(tryAgainLink);
        //Act
        ResponseEntity dto = ctrl.addFamily(in);

        //Assert
        assertEquals(expected.getMessage(),((ExceptionDTO) dto.getBody()).getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, dto.getStatusCode());
    }

    @Test
    void addFamily_moreInvalidParameters() {
        //Arrange
        AssemblerToDTO assembler = new AssemblerToDTO();
        AddFamilyController ctrl = new AddFamilyController(assembler, familyService);

        String familyName = "Silva";
        String name = "";
        String adminEmail = "ze@isep.pt";
        int adminVat = 0;
        String adminBirthDate = "01-01-1990";
        FamilyDTO in = new FamilyDTO(familyName, adminEmail,name,adminVat,adminBirthDate);

        ExceptionDTO expected = new ExceptionDTO("Invalid name format");
        Link tryAgainLink = linkTo(methodOn(AddFamilyController.class).addFamily(in)).withRel("try again");
        expected.add(tryAgainLink);
        //Act
        ResponseEntity dto = ctrl.addFamily(in);

        //Assert
        assertEquals(expected.getMessage(),((ExceptionDTO) dto.getBody()).getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, dto.getStatusCode());
    }

    @Test
    void getFamily() {
        //Arrange
        AssemblerToDTO assembler = new AssemblerToDTO();
        AddFamilyController ctrl = new AddFamilyController(assembler, familyService);

        String id = "1";

        FamilyDTO expected = new FamilyDTO("Silva", "silva@isep.pt", id);

        Family fam = new Family.Builder(new FamilyID(id)).setName(new FamilyName("Silva"))
                .setAdminId(new PersonID(new Email("silva@isep.pt"))).setRegistrationDate(new RegistrationDate("25-07-2020")).build();
        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.of(fam));

        //Act
        ResponseEntity dto = ctrl.getFamily(id);
        FamilyDTO result = (FamilyDTO) dto.getBody();

        //Assert
        assertEquals(expected.getAdminEmail(), result.getAdminEmail());
        assertEquals(expected.getName(), result.getName());
        assertEquals(HttpStatus.OK, dto.getStatusCode());
    }

    @Test
    void getFamilyFail() {
        //Arrange
        AssemblerToDTO assembler = new AssemblerToDTO();
        AddFamilyController ctrl = new AddFamilyController(assembler, familyService);

        String id = "1";

        FamilyDTO expected = new FamilyDTO("Silva", "silva@isep.pt", id);

        Family fam = new Family.Builder(new FamilyID(id)).setName(new FamilyName("Silva"))
                .setAdminId(new PersonID(new Email("silva@isep.pt"))).setRegistrationDate(new RegistrationDate("25-07-2020")).build();

        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.empty());

        //Act
        ResponseEntity dto = ctrl.getFamily(id);
        FamilyDTO result = (FamilyDTO) dto.getBody();

        //Assert
        assertEquals(null, result.getAdminEmail());
        assertEquals(null, result.getName());
        assertEquals(HttpStatus.OK, dto.getStatusCode());
    }

}

package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.AccountService;
import switchtwentytwenty.project.applicationservices.implappservices.FamilyCashAccountService;
import switchtwentytwenty.project.applicationservices.implappservices.FamilyService;
import switchtwentytwenty.project.applicationservices.implassemblers.AccountAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.AccountAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.implementations.FamilyAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IAccountAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IFamilyIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;
import switchtwentytwenty.project.persistence.data.FamilyJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IFamilyRepositoryJPA;
import switchtwentytwenty.project.repositories.AccountRepository;
import switchtwentytwenty.project.repositories.FamilyRepository;
import switchtwentytwenty.project.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CreateFamilyCashAccountControllerItTest {
    private IFamilyRepositoryJPA familyRepositoryJPAMock;
    private IAccountRepositoryJPA accountRepositoryJPAMock;
    private IPersonRepository iPersonRepositoryMock;
    private CreateFamilyCashAccountController ctrl;

    @BeforeEach
    void createsFamilyWithAdmin() {
        familyRepositoryJPAMock = Mockito.mock(IFamilyRepositoryJPA.class);
        accountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
        iPersonRepositoryMock = Mockito.mock(IPersonRepository.class);

        PersonRepository personRepository = new PersonRepository();
        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();

        IFamilyIAssemblerJPA familyIAssemblerJPAMock = new FamilyAssemblerJPA();
        FamilyRepository familyRepository = new FamilyRepository(familyRepositoryJPAMock, familyIAssemblerJPAMock);

        IAccountAssemblerJPA accountAssemblerJPA = new AccountAssemblerJPA();

        AccountRepository accountRepository = new AccountRepository(accountAssemblerJPA, accountRepositoryJPAMock);

        AccountAssembler accountAssembler = new AccountAssembler();
        AccountService accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO,iPersonRepositoryMock);
        FamilyService familyService = new FamilyService(familyRepository, personRepository, new AssemblerToDTO());

        FamilyCashAccountService familyCashAccountService = new FamilyCashAccountService(familyService, accountService);

        ctrl = new CreateFamilyCashAccountController(assemblerToDTO, familyCashAccountService);


    }

    @DisplayName("Create family cash account unsuccessfully - Information Error")
    @Test
    void createFamilyCashAccountUnsuccessfully(){
        //arrange

        String mainEmail = "abc@abc.pt";
        double initialBalance = 50;

        FamilyJPA familyJPA1 = new FamilyJPA(new FamilyID("1"), new PersonID(new Email(mainEmail)), "silva", "01-02-2020");
        List<FamilyJPA> families = new ArrayList<>();
        families.add(familyJPA1);
        Mockito.when(familyRepositoryJPAMock.findAll()).thenReturn(families);

        AccountDTO inputDTO = new AccountDTO(mainEmail, null, initialBalance,EAccountType.FAMILY_CASH);

        //act
        ResponseEntity result = ctrl.createFamilyCashAccount(mainEmail, inputDTO);

        //assert
        assertEquals(400, result.getStatusCodeValue());
        assertTrue(result.getBody().equals("Information Error"));
    }

    @DisplayName("Create family cash account unsuccessfully due to already existing family cash account")
    @Test
    void createFamilyCashAccountUnsuccessfully_familyCashAccountAlreadyExists(){
        //arrange

        String description = "Family cash account";
        String mainEmail = "etc@etc.pt";
        double initialBalance = 50;

        FamilyJPA familyJPA1 = new FamilyJPA(new FamilyID("1"), new PersonID(new Email(mainEmail)), "silva", "01-02-2020");
        List<FamilyJPA> families = new ArrayList<>();
        families.add(familyJPA1);
        Mockito.when(familyRepositoryJPAMock.findAll()).thenReturn(families);

        AccountJPA accountJPA = new AccountJPA(new AccountID("1"), new PersonID(new Email(mainEmail)), description, initialBalance, EAccountType.FAMILY_CASH);
        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);
        Mockito.when(accountRepositoryJPAMock.findAllByPersonID(new PersonID(new Email(mainEmail)))).thenReturn(accounts);

        AccountDTO inputDTO = new AccountDTO(mainEmail, description, initialBalance,EAccountType.FAMILY_CASH);

        //act
        ResponseEntity result = ctrl.createFamilyCashAccount(mainEmail, inputDTO);

        // assert
        assertEquals(400, result.getStatusCodeValue());
        assertTrue(result.getBody().equals("Family already has a cash account"));

    }

    @DisplayName("Create family cash account unsuccessfully - user is not the family admin")
    @Test
    void createFamilyCashAccountUnsuccessfully_notFamilyAdmin(){
        //arrange

        String description = "Family cash account";
        String mainEmail = "xpto@xpto.pt";
        double initialBalance = 50;

        AccountDTO inputDTO = new AccountDTO(mainEmail, description, initialBalance,EAccountType.FAMILY_CASH);

        //act
        ResponseEntity result = ctrl.createFamilyCashAccount(mainEmail, inputDTO);

        // assert
        assertEquals(400, result.getStatusCodeValue());
        assertTrue(result.getBody().equals("Person is not the family administrator of this family"));
    }

    @DisplayName("Create family cash account successfully")
    @Test
    void createFamilyCashAccountSuccessfully() {
        //arrange
        String description = "Family cash account";
        String mainEmail = "abc@abc.pt";
        double initialBalance = 50;

        AccountDTO inputDTO = new AccountDTO(mainEmail, description, initialBalance, EAccountType.FAMILY_CASH);

        //add Family + create admin
        List<FamilyJPA> families = new ArrayList<>();
        FamilyJPA family1 = new FamilyJPA(new FamilyID("1"), new PersonID(new Email("abc@abc.pt")), "Abc family", "09/06/2021");
        families.add(family1);

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountType(EAccountType.BANK);
        List<AccountJPA> accountJPAList = new ArrayList<>();
        accountJPAList.add(accountJPA);

        Mockito.when(familyRepositoryJPAMock.findAll()).thenReturn(families);
        Mockito.when(accountRepositoryJPAMock.findAllByPersonID(new PersonID(new Email(mainEmail)))).thenReturn(accountJPAList);

        //act
        ResponseEntity result = ctrl.createFamilyCashAccount(mainEmail, inputDTO);
        AccountDTO outputDTO = (AccountDTO) result.getBody();

        //assert
        assertNotEquals(inputDTO.getAccountId(), outputDTO.getAccountId());
        assertEquals(201, result.getStatusCodeValue());
        assertNotNull(result.getBody());
    }

    @Test
    public void getAccountById_noCashAccount() {
        //arrange

        String description = "Family cash account";
        String mainEmail = "abc@abc.pt";
        double initialBalance = 50;

        AccountDTO inputDTO = new AccountDTO(mainEmail, description, initialBalance, EAccountType.FAMILY_CASH);

        //add Family + create admin
        List<FamilyJPA> families = new ArrayList<>();
        FamilyJPA family1 = new FamilyJPA(new FamilyID("1"), new PersonID(new Email("abc@abc.pt")), "Abc family", "09/06/2021");
        families.add(family1);

        Mockito.when(familyRepositoryJPAMock.findAll()).thenReturn(families);

        AccountDTO cashAccountOutputDTO = (AccountDTO) ctrl.createFamilyCashAccount(mainEmail, inputDTO).getBody();

        //act
        ResponseEntity result = ctrl.getAccountByID(mainEmail, cashAccountOutputDTO.getAccountId());

        //assert
        assertEquals(400, result.getStatusCodeValue());
        assertNotNull(result.getBody());
    }

    @Test
    public void getAccountById_success1(){
        //arrange

        String ownerId = "abc@abc.pt";
        String accountId = "2132";

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountType(EAccountType.FAMILY_CASH);
        accountJPA.setPersonID(new PersonID(new Email(ownerId)));
        accountJPA.setAccountID(new AccountID(accountId));
        accountJPA.setDescription("description test");
        accountJPA.setBalance(50);


        Mockito.when(accountRepositoryJPAMock.findByAccountID(new AccountID(accountId))).thenReturn(accountJPA);

        //act
        ResponseEntity<Object> result = ctrl.getAccountByID(ownerId, accountId);

        //assert
        assertEquals(200, result.getStatusCodeValue());
        assertNotNull(result.getBody());
    }
}

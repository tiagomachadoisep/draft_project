package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.implappservices.CategoryService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


class AddStandardCategoryControllerTest {


    private ICategoryService categoryServiceMock = Mockito.mock(ICategoryService.class);

    private AssemblerToDTO mapper = Mockito.mock(AssemblerToDTO.class);

    private AddStandardCategoryController controller = new AddStandardCategoryController(categoryServiceMock, mapper);


    @Test
     void successAddingStandardCategoryUnitTest() {
        //ARRANGE
        String categoryName = "Netflix";
        String idExpected = "1";
        CategoryDTO input = new CategoryDTO();
        input.setName("Netflix");

        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);

        CategoryDTO output = new CategoryDTO();
        output.setId(idExpected);

        Mockito.when(mapper.inputToCategoryDTO(input)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardCategory(categoryCreated)).thenReturn(output);

        //ACT
        ResponseEntity<Object> realOutput = controller.addStandardCategory(input);

        //ASSERT
        assertThat(realOutput.getBody()).isEqualTo(output);

    }

    @Test
     void failAddingStandardCategoryDueToInvalidNameUnitTest() {
        //ARRANGE
        String categoryName = "123kgjrur";
        String invalidName = "Invalid parameters";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);


        Mockito.when(mapper.inputToCategoryDTO(categoryCreated)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardCategory(categoryCreated)).thenThrow(InvalidDesignationException.class);

        //ACT
        ResponseEntity<Object> response = controller.addStandardCategory(categoryCreated);

        //ASSERT
        assertThat(response.getBody()).isEqualTo(invalidName);

    }


    @Test
     void successAddingStandardSubCategoryUnitTest() {
        //ARRANGE
        String categoryName = "Health";
        String idExpected = "1";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setParentId("2");

        CategoryDTO dto = new CategoryDTO();
        dto.setId(idExpected);

        Mockito.when(mapper.inputToCategoryDTO(categoryCreated)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardSubCategory(categoryCreated)).thenReturn(dto);

        //ACT
        ResponseEntity<Object> response = controller.addStandardCategory(categoryCreated);

        //ASSERT
        assertThat(response.getBody()).isEqualTo(dto);

    }

    @Test
     void failAddingStandardSubCategoryDueToInvalidNameUnitTest() {
        //ARRANGE
        String categoryName = "123XEIS";
        String invalidName = "Invalid parameters";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setParentId("2");

        Mockito.when(mapper.inputToCategoryDTO(categoryCreated)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardSubCategory(categoryCreated)).thenThrow(InvalidDesignationException.class);

        //ACT
        ResponseEntity<Object> response = controller.addStandardCategory(categoryCreated);

        //ASSERT
        assertThat(response.getBody()).isEqualTo(invalidName);

    }

    @Test
    void failAddingStandardSubCategoryDueToInvalidParentId() {
        //ARRANGE
        String categoryName = "XEIS";
        String invalidName = "ParentID not found";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setParentId("2");

        Mockito.when(mapper.inputToCategoryDTO(categoryCreated)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardSubCategory(categoryCreated)).thenThrow(NoSuchElementException.class);

        //ACT
        ResponseEntity<Object> response = controller.addStandardCategory(categoryCreated);

        //ASSERT
        assertEquals(response.getBody(), invalidName);

    }

    @Test
    void failAddingStandardSubCategorySubcategoryAlreadyExists() {
        //ARRANGE
        String categoryName = "XEIS";
        String invalidName = "Category already exists";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setParentId("2");

        Mockito.when(mapper.inputToCategoryDTO(categoryCreated)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardSubCategory(categoryCreated)).thenThrow(DuplicatedValueException.class);

        //ACT
        ResponseEntity<Object> response = controller.addStandardCategory(categoryCreated);

        //ASSERT
        assertEquals(response.getBody(), invalidName);

    }





    @Test
     void failAddingCategoryAlreadyExistsUnitTest() {
        //ARRANGE
        String categoryName = "Health";
        String errorExpected = "Category already exists";
        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);

        CategoryDTO output = new CategoryDTO();
        output.setId(errorExpected);

        Mockito.when(mapper.inputToCategoryDTO(categoryCreated)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addStandardCategory(categoryCreated)).thenReturn(output);

        //ACT
        ResponseEntity<Object> response = controller.addStandardCategory(categoryCreated);

        //ASSERT
        assertEquals(response.getBody(), output);
    }

    @Test
    void searchCategorySuccessfully() {
        //ARRANGE
        String id = "1";

        CategoryDTO expected = new CategoryDTO();
        expected.setId(id);
        expected.setName("Drinks");

        Mockito.when(categoryServiceMock.getCategoryById(id)).thenReturn(expected);
        //ACT
        ResponseEntity<Object> actual = controller.getCategoryById(id);
        assertEquals(actual.getBody(),expected);
    }

    @Test
    void searchingForCategoryFailure() {
        //ARRANGE
        String id = "1";

        String expected = "Category not found";

        Mockito.when(categoryServiceMock.getCategoryById(id)).thenThrow(NoSuchElementException.class);
        //ACT
        ResponseEntity<Object> actual = controller.getCategoryById(id);
        assertEquals(actual.getBody(),expected);
    }

}



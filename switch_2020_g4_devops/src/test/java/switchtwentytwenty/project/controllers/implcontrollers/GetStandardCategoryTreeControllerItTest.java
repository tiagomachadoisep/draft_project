package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.implappservices.CategoryService;
import switchtwentytwenty.project.applicationservices.implassemblers.CategoryAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepository;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.CategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;
import switchtwentytwenty.project.repositories.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GetStandardCategoryTreeControllerItTest {
    private ICategoryService iCategoryService;
    private ICategoryRepositoryJPA iCategoryRepositoryJPA = Mockito.mock(ICategoryRepositoryJPA.class);

    @BeforeEach
    void setup() {

        ApplicationContext context = new ClassPathXmlApplicationContext("repositoryhttp.xml");

        ICategoryRepositoryHttp httpRepository = context.getBean("repositoryGroup",
                ICategoryRepositoryHttp.class);

        ICategoryRepository categoryRepository = new CategoryRepository(new CategoryAssemblerJPA(),
                iCategoryRepositoryJPA);

        iCategoryService = new CategoryService(categoryRepository, new CategoryAssembler(), new AssemblerToDTO());

    }

    @Test
    @DisplayName("Get standard category tree")
    void retrieveStandardCategoryTree() {
        //arrange
        GetStandardCategoryTreeController ctrl = new GetStandardCategoryTreeController(iCategoryService);

        CategoryJPA categoryJPA1 = new CategoryJPA();
        categoryJPA1.setId(new CategoryID("1"));
        categoryJPA1.setParentCategory(null);
        categoryJPA1.setFamilyId(null);
        categoryJPA1.setName("Health");

        CategoryJPA categoryJPA2 = new CategoryJPA();
        categoryJPA2.setId(new CategoryID("2"));
        categoryJPA2.setParentCategory(null);
        categoryJPA2.setFamilyId(null);
        categoryJPA2.setName("Sports");

        CategoryJPA categoryJPA3 = new CategoryJPA();
        categoryJPA3.setId(new CategoryID("3"));
        categoryJPA3.setParentCategory("2");
        categoryJPA3.setFamilyId(null);
        categoryJPA3.setName("Football");

        List<CategoryJPA> categoriesJPA = new ArrayList<>();
        categoriesJPA.add(categoryJPA1);
        categoriesJPA.add(categoryJPA2);
        categoriesJPA.add(categoryJPA3);

        Mockito.when(iCategoryRepositoryJPA.findAll()).thenReturn(categoriesJPA);

        List<CategoryDTO> expected = new ArrayList<>();
        CategoryDTO dto1 = new CategoryDTO("Health", "1", null, null);
        CategoryDTO dto2 = new CategoryDTO("Sports", "2", null, null);
        CategoryDTO dto3 = new CategoryDTO("Football", "3", "2", null);
        expected.add(dto1);
        expected.add(dto2);
        expected.add(dto3);

        //act
        ResponseEntity<Object> output = ctrl.getStandardCategoryTree();

        //assert
        assertNotNull(output.getBody());
        //assertNotSame(expected, output.getBody());
        assertEquals(expected, output.getBody());
    }

    @Test
    @DisplayName("Get empty standard category tree")
    void retrieveEmptyStandardCategoryTree() {
        //arrange
        GetStandardCategoryTreeController ctrl = new GetStandardCategoryTreeController(iCategoryService);

        List<CategoryJPA> categoriesJPA = new ArrayList<>();
        Mockito.when(iCategoryRepositoryJPA.findAll()).thenReturn(categoriesJPA);

        //act
        ResponseEntity<Object> output = ctrl.getStandardCategoryTree();

        //assert
        assertNotNull(output.getBody());
        assertEquals("Empty category tree", output.getBody());
    }


    @Test
    @DisplayName("Get standard category tree with external categories")
    void retrieveFullStandardCategoryTree() {
        //arrange
        GetStandardCategoryTreeController ctrl = new GetStandardCategoryTreeController(iCategoryService);

        CategoryJPA categoryJPA1 = new CategoryJPA();
        categoryJPA1.setId(new CategoryID("1"));
        categoryJPA1.setParentCategory(null);
        categoryJPA1.setFamilyId(null);
        categoryJPA1.setName("Health");

        CategoryJPA categoryJPA2 = new CategoryJPA();
        categoryJPA2.setId(new CategoryID("2"));
        categoryJPA2.setParentCategory(null);
        categoryJPA2.setFamilyId(null);
        categoryJPA2.setName("Sports");

        CategoryJPA categoryJPA3 = new CategoryJPA();
        categoryJPA3.setId(new CategoryID("3"));
        categoryJPA3.setParentCategory("2");
        categoryJPA3.setFamilyId(null);
        categoryJPA3.setName("Football");

        List<CategoryJPA> categoriesJPA = new ArrayList<>();
        categoriesJPA.add(categoryJPA1);
        categoriesJPA.add(categoryJPA2);
        categoriesJPA.add(categoryJPA3);

        Mockito.when(iCategoryRepositoryJPA.findAll()).thenReturn(categoriesJPA);

        List<CategoryDTO> expected = new ArrayList<>();
        CategoryDTO dto1db = new CategoryDTO("Health", "1", null, null);
        CategoryDTO dto2db = new CategoryDTO("Sports", "2", null, null);
        CategoryDTO dto3db = new CategoryDTO("Football", "3", "2", null);
        expected.add(dto1db);
        expected.add(dto2db);
        expected.add(dto3db);

        CategoryDTO dto0 = new CategoryDTO("All", "HTTPGIST/0", null, null);
        CategoryDTO dto1 = new CategoryDTO("Books", "HTTPGIST/1", null, null);
        CategoryDTO dto2 = new CategoryDTO("Hardcover", "HTTPGIST/1", "HTTPGIST/1", null);
        CategoryDTO dto3 = new CategoryDTO("Paperback", "HTTPGIST/2", "HTTPGIST/1", null);
        CategoryDTO dto4 = new CategoryDTO("Electronic", "HTTPGIST/3", "HTTPGIST/1", null);
        CategoryDTO dto5 = new CategoryDTO("Movies", "HTTPGIST/2", null, null);
        CategoryDTO dto6 = new CategoryDTO("DVD", "HTTPGIST/4", "HTTPGIST/2", null);
        CategoryDTO dto7 = new CategoryDTO("BluRay", "HTTPGIST/5", "HTTPGIST/2", null);
        CategoryDTO dto8 = new CategoryDTO("Download", "HTTPGIST/6", "HTTPGIST/2", null);
        CategoryDTO dto9 = new CategoryDTO("Games", "HTTPGIST/3", null, null);
        CategoryDTO dto10 = new CategoryDTO("XBox", "HTTPGIST/7", "HTTPGIST/3", null);
        CategoryDTO dto11 = new CategoryDTO("PC", "HTTPGIST/8", "HTTPGIST/3", null);
        CategoryDTO dto12 = new CategoryDTO("Music", "HTTPGIST/4", null, null);

        expected.add(dto0);
        expected.add(dto1);
        expected.add(dto2);
        expected.add(dto3);
        expected.add(dto4);
        expected.add(dto5);
        expected.add(dto6);
        expected.add(dto7);
        expected.add(dto8);
        expected.add(dto9);
        expected.add(dto10);
        expected.add(dto11);
        expected.add(dto12);

        //act
        ResponseEntity<Object> output = ctrl.getFullStandardCategoryTree();

        //assert
        assertNotNull(output.getBody());
        //assertNotSame(expected, output.getBody());
        //assertEquals(expected, output.getBody());
        assertNotNull(output.getBody());
    }

//    @Test
//    void getCategories() {
//
//        GetStandardCategoryTreeController ctrl = new GetStandardCategoryTreeController(iCategoryService);
//        ctrl.getFullStandardCategoryTree();
//
//
//    }
}

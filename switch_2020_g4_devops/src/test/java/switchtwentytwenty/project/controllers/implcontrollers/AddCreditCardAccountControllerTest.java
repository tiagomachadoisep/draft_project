package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.CreditCardAccount;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//@SpringBootTest
class AddCreditCardAccountControllerTest {

    //@Mock
    private final IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    //@Mock
    private final AssemblerToDTO assemblerToDTOMock = Mockito.mock(AssemblerToDTO.class);
    //@InjectMocks
    private final AddCreditCardAccountController controller = new AddCreditCardAccountController(iAccountServiceMock, assemblerToDTOMock);

    @Test
    void whenTryToAddNewCreditCard_Success_UT() {

        AccountDTO accountDTO = new AccountDTO("luis@isep.pt", "CC Luis", 50, EAccountType.CREDIT_CARD);
        AccountDTO newAccountDTO = new AccountDTO("luis@isep.pt", "CC Luis", 50, "20", EAccountType.CREDIT_CARD);

        Account newAccount = new CreditCardAccount.Builder(new AccountID("20")).setDescription(new Description("CC Luis"))
                .setOwnerID(new PersonID(new Email("luis@isep.pt"))).setBalance(new MoneyValue(50)).setAccountType().build();

        Link newAccountInfoLink = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountByID(accountDTO.getAccountId())).withRel("New Account Info");

        Link allMemberAccountsLink = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(accountDTO.getOwnerId())).withRel("All Member accounts");


        Mockito.when(iAccountServiceMock.addCreditCardAccount(accountDTO)).thenReturn(newAccountDTO);
        Mockito.when(assemblerToDTOMock.toAccountDTO(newAccount)).thenReturn(newAccountDTO);

        newAccountDTO.add(newAccountInfoLink);
        newAccountDTO.add(allMemberAccountsLink);

        ResponseEntity<AccountDTO> expected = new ResponseEntity<>(newAccountDTO, HttpStatus.OK);
        ResponseEntity<Object> result = controller.addCreditCardAccount("luis@isep.pt", accountDTO);

        assertEquals(expected, result);
    }

    @Test
    void whenTryToAddNewCreditCard_FailEmptyID_UT() {

        AccountDTO accountDTO = new AccountDTO("luis@isep.pt", "", 50, EAccountType.CREDIT_CARD);
        Link allMemberAccountsLink = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(accountDTO.getOwnerId())).withRel("All Member accounts");

        IllegalArgumentException exception = new IllegalArgumentException();

        String responseMessage = "Can't add Credit Card Account - " + exception.getMessage();
        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);
        exceptionDTO.add(allMemberAccountsLink);

        Mockito.when(iAccountServiceMock.addCreditCardAccount(accountDTO)).thenThrow(IllegalArgumentException.class);
        Mockito.when(assemblerToDTOMock.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        ResponseEntity<Object> result = controller.addCreditCardAccount("luis@isep.pt", accountDTO);

        assertEquals(exceptionDTO, result.getBody());
    }
}
package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GetStandardCategoryTreeControllerTest {
    private ICategoryService iCategoryService = Mockito.mock(ICategoryService.class);
    private ApplicationContext applicationContext = Mockito.mock(ApplicationContext.class);
    private ICategoryRepositoryHttp standardCategoryAdapter = Mockito.mock(ICategoryRepositoryHttp.class);
    private GetStandardCategoryTreeController ctrl = new GetStandardCategoryTreeController(iCategoryService);

    @Test
    @DisplayName("Get standard category tree")
    void retrieveStandardCategoryTree() {
        //Arrange
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);

        CategoryDTO category2 = new CategoryDTO();
        category2.setId("2");
        category2.setName("Sport");
        category2.setParentId(null);

        List<CategoryDTO> outputCategoryTreeDTO = new ArrayList<>();
        outputCategoryTreeDTO.add(category1);
        outputCategoryTreeDTO.add(category2);

        Mockito.when(iCategoryService.getStandardCategoryTree()).thenReturn(outputCategoryTreeDTO);

        List<CategoryDTO> expected = new ArrayList<>();
        expected.add(category1);
        expected.add(category2);

        //act
        ResponseEntity<Object> output = ctrl.getStandardCategoryTree();

        //assert
        assertEquals(expected, output.getBody());
        assertNotSame(expected, output.getBody());

    }

    @Test
    @DisplayName("Get standard category tree before creating categories")
    void retrieveEmptyStandardCategoryTree(){
        //Arrange
        List<CategoryDTO> outputCategoryTreeDTO = new ArrayList<>();

        Mockito.when(iCategoryService.getStandardCategoryTree()).thenReturn(outputCategoryTreeDTO);

        String expected = "Empty category tree";

        //act
        ResponseEntity<Object> output = ctrl.getStandardCategoryTree();

        //assert
        assertEquals(expected, output.getBody());
    }


    @Test
    @DisplayName("Get standard category tree")
    void retrieveFullStandardCategoryTree() {
        //Arrange
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);

        CategoryDTO category2 = new CategoryDTO();
        category2.setId("2");
        category2.setName("Sport");
        category2.setParentId(null);

        CategoryDTO dto0 = new CategoryDTO("All","HTTPGIST/0","","");
        CategoryDTO dto1 = new CategoryDTO("Books","HTTPGIST/1","","");
        CategoryDTO dto2 = new CategoryDTO("Hardcover","HTTPGIST/1","HTTPGIST/1","");
        CategoryDTO dto3 = new CategoryDTO("Paperback","HTTPGIST/2","HTTPGIST/1","");
        CategoryDTO dto4 = new CategoryDTO("Electronic","HTTPGIST/3","HTTPGIST/1","");
        CategoryDTO dto5 = new CategoryDTO("Movies","HTTPGIST/2","","");
        CategoryDTO dto6 = new CategoryDTO("DVD","HTTPGIST/4","HTTPGIST/2","");
        CategoryDTO dto7 = new CategoryDTO("BluRay","HTTPGIST/5","HTTPGIST/2","");
        CategoryDTO dto8 = new CategoryDTO("Download","HTTPGIST/6","HTTPGIST/2","");
        CategoryDTO dto9 = new CategoryDTO("Games","HTTPGIST/3","","");
        CategoryDTO dto10 = new CategoryDTO("XBox","HTTPGIST/7","HTTPGIST/3","");
        CategoryDTO dto11 = new CategoryDTO("PC","HTTPGIST/8","HTTPGIST/3","");
        CategoryDTO dto12 = new CategoryDTO("Music","HTTPGIST/4","","");

        List<CategoryDTO> expectedExternal = new ArrayList<>();
        expectedExternal.add(dto0);
        expectedExternal.add(dto1);
        expectedExternal.add(dto2);
        expectedExternal.add(dto3);
        expectedExternal.add(dto4);
        expectedExternal.add(dto5);
        expectedExternal.add(dto6);
        expectedExternal.add(dto7);
        expectedExternal.add(dto8);
        expectedExternal.add(dto9);
        expectedExternal.add(dto10);
        expectedExternal.add(dto11);
        expectedExternal.add(dto12);

        List<CategoryDTO> expected = new ArrayList<>();
        expected.add(category1);
        expected.add(category2);
        expected.addAll(expectedExternal);

        Mockito.when(iCategoryService.importAllCategories()).thenReturn(expected);


        //act
        ResponseEntity<Object> output = ctrl.getFullStandardCategoryTree();

        //assert
        assertNotNull(output.getBody());
        assertEquals(expected, output.getBody());

    }

    @Test
    void retrieveEmptyCategories() {
        //Arrange
        Mockito.when(iCategoryService.importAllCategories()).thenReturn(new ArrayList<>());
        List<CategoryDTO> expected = new ArrayList<>();

        //Act
        ResponseEntity<Object> output = ctrl.getFullStandardCategoryTree();

        //Assert
        assertEquals(expected, output.getBody());
    }

}
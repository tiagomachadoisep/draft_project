package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextImpl;
import switchtwentytwenty.project.applicationservices.iappservices.IAuthenticationService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@SpringBootTest
class GetProfileInfoControllerTest {
/*

    @Mock
    IAuthenticationService authenticationServiceMock;
    @Mock
    IPersonService personServiceMock;
    @Mock
    ISecurityContextProvider provider;
    @Mock
    AssemblerToDTO assemblerToDTO;
    @InjectMocks
    GetProfileInfoController controller;

    @Test
    void getProfileInfo() {

        String name = "Julia";
        int vat = 123456789;
        String emailAddress = "julia@isep.pt";
        String address = "Rua de baixo";
        String familyID = "40";
        String birthDate = "01-03-1990";
        PersonDTO personDTO = new PersonDTO(familyID, emailAddress, name, vat, address, birthDate);

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(emailAddress,"123")));
        Mockito.when(personServiceMock.getPersonDTO(emailAddress)).thenReturn(personDTO);
        Mockito.when(assemblerToDTO.toExceptionDto("Forbidden - this is not you")).thenReturn(new ExceptionDTO( "Forbidden - this is not you"));
        Mockito.when(authenticationServiceMock.isCurrentUser(emailAddress)).thenReturn(true);
        Mockito.when(personServiceMock.getPersonDTO(emailAddress)).thenReturn(personDTO);

        ResponseEntity<Object> result = controller.getProfileInfo(emailAddress);

        assertEquals(personDTO, result.getBody());

    }

    @Test
    @DisplayName("Failure - The person doesn't exist.")
    void getProfileInfo_Failure() {

        String emailAddress = "juju@isep.pt";

        Mockito.when(personServiceMock.getPersonDTO(emailAddress)).thenReturn(null);
        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(emailAddress,"123")));

        ResponseEntity<Object> result = controller.getProfileInfo(emailAddress);

        assertEquals("Invalid parameter.", result.getBody());
    }
*/


}
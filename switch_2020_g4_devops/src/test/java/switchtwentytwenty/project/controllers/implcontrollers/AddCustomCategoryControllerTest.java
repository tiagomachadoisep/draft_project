package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class AddCustomCategoryControllerTest {

    private ICategoryService categoryServiceMock = Mockito.mock(ICategoryService.class);

    private AssemblerToDTO mapper = Mockito.mock(AssemblerToDTO.class);

    private AddCustomCategoryController controller = new AddCustomCategoryController(categoryServiceMock, mapper);

    @Test
    void successAddingCustomCategoryUnitTest() {

        //ARRANGE
        String categoryName = "Netflix";
        String familyId = "1";
        CategoryDTO input = new CategoryDTO();
        input.setName("Netflix");
        input.setParentId("");

        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setFamilyId(familyId);

        CategoryDTO output = new CategoryDTO();
        output.setId("2.3");

        Mockito.when(mapper.inputToCustomCategoryDTO(familyId, input)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addCustomCategory(categoryCreated)).thenReturn(output);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomCategory(familyId, input);

        //ASSERT
        assertEquals(realOutput.getBody(), output);

    }

    @Test
    void successAddingCustomSubcategoryUnitTest() {

        //ARRANGE
        String categoryName = "Netflix";
        String familyId = "1";
        CategoryDTO input = new CategoryDTO();
        input.setName("Netflix");
        input.setParentId("2");

        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setFamilyId(familyId);

        CategoryDTO output = new CategoryDTO();
        output.setId("2.3");

        Mockito.when(mapper.inputToCustomCategoryDTO(familyId, input)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.checksParentIdAndAddsCustomSubcategory(categoryCreated)).thenReturn(output);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomCategory(familyId, input);

        //ASSERT
        assertEquals(realOutput.getBody(), output);

    }

    @Test
    void failAddingCustomSubcategory_NoParentId() {

        //ARRANGE
        String categoryName = "Netflix";
        String familyId = "1";
        CategoryDTO input = new CategoryDTO();
        input.setName("Netflix");
        input.setParentId("2");

        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setFamilyId(familyId);

        Mockito.when(mapper.inputToCustomCategoryDTO(familyId, input)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.checksParentIdAndAddsCustomSubcategory(categoryCreated)).thenThrow(NoSuchElementException.class);

        String errorMessage = "ParentID not found";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomCategory(familyId, input);

        //ASSERT
        assertEquals(realOutput.getBody(), expected);

    }

    @Test
    void failAddingCustomCategory_CategoryAlreadyExists() {

        //ARRANGE
        String categoryName = "Netflix";
        String familyId = "1";
        CategoryDTO input = new CategoryDTO();
        input.setName(categoryName);
        input.setParentId("");

        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setFamilyId(familyId);

        Mockito.when(mapper.inputToCustomCategoryDTO(familyId, input)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.addCustomCategory(categoryCreated)).thenThrow(DuplicatedValueException.class);

        String errorMessage = "Category already exists";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomCategory(familyId, input);

        //ASSERT
        assertEquals(realOutput.getBody(), expected);

    }

    @Test
    void failAddingCustomSubcategory_InvalidName() {

        //ARRANGE
        String categoryName = "";
        String familyId = "1";
        CategoryDTO input = new CategoryDTO();
        input.setName(categoryName);
        input.setParentId("2");

        CategoryDTO categoryCreated = new CategoryDTO();
        categoryCreated.setName(categoryName);
        categoryCreated.setFamilyId(familyId);

        Mockito.when(mapper.inputToCustomCategoryDTO(familyId, input)).thenReturn(categoryCreated);
        Mockito.when(categoryServiceMock.checksParentIdAndAddsCustomSubcategory(categoryCreated)).thenThrow(IllegalArgumentException.class);

        String errorMessage = "Invalid parameters";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomCategory(familyId, input);

        //ASSERT
        assertEquals(realOutput.getBody(), expected);

    }

    @Test
    void getsCustomCategory() {

        //ARRANGE
        String familyId = "1";
        String categoryId = "ahde3";

        CategoryDTO catExpected = new CategoryDTO();
        catExpected.setName("Roses");
        catExpected.setId(categoryId);
        catExpected.setFamilyId(familyId);

        Mockito.when(categoryServiceMock.getFamilyCategoryByCategoryId(familyId, categoryId)).thenReturn(catExpected);

        //ACT
        ResponseEntity<Object> output = controller.getFamilyCategoryByCategoryId(familyId, categoryId);

        //ASSERT
        assertEquals(output.getBody(), catExpected);
    }

    @Test
    void doesNotFindCustomCategory() {

        //ARRANGE
        String familyId = "1";
        String categoryId = "ahde3";

        Mockito.when(categoryServiceMock.getFamilyCategoryByCategoryId(familyId, categoryId)).thenThrow(NoSuchElementException.class);

        String errorMessage = "Category does not exist";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> output = controller.getFamilyCategoryByCategoryId(familyId, categoryId);

        //ASSERT
        assertEquals(output.getBody(), expected);
    }

    @Test
    void getFamilyCategoryByCategoryId_getsCustomCategoryButDoesNotBelongToFamily() {

        //ARRANGE
        String familyId = "1";
        String categoryId = "ahde3";

        Mockito.when(categoryServiceMock.getFamilyCategoryByCategoryId(familyId, categoryId)).thenThrow(IllegalStateException.class);

        String errorMessage = "Category does not belong to this family";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> output = controller.getFamilyCategoryByCategoryId(familyId, categoryId);

        //ASSERT
        assertEquals(output.getBody(), expected);
    }

    @Test
    void createAndAddLinksToExceptionDTO_ExceptionDTOIsNull() {

        //Arrange
        String familyId = "123";
        ExceptionDTO exceptionDTO = new ExceptionDTO("Error message");

        //Act
        ExceptionDTO result = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //Assert
        assertNotNull(result);

    }


}
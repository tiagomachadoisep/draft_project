package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.utils.Result;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class ChangeRelationshipControllerTest {

    private AssemblerToDTO assembler = Mockito.mock(AssemblerToDTO.class);

    private IFamilyRelationshipService service = Mockito.mock(IFamilyRelationshipService.class);

    private ChangeRelationshipController controller = new ChangeRelationshipController(assembler, service);

    private void addLinksToDto(RelationshipDTO relationshipDTO, String familyId) {

        Link selfLink = linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getRelationship(relationshipDTO.getFamilyId(), relationshipDTO.getRelationshipId()))
                .withSelfRel();

        Link changeRelLink = linkTo(ChangeRelationshipController.class).slash("families").slash(familyId).
                slash("relations").slash(relationshipDTO.getRelationshipId()).withRel("changeRel");

        Link familyLink = linkTo(methodOn(AddFamilyController.class).getFamily(relationshipDTO.getFamilyId())).
                withRel("family");

        Link personOneLink = linkTo(methodOn(GetProfileInfoController.class)
                .getProfileInfo(relationshipDTO.getFirstPersonEmail())).withRel("first person");

        Link personTwoLink = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(relationshipDTO.
                getSecondPersonEmail())).withRel("second person");

        Link relationsLink = linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getAllRelationships(familyId)).withRel("relationships");

        relationshipDTO.add(selfLink);
        relationshipDTO.add(changeRelLink);
        relationshipDTO.add(familyLink);
        relationshipDTO.add(personOneLink);
        relationshipDTO.add(personTwoLink);
        relationshipDTO.add(relationsLink);
    }

    private void addLinksToExceptionDTO(ExceptionDTO exceptionDTO, String familyId) {
        Link familyLink = linkTo(methodOn(AddFamilyController.class).getFamily(familyId)).
                withRel("family");
        Link relationsLink = linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getAllRelationships(familyId)).withRel("relationships");

        exceptionDTO.add(familyLink);
        exceptionDTO.add(relationsLink);
    }

    @Test
    void changingRelationSuccessfully() {
        //arrange
        String stringFamilyId = "1";
        String newDesignation = "cousin";
        String relationshipId = "sd2e";

        RelationshipDTO relationDTO = new RelationshipDTO();
        relationDTO.setFamilyId(stringFamilyId);
        relationDTO.setRelationshipId(relationshipId);
        relationDTO.setDesignation(newDesignation);

        RelationshipDTO serviceOutputDto = new RelationshipDTO();
        serviceOutputDto.setSecondPersonEmail("two@gmail.com");
        serviceOutputDto.setFirstPersonEmail("one@gmail.com");
        serviceOutputDto.setDesignation(newDesignation);
        serviceOutputDto.setRelationshipId(relationshipId);
        serviceOutputDto.setFamilyId(stringFamilyId);

        RelationshipDTO expectedDto = new RelationshipDTO();
        expectedDto.setSecondPersonEmail("two@gmail.com");
        expectedDto.setFirstPersonEmail("one@gmail.com");
        expectedDto.setDesignation(newDesignation);
        expectedDto.setRelationshipId(relationshipId);
        expectedDto.setFamilyId(stringFamilyId);

        addLinksToDto(expectedDto, stringFamilyId);

        ResponseEntity<Object> expected =
                new ResponseEntity<>(expectedDto, HttpStatus.ACCEPTED);

        Mockito.when(assembler.inputToRelationshipDTO(stringFamilyId, relationshipId, newDesignation))
                .thenReturn(relationDTO);
        Mockito.when(service.changeRelationship(relationDTO))
                .thenReturn(Result.success(serviceOutputDto));

        //act
        ResponseEntity<Object> result = controller.changeRelationship(stringFamilyId, relationshipId, relationDTO);

        //assert
        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void failChangingRelationFamilyDoesNotExist() {
        //arrange
        String stringFamilyId = "2";
        String newDesignation = "cousin";
        String relationshipId = "sd2e";

        RelationshipDTO relationDTO = new RelationshipDTO();
        relationDTO.setFamilyId(stringFamilyId);
        relationDTO.setRelationshipId(relationshipId);
        relationDTO.setDesignation(newDesignation);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Could not change the relationship designation! " +
                        "A family with the passed ID does not exist.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Could not change the relationship designation! " +
                        "A family with the passed ID does not exist.");

        addLinksToExceptionDTO(expectedExceptionDTO, stringFamilyId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assembler.inputToRelationshipDTO(stringFamilyId, relationshipId, newDesignation)).thenReturn(relationDTO);
        Mockito.when(service.changeRelationship(relationDTO)).thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.changeRelationship(stringFamilyId, relationshipId, relationDTO);

        //assert

        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

    @Test
    void failChangingRelationDueToNullDescription() {
        //arrange
        String stringFamilyId = "1";
        String relationshipId = "sd2e";
        RelationshipDTO relationDTO = new RelationshipDTO();
        relationDTO.setFamilyId(stringFamilyId);
        relationDTO.setRelationshipId(relationshipId);
        relationDTO.setDesignation(null);

        ExceptionDTO serviceExceptionDTO =
                new ExceptionDTO("Could not change the relationship designation! " +
                        "The relationship designation cannot be null.");

        ExceptionDTO expectedExceptionDTO =
                new ExceptionDTO("Could not change the relationship designation! " +
                        "The relationship designation cannot be null.");

        addLinksToExceptionDTO(expectedExceptionDTO, stringFamilyId);

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedExceptionDTO, HttpStatus.BAD_REQUEST);

        Mockito.when(assembler.inputToRelationshipDTO(stringFamilyId, relationshipId, null)).thenReturn(relationDTO);

        Mockito.when(service.changeRelationship(relationDTO)).thenReturn(Result.failure(serviceExceptionDTO));

        //act
        ResponseEntity<Object> result = controller.changeRelationship(stringFamilyId, relationshipId, relationDTO);

        //assert

        assertEquals(expected, result);
        assertEquals(expected.getBody(), result.getBody());

    }

}
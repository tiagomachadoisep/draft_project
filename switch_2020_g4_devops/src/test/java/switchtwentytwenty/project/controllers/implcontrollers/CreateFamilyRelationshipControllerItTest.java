package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.applicationservices.implappservices.FamilyRelationshipService;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

class CreateFamilyRelationshipControllerItTest {


    private IFamilyRepository familyRepository = Mockito.mock(IFamilyRepository.class);

    private IPersonRepository personRepository = Mockito.mock(IPersonRepository.class);

    private AssemblerToDTO assembler = new AssemblerToDTO();

    private IFamilyRelationshipService familyRelationshipService =
            new FamilyRelationshipService(personRepository, familyRepository, assembler);

    private CreateFamilyRelationshipController controller =
            new CreateFamilyRelationshipController(familyRelationshipService, assembler);

    private Family family;
    private Person personOneAna;
    private Person personTwoJose;

    private Family otherFamily;
    private Person personMaria;
    private Person personRoberto;

    @BeforeEach
    void setup() {

        family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-04-1993"))
                .build();
        String anaEmail = "ana@gmail.com";
        personOneAna = new Person.Builder(new PersonID(new Email(anaEmail))).setName(new PersonName("Ana")).setVat(new VATNumber(123456789))
                .setAddress(new Address("RuaDaAna")).setBirthDate(new BirthDate("25-03-1996")).setFamilyId(new FamilyID("1")).build();
        String joseEmail = "jose@gmail.com";
        personTwoJose = new Person.Builder(new PersonID(new Email(joseEmail))).setFamilyId(new FamilyID("1"))
                .setName(new PersonName("Jose")).setVat(new VATNumber(502011378)).setBirthDate(new BirthDate("13-06-1992")).setAddress(new Address("RuaDoJose")).build();

        otherFamily = new Family.Builder(new FamilyID("2")).setName(new FamilyName("Dias")).setAdminId(new PersonID(new Email("admin2@gmail.com")))
                .setRegistrationDate(new RegistrationDate("25-04-1994")).build();

        String mariaEmail = "maria@gmail.com";
        personMaria = new Person.Builder(new PersonID(new Email(mariaEmail))).setFamilyId(new FamilyID("2"))
                .setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setAddress(new Address("RuaDaMaria"))
                .setBirthDate(new BirthDate("25-03-1996")).build();

        String robertoEmail = "roberto@gmail.com";
        personRoberto = new Person.Builder(new PersonID(new Email(robertoEmail))).setName(new PersonName("Roberto")).setFamilyId(new FamilyID("2"))
                .setVat(new VATNumber(502011378)).setBirthDate(new BirthDate("13-06-1992")).build();
    }

    @Test
    void newFamilyRelationship_successCase() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "sister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(firstPersonEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(secondPersonEmail)))).
                thenReturn(Optional.of(personTwoJose));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(201), result.getStatusCode());
        Mockito.verify(familyRepository, times(1)).saveNew(family);

    }

    @Test
    void newFamilyRelationship_failCase_relationshipBetweenMembersAlreadyExists() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "sister";

        //Add existing relationship
        family.createRelationship(new PersonID(new Email(firstPersonEmail)), new PersonID(new Email(secondPersonEmail)),
                new FamilyRelationType(designationSister), new FamilyRelationshipID("id_123"));

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(firstPersonEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(secondPersonEmail)))).
                thenReturn(Optional.of(personTwoJose));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());
    }

    @Test
    void newFamilyRelationship_failCase_membersDoNotBelongToTheSameFamily() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "roberto@gmail.com";
        String designationSister = "sister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(firstPersonEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(secondPersonEmail)))).
                thenReturn(Optional.of(personRoberto));

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());

    }

    @Test
    void newFamilyRelationship_failCase_membersDoNotExist() {
        //arrange
        String id = "1";
        String firstPersonEmail = "anabela@gmail.com";
        String secondPersonEmail = "pedro@gmail.com";
        String designationSister = "sister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(firstPersonEmail)))).
                thenReturn(Optional.empty());
        Mockito.when(personRepository.findByID(new PersonID(new Email(secondPersonEmail)))).
                thenReturn(Optional.empty());

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());

    }

    @Test
    void newFamilyRelationship_failCase_TheFamilyDoesNotExist() {
        //arrange
        String id = "3";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "sister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        Mockito.when(familyRepository.findByID(new FamilyID(id))).thenReturn(Optional.empty());

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());


    }

    @Test
    void newFamilyRelationship_failCase_designationIsInvalid() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "littlesister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());

    }

    @Test
    void newFamilyRelationship_failCase_designationIsEmpty() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());
    }

    @Test
    void newFamilyRelationship_failCase_designationIsNull() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = null;

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());

    }

    @Test
    void newFamilyRelationship_failCase_InvalidPersonTwoEmail() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@@@gmail.com";
        String designationSister = "sister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());

    }

    @Test
    void newFamilyRelationship_failCase_InvalidPersonOneEmail() {
        //arrange
        String id = "1";
        String firstPersonEmail = "ana@@@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "sister";

        RelationshipDTO inputRelationshipDTO = new RelationshipDTO();
        inputRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        inputRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        inputRelationshipDTO.setDesignation(designationSister);

        //act
        ResponseEntity<Object> result = controller.createRelationship(inputRelationshipDTO, id);
        //assert
        assertEquals(HttpStatus.valueOf(400), result.getStatusCode());

    }

    @Test
    void getRelationship_successCase() {

        family.createRelationship(new PersonID(new Email("ana@gmail.com")),
                new PersonID(new Email("jose@gmail.com")),
                new FamilyRelationType("sister"),
                new FamilyRelationshipID("rel123"));

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email("ana@gmail.com")))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email("jose@gmail.com")))).
                thenReturn(Optional.of(personTwoJose));


        ResponseEntity result = controller.getRelationship("1", "rel123");

        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getRelationship_failCase_RelationshipNotFound() {

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email("ana@gmail.com")))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email("jose@gmail.com")))).
                thenReturn(Optional.of(personTwoJose));

        ResponseEntity result = controller.getRelationship("1", "notAnExistingId");

        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    void getRelationship_failCase_FamilyNotFound() {

        Mockito.when(familyRepository.findByID(new FamilyID("5"))).thenReturn(Optional.empty());

        ResponseEntity result = controller.getRelationship("5", "rel123");

        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    void getAllRelationships_successCase() {
        //arrange
        String famId = "1";
        String firstPersonEmail = "ana@gmail.com";
        String secondPersonEmail = "jose@gmail.com";
        String designationSister = "sister";

        //Add existing relationship
        family.createRelationship(new PersonID(new Email(firstPersonEmail)), new PersonID(new Email(secondPersonEmail)),
                new FamilyRelationType(designationSister), new FamilyRelationshipID("id_123"));

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(firstPersonEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(secondPersonEmail)))).
                thenReturn(Optional.of(personTwoJose));

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getAllRelationships_successCase_butFamilyHasNoRelationships() {
        //arrange
        String famId = "1";

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email("ana@gmail.com")))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email("jose@gmail.com")))).
                thenReturn(Optional.of(personTwoJose));

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getAllRelationships_failCase_FamilyDoesNotExist() {
        //arrange
        String famId = "543";

        Mockito.when(familyRepository.findByID(new FamilyID(famId))).thenReturn(Optional.empty());

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    void getAllRelationships_failCase_nullFamId() {
        //arrange
        String famId = null;

        //act
        ResponseEntity<Object> result = controller.getAllRelationships(famId);
        //assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }


}

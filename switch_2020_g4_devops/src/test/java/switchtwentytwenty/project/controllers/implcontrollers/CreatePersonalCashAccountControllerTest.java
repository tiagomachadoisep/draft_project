package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonalCashAccountService;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.NonExistentPersonException;
import switchtwentytwenty.project.exceptions.PersonAlreadyHasCashAccountException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class CreatePersonalCashAccountControllerTest {

    private final IPersonalCashAccountService iPersonalCashAccountService =
            Mockito.mock(IPersonalCashAccountService.class);
    private final AssemblerToDTO assemblerToDTO = Mockito.mock(AssemblerToDTO.class);
    private final CreatePersonalCashAccountController createPersonalCashAccountControllerREST =
            new CreatePersonalCashAccountController(assemblerToDTO, iPersonalCashAccountService);

    @Test
    void createPersonalCashAccount_ValidAccountAndPerson() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 30,
                EAccountType.PERSONAL_CASH);
        AccountDTO assembledDTO = new AccountDTO(ownerId, "My cash account", 30, EAccountType.PERSONAL_CASH);

        AccountDTO accountDTOBeforeLinks = new AccountDTO(ownerId, "My cash account", 30, "12ds",
                EAccountType.PERSONAL_CASH);

        AccountDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToDTO(accountDTOBeforeLinks);

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenReturn(assembledDTO);
        Mockito.when(iPersonalCashAccountService.createPersonalCashAccount(assembledDTO)).thenReturn(expected);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                        inputPersonalCashAccountDTO);

        //Assert
        assertEquals(201, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createPersonalCashAccount_NullId() {

        //Arrange
        String ownerId = null;
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 30,
                EAccountType.PERSONAL_CASH);
        String errorMessage = "Wrong info";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                null);

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenThrow(IllegalArgumentException.class);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                        inputPersonalCashAccountDTO);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createPersonalCashAccount_EmptyId() {

        //Arrange
        String ownerId = "";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 30,
                EAccountType.PERSONAL_CASH);
        String errorMessage = "Wrong info";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                "");

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenThrow(IllegalArgumentException.class);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                        inputPersonalCashAccountDTO);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createPersonalCashAccount_NullInputDTO() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = null;
        String errorMessage = "Wrong info";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenThrow(IllegalArgumentException.class);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                        inputPersonalCashAccountDTO);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createPersonalCashAccount_EmptyInputDTO() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO();
        String errorMessage = "Wrong info";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenThrow(IllegalArgumentException.class);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId,
                        inputPersonalCashAccountDTO);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createPersonalCashAccount_PersonDoesNotExist() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 30,
                EAccountType.PERSONAL_CASH);
        AccountDTO assembledDTO = new AccountDTO(ownerId, "My cash account", 30, EAccountType.PERSONAL_CASH);
        String errorMessage = "Person does not exist";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenReturn(assembledDTO);
        Mockito.when(iPersonalCashAccountService.createPersonalCashAccount(assembledDTO)).thenThrow(NonExistentPersonException.class);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId, inputPersonalCashAccountDTO);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createPersonalCashAccount_PersonExists_PersonHasACashAccount() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO("My cash account", 30,
                EAccountType.PERSONAL_CASH);
        AccountDTO assembledDTO = new AccountDTO(ownerId, "My cash account", 30, EAccountType.PERSONAL_CASH);
        String errorMessage = "Person already has a cash account";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                ownerId);

        Mockito.when(assemblerToDTO.inputToPersonalCashAccountDTO(ownerId, inputPersonalCashAccountDTO)).thenReturn(assembledDTO);
        Mockito.when(iPersonalCashAccountService.createPersonalCashAccount(assembledDTO)).thenThrow(PersonAlreadyHasCashAccountException.class);

        //Act
        ResponseEntity<Object> result =
                createPersonalCashAccountControllerREST.createPersonalCashAccount(ownerId, inputPersonalCashAccountDTO);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());
        assertNotNull(ownerId);
        assertFalse(ownerId.equals(""));
        assertFalse(errorMessage.equals("Person does not exist"));
    }

    @Test
    void getPersonalCashAccountByID_AccountExists() {

        //Arrange
        String id = "filipa@gmail.com";
        String accountID = "1244fas";
        Optional<AccountDTO> expected = Optional.of(new AccountDTO());
        expected.get().setAccountId(accountID);
        expected.get().setAccountType(EAccountType.PERSONAL_CASH);
        expected.get().setBalance(24);
        expected.get().setDescription("My cash account");
        expected.get().setOwnerId(id);

        Mockito.when(iPersonalCashAccountService.getPersonalCashAccountByID(accountID)).thenReturn(expected);

        //Act
        ResponseEntity<Object> result = createPersonalCashAccountControllerREST.getPersonalCashAccountByAccountId(id,
                accountID);

        //Assert
        assertEquals(200, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void getPersonalCashAccountByID_AccountDoesNotExist() {

        //Arrange
        String id = "filipa@gmail.com";
        String accountID = "";

        Mockito.when(iPersonalCashAccountService.getPersonalCashAccountByID(accountID)).thenThrow(IllegalArgumentException.class);

        String errorMessage = "Account does not exist";
        ExceptionDTO expected = createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                id);

        //Act
        ResponseEntity<Object> result = createPersonalCashAccountControllerREST.getPersonalCashAccountByAccountId(id,
                accountID);

        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals(expected, result.getBody());

    }

    @Test
    void createAndAddLinksToExceptionDTO() {

        //Arrange
        String ownerId = "filipa@gmail.com";
        String errorMessage = "Error message";
        ExceptionDTO expected =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage, ownerId);

        //Act
        ExceptionDTO result =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage, ownerId);

        //Assert
        assertEquals(expected, result);
        assertNotNull(result);

    }

    @Test
    void createAndAddLinksToExceptionDTO_OwnerIdIsNull() {

        //Arrange
        String ownerId = null;
        String errorMessage = "Error message";
        ExceptionDTO expected =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage, ownerId);

        //Act
        ExceptionDTO result =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage, ownerId);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void createAndAddLinksToExceptionDTO_OwnerIdIsEmpty() {

        //Arrange
        String ownerId = "";
        String errorMessage = "Error message";
        ExceptionDTO expected =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage, ownerId);

        //Act
        ExceptionDTO result =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage, ownerId);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void createAndAddLinksToExceptionDTO_PersonDoesNotExist() {

        //Arrange
        String errorMessage = "Person does not exist";
        String ownerId = "filipa@gmail.com";
        ExceptionDTO expected =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                        ownerId);

        //Act
        ExceptionDTO result =
                createPersonalCashAccountControllerREST.createAndAddLinksToExceptionDTO(errorMessage,
                        ownerId);

        //Assert
        assertEquals(expected, result);
        assertEquals(errorMessage, result.getMessage());

    }

}

package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextImpl;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.applicationservices.implappservices.AuthenticationService;
import switchtwentytwenty.project.applicationservices.implappservices.PersonService;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//@SpringBootTest
public class GetProfileInfoControllerItTest {
/*

    @Autowired
    private IFamilyService familyService;

    @Autowired
    private AssemblerToDTO assembler;
    @Mock
    private ISecurityContextProvider provider;
    @InjectMocks
    private PersonService personService;
    //private final ISecurityContextProvider provider = Mockito.mock(ISecurityContextProvider.class);

    //private final IAuthenticationService authenticationService=new AuthenticationService(provider,personService);
    @InjectMocks
    private AuthenticationService authenticationService;
    @Mock
    private IPersonRepository personRepository;

    /*@Test
    @DisplayName("Success - get the profile information")
    void getProfileInfo_success() {
        String adminEmail = "adminze@isep.pt";

        FamilyDTO dto = familyService.createAndSaveFamily(new FamilyDTO("Silva",adminEmail),new PersonDTO("0",adminEmail,"ze",0,"rua das coves 22","01-01-1900"));


        String emailAddress = "julia@isep.pt";
        PersonDTO personDTO = new PersonDTO(dto.getId(), emailAddress, "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");
        Person person = new Person.Builder(new PersonID(new Email(emailAddress))).setFamilyId(new FamilyID("110"))
                .setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Coves, 22"))
                .setBirthDate(new BirthDate("01-03-1992")).build();

        PersonID personID = personService.addMember(personDTO);

        Link selfLInk = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(emailAddress)).withSelfRel();
        personDTO.add(selfLInk);

        //ISecurityContextProvider provider = Mockito.mock(ISecurityContextProvider.class);
        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(emailAddress,"123")));

        GetProfileInfoController controller = new GetProfileInfoController(provider,authenticationService, personService, assembler);

        ResponseEntity<Object> result = controller.getProfileInfo(emailAddress);

        assertEquals(personDTO, result.getBody());
    }

    @Test
    @DisplayName("Success - get the profile information")
    void getProfileInfo_success() {
        String adminEmail = "adminze@isep.pt";
        Person person = new Person.Builder(new PersonID(new Email(adminEmail))).setFamilyId(new FamilyID("110"))
                .setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Coves, 22"))
                .setBirthDate(new BirthDate("01-03-1992")).build();
        PersonDTO expected = assembler.toPersonDTO(person);
        expected.add(linkTo(GetProfileInfoController.class).slash("members").slash("profile").withSelfRel());

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(adminEmail,"123")));
        Mockito.when(personRepository.findByID(new PersonID(new Email(adminEmail)))).thenReturn(Optional.of(person));

        GetProfileInfoController controller = new GetProfileInfoController(provider,authenticationService, personService, assembler);

        ResponseEntity<Object> result = controller.getProfileInfo(adminEmail);

        PersonDTO body = (PersonDTO) result.getBody();

        assertEquals(expected,body);

    }

    @Test
    @DisplayName("Success - get the profile information")
    void getCurrentUserProfileInfo_success() {
        String adminEmail = "adminze@isep.pt";
        Person person = new Person.Builder(new PersonID(new Email(adminEmail))).setFamilyId(new FamilyID("110"))
                .setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Coves, 22"))
                .setBirthDate(new BirthDate("01-03-1992")).build();
        PersonDTO expected = assembler.toPersonDTO(person);
        expected.add(linkTo(GetProfileInfoController.class).slash("members").slash("profile").withSelfRel());

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(adminEmail,"123")));
        Mockito.when(personRepository.findByID(new PersonID(new Email(adminEmail)))).thenReturn(Optional.of(person));

        GetProfileInfoController controller = new GetProfileInfoController(provider,authenticationService, personService, assembler);

        ResponseEntity<Object> result = controller.getCurrentUserProfileInfo();

        PersonDTO body = (PersonDTO) result.getBody();

        assertEquals(expected,body);

    }

    @Test
    @DisplayName("Failure - the person is not in the app.")
    void getProfileInfo_failure() {

        String emailAddress = "ginja@isep.pt";

        Mockito.when(personRepository.findByID(null)).thenThrow(IllegalArgumentException.class);
        //ISecurityContextProvider provider = Mockito.mock(ISecurityContextProvider.class);
        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(emailAddress,"123")));

        GetProfileInfoController controller = new GetProfileInfoController(provider,authenticationService, personService, assembler);

        ResponseEntity<Object> result = controller.getProfileInfo(emailAddress);

        assertEquals("Invalid parameter.", result.getBody());
    }

    @Test
    @DisplayName("Failure - the person is not the current user.")
    void getProfileInfo_failureTwo() {

        String emailAddress = "ginja@isep.pt";
        String userEmail = "outro@isep.pt";

        //ISecurityContextProvider provider = Mockito.mock(ISecurityContextProvider.class);
        Mockito.when(personRepository.findByID(null)).thenThrow(IllegalArgumentException.class);
        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(userEmail,"123")));

        GetProfileInfoController controller = new GetProfileInfoController(provider,authenticationService, personService, assembler);

        ResponseEntity<Object> result = controller.getProfileInfo(emailAddress);

        assertEquals("Forbidden - this is not you",((ExceptionDTO) result.getBody()).getMessage());
    }

*/

}

package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.applicationservices.implappservices.PersonService;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IFamilyIAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IPersonIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IFamilyRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;
import switchtwentytwenty.project.repositories.FamilyRepository;
import switchtwentytwenty.project.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class DeleteSecondaryEmailControllerItTest {
    private IPersonRepositoryJPA personRepositoryJPAMock = Mockito.mock(IPersonRepositoryJPA.class);
    private IPersonService personService;
    private AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
    private IPersonIAssemblerJPA personAssemblerJPAMock;
    private DeleteSecondaryEmailController ctrl;


    @BeforeEach
    void setup(){
        personAssemblerJPAMock = Mockito.mock(IPersonIAssemblerJPA.class);
        PersonRepository personRepository = new PersonRepository(personAssemblerJPAMock, personRepositoryJPAMock);
        IPersonAssembler personAssembler = new PersonAssembler();
        personService = new PersonService(personRepository, personAssembler);
        ctrl = new DeleteSecondaryEmailController(personService, assemblerToDTO);

        //Create family and Admin
        IFamilyRepositoryJPA familyRepositoryJPAMock = Mockito.mock(IFamilyRepositoryJPA.class);
        IFamilyIAssemblerJPA familyIAssemblerJPAMock = Mockito.mock(IFamilyIAssemblerJPA.class);
        FamilyRepository familyRepository = new FamilyRepository(familyRepositoryJPAMock, familyIAssemblerJPAMock);

        String luisMail = "luis@isep.pt";
        Family rodriguesFamily = new Family.Builder((new FamilyID("1")))
                .setName(new FamilyName("Rodrigues"))
                .setAdminId(new PersonID(new Email(luisMail))).build();
        familyRepository.saveNew(rodriguesFamily);



    }

    @Test
    void deleteSecondaryEmailSuccessfully() {
        //Arrange
        String mainEmail = "carlos@isep.pt";
        String emailToRemove = "carlosA@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));

        //Create Person and add Emails
        List<Email> otherEmails = new ArrayList<>();
        Email emailOne = new Email("carlosA@isep.pt");
        Email emailTwo = new Email("carlosB@isep.pt");
        Email emailThree = new Email("carlosC@isep.pt");
        Email emailFour = new Email("carlosD@isep.pt");
        otherEmails.add(emailOne);
        otherEmails.add(emailTwo);
        otherEmails.add(emailThree);
        otherEmails.add(emailFour);

        PersonJPA personJPA = new PersonJPA(mainEmail, "1", "Carlos", 252231716, "FAMILYONE_ADDRESS", "21-07-1993");
        personJPA.addEmail("carlosA@isep.pt");
        personJPA.addEmail("carlosB@isep.pt");
        personJPA.addEmail("carlosC@isep.pt");
        personJPA.addEmail("carlosD@isep.pt");

        Mockito.when(personRepositoryJPAMock.findById(personID)).thenReturn(Optional.of(personJPA));
        Mockito.when(personAssemblerJPAMock.assembleEmail(personJPA)).thenReturn(otherEmails);

        //Expected dto
        EmailDTO expectedDTO = new EmailDTO("carlosA@isep.pt", mainEmail);
        List<String> outputEmailList = new ArrayList<>();
        Link deleteOtherLink = linkTo(methodOn(DeleteSecondaryEmailController.class).deleteSecondaryEmail(expectedDTO.getEmail(), expectedDTO.getMainEmail())).withSelfRel();
        Link profileInfoLink = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(expectedDTO.getMainEmail())).withRel("Get profile information");
        expectedDTO.add(deleteOtherLink);
        expectedDTO.add(profileInfoLink);
        outputEmailList.add(emailTwo.toString());
        outputEmailList.add(emailThree.toString());
        outputEmailList.add(emailFour.toString());
        expectedDTO.setEmailsList(outputEmailList);

        //Act
        ResponseEntity result = ctrl.deleteSecondaryEmail(mainEmail, emailToRemove);


        //Assert
        assertEquals(200, result.getStatusCodeValue());
        assertNotNull(result.getBody());
        assertEquals(expectedDTO, result.getBody());
    }

    @Test
    void deleteSecondaryEmailUnsuccessfully_cannotRemoveMainEmail() {
        //Arrange
        String mainEmail = "carlos@isep.pt";
        String emailToRemove = "carlos@isep.pt";

        //Act
        ResponseEntity result = ctrl.deleteSecondaryEmail(mainEmail, emailToRemove);


        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals("Cannot remove main email", result.getBody());
    }

    @Test
    void deleteSecondaryEmailUnsuccessfully_PersonDoesNotExist() {
        //Arrange
        String mainEmail = "carlos@isep.pt";
        String emailToRemove = "carlos@isep.pt";

        Mockito.when(personRepositoryJPAMock.findById(new PersonID(new Email(mainEmail))))
                .thenReturn(Optional.empty());

        //Act
        ResponseEntity result = ctrl.deleteSecondaryEmail(mainEmail, emailToRemove);


        //Assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals("Cannot remove main email", result.getBody());
    }
}
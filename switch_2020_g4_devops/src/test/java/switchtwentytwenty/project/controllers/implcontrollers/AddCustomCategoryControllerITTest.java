package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.CategoryService;
import switchtwentytwenty.project.applicationservices.implassemblers.CategoryAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.CategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;
import switchtwentytwenty.project.repositories.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//@SpringBootTest
public class AddCustomCategoryControllerITTest {
/*
    private AddCustomCategoryController addCustomCategoryController;
    private ICategoryRepositoryJPA categoryRepositoryJPAMock;

    @BeforeEach
    void setUp() {
        categoryRepositoryJPAMock = Mockito.mock(ICategoryRepositoryJPA.class);
        CategoryAssemblerJPA categoryAssemblerJPA = new CategoryAssemblerJPA();
        CategoryRepository categoryRepository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);
        CategoryAssembler categoryAssembler = new CategoryAssembler();
        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        ApplicationContext context = new ClassPathXmlApplicationContext("repositoryhttp.xml");

        ICategoryRepositoryHttp httpRepository = context.getBean("repositoryGroup",
                ICategoryRepositoryHttp.class);
        CategoryService categoryService = new CategoryService(categoryRepository, categoryAssembler, assemblerToDTO);
        addCustomCategoryController = new AddCustomCategoryController(categoryService, assemblerToDTO);
    }

    @Test
    void successAddingCustomCategory() {

        //Arrange
        String id = "3";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId("2345A");
        inputCategoryDTO.setName("HBO");
        inputCategoryDTO.setParentId("");
        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID("2345A"));
        categoryJPA.setId(new CategoryID("3"));
        categoryJPA.setName("Hbo");
        categoryJPA.setParentCategory(null);

        Mockito.when(categoryRepositoryJPAMock.findByParentId(null)).thenReturn(new ArrayList<>());
        Mockito.when(categoryRepositoryJPAMock.save(categoryJPA)).thenReturn(categoryJPA);

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.addCustomCategory(id, inputCategoryDTO);

        //Assert
        assertNotNull(result.getBody());
        assertEquals(201, result.getStatusCodeValue());

    }

    @Test
    void successAddingCustomSubcategory_OtherCategoriesWithSameParentButWithDifferentNames() {

        //Arrange
        String id = "3";
        String parentId = "sasa1212";
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setName("HBO");
        inputCategoryDTO.setParentId(parentId);
        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID(familyID));
        categoryJPA.setId(new CategoryID("3"));
        categoryJPA.setName("Hbo");
        categoryJPA.setParentCategory(parentId);

        CategoryJPA categoryJPA_2 = new CategoryJPA();
        categoryJPA_2.setId(new CategoryID("123"));
        categoryJPA_2.setParentCategory(parentId);
        categoryJPA_2.setName("Water");
        categoryJPA_2.setFamilyId(new FamilyID(familyID));
        CategoryJPA categoryJPA_3 = new CategoryJPA();
        categoryJPA_3.setId(new CategoryID("1234"));
        categoryJPA_3.setParentCategory(parentId);
        categoryJPA_3.setName("Electricity");
        categoryJPA_3.setFamilyId(new FamilyID(familyID));

        List<CategoryJPA> categoriesWithSameParent = new ArrayList<>();
        categoriesWithSameParent.add(categoryJPA_2);
        categoriesWithSameParent.add(categoryJPA_3);

        Mockito.when(categoryRepositoryJPAMock.existsById(new CategoryID(parentId))).thenReturn(true);
        Mockito.when(categoryRepositoryJPAMock.findByParentId(parentId)).thenReturn(categoriesWithSameParent);
        Mockito.when(categoryRepositoryJPAMock.save(categoryJPA)).thenReturn(categoryJPA);

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.addCustomCategory(id, inputCategoryDTO);

        //Assert
        assertNotNull(result.getBody());
        assertEquals(201, result.getStatusCodeValue());

    }

    @Test
    void failAddingCustomSubcategory_NoParentId() {

        //Arrange
        String parentId = "sasa1212";
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setName("HBO");
        inputCategoryDTO.setParentId(parentId);
        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID(familyID));
        categoryJPA.setId(new CategoryID("3"));
        categoryJPA.setName("Hbo");
        categoryJPA.setParentCategory(parentId);

        String errorMessage = "ParentID not found";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryController.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        Mockito.when(categoryRepositoryJPAMock.existsById(new CategoryID(parentId))).thenReturn(false);

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.addCustomCategory(familyID, inputCategoryDTO);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }

    @Test
    void failAddingCustomSubcategory_CategoryAlreadyExists() {

        //Arrange
        String parentId = "sasa1212";
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setName("HBO");
        inputCategoryDTO.setParentId(parentId);
        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID(familyID));
        categoryJPA.setId(new CategoryID("3"));
        categoryJPA.setName("Hbo");
        categoryJPA.setParentCategory(parentId);

        CategoryJPA categoryJPA_2 = new CategoryJPA();
        categoryJPA_2.setId(new CategoryID("123"));
        categoryJPA_2.setParentCategory(parentId);
        categoryJPA_2.setName("Hbo");
        categoryJPA_2.setFamilyId(new FamilyID(familyID));
        CategoryJPA categoryJPA_3 = new CategoryJPA();
        categoryJPA_3.setId(new CategoryID("1234"));
        categoryJPA_3.setParentCategory(parentId);
        categoryJPA_3.setName("Electricity");
        categoryJPA_3.setFamilyId(new FamilyID(familyID));

        List<CategoryJPA> categoriesWithSameParent = new ArrayList<>();
        categoriesWithSameParent.add(categoryJPA_2);
        categoriesWithSameParent.add(categoryJPA_3);

        String errorMessage = "Category already exists";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryController.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        Mockito.when(categoryRepositoryJPAMock.existsById(new CategoryID(parentId))).thenReturn(true);
        Mockito.when(categoryRepositoryJPAMock.findByParentId(parentId)).thenReturn(categoriesWithSameParent);

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.addCustomCategory(familyID, inputCategoryDTO);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }

    @Test
    void failAddingCustomSubcategory_InvalidName() {

        //Arrange
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setParentId("");
        inputCategoryDTO.setName("");

        String errorMessage = "Invalid parameters";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryController.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.addCustomCategory(familyID, inputCategoryDTO);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }

    @Test
    void getFamilyCategoryByCategoryId_getsCustomCategory() {

        //Arrange
        String id = "31S";
        String familyID = "skd32";
        CategoryDTO expected = new CategoryDTO();
        expected.setName("Gifts");
        expected.setId(id);
        expected.setFamilyId(familyID);

        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID(familyID));
        categoryJPA.setId(new CategoryID(id));
        categoryJPA.setName("Gifts");

        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID(id))).thenReturn(Optional.of(categoryJPA));

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.getFamilyCategoryByCategoryId(familyID, id);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(200, result.getStatusCodeValue());

    }

    @Test
    void getFamilyCategoryByCategoryId_DoesNotFindCustomCategory() {

        //Arrange
        String id = "31S";
        String familyID = "skd32";

        String errorMessage = "Category does not exist";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryController.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID(id))).thenReturn(Optional.empty());

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.getFamilyCategoryByCategoryId(familyID, id);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }

    @Test
    void getFamilyCategoryByCategoryId_getsCustomCategoryButDoesNotBelongToFamily() {

        //Arrange
        String id = "31S";
        String familyID = "skd32";

        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID("adfsfdf"));
        categoryJPA.setId(new CategoryID(id));
        categoryJPA.setName("Gifts");

        String errorMessage = "Category does not belong to this family";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryController.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID(id))).thenReturn(Optional.of(categoryJPA));

        //Act
        ResponseEntity<Object> result = addCustomCategoryController.getFamilyCategoryByCategoryId(familyID, id);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }*/

}

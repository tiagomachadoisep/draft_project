package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.implappservices.CategoryService;
import switchtwentytwenty.project.applicationservices.implassemblers.CategoryAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepository;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.CategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;
import switchtwentytwenty.project.repositories.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AddStandardCategoryControllerItTest {

    private ICategoryRepositoryJPA categoryRepositoryJPAMock;
    private ICategoryService service;
    private ApplicationContext context = new ClassPathXmlApplicationContext("repositoryhttp.xml");
    private ICategoryRepositoryHttp httpRepository = context.getBean("repositoryGroup",
            ICategoryRepositoryHttp.class);
    private AddStandardCategoryController ctrl;

    @BeforeEach
    void dataSetUp() {
        categoryRepositoryJPAMock = Mockito.mock(ICategoryRepositoryJPA.class);
        ICategoryRepository categoryRepository = new CategoryRepository(new CategoryAssemblerJPA(),
                categoryRepositoryJPAMock);
        service = new CategoryService(categoryRepository, new CategoryAssembler(), new AssemblerToDTO());
        ctrl = new AddStandardCategoryController(service, new AssemblerToDTO());
    }

    @Test
    void standardCategoryAdded() {

        CategoryDTO dto = new CategoryDTO();
        dto.setName("Dentist");

        Mockito.when(categoryRepositoryJPAMock.findByParentId(null)).thenReturn(new ArrayList<>());

        //ACT
        ResponseEntity<Object> output = ctrl.addStandardCategory(dto);
        //ASSERT
        assertNotNull(output);
    }

    @Test
    void standardSubcategoryAdded() {

        CategoryDTO dto = new CategoryDTO();
        dto.setName("Dentist");
        dto.setParentId("1");

        Mockito.when(categoryRepositoryJPAMock.findByParentId("1")).thenReturn(new ArrayList<>());
        Mockito.when(categoryRepositoryJPAMock.existsById(new CategoryID("1"))).thenReturn(true);
        //ACT
        ResponseEntity<Object> output = ctrl.addStandardCategory(dto);
        //ASSERT
        assertNotNull(output);
    }

    @Test
    void failAddingStandardSubcategory_ParentIdDoesNotExist() {

        CategoryDTO dto = new CategoryDTO();
        dto.setName("Dentist");
        dto.setParentId("30");
        String expected = "ParentID not found";

        Mockito.when(categoryRepositoryJPAMock.existsById(new CategoryID("30"))).thenReturn(false);
        //ACT
        ResponseEntity<Object> output = ctrl.addStandardCategory(dto);
        //ASSERT
        assertEquals(output.getBody(), expected);
    }

    @Test
    void failAddingStandardCategory_CategoryAlreadyExists() {

        CategoryDTO dto = new CategoryDTO();
        dto.setName("Dentist");
        dto.setParentId(null);
        String expected = "Category already exists";

        CategoryJPA jpa = new CategoryJPA();
        jpa.setName("Dentist");
        jpa.setId(new CategoryID("2"));
        List<CategoryJPA> categories = new ArrayList<>();
        categories.add(jpa);


        Mockito.when(categoryRepositoryJPAMock.findByParentId(null)).thenReturn(categories);
        //ACT
        ResponseEntity<Object> output = ctrl.addStandardCategory(dto);
        //ASSERT
        assertEquals(output.getBody(), expected);
    }

    @Test
    void failAddingStandardCategory_InvalidCategoryName() {

        CategoryDTO dto = new CategoryDTO();
        dto.setName("1963jdam");
        dto.setParentId(null);
        String expected = "Invalid parameters";

        //ACT
        ResponseEntity<Object> output = ctrl.addStandardCategory(dto);
        //ASSERT
        assertEquals(output.getBody(), expected);
    }

}

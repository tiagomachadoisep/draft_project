package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iassemblers.IAccountAssembler;
import switchtwentytwenty.project.applicationservices.implappservices.AccountService;
import switchtwentytwenty.project.applicationservices.implassemblers.AccountAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IAccountRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.AccountAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IAccountAssemblerJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.repositories.AccountRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddBankSavingsAccountControllerITTest {

    private final IAccountRepositoryJPA accountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
    private final IAccountAssemblerJPA accountAssemblerJPA = new AccountAssemblerJPA();
    private final IAccountRepository accountRepository = new AccountRepository(accountAssemblerJPA, accountRepositoryJPAMock);
    private final IAccountAssembler accountAssembler = new AccountAssembler();
    private final AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
    private final IPersonRepository iPersonRepositoryMock = Mockito.mock(IPersonRepository.class);
    private final AccountService accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO, iPersonRepositoryMock);

    @Test
    void successByAddingBankSavingsAccount() {
        //ARRANGE
        AddBankSavingsAccountController ctrl = new AddBankSavingsAccountController(assemblerToDTO, accountService);

        String id = "goncalo@isep.com";
        AccountDTO dto = new AccountDTO();
        dto.setOwnerId(id);
        dto.setDescription("Millenium");
        dto.setBalance(100);

        AccountDTO expected = new AccountDTO();
        expected.setAccountId("1");
        expected.setOwnerId(id);
        expected.setDescription("Millenium");
        expected.setBalance(100);
        expected.setAccountType(EAccountType.BANK_SAVINGS);

        //ACT
        ResponseEntity<Object> result = ctrl.addBankSavingsAccount(id, dto);
        AccountDTO actualAccountDTO = (AccountDTO) result.getBody();

        //assert
        assert actualAccountDTO != null;
        assertEquals(expected.getAccountType(), actualAccountDTO.getAccountType());
        assertEquals(expected.getOwnerId(), actualAccountDTO.getOwnerId());
        assertEquals(expected.getBalance(), actualAccountDTO.getBalance());
        assertEquals(expected.getDescription(), actualAccountDTO.getDescription());
    }

    @Test
    void failAddingBankSavingsAccount_ownerIdNotPresent() {
        //ARRANGE
        String id = null;
        AccountDTO dto = new AccountDTO();
        dto.setOwnerId(id);
        dto.setDescription("Millenium");
        dto.setBalance(100);


        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        AddBankSavingsAccountController ctrl = new AddBankSavingsAccountController(assemblerToDTO, accountService);

        //ACT
        ResponseEntity<Object> result = ctrl.addBankSavingsAccount(id, dto);

        //assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }
}

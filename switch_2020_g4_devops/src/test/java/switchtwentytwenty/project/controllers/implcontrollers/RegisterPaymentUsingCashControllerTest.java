package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.applicationservices.implappservices.LedgerService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.PaymentDTO;

import static org.junit.jupiter.api.Assertions.*;

//@SpringBootTest
class RegisterPaymentUsingCashControllerTest {

    //@Mock
    private ILedgerService ledgerServiceMock= Mockito.mock(ILedgerService.class);
    //@Mock
    private AssemblerToDTO mapperMock= Mockito.mock(AssemblerToDTO.class);
    //@InjectMocks
    private RegisterPaymentUsingCashController ctrl= new RegisterPaymentUsingCashController(ledgerServiceMock,mapperMock);

    @Test
    void registerPayment() {
        //Arrange
        String mainEmail="ze@isep.pt";
        double amount=20;
        String categoryId="1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail,amount,categoryId);
        PaymentDTO expected=new PaymentDTO(mainEmail,amount,categoryId);

        Mockito.when(mapperMock.toPaymentDTO(mainEmail,amount,categoryId)).thenReturn(paymentDTO);
        Mockito.when(ledgerServiceMock.registerPaymentDS(paymentDTO)).thenReturn(expected);

        //Act
        PaymentDTO result=ctrl.registerPayment(mainEmail,amount,categoryId);

        //Assert
        assertEquals(expected,result);
    }

    @Test
    void registerPaymentRest() {
        //arrange
        String mainEmail="ze@isep.pt";
        double amount=20;
        String categoryId="1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail,amount,categoryId);
        PaymentDTO exitDTO = new PaymentDTO(mainEmail,amount,categoryId);
        ResponseEntity<Object> expected = new ResponseEntity<>(exitDTO, HttpStatus.CREATED);

        Mockito.when(ledgerServiceMock.registerPaymentDS(paymentDTO)).thenReturn(exitDTO);
        //act
        ResponseEntity<Object> result = ctrl.registerPaymentRest(paymentDTO);
        //assert
        assertEquals(expected,result);
    }

    @Test
    void registerPaymentRestInsufficientFunds(){
        //arrange
        String mainEmail="ze@isep.pt";
        double amount=20;
        String categoryId="1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail,amount,categoryId);
        PaymentDTO exitDTO = new PaymentDTO(mainEmail,0,categoryId);
        ResponseEntity<Object> expected = new ResponseEntity<>("Insufficient funds", HttpStatus.BAD_REQUEST);

        Mockito.when(ledgerServiceMock.registerPaymentDS(paymentDTO)).thenReturn(exitDTO);
        //act
        ResponseEntity<Object> result = ctrl.registerPaymentRest(paymentDTO);
        //assert
        assertEquals(expected,result);
    }
}
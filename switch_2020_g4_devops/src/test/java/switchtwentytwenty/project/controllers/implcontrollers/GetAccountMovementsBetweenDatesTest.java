package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.applicationservices.implappservices.LedgerService;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;
import switchtwentytwenty.project.dto.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class GetAccountMovementsBetweenDatesTest {
    private final ILedgerService ledgerServiceMock = Mockito.mock(LedgerService.class);
    private final GetAccountMovementsBetweenDatesController ctrl = new GetAccountMovementsBetweenDatesController(ledgerServiceMock);

    @Test
    void GetAccountMovementsBetweenDates_Success() {

        //ARRANGE
        LocalDate date1 = LocalDate.of(2021,1,1);

        MovementDTO movementDTOExpected = new MovementDTO("1", -10);
        List<MovementDTO> expected = new ArrayList<>();
        Link transactionInfo = linkTo(methodOn(GetAccountMovementsBetweenDatesController.class).
                getTransaction(1, new TransactionID("1").toString())).withRel("Possible Transactions Info");
        movementDTOExpected.add(transactionInfo);
        expected.add(movementDTOExpected);

        MovementDTO movementDTO = new MovementDTO("1", -10);
        List<MovementDTO> movementsDTO = new ArrayList<>();
        movementsDTO.add(movementDTO);

        TransactionDTO transactionDTO = new PaymentDTO(null, 10, "comida", "1",
                "1", date1);
        List<TransactionDTO> transactionsDTO = new ArrayList<>();
        transactionsDTO.add(transactionDTO);

        //ACT
        Mockito.when(ledgerServiceMock.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"),
                new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"))).
                        thenReturn(movementsDTO);
        Mockito.when(ledgerServiceMock.findPossibleTransactionsByMovement(new LedgerID(1), new AccountID("1"),
                new MoneyValue(10).toNegative())).thenReturn(transactionsDTO);

        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, "1",
                "2021-01-01", "2021-12-31");

        //ASSERT
        List<MovementDTO> actual = (List<MovementDTO>) result.getBody();
        assert actual != null;
        for (MovementDTO movementExpected : expected) {
            for (MovementDTO movementActual : actual) {
                assertEquals(movementExpected.getAmount(), movementActual.getAmount());
                assertEquals(movementExpected.getAccountID(), movementActual.getAccountID());
                break;
            }
        }

        assertEquals(expected, result.getBody());
    }

    @Test
    void GetAccountMovementsBetweenDates_Failure_DifferentAccounts() {

        //ARRANGE
        LocalDate date1 = LocalDate.of(2021,1,1);

        MovementDTO movementDTOExpected = new MovementDTO("1", -10);
        List<MovementDTO> expected = new ArrayList<>();
        Link transactionInfo = linkTo(methodOn(GetAccountMovementsBetweenDatesController.class).
                getTransaction(1, new TransactionID("1").toString())).withRel("Possible Transactions Info");
        movementDTOExpected.add(transactionInfo);
        expected.add(movementDTOExpected);

        MovementDTO movementDTO = new MovementDTO("2", -10);
        List<MovementDTO> movementsDTO = new ArrayList<>();
        movementsDTO.add(movementDTO);

        TransactionDTO transactionDTO = new PaymentDTO(null, 10, "comida", "1",
                "1", date1);
        List<TransactionDTO> transactionsDTO = new ArrayList<>();
        transactionsDTO.add(transactionDTO);

        //ACT
        Mockito.when(ledgerServiceMock.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"),
                new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"))).
                thenReturn(movementsDTO);
        Mockito.when(ledgerServiceMock.findPossibleTransactionsByMovement(new LedgerID(1), new AccountID("1"),
                new MoneyValue(10).toNegative())).thenReturn(transactionsDTO);

        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, "1",
                "2021-01-01", "2021-12-31");

        //ASSERT
        List<MovementDTO> actual = (List<MovementDTO>) result.getBody();
        assert actual != null;
        for (MovementDTO movementExpected : expected) {
            for (MovementDTO movementActual : actual) {
                assertEquals(movementExpected.getAmount(), movementActual.getAmount());
                assertNotEquals(movementExpected.getAccountID(), movementActual.getAccountID());
                break;
            }
        }
    }

    @Test
    void GetAccountMovementsBetweenDates_Failure_DifferentAmounts() {

        //ARRANGE
        LocalDate date1 = LocalDate.of(2021,1,1);

        MovementDTO movementDTOExpected = new MovementDTO("1", -20);
        List<MovementDTO> expected = new ArrayList<>();
        Link transactionInfo = linkTo(methodOn(GetAccountMovementsBetweenDatesController.class).
                getTransaction(1, new TransactionID("1").toString())).withRel("Possible Transactions Info");
        movementDTOExpected.add(transactionInfo);
        expected.add(movementDTOExpected);

        MovementDTO movementDTO = new MovementDTO("1", -10);
        List<MovementDTO> movementsDTO = new ArrayList<>();
        movementsDTO.add(movementDTO);

        TransactionDTO transactionDTO = new PaymentDTO(null, 10, "comida", "1",
                "1", date1);
        List<TransactionDTO> transactionsDTO = new ArrayList<>();
        transactionsDTO.add(transactionDTO);

        //ACT
        Mockito.when(ledgerServiceMock.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"),
                new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"))).
                thenReturn(movementsDTO);
        Mockito.when(ledgerServiceMock.findPossibleTransactionsByMovement(new LedgerID(1), new AccountID("1"),
                new MoneyValue(10).toNegative())).thenReturn(transactionsDTO);

        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, "1",
                "2021-01-01", "2021-12-31");

        //ASSERT
        List<MovementDTO> actual = (List<MovementDTO>) result.getBody();
        assert actual != null;
        for (MovementDTO movementExpected : expected) {
            for (MovementDTO movementActual : actual) {
                assertNotEquals(movementExpected.getAmount(), movementActual.getAmount());
                assertEquals(movementExpected.getAccountID(), movementActual.getAccountID());
                break;
            }
        }

    }

    @Test
    void GetAccountMovementsBetweenDates_Failure_DifferentLinks() {

        //ARRANGE
        LocalDate date1 = LocalDate.of(2021,1,1);

        MovementDTO movementDTOExpected = new MovementDTO("1", -10);
        List<MovementDTO> expected = new ArrayList<>();
        expected.add(movementDTOExpected);

        MovementDTO movementDTO = new MovementDTO("1", -10);
        List<MovementDTO> movementsDTO = new ArrayList<>();
        movementsDTO.add(movementDTO);

        TransactionDTO transactionDTO = new PaymentDTO(null, 10, "comida", "1",
                "1", date1);
        List<TransactionDTO> transactionsDTO = new ArrayList<>();
        transactionsDTO.add(transactionDTO);

        //ACT
        Mockito.when(ledgerServiceMock.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"),
                new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"))).
                thenReturn(movementsDTO);
        Mockito.when(ledgerServiceMock.findPossibleTransactionsByMovement(new LedgerID(1), new AccountID("1"),
                new MoneyValue(10).toNegative())).thenReturn(transactionsDTO);

        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, "1",
                "2021-01-01", "2021-12-31");

        //ASSERT
        List<MovementDTO> actual = (List<MovementDTO>) result.getBody();
        assert actual != null;
        for (MovementDTO movementExpected : expected) {
            for (MovementDTO movementActual : actual) {
                assertEquals(movementExpected.getAmount(), movementActual.getAmount());
                assertEquals(movementExpected.getAccountID(), movementActual.getAccountID());
                break;
            }
        }
    }

    @Test
    public void getTransaction(){
        //arrange
        int ledgerID = 1;
        String transactionID = "321";

        TransactionDTO transactionDTO = new PaymentDTO("Test");
        Mockito.when(ledgerServiceMock.findTransactionByID(new TransactionID(transactionID), new LedgerID(ledgerID))).thenReturn(transactionDTO);

        //act
        ResponseEntity result = ctrl.getTransaction(ledgerID, transactionID);

        //assert
        assertEquals(200, result.getStatusCodeValue());
        assertNotNull(result.getBody());
    }

    @Test
    public void unableToGetTransactionDueToNonExistingTransaction(){
        //arrange
        int ledgerID = 1;
        String transactionID = "321";

        TransactionDTO transactionDTO = new PaymentDTO("Test");
        Mockito.when(ledgerServiceMock.findTransactionByID(new TransactionID(transactionID), new LedgerID(ledgerID))).thenThrow(NullPointerException.class);

        //act
        ResponseEntity result = ctrl.getTransaction(ledgerID, transactionID);

        //assert
        assertEquals(400, result.getStatusCodeValue());
        assertEquals("Wrong info", result.getBody());
    }
}



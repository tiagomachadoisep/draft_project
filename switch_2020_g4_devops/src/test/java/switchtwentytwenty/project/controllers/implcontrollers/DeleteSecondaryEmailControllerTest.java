package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class DeleteSecondaryEmailControllerTest {
    private IPersonService personService = Mockito.mock(IPersonService.class);
    private AssemblerToDTO assemblerToDTO = Mockito.mock(AssemblerToDTO.class);
    private DeleteSecondaryEmailController ctrl = new DeleteSecondaryEmailController(personService, assemblerToDTO);

    @Test
    void deleteSecondaryEmailSuccessfully() {
        String ownerID = "carlos@isep.pt";
        String emailToRemove = "carlosA@isep.pt";
        EmailDTO dto = new EmailDTO(emailToRemove, ownerID);

        Mockito.when(assemblerToDTO.toEmailDTO(emailToRemove,ownerID)).thenReturn(dto);

        List<String> emailList = new ArrayList<>();
        emailList.add("carlosB@isep.pt");
        emailList.add("carlosC@isep.pt");
        emailList.add("carlosD@isep.pt");
        Mockito.when(personService.deleteSecondaryEmail(dto)).thenReturn(emailList);

        EmailDTO outputDTO = new EmailDTO(emailToRemove, ownerID);
        outputDTO.setEmailsList(emailList);

        ResponseEntity result = ctrl.deleteSecondaryEmail(ownerID, emailToRemove);


        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void deleteSecondaryEmailUnsuccessfully() {
        String ownerID = "carlos@isep.pt";
        String emailToRemove = "carlosA@isep.pt";
        EmailDTO dto = new EmailDTO(emailToRemove, ownerID);

        Mockito.when(assemblerToDTO.toEmailDTO(emailToRemove,ownerID)).thenReturn(dto);

        Mockito.when(personService.deleteSecondaryEmail(dto)).thenThrow(IllegalArgumentException.class);


        ResponseEntity result = ctrl.deleteSecondaryEmail(ownerID, emailToRemove);

        assertEquals(400, result.getStatusCodeValue());
    }
}

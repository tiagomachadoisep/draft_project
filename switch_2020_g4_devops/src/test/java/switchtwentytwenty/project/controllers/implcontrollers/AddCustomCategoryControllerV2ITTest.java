package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.CategoryService;
import switchtwentytwenty.project.applicationservices.implassemblers.CategoryAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.CategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;
import switchtwentytwenty.project.repositories.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AddCustomCategoryControllerV2ITTest {

    private AddCustomCategoryControllerV2 addCustomCategoryControllerV2;
    private ICategoryRepositoryJPA categoryRepositoryJPAMock;
    private ICategoryRepositoryHttp categoryRepositoryHTTP;

    @BeforeEach
    void setUp() {
        categoryRepositoryJPAMock = Mockito.mock(ICategoryRepositoryJPA.class);
        CategoryAssemblerJPA categoryAssemblerJPA = new CategoryAssemblerJPA();
        CategoryRepository categoryRepository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);
        CategoryAssembler categoryAssembler = new CategoryAssembler();
        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        ApplicationContext applicationContext = Mockito.mock(ApplicationContext.class);
        categoryRepositoryHTTP = Mockito.mock(ICategoryRepositoryHttp.class);
        CategoryService categoryService = new CategoryService(categoryRepository, categoryAssembler, assemblerToDTO);

        addCustomCategoryControllerV2 = new AddCustomCategoryControllerV2(categoryService, assemblerToDTO);
    }

    public List<CategoryJPA> getInternalStandardCategoryJPAList() {

        List<CategoryJPA> internalStandardCategoriesJPA = new ArrayList<>();
        CategoryJPA internalStandardCategoryJPA_1 = new CategoryJPA();
        internalStandardCategoryJPA_1.setId(new CategoryID("parentId"));
        internalStandardCategoryJPA_1.setName("Football");
        internalStandardCategoriesJPA.add(internalStandardCategoryJPA_1);

        return internalStandardCategoriesJPA;

    }

    public List<CategoryJPA> getCategoriesWithSameParent() {

        CategoryJPA categoryJPA_2 = new CategoryJPA();
        categoryJPA_2.setId(new CategoryID("123456"));
        categoryJPA_2.setParentCategory("parentId");
        categoryJPA_2.setName("Benficatv");
        categoryJPA_2.setFamilyId(new FamilyID("2345A"));

        List<CategoryJPA> categoriesWithSameParent = new ArrayList<>();
        categoriesWithSameParent.add(categoryJPA_2);

        return categoriesWithSameParent;
    }

    public List<CategoryDTO> getExternalStandardCategoryList() {

        CategoryDTO dto0 = new CategoryDTO("All", "HTTPGIST/0", "", "");
        CategoryDTO dto1 = new CategoryDTO("Books", "HTTPGIST/1", "", "");
        CategoryDTO dto2 = new CategoryDTO("Hardcover", "HTTPGIST/1", "HTTPGIST/1", "");
        CategoryDTO dto3 = new CategoryDTO("Paperback", "HTTPGIST/2", "HTTPGIST/1", "");
        CategoryDTO dto4 = new CategoryDTO("Electronic", "HTTPGIST/3", "HTTPGIST/1", "");
        CategoryDTO dto5 = new CategoryDTO("Movies", "HTTPGIST/2", "", "");
        CategoryDTO dto6 = new CategoryDTO("DVD", "HTTPGIST/4", "HTTPGIST/2", "");
        CategoryDTO dto7 = new CategoryDTO("BluRay", "HTTPGIST/5", "HTTPGIST/2", "");
        CategoryDTO dto8 = new CategoryDTO("Download", "HTTPGIST/6", "HTTPGIST/2", "");
        CategoryDTO dto9 = new CategoryDTO("Games", "HTTPGIST/3", "", "");
        CategoryDTO dto10 = new CategoryDTO("XBox", "HTTPGIST/7", "HTTPGIST/3", "");
        CategoryDTO dto11 = new CategoryDTO("PC", "HTTPGIST/8", "HTTPGIST/3", "");
        CategoryDTO dto12 = new CategoryDTO("Music", "HTTPGIST/4", "", "");

        List<CategoryDTO> externalStandardCategoriesList = new ArrayList<>();
        externalStandardCategoriesList.add(dto0);
        externalStandardCategoriesList.add(dto1);
        externalStandardCategoriesList.add(dto2);
        externalStandardCategoriesList.add(dto3);
        externalStandardCategoriesList.add(dto4);
        externalStandardCategoriesList.add(dto5);
        externalStandardCategoriesList.add(dto6);
        externalStandardCategoriesList.add(dto7);
        externalStandardCategoriesList.add(dto8);
        externalStandardCategoriesList.add(dto9);
        externalStandardCategoriesList.add(dto10);
        externalStandardCategoriesList.add(dto11);
        externalStandardCategoriesList.add(dto12);

        return externalStandardCategoriesList;

    }

    @Test
    void successAddingCustomSubcategory_OtherCategoriesWithSameParentButWithDifferentNames() {

        //Arrange
        String id = "3";
        String parentId = "parentId";
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setName("SportTV");
        inputCategoryDTO.setParentId(parentId);

        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID(familyID));
        categoryJPA.setId(new CategoryID(id));
        categoryJPA.setName("Sporttv");
        categoryJPA.setParentCategory(parentId);

        Mockito.when(categoryRepositoryJPAMock.findAll()).thenReturn(getInternalStandardCategoryJPAList());
        Mockito.when(categoryRepositoryHTTP.importCategories()).thenReturn(getExternalStandardCategoryList());
        Mockito.when(categoryRepositoryJPAMock.findByParentId(parentId)).thenReturn(getCategoriesWithSameParent());
        Mockito.when(categoryRepositoryJPAMock.save(categoryJPA)).thenReturn(categoryJPA);

        //Act
        ResponseEntity<Object> result = addCustomCategoryControllerV2.addCustomSubcategoryV2(id, inputCategoryDTO);

        //Assert
        assertNotNull(result.getBody());
        assertEquals(201, result.getStatusCodeValue());

    }

    @Test
    void failAddingCustomSubcategory_ParentCategoryIsNotAvailable() {

        //Arrange
        String parentId = "cx";
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setName("SportTV");
        inputCategoryDTO.setParentId(parentId);

        Mockito.when(categoryRepositoryJPAMock.findAll()).thenReturn(getInternalStandardCategoryJPAList());
        Mockito.when(categoryRepositoryHTTP.importCategories()).thenReturn(getExternalStandardCategoryList());

        String errorMessage = "Category creation failed! You must select a parent category.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryControllerV2.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        //Act
        ResponseEntity<Object> result = addCustomCategoryControllerV2.addCustomSubcategoryV2(familyID,
                inputCategoryDTO);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }

    @Test
    void failAddingCustomSubcategory_CategoryAlreadyExists() {

        //Arrange
        String id = "3";
        String parentId = "parentId";
        String familyID = "2345A";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setName("BenficaTV");
        inputCategoryDTO.setParentId(parentId);

        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setFamilyId(new FamilyID(familyID));
        categoryJPA.setId(new CategoryID(id));
        categoryJPA.setName("Benficatv");
        categoryJPA.setParentCategory(parentId);

        Mockito.when(categoryRepositoryJPAMock.findAll()).thenReturn(getInternalStandardCategoryJPAList());
        Mockito.when(categoryRepositoryHTTP.importCategories()).thenReturn(getExternalStandardCategoryList());
        Mockito.when(categoryRepositoryJPAMock.findByParentId(parentId)).thenReturn(getCategoriesWithSameParent());

        String errorMessage = "Category creation failed! A category with that name already exists.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryControllerV2.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        //Act
        ResponseEntity<Object> result = addCustomCategoryControllerV2.addCustomSubcategoryV2(familyID,
                inputCategoryDTO);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());

    }

    @Test
    void failAddingCustomSubcategory_InvalidName() {

        //Arrange
        String familyID = "2345A";
        String parentId = "parentId";
        CategoryDTO inputCategoryDTO = new CategoryDTO();
        inputCategoryDTO.setFamilyId(familyID);
        inputCategoryDTO.setParentId(parentId);
        inputCategoryDTO.setName("");

        Mockito.when(categoryRepositoryJPAMock.findAll()).thenReturn(getInternalStandardCategoryJPAList());
        Mockito.when(categoryRepositoryHTTP.importCategories()).thenReturn(getExternalStandardCategoryList());
        Mockito.when(categoryRepositoryJPAMock.findByParentId(parentId)).thenReturn(getCategoriesWithSameParent());

        String errorMessage = "Category creation failed! Invalid category name. Spaces and numbers are not allowed.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = addCustomCategoryControllerV2.createAndAddLinksToExceptionDTO(exceptionDTO, familyID);

        //Act
        ResponseEntity<Object> result = addCustomCategoryControllerV2.addCustomSubcategoryV2(familyID,
                inputCategoryDTO);

        //Assert
        assertEquals(expected, result.getBody());
        assertEquals(400, result.getStatusCodeValue());
        assertNotNull(result);

    }


}

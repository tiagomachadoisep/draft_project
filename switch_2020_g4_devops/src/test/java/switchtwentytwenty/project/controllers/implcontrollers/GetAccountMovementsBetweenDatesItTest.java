package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.applicationservices.iassemblers.IAccountAssembler;
import switchtwentytwenty.project.applicationservices.implappservices.AccountService;
import switchtwentytwenty.project.applicationservices.implappservices.LedgerService;
import switchtwentytwenty.project.applicationservices.implassemblers.AccountAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.*;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.domainservices.ILedgerAccountService;
import switchtwentytwenty.project.domain.domainservices.LedgerAccountService;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.MovementDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.CategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.implementations.FamilyAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.implementations.LedgerAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ICategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IFamilyIAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ILedgerIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.LedgerJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IFamilyRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ILedgerRepositoryJPA;
import switchtwentytwenty.project.repositories.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class GetAccountMovementsBetweenDatesItTest {

    private final IAccountRepository accountRepository = new AccountRepository();
    private final IAccountAssembler accountAssembler = new AccountAssembler();
    private final IPersonRepository iPersonRepository= new PersonRepository();
    private final AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
    private final IAccountService accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO,iPersonRepository);

    private final ILedgerRepositoryJPA ledgerRepositoryJPA = Mockito.mock(ILedgerRepositoryJPA.class);
    private final ILedgerIAssemblerJPA ledgerAssemblerJPA = new LedgerAssemblerJPA();
    private final ILedgerRepository ledgerRepository = new LedgerRepository(ledgerRepositoryJPA, ledgerAssemblerJPA);
    private final IPersonRepository personRepository = new PersonRepository();
    private final ICategoryAssemblerJPA categoryAssemblerJPA = new CategoryAssemblerJPA();
    private final ICategoryRepositoryJPA categoryRepositoryJPA = Mockito.mock(ICategoryRepositoryJPA.class);
    private final ICategoryRepository categoryRepository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPA);
    private final IFamilyIAssemblerJPA familyAssemblerJPA = new FamilyAssemblerJPA();
    private final IFamilyRepositoryJPA familyRepositoryJPA = Mockito.mock(IFamilyRepositoryJPA.class);
    private final IFamilyRepository familyRepository = new FamilyRepository(familyRepositoryJPA, familyAssemblerJPA);
    private final ILedgerAccountService ledgerAccountService = new LedgerAccountService();
    private final ILedgerService ledgerService= new LedgerService(ledgerRepository, accountRepository, personRepository,
            categoryRepository, familyRepository, ledgerAccountService, assemblerToDTO);

    private final GetAccountMovementsBetweenDatesController ctrl = new GetAccountMovementsBetweenDatesController
            (ledgerService);

    @Test
    void GetAccountMovementsBetweenDates_Success() {

        //ARRANGE
        String id = "goncalo@isep.pt";
        LocalDate date1 = LocalDate.of(2021,1,2);
        LocalDate date2 = LocalDate.of(2020,12,31);

        MovementDTO movementDTOExpected = new MovementDTO("1", -10);
        List<MovementDTO> expected = new ArrayList<>();
        Link transactionInfo = linkTo(methodOn(GetAccountMovementsBetweenDatesController.class).
                getTransaction(1, new TransactionID("1").toString())).withRel("Possible Transactions Info");
        movementDTOExpected.add(transactionInfo);
        expected.add(movementDTOExpected);

        Ledger ledger = new Ledger(1, id);
        ledger.addPayment(new TransactionID("1"), new AccountID("1"), new MoneyValue(10), new CategoryID("comida"), new TransactionDate(date1));
        ledger.addPayment(new TransactionID("2"), new AccountID("1"), new MoneyValue(20), new CategoryID("comida"), new TransactionDate(date2));
        ledger.addPayment(new TransactionID("3"), new AccountID("2"), new MoneyValue(30), new CategoryID("comida"), new TransactionDate(date1));

        //ACT
        LedgerJPA ledgerJPA = ledgerAssemblerJPA.toJPA(ledger);
        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(java.util.Optional.ofNullable(ledgerJPA));
        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, "1",
                "2021-01-01", "2021-12-31");

        //ASSERT
        List<MovementDTO> actual = (List<MovementDTO>) result.getBody();
        assert actual != null;
        for (MovementDTO movementExpected : expected) {
            for (MovementDTO movementActual : actual) {
                assertEquals(movementExpected.getAmount(), movementActual.getAmount());
                assertEquals(movementExpected.getAccountID(), movementActual.getAccountID());
                break;
            }
        }
        assertEquals(expected, result.getBody());
    }

    @Test
    void GetAccountMovementsBetweenDates_EmptyList() {

        //ARRANGE
        String id = "goncalo@isep.pt";
        LocalDate date1 = LocalDate.of(2021,1,2);
        LocalDate date2 = LocalDate.of(2020,12,31);

        List<MovementDTO> expected = new ArrayList<>();

        Ledger ledger = new Ledger(1, id);
        ledger.addPayment(new TransactionID("1"), new AccountID("2"), new MoneyValue(10), new CategoryID("comida"), new TransactionDate(date1));
        ledger.addPayment(new TransactionID("2"), new AccountID("2"), new MoneyValue(20), new CategoryID("comida"), new TransactionDate(date2));
        ledger.addPayment(new TransactionID("3"), new AccountID("2"), new MoneyValue(30), new CategoryID("comida"), new TransactionDate(date1));

        //ACT
        LedgerJPA ledgerJPA = ledgerAssemblerJPA.toJPA(ledger);
        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(java.util.Optional.ofNullable(ledgerJPA));
        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, "1",
                "2021-01-01", "2021-12-31");

        //ASSERT
        assertEquals(expected, result.getBody());
    }

    @Test
    void GetAccountMovementsBetweenDates_Failure_WrongInfo() {
        //ARRANGE
        String id = "goncalo@isep.pt";
        LocalDate date1 = LocalDate.of(2021,1,2);
        LocalDate date2 = LocalDate.of(2020,12,31);

        String expected = "Wrong info";

        Ledger ledger = new Ledger(1, id);
        ledger.addPayment(new TransactionID("1"), new AccountID("1"), new MoneyValue(10), new CategoryID("comida"), new TransactionDate(date1));
        ledger.addPayment(new TransactionID("2"), new AccountID("1"), new MoneyValue(20), new CategoryID("comida"), new TransactionDate(date2));
        ledger.addPayment(new TransactionID("3"), new AccountID("2"), new MoneyValue(30), new CategoryID("comida"), new TransactionDate(date1));

        //ACT
        LedgerJPA ledgerJPA = ledgerAssemblerJPA.toJPA(ledger);
        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(java.util.Optional.ofNullable(ledgerJPA));
        ResponseEntity<Object> result = ctrl.getAccountMovementsBetweenDates(1, null,
                "2021-01-01", "2021-12-31");

        //ASSERT
        assertEquals(expected, result.getBody());
    }
}

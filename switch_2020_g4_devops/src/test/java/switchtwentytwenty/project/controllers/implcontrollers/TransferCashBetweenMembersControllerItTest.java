package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.implappservices.LedgerService;
import switchtwentytwenty.project.applicationservices.irepositories.*;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.PersonalCashAccount;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.domainservices.ILedgerAccountService;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.TransferDTO;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//@SpringBootTest
public class TransferCashBetweenMembersControllerItTest {
/*
    @Mock
    private ILedgerRepository ledgerRepository;
    @Mock
    private IAccountRepository accountRepository;
    @Mock
    private IPersonRepository personRepository;
    @Mock
    private ICategoryRepository categoryRepository;
    @Mock
    private IFamilyRepository familyRepository;
    @Autowired
    private ILedgerAccountService ledgerAccountService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void transfer(){
        //Arrange
        LedgerService ledgerService = new LedgerService(ledgerRepository, accountRepository, personRepository, categoryRepository, familyRepository, ledgerAccountService, new AssemblerToDTO());

        TransferCashBetweenMembersController ctrl = new TransferCashBetweenMembersController(ledgerService);

        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));

        String otherEmail = "carlos@isep.pt";
        PersonID otherID = new PersonID(new Email(otherEmail));

        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();

        double balance = 20;

        String categoryId = "1";

        LocalDate date = LocalDate.of(2020, 1, 1);

        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(200))
                .setDescription(new Description("cash do ze")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("2")).setAccountType().setBalance(new MoneyValue(0))
                .setDescription(new Description("cash do carlos")).setOwnerID(otherID).build();


        Ledger memberLedger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        TransferDTO in = new TransferDTO(mainEmail, otherEmail, balance, categoryId, date);

        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepository.findByID(otherID)).thenReturn(Optional.of(other));

        Mockito.when(accountRepository.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepository.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepository.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepository.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepository.findByOwnerID(personID)).thenReturn(Optional.of(memberLedger));
        Mockito.when(ledgerRepository.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(categoryRepository.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepository.hasSameFamily(personID,otherID)).thenReturn(true);

        //Act
        ResponseEntity<Object> result = ctrl.transferCash(in);

        //Assert
        assertNotNull(result.getBody());
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(memberAccount.getBalance(),new MoneyValue(180));
        assertEquals(otherAccount.getBalance(),new MoneyValue(20));
    }

    @Test
    void transferFail(){
        //Arrange
        //Arrange
        LedgerService ledgerService = new LedgerService(ledgerRepository, accountRepository, personRepository, categoryRepository, familyRepository, ledgerAccountService, new AssemblerToDTO());

        TransferCashBetweenMembersController ctrl = new TransferCashBetweenMembersController(ledgerService);

        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));

        String otherEmail = "carlos@isep.pt";
        PersonID otherID = new PersonID(new Email(otherEmail));

        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();

        double balance = 20000;

        String categoryId = "1";

        LocalDate date = LocalDate.of(2020, 1, 1);

        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(200))
                .setDescription(new Description("cash do ze")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("2")).setAccountType().setBalance(new MoneyValue(0))
                .setDescription(new Description("cash do carlos")).setOwnerID(otherID).build();

        Ledger memberLedger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        TransferDTO in = new TransferDTO(mainEmail, otherEmail, balance, categoryId, date);

        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepository.findByID(otherID)).thenReturn(Optional.of(other));

        Mockito.when(accountRepository.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepository.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepository.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepository.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepository.findByOwnerID(personID)).thenReturn(Optional.of(memberLedger));
        Mockito.when(ledgerRepository.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(categoryRepository.existsID(new CategoryID(categoryId))).thenReturn(true);

        //Act
        ResponseEntity<Object> result = ctrl.transferCash(in);

        //Assert
        assertEquals("Insufficient funds", result.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        assertEquals(memberAccount.getBalance(),new MoneyValue(200));
        assertEquals(otherAccount.getBalance(),new MoneyValue(0));
    }*/
}

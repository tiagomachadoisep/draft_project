package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;
import switchtwentytwenty.project.repositories.PersonRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

//@SpringBootTest
public class AddEmailControllerITTest {
/*

    @Autowired
    private IFamilyService familyService;
    @Autowired
    private IPersonService personService;
    @Autowired
    private IPersonRepositoryJPA repositoryJPA;
    @Autowired
    private AssemblerToDTO assemblerToDTO;
    @Mock
    private PersonRepository personRepository;


    @BeforeEach
    void cleanDB() {
        repositoryJPA.deleteAll();
    }
/*
    @Test
    @DisplayName("Success - add valid email")
    void addEmailAccountToProfile_Success() {
        //Arrange
        String adminEmail = "ze@isep.pt";

        AddEmailController emailController = new AddEmailController(personService, assemblerToDTO);

        String mainEmail = "sraCastro1970@hotmail.com";
        String secondaryEmail = "sraCastro@hotmail.com";
        EmailDTO emailDTO = new EmailDTO(secondaryEmail);

        FamilyDTO dto = familyService.createAndSaveFamily(new FamilyDTO("Silva",adminEmail),new PersonDTO("0",adminEmail,"ze",0,"rua das coves 22","01-01-1900"));

        PersonDTO personDTO = new PersonDTO(dto.getId(), mainEmail, "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");
        Person person = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setFamilyId(new FamilyID("1"))
                .setAddress(new Address("Rua das Coves, 22")).setBirthDate(new BirthDate("01-03-1990")).build();

        PersonID personID = personService.addMember(personDTO);

        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.of(person));


        EmailDTO outputDTO = assemblerToDTO.toEmailDTO(secondaryEmail);

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(emailDTO, mainEmail);

        //Assert
        assertThat(result.getBody()).isEqualTo(outputDTO);
    }

    @Test
    @DisplayName("Failure - try to add an invalid email")
    void addEmailAccountToProfile_Failure() {
        //Arrange
        String adminEmail = "ze@isep.pt";
        FamilyDTO dto = familyService.createAndSaveFamily(new FamilyDTO("Silva",adminEmail),new PersonDTO("0",adminEmail,"ze",0,"rua das coves 22","01-01-1900"));

        AddEmailController emailController = new AddEmailController(personService, assemblerToDTO);

        String mainEmail = "mariaAlba@hotmail.com";
        String secondaryEmail = "mariahotmail.com";
        EmailDTO emailDTO = new EmailDTO(secondaryEmail);

        PersonDTO personDTO = new PersonDTO(dto.getId(), mainEmail, "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");
        Person person = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setFamilyId(new FamilyID("33"))
                .setAddress(new Address("Rua das Coves, 22")).setBirthDate(new BirthDate("01-03-1990")).build();
        PersonID personID = personService.addMember(personDTO);

        Mockito.when(personRepository.findByID(personID)).thenReturn(Optional.of(person));
        String expected = "Invalid parameters.";

        //Act
        ResponseEntity<Object> result = emailController.addEmailAccountToProfile(emailDTO, mainEmail);

        //Assert
        assertThat(result.getBody()).isEqualTo(expected);
    }
*/

}

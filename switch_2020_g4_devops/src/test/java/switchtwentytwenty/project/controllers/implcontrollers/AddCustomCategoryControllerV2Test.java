package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddCustomCategoryControllerV2Test {

    private ICategoryService categoryServiceMock = Mockito.mock(ICategoryService.class);
    private AssemblerToDTO assemblerToDTO = Mockito.mock(AssemblerToDTO.class);
    private ApplicationContext applicationContext = Mockito.mock(ApplicationContext.class);
    private ICategoryRepositoryHttp categoryRepositoryHttp = Mockito.mock(ICategoryRepositoryHttp.class);
    private AddCustomCategoryControllerV2 controller = new AddCustomCategoryControllerV2(categoryServiceMock,
            assemblerToDTO);

    public List<CategoryDTO> getInternalStandardCategories() {

        CategoryDTO categoryDTO1 = new CategoryDTO("Food", "assdada", null, null);
        CategoryDTO categoryDTO2 = new CategoryDTO("Restaurants", "23q23332", "assdada", null);
        CategoryDTO categoryDTO3 = new CategoryDTO("Children", "parentId", null, null);
        CategoryDTO categoryDTO4 = new CategoryDTO("Television", "jkhg", null, null);

        List<CategoryDTO> internalStandardCategoriesList = new ArrayList<>();
        internalStandardCategoriesList.add(categoryDTO1);
        internalStandardCategoriesList.add(categoryDTO2);
        internalStandardCategoriesList.add(categoryDTO3);
        internalStandardCategoriesList.add(categoryDTO4);

        return internalStandardCategoriesList;

    }

    public List<CategoryDTO> getExternalStandardCategoryList() {

        CategoryDTO dto0 = new CategoryDTO("All", "HTTPGIST/0", "", "");
        CategoryDTO dto1 = new CategoryDTO("Books", "HTTPGIST/1", "", "");
        CategoryDTO dto2 = new CategoryDTO("Hardcover", "HTTPGIST/1", "HTTPGIST/1", "");
        CategoryDTO dto3 = new CategoryDTO("Paperback", "HTTPGIST/2", "HTTPGIST/1", "");
        CategoryDTO dto4 = new CategoryDTO("Electronic", "HTTPGIST/3", "HTTPGIST/1", "");
        CategoryDTO dto5 = new CategoryDTO("Movies", "HTTPGIST/2", "", "");
        CategoryDTO dto6 = new CategoryDTO("DVD", "HTTPGIST/4", "HTTPGIST/2", "");
        CategoryDTO dto7 = new CategoryDTO("BluRay", "HTTPGIST/5", "HTTPGIST/2", "");
        CategoryDTO dto8 = new CategoryDTO("Download", "HTTPGIST/6", "HTTPGIST/2", "");
        CategoryDTO dto9 = new CategoryDTO("Games", "HTTPGIST/3", "", "");
        CategoryDTO dto10 = new CategoryDTO("XBox", "HTTPGIST/7", "HTTPGIST/3", "");
        CategoryDTO dto11 = new CategoryDTO("PC", "HTTPGIST/8", "HTTPGIST/3", "");
        CategoryDTO dto12 = new CategoryDTO("Music", "HTTPGIST/4", "", "");

        List<CategoryDTO> externalStandardCategoriesList = new ArrayList<>();
        externalStandardCategoriesList.add(dto0);
        externalStandardCategoriesList.add(dto1);
        externalStandardCategoriesList.add(dto2);
        externalStandardCategoriesList.add(dto3);
        externalStandardCategoriesList.add(dto4);
        externalStandardCategoriesList.add(dto5);
        externalStandardCategoriesList.add(dto6);
        externalStandardCategoriesList.add(dto7);
        externalStandardCategoriesList.add(dto8);
        externalStandardCategoriesList.add(dto9);
        externalStandardCategoriesList.add(dto10);
        externalStandardCategoriesList.add(dto11);
        externalStandardCategoriesList.add(dto12);

        return externalStandardCategoriesList;

    }

    public List<CategoryDTO> importAllStandardCategories() {

        List<CategoryDTO> allCategories = new ArrayList<>();
        allCategories.addAll(getInternalStandardCategories());
        allCategories.addAll(getExternalStandardCategoryList());

        return allCategories;

    }

    @Test
    void successAddingCustomSubCategoryUnitTest() {

        //ARRANGE
        String categoryName = "School";
        String familyId = "1";
        String parentId = "HTTPGIST/0";
        CategoryDTO input = new CategoryDTO();
        input.setName(categoryName);
        input.setParentId(parentId);

        CategoryDTO assembledCategoryDTO = new CategoryDTO();
        assembledCategoryDTO.setName(categoryName);
        assembledCategoryDTO.setFamilyId(familyId);
        assembledCategoryDTO.setParentId(parentId);

        CategoryDTO output = new CategoryDTO();
        output.setName(categoryName);
        output.setParentId(parentId);
        output.setId("randomId");

        Mockito.when(categoryServiceMock.importAllCategories()).thenReturn(importAllStandardCategories());
        Mockito.when(assemblerToDTO.inputToCustomCategoryDTO(familyId, input)).thenReturn(assembledCategoryDTO);
        Mockito.when(categoryServiceMock.addCustomSubCategory(assembledCategoryDTO)).thenReturn(output);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomSubcategoryV2(familyId, input);

        //ASSERT
        assertEquals(output, realOutput.getBody());

    }

    @Test
    void failAddingCustomSubcategory_ParentCategoryIsNotAvailable() {

        //ARRANGE
        String categoryName = "School";
        String familyId = "1";
        String parentId = "noParentId";
        CategoryDTO input = new CategoryDTO();
        input.setName(categoryName);
        input.setParentId(parentId);

        CategoryDTO assembledCategoryDTO = new CategoryDTO();
        assembledCategoryDTO.setName(categoryName);
        assembledCategoryDTO.setFamilyId(familyId);
        assembledCategoryDTO.setParentId(parentId);

        Mockito.when(categoryServiceMock.importAllCategories()).thenReturn(importAllStandardCategories());

        String errorMessage = "Category creation failed! You must select a parent category.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomSubcategoryV2(familyId, input);

        //ASSERT
        assertEquals(expected, realOutput.getBody());
        assertEquals(400, realOutput.getStatusCodeValue());
        assertNotNull(expected);
    }

    @Test
    void failAddingCustomSubcategory_CategoryAlreadyExists() {

        //ARRANGE
        String categoryName = "School";
        String familyId = "1";
        String parentId = "parentId";
        CategoryDTO input = new CategoryDTO();
        input.setName(categoryName);
        input.setParentId(parentId);

        CategoryDTO assembledCategoryDTO = new CategoryDTO();
        assembledCategoryDTO.setName(categoryName);
        assembledCategoryDTO.setFamilyId(familyId);
        assembledCategoryDTO.setParentId(parentId);

        Mockito.when(categoryServiceMock.importAllCategories()).thenReturn(importAllStandardCategories());
        Mockito.when(assemblerToDTO.inputToCustomCategoryDTO(familyId, input)).thenReturn(assembledCategoryDTO);
        Mockito.when(categoryServiceMock.addCustomSubCategory(assembledCategoryDTO)).thenThrow(DuplicatedValueException.class);

        String errorMessage = "Category creation failed! A category with that name already exists.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomSubcategoryV2(familyId, input);

        //ASSERT
        assertEquals(expected, realOutput.getBody());
        assertEquals(400, realOutput.getStatusCodeValue());
    }

    @Test
    void failAddingCustomSubcategory_InvalidName() {

        //ARRANGE
        String categoryName = "Sc";
        String familyId = "1";
        String parentId = "parentId";
        CategoryDTO input = new CategoryDTO();
        input.setName(categoryName);
        input.setParentId(parentId);

        CategoryDTO assembledCategoryDTO = new CategoryDTO();
        assembledCategoryDTO.setName(categoryName);
        assembledCategoryDTO.setFamilyId(familyId);
        assembledCategoryDTO.setParentId(parentId);

        Mockito.when(categoryServiceMock.importAllCategories()).thenReturn(importAllStandardCategories());
        Mockito.when(assemblerToDTO.inputToCustomCategoryDTO(familyId, input)).thenReturn(assembledCategoryDTO);
        Mockito.when(categoryServiceMock.addCustomSubCategory(assembledCategoryDTO)).thenThrow(InvalidDesignationException.class);

        String errorMessage = "Category creation failed! Invalid category name. Spaces and numbers are not allowed.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        ExceptionDTO expected = controller.createAndAddLinksToExceptionDTO(exceptionDTO, familyId);

        //ACT
        ResponseEntity<Object> realOutput = controller.addCustomSubcategoryV2(familyId, input);

        //ASSERT
        assertEquals(expected, realOutput.getBody());
        assertEquals(400, realOutput.getStatusCodeValue());

    }

    @Test
    void isSelectedParentCategoryAvailable_True() {

        //Arrange
        String categoryName = "School";
        String familyId = "1";
        String parentId = "HTTPGIST/5";
        CategoryDTO input = new CategoryDTO();
        input.setFamilyId(familyId);
        input.setName(categoryName);
        input.setParentId(parentId);

        Mockito.when(categoryServiceMock.importAllCategories()).thenReturn(importAllStandardCategories());

        //Act
        boolean result = controller.isSelectedParentCategoryAvailable(input);

        //Assert
        assertTrue(result);

    }

    @Test
    void isSelectedParentCategoryAvailable_False() {

        //Arrange
        String categoryName = "School";
        String familyId = "1";
        String parentId = "noParentId";
        CategoryDTO input = new CategoryDTO();
        input.setFamilyId(familyId);
        input.setName(categoryName);
        input.setParentId(parentId);

        Mockito.when(categoryServiceMock.getStandardCategoryTree()).thenReturn(getInternalStandardCategories());

        //Act
        boolean result = controller.isSelectedParentCategoryAvailable(input);

        //Assert
        assertFalse(result);

    }

    @Test
    void isSelectedParentCategoryAvailable_False_ListIsEmpty() {

        //Arrange
        String categoryName = "School";
        String familyId = "1";
        String parentId = "noParentId";
        CategoryDTO input = new CategoryDTO();
        input.setFamilyId(familyId);
        input.setName(categoryName);
        input.setParentId(parentId);

        Mockito.when(categoryServiceMock.getStandardCategoryTree()).thenReturn(new ArrayList<>());

        //Act
        boolean result = controller.isSelectedParentCategoryAvailable(input);

        //Assert
        assertFalse(result);

    }

    @Test
    void isSelectedParentCategoryAvailable_False_ExceptionIsCaught() {

        //Arrange
        String categoryName = "School";
        String familyId = "1";
        String parentId = "noParentId";
        CategoryDTO input = new CategoryDTO();
        input.setFamilyId(familyId);
        input.setName(categoryName);
        input.setParentId(parentId);

        Mockito.when(categoryServiceMock.importAllCategories()).thenThrow(IllegalStateException.class);

        //Act
        boolean result = controller.isSelectedParentCategoryAvailable(input);

        //Assert
        assertFalse(result);

    }


}




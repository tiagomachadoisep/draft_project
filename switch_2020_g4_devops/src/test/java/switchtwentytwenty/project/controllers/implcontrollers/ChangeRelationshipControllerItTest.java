package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.applicationservices.implappservices.FamilyRelationshipService;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ChangeRelationshipControllerItTest {

    private IFamilyRepository familyRepository = Mockito.mock(IFamilyRepository.class);
    private IPersonRepository personRepository = Mockito.mock(IPersonRepository.class);

    private AssemblerToDTO assembler = new AssemblerToDTO();

    private IFamilyRelationshipService familyRelationshipService =
            new FamilyRelationshipService(personRepository, familyRepository, assembler);

    private ChangeRelationshipController controller =
            new ChangeRelationshipController(assembler, familyRelationshipService);

    private Family aFamily;
    private String personEmailOne;
    private String personEmailTwo;
    private RelationshipDTO relationDto;
    private Person personOneMarta;
    private Person personTwoSensei;

    @BeforeEach
    void dataSetUp() {

        relationDto = new RelationshipDTO();

        personEmailOne = "marta@isep.ipp.pt";
        personEmailTwo = "sensei@switch.com";
        aFamily = new Family.Builder(new FamilyID("1"))
                .setName(new FamilyName("Martins")).setAdminId(new PersonID(new Email(personEmailOne)))
                .setRegistrationDate(new RegistrationDate("25-04-2004"))
                .build();

        aFamily.createRelationship(new PersonID(new Email(personEmailOne)), new PersonID(new Email(personEmailTwo)),
                new FamilyRelationType("mother"), new FamilyRelationshipID("1231"));

        personOneMarta = new Person.Builder(new PersonID(new Email(personEmailOne))).setName(new PersonName("Marta"))
                .setVat(new VATNumber(123456789)).setAddress(new Address("RuaDaMarta")).setFamilyId(new FamilyID("1")).setBirthDate(new BirthDate("25-03-1996"))
                .build();
        personTwoSensei = new Person.Builder(new PersonID(new Email(personEmailTwo))).setName(new PersonName("Sensei"))
                .setVat(new VATNumber(502011378)).setAddress(new Address("RuaDoSensei")).setFamilyId(new FamilyID("1")).setBirthDate(new BirthDate("13-06-1992"))
                .build();

    }

    @Test
    void updatingAValidRelationship() {
        //ARRANGE
        relationDto.setDesignation("sister");

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(aFamily));

        //ACT
        ResponseEntity<Object> result = controller.changeRelationship("1", "1231", relationDto);

        //ASSERT
        assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
    }

    @Test
    void failTestBecauseRelationDoesNotExist() {
        //ARRANGE
        relationDto.setFamilyId("1");
        relationDto.setRelationshipId("11243432");
        relationDto.setDesignation("sister");

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(aFamily));
        //ACT
        ResponseEntity<Object> result = controller.changeRelationship("1", "11243432", relationDto);

        //ASSERT
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    void failTestBecauseFamilyDoesNotExist() {
        //ARRANGE
        relationDto.setFamilyId("2");
        relationDto.setRelationshipId("1231");
        relationDto.setDesignation("sister");

        Mockito.when(familyRepository.findByID(new FamilyID("2"))).thenReturn(Optional.empty());
        //ACT
        ResponseEntity<Object> result = controller.changeRelationship("2", "1231", relationDto);

        //ASSERT
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    void failTestDueToNullRelationshipType() {
        //ARRANGE
        relationDto.setFamilyId("1");
        relationDto.setRelationshipId("1231");
        relationDto.setDesignation(null);

        Mockito.when(familyRepository.findByID(new FamilyID("1"))).thenReturn(Optional.of(aFamily));
        //ACT
        ResponseEntity<Object> result = controller.changeRelationship("1", "1231", relationDto);

        //ASSERT
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }


}

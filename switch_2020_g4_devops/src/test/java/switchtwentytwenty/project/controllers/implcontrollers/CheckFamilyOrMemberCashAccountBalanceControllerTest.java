package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import static org.junit.jupiter.api.Assertions.assertEquals;


class CheckFamilyOrMemberCashAccountBalanceControllerTest {

    //@Mock
    private IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    //@InjectMocks
    private CheckFamilyOrMemberCashAccountBalanceController controller = new CheckFamilyOrMemberCashAccountBalanceController(iAccountServiceMock);

    @Test
    void checkFamilyOrMemberCashAccountBalance_success() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("jose@gmail.com"));
        AccountDTO accountDTO = new AccountDTO(200);
        Mockito.when(iAccountServiceMock.getFamilyOrMemberCashAccount(ownerId)).thenReturn(accountDTO);
        //Act
        ResponseEntity<Object> result = controller.checkFamilyOrMemberCashAccountBalance("jose@gmail.com");
        //Assert
        assertEquals(accountDTO, result.getBody());
    }

    @Test
    void checkFamilyOrMemberCashAccountBalance_failure() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("jose@gmail.com"));

        Mockito.when(iAccountServiceMock.getFamilyOrMemberCashAccount(ownerId)).thenReturn(null);
        //Act
        ResponseEntity<Object> result = controller.checkFamilyOrMemberCashAccountBalance("jose@gmail.com");
        //Assert
        assertEquals("Invalid parameter", result.getBody());
    }

    @Test
    void checkFamilyOrMemberCashAccountBalance_invalidEmail() {
        //Arrange
        Mockito.when(iAccountServiceMock.getFamilyOrMemberCashAccount(null)).thenThrow(IllegalArgumentException.class);
        //Act
        ResponseEntity<Object> result = controller.checkFamilyOrMemberCashAccountBalance(null);
        //Assert
        assertEquals("Invalid parameter", result.getBody());
    }



}
package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.PersonDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//@SpringBootTest
class AddFamilyControllerTest {

    //@Mock
    private final IFamilyService familyServiceMock = Mockito.mock(IFamilyService.class);
    //@Mock
    private final AssemblerToDTO assemblerToDTO = Mockito.mock(AssemblerToDTO.class);
    //@InjectMocks
    private final AddFamilyController controller = new AddFamilyController(assemblerToDTO, familyServiceMock);

    private String name;
    private String adminEmail;
    private String adminName;
    private int adminVat;
    private String adminBirthDate;
    private FamilyDTO familyDTO;
    private PersonDTO personDTO;
    private FamilyDTO familyDTOin;

    void setUp() {
        name = "Silva";
        adminEmail = "ze@isep.pt";
        adminName = "Ze";
        adminVat = 0;
        adminBirthDate = "01-01-1990";
        familyDTOin = new FamilyDTO(name, adminEmail, adminName,adminVat,adminBirthDate);
        familyDTO = new FamilyDTO(name, adminEmail);
        personDTO = new PersonDTO(adminEmail,adminName,  adminVat,adminBirthDate);
    }


    @Test
    void addFamily() {
        //Arrange
        setUp();

        Mockito.when(familyServiceMock.createAndSaveFamily(familyDTO, personDTO)).thenReturn(familyDTO);
        Mockito.when(assemblerToDTO.toPersonDTO(adminName, adminEmail, adminVat,adminBirthDate)).thenReturn(personDTO);
        Mockito.when(assemblerToDTO.toFamilyDTO(name, adminEmail)).thenReturn(familyDTO);
        //Act
        ResponseEntity<Object> result = controller.addFamily(familyDTOin);
        //Assert
        assertEquals(familyDTO, result.getBody());
    }

    @Test
    void addFamilyTestLinks() {
        //Arrange
        setUp();

        Mockito.when(familyServiceMock.createAndSaveFamily(familyDTO, personDTO)).thenReturn(familyDTO);
        Mockito.when(assemblerToDTO.toPersonDTO(adminName, adminEmail, adminVat,adminBirthDate)).thenReturn(personDTO);
        Mockito.when(assemblerToDTO.toFamilyDTO(name, adminEmail)).thenReturn(familyDTO);

        FamilyDTO expected= new FamilyDTO(name, adminEmail, adminName,adminVat,adminBirthDate);
        Link selfLink = linkTo(methodOn(AddFamilyController.class).getFamily(familyDTO.getId())).withSelfRel();
        Link membersLink = linkTo(AddFamilyController.class).slash("families").slash(familyDTO.getId()).slash("members").withRel("members");
        Link adminLink = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(familyDTO.getAdminEmail())).withRel("admin");
        Link relationsLink = linkTo(methodOn(CreateFamilyRelationshipController.class).getAllRelationships(familyDTO.getId())).withRel("relations");
        Link accountLink = linkTo(CreateFamilyCashAccountController.class).slash("families").slash(familyDTO.getId()).slash("accounts").withRel("account");

        expected.add(selfLink);
        expected.add(membersLink);
        expected.add(adminLink);
        expected.add(relationsLink);
        expected.add(accountLink);
        //Act
        ResponseEntity<Object> result = controller.addFamily(familyDTOin);
        //Assert
        expected.setFamilyId(((FamilyDTO) result.getBody()).getId());
        assertEquals(expected, result.getBody());
    }

    @Test
    void whenTryToAddFamily_FailFamilyDTOoutNull() {
        setUp();
        String expected = "Member already registered";
        Mockito.when(familyServiceMock.createAndSaveFamily(familyDTO, personDTO)).thenReturn(null);
        Mockito.when(assemblerToDTO.toExceptionDto(expected)).thenReturn(new ExceptionDTO(expected));
        Mockito.when(assemblerToDTO.toPersonDTO(adminName, adminEmail, adminVat,adminBirthDate)).thenReturn(personDTO);
        Mockito.when(assemblerToDTO.toFamilyDTO(name, adminEmail)).thenReturn(familyDTO);

        ResponseEntity<Object> result = controller.addFamily(familyDTOin);
        ExceptionDTO resultDto = (ExceptionDTO) result.getBody();

        assertEquals(expected, resultDto.getMessage());

    }

    @Test
    void whenTryToAddFamily_throwInvalidParametersException() {
        setUp();
        String expectedString = "Invalid name.";
        Mockito.when(familyServiceMock.createAndSaveFamily(familyDTO, personDTO)).thenThrow(new IllegalArgumentException(expectedString));
        Mockito.when(assemblerToDTO.toExceptionDto(expectedString)).thenReturn(new ExceptionDTO(expectedString));
        Mockito.when(assemblerToDTO.toPersonDTO(adminName, adminEmail, adminVat,adminBirthDate)).thenReturn(personDTO);
        Mockito.when(assemblerToDTO.toFamilyDTO(name, adminEmail)).thenReturn(familyDTO);

        ExceptionDTO expected = new ExceptionDTO(expectedString);
        Link tryAgainLink = linkTo(methodOn(AddFamilyController.class).addFamily(familyDTOin)).withRel("try again");
        expected.add(tryAgainLink);

        ResponseEntity<Object> result = controller.addFamily(familyDTOin);

        ExceptionDTO body = (ExceptionDTO) result.getBody();

        assertEquals(expected, body);

    }

    @Test
    void whenTryToGetFamilyByID_Success() {
        setUp();
        Mockito.when(familyServiceMock.getFamilyById(adminEmail)).thenReturn(familyDTO);

        ResponseEntity<Object> result = controller.getFamily(adminEmail);

        assertEquals(familyDTO, result.getBody());
        assertNotEquals(familyDTO.getLinks(), Links.of(Collections.emptyList()));
    }

    @Test
    void whenTryToGetFamiliesSuccess() {
        setUp();
        List<FamilyDTO> expected = new ArrayList<>();
        expected.add(familyDTO);

        Mockito.when(familyServiceMock.getFamilies()).thenReturn(expected);

        ResponseEntity<Object> result = controller.getFamilies();

        assertEquals(expected, result.getBody());

    }


}
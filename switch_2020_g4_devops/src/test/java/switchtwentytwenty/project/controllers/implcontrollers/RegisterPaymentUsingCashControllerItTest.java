package switchtwentytwenty.project.controllers.implcontrollers;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import switchtwentytwenty.project.applicationservices.implappservices.LedgerService;
import switchtwentytwenty.project.applicationservices.irepositories.*;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.PersonalCashAccount;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.domainservices.ILedgerAccountService;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.PaymentDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@SpringBootTest
public class RegisterPaymentUsingCashControllerItTest {
/*
    @Mock
    private ILedgerRepository ledgerRepository;
    @Mock
    private IAccountRepository accountRepository;
    @Mock
    private IPersonRepository personRepository;
    @Mock
    private ICategoryRepository categoryRepository;
    @Mock
    private IFamilyRepository familyRepository;
    @Autowired
    private ILedgerAccountService ledgerAccountService;

    //private LedgerService ledgerService=new LedgerService(ledgerRepository,accountRepository,personRepository,categoryRepository,familyRepository,ledgerAccountService,new AssemblerToDTO());

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void registerPayment() {
        LedgerService ledgerService=new LedgerService(ledgerRepository,accountRepository,personRepository,categoryRepository,familyRepository,ledgerAccountService,new AssemblerToDTO());


        AssemblerToDTO mapper = new AssemblerToDTO();
        RegisterPaymentUsingCashController ctrl = new RegisterPaymentUsingCashController(ledgerService, mapper);

        String mainEmail = "ze@isep.pt";
        double amount = 10;
        String categoryId = "1";
        PersonID personID = new PersonID(new Email(mainEmail));

        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash")).setOwnerID(personID).build();


        Ledger ledger = new Ledger(1, mainEmail);

        Mockito.when(accountRepository.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepository.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepository.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepository.existsID(new CategoryID(categoryId))).thenReturn(true);

        PaymentDTO expected = new PaymentDTO(mainEmail, amount, categoryId);

        PaymentDTO result=ctrl.registerPayment(mainEmail,amount,categoryId);
        //Assert
        assertEquals(result.getCategoryId(),expected.getCategoryId());
        assertEquals(result.getBalance(),expected.getBalance());
        assertEquals(result.getMemberId(),expected.getMemberId());
        assertEquals(account.getBalance(),new MoneyValue(90));
    }

    @Test
    void registerPaymentInsufficientBalance() {
        LedgerService ledgerService=new LedgerService(ledgerRepository,accountRepository,personRepository,categoryRepository,familyRepository,ledgerAccountService,new AssemblerToDTO());

        AssemblerToDTO mapper = new AssemblerToDTO();
        RegisterPaymentUsingCashController ctrl = new RegisterPaymentUsingCashController(ledgerService, mapper);

        String mainEmail = "ze@isep.pt";
        double amount = 1000;
        String categoryId = "1";
        PersonID personID = new PersonID(new Email(mainEmail));
        CategoryID categoryID = new CategoryID(categoryId);

        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash")).setOwnerID(personID).build();


        Ledger ledger = new Ledger(1, mainEmail);

        Mockito.when(accountRepository.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepository.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepository.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepository.existsID(categoryID)).thenReturn(true);

        PaymentDTO expected = new PaymentDTO("Not enough balance");

        PaymentDTO result=ctrl.registerPayment(mainEmail,amount,categoryId);
        //Assert
        assertEquals(result,expected);
    }*/
}

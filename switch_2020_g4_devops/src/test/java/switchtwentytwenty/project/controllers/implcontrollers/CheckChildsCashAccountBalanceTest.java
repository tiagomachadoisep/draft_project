package switchtwentytwenty.project.controllers.implcontrollers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckChildsCashAccountBalanceTest {

    private AssemblerToDTO assembler = Mockito.mock(AssemblerToDTO.class);
    private IFamilyRelationshipService relationshipService = Mockito.mock(IFamilyRelationshipService.class);
    private IAccountService accountService = Mockito.mock(IAccountService.class);

    private CheckChildsCashAccountBalance ctrl = new CheckChildsCashAccountBalance(accountService, relationshipService, assembler);


    @Test
    void returnsBalanceFromChild() {
        //Arrange
        String idOfFamily = "170";
        String child = "baby@isep.pt";
        String parent = "parent@sapo.pt";

        RelationshipDTO input = new RelationshipDTO();
        input.setFamilyId(idOfFamily);
        input.setFirstPersonEmail(child);

        RelationshipDTO expectedDto = new RelationshipDTO();
        expectedDto.setFirstPersonEmail(parent);
        expectedDto.setSecondPersonEmail(child);
        expectedDto.setFamilyId(idOfFamily);

        AccountDTO childAccount = new AccountDTO();
        childAccount.setBalance(200);

        Mockito.when(assembler.inputSearchForRelationshipDTO(idOfFamily, parent, child)).thenReturn(expectedDto);
        Mockito.when(relationshipService.checkIfIsChild(expectedDto)).thenReturn(true);
        Mockito.when(accountService.checkChildsCashAccountBalance(child)).thenReturn(childAccount);

        //Act
        ResponseEntity<Object> actual = ctrl.checkChildsCashAccount(parent, input);

        //Assert
        assertEquals(childAccount, actual.getBody());

    }

    @Test
    void memberIsNotTheParent() {
        //Arrange
        String idOfFamily = "170";
        String child = "baby@isep.pt";
        String notParent = "notParent@sapo.pt";

        RelationshipDTO input = new RelationshipDTO();
        input.setFamilyId(idOfFamily);
        input.setFirstPersonEmail(child);

        RelationshipDTO expectedDto = new RelationshipDTO();
        expectedDto.setFirstPersonEmail(notParent);
        expectedDto.setSecondPersonEmail(child);
        expectedDto.setFamilyId(idOfFamily);

        String errorExpected = "The member is not the parent";

        Mockito.when(assembler.inputSearchForRelationshipDTO(idOfFamily, notParent, child)).thenReturn(expectedDto);
        Mockito.when(relationshipService.checkIfIsChild(expectedDto)).thenReturn(false);

        //Act
        ResponseEntity<Object> actual = ctrl.checkChildsCashAccount(notParent, input);

        //Assert
        assertEquals(errorExpected, actual.getBody());

    }

    @Test
    void childDoesNotHaveCashAccount() {
        //Arrange
        String idOfFamily = "170";
        String child = "baby@isep.pt";
        String parent = "parent@sapo.pt";

        RelationshipDTO input = new RelationshipDTO();
        input.setFamilyId(idOfFamily);
        input.setFirstPersonEmail(child);

        RelationshipDTO expectedDto = new RelationshipDTO();
        expectedDto.setFirstPersonEmail(parent);
        expectedDto.setSecondPersonEmail(child);
        expectedDto.setFamilyId(idOfFamily);

        Mockito.when(assembler.inputSearchForRelationshipDTO(idOfFamily, parent, child)).thenReturn(expectedDto);
        Mockito.when(relationshipService.checkIfIsChild(expectedDto)).thenReturn(true);
        Mockito.when(accountService.checkChildsCashAccountBalance(child)).thenThrow(NoSuchElementException.class);


        //Act
        ResponseEntity<Object> actual = ctrl.checkChildsCashAccount(parent, input);

        //Assert
        assertEquals(HttpStatus.valueOf(400), actual.getStatusCode());

    }

}
package switchtwentytwenty.project.domain.aggregates.person;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.PersonDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @Test
    @DisplayName("Failure - The email is empty")
    void addEmailToProfile_emptyEmail() {
        String email = "";
        PersonDTO personDTO = new PersonDTO("Jocimara", "jocimara@hotmail.com", 123456789);
        personDTO.setFamilyId("fam1");
        Person person = new Person.Builder(new PersonID(new Email("jocimara@hotmail.com"))).setName(new PersonName("Jocimara"))
                .setFamilyId(new FamilyID("fam1")).setVat(new VATNumber(123456789)).build();
        assertThrows(IllegalArgumentException.class, () -> person.addEmailToProfile(email));
    }

    @Test
    @DisplayName("Failure - The email is equals the main")
    void addEmailToProfile_equals() {
        String email = "jocimara@hotmail.com";
        PersonDTO personDTO = new PersonDTO("Jocimara", "jocimara@hotmail.com", 123456789);
        personDTO.setFamilyId("fam1");
        Person person = new Person.Builder(new PersonID(new Email("jocimara@hotmail.com"))).setName(new PersonName("Jocimara"))
                .setFamilyId(new FamilyID("fam1")).setVat(new VATNumber(123456789)).build();
        boolean result = person.addEmailToProfile(email);
        assertFalse(result);
    }

    @Test
    void hasSameFamilyFalseTest() {

        Person member = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).setFamilyId(new FamilyID("50")).build();
        Person otherMember = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setFamilyId(new FamilyID("2323")).build();
        Person otherMemberTwo = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setFamilyId(new FamilyID("50")).build();

        boolean result = member.hasSameFamily(otherMember);
        boolean resultTwo = member.hasSameFamily(otherMemberTwo);

        assertFalse(result);
        assertTrue(resultTwo);
    }


    @Test
    void useEqualsWithSameObject() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();
        //Person personTwo = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();

        assertEquals(person, person);
        assertEquals(person.hashCode(), person.hashCode());
    }

    @Test
    void useEqualsWithSameObjectDifferentName() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();
        Person personTwo = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();

        assertEquals(person, personTwo);
        assertEquals(person.hashCode(), person.hashCode());
    }

    @Test
    void useEqualsWithTwoObjectsOfDifferentClasses() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();
        Family family = new Family.Builder(new FamilyID("1")).build();

        assertNotEquals(person, family);
    }

    @Test
    void useEqualsWithSameClassObject() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();
        Person personTwo = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).build();

        assertNotEquals(person, personTwo);
        assertNotEquals(person.hashCode(), personTwo.hashCode());
    }


    @Test
    void useEqualsWithNullClassObject() {
        Person person = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).build();
        Person personTwo = null;

        assertNotEquals(person, personTwo);
    }

    @Test
    void isID() {
        Person person = new Person.Builder(new PersonID(new Email("mariaJoaquina@hotmail.com"))).build();
        boolean result = person.isID(new PersonID(new Email("mariaJoaquina@hotmail.com")));
        assertTrue(result);
    }

    @Test
    void isID_False() {
        Person person = new Person.Builder(new PersonID(new Email("mariaJoaquina@hotmail.com"))).setPhoneNumbers(new ArrayList<>()).build();
        boolean result = person.isID(new PersonID(new Email("joaquim@hotmail.com")));
        assertFalse(result);
    }

    @Test
    void testingAttributePhoneNumber() {

        PhoneNumber number = new PhoneNumber("919964069");
        List<PhoneNumber> phones = new ArrayList<>();
        phones.add(number);

        Person person = new Person.Builder(new PersonID(new Email("mariaJoaquina@hotmail.com"))).setPhoneNumbers(phones)
                .build();

        assertThrows(IllegalArgumentException.class, () -> person.addEmailToProfile("email@@isep.pt"));
    }

    @Test
    void createPersonWithAllAttributes(){
        PhoneNumber tlm = new PhoneNumber("935854555");
        List<PhoneNumber> phoneNumbers = new ArrayList();
        phoneNumbers.add(tlm);

        Person person = new Person.Builder(new PersonID(new Email("abc@abc.pt")))
                .setAddress(new Address("Rua escura"))
                .setBirthDate(new BirthDate("21-02-1990"))
                .setFamilyId(new FamilyID("1"))
                .setName(new PersonName("Luis"))
                .setOtherEmails(new ArrayList<>())
                .setPhoneNumbers(phoneNumbers)
                .setVat(new VATNumber(123456789))
                .setRoleAsUser()
                .build();

        String email = ("luis@hotmail.com");
        person.addEmailToProfile(email);

        assertNotNull(person);
        assertEquals(phoneNumbers, person.getPhoneNumbers());
        assertEquals(ERole.USER,person.getRole());
    }

    @Test
    void createPersonAdminWithAllAttributes(){
        PhoneNumber tlm = new PhoneNumber("935854555");
        List<PhoneNumber> phoneNumbers = new ArrayList();
        phoneNumbers.add(tlm);

        Person person = new Person.Builder(new PersonID(new Email("abc@abc.pt")))
                .setAddress(new Address("Rua escura"))
                .setBirthDate(new BirthDate("21-02-1990"))
                .setFamilyId(new FamilyID("1"))
                .setName(new PersonName("Luis"))
                .setOtherEmails(new ArrayList<>())
                .setPhoneNumbers(phoneNumbers)
                .setVat(new VATNumber(123456789))
                .setRoleAsFamilyAdministrator().build();

        String email = ("luis@hotmail.com");
        person.addEmailToProfile(email);

        assertNotNull(person);
        assertEquals(phoneNumbers, person.getPhoneNumbers());
        assertEquals(ERole.ADMIN,person.getRole());
    }

    @Test
    void createPersonSysAdminWithAllAttributes(){
        PhoneNumber tlm = new PhoneNumber("935854555");
        List<PhoneNumber> phoneNumbers = new ArrayList();
        phoneNumbers.add(tlm);

        Person person = new Person.Builder(new PersonID(new Email("abc@abc.pt")))
                .setAddress(new Address("Rua escura"))
                .setBirthDate(new BirthDate("21-02-1990"))
                .setFamilyId(new FamilyID("1"))
                .setName(new PersonName("Luis"))
                .setOtherEmails(new ArrayList<>())
                .setPhoneNumbers(phoneNumbers)
                .setVat(new VATNumber(123456789))
                .setRoleAsSystemManager().build();

        String email = ("luis@hotmail.com");
        person.addEmailToProfile(email);

        assertNotNull(person);
        assertEquals(phoneNumbers, person.getPhoneNumbers());
        assertEquals(ERole.SYSADMIN,person.getRole());
    }
}


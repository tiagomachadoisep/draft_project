package switchtwentytwenty.project.domain.aggregates.account;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class ElectronicAccountTest {

    @Test
    void getID() {

        //Arrange
        AccountID accountID = new AccountID("123");
        ElectronicAccount electronicAccount = new CreditCardAccount.Builder(accountID).build();

        //Act
        AccountID result = electronicAccount.getID();

        //Assert
        assertEquals(accountID, result);
        assertNotNull(accountID);
        assertNotNull(result);

    }

    @Test
    void getID_IDIsNull() {

        //Arrange
        ElectronicAccount electronicAccount = new CreditCardAccount.Builder(null).build();

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> {
            electronicAccount.getID();
        });

    }

    @Test
    void equals_ComparingWithNullObject() {

        //Arrange
        ElectronicAccount electronicAccount = new BankAccount.Builder(new AccountID("123")).build();

        //Act
        boolean result = electronicAccount.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_ComparingWithObjectOfDifferentClass() {

        //Arrange
        ElectronicAccount electronicAccount = new BankAccount.Builder(new AccountID("123")).build();
        Person person = new Person.Builder(new PersonID(new Email("filipa@gmail.com"))).build();

        //Act
        boolean result = electronicAccount.equals(person);

        //Assert
        assertFalse(result);

    }


}
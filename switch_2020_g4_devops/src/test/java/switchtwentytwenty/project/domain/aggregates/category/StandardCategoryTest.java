package switchtwentytwenty.project.domain.aggregates.category;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class StandardCategoryTest {

    @Test
    void checkIfItIsAStandardCategory() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        //ACT
        boolean result = category.isStandard();
        //ASSERT
        assertTrue(result);
    }

    @Test
    void checkIfTheStandardCategoryHasSameNameWhichDoesntHave() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryName nameToCheck = new CategoryName("Netflix");

        //ACT
        boolean result = category.hasSameName(nameToCheck);
        //ASSERT
        assertFalse(result);
    }

    @Test
    void checkIfTheStandardCategoryHasSameNameWhichHas() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryName nameToCheck = new CategoryName("judo");

        //ACT
        boolean result = category.hasSameName(nameToCheck);
        //ASSERT
        assertTrue(result);
    }

    @Test
    void checkIfTheStandardCategoryHasSameIDWhichHas() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryID idToCheck = new CategoryID("1");

        //ACT
        boolean result = category.isID(idToCheck);
        //ASSERT
        assertTrue(result);
    }

    @Test
    void checkIfTheStandardCategoryHasSameIDWhichDoesntHave() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryID idToCheck = new CategoryID("1.2");

        //ACT
        boolean result = category.isID(idToCheck);
        //ASSERT
        assertFalse(result);
    }

    @Test
    void checkIfTheCategoryCanBeUsedByFamily() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();
        CategoryID idExpected = new CategoryID("1");
        //ACT
        boolean result = category.canBeUsedByFamily(new FamilyID("1"));
        //ASSERT
        assertTrue(result);
        assertEquals(idExpected, category.getID());
    }

    @Test
    void checkIfTwoCategoriesAreTheSame() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        //ACT
        boolean result = category.isAtRoot();
        //ASSERT
        assertTrue(result);
        assertEquals(category, category);
        assertNotEquals(null, category);
        assertEquals(category.hashCode(), category.hashCode());
    }

    @Test
    void checkIfACategoryIsAFamilyID() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();
        FamilyID id = new FamilyID("1");
        //ASSERT
        assertNotEquals(category, id);
    }

    @Test
    void checkTwoCategoriesAreNotTheSame() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("2"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        StandardCategory categoryToCompare = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(new CategoryID("2"))
                .build();

        //ACT
        boolean result = categoryToCompare.isAtRoot();
        //ASSERT
        assertNotEquals(category, categoryToCompare);
        assertFalse(result);
        assertNotEquals(category.hashCode(), categoryToCompare.hashCode());
    }
    @Test
    void checkTwoCategoriesAreNotTheSamePat2() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("32"))
                .setName(new CategoryName("Merlin"))
                .setParent(new CategoryID("2"))
                .build();

        StandardCategory categoryToCompare = new StandardCategory.Builder(new CategoryID("33"))
                .setName(new CategoryName("Judo"))
                .setParent(new CategoryID("2"))
                .build();

        //ACT
        boolean result = categoryToCompare.isAtRoot();
        //ASSERT
        assertNotEquals(category, categoryToCompare);
    }
    @Test
    void checkTwoCategoriesAreNotTheSameWithDifferentCategoryID() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("3"))
                .setName(new CategoryName("Judo"))
                .setParent(new CategoryID("2"))
                .build();

        StandardCategory categoryToCompare = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(new CategoryID("2"))
                .build();

        //ACT
        boolean result = categoryToCompare.isAtRoot();
        //ASSERT
        assertNotEquals(category, categoryToCompare);
    }

    @Test
    void checkSetAndGetterOfParentId() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryID parentIdExpected = new CategoryID("2");

        //ACT
        category.setParentId(parentIdExpected);

        //ASSERT
        assertEquals(parentIdExpected, category.getParentId());

    }
    @Test
    void checkSetAndGetterOfId() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryID idExpected = new CategoryID("2");

        //ACT
        category.setId(idExpected);

        //ASSERT
        assertEquals(idExpected, category.getID());

    }

    @Test
    void checkSetAndGetterOfCategoryName() {

        //ARRANGE
        StandardCategory category = new StandardCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setParent(null)
                .build();

        CategoryName nameExpected = new CategoryName("Hotel");

        //ACT
        category.setName(nameExpected);

        //ASSERT
        assertEquals(nameExpected, category.getName());

    }
}
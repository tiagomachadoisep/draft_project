package switchtwentytwenty.project.domain.aggregates.ledger;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TransferTest {

    @Test
    void transfer() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);

        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);

        assertEquals(description, transfer.getDescription());
        assertEquals(transactionID, transfer.getID());
        assertEquals(transactionID, transfer.getID());
        assertTrue(transfer.isID(transactionID));
        assertEquals(new Movement(accountID, amount.toNegative()), transfer.getMovementDebit());
        assertEquals(new Movement(destinationID, amount), transfer.getMovementCredit());
        assertTrue(transfer.isTransfer());
        assertFalse(transfer.isPayment());
        assertEquals(date, transfer.getTransactionDate());
    }

    @Test
    void equals() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);

        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);

        Transfer transfer1 = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);

        Family family = new Family.Builder(new FamilyID("1")).build();

        assertEquals(transfer,transfer);
        assertNotEquals(transfer,family);
        assertEquals(transfer1, transfer);
        assertEquals(transfer1.hashCode(), transfer.hashCode());
    }

    @Test
    void equalsNot() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);

        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);

        Transfer transfer1 = new Transfer(new TransactionID("12345"), accountID, destinationID, amount, categoryID, date, description);

        assertNotEquals(transfer1, transfer);
        assertNotEquals(transfer1.hashCode(), transfer.hashCode());
    }

    @Test
    void differentObjects() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);

        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);

        FamilyID family = new FamilyID("1lf");

        assertNotEquals(family, transfer);
    }

    @Test
    void transactionType() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);

        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);
        ETransactionType type = transfer.getTransactionType();
        assertNotNull(type);
    }

    @Test
    void transactionCategory() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);


        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);
        CategoryID actual = transfer.getCategoryId();
        assertEquals(categoryID, actual);
    }

    @Test
    void transactionAmount() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);


        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);
        double actual = transfer.getAmount();
        assertEquals(10, actual);
    }

    @Test
    void transactionWhenIdIsDifferent() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);


        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);
        boolean result = transfer.isID(new TransactionID("2"));

        assertFalse(result);
    }

    @Test
    void transactionWhenIdIsTheSame() {
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID destinationID = new AccountID("2");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);


        Transfer transfer = new Transfer(transactionID, accountID, destinationID, amount, categoryID, date, description);
        boolean result = transfer.isID(new TransactionID("1"));

        assertTrue(result);
    }

}
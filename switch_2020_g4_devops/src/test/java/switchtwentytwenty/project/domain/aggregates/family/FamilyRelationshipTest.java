package switchtwentytwenty.project.domain.aggregates.family;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class FamilyRelationshipTest {


    @Test
    void familyRelationshipConstructor_validCase_FirstCase() {

        String stringPersonOneEmail = "ole1@gmail.com";
        String stringPersonTwoEmail = "ole2@gmail.com";
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("2i1j423");
        PersonID personOneID = new PersonID(new Email(stringPersonOneEmail));
        PersonID personTwoID = new PersonID(new Email(stringPersonTwoEmail));
        String designation = "father";
        FamilyRelationType relationTypeFather = new FamilyRelationType(designation);

        FamilyRelationship result = new FamilyRelationship(personOneID, personTwoID, relationTypeFather, familyRelationshipID);

        assertNotNull(result);
    }

    @Test
    void familyRelationshipConstructor_invalidCase_nullDesignation() {

        String stringPersonOneEmail = "ole1@gmail.com";
        String stringPersonTwoEmail = "ole2@gmail.com";
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("2i1j423");
        PersonID personOneID = new PersonID(new Email(stringPersonOneEmail));
        PersonID personTwoID = new PersonID(new Email(stringPersonTwoEmail));

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationship result = new FamilyRelationship(personOneID, personTwoID, null, familyRelationshipID);
        });
    }

    @Test
    void familyRelationshipConstructor_invalidCase_nullPersonOneEmail() {

        String stringPersonTwoEmail = "ole2@gmail.com";
        PersonID personOneID = null;
        PersonID personTwoID = new PersonID(new Email(stringPersonTwoEmail));
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("2i1j423");
        String designation = "father";
        FamilyRelationType relationTypeFather = new FamilyRelationType(designation);

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationship result = new FamilyRelationship(personOneID, personTwoID, relationTypeFather, familyRelationshipID);
        });
    }

    @Test
    void familyRelationshipConstructor_invalidCase_nullPersonTwoEmail() {

        String stringPersonOneEmail = "ole1@gmail.com";
        PersonID personOneID = new PersonID(new Email(stringPersonOneEmail));
        PersonID personTwoID = null;
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("2i1j423");
        String designation = "father";
        FamilyRelationType relationTypeFather = new FamilyRelationType(designation);

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationship result = new FamilyRelationship(personOneID, personTwoID, relationTypeFather, familyRelationshipID);
        });
    }

    @Test
    void familyRelationshipConstructor_invalidCase_nullPersonsEmail() {

        PersonID personOneID = null;
        PersonID personTwoID = null;
        String designation = "father";
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("2i1j423");
        FamilyRelationType relationTypeFather = new FamilyRelationType(designation);

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationship result = new FamilyRelationship(personOneID, personTwoID, relationTypeFather, familyRelationshipID);
        });
    }

    @Test
    void familyRelationshipConstructor_invalidCase_nullFamilyRelationshipID() {

        String stringPersonOneEmail = "ole1@gmail.com";
        String stringPersonTwoEmail = "ole2@gmail.com";
        FamilyRelationshipID familyRelationshipID = null;
        PersonID personOneID = new PersonID(new Email(stringPersonOneEmail));
        PersonID personTwoID = new PersonID(new Email(stringPersonTwoEmail));
        String designation = "father";
        FamilyRelationType relationTypeFather = new FamilyRelationType(designation);

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationship result = new FamilyRelationship(personOneID, personTwoID, relationTypeFather, familyRelationshipID);
        });
    }

    @Test
    void useOfSettersOnEmptyConstructor_successCase() {

        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("id123");
        FamilyRelationType familyRelationType = new FamilyRelationType("sister");
        PersonID personTwoID = new PersonID(new Email("two@gmail.com"));
        PersonID personOneID = new PersonID(new Email("one@gmail.com"));

        FamilyRelationship familyRelationship = new FamilyRelationship();

        familyRelationship.setFamilyRelationshipID(familyRelationshipID);
        familyRelationship.setRelationType(familyRelationType);
        familyRelationship.setPersonTwoID(personTwoID);
        familyRelationship.setPersonOneID(personOneID);


        assertEquals(familyRelationship.getRelationType(), familyRelationType);
        assertEquals(familyRelationship.getID(), familyRelationshipID);
        assertEquals(familyRelationship.getPersonOneID(), personOneID);
        assertEquals(familyRelationship.getPersonTwoID(), personTwoID);
    }

    @Test
    void useOfSettersOnEmptyConstructor_failCase_invalidPersonOneID() {

        PersonID personOneID = null;

        FamilyRelationship familyRelationship = new FamilyRelationship();

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyRelationship.setPersonOneID(personOneID);
        });
    }

    @Test
    void useOfSettersOnEmptyConstructor_failCase_invalidPersonTwoID() {

        PersonID personTwoID = null;

        FamilyRelationship familyRelationship = new FamilyRelationship();

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyRelationship.setPersonTwoID(personTwoID);
        });
    }

    @Test
    void useOfSettersOnEmptyConstructor_failCase_invalidFamilyRelationType() {

        FamilyRelationType familyRelationType = null;

        FamilyRelationship familyRelationship = new FamilyRelationship();

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyRelationship.setRelationType(familyRelationType);
        });
    }

    @Test
    void useOfSettersOnEmptyConstructor_failCase_invalidRelationshipID() {

        FamilyRelationshipID familyRelationshipID = null;

        FamilyRelationship familyRelationship = new FamilyRelationship();

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyRelationship.setFamilyRelationshipID(familyRelationshipID);
        });
    }

    @Test
    void equalsAndHashcode() {

        FamilyRelationship familyRelationship =
                new FamilyRelationship(new PersonID(new Email("um@gmail.com")),
                        new PersonID(new Email("dois@gmail.com")),
                        new FamilyRelationType("brother"),
                        new FamilyRelationshipID("id1"));

        FamilyRelationship equalRelationship =
                new FamilyRelationship(new PersonID(new Email("um@gmail.com")),
                        new PersonID(new Email("dois@gmail.com")),
                        new FamilyRelationType("brother"),
                        new FamilyRelationshipID("id1"));

        FamilyRelationship invertedPersons =
                new FamilyRelationship(new PersonID(new Email("dois@gmail.com")),
                        new PersonID(new Email("um@gmail.com")),
                        new FamilyRelationType("brother"),
                        new FamilyRelationshipID("id1"));

        FamilyRelationship differentPersonOne =
                new FamilyRelationship(new PersonID(new Email("different@gmail.com")),
                        new PersonID(new Email("dois@gmail.com")),
                        new FamilyRelationType("brother"),
                        new FamilyRelationshipID("id1"));

        FamilyRelationship differentPersonTwo =
                new FamilyRelationship(new PersonID(new Email("um@gmail.com")),
                        new PersonID(new Email("different@gmail.com")),
                        new FamilyRelationType("brother"),
                        new FamilyRelationshipID("id1"));

        FamilyRelationship differentPersons =
                new FamilyRelationship(new PersonID(new Email("differentUm@gmail.com")),
                        new PersonID(new Email("differentDois@gmail.com")),
                        new FamilyRelationType("brother"),
                        new FamilyRelationshipID("id1"));

        FamilyRelationship nullRelationship = null;

        FamilyRelationshipID differentClass = new FamilyRelationshipID("anID");

        assertEquals(familyRelationship, familyRelationship);
        assertEquals(familyRelationship.hashCode(), familyRelationship.hashCode());

        assertEquals(familyRelationship, equalRelationship);
        assertEquals(familyRelationship.hashCode(), equalRelationship.hashCode());

        assertEquals(familyRelationship, invertedPersons);
        assertEquals(familyRelationship.hashCode(), invertedPersons.hashCode());

        assertNotEquals(familyRelationship, differentPersonOne);
        assertNotEquals(familyRelationship.hashCode(), differentPersonOne.hashCode());

        assertNotEquals(familyRelationship, differentPersonTwo);
        assertNotEquals(familyRelationship.hashCode(), differentPersonTwo.hashCode());

        assertNotEquals(familyRelationship, differentPersons);
        assertNotEquals(familyRelationship.hashCode(), differentPersons.hashCode());


        assertNotEquals(invertedPersons, differentPersonOne);
        assertNotEquals(invertedPersons.hashCode(), differentPersonOne.hashCode());

        assertNotEquals(invertedPersons, differentPersonTwo);
        assertNotEquals(invertedPersons.hashCode(), differentPersonTwo.hashCode());

        assertNotEquals(invertedPersons, differentPersons);
        assertNotEquals(invertedPersons.hashCode(), differentPersons.hashCode());

        assertNotEquals(familyRelationship, nullRelationship);

        assertNotEquals(familyRelationship, differentClass);
        assertNotEquals(familyRelationship.hashCode(), differentClass.hashCode());

    }

    @Test
    void whenThePersonIsParent_Father() {
        //ARRANGE
        PersonID fatherId = new PersonID(new Email("father@sapo.pt"));
        PersonID childId = new PersonID(new Email("child@sapo.pt"));
        FamilyRelationType designation = new FamilyRelationType("father");
        FamilyRelationshipID id = new FamilyRelationshipID("007");
        FamilyRelationship father = new FamilyRelationship(fatherId, childId, designation, id);

        //ACT
        boolean result = father.isParent();

        //ASSERT
        assertTrue(result);
    }

    @Test
    void whenThePersonIsParent_Mother() {
        //ARRANGE
        PersonID motherId = new PersonID(new Email("mother@sapo.pt"));
        PersonID childId = new PersonID(new Email("child@sapo.pt"));
        FamilyRelationType designation = new FamilyRelationType("mother");
        FamilyRelationshipID id = new FamilyRelationshipID("007");
        FamilyRelationship mother = new FamilyRelationship(motherId, childId, designation, id);

        //ACT
        boolean result = mother.isParent();

        //ASSERT
        assertTrue(result);
    }

    @Test
    void whenThePersonIsNotParent_Brother() {
        //ARRANGE
        PersonID brotherId = new PersonID(new Email("brother@sapo.pt"));
        PersonID childId = new PersonID(new Email("child@sapo.pt"));
        FamilyRelationType designation = new FamilyRelationType("brother");
        FamilyRelationshipID id = new FamilyRelationshipID("007");
        FamilyRelationship brother = new FamilyRelationship(brotherId, childId, designation, id);

        //ACT
        boolean result = brother.isParent();

        //ASSERT
        assertFalse(result);
    }

    @Test
    void areTheSamePersons() {

        //ARRANGE
        PersonID brotherId = new PersonID(new Email("brother@sapo.pt"));
        PersonID childId = new PersonID(new Email("child@sapo.pt"));
        FamilyRelationType designation = new FamilyRelationType("brother");
        FamilyRelationshipID id = new FamilyRelationshipID("007");
        FamilyRelationship brother = new FamilyRelationship(brotherId, childId, designation, id);

        //ACT
        boolean result = brother.areTheSamePersons(brotherId, childId);

        //ASSERT
        assertTrue(result);

    }

    @Test
    void areNotTheSamePersons() {

        //ARRANGE
        PersonID brotherId = new PersonID(new Email("brother@sapo.pt"));
        PersonID childId = new PersonID(new Email("child@sapo.pt"));
        FamilyRelationType designation = new FamilyRelationType("brother");
        FamilyRelationshipID id = new FamilyRelationshipID("007");
        FamilyRelationship brother = new FamilyRelationship(brotherId, childId, designation, id);

        //ACT
        boolean result = brother.areTheSamePersons(childId, brotherId);

        //ASSERT
        assertFalse(result);

    }

}
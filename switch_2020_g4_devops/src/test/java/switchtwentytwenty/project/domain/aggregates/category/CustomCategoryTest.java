package switchtwentytwenty.project.domain.aggregates.category;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class CustomCategoryTest {

    @Test
    void checkIfItIsAStandardCategory() {

        //ARRANGE
        CustomCategory category = new CustomCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setFamily(new FamilyID("1"))
                .setParent(null)
                .build();

        //ACT
        boolean result = category.isStandard();
        //ASSERT
        assertFalse(result);
    }

    @Test
    void checkIfItCanBeUsedByFamily() {

        //ARRANGE
        CustomCategory category = new CustomCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setFamily(new FamilyID("1"))
                .setParent(null)
                .build();

        //ACT
        boolean result = category.canBeUsedByFamily(new FamilyID("1"));
        //ASSERT
        assertTrue(result);
    }

    @Test
    void cannotBeUsedByFamily() {

        //ARRANGE
        CustomCategory category = new CustomCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setFamily(new FamilyID("1"))
                .setParent(null)
                .build();

        //ACT
        boolean result = category.canBeUsedByFamily(new FamilyID("2"));
        //ASSERT
        assertFalse(result);
    }

    @Test
    void getsFamilyId() {

        //ARRANGE
        FamilyID expected = new FamilyID("1");
        CustomCategory category = new CustomCategory.Builder(new CategoryID("1"))
                .setName(new CategoryName("Judo"))
                .setFamily(expected)
                .setParent(null)
                .build();

        //ACT
        FamilyID result = category.getFamilyId();
        //ASSERT
        assertEquals(result, expected);
    }



}
package switchtwentytwenty.project.domain.aggregates.invoice;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.VATNumber;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.InvoiceID;

import static org.junit.jupiter.api.Assertions.*;

class InvoiceTest {

    @Test
    void validInvoice() {
        Invoice invoice = new Invoice(new InvoiceID(123), new Description("something"), new VATNumber(232598568), new CategoryID("1"));
        assertNotNull(invoice);
    }

    @Test
    void getInvoiceId() {


        Invoice invoice = new Invoice(new InvoiceID(123), new Description("something"), new VATNumber(232598568), new CategoryID("1"));
        InvoiceID actual = invoice.getID();

        assertNotNull(actual);
    }

    @Test
    void isInvoiceId() {

        InvoiceID expected = new InvoiceID(123);
        Invoice invoice = new Invoice(new InvoiceID(123), new Description("something"), new VATNumber(232598568), new CategoryID("1"));

        boolean result = invoice.isID(expected);
        assertTrue(result);
    }

    @Test
    void isNotInvoiceId() {

        InvoiceID expected = new InvoiceID(456);
        Invoice invoice = new Invoice(new InvoiceID(123), new Description("something"), new VATNumber(232598568), new CategoryID("1"));

        boolean result = invoice.isID(expected);
        assertFalse(result);
    }

}
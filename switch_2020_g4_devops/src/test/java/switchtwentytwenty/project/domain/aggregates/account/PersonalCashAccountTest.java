package switchtwentytwenty.project.domain.aggregates.account;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class PersonalCashAccountTest {

    @Test
    void PersonalCashAccountConstructor() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        PersonalCashAccount expected = new PersonalCashAccount.Builder(new AccountID("2")).setOwnerID(personID)
                .setDescription(new Description("My cash account")).setBalance(new MoneyValue(25.50)).setAccountType().build();

        //Act
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("2")).setOwnerID(personID)
                .setDescription(new Description("My cash account")).setBalance(new MoneyValue(25.50)).setAccountType().build();

        //Assert
        assertNotNull(personalCashAccount);
        assertEquals(expected, personalCashAccount);

    }

    @Test
    void getID() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("3")).setOwnerID(personID)
                .setDescription(new Description("My cash account")).setBalance(new MoneyValue(12.50)).setAccountType().build();

        ID expected = new AccountID("3");

        //Act
        ID ID = personalCashAccount.getID();

        //Assert
        assertEquals(expected, ID);

    }

    @Test
    void getAccountType_CashAccount_Equals() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("1")).setOwnerID(personID)
                .setDescription(new Description("My cash account")).setBalance(new MoneyValue(80)).setAccountType().build();

        EAccountType expected = EAccountType.PERSONAL_CASH;

        //Act
        EAccountType accountType = personalCashAccount.getAccountType();

        //Assert
        assertEquals(expected, accountType);

    }

    @Test
    void getAccountType_CashAccount_NotEquals() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("1")).setOwnerID(personID)
                .setDescription(new Description("My cash account")).setBalance(new MoneyValue(80)).setAccountType().build();

        EAccountType expected = EAccountType.CREDIT_CARD;

        //Act
        EAccountType accountType = personalCashAccount.getAccountType();

        //Assert
        assertNotEquals(expected, accountType);

    }

    @Test
    void isCashAccount_True() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("4")).setOwnerID(personID)
                .setDescription(new Description("My cash account")).setBalance(new MoneyValue(15)).setAccountType().build();

        //Act
        boolean result = personalCashAccount.isCashAccount();

        //Assert
        assertTrue(result);

    }

    @Test
    void isAccountOwner_True() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("6")).setOwnerID(personID)
                .setDescription(new Description("My personal cash " +
                        "account")).setBalance(new MoneyValue(46)).setAccountType().build();

        ID otherPersonID = new PersonID(new Email("filipa@gmail.com"));

        //Act
        boolean result = personalCashAccount.isAccountOwner(otherPersonID);

        //Assert
        assertTrue(result);

    }

    @Test
    void isAccountOwner_False() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("6")).setOwnerID(personID)
                .setDescription(new Description("My personal cash " +
                        "account")).setBalance(new MoneyValue(46)).setAccountType().build();

        ID otherPersonID = new PersonID(new Email("sara@gmail.com"));

        //Act
        boolean result = personalCashAccount.isAccountOwner(otherPersonID);

        //Assert
        assertFalse(result);

    }

    @Test
    void isAccountIDEqualToProvidedID_ReturnsTrue() {

        //Arrange
        AccountID accountID = new AccountID("sak2");
        AccountID providedAccountID = new AccountID("sak2");
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(accountID).build();

        //Act
        boolean result = personalCashAccount.isID(providedAccountID);

        //Assert
        assertTrue(result);

    }

    @Test
    void isAccountIDEqualToProvidedID_ReturnsFalse() {

        //Arrange
        AccountID accountID = new AccountID("sak2");
        AccountID providedAccountID = new AccountID("sak2234");
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(accountID).build();

        //Act
        boolean result = personalCashAccount.isID(providedAccountID);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_AllParametersAreEqual() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf3");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

        AccountID accountID_2 = new AccountID("asdf3");
        Description description_2 = new Description("Account 1");
        MoneyValue balance_2 = new MoneyValue(23);
        PersonID personID_2 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_2 =
                new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

        //Act
        boolean result = personalCashAccount_1.equals(personalCashAccount_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void equals_AccountIDIsDifferent() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf31");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

        AccountID accountID_2 = new AccountID("asdf3");
        Description description_2 = new Description("Account 1");
        MoneyValue balance_2 = new MoneyValue(23);
        PersonID personID_2 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_2 =
                new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

        //Act
        boolean result = personalCashAccount_1.equals(personalCashAccount_2);

        //Assert
        assertFalse(result);

    }

    /*  @Test
      void equals_DescriptionIsDifferent() {

          //Arrange
          AccountID accountID_1 = new AccountID("asdf3");
          Description description_1 = new Description("Account 1");
          MoneyValue balance_1 = new MoneyValue(23);
          PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

          PersonalCashAccount personalCashAccount_1 =
                  new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

          AccountID accountID_2 = new AccountID("asdf3");
          Description description_2 = new Description("Account 2");
          MoneyValue balance_2 = new MoneyValue(23);
          PersonID personID_2 = new PersonID(new Email("filipa@gmail.com"));

          PersonalCashAccount personalCashAccount_2 =
                  new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

          //Act
          boolean result = personalCashAccount_1.equals(personalCashAccount_2);

          //Assert
          assertFalse(result);

      }

     @Test
      void equals_BalanceIsDifferent() {

          //Arrange
          AccountID accountID_1 = new AccountID("asdf3");
          Description description_1 = new Description("Account 1");
          MoneyValue balance_1 = new MoneyValue(23);
          PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

          PersonalCashAccount personalCashAccount_1 =
                  new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

          AccountID accountID_2 = new AccountID("asdf3");
          Description description_2 = new Description("Account 1");
          MoneyValue balance_2 = new MoneyValue(234);
          PersonID personID_2 = new PersonID(new Email("filipa@gmail.com"));

          PersonalCashAccount personalCashAccount_2 =
                  new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

          //Act
          boolean result = personalCashAccount_1.equals(personalCashAccount_2);

          //Assert
          assertFalse(result);

      }

      @Test
      void equals_PersonIDIsDifferent() {

          //Arrange
          AccountID accountID_1 = new AccountID("asdf3");
          Description description_1 = new Description("Account 1");
          MoneyValue balance_1 = new MoneyValue(23);
          PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

          PersonalCashAccount personalCashAccount_1 =
                  new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

          AccountID accountID_2 = new AccountID("asdf3");
          Description description_2 = new Description("Account 1");
          MoneyValue balance_2 = new MoneyValue(23);
          PersonID personID_2 = new PersonID(new Email("filipe@gmail.com"));

          PersonalCashAccount personalCashAccount_2 =
                  new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

          //Act
          boolean result = personalCashAccount_1.equals(personalCashAccount_2);

          //Assert
          assertFalse(result);

      }
  */
    @Test
    void equals_NullObject() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf3");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

        //Act
        boolean result = personalCashAccount_1.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_TheSameObject() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf3");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();


        //Act
        boolean result = personalCashAccount_1.equals(personalCashAccount_1);

        //Assert
        assertTrue(result);

    }

    @Test
    void equals_ObjectOfDifferentClass() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf3");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

        Family family = new Family.Builder(new FamilyID("12")).build();

        //Act
        boolean result = personalCashAccount_1.equals(family);

        //Assert
        assertFalse(result);

    }

    @Test
    void hashCode_ReturnsTrue() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf3");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

        AccountID accountID_2 = new AccountID("asdf3");
        Description description_2 = new Description("Account 1");
        MoneyValue balance_2 = new MoneyValue(23);
        PersonID personID_2 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_2 =
                new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

        //Act
        int result_1 = personalCashAccount_1.hashCode();
        int result_2 = personalCashAccount_2.hashCode();

        //Assert
        assertEquals(result_1, result_2);

    }

    @Test
    void hashCode_ReturnsFalse() {

        //Arrange
        AccountID accountID_1 = new AccountID("asdf3");
        Description description_1 = new Description("Account 1");
        MoneyValue balance_1 = new MoneyValue(23);
        PersonID personID_1 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_1 =
                new PersonalCashAccount.Builder(accountID_1).setAccountType().setBalance(balance_1).setDescription(description_1).setOwnerID(personID_1).build();

        AccountID accountID_2 = new AccountID("asdf34");
        Description description_2 = new Description("Account 1");
        MoneyValue balance_2 = new MoneyValue(23);
        PersonID personID_2 = new PersonID(new Email("filipa@gmail.com"));

        PersonalCashAccount personalCashAccount_2 =
                new PersonalCashAccount.Builder(accountID_2).setAccountType().setBalance(balance_2).setDescription(description_2).setOwnerID(personID_2).build();

        //Act
        int result_1 = personalCashAccount_1.hashCode();
        int result_2 = personalCashAccount_2.hashCode();

        //Assert
        assertNotEquals(result_1, result_2);

    }


}
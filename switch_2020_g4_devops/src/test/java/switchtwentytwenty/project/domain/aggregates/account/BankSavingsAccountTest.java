package switchtwentytwenty.project.domain.aggregates.account;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

public class BankSavingsAccountTest {

    @Test
    void getAccountTypeSuccess() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).setAccountType().build();
        EAccountType expected = EAccountType.BANK_SAVINGS;

        EAccountType accountType = bankSavingsAccount.getAccountType();

        assertEquals(expected, accountType);
    }

    @Test
    void getAccountTypeFailure() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).setAccountType().build();
        EAccountType expected = EAccountType.CREDIT_CARD;

        EAccountType accountType = bankSavingsAccount.getAccountType();

        assertNotEquals(expected, accountType);
    }

    @Test
    void hasAccountOwnerSuccess() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();
        Email email = new Email("example@gmail.com");
        PersonID personID = new PersonID(email);

        boolean result = bankSavingsAccount.isAccountOwner(personID);

        assertTrue(result);
    }

    @Test
    void hasAccountOwnerFailure() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();
        Email email = new Email("example2@gmail.com");
        PersonID personID = new PersonID(email);

        boolean result = bankSavingsAccount.isAccountOwner(personID);

        assertFalse(result);
    }

    @Test
    void getIDSuccess() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();
        ID expected = new AccountID("1");

        ID ID = bankSavingsAccount.getID();

        assertEquals(expected, ID);
    }

    @Test
    void getIDFailure() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();
        ID expected = new AccountID("2");

        ID ID = bankSavingsAccount.getID();

        assertNotEquals(expected, ID);
    }

    @Test
    void isIDSuccess() {
        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();
        boolean result = bankSavingsAccount.isID(new AccountID("1"));

        assertTrue(result);
    }

    @Test
    void isIDFailure() {

        BankSavingsAccount bankSavingsAccount = new BankSavingsAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();
        boolean result = bankSavingsAccount.isID(new AccountID("2"));

        assertFalse(result);
    }
}


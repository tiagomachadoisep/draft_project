package switchtwentytwenty.project.domain.aggregates.account;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

public class BankAccountTest {

    @Test
    void getAccountTypeSuccess() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).setAccountType().build();

        EAccountType expected = EAccountType.BANK;

        EAccountType accountType = bankAccount.getAccountType();

        assertEquals(expected, accountType);
    }

    @Test
    void getAccountTypeFailure() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).setAccountType().build();

        EAccountType expected = EAccountType.PERSONAL_CASH;

        EAccountType accountType = bankAccount.getAccountType();

        assertNotEquals(expected, accountType);
    }

    @Test
    void hasAccountOwnerSuccess() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();

        Email email = new Email("example@gmail.com");
        PersonID personID = new PersonID(email);

        boolean result = bankAccount.isAccountOwner(personID);

        assertTrue(result);
    }

    @Test
    void hasAccountOwnerFailure() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();

        Email email = new Email("example2@gmail.com");
        PersonID personID = new PersonID(email);

        boolean result = bankAccount.isAccountOwner(personID);

        assertFalse(result);
    }

    @Test
    void getIDSuccess() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();

        ID expected = new AccountID("1");

        ID ID = bankAccount.getID();

        assertEquals(expected, ID);
    }

    @Test
    void getIDFailure() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();

        ID expected = new AccountID("2");

        ID ID = bankAccount.getID();

        assertNotEquals(expected, ID);
    }

    @Test
    void isIDSuccess() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();

        boolean result = bankAccount.isID(new AccountID("1"));

        assertTrue(result);
    }

    @Test
    void isIDFailure() {
        BankAccount bankAccount = new BankAccount.Builder(new AccountID("1")).
                setOwnerID(new PersonID(new Email("example@gmail.com"))).build();


        boolean result = bankAccount.isID(new AccountID("2"));

        assertFalse(result);
    }
}

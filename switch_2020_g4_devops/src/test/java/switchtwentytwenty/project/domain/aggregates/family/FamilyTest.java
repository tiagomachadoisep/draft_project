package switchtwentytwenty.project.domain.aggregates.family;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void createRelationship_validCase() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        FamilyRelationType father = new FamilyRelationType("father");
        int expectedListSize = 1;
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("2i1j423");
        FamilyRelationship relationshipExpected = new FamilyRelationship(personOneID, personTwoID, father, familyRelationshipID);

        family.createRelationship(personOneID, personTwoID, father, familyRelationshipID);
        int resultListSize = family.getFamilyRelationships().size();
        FamilyRelationship relationshipResult = family.getFamilyRelationships().get(0);

        assertEquals(expectedListSize, resultListSize);
        assertEquals(relationshipExpected, relationshipResult);

    }

    @Test
    void createRelationship_validCase_creatingTwoDifferentRelationships() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        PersonID personThreeID = new PersonID(new Email("ole3@gmail.com"));
        FamilyRelationType father = new FamilyRelationType("father");
        FamilyRelationType son = new FamilyRelationType("son");
        int expectedListSize = 2;
        FamilyRelationshipID firstRelationID = new FamilyRelationshipID("2i1j423");
        FamilyRelationshipID secondRelationID = new FamilyRelationshipID("dfjewrew");

        FamilyRelationship firstRelationshipExpected = new FamilyRelationship(personOneID, personTwoID, father, firstRelationID);
        FamilyRelationship secondRelationshipExpected = new FamilyRelationship(personOneID, personThreeID, son, secondRelationID);
        List<FamilyRelationship> expectedRelationshipList = new ArrayList<>();
        expectedRelationshipList.add(firstRelationshipExpected);
        expectedRelationshipList.add(secondRelationshipExpected);

        family.createRelationship(personOneID, personTwoID, father, firstRelationID);
        family.createRelationship(personOneID, personThreeID, son, secondRelationID);
        int resultListSize = family.getFamilyRelationships().size();
        List<FamilyRelationship> resultRelationshipList = new ArrayList<>(family.getFamilyRelationships());

        assertEquals(expectedListSize, resultListSize);
        assertEquals(expectedRelationshipList, resultRelationshipList);

    }

    @Test
    void createRelationship_invalidCase_symmetricRelationship() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        FamilyRelationType father = new FamilyRelationType("father");
        FamilyRelationType son = new FamilyRelationType("son");
        int expectedListSize = 1;
        FamilyRelationshipID firstRelationID = new FamilyRelationshipID("2i1j423");
        FamilyRelationshipID secondRelationID = new FamilyRelationshipID("dfjewrew");

        FamilyRelationship relationshipExpected = new FamilyRelationship(personOneID, personTwoID, father, firstRelationID);

        family.createRelationship(personOneID, personTwoID, father, firstRelationID);

        int resultListSize = family.getFamilyRelationships().size();
        FamilyRelationship relationshipResult = family.getFamilyRelationships().get(0);

        Assertions.assertThrows(DuplicatedValueException.class, () -> {
            family.createRelationship(personTwoID, personOneID, son, secondRelationID);
        });
        assertEquals(expectedListSize, resultListSize);
        assertEquals(relationshipExpected, relationshipResult);

    }

    @Test
    void createRelationship_invalidCase_relationshipBetweenPersonsAlreadyExists() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        FamilyRelationType father = new FamilyRelationType("father");
        int expectedListSize = 1;
        FamilyRelationshipID firstRelationID = new FamilyRelationshipID("2i1j423");
        FamilyRelationshipID secondRelationID = new FamilyRelationshipID("dfjewrew");
        FamilyRelationship relationshipExpected = new FamilyRelationship(personOneID, personTwoID, father, firstRelationID);

        family.createRelationship(personOneID, personTwoID, father, firstRelationID);

        int resultListSize = family.getFamilyRelationships().size();
        FamilyRelationship relationshipResult = family.getFamilyRelationships().get(0);

        Assertions.assertThrows(DuplicatedValueException.class, () -> {
            family.createRelationship(personOneID, personTwoID, father, secondRelationID);
        });
        assertEquals(expectedListSize, resultListSize);
        assertEquals(relationshipExpected, relationshipResult);

    }

    @Test
    void createRelationship_invalidCase_nullPersonOne() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = null;
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        FamilyRelationType father = new FamilyRelationType("father");
        int expectedListSize = 0;
        FamilyRelationshipID firstRelationID = new FamilyRelationshipID("2i1j423");

        Assertions.assertThrows(NullArgumentException.class, () -> {
            family.createRelationship(personOneID, personTwoID, father, firstRelationID);
        });

        int resultListSize = family.getFamilyRelationships().size();
        assertEquals(expectedListSize, resultListSize);

    }

    @Test
    void createRelationship_invalidCase_nullPersonTwo() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = null;
        FamilyRelationType father = new FamilyRelationType("father");
        int expectedListSize = 0;
        FamilyRelationshipID firstRelationID = new FamilyRelationshipID("2i1j423");

        Assertions.assertThrows(NullArgumentException.class, () -> {
            family.createRelationship(personOneID, personTwoID, father, firstRelationID);
        });

        int resultListSize = family.getFamilyRelationships().size();
        assertEquals(expectedListSize, resultListSize);

    }

    @Test
    void createRelationship_invalidCase_nullRelationType() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        FamilyRelationType father = null;
        int expectedListSize = 0;
        FamilyRelationshipID firstRelationID = new FamilyRelationshipID("2i1j423");

        Assertions.assertThrows(NullArgumentException.class, () -> {
            family.createRelationship(personOneID, personTwoID, father, firstRelationID);
        });

        int resultListSize = family.getFamilyRelationships().size();
        assertEquals(expectedListSize, resultListSize);

    }

    @Test
    void createRelationship_invalidCase_nullFamilyRelationshipID() {

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("12/11/2019"))
                .build();
        PersonID personOneID = new PersonID(new Email("ole1@gmail.com"));
        PersonID personTwoID = new PersonID(new Email("ole2@gmail.com"));
        FamilyRelationType father = new FamilyRelationType("father");
        int expectedListSize = 0;
        FamilyRelationshipID firstRelationID = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            family.createRelationship(personOneID, personTwoID, father, firstRelationID);
        });

        int resultListSize = family.getFamilyRelationships().size();
        assertEquals(expectedListSize, resultListSize);

    }

    @Test
    void FamilyConstructor() {

        //Arrange
        String name = "Sousa";
        String id = "5";
        String adminMainEmail = "sandra_sousa@gmail.com";
        String regDate = "12/02/2021";

        //Act
        Family family = new Family.Builder(new FamilyID(id)).setName(new FamilyName(name))
                .setAdminId(new PersonID(new Email(adminMainEmail))).setRegistrationDate(new RegistrationDate(regDate)).build();

        //Assert
        assertNotNull(family);

    }

    @Test
    void getID() {

        //Arrange
        Family family = new Family.Builder(new FamilyID("2")).setName(new FamilyName("Barros"))
                .setAdminId(new PersonID(new Email("ana.barros@gmail.com"))).setRegistrationDate(new RegistrationDate("16/03/2021"))
                .build();
        ID expected = new FamilyID("2");

        //Act
        ID result = family.getID();

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void familyChangeRelationship() {
        //ARRANGE
        String designation = "sister";
        String newDesignation = "mother";
        PersonID personOne = new PersonID(new Email("marta@gmail.com"));
        PersonID personTwo = new PersonID(new Email("bia@sapo.pt"));
        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Martins"))
                .setAdminId(personOne).build();

        family.createRelationship(personOne, personTwo, new FamilyRelationType(designation),
                new FamilyRelationshipID("1231"));

        List<FamilyRelationship> expected = new ArrayList<>();
        expected.add(new FamilyRelationship(personOne, personTwo, new FamilyRelationType(newDesignation),
                new FamilyRelationshipID("1231")));

        //ACT
        family.changeRelationship(new FamilyRelationshipID("1231"), new FamilyRelationType(newDesignation));
        List<FamilyRelationship> actual = family.getFamilyRelationships();
        FamilyRelationship resultRelationship = actual.get(0);
        String resultDesignation = resultRelationship.getRelationType().toString();

        //ASSERT
        assertEquals(newDesignation, resultDesignation);

    }

    @Test
    void familyChangeRelationshipFailedRelationNotFound() {
        //ARRANGE
        PersonID personOne= new PersonID(new Email("marta@gmail.com"));
        PersonID personTwo = new PersonID(new Email("bia@sapo.pt"));
        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Martins"))
                .setAdminId(personOne).build();

        family.createRelationship(personOne, personTwo, new FamilyRelationType("sister"),
                new FamilyRelationshipID("1231"));


        //ACT + ASSERT
        Assertions.assertThrows(NoSuchElementException.class, () -> {
            family.changeRelationship(new FamilyRelationshipID("1234444"), new FamilyRelationType("mother"));
            ;
        });

    }

    @Test
    void familyChangeRelationshipFailed_NoRelationshipExists() {
        //ARRANGE
        PersonID personOne = new PersonID(new Email("marta@gmail.com"));
        PersonID personTwo = new PersonID(new Email("bia@sapo.pt"));
        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Martins"))
                .setAdminId(personOne).build();


        //ACT + ASSERT
        Assertions.assertThrows(NoSuchElementException.class, () -> {
            family.changeRelationship(new FamilyRelationshipID("1234444"), new FamilyRelationType("mother"));
            ;
        });

    }

    @Test
    void isID() {
        FamilyID familyID = new FamilyID("123");
        FamilyID otherFamilyID = new FamilyID("656");
        Family family = new Family.Builder(familyID).build();

        boolean sameIdResult = family.isID(familyID);
        boolean differentIdResult = family.isID(otherFamilyID);

        assertTrue(sameIdResult);
        assertFalse(differentIdResult);
    }

    @Test
    void findRelationshipById_successCase() {
        Family family = new Family.Builder(new FamilyID("1")).build();
        FamilyRelationship expected = new FamilyRelationship(
                new PersonID(new Email("one@gmail.com")),
                new PersonID(new Email("two@gmail.com")),
                new FamilyRelationType("sister"),
                new FamilyRelationshipID("id123")
        );

        family.createRelationship(
                new PersonID(new Email("three@gmail.com")),
                new PersonID(new Email("four@gmail.com")),
                new FamilyRelationType("brother"),
                new FamilyRelationshipID("id456")
        );

        family.createRelationship(
                new PersonID(new Email("one@gmail.com")),
                new PersonID(new Email("two@gmail.com")),
                new FamilyRelationType("sister"),
                new FamilyRelationshipID("id123")
        );

        Optional<FamilyRelationship> optionalFamilyRelationship = family.findRelationshipById(
                new FamilyRelationshipID("id123")
        );

        FamilyRelationship result = optionalFamilyRelationship.get();

        assertEquals(expected, result);

    }

    @Test
    void findRelationshipById_failCase_RelationshipNotFound_OneDifferentRelationshipPresent() {
        Family family = new Family.Builder(new FamilyID("1")).build();

        family.createRelationship(
                new PersonID(new Email("three@gmail.com")),
                new PersonID(new Email("four@gmail.com")),
                new FamilyRelationType("brother"),
                new FamilyRelationshipID("id456")
        );

        Optional<FamilyRelationship> optionalFamilyRelationship = family.findRelationshipById(
                new FamilyRelationshipID("id123")
        );

        assertFalse(optionalFamilyRelationship.isPresent());

    }

    @Test
    void findRelationshipById_failCase_RelationshipNotFound_NoRelationshipsPresent() {
        Family family = new Family.Builder(new FamilyID("1")).build();

        Optional<FamilyRelationship> optionalFamilyRelationship = family.findRelationshipById(
                new FamilyRelationshipID("id123")
        );

        assertFalse(optionalFamilyRelationship.isPresent());

    }

    @Test
    void findRelationshipById_failCase_nullRelationIdPassed() {
        Family family = new Family.Builder(new FamilyID("1")).build();

        Assertions.assertThrows(NullArgumentException.class, () -> {
            Optional<FamilyRelationship> optionalFamilyRelationship = family.findRelationshipById(
                    null
            );
        });

    }


    @Test
    void equalsAndHashcode() {
        //Used custom date because hashcode uses it. Is it necessary?
        RegistrationDate regDate = new RegistrationDate("25-03-2003");

        Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos")).
                setRegistrationDate(regDate).build();

        Family otherFamilySameAttributes = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos")).
                setRegistrationDate(regDate).build();

        Family otherFamilyDifferentName = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Dias")).
                setRegistrationDate(regDate).build();

        Family otherFamilyDifferentId = new Family.Builder(new FamilyID("2")).setName(new FamilyName("Santos")).
                setRegistrationDate(regDate).build();

        Family nullFamily = null;

        PersonID differentClass = new PersonID(new Email("admin@gmail.com"));


        assertEquals(family, family);
        assertEquals(family.hashCode(), family.hashCode());

        assertEquals(family, otherFamilySameAttributes);
        assertEquals(family.hashCode(), otherFamilySameAttributes.hashCode());

        assertNotEquals(family, differentClass);
        assertNotEquals(family.hashCode(), differentClass.hashCode());

        assertNotEquals(family, otherFamilyDifferentId);
        assertNotEquals(family.hashCode(), otherFamilyDifferentId.hashCode());

        assertNotEquals(family, nullFamily);

        assertEquals(family, otherFamilyDifferentName);
        assertNotEquals(family.hashCode(), otherFamilyDifferentName.hashCode());

    }

    @Test
    void isChild_True() {
        //Arrange
        FamilyID id = new FamilyID("200");
        FamilyName name = new FamilyName("Leite");
        PersonID admin = new PersonID(new Email("wiii@hotmail.com"));
        PersonID adminChild = new PersonID(new Email("baby@live.pt"));
        FamilyRelationType child = new FamilyRelationType("father");
        Family family = new Family.Builder(id).setName(name)
                .setRegistrationDate(new RegistrationDate("12/01/2021"))
                .setAdminId(admin).build();

        family.createRelationship(admin, adminChild, child, new FamilyRelationshipID("00"));

        //Act
        boolean result = family.isChild(admin, adminChild);

        //Assert
        assertTrue(result);
    }

    @Test
    void isChild_False() {
        //Arrange
        FamilyID id = new FamilyID("200");
        FamilyName name = new FamilyName("Leite");
        PersonID admin = new PersonID(new Email("wiii@hotmail.com"));
        PersonID adminChild = new PersonID(new Email("baby@live.pt"));
        FamilyRelationType cousin = new FamilyRelationType("cousin");
        Family family = new Family.Builder(id).setName(name)
                .setRegistrationDate(new RegistrationDate("12/01/2021"))
                .setAdminId(admin).build();

        family.createRelationship(admin, adminChild, cousin, new FamilyRelationshipID("00"));

        //Act
        boolean result = family.isChild(admin, adminChild);

        //Assert
        assertFalse(result);
    }

    @Test
    void theRelationshipBetweenTwoMembersDoesNotExist() {
        //Arrange
        FamilyID id = new FamilyID("200");
        FamilyName name = new FamilyName("Leite");
        PersonID admin = new PersonID(new Email("wiii@hotmail.com"));
        PersonID adminChild = new PersonID(new Email("baby@live.pt"));
        PersonID outsider = new PersonID(new Email("intruder@gmail.com"));
        FamilyRelationType child = new FamilyRelationType("mother");
        Family family = new Family.Builder(id).setName(name)
                .setRegistrationDate(new RegistrationDate("12/01/2021"))
                .setAdminId(admin).build();

        family.createRelationship(admin, adminChild, child, new FamilyRelationshipID("00"));

        //Act + Assert
        assertThrows(NoSuchElementException.class, () -> family.isChild(admin, outsider));

    }


}
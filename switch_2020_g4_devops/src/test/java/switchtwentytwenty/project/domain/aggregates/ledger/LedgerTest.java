package switchtwentytwenty.project.domain.aggregates.ledger;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class LedgerTest {

    @Test
    void addPayment(){
        //arrange
        AccountID accountID = new AccountID("1");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID= new CategoryID("1");

        Ledger ledger=new Ledger(1,"ze@isep.pt");

        //act
        ledger.addPayment(new TransactionID("test"),accountID,amount,categoryID);
        //assert
        assertTrue(ledger.getTransactionCount()==1);
    }


    @Test
    void getMovements(){
        //Arrange
        Ledger ledger=new Ledger(1,"ze@isep.pt");
        ledger.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),new MoneyValue(10),new CategoryID("1"));

        List<Movement> expected = new ArrayList<>();
        expected.add(new Movement(new AccountID("1"),new MoneyValue(-10)));
        expected.add(new Movement(new AccountID("2"),new MoneyValue(10)));


        List<Movement> movementList = ledger.getMovements(ledger.getTransactions());

        assertEquals(expected,movementList);
    }

    @Test
    void getTransactionsBetweenDates(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        ledger.addPayment(new TransactionID("1"),new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("2021-05-05"));
        ledger.addPayment(new TransactionID("2"),new AccountID("2"),new MoneyValue(100),new CategoryID("1"),new TransactionDate("2020-12-01"));

        List<Transactionable> list = ledger.getTransactionsBetweenDates(ledger.getTransactions(),new TransactionDate("2021-01-01"),new TransactionDate("2022-01-01"));

        List<Transactionable> expected = new ArrayList<>();
        expected.add(new Payment(new TransactionID("1"),new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new Description("other"),new TransactionDate("2021-05-05")));

        assertEquals(expected,list);


    }


    @Test
    void getLedgerType() {
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        ELedgerType ledgerType = ELedgerType.PERSONAL;

        ELedgerType result = ledger.getLedgerType();

        assertEquals(ledgerType,result);
    }

    @Test
    void isID() {
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        LedgerID ledgerID = new LedgerID(1);

        assertTrue(ledger.isID(ledgerID));
    }

    @Test
    void isNotID() {
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        LedgerID ledgerID = new LedgerID(2);

        assertFalse(ledger.isID(ledgerID));
    }

    @Test
    void equalsSameLedger(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        assertEquals(ledger,ledger);
    }

    @Test
    void equalDiffObject(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        assertNotEquals(new MoneyValue(10),ledger);
    }

    @Test
    void differentLedger(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        Ledger ledgerTwo = new Ledger(2,"ze2@isep.pt");

        assertNotEquals(ledger, ledgerTwo);
    }


    @Test
    void differentClass(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        Family family = new Family.Builder(new FamilyID("1")).build();

        assertNotEquals(ledger, family);
    }

    @Test
    void hashCodeTest(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        int hash = ledger.hashCode();

        assertEquals(63,hash);
    }

    @Test
    void findPossibleTransactionByMovement_whenListIsEmpty(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        AccountID accountID = new AccountID("23");
        MoneyValue amount = new MoneyValue(200);

        Assertions.assertThrows(NullPointerException.class, () -> {
            ledger.findPossibleTransactionsByMovement(accountID,amount);
        });
    }

    @Test
    void findPossibleTransactions(){
        Ledger ledger = new Ledger(1,"ze@isep.pt");
        AccountID accountID = new AccountID("23");
        MoneyValue amount = new MoneyValue(200);
        ledger.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),new MoneyValue(10),new CategoryID("1"));

        List<Transactionable> expected = new ArrayList<>();
        expected.add(new Transfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),new MoneyValue(10),new CategoryID("1")));

        List<Transactionable> result = ledger.findPossibleTransactionsByMovement(new AccountID("2"),new MoneyValue(10));

        assertEquals(expected,result);
    }
}
package switchtwentytwenty.project.domain.aggregates.account;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class CreditCardAccountTest {

    @Test
    void checkIfCreditCardAccountID_isGivenID() {
        AccountID accountID = new AccountID("20");
        CreditCardAccount cc = new CreditCardAccount.Builder(accountID).build();

        boolean result = cc.isID(accountID);

        assertTrue(result);
    }

    @Test
    void getAccountIDTest(){
        PersonID personID = new PersonID(new Email("luis@isep.pt"));
        CreditCardAccount cc = new CreditCardAccount.Builder(new AccountID("1")).setOwnerID(personID).build();

        AccountID expectedAccountID = new AccountID("1");
        PersonID expectedPersonID = new PersonID(new Email("luis@isep.pt"));

        AccountID accountIDResult = cc.getAccountId();
        PersonID personIDResult = cc.getPersonId();

        assertEquals(expectedAccountID,accountIDResult);
        assertEquals(expectedPersonID,personIDResult);



    }


    @Test
    void checkIfCreditCardAccountID_isNotGivenID() {
        AccountID accountID = new AccountID("20");
        CreditCardAccount cc = new CreditCardAccount.Builder(accountID).build();

        boolean result = cc.isID(new AccountID("14"));

        assertFalse(result);
    }

    @Test
    void testToIsCashAccount() {
        AccountID accountID = new AccountID("20");
        CreditCardAccount cc = new CreditCardAccount.Builder(accountID).build();

        assertFalse(cc.isCashAccount());
    }

    @Test
    void testToIsID() {
        AccountID accountID = new AccountID("20");
        CreditCardAccount cc = new CreditCardAccount.Builder(accountID).build();

        assertTrue(cc.isID(accountID));
    }

    @Test
    void testEquals() {
        CreditCardAccount creditCardAccount = new CreditCardAccount.Builder(new AccountID("1")).build();
        CreditCardAccount creditCardAccountTwo = new CreditCardAccount.Builder(new AccountID("1")).build();
        Family family = new Family.Builder(new FamilyID("32")).build();
        CreditCardAccount creditCardAccountDifferent = new CreditCardAccount.Builder(new AccountID("4564")).build();


        assertEquals(creditCardAccount, creditCardAccount);
        assertEquals(creditCardAccount.hashCode(), creditCardAccount.hashCode());
        assertEquals(creditCardAccount, creditCardAccountTwo);
        assertNotEquals(creditCardAccount, family);
        assertNotEquals(creditCardAccount, creditCardAccountDifferent);
        assertNotEquals(creditCardAccount.hashCode(), creditCardAccountDifferent.hashCode());
        assertNotEquals(null, creditCardAccount);


    }


}
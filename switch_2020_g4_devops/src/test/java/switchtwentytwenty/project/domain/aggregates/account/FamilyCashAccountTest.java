package switchtwentytwenty.project.domain.aggregates.account;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class FamilyCashAccountTest {

    @Test
    void familyCashAccountConstructor() {
        //arrange
        String description = "Family cash account";
        AccountID accountID = new AccountID("12344");
        PersonID familyId = new PersonID(new Email("frodo@isep.pt"));
        double initialBalance = 100;

        FamilyCashAccount expectedCashAccount = new FamilyCashAccount.Builder(accountID).setAccountType().setOwnerID(familyId)
                .setDescription(new Description(description)).setBalance(new MoneyValue(initialBalance)).build();
        FamilyCashAccount secondExpectedCashAccount = new FamilyCashAccount.Builder(new AccountID("2")).setAccountType().setOwnerID(familyId)
                .setDescription(new Description(description)).setBalance(new MoneyValue(initialBalance)).build();

        MoneyValue expectedBalance = new MoneyValue(100);
        AccountID expectedID = new AccountID("12344");
        EAccountType expectedAccountType = EAccountType.FAMILY_CASH;


        //act
        FamilyCashAccount resultCashAccount = new FamilyCashAccount.Builder(accountID).setAccountType().setOwnerID(familyId)
                .setDescription(new Description(description)).setBalance(new MoneyValue(initialBalance)).build();
        //assert
        assertNotNull(resultCashAccount);
        assertEquals(expectedCashAccount, resultCashAccount);
        assertNotEquals(secondExpectedCashAccount, resultCashAccount);
        assertEquals(expectedBalance, resultCashAccount.getBalance());
        assertEquals(expectedID, resultCashAccount.getID());
        assertEquals(expectedAccountType, resultCashAccount.getAccountType());
        assertTrue(resultCashAccount.isAccountOwner(familyId));
        assertTrue(resultCashAccount.isCashAccount());
    }

    @Test
    void isAccountOwner_False() {

        //Arrange
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        ID otherID = new FamilyID("123");

        //Act
        boolean result = familyCashAccount.isAccountOwner(otherID);

        //Assert
        assertFalse(result);
    }

    @Test
    void isIDFalse() {

        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("2")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        assertFalse(familyCashAccount.isID(new AccountID("1")));
    }

    @Test
    void isID() {

        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        assertTrue(familyCashAccount.isID(new AccountID("1")));
    }

    @Test
    void equals() {
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        FamilyCashAccount familyCashAccount2 = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();

        assertEquals(familyCashAccount, familyCashAccount2);
        assertEquals(familyCashAccount.hashCode(), familyCashAccount2.hashCode());
    }

    @Test
    void equalsFalse() {
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        FamilyCashAccount familyCashAccount2 = new FamilyCashAccount.Builder(new AccountID("2")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();

        assertNotEquals(familyCashAccount, familyCashAccount2);
        assertNotEquals(familyCashAccount.hashCode(), familyCashAccount2.hashCode());
    }

    @Test
    void testEqualsDifferentClass() {
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        Family family = new Family.Builder(new FamilyID("1")).build();

        assertNotEquals(familyCashAccount, family);
    }

    @Test
    void testEqualsSameObject() {
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        assertEquals(familyCashAccount, familyCashAccount);
        assertEquals(familyCashAccount.hashCode(), familyCashAccount.hashCode());
    }


    @Test
    void testEqualsSameClassNull() {
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        FamilyCashAccount familyCashAccountTwo = null;

        assertNotEquals(familyCashAccount, familyCashAccountTwo);
    }

    @Test
    void addFunds() {
        FamilyCashAccount familyCashAccount = new FamilyCashAccount.Builder(new AccountID("1")).setAccountType().setOwnerID(new PersonID(new Email("baggins@isep.pt")))
                .setDescription(new Description("Cash account")).setBalance(new MoneyValue(100)).build();
        familyCashAccount.addFunds(new MoneyValue(10));

        assertEquals(new MoneyValue(110), familyCashAccount.getBalance());
    }
}

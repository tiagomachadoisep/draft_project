package switchtwentytwenty.project.domain.aggregates.category;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.CreditCardAccount;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;

import static org.junit.jupiter.api.Assertions.*;

class BasicCategoryTest {

    @Test
    void testEquals_ValueIsNull() {

        //Arrange
        CategoryID categoryID = new CategoryID("123");
        BasicCategory category1 = new StandardCategory.Builder(categoryID).build();

        //Act
        boolean result = category1.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void testEquals_ValueIsOfDifferentClass() {

        //Arrange
        CategoryID categoryID = new CategoryID("123");
        BasicCategory category1 = new StandardCategory.Builder(categoryID).build();
        Account creditCardAccount = new CreditCardAccount.Builder(new AccountID("123")).build();

        //Act
        boolean result = category1.equals(creditCardAccount);

        //Assert
        assertFalse(result);

    }
}
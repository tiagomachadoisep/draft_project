package switchtwentytwenty.project.domain.aggregates.ledger;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    @Test
    void payment(){
        //Arrange
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        Description description = new Description("test payment");
        TransactionDate date = new TransactionDate(LocalDate.MIN);
        Payment payment = new Payment(transactionID,accountID,amount,categoryID,description,date);
        //Assert
        assertEquals(description, payment.getDescription());
        assertEquals(transactionID, payment.getID());
        assertEquals(transactionID, payment.getID());
        assertTrue(payment.isID(transactionID));
        assertFalse(payment.isID(new TransactionID("no")));
        assertEquals(new Movement(accountID,amount.toNegative()),payment.getMovementDebit());
        assertTrue(payment.isPayment());
        assertFalse(payment.isTransfer());
        assertEquals(date,payment.getTransactionDate());
        assertEquals(categoryID,payment.getCategoryId());
        assertEquals(ETransactionType.PAYMENT,payment.getTransactionType());
        assertEquals(accountID,payment.getAccountID());
        assertEquals(-amount.getValue(),payment.getAmount());
        assertEquals(payment,payment);
        assertNotEquals(amount,payment);
        assertEquals(new Payment(transactionID,accountID,amount,categoryID,description,date),payment);
        assertEquals(111,payment.hashCode());
    }

    @Test
    void testEqualsAndHashCode(){
        TransactionID transactionID = new TransactionID("211245");
        AccountID accountID = new AccountID("1");
        MoneyValue value = new MoneyValue(300);
        CategoryID categoryID = new CategoryID("20");

        Payment payment = new Payment(transactionID,accountID,value,categoryID);
        Payment paymentTwo = new Payment(transactionID,accountID,value,categoryID);
        Payment paymentThree = new Payment(new TransactionID("21"),accountID,value,categoryID);
        Family family = new Family.Builder(new FamilyID("2")).build();

        assertEquals(payment,payment);
        assertEquals(payment,paymentTwo);
        assertEquals(payment.hashCode(),paymentTwo.hashCode());
        assertNotEquals(payment,paymentThree);
        assertNotEquals(payment.hashCode(),paymentThree.hashCode());
        assertNotEquals(payment,family);

    }

}
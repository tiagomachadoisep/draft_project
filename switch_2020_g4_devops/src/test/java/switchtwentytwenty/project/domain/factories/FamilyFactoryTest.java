package switchtwentytwenty.project.domain.factories;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyFactoryTest {

    @Test
    void createFamily(){
        FamilyFactory factory= new FamilyFactory();
        FamilyID familyID = new FamilyID("1");
        FamilyName name = new FamilyName("Silva");
        PersonID adminEmail = new PersonID(new Email("ze@isep.pt"));
        PersonName adminName = new PersonName("Ze");
        VATNumber adminVat = new VATNumber(379467062);
        BirthDate birthDate = new BirthDate("22-01-1990");
        Address address = new Address("other");

        List<Object> list = factory.createFamilyAndAdmin(familyID,name,adminEmail,adminName,adminVat,birthDate, address);

        Person admin = (Person) list.get(0);
        Family family = (Family) list.get(1);

        assertEquals(admin.getName(),adminName);
        assertEquals(admin.getID(),adminEmail);
        assertEquals(admin.getFamilyID(),familyID);
        assertEquals(family.getName(),name.toString());
        assertEquals(family.getID(),familyID);
    }

}
package switchtwentytwenty.project.domain.domainservices;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.PersonalCashAccount;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LedgerAccountServiceTest {

    @Test
    void addPayment() {
        //arrange
        Ledger memberLedger = new Ledger(1, "ze@isep.pt");
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("ze cash account")).setOwnerID(new PersonID(new Email("ze@isep.pt"))).build();

        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");
        TransactionDate date = new TransactionDate(LocalDate.now());

        LedgerAccountService service = new LedgerAccountService();

        //act
        TransactionID id = service.addPayment(memberLedger, memberAccount, amount, categoryID, date);

        //assert
        assertEquals(90, memberAccount.getBalance().getValue());
        assertEquals(1, memberLedger.getTransactionCount());
    }

    @Test
    void addPayment_NoFunds() {
        //arrange
        Ledger memberLedger = new Ledger(1, "ze@isep.pt");
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("ze cash account")).setOwnerID(new PersonID(new Email("ze@isep.pt"))).build();

        MoneyValue amount = new MoneyValue(1000);
        CategoryID categoryID = new CategoryID("1");
        TransactionDate date = new TransactionDate(LocalDate.now());

        LedgerAccountService service = new LedgerAccountService();

        //act
        TransactionID id = service.addPayment(memberLedger, memberAccount, amount, categoryID, date);

        //assert
        assertEquals(100, memberAccount.getBalance().getValue());
        assertEquals(0, memberLedger.getTransactionCount());
        assertEquals(id, new TransactionID("-1"));
    }

    @Test
    void addTransfer() {
        //Arrange
        Ledger memberLedger = new Ledger(1, "ze@isep.pt");
        Ledger otherLedger = new Ledger(2, "carlos@isep.pt");
        TransactionDate date = new TransactionDate(LocalDate.now());

        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("ze cash account")).setOwnerID(new PersonID(new Email("ze@isep.pt"))).build();

        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("2")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("carlos cash account")).setOwnerID(new PersonID(new Email("carlos@isep.pt"))).build();

        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");

        LedgerAccountService service = new LedgerAccountService();
        //Act
        TransactionID id = service.addTransfer(memberLedger, otherLedger, memberAccount, otherAccount, amount, categoryID, date);

        //Assert
        assertEquals(90, memberAccount.getBalance().getValue());
        assertEquals(20,otherAccount.getBalance().getValue());
        assertTrue(memberLedger.getTransactionCount()==1);
    }

    @Test
    void addTransfer_NoFunds() {
        //Arrange
        Ledger memberLedger = new Ledger(1, "ze@isep.pt");
        Ledger otherLedger = new Ledger(2, "carlos@isep.pt");
        TransactionDate date = new TransactionDate(LocalDate.now());

        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("ze cash account")).setOwnerID(new PersonID(new Email("ze@isep.pt"))).build();

        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("2")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("carlos cash account")).setOwnerID(new PersonID(new Email("carlos@isep.pt"))).build();

        MoneyValue amount = new MoneyValue(1000);
        CategoryID categoryID = new CategoryID("1");

        LedgerAccountService service = new LedgerAccountService();
        //Act
        TransactionID id = service.addTransfer(memberLedger, otherLedger, memberAccount, otherAccount, amount, categoryID, date);

        //Assert
        assertEquals(100, memberAccount.getBalance().getValue());
        assertTrue(memberLedger.getTransactionCount()==0);
        assertEquals(10,otherAccount.getBalance().getValue());
        assertTrue(otherLedger.getTransactionCount()==0);
        assertEquals(id,new TransactionID("-1"));
    }
}
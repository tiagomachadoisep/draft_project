package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VATNumberTest {

    @Test
    public void createInvalidVat() {
        assertThrows(IllegalArgumentException.class, () -> { VATNumber vat = new VATNumber(211466043);
            });
    }
    @Test
    public void createVatWithManyNumbers() {
        assertThrows(IllegalArgumentException.class, () -> {
            VATNumber vat = new VATNumber(2114660431);
        });
    }

    @Test
     public void comparingVats(){
         VATNumber vat1 = new VATNumber(268133115);
         VATNumber vat2 = new VATNumber(211466042);
         VATNumber vat3 = new VATNumber(268133115);
         VATNumber vatNull = null;

         assertEquals(vat1, vat3);
         assertEquals(vat1.hashCode(), vat3.hashCode());
         assertNotEquals(vat1, vat2);
         assertNotEquals(vat1.hashCode(), vat2.hashCode());
         assertNotEquals(vat1,vatNull);

    }
    @Test
    public void validVatEqualsTesting() {
               VATNumber vat = new VATNumber(100000002);
                assertNotEquals(null, vat);
                assertEquals(vat, vat);
    }
     @Test
    public void comparingVATandEmailEqualsTesting() {
               VATNumber vat = new VATNumber(100000002);
                Email emailAddress = new Email("marta@lol.pt");
                assertNotEquals(vat, emailAddress);
    }

    @Test
    public void createValidVatDefault() {
        VATNumber vat = new VATNumber(278644600);
        assertNotNull(vat);
    }

    @Test
   public void createValidVatZero() {
        VATNumber vat = new VATNumber(0);
        assertNotNull(vat);
    }
    @Test
    public void getVat() {
        VATNumber vat = new VATNumber(100000002);
        int vatInt = vat.getVat();
        int expected = 100000002;
                assertEquals(vatInt,expected);
    }

    @Test
    void testvalidateVAT(){
        assertThrows(IllegalArgumentException.class, () -> {
            VATNumber vat = new VATNumber(241670477);
        });
    }

    @Test
    public void createValidVatRestZero() {
        VATNumber vat = new VATNumber(241670470);
        assertNotNull(vat);
    }

    @Test
    public void createValidVatRestOne() {
        VATNumber vat = new VATNumber(241670420);
        assertNotNull(vat);
    }

    @Test
    public void toStringValidVatRestOne() {
        String vatThing = "241670420";
        VATNumber vat = new VATNumber(241670420);
        String actual = vat.toString();
        assertEquals(vatThing, actual);
    }

}

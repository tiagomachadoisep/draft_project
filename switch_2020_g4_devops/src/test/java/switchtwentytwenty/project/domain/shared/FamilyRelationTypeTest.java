package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class FamilyRelationTypeTest {

    @Test
    void familyRelationTypeConstructor_validString_FirstCase() {

        String designation = "father";

        FamilyRelationType result = new FamilyRelationType(designation);

        assertNotNull(result);
    }

    @Test
    void familyRelationTypeConstructor_validString_SecondCase() {

        String designation = "mother-in-law";

        FamilyRelationType result = new FamilyRelationType(designation);

        assertNotNull(result);
    }

    @Test
    void familyRelationTypeConstructor_validString_spacesAtStartAndEnd() {

        String designation = "  father    ";

        FamilyRelationType result = new FamilyRelationType(designation);

        assertNotNull(result);
    }

    @Test
    void familyRelationTypeConstructor_validString_CapitalizedLetters() {

        String designation = "FATHER";

        FamilyRelationType result = new FamilyRelationType(designation);

        assertNotNull(result);
    }

    @Test
    void familyRelationTypeConstructor_validString_FirstLetterCapitalized() {

        String designation = "Father";

        FamilyRelationType result = new FamilyRelationType(designation);

        assertNotNull(result);
    }

    @Test
    void familyRelationTypeConstructor_invalidString_nullString() {

        String designation = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationType result = new FamilyRelationType(designation);
        });
    }

    @Test
    void familyRelationTypeConstructor_invalidString_emptyString() {

        String designation = "";

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationType result = new FamilyRelationType(designation);
        });
    }

    @Test
    void familyRelationTypeConstructor_invalidString_invalidDesignation() {

        String designation = "little sister";

        Assertions.assertThrows(InvalidDesignationException.class, () -> {
            FamilyRelationType result = new FamilyRelationType(designation);
        });
    }

    @Test
    void EqualsAndHashcode() {

        FamilyRelationType familyRelationType = new FamilyRelationType("brother");
        FamilyRelationType sameDesignation = new FamilyRelationType("brother");
        FamilyRelationType differentDesignation = new FamilyRelationType("sister");
        PersonID differentClass = new PersonID(new Email("admin@gmail.com"));
        FamilyRelationType nullRelationType = null;

        assertEquals(familyRelationType, familyRelationType);
        assertEquals(familyRelationType.hashCode(), familyRelationType.hashCode());

        assertEquals(familyRelationType, sameDesignation);
        assertEquals(familyRelationType.hashCode(), sameDesignation.hashCode());

        assertNotEquals(familyRelationType, differentDesignation);
        assertNotEquals(familyRelationType.hashCode(), differentDesignation.hashCode());

        assertNotEquals(familyRelationType, differentClass);
        assertNotEquals(familyRelationType.hashCode(), differentClass.hashCode());

        assertNotEquals(familyRelationType, nullRelationType);
    }

}
package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import static org.junit.jupiter.api.Assertions.*;

public class EmailTest {

        @Test
        @DisplayName("Testing create email with valid format")
        public void CreatingValidEmailAddress() {
            Email email = new Email("1120717@isep.ipp.pt");
            assertNotNull(email);
        }

        @Test
        @DisplayName("Testing null email")
        public void NullEmailAddress() {
            assertThrows(IllegalArgumentException.class, () -> {
                Email email = new Email(null);
            });
        }

        @Test
        @DisplayName("Testing create email with wrong format")
        public void CreatingEmailAddressEmpty() {

            assertThrows(IllegalArgumentException.class, () -> {
                Email badEmail = new Email(" ");
            });
        }

        @Test
        @DisplayName("Testing create email with wrong format")
        public void createEmailAddressWithInvalidDomain() {

            assertThrows(IllegalArgumentException.class, () -> {
                Email badEmail = new Email("1128717@isep.ipp.p");
            });
        }

        @Test
        @DisplayName("Testing create email with wrong format")
        public void ValidateEmailAddressWithNoAtSign() {

                    assertThrows(IllegalArgumentException.class, () -> {
                        Email badEmail = new Email("1120717.isep.ipp.pt");
                    });
        }

        @Test
        @DisplayName("Testing create email with wrong format")
        public void CreatingEmailAddressWithSpace() {

                    assertThrows(IllegalArgumentException.class, () -> {
                        Email badEmail = new Email("11 20717@isep.ipp.pt");
                    });
        }

        @Test
        @DisplayName("Testing create email with wrong format")
        public void CreatingEmailAddressIncomplete() {

                    assertThrows(IllegalArgumentException.class, () -> {
                        Email badEmail = new Email("1120717@incomplete");
                    });
        }

        @Test
        @DisplayName("Testing hasSameEmail method")
        void hasSameEmail() {
            String expected = "test@test.pt";
            Email newExp = new Email(expected);
            boolean result = newExp.sameEmail("test@test.pt");
            assertTrue(result);
        }

        @Test
        @DisplayName("Testing hasSameEmail negative method")
        void hasNotSameEmail() {
            String expected = "marta@test.pt";
            Email newExp = new Email(expected);
            boolean result = newExp.sameEmail("test@test.pt");
            assertFalse(result);
        }

        @Test
        @DisplayName("Testing ToString method")
        void testToString() {
            Email emailAddress = new Email("test@test.pt");
            String expected = emailAddress.toString();
            String result = "test@test.pt";
            assertEquals(expected, result);
        }

        @Test
        @DisplayName("Testing ToString method with false result")
        void testToString_notEquals() {
            Email emailAddress = new Email("test@test.pt");
            String expected = emailAddress.toString();
            String result = "test@test2.pt";
            assertNotEquals(expected, result);
        }

        @Test
        @DisplayName("Testing equals method")
        void testEquals() {
            Email emailAddress = new Email("test@test.pt");
            Email emailAddress1 = new Email("test@test.pt");
            boolean result = emailAddress.equals(emailAddress1);
            assertTrue(result);
            assertEquals(emailAddress.hashCode(),emailAddress1.hashCode());
        }

        @Test
        @DisplayName("Testing different instances of the class")
        void testEquals2() {
            Email emailAddress = new Email("test@test.pt");
            Email emailAddress1 = new Email("test@321.pt");

            assertNotEquals(emailAddress,emailAddress1);
            assertNotEquals(emailAddress.hashCode(),emailAddress1.hashCode());
        }

        @Test
        @DisplayName("Testing equals Same Object")
        void testEqualsSameObject() {
            Email emailAddress = new Email("test@test.pt");


            assertEquals(emailAddress, emailAddress);

        }


        @Test
        @DisplayName("Testing equals Different Class")
        void testEqualsDifferentClass() {
            Email emailAddress = new Email("test@test.pt");
            Family family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Rodrigues"))
                    .setAdminId(new PersonID(new Email("abc@abc.pt"))).setRegistrationDate(new RegistrationDate("01.01.2021")).build();

            assertNotEquals(emailAddress, family);
        }

    @Test
    void testEquals1() {
            Email email1 = new Email();
            Email email2 = new Email();

        assertEquals(email1, email2);
    }

    @Test
    void testEquals3() {
        Email email1 = new Email();
        Email email2 = new Email("test@isep.pt");

        assertNotEquals(email1, email2);
        assertNotEquals(email2, email1);
    }
}

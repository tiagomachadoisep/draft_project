package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class MovementTest {

    @Test
    void testEqualsAndHash() {
        AccountID accountID = new AccountID("1");
        MoneyValue moneyValue = new MoneyValue(200);

        Movement movementOne = new Movement(accountID,moneyValue);
        Movement movementTwo = new Movement(new AccountID("2"),new MoneyValue(500));
        Movement movementThree = new Movement(accountID,new MoneyValue(1000));
        Movement movementFour = new Movement(new AccountID("3"),moneyValue);
        Movement movementFive = new Movement(accountID,moneyValue);
        Family family = new Family.Builder(new FamilyID("1")).build();


        assertEquals(movementOne,movementOne);
        assertEquals(movementOne,movementFive);
        assertEquals(movementOne.hashCode(),movementFive.hashCode());
        assertNotEquals(movementOne,movementTwo);
        assertNotEquals(movementOne.hashCode(),movementTwo.hashCode());
        assertNotEquals(movementOne,movementThree);
        assertNotEquals(movementOne,movementFour);
        assertNotEquals(movementOne,family);
        assertNotEquals(null, movementOne);


    }


}
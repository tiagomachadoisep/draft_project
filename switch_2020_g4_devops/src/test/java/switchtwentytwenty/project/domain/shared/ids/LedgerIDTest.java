package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class LedgerIDTest {

    @Test
    void setLedgerID() {

        LedgerID ledgerID = new LedgerID(2);
        ledgerID.setLedgerIdValue(14);

        assertEquals(14, ledgerID.getLedgerIdValue());
    }

    @Test
    void testEqualsSameObject() {
        LedgerID ledgerID = new LedgerID(1);

        assertEquals(ledgerID, ledgerID);
        assertEquals(ledgerID.hashCode(), ledgerID.hashCode());
    }


    @Test
    void testEqualsDifferentObjectSameArguments() {
        LedgerID ledgerID = new LedgerID(1);
        LedgerID ledgerIDTwo = new LedgerID(1);

        assertEquals(ledgerID, ledgerIDTwo);
        assertEquals(ledgerID.hashCode(), ledgerIDTwo.hashCode());
    }

    @Test
    void testEqualsDifferentClass() {
        LedgerID ledgerID = new LedgerID(1);
        Family family = new Family.Builder(new FamilyID("20")).build();

        assertNotEquals(ledgerID, family);
        assertNotEquals(ledgerID.hashCode(), family.hashCode());
    }


    @Test
    void testEqualsSameClassDifferentArguments() {
        LedgerID ledgerID = new LedgerID(1);
        LedgerID ledgerIDTwo = new LedgerID(2);

        assertNotEquals(ledgerID, ledgerIDTwo);
        assertNotEquals(ledgerID.hashCode(), ledgerIDTwo.hashCode());
    }


    @Test
    void testEqualsSameClassOneNullObject() {
        LedgerID ledgerID = new LedgerID(1);
        LedgerID ledgerIDTwo = null;

        assertNotEquals(ledgerID, ledgerIDTwo);

    }


}
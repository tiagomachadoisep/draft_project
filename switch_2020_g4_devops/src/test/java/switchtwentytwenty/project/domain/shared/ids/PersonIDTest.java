package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Email;

import static org.junit.jupiter.api.Assertions.*;

class PersonIDTest {


    @Test
    void PersonIDConstructor() {

        //Arrange
        Email email = new Email("carla@hotmail.com");

        //Act
        PersonID personID = new PersonID(email);
        PersonID personWithoutEmailID = new PersonID();

        //Assert
        assertNotNull(personID);
        assertNotNull(personWithoutEmailID);
    }

    @Test
    void PersonID_Equals() {

        //Arrange
        Email email = new Email("carla@hotmail.com");
        PersonID expected = new PersonID(email);
        PersonID personIDNull = null;

        //Act
        PersonID personID = new PersonID(email);

        //Assert
        assertNotEquals(personIDNull,personID);
        assertEquals(expected, personID);
        assertNotEquals(null, expected);
        assertEquals(expected.hashCode(),personID.hashCode());
    }

    @Test
    void PersonID_NotEquals() {

        //Arrange
        Email email = new Email("carla@hotmail.com");
        PersonID expected = new PersonID(new Email("carlota@hotmail.com"));

        //Act
        PersonID personID = new PersonID(email);

        //Assert
        assertNotEquals(expected, personID);
    }
    @Test
    void PersonID_toString() {

        //Arrange
        String expected = "carla@hotmail.com";
        Email email = new Email("carla@hotmail.com");
        PersonID personIDexpected = new PersonID(email);

        //Act
        String actual = personIDexpected.toString();

        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void PersonID_toStringWhenIdNull() {

        //Arrange
        PersonID personIdExpected = new PersonID(null);

        //Act
        String actual = personIdExpected.toString();

        //Assert
        assertEquals("", actual);
    }
    @Test
    void PersonID_isAPerson() {

        //Arrange
        String expected = "carla@hotmail.com";
        Email email = new Email("carla@hotmail.com");
        PersonID personIDexpected = new PersonID(email);

        //Act
        boolean result = personIDexpected.isPerson();

        //Assert
        assertTrue(result);
    }

    @Test
    void PersonID_AndFamilyIDAreNotTheSame() {

        //Arrange
        String expected = "carla@hotmail.com";
        Email email = new Email("carla@hotmail.com");
        PersonID personIDexpected = new PersonID(email);

        FamilyID id = new FamilyID("1idj");
        //Act


        //Assert
        assertNotEquals(personIDexpected, id);
    }

}
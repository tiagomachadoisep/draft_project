package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import static org.junit.jupiter.api.Assertions.*;

class CategoryNameTest {

    @Test
    void validCategoryName() {
        //ARRANGE
        String categoryNameToBe = "Animals";
        //ACT
        CategoryName categoryName = new CategoryName(categoryNameToBe);
        //ASSERT
        assertNotNull(categoryName);
    }

    @Test
    void capitalizedCategoryName() {
        //ARRANGE
        String categoryNameToBe = "any";
        CategoryName expected = new CategoryName("Any");
        //ACT
        CategoryName categoryName = new CategoryName(categoryNameToBe);
        //ASSERT
        assertEquals(expected, categoryName);
    }

    @Test
    void invalidCategoryNameFormat() {
        //ARRANGE
        String categoryNameToBe = "Animals1028";
        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            CategoryName categoryName = new CategoryName(categoryNameToBe);
        });


    }
    @Test
    void invalidCategoryNameFormatItOnlyHasTwoLetters() {
        //ARRANGE
        String categoryNameToBe = "An";;
        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            CategoryName categoryName = new CategoryName(categoryNameToBe);
        });


    }

    @Test
    void nullCategoryName() {
        //ARRANGE

        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            CategoryName categoryName = new CategoryName(null);
        });


    }

    @Test
    void emptyCategoryName() {
        //ARRANGE
        String categoryNameToBe = "       ";
        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            CategoryName categoryName = new CategoryName(categoryNameToBe);
        });

    }

    @Test
    void anotherEmptyCategoryName() {
        //ARRANGE
        String categoryNameToBe = "";
        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            CategoryName categoryName = new CategoryName(categoryNameToBe);
        });

    }

    @Test
    void toStringCategoryName() {
        //ARRANGE
        String categoryNameToBe = "Switch";
        CategoryName categoryName = new CategoryName(categoryNameToBe);
        //ACT
        String actualName = categoryName.toString();
        //ASSERT
        assertEquals(categoryNameToBe, actualName);


    }
    @Test
    void testingEqualsAndHashCodeCategoryName() {
        //ARRANGE
        String categoryNameToBe = "Animals";
        CategoryName categoryName = new CategoryName(categoryNameToBe);
        CategoryName secondCategoryName = new CategoryName(categoryNameToBe);
        //ACT
        boolean result = categoryName.equals(secondCategoryName);
        //ASSERT
        assertTrue(result);
        assertNotEquals(null, categoryName);
        assertEquals(categoryName.hashCode(), secondCategoryName.hashCode());

    }

    @Test
    void sameClassesButDifferentAttributes() {
        //ARRANGE
        String categoryNameToBe = "Animals";
        CategoryName categoryName = new CategoryName(categoryNameToBe);
        CategoryName secondCategoryName = new CategoryName("Plants");
        //ACT
        boolean result = categoryName.equals(secondCategoryName);
        //ASSERT
        assertFalse(result);
        assertNotEquals(categoryName.hashCode(), secondCategoryName.hashCode());

    }
    @Test
    void testingEqualsWithDifferentClassesAndDifferentCategoryNames() {
        //ARRANGE
        String categoryNameToBe = "Animals";
        String anotherName = "Animes";
        CategoryName categoryName = new CategoryName(categoryNameToBe);
        CategoryName secondCategoryName = new CategoryName(anotherName);
        Email email = new Email("jujutsu@mappa.pt");
        //ACT
        boolean result = categoryName.equals(secondCategoryName);
        boolean differentClasses = categoryName.equals(email);
        //ASSERT
        assertFalse(result);
        assertFalse(differentClasses);

    }

    @Test
    void testingEqualsComparingWithNullObject() {

        //ARRANGE
        String categoryNameString = "Beer";
        CategoryName categoryName = new CategoryName(categoryNameString);

        //ACT
        boolean result = categoryName.equals(null);

        //ASSERT
        assertFalse(result);

    }

}
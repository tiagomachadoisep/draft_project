package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class RelationTypeTest {

    @Test
    void valueOfType_trueCase() {
        String designation = "sister-in-law";

        boolean result = RelationType.isThereValueOfType(designation);

        assertTrue(result);
    }

    @Test
    void valueOfType_falseCase_EnumTypeDoesNotExist() {
        String designation = "darling";

        boolean result = RelationType.isThereValueOfType(designation);

        assertFalse(result);
    }

    @Test
    void valueOfType_falseCase_IncorrectEnumType() {
        String designation = "Sister-in-law";

        boolean result = RelationType.isThereValueOfType(designation);

        assertFalse(result);
    }

    @Test
    void valueOfType_falseCase_nulLStringPassed() {
        String designation = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            boolean result = RelationType.isThereValueOfType(designation);
        });
    }

    @Test
    void valueOfType_falseCase_emptyStringPassed() {
        String designation = "";

        boolean result = RelationType.isThereValueOfType(designation);

        assertFalse(result);
    }

    @Test
    void testToString_sameString() {
        String stepsister = "stepsister";

        String result = RelationType.STEPSISTER.toString();

        assertEquals(stepsister,result);
    }

    @Test
    void testToString_differentString() {
        String mother = "mother";

        String result = RelationType.STEPSISTER.toString();

        assertNotEquals(mother,result);
    }

    @Test
    void values_sameObject() {
        RelationType[] relationTypes = {RelationType.MOTHER,
                RelationType.FATHER,
                RelationType.SON,
                RelationType.DAUGHTER,
                RelationType.HUSBAND,
                RelationType.WIFE,
                RelationType.BROTHER,
                RelationType.SISTER,
                RelationType.COUSIN,
                RelationType.AUNT,
                RelationType.UNCLE,
                RelationType.NEPHEW,
                RelationType.NIECE,
                RelationType.GRANDFATHER,
                RelationType.GRANDMOTHER,
                RelationType.GRANDSON,
                RelationType.GRANDDAUGHTER,
                RelationType.FRIEND,
                RelationType.BOYFRIEND,
                RelationType.GIRLFRIEND,
                RelationType.SISTER_IN_LAW,
                RelationType.BROTHER_IN_LAW,
                RelationType.FATHER_IN_LAW,
                RelationType.MOTHER_IN_LAW,
                RelationType.STEPFATHER,
                RelationType.STEPMOTHER,
                RelationType.STEPDAUGHTER,
                RelationType.STEPSON,
                RelationType.STEPSISTER,
                RelationType.STEPBROTHER};

        RelationType[] result = RelationType.values();

        assertArrayEquals(relationTypes, result);
    }

    @Test
    void values_notSameObject() {
        RelationType[] relationTypes = {RelationType.MOTHER,
                RelationType.MOTHER,
                RelationType.SON,
                RelationType.DAUGHTER,
                RelationType.HUSBAND,
                RelationType.WIFE,
                RelationType.BROTHER,
                RelationType.SISTER,
                RelationType.COUSIN,
                RelationType.AUNT,
                RelationType.UNCLE,
                RelationType.NEPHEW,
                RelationType.NIECE,
                RelationType.GRANDFATHER,
                RelationType.GRANDMOTHER,
                RelationType.GRANDSON,
                RelationType.GRANDDAUGHTER,
                RelationType.FRIEND,
                RelationType.BOYFRIEND,
                RelationType.GIRLFRIEND,
                RelationType.SISTER_IN_LAW,
                RelationType.BROTHER_IN_LAW,
                RelationType.FATHER_IN_LAW,
                RelationType.MOTHER_IN_LAW,
                RelationType.STEPFATHER,
                RelationType.STEPMOTHER,
                RelationType.STEPDAUGHTER,
                RelationType.STEPSON,
                RelationType.STEPSISTER,
                RelationType.STEPBROTHER};

        RelationType[] result = RelationType.values();

        assertFalse(Arrays.equals(relationTypes, result));
    }

    @Test
    void valueOf_sameObject() {

        RelationType relationType = RelationType.valueOf("SISTER_IN_LAW");

        assertSame(RelationType.SISTER_IN_LAW, relationType);
    }

    @Test
    void valueOf_differentObject() {

        RelationType relationType = RelationType.valueOf("MOTHER");

        assertNotSame(RelationType.SISTER_IN_LAW, relationType);
    }

}
package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyValueTest {

    @Test
    void MoneyValueConstructor_Value() {

        //Arrange
        MoneyValue expected = new MoneyValue(14.52);

        //Act
        MoneyValue balance = new MoneyValue(14.52);

        //Assert
        assertNotNull(balance);
        assertEquals(expected, balance);

    }

    @Test
    void MoneyValueConstructor_ValueAndCurrency() {

        //Arrange
        MoneyValue expected = new MoneyValue(-51, "USD");

        //Act
        MoneyValue balance = new MoneyValue(-51, "USD");

        //Assert
        assertNotNull(balance);
        assertEquals(expected, balance);

    }

    @Test
    void isPositive_True() {

        //Arrange
        MoneyValue balance = new MoneyValue(30);

        //Act
        boolean result = balance.isPositive();

        //Arrange
        assertTrue(result);
    }

    @Test
    void isPositive_FalseBecauseIsZero() {

        //Arrange
        MoneyValue balance = new MoneyValue(0);

        //Act
        boolean result = balance.isPositive();

        //Arrange
        assertFalse(result);
    }

    @Test
    void isPositive_FalseBecauseIsNegative() {

        //Arrange
        MoneyValue balance = new MoneyValue(-10.50);

        //Act
        boolean result = balance.isPositive();

        //Arrange
        assertFalse(result);
    }

    @Test
    void isNegative_True() {

        //Arrange
        MoneyValue balance = new MoneyValue(-30);

        //Act
        boolean result = balance.isNegative();

        //Arrange
        assertTrue(result);

    }

    @Test
    void isNegative_FalseBecauseIsZero() {

        //Arrange
        MoneyValue balance = new MoneyValue(0);

        //Act
        boolean result = balance.isNegative();

        //Arrange
        assertFalse(result);
    }

    @Test
    void isNegative_FalseBecauseIsPositive() {

        //Arrange
        MoneyValue balance = new MoneyValue(10.50);

        //Act
        boolean result = balance.isNegative();

        //Arrange
        assertFalse(result);
    }

    @Test
    void equals_ValueAndCurrencyAreEqual() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100);
        MoneyValue balance_2 = new MoneyValue(100);

        //Act
        boolean result = balance_1.equals(balance_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void equals_ValueAndCurrencyAreDifferent() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        MoneyValue balance_2 = new MoneyValue(50, "USD");

        //Act
        boolean result = balance_1.equals(balance_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_ValueIsEqualAndCurrencyIsDifferent() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        MoneyValue balance_2 = new MoneyValue(100, "USD");

        //Act
        boolean result = balance_1.equals(balance_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_ValueIsDifferentAndCurrencyIsEqual() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        MoneyValue balance_2 = new MoneyValue(1000, "EUR");

        //Act
        boolean result = balance_1.equals(balance_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_CompareObjectWithItself() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        //Act
        boolean result = balance_1.equals(balance_1);

        //Assert
        assertTrue(result);

    }

    @Test
    void equals_CompareObjectWithObjectOfDifferentClass() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        FamilyName familyName = new FamilyName("Santos");

        //Act
        boolean result = balance_1.equals(familyName);

        //Assert
        assertFalse(result);

    }

    @Test
    void equals_CompareObjectWithNull() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");

        //Act
        boolean result = balance_1.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void hashCode_ReturnsTrue() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        MoneyValue balance_2 = new MoneyValue(100, "EUR");

        //Act
        int result_1 = balance_1.hashCode();
        int result_2 = balance_2.hashCode();

        //Assert
        assertEquals(result_1, result_2);

    }

    @Test
    void hashCode_ReturnsFalse() {

        //Arrange
        MoneyValue balance_1 = new MoneyValue(100, "EUR");
        MoneyValue balance_2 = new MoneyValue(1000, "EUR");

        //Act
        int result_1 = balance_1.hashCode();
        int result_2 = balance_2.hashCode();

        //Assert
        assertNotEquals(result_1, result_2);

    }

    @Test
    void toNegative_PositiveValue() {

        //Arrange
        MoneyValue balance = new MoneyValue(100);
        int expected = -100;

        //Act
        MoneyValue result = balance.toNegative();

        //Assert
        assertEquals(expected, result.getValue());

    }

    @Test
    void toNegative_NegativeValue() {

        //Arrange
        MoneyValue balance = new MoneyValue(-100);
        int expected = 100;

        //Act
        MoneyValue result = balance.toNegative();

        //Assert
        assertEquals(expected, result.getValue());

    }


}
package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;


import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    @Test
    void validAddress() {
        Address address = new Address("Rua tal");
        assertNotNull(address);
    }

    @Test
    void whenCreteNewAddress_throwExceptionIfEmpty(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Address address = new Address("");
        });
    }

    @Test
    void whenCreteNewAddress_throwExceptionIfNull(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Address address = new Address(null);
        });
    }

    @Test
    void toStringValidAddress() {
        String addressValue = "Rua tal";
        Address address = new Address(addressValue);

        //ACT
        String actual = address.toString();

        //ASSERT
        assertEquals(addressValue, actual);
    }

}
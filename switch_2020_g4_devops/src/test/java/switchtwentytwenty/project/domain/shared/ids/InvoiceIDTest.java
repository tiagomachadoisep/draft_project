package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class InvoiceIDTest {

    @Test
    void validInvoiceId() {
        //ARRANGE
        int invoice = 1234;
        InvoiceID id = new InvoiceID(invoice);

        //ASSERT
        assertNotNull(id);
    }

    @Test
    void getInvoiceId() {
        //ARRANGE
        int invoice = 1234;
        InvoiceID id = new InvoiceID(invoice);

        //ASSERT
        int actual = id.getId();
        assertEquals(actual, invoice);
    }

}
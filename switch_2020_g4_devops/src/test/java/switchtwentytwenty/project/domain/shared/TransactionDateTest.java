package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TransactionDateTest {

    @Test
    void testEqualsAndHashCode(){
        String transactionDateString = "2020-12-24";
        TransactionDate transactionDate = new TransactionDate(transactionDateString);
        TransactionDate transactionDateTwo = new TransactionDate(transactionDateString);
        TransactionDate transactionDateThree = new TransactionDate("2020-05-05");
        Family family = new Family.Builder(new FamilyID("2")).build();

        assertEquals(transactionDate,transactionDate);
        assertEquals(transactionDate,transactionDateTwo);
        assertEquals(transactionDate.hashCode(),transactionDateTwo.hashCode());
        assertNotEquals(transactionDate,transactionDateThree);
        assertNotEquals(transactionDate.hashCode(),transactionDateThree.hashCode());
        assertNotEquals(transactionDate,family);
        assertNotEquals(null,transactionDate);

    }

    @Test
    void whenTransactionDateIsNull_returnLocalDateMin(){
        String transactionDateValue = null;
        TransactionDate transactionDate = new TransactionDate(transactionDateValue);

        assertEquals(LocalDate.MIN, transactionDate.getTransactionDate());
    }

    @Test
    void checkIfDateIsOutOfGivenBoundaries_thenReturnFalse(){

        TransactionDate start = new TransactionDate(LocalDate.of(2020,2,3));
        TransactionDate end = new TransactionDate(LocalDate.of(2020,8,3));

        TransactionDate testStart = new TransactionDate(LocalDate.of(2019,2,3));
        TransactionDate testEnd = new TransactionDate(LocalDate.of(2021,2,3));

        boolean result= testStart.isBetween(start,end);
        boolean resultTwo = testEnd.isBetween(start,end);

        assertFalse(result);
        assertFalse(resultTwo);
    }

   /* @Test
    void checkIfDateIsNull_thenReturnFalse(){
        TransactionDate transactionDateTest = null;

        TransactionDate start = new TransactionDate(transactionDateTest);
        TransactionDate end = new TransactionDate(LocalDate.of(2020,8,3));

        TransactionDate test = new TransactionDate(LocalDate.of(2021,2,3));

        boolean result= test.isBetween(start,end);

        assertFalse(result);
    }*/

    @Test
    void isBetweenNull(){
        TransactionDate transactionDateTest = new TransactionDate(LocalDate.of(2020,5,1));
        TransactionDate end = new TransactionDate(LocalDate.of(2020,1,1));

        assertFalse(transactionDateTest.isBetween(null,end));

    }
}
package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Email;

import static org.junit.jupiter.api.Assertions.*;

class CategoryIDTest {

    @Test
    void sameCategoryID() {
        //Arrange
        String id = "1";
        CategoryID newCategoryID = new CategoryID(id);
        CategoryID categoryIDNull = null;

        //Act + assert
        assertNotEquals(newCategoryID,categoryIDNull);
        assertEquals(newCategoryID, newCategoryID);
        assertNotEquals(null, newCategoryID);
        assertEquals(newCategoryID.hashCode(), newCategoryID.hashCode());
    }
    @Test
    void notSameCategoryIDNorSameClass() {
        //Arrange
        String id = "1";
        String newId = "1.1";
        CategoryID newCategoryID = new CategoryID(id);
        CategoryID anotherCategoryID = new CategoryID(newId);
        Email newEmail = new Email("freunde@bleiben.com");
        //Act
        boolean result = newCategoryID.equals(anotherCategoryID);
        boolean differentClasses = newCategoryID.equals(newEmail);
        assertFalse(result);
        assertFalse(differentClasses);
    }
    @Test
    void toStringCategoryID() {
        //Arrange
        String id = "1";
        CategoryID newCategoryID = new CategoryID(id);
        //Act
        String result = newCategoryID.toString();
       //Assert
        assertEquals(id, result);
    }

}
package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.shared.Email;

import static org.junit.jupiter.api.Assertions.*;

class AccountIDTest {

    @Test
    void sameAccountID() {
        AccountID firstId = new AccountID("1");
        AccountID secondId = new AccountID("1");

        //ACT
        boolean result = firstId.equals(secondId);
        //assert
        assertTrue(result);
        assertEquals(firstId.hashCode(), secondId.hashCode());
    }
    @Test
    void differentAccountIDandDifferentClass() {
        AccountID firstId = new AccountID("1");
        AccountID secondId = new AccountID("6");
        Email email = new Email("dagegen@ichbin.pt");
        //ACT
        boolean result = firstId.equals(secondId);
        boolean differentClasses = firstId.equals(email);
        //assert
        assertFalse(result);
        assertFalse(differentClasses);
    }
    @Test
    void settingAccountID() {
        AccountID firstId = new AccountID("1");
        AccountID secondId = new AccountID();

        //ACT
        secondId.setAccountIDValue("1");
        //assert
        assertEquals(firstId, secondId);

    }

    @Test
    void equals_NullObject() {

        //Arrange
        AccountID accountID = new AccountID("skl");

        //Act
        boolean result = accountID.equals(null);

        //Assert
        assertFalse(result);

    }

    @Test
    void hashCode_Equals() {

        //Arrange
        AccountID accountID_1 = new AccountID("skl");
        AccountID accountID_2 = new AccountID("skl");

        //Act
        int result_1 = accountID_1.hashCode();
        int result_2 = accountID_2.hashCode();

        //Assert
        assertEquals(result_1, result_2);

    }

    @Test
    void hashCode_NotEquals() {

        //Arrange
        AccountID accountID_1 = new AccountID("skl");
        AccountID accountID_2 = new AccountID("sdsds");

        //Act
        int result_1 = accountID_1.hashCode();
        int result_2 = accountID_2.hashCode();

        //Assert
        assertNotEquals(result_1, result_2);

    }

    @Test
    void testToString() {

        //Arrange
        AccountID accountID = new AccountID("1234");
        String expected = "1234";

        //Act
        String result = accountID.toString();

        //Assert
        assertEquals(expected, result);
        assertNotNull(result);
        assertNotEquals("", result);

    }
}
package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class PhoneNumberTest {

    @Test
    @DisplayName("Exception on creating a blank phone number")
    void creatingBlankPhoneNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            PhoneNumber phone = new PhoneNumber("");
        });
    }

    @Test
    @DisplayName("Exception on creating a phone number starting with 3")
    void creatingPhoneNumberStartingWithThree() {

        assertThrows(IllegalArgumentException.class, () -> {
            PhoneNumber phone = new PhoneNumber("326787645");
        });
    }

    @Test
    @DisplayName("Exception on creating a phone number with only seven numbers")
    void creatingPhoneNumberWithSevenNumbers() {

        assertThrows(IllegalArgumentException.class, () -> {
            PhoneNumber phone = new PhoneNumber("9145788");
        });
    }

    @Test
    @DisplayName("Exception on creating a null phone number")
    void creatingPhoneNumberNull() {

        assertThrows(IllegalArgumentException.class, () -> {
            PhoneNumber phone = new PhoneNumber(null);
        });
    }

    @Test
    @DisplayName("Creating a phone number with letters")
    void creatingPhoneNumberWithLetters() {

        assertThrows(IllegalArgumentException.class, () -> {
            PhoneNumber phone = new PhoneNumber("9uhsamsaa");
        });
    }

    @Test
    @DisplayName("Creating a valid phone number")
    void creatingValidPhoneNumber() {

        PhoneNumber phone = new PhoneNumber("212385547");
        assertNotNull(phone);
    }


    @Test
    @DisplayName("Creating a valid phone number with hyphens")
    void creatingValidPhoneNumberWithHyphens() {

        assertThrows(IllegalArgumentException.class, () -> {
            PhoneNumber phone = new PhoneNumber("212-398-547");
        });
    }

    @Test
    @DisplayName("Equals - check not same class")
    void equalsPhoneNumberTestDifferentClass() {

        PhoneNumber phoneNumber = new PhoneNumber("912385547");

        Email emailAddress = new Email("marta@iol.pt");

        assertFalse(phoneNumber.equals(emailAddress));
    }

    @Test
    @DisplayName("Equals - check False")
    void equalsPhoneNumberTestTrue() {

        PhoneNumber phoneNumber = new PhoneNumber("912385547");
        PhoneNumber differentPhoneNumber = new PhoneNumber("911385547");
        PhoneNumber phoneNumberNull = null;

        assertNotEquals(phoneNumber, phoneNumberNull);
        assertNotEquals(phoneNumber, differentPhoneNumber);
        assertNotEquals(phoneNumber.hashCode(), differentPhoneNumber.hashCode());
    }


    @Test
    @DisplayName("Equals - check True")
    void equalsPhoneNumberTestTrue_() {

        PhoneNumber phoneNumber = new PhoneNumber("912385547");

        assertEquals(phoneNumber, phoneNumber);
        assertNotEquals(null, phoneNumber);
        assertEquals(phoneNumber.hashCode(), phoneNumber.hashCode());
    }

    @Test
    @DisplayName("Equals - check True")
    void equalsPhoneNumberButDifferentObjects() {

        PhoneNumber phoneNumber = new PhoneNumber("912385547");
        PhoneNumber second = new PhoneNumber("912385547");
        assertEquals(phoneNumber, second);
    }

    @Test
    @DisplayName("Not Equals - compares phone number with family id")
    void differentClasses() {

        PhoneNumber phoneNumber = new PhoneNumber("912385547");
        FamilyID id = new FamilyID("1");

        assertNotEquals(phoneNumber, id);
    }

    @Test
    @DisplayName("gets phone number")
    void getsPhoneNumber() {

        PhoneNumber phoneNumber = new PhoneNumber("912385547");
        String expected = "912385547";
        String result = phoneNumber.toString();

        assertEquals(expected, result);
    }

}
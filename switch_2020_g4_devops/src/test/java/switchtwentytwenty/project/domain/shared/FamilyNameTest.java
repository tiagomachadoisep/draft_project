package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import static org.junit.jupiter.api.Assertions.*;

class FamilyNameTest {

    @Test
    void FamilyName_Success() {

        //Arrange
        String name = "Carolina";

        //Act
        FamilyName familyName = new FamilyName(name);

        //Assert
        assertNotNull(familyName);

    }

    @Test
    void FamilyName_FailureBecauseNameIsNull() {

        //Act + Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new FamilyName(null);
        });

    }

    @Test
    void FamilyName_FailureBecauseNameIsEmpty() {

        //Act + Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new FamilyName("");
        });

    }

    @Test
    void FamilyName_FailureBecauseNameHasInvalidFormat() {

        //Act + Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new FamilyName("Rodrigo12");
        });

    }

    @Test
    void FamilyName_Equals() {

        //Arrange
        FamilyName expected = new FamilyName("Oliveira");
        FamilyName familyNameNull = null;


        //Assert
        assertNotEquals(expected,familyNameNull);
        assertEquals(expected, expected);
        assertNotEquals(null, expected);
        assertEquals(expected.hashCode(), expected.hashCode());

    }

    @Test
    void FamilyName_NotEquals() {

        //Arrange
        FamilyName expected = new FamilyName("Vasconcelos");

        //Act
        FamilyName familyName = new FamilyName("Oliveira");

        //Assert
        assertNotEquals(expected, familyName);
        assertNotEquals(expected.hashCode(), familyName.hashCode());


    }
    @Test
    void differentClassesComparision() {

        //Arrange
        FamilyName expected = new FamilyName("Vasconcelos");

        //Act
        FamilyID familyName = new FamilyID("1");

        //Assert
        assertNotEquals(expected, familyName);


    }


}
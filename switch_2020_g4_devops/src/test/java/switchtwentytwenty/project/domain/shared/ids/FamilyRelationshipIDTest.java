package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class FamilyRelationshipIDTest {

    @Test
    void validFamilyRelationshipID() {
        //Arrange
        String id = "14efr";
        //Act
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID(id);
        //Assert
        assertNotNull(familyRelationshipID);
        assertEquals(familyRelationshipID.getRelationshipId(), id);

    }

    @Test
    void invalidFamilyRelationshipID_nullID() {

        String id = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID(id);
        });

    }

    @Test
    void invalidFamilyRelationshipID_emptyID() {

        String id = "";

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID(id);
        });

    }

    @Test
    void validFamilyRelationshipID_emptyConstructorSuccess() {
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID();

        assertNotNull(familyRelationshipID);
        assertNotNull(familyRelationshipID.getRelationshipId());
    }

    @Test
    void setRelationshipId() {

        String idExpected = "anId_22132";

        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID();
        familyRelationshipID.setRelationshipId("anId_22132");

        String idResult = familyRelationshipID.getRelationshipId();

        assertEquals(idExpected, idResult);
        assertNotNull(idResult);

    }

    @Test
    void testingEqualsAndHashCode() {
        //Arrange
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("anID");
        FamilyRelationshipID familyRelationshipIDOther = new FamilyRelationshipID("anID");
        FamilyRelationshipID differentID = new FamilyRelationshipID("otherID");
        FamilyRelationshipID nullID = null;
        PersonID differentClass = new PersonID(new Email("ole@gmail.com"));

        assertEquals(familyRelationshipID, familyRelationshipID);
        assertEquals(familyRelationshipID.hashCode(), familyRelationshipID.hashCode());

        assertEquals(familyRelationshipID, familyRelationshipIDOther);
        assertEquals(familyRelationshipID.hashCode(), familyRelationshipIDOther.hashCode());

        assertNotEquals(familyRelationshipID, differentID);
        assertNotEquals(familyRelationshipID.hashCode(), differentID.hashCode());

        assertNotEquals(familyRelationshipID, differentClass);
        assertNotEquals(familyRelationshipID.hashCode(), differentClass.hashCode());

        assertNotEquals(familyRelationshipID, nullID);


    }

}
package switchtwentytwenty.project.domain.shared;

import javassist.runtime.Desc;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import static org.junit.jupiter.api.Assertions.*;

class DescriptionTest {

    @Test
    void validDescription() {
        //Arrange
        String descriptionToBe = "Totta";
        //Act
        Description accountDescription = new Description(descriptionToBe);
        //Assert
        assertNotNull(accountDescription);
    }

    @Test
    void nullDescription() {
        //Arrange

        //Act + assert
        assertThrows(InvalidDesignationException.class, () -> {
            Description accountDescription = new Description(null);
        });
    }
    @Test
    void emptyDescription() {
        //Arrange
        String descriptionToBe = "     ";
        //Act + assert
        assertThrows(InvalidDesignationException.class, () -> {
            Description accountDescription = new Description(descriptionToBe);
        });
    }

    @Test
    void anotherEmptyDescription() {
        //Arrange
        String descriptionToBe = "";
        //Act + assert
        assertThrows(InvalidDesignationException.class, () -> {
            Description accountDescription = new Description(descriptionToBe);
        });
    }

    @Test
    void descriptionWithOneLetter() {
        //Arrange
        String descriptionToBe = "e    ";
        //Act + assert
        assertThrows(InvalidDesignationException.class, () -> {
            Description accountDescription = new Description(descriptionToBe);
        });
    }

    @Test
    void descriptionWithThreeLetters() {
        //Arrange
        String descriptionToBe = "kia";
        //Act
        Description accountDescription = new Description(descriptionToBe);
        //assert
        assertNotNull(accountDescription);
    }
    @Test
    void descriptionToString() {
        //Arrange
        String descriptionToBe = "Montepio";
        Description accountDescription = new Description(descriptionToBe);
        //Act
        String actualDescription = accountDescription.toString();
        //Assert
        assertEquals(descriptionToBe,actualDescription);


    }
    @Test
    void descriptionEqualsTesting() {
        //Arrange
        String descriptionToBe = "Montepio";
        Description accountDescription = new Description(descriptionToBe);
        Description anotherAccountDescription = new Description(descriptionToBe);
        Email emailAddress = new Email("testing@something.com");
        //Act
        boolean result = accountDescription.equals(anotherAccountDescription);
        boolean differentClasses = accountDescription.equals(emailAddress);
        //Assert
        assertTrue(result);
        assertEquals(accountDescription.hashCode(),anotherAccountDescription.hashCode());
        assertFalse(differentClasses);

    }
    @Test
    void descriptionNotEqualsTesting() {
        //Arrange
        String descriptionToBe = "Montepio";
        Description accountDescription = new Description(descriptionToBe);
        Description anotherAccountDescription = new Description("ActivoBank");
        //Act
        boolean result = accountDescription.equals(anotherAccountDescription);
        //Assert
        assertFalse(result);
        assertNotEquals(accountDescription.hashCode(),anotherAccountDescription.hashCode());


    }

    @Test
    void sameDescriptionTesting() {
        //Arrange
        String descriptionToBe = "Montepio";
        Description accountDescription = new Description(descriptionToBe);

        //Assert
        assertEquals(accountDescription, accountDescription);
        assertNotEquals(null, accountDescription);
        assertEquals(accountDescription.hashCode(), accountDescription.hashCode());

    }

    @Test
    void nullObjectDescriptionTesting() {

        //Arrange
        String descriptionString = "My account";
        Description description = new Description(descriptionString);

        //Act
        boolean result = description.equals(null);

        //Assert
        assertFalse(result);

    }

}
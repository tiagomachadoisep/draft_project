package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import static org.junit.jupiter.api.Assertions.*;


class PersonNameTest {

    @Test
    void validPersonName() {
        //ARRANGE
        String personNameToBe = "Tom";
        //ACT
        PersonName personName = new PersonName(personNameToBe);
        //ASSERT
        assertNotNull(personName);
    }

    @Test
    void invalidPersonNameFormat() {
        //ARRANGE
        String personNameToBe = "Tom123";
        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            PersonName personName = new PersonName(personNameToBe);
        });


    }

    @Test
    void nullPersonName() {
        //ARRANGE

        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            PersonName personName = new PersonName(null);
        });


    }

    @Test
    void emptyPersonName() {
        //ARRANGE
        String personNameToBe = "       ";
        //ACT + ASSERT
        assertThrows(InvalidDesignationException.class, () -> {
            PersonName personName = new PersonName(personNameToBe);
        });

    }

    @Test
    void toStringPersonName() {
        //ARRANGE
        String personNameToBe = "Switch";
        PersonName personName = new PersonName(personNameToBe);
        //ACT
        String actualName = personName.toString();
        //ASSERT
        assertEquals(personNameToBe, actualName);


    }
    @Test
    void equalsTruePersonName() {
        //ARRANGE
        String personNameToBe = "Switch";
        PersonName personName = new PersonName(personNameToBe);
        PersonName personNameNull = null;

        //ASSERT
        assertNotEquals(personName,personNameNull);
        assertEquals(personName, personName);
        assertNotEquals(null, personName);
        assertEquals(personName.hashCode(), personName.hashCode());

    }

    @Test
    void differentObjectsButSamePersonName() {
        //ARRANGE
        String personNameToBe = "Switch";
        PersonName personName = new PersonName(personNameToBe);
        PersonName secondPersonName = new PersonName(personNameToBe);
        //ASSERT
        assertEquals(secondPersonName, personName);

    }

    @Test
    void notEqualsTruePersonName() {
        //ARRANGE
        String personNameToBe = "Switch";
        String anotherName = "Jetzt";
        PersonName personName = new PersonName(personNameToBe);
        PersonName secondPersonName = new PersonName(anotherName);
        Email email = new Email("amanhatoumelhor@hotmail.com");
        //ACT
        boolean sameName = personName.equals(secondPersonName);
        boolean differentClasses = personName.equals(email);
        //ASSERT
        assertFalse(sameName);
        assertFalse(differentClasses);
        assertNotEquals(personName.hashCode(), secondPersonName.hashCode());


    }

}
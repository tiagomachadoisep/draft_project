package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.exceptions.InvalidDateExpressionException;

import static org.junit.jupiter.api.Assertions.*;

class RegistrationDateTest {

    @Test
    void RegistrationDateConstructor() {

        //Arrange
        String date = "12/02/2021";

        //Act
        RegistrationDate registrationDate = new RegistrationDate(date);

        //Assert
        assertNotNull(registrationDate);

    }
    @Test
    void nullRegistrationDateConstructor() {

        assertThrows(InvalidDateExpressionException.class, () -> {
            RegistrationDate reg = new RegistrationDate(null);
        });

    }
    @Test
    void emptyRegistrationDateConstructor() {

        assertThrows(InvalidDateExpressionException.class, () -> {
            RegistrationDate reg = new RegistrationDate(" ");
        });

    }

    @Test
    void EqualsRegistrationDate() {

       RegistrationDate reg = new RegistrationDate("12.12.12");

       assertEquals(reg, reg);
       assertNotEquals(null, reg);
       assertEquals(reg.hashCode(), reg.hashCode());
    }

    @Test
    void notEqualsRegistrationDateWithDifferentClass() {

        RegistrationDate reg = new RegistrationDate("12.12.12");
        RegistrationDate regTwo = new RegistrationDate("12.11.14");
        RegistrationDate regThree = new RegistrationDate("12.12.12");
        CategoryID id = new CategoryID("1");
        RegistrationDate regNull = null;

        assertNotEquals(id, reg);
        assertEquals(reg,regThree);
        assertNotEquals(reg, regTwo);
        assertNotEquals(reg,regNull);
    }

    @Test
    void hash(){
        RegistrationDate reg = new RegistrationDate("12.12.12");

        assertEquals(-2007366880,reg.hashCode());
    }

    @Test
    void testtoString(){
        RegistrationDate reg = new RegistrationDate("12.12.12");
        String expected = "12.12.12";
        assertEquals(expected,reg.toString());

    }


}
package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransactionIDTest {

    @Test
    void getAndSetTransactionID() {

        String transaction = "M890";
        TransactionID id = new TransactionID();
        id.setId(transaction);

        assertEquals(transaction, id.getId());
        assertEquals(transaction,id.toString());
    }

    @Test
    void testingEqualsAndHashcodeDifferentTransactionId() {
        String transaction = "M890";
        String nextTransaction = "8923642bds";
        TransactionID id = new TransactionID(transaction);
        TransactionID nextId = new TransactionID(nextTransaction);

        assertNotEquals(id,nextId);
        assertNotEquals(id.hashCode(), nextId.hashCode());

    }

    @Test
    void testingEqualsAndHashcodeSameTransactionId() {
        String transaction = "M890";
        TransactionID id = new TransactionID(transaction);
        TransactionID nextId = new TransactionID(transaction);

        assertEquals(id,nextId);
        assertEquals(id.hashCode(), nextId.hashCode());

    }

}
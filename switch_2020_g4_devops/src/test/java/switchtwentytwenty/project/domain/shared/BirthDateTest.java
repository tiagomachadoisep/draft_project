package switchtwentytwenty.project.domain.shared;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BirthDateTest {

    @Test
    void testBirthDate_ToString() {
        String date = "01-01-1911";

        BirthDate birthDate = new BirthDate(date);

        String result = birthDate.toString();

        assertEquals(date, result);
    }

    @Test
    void testBirthDate_validateDate(){
        BirthDate birthDate = new BirthDate("01-01-2001");

        assertNotNull(birthDate);

    }

    @Test
    void validateDate(){
        assertThrows(IllegalArgumentException.class,()->{new BirthDate("error");});
    }
}
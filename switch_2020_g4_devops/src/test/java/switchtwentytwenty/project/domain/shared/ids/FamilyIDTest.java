package switchtwentytwenty.project.domain.shared.ids;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class FamilyIDTest {

    @Test
    void FamilyIDConstructor() {

        //Arrange
        String id = "5";

        //Act
        FamilyID familyID = new FamilyID(id);
        FamilyID nullButNotNullID = new FamilyID();

        //Assert
        assertNotNull(familyID);
        assertNotNull(nullButNotNullID);
        assertEquals("5",familyID.getFamilyIdValue());

    }

    @Test
    void FamilyID_Equals() {

        //Arrange
        FamilyID expected = new FamilyID("6");

        //Act
        FamilyID familyID = new FamilyID("6");

        //Assert
        assertEquals(expected, familyID);
        assertNotEquals(null, expected);
        assertEquals(expected.hashCode(), familyID.hashCode());

    }

    @Test
    void FamilyID_NotEqualsToCategoryID() {

        //Arrange
        FamilyID expected = new FamilyID("9");

        //Act
        CategoryID cat = new CategoryID("1");

        //Assert
        assertNotEquals(expected, cat);

    }
    @Test
    void FamilyID_setFamilyIDAndToString() {

        //Arrange
        String expectedString = "9";
        FamilyID expected = new FamilyID("9");
        FamilyID setID = new FamilyID();
        FamilyID expectedAfterSettingID = new FamilyID("6");

        //Act
        String actualString = expected.toString();
        setID.setFamilyIdValue("6");

        //Assert
        assertEquals(expectedAfterSettingID, setID);
        assertEquals(expectedString,actualString);

    }

    @Test
    void FamilyID_NotEquals() {

        //Arrange
        FamilyID expected = new FamilyID("9");

        //Act
        FamilyID familyID = new FamilyID("6");

        //Assert
        assertNotEquals(expected, familyID);

    }
    @Test
    void FamilyID_NotAPerson() {

        //Arrange
        FamilyID expected = new FamilyID("9");

        //Act
        boolean result = expected.isPerson();

        //Assert
        assertFalse(result);

    }

    @Test
    void setFamilyIdValue_nullString() {
        String nullString = null;

        FamilyID familyID = new FamilyID();

        Assertions.assertThrows(NullArgumentException.class, () -> {
            familyID.setFamilyIdValue(nullString);
        });
    }

    @Test
    void setFamilyIdValue_emptyString() {
        String emptyString = "";

        FamilyID familyID = new FamilyID();

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            familyID.setFamilyIdValue(emptyString);
        });
    }

    @Test
    void FamilyId_ArgsConstructor_nullString() {

        String nullString = null;

        Assertions.assertThrows(NullArgumentException.class, () -> {
            FamilyID familyID = new FamilyID(nullString);
        });
    }

    @Test
    void FamilyId_ArgsConstructor_emptyString() {

        String emptyString = "";

        Assertions.assertThrows(EmptyArgumentException.class, () -> {
            FamilyID familyID = new FamilyID(emptyString);
        });
    }


}
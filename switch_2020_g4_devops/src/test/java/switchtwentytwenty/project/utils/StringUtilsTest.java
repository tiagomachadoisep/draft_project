package switchtwentytwenty.project.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class StringUtilsTest {

    @Test
    void nullStringCapitalize() {
        String expected = StringUtils.capitalize(null);

        assertNull(expected);
    }

    @Test
    void nullStringCamelCase() {
        String expected = StringUtils.toCamelCase(null);

        assertNull(expected);
    }

    @Test
    void camelCase(){
        String example = "my_camel_case";
        String expected = "MyCamelCase";
        String result = StringUtils.toCamelCase(example);
        assertEquals(expected,result);
    }

}
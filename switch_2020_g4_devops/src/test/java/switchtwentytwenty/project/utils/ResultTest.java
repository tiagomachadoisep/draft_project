package switchtwentytwenty.project.utils;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;

import static org.junit.jupiter.api.Assertions.*;


class ResultTest {

    @Test
    void equalsResultContent() {

        Result result = null;
        Result test = Result.success("1");

        assertNotEquals(result, test);
        assertNotEquals(null, Result.success());
        assertEquals(Result.success(), Result.success());
    }

    @Test
    void equalsResultStatus() {
        assertNotEquals(Result.failure("something"), Result.success());
    }

    @Test
    void equalsSameObject() {
        Result result = Result.success("1");
        Result resultTwo = Result.success("1");

        assertEquals(result, result);
        assertEquals(result, resultTwo);
    }

    @Test
    void hashCodeTest() {

        assertEquals(Result.success().hashCode(), Result.success().hashCode());
        assertEquals(Result.success("2").hashCode(), Result.success("2").hashCode());
        assertNotEquals(Result.success("2").hashCode(), Result.success("4").hashCode());

    }

    @Test
    void testNullContent() {
        Result test = Result.success(null);
        Result testTwo = Result.success("3");
        Result testThree = Result.success("503");
        Family family = new Family.Builder(new FamilyID("1")).build();
        Family family2 = new Family.Builder(new FamilyID("2")).build();

        assertTrue(family2.equals(family2));
        assertFalse(testThree.equals(testTwo));
        assertFalse(testTwo.equals(family));
        assertNotEquals(testTwo.getClass(), family.getClass());
        assertNotEquals(test, testTwo);
        assertNotEquals(testTwo, testThree);
        assertNotEquals(testThree, family);

    }


    @Test
    void resultComparingWithDifferentClass() {

        assertNotEquals(Result.success(), new FamilyID().toString());
    }

    @Test
    void resultWithSameContent() {
        //ARRANGE
        String expected = "content not blank";

        //ACT
        String actual = Result.success("content not blank").getContent();
        boolean result = expected.equals(actual);
        //ASSERT
        assertTrue(result);
    }

    @Test
    void resultWithDifferentContent() {
        //ARRANGE
        String expected = "content blank";

        //ACT
        String actual = Result.success("content not blank").getContent();
        boolean result = expected.equals(actual);
        //ASSERT
        assertFalse(result);
    }

    @Test
    void failureWithDifferentContent() {
        //ARRANGE
        Result one = Result.failure("something");
        Result two = Result.failure("not blank");

        //ACT
        boolean result = one.getContent().equals(two.getContent());
        //ASSERT
        assertFalse(result);
        assertNotEquals(one.hashCode(), two.hashCode());
    }

    @Test
    void nullAndDifferentClass() {
        Result resultSuccess = Result.success();
        Result resultFailure = Result.failure("lol");
        Result resultNull = null;
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID("123");

        assertNotEquals(resultSuccess, resultNull);
        assertNotEquals(resultFailure, resultNull);
        assertNotEquals(resultSuccess, familyRelationshipID);
        assertNotEquals(resultFailure, familyRelationshipID);

    }
}
package switchtwentytwenty.project.securities;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.parameters.P;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import javax.xml.bind.ValidationException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class UserControllerTest {

    private IPersonRepositoryJPA repositoryJPA = Mockito.mock(IPersonRepositoryJPA.class);
    private UserController controller = new UserController(repositoryJPA);

    @Test
    void create() throws ValidationException, NoSuchAlgorithmException {
        String username = "ze@isep.pt";
        String fullname = "ze silva";
        String password = "1234";

        Map<String,String> request = new HashMap<>();
        request.put("username",username);
        request.put("password",password);
        request.put("fullname",fullname);

        Mockito.when(repositoryJPA.existsById(new PersonID(new Email(username)))).thenReturn(false);

        assertTrue(controller.create(request));
    }

    @Test
    void createError() throws ValidationException, NoSuchAlgorithmException {
        String username = "ze@isep.pt";
        String fullname = "ze silva";
        String password = "1234";

        Map<String,String> request = new HashMap<>();
        request.put("username",username);
        request.put("password",password);
        request.put("fullname",fullname);

        Mockito.when(repositoryJPA.existsById(new PersonID(new Email(username)))).thenReturn(true);

        assertThrows(ValidationException.class,()->{controller.create(request);});
    }
}
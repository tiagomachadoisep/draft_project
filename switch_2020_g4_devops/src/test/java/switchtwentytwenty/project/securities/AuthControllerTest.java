package switchtwentytwenty.project.securities;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;


class AuthControllerTest {


    private AuthenticationManager manager = Mockito.mock(AuthenticationManager.class);

    private JwtToken token= Mockito.mock(JwtToken.class);

    private JwtUserDetailsService serviceMock = Mockito.mock(JwtUserDetailsService.class);

    private AuthController controller= new AuthController(manager,token,serviceMock);

    @Test
    void createAuthenticationToken() throws Exception {
        String username = "ze@isep.pt";
        String password = new BCryptPasswordEncoder().encode( "1234");
        JwtRequest request = new JwtRequest(username,password);

        User user = new User(username,password,new ArrayList<>());
        Mockito.when(serviceMock.loadUserByUsername(username)).thenReturn(user);
        Mockito.when(token.generateToken(user)).thenReturn("token");
        Mockito.when(manager.authenticate(new UsernamePasswordAuthenticationToken(username,password))).thenReturn(new UsernamePasswordAuthenticationToken(username,password));

        JwtResponse jwtResponse = new JwtResponse("token");

        ResponseEntity result = controller.createAuthenticationToken(request);
        JwtResponse body = (JwtResponse) result.getBody();

        assertEquals(jwtResponse.getToken(),body.getToken());


    }

    @Test
    void createAuthenticationTokenException() throws Exception {
        String username = "ze@isep.pt";
        String password = new BCryptPasswordEncoder().encode( "1234");
        JwtRequest request = new JwtRequest(username,password);

        User user = new User(username,password,new ArrayList<>());
        Mockito.when(serviceMock.loadUserByUsername(username)).thenReturn(user);
        Mockito.when(token.generateToken(user)).thenReturn("token");
        Mockito.when(manager.authenticate(new UsernamePasswordAuthenticationToken(username,password))).thenThrow(new DisabledException("disabled"));

        assertThrows(Exception.class,()->{controller.createAuthenticationToken(request);});


    }

    @Test
    void createAuthenticationTokenExceptionOther() throws Exception {
        String username = "ze@isep.pt";
        String password = new BCryptPasswordEncoder().encode( "1234");
        JwtRequest request = new JwtRequest(username,password);

        User user = new User(username,password,new ArrayList<>());
        Mockito.when(serviceMock.loadUserByUsername(username)).thenReturn(user);
        Mockito.when(token.generateToken(user)).thenReturn("token");
        Mockito.when(manager.authenticate(new UsernamePasswordAuthenticationToken(username,password))).thenThrow(new BadCredentialsException("bad"));

        assertThrows(Exception.class,()->{controller.createAuthenticationToken(request);});


    }

    @Test
    void getCurrentUserInfo() {
        String username = "ze@isep.pt";
        String password = "1234";

        Authentication authentication = new UsernamePasswordAuthenticationToken(username,password);
        ResponseEntity<?> responseEntity =  controller.getCurrentUserInfo(authentication);

        UserDTO body = (UserDTO) responseEntity.getBody();

        UserDTO expected = new UserDTO(username,new ArrayList<>());

        assertEquals(expected,body);
    }
}
package switchtwentytwenty.project.securities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class JwtUserDetailsServiceTest {

    @Mock
    private IPersonRepositoryJPA repositoryJPA;
    @InjectMocks
    private JwtUserDetailsService service;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void loadUser(){
        String email = "ze@isep.pt";
        Mockito.when(repositoryJPA.findById(new PersonID(new Email(email)))).thenReturn(Optional.of(new PersonJPA(email,"1","ze",0,"outro")));

        UserDetails user = service.loadUserByUsername(email);

        User expected = new User(email,new BCryptPasswordEncoder().encode(String.valueOf(0)),new ArrayList<>());

        assertEquals(expected,user);
    }

    @Test
    void loadUserNotExist(){
        String email = "ze@isep.pt";
        Mockito.when(repositoryJPA.findById(new PersonID(new Email(email)))).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class,()->{service.loadUserByUsername(email);});
    }
}
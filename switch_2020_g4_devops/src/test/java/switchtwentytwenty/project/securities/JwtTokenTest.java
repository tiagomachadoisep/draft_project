package switchtwentytwenty.project.securities;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;


class JwtTokenTest {

    @Test
    void getUsernameFromToken(){
        String username= "ze@isep.pt";
        int password = 1234;
        String encodedPassword = new BCryptPasswordEncoder().encode(String.valueOf(password));
        String role = "ADMIN";
        UserDetails user = new User(username,encodedPassword, Collections.singletonList(new SimpleGrantedAuthority(role)));

        JwtToken jwtToken = new JwtToken();
        jwtToken.setSecret("secret");
        jwtToken.setJWT_TOKEN_VALIDITY(1000000);

        String token = jwtToken.generateToken(user);
        String obtainUsername= jwtToken.getUsernameFromToken(token);

        assertNotNull(token);
        assertEquals(username,obtainUsername);
        assertTrue(jwtToken.validateToken(token,user));

    }

}
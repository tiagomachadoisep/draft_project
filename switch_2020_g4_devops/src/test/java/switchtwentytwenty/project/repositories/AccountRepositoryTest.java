package switchtwentytwenty.project.repositories;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.domain.aggregates.account.*;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IAccountAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class AccountRepositoryTest {

    private final IAccountRepositoryJPA iAccountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
    private final IAccountAssemblerJPA iAccountAssemblerJPAMock = Mockito.mock(IAccountAssemblerJPA.class);
    private final AccountRepository accountRepository = new AccountRepository(iAccountAssemblerJPAMock,
            iAccountRepositoryJPAMock);

    @Test
    void withAccountID_FindIDJPA() {
        AccountID accountID = new AccountID("3");

        AccountJPA accountJPA = new AccountJPA(accountID,
                new PersonID(new Email("howl@mc.sg")), "CC howl", 0, EAccountType.CREDIT_CARD);
        Account account = new CreditCardAccount.Builder(accountJPA.getAccountID()).setOwnerID(accountJPA.getPersonID()).
                setDescription(new Description(accountJPA.getDescription())).setBalance(new MoneyValue(accountJPA.getBalance())).setAccountType().build();

        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(accountID)).thenReturn(accountJPA);
        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA)).thenReturn(accountID);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA)).thenReturn(EAccountType.CREDIT_CARD);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA)).thenReturn(new MoneyValue(0));
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA)).thenReturn(new Description(
                "CC howl"));
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA)).thenReturn(new PersonID(new Email("howl@mc.sg")));

        Optional<Account> result = accountRepository.findByID(accountID);

        assertEquals(account.getAccountType(), result.get().getAccountType());
        assertEquals(account.getBalance(), result.get().getBalance());
        assertEquals(account.getDescription(), result.get().getDescription());
        assertEquals(account.getID(), result.get().getID());
        assertEquals(account.getOwnerID(), result.get().getOwnerID());
    }

    @Test
    void findByID_IDDoesNotExist() {

        //Arrange
        AccountID accountID = new AccountID("44");

        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(accountID)).thenReturn(null);

        //Act
        Optional<Account> result = accountRepository.findByID(accountID);

        //Assert
        assertFalse(result.isPresent());

    }

    @Test
    void checkIfIDExists_inPersistence_False() {
        AccountID accountID = new AccountID("20");
        Mockito.when(iAccountRepositoryJPAMock.existsById(accountID)).thenReturn(false);

        boolean result = accountRepository.existsID(accountID);

        assertFalse(result);
    }


    @Test
    void checkIfIDExists_inPersistence_True() {
        AccountID accountID = new AccountID("20");
        Mockito.when(iAccountRepositoryJPAMock.existsById(accountID)).thenReturn(true);

        boolean result = accountRepository.existsID(accountID);

        assertTrue(result);
    }

    @Test
    void hasPersonACashAccount_ReturnsTrue() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        String description = "My cash account";
        double balance = 100;
        AccountID accountID = new AccountID("6");

        AccountJPA personalCashAccountJPA = new AccountJPA();
        personalCashAccountJPA.setAccountID(accountID);
        personalCashAccountJPA.setDescription(description);
        personalCashAccountJPA.setBalance(balance);
        personalCashAccountJPA.setPersonID(personID);
        personalCashAccountJPA.setAccountType(EAccountType.PERSONAL_CASH);

        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(personalCashAccountJPA);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(accounts);

        //Act
        boolean result = accountRepository.hasPersonACashAccount(personID);

        //Assert
        assertTrue(result);

    }

    @Test
    void hasPersonACashAccount_PersonHasNoAccounts_ReturnsFalse() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(new ArrayList<>());

        //Act
        boolean result = accountRepository.hasPersonACashAccount(personID);

        //Assert
        assertFalse(result);

    }

    @Test
    void hasPersonACashAccount_PersonHasAccountsButNotCashAccount_ReturnsFalse() {

        //Arrange
        PersonID personID = new PersonID(new Email("filipa@gmail.com"));
        String description = "My cash account";
        double balance = 100;
        AccountID accountID = new AccountID("6");

        AccountJPA personalCashAccountJPA = new AccountJPA();
        personalCashAccountJPA.setAccountID(accountID);
        personalCashAccountJPA.setDescription(description);
        personalCashAccountJPA.setBalance(balance);
        personalCashAccountJPA.setPersonID(personID);
        personalCashAccountJPA.setAccountType(EAccountType.BANK);

        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(personalCashAccountJPA);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(accounts);

        //Act
        boolean result = accountRepository.hasPersonACashAccount(personID);

        //Assert
        assertFalse(result);

    }

    @Test
    void findCashByOwnerId_FindsCash() {

        //Arrange
        PersonID personID = new PersonID(new Email("joana@gmail.com"));
        AccountID accountID = new AccountID("124j");
        MoneyValue balance = new MoneyValue(1000);
        Description description = new Description("cash account");
        Account expected =
                new PersonalCashAccount.Builder(accountID).setAccountType().setBalance(balance).setDescription(description).setOwnerID(personID).build();

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);

        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(accounts);
        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA)).thenReturn(accountID);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA)).thenReturn(EAccountType.PERSONAL_CASH);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA)).thenReturn(balance);
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA)).thenReturn(description);
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA)).thenReturn(personID);

        //Act
        Optional<Account> result = accountRepository.findCashByOwnerId(personID);

        //Assert
        assertEquals(expected, result.get());

    }

    @Test
    void findCashByOwnerId_DoesNotFindCash() {

        //Arrange
        PersonID personID = new PersonID(new Email("joana@gmail.com"));
        AccountID accountID = new AccountID("124j");
        MoneyValue balance = new MoneyValue(1000);
        Description description = new Description("cash account");

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.CREDIT_CARD);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);

        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(accounts);

        //Act
        Optional<Account> result = accountRepository.findCashByOwnerId(personID);

        //Assert
        assertFalse(result.isPresent());

    }

    @Test
    void findFamilyCashByOwnerId_FindsFamilyCash() {

        //Arrange
        PersonID personID = new PersonID(new Email("joana@gmail.com"));
        AccountID accountID = new AccountID("124j");
        MoneyValue balance = new MoneyValue(1000);
        Description description = new Description("family cash account");
        Account expected =
                new FamilyCashAccount.Builder(accountID).setAccountType().setBalance(balance).setDescription(description).setOwnerID(personID).build();

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.FAMILY_CASH);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);

        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(accounts);
        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA)).thenReturn(accountID);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA)).thenReturn(EAccountType.FAMILY_CASH);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA)).thenReturn(balance);
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA)).thenReturn(description);
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA)).thenReturn(personID);

        //Act
        Optional<Account> result = accountRepository.findFamilyCashByOwnerId(personID);

        //Assert
        assertEquals(expected, result.get());

    }

    @Test
    void findFamilyCashByOwnerId_DoesNotFindFamilyCash() {

        //Arrange
        PersonID personID = new PersonID(new Email("joana@gmail.com"));
        AccountID accountID = new AccountID("124j");
        MoneyValue balance = new MoneyValue(1000);
        Description description = new Description("family cash account");

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.CREDIT_CARD);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);

        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(accounts);

        //Act
        Optional<Account> result = accountRepository.findCashByOwnerId(personID);

        //Assert
        assertFalse(result.isPresent());

    }

    @Test
    void toEntity_BankAccount() {

        //Arrange
        AccountID accountID = new AccountID("kosdos02");
        MoneyValue balance = new MoneyValue(0);
        Description description = new Description("bank account");
        PersonID personID = new PersonID(new Email("joana@gmail.com"));

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.BANK);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);

        Account expected =
                new BankAccount.Builder(accountID).setAccountType().setBalance(balance).setDescription(description).setOwnerID(personID).build();

        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA)).thenReturn(accountID);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA)).thenReturn(EAccountType.BANK);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA)).thenReturn(balance);
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA)).thenReturn(description);
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA)).thenReturn(personID);

        //Act
        Account result = accountRepository.toEntity(accountJPA);

        //Assert
        assertEquals(expected.getAccountType(), result.getAccountType());
        assertEquals(expected.getBalance(), result.getBalance());
        assertEquals(expected.getDescription(), result.getDescription());
        assertEquals(expected.getID(), result.getID());
        assertEquals(expected.getOwnerID(), result.getOwnerID());

    }

    @Test
    void toEntity_BankSavingsAccount() {

        //Arrange
        AccountID accountID = new AccountID("kosdos02");
        MoneyValue balance = new MoneyValue(0);
        Description description = new Description("bank savings account");
        PersonID personID = new PersonID(new Email("joana@gmail.com"));

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.BANK_SAVINGS);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);

        Account expected =
                new BankSavingsAccount.Builder(accountID).setAccountType().setBalance(balance).setDescription(description).setOwnerID(personID).build();

        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA)).thenReturn(accountID);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA)).thenReturn(EAccountType.BANK_SAVINGS);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA)).thenReturn(balance);
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA)).thenReturn(description);
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA)).thenReturn(personID);

        //Act
        Account result = accountRepository.toEntity(accountJPA);

        //Assert
        assertEquals(expected.getAccountType(), result.getAccountType());
        assertEquals(expected.getBalance(), result.getBalance());
        assertEquals(expected.getDescription(), result.getDescription());
        assertEquals(expected.getID(), result.getID());
        assertEquals(expected.getOwnerID(), result.getOwnerID());

    }

    @Test
    void assembleToAccountList() {

        //Arrange
        List<AccountJPA> accountJPAList = new ArrayList<>();
        AccountID accountID = new AccountID("123");
        MoneyValue balance = new MoneyValue(0);
        Description description = new Description("Bank account");
        AccountID accountID_2 = new AccountID("1234");
        Description description_2 = new Description("Bank savings account");
        PersonID personID = new PersonID(new Email("vasco@isep.pt"));

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(accountID);
        accountJPA.setAccountType(EAccountType.BANK);
        accountJPA.setBalance(balance.getValue());
        accountJPA.setDescription(description.toString());
        accountJPA.setPersonID(personID);
        AccountJPA accountJPA_2 = new AccountJPA();
        accountJPA_2.setAccountID(accountID_2);
        accountJPA_2.setAccountType(EAccountType.BANK_SAVINGS);
        accountJPA_2.setBalance(balance.getValue());
        accountJPA_2.setDescription(description_2.toString());
        accountJPA_2.setPersonID(personID);
        accountJPAList.add(accountJPA);
        accountJPAList.add(accountJPA_2);

        Account account =
                new BankAccount.Builder(accountID).setAccountType().setBalance(balance).setDescription(description).setOwnerID(personID).build();
        Account account_2 =
                new BankSavingsAccount.Builder(accountID_2).setAccountType().setBalance(balance).setDescription(description_2).setOwnerID(personID).build();
        List<Account> accountList = new ArrayList<>();
        accountList.add(account);
        accountList.add(account_2);

        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA)).thenReturn(accountID);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA)).thenReturn(EAccountType.BANK);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA)).thenReturn(balance);
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA)).thenReturn(description);
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA)).thenReturn(personID);

        Mockito.when(iAccountAssemblerJPAMock.getAccountIDFromAccountJPA(accountJPA_2)).thenReturn(accountID_2);
        Mockito.when(iAccountAssemblerJPAMock.getAccountTypeFromAccountJPA(accountJPA_2)).thenReturn(EAccountType.BANK_SAVINGS);
        Mockito.when(iAccountAssemblerJPAMock.getBalanceFromAccountJPA(accountJPA_2)).thenReturn(balance);
        Mockito.when(iAccountAssemblerJPAMock.getDescriptionFromAccountJPA(accountJPA_2)).thenReturn(description_2);
        Mockito.when(iAccountAssemblerJPAMock.getPersonIDFromAccountJPA(accountJPA_2)).thenReturn(personID);

        //Act
        List<Account> result = accountRepository.assembleToAccountList(accountJPAList);

        //Assert
        assertEquals(accountList.get(0).getAccountType(), result.get(0).getAccountType());
        assertEquals(accountList.get(0).getBalance(), result.get(0).getBalance());
        assertEquals(accountList.get(0).getDescription(), result.get(0).getDescription());
        assertEquals(accountList.get(0).getID(), result.get(0).getID());
        assertEquals(accountList.get(0).getOwnerID(), result.get(0).getOwnerID());
        assertEquals(accountList.get(1).getAccountType(), result.get(1).getAccountType());
        assertEquals(accountList.get(1).getBalance(), result.get(1).getBalance());
        assertEquals(accountList.get(1).getDescription(), result.get(1).getDescription());
        assertEquals(accountList.get(1).getID(), result.get(1).getID());
        assertEquals(accountList.get(1).getOwnerID(), result.get(1).getOwnerID());

    }

    @Test
    void tryToSaveNewAccount_Test() {
        AccountID accountID = new AccountID("1");
        Account account = new CreditCardAccount.Builder(accountID)
                .setBalance(new MoneyValue(200)).setDescription(new Description("test"))
                .setOwnerID(new PersonID(new Email("luis@isep.pt"))).setAccountType().build();

        accountRepository.saveNew(account);

        Mockito.when(iAccountRepositoryJPAMock.existsById(accountID)).thenReturn(true);

        assertTrue(accountRepository.existsID(new AccountID("1")));
    }

    @Test
    @DisplayName("Success - get a cash account from person or family.")
    void getFamilyOrMemberCashAccount_success() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("sanda@hotmail.com"));
        Account account = new PersonalCashAccount.Builder(new AccountID("1"))
                .setOwnerID(new PersonID(new Email("sanda@hotmail.com")))
                .setDescription(new Description("My new account"))
                .setBalance(new MoneyValue(2080)).setAccountType().build();

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("1"));
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(2080);
        accountJPA.setDescription("My new account");
        accountJPA.setPersonID(ownerId);

        List<AccountJPA> accountJPAS = new ArrayList<>();

        Mockito.when(iAccountAssemblerJPAMock.toJPA(account)).thenReturn(accountJPA);
        accountJPAS.add(accountJPA);
        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(ownerId)).thenReturn(accountJPAS);
        Account accountOut = accountRepository.toEntity(accountJPA);
        //Act
        Account expected = accountRepository.getFamilyOrMemberCashAccount(ownerId);
        //Assert
        assertEquals(accountOut, expected);
    }

    @Test
    @DisplayName("Failure - the account is not cash.")
    void getFamilyOrMemberCashAccount_notCash() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("sanda@hotmail.com"));
        Account account = new PersonalCashAccount.Builder(new AccountID("1"))
                .setOwnerID(new PersonID(new Email("sanda@hotmail.com")))
                .setDescription(new Description("My new account"))
                .setBalance(new MoneyValue(2080)).setAccountType().build();

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("1"));
        accountJPA.setAccountType(EAccountType.CREDIT_CARD);
        accountJPA.setBalance(2080);
        accountJPA.setDescription("My new account");
        accountJPA.setPersonID(ownerId);

        List<AccountJPA> accountJPAS = new ArrayList<>();
        Mockito.when(iAccountAssemblerJPAMock.toJPA(account)).thenReturn(accountJPA);
        accountJPAS.add(accountJPA);
        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(ownerId)).thenReturn(accountJPAS);
        //Act
        //Assert
        assertThrows(IllegalArgumentException.class, () -> accountRepository.getFamilyOrMemberCashAccount(ownerId));
    }

    @Test
    @DisplayName("Failure - the cash account is null.")
    void getFamilyOrMemberCashAccount_Failure() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("sanda@hotmail.com"));
        List<AccountJPA> accountJPAS = new ArrayList<>();
        Mockito.when(iAccountAssemblerJPAMock.toJPA(null)).thenThrow(IllegalArgumentException.class);
        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(ownerId)).thenReturn(accountJPAS);
        //Act
        //Assert
        assertThrows(IllegalArgumentException.class, () -> accountRepository.getFamilyOrMemberCashAccount(ownerId));
    }
}



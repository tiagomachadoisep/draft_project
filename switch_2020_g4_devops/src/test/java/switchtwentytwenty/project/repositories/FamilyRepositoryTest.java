package switchtwentytwenty.project.repositories;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.parameters.P;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.AdminStateException;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IFamilyIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.FamilyJPA;
import switchtwentytwenty.project.persistence.data.FamilyRelationshipJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IFamilyRepositoryJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class FamilyRepositoryTest {

    private IFamilyRepositoryJPA familyRepositoryJPA = Mockito.mock(IFamilyRepositoryJPA.class);
    private IFamilyIAssemblerJPA familyIAssemblerJPA = Mockito.mock(IFamilyIAssemblerJPA.class);

    private FamilyRepository familyRepository = new FamilyRepository(familyRepositoryJPA, familyIAssemblerJPA);

    @Test
    void findByID_successCase() {
        FamilyID familyID = new FamilyID("1");
        PersonID adminID = new PersonID(new Email("admin@gmail.com"));
        String stringFamilyName = "Santos";
        FamilyName familyName = new FamilyName(stringFamilyName);
        String regDate = "25-03-1993";
        RegistrationDate registrationDate = new RegistrationDate(regDate);
        List<FamilyRelationshipJPA> familyRelationshipJPAList = new ArrayList<>();

        FamilyJPA familyJPA = new FamilyJPA(familyID, adminID, stringFamilyName, regDate, familyRelationshipJPAList);

        familyJPA.addFamilyRelationship("rel123", "one@gmail.com", "two@gmail.com",
                "sister");

        familyRepositoryJPA.save(familyJPA);

        Optional<FamilyJPA> optionalFamilyJPA = Optional.of(familyJPA);

        Family familyExpected = new Family.Builder(familyID).setAdminId(adminID).setRegistrationDate(registrationDate).
                setName(familyName).build();

        Optional<Family> optionalFamilyExpected = Optional.of(familyExpected);

        Mockito.when(familyRepositoryJPA.findById(familyID)).thenReturn(optionalFamilyJPA);
        Mockito.when(familyIAssemblerJPA.toFamilyName(stringFamilyName)).
                thenReturn(familyName);
        Mockito.when(familyIAssemblerJPA.toRegistrationDate(regDate)).
                thenReturn(registrationDate);
        Mockito.when(familyIAssemblerJPA.toPersonID("one@gmail.com")).
                thenReturn(new PersonID(new Email("one@gmail.com")));
        Mockito.when(familyIAssemblerJPA.toPersonID("two@gmail.com")).
                thenReturn(new PersonID(new Email("two@gmail.com")));
        Mockito.when(familyIAssemblerJPA.toFamilyRelationType("sister")).
                thenReturn(new FamilyRelationType("sister"));
        Mockito.when(familyIAssemblerJPA.toFamilyRelationshipID("rel123")).
                thenReturn(new FamilyRelationshipID("rel123"));

        Optional<Family> optionalResult = familyRepository.findByID(familyID);

        assertEquals(optionalFamilyExpected, optionalResult);

    }

    @Test
    void findByID_failCase_familyDoesNotExistInDB() {
        FamilyID familyID = new FamilyID("1");

        Optional<FamilyJPA> optionalFamilyJPA = Optional.empty();

        Optional<Family> optionalFamilyExpected = Optional.empty();

        Mockito.when(familyRepositoryJPA.findById(familyID)).thenReturn(optionalFamilyJPA);

        Optional<Family> optionalResult = familyRepository.findByID(familyID);

        assertFalse(optionalResult.isPresent());
        assertEquals(optionalFamilyExpected, optionalResult);

    }

    @Test
    void existsID_trueCase() {
        FamilyID familyID = new FamilyID("fam123");

        Mockito.when(familyRepositoryJPA.existsById(familyID)).thenReturn(true);

        boolean result = familyRepository.existsID(familyID);

        assertTrue(result);
    }

    @Test
    void existsID_falseCase() {
        FamilyID familyID = new FamilyID("fam123");

        Mockito.when(familyRepositoryJPA.existsById(familyID)).thenReturn(false);

        boolean result = familyRepository.existsID(familyID);

        assertFalse(result);
    }

    @Test
    void findsAllFamilies() {
        FamilyJPA family1 = new FamilyJPA();
        family1.setFamilyID(new FamilyID("1"));
        family1.setFamilyName("Reis");
        family1.setRegistrationDate("11.13.21");

        FamilyJPA family2 = new FamilyJPA();
        family2.setFamilyID(new FamilyID("178"));
        family2.setFamilyName("Leite");
        family2.setRegistrationDate("11.13.20");

        FamilyJPA family3 = new FamilyJPA();
        family3.setFamilyID(new FamilyID("1789"));
        family3.setFamilyName("Santos");
        family3.setRegistrationDate("21.12.20");

        List<FamilyJPA> allFamilies = new ArrayList<>();
        allFamilies.add(family1);
        allFamilies.add(family2);
        allFamilies.add(family3);

        Mockito.when(familyRepositoryJPA.findAll()).thenReturn(allFamilies);

        //act
        List<Family> actual = familyRepository.findAll();
        //Assert
        assertEquals(allFamilies.size(), actual.size());

    }

    @Test
    void familyIdReturned() {
        PersonID martaEmail = new PersonID(new Email("marta@isep.pt"));

        FamilyJPA family1 = new FamilyJPA();
        family1.setFamilyID(new FamilyID("1"));
        family1.setFamilyName("Reis");
        family1.setAdminEmail(martaEmail);
        family1.setRegistrationDate("11.13.21");

        FamilyJPA family2 = new FamilyJPA();
        family2.setFamilyID(new FamilyID("178"));
        family2.setFamilyName("Leite");
        family2.setAdminEmail(new PersonID(new Email("ki@sapo.pt")));
        family2.setRegistrationDate("11.13.20");

        FamilyJPA family3 = new FamilyJPA();
        family3.setFamilyID(new FamilyID("1789"));
        family3.setFamilyName("Santos");
        family3.setAdminEmail(new PersonID(new Email("yumi@sapo.pt")));
        family3.setRegistrationDate("21.12.20");

        List<FamilyJPA> allFamilies = new ArrayList<>();
        allFamilies.add(family1);
        allFamilies.add(family2);
        allFamilies.add(family3);

        Mockito.when(familyRepositoryJPA.findAll()).thenReturn(allFamilies);

        //act
        FamilyID actual = familyRepository.getFamilyIdByAdminMainEmail(new Email("marta@isep.pt"));
        //Assert
        assertEquals(new FamilyID("1"), actual);

    }

    @Test
    void emailAdminDoesNotExist() {
        PersonID martaEmail = new PersonID(new Email("marta@isep.pt"));

        FamilyJPA family1 = new FamilyJPA();
        family1.setFamilyID(new FamilyID("1"));
        family1.setFamilyName("Reis");
        family1.setAdminEmail(martaEmail);
        family1.setRegistrationDate("11.13.21");

        FamilyJPA family2 = new FamilyJPA();
        family2.setFamilyID(new FamilyID("178"));
        family2.setFamilyName("Leite");
        family2.setAdminEmail(new PersonID(new Email("ki@sapo.pt")));
        family2.setRegistrationDate("11.13.20");

        FamilyJPA family3 = new FamilyJPA();
        family3.setFamilyID(new FamilyID("1789"));
        family3.setFamilyName("Santos");
        family3.setAdminEmail(new PersonID(new Email("yumi@sapo.pt")));
        family3.setRegistrationDate("21.12.20");

        List<FamilyJPA> allFamilies = new ArrayList<>();
        allFamilies.add(family1);
        allFamilies.add(family2);
        allFamilies.add(family3);

        Mockito.when(familyRepositoryJPA.findAll()).thenReturn(allFamilies);

        //Assert
        assertThrows(AdminStateException.class, () -> familyRepository.getFamilyIdByAdminMainEmail(new Email("stranger@isep.pt")));

    }

    @Test
    void isNotAdmin() {
        PersonID admin = new PersonID(new Email("luffy@gmail.com"));
        FamilyJPA jpa = new FamilyJPA();
        jpa.setAdminEmail(admin);
        jpa.setFamilyID(new FamilyID("1"));
        jpa.setFamilyName("Finral");

        List<FamilyJPA> allFamilies = new ArrayList<>();
        allFamilies.add(jpa);

        Mockito.when(familyRepositoryJPA.findAll()).thenReturn(allFamilies);

        //act
        boolean result = familyRepository.isAdmin(new PersonID(new Email("gom-gom@email.com")));

        //assert
        assertFalse(result);
    }

    @Test
    void isTheAdmin() {
        PersonID admin = new PersonID(new Email("luffy@gmail.com"));
        FamilyJPA jpa = new FamilyJPA();
        jpa.setAdminEmail(admin);
        jpa.setFamilyID(new FamilyID("1"));
        jpa.setFamilyName("Finral");

        List<FamilyJPA> allFamilies = new ArrayList<>();
        allFamilies.add(jpa);

        Mockito.when(familyRepositoryJPA.findAll()).thenReturn(allFamilies);

        //act
        boolean result = familyRepository.isAdmin(admin);

        //assert
        assertTrue(result);
    }

    @Test
    void familyJPAtoDomain(){
        PersonID admin = new PersonID(new Email("luffy@gmail.com"));
        FamilyJPA jpa = new FamilyJPA();
        jpa.setAdminEmail(admin);
        jpa.setFamilyID(new FamilyID("1"));
        jpa.setFamilyName("Finral");
        jpa.setRegistrationDate("01-01-1900");
        jpa.addFamilyRelationship("test","luffy@gmail.com","ze@isep.pt","brother");

        Family family = new Family.Builder(new FamilyID("1")).setAdminId(admin).setName(new FamilyName("Finral")).setRegistrationDate(new RegistrationDate("01-01-1900")).build();
        family.createRelationship(admin,new PersonID(new Email("ze@isep.pt")),new FamilyRelationType("brother"),new FamilyRelationshipID("test"));

        Mockito.when(familyRepositoryJPA.findById(new FamilyID("1"))).thenReturn(Optional.of(jpa));
        Mockito.when(familyIAssemblerJPA.toFamilyName("Finral")).thenReturn(new FamilyName("Finral"));
        Mockito.when(familyIAssemblerJPA.toRegistrationDate("01-01-1900")).thenReturn(new RegistrationDate("01-01-1900"));
        Mockito.when(familyIAssemblerJPA.toPersonID("luffy@gmail.com")).thenReturn(admin);
        Mockito.when(familyIAssemblerJPA.toPersonID("ze@isep.pt")).thenReturn(new PersonID(new Email("ze@isep.pt")));
        Mockito.when(familyIAssemblerJPA.toFamilyRelationType("brother")).thenReturn(new FamilyRelationType("brother"));
        Mockito.when(familyIAssemblerJPA.toFamilyRelationshipID("test")).thenReturn(new FamilyRelationshipID("test"));

        Optional<Family> result = familyRepository.findByID(new FamilyID("1"));

        assertEquals(Optional.of(family),result);
    }


}


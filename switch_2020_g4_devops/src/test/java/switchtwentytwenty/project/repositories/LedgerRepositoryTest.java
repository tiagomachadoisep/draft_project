package switchtwentytwenty.project.repositories;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.core.parameters.P;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.aggregates.ledger.Transfer;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.dto.PaymentDTO;
import switchtwentytwenty.project.persistence.assemblers.implementations.LedgerAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ILedgerIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.LedgerJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ILedgerRepositoryJPA;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class LedgerRepositoryTest {

    private final ILedgerRepositoryJPA ledgerRepositoryJPA = Mockito.mock(ILedgerRepositoryJPA.class);
    private final ILedgerIAssemblerJPA assemblerJPA = new LedgerAssemblerJPA();
    private final LedgerRepository ledgerRepository = new LedgerRepository(ledgerRepositoryJPA, assemblerJPA);

    @Test
    void getAccountMovementsBetweenDates() {
        Ledger ledger = new Ledger(1, "ze@isep.pt");
        ledger.addPayment(new TransactionID("1"), new AccountID("1"), new MoneyValue(100), new CategoryID("1"), new TransactionDate("2021-02-07"));

        LedgerJPA ledgerJPA = new LedgerJPA(1, "ze@isep.pt");
        ledgerJPA.addPayment("1", "1", -10, "1", LocalDate.of(2021, 1, 11));

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.of(ledgerJPA));

        List<Movement> list = ledgerRepository.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"), new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"));

        List<Movement> expected = new ArrayList<>();
        expected.add(new Movement(new AccountID("1"), new MoneyValue(-10)));

        assertEquals(expected, list);
    }

    @Test
    void getAccountMovementsBetweenDatesSeveralPayments() {

        LedgerJPA ledgerJPA = new LedgerJPA(1, "ze@isep.pt");
        ledgerJPA.addPayment("1", "1", -10, "1", LocalDate.of(2021, 1, 11));
        ledgerJPA.addPayment("2", "1", -20, "1", LocalDate.of(2019, 1, 11));

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.of(ledgerJPA));

        List<Movement> list = ledgerRepository.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"), new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"));

        List<Movement> expected = new ArrayList<>();
        expected.add(new Movement(new AccountID("1"), new MoneyValue(-10)));

        assertEquals(expected, list);
    }

    @Test
    void getAccountMovementsBetweenDatesNoLedger() {

        LedgerJPA ledgerJPA = new LedgerJPA(1, "ze@isep.pt");
        ledgerJPA.addPayment("1", "1", -10, "1", LocalDate.of(2021, 1, 11));
        ledgerJPA.addPayment("2", "1", -20, "1", LocalDate.of(2019, 1, 11));

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.empty());

        List<Movement> list = ledgerRepository.getAccountMovementsBetweenDates(new LedgerID(1), new AccountID("1"), new TransactionDate("2021-01-01"), new TransactionDate("2021-12-31"));

        List<Movement> expected = new ArrayList<>();

        assertEquals(expected, list);
    }

    @Test
    void saveNew() {
        int ledgerId = 1;
        String ownerEmail = "ze@isep.pt";
        Ledger ledger = new Ledger(ledgerId, ownerEmail);

        Mockito.when(ledgerRepositoryJPA.save(assemblerJPA.toJPA(ledger))).thenReturn(assemblerJPA.toJPA(ledger));

        assertDoesNotThrow(() -> {
            ledgerRepository.saveNew(ledger);
        });
    }

    @Test
    void findById() {
        int ledgerId = 1;
        String ownerEmail = "ze@isep.pt";
        Optional<Ledger> expected = Optional.of(new Ledger(ledgerId, ownerEmail, ELedgerType.PERSONAL));

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(ledgerId))).thenReturn(Optional.of(new LedgerJPA(ledgerId, ownerEmail)));

        Optional<Ledger> result = ledgerRepository.findByID(new LedgerID(ledgerId));

        assertEquals(expected, result);

    }

    @Test
    void findAll() {
        int ledgerId = 1;
        String ownerEmail = "ze@isep.pt";
        int ledgerId2 = 2;
        String ownerEmail2 = "ana@isep.pt";

        Ledger ledger = new Ledger(ledgerId, ownerEmail, ELedgerType.PERSONAL);
        Ledger ledger2 = new Ledger(ledgerId2, ownerEmail2, ELedgerType.PERSONAL);

        Optional<Ledger> optionalLedger = Optional.of(ledger);
        Optional<Ledger> optionalLedger2 = Optional.of(ledger2);

        List<LedgerJPA> list = new ArrayList<>();
        list.add(assemblerJPA.toJPA(ledger));
        list.add(assemblerJPA.toJPA(ledger2));

        Mockito.when(ledgerRepositoryJPA.findAll()).thenReturn(list);

        List<Ledger> expected = new ArrayList<>();
        expected.add(ledger);
        expected.add(ledger2);

        List<Ledger> result = ledgerRepository.findAll();

        assertEquals(expected, result);
    }

    @Test
    void existsId() {
        Mockito.when(ledgerRepositoryJPA.existsById(new LedgerID(1))).thenReturn(true);

        assertTrue(ledgerRepository.existsID(new LedgerID(1)));
    }

    @Test
    void findByOwnerIdFail() {
        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("ze@isep.pt")))).thenReturn(new ArrayList<>());

        //ACT
        Optional<Ledger> result = ledgerRepository.findByOwnerID(new PersonID(new Email("ze@isep.pt")));
        assertEquals(result, Optional.empty());
    }

    @Test
    void findByOwnerIdButItOnlyHasTheFamilyLedger() {
        LedgerJPA familyLedgerJPA = new LedgerJPA(2, "ze@isep.pt", ELedgerType.FAMILY);

        List<LedgerJPA> ledgerJPAList = new ArrayList<>();
        ledgerJPAList.add(familyLedgerJPA);

        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("ze@isep.pt")))).thenReturn(ledgerJPAList);

        //ACT
        Optional<Ledger> result = ledgerRepository.findByOwnerID(new PersonID(new Email("ze@isep.pt")));

        //ASSERT
        assertEquals(Optional.empty(), result);
    }

    @Test
    void findByOwnerId() {
        LedgerJPA ledgerJPA = new LedgerJPA(1, "ze@isep.pt");
        LedgerJPA familyLedgerJPA = new LedgerJPA(2, "ze@isep.pt", ELedgerType.FAMILY);

        List<LedgerJPA> ledgerJPAList = new ArrayList<>();
        ledgerJPAList.add(ledgerJPA);
        ledgerJPAList.add(familyLedgerJPA);

        Ledger expected = new Ledger(1, "ze@isep.pt");

        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("ze@isep.pt")))).thenReturn(ledgerJPAList);

        //ACT
        Optional<Ledger> result = ledgerRepository.findByOwnerID(new PersonID(new Email("ze@isep.pt")));

        //ASSERT
        assertEquals(result, Optional.of(expected));
    }

    @Test
    void findByFamilyIdFail() {
        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("ze@isep.pt")))).thenReturn(new ArrayList<>());

        //ACT
        Optional<Ledger> result = ledgerRepository.findFamilyByOwnerID(new PersonID(new Email("ze@isep.pt")));
        assertEquals(result, Optional.empty());
    }

    @Test
    void findByFamilyIdButItOnlyHasTheMemberLedger() {
        LedgerJPA familyLedgerJPA = new LedgerJPA(2, "ze@isep.pt", ELedgerType.PERSONAL);

        List<LedgerJPA> ledgerJPAList = new ArrayList<>();
        ledgerJPAList.add(familyLedgerJPA);

        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("ze@isep.pt")))).thenReturn(ledgerJPAList);

        //ACT
        Optional<Ledger> result = ledgerRepository.findFamilyByOwnerID(new PersonID(new Email("ze@isep.pt")));

        //ASSERT
        assertEquals(Optional.empty(), result);
    }

    @Test
    void findByFamilyId() {
        LedgerJPA ledgerJPA = new LedgerJPA(1, "ze@isep.pt");
        LedgerJPA familyLedgerJPA = new LedgerJPA(2, "ze@isep.pt", ELedgerType.FAMILY);

        List<LedgerJPA> ledgerJPAList = new ArrayList<>();
        ledgerJPAList.add(ledgerJPA);
        ledgerJPAList.add(familyLedgerJPA);

        Ledger expected = new Ledger(2, "ze@isep.pt");

        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("ze@isep.pt")))).thenReturn(ledgerJPAList);

        //ACT
        Optional<Ledger> result = ledgerRepository.findFamilyByOwnerID(new PersonID(new Email("ze@isep.pt")));

        //ASSERT
        assertEquals(result, Optional.of(expected));
    }


    @Test
    void findByID_whenIsNotPresent() {

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.empty());

        Optional<Ledger> result = ledgerRepository.findByID(new LedgerID(1));

        assertEquals(result, Optional.empty());
    }

    @Test
    void findByOwnerID_whenLedgerTypeIsNotPersonalType() {

        Mockito.when(ledgerRepositoryJPA.findByPersonID(new PersonID(new Email("luis@isep.pt")))).thenReturn(new ArrayList<>());
        Optional<Ledger> result = ledgerRepository.findByOwnerID(new PersonID(new Email("luis@isep.pt")));

        assertEquals(result, Optional.empty());
    }

    @Test
    void toEntity(){
        LedgerJPA ledgerJPA = new LedgerJPA(1,"ze@isep.pt");
        ledgerJPA.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate(LocalDate.of(2020,1,1)));

        Ledger expectedLedger = new Ledger(1,"ze@isep.pt");
        expectedLedger.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate(LocalDate.of(2020,1,1)));

        Optional<Ledger> expected = Optional.of(expectedLedger);

        Optional<LedgerJPA> expectedjpa = Optional.of(ledgerJPA);

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(expectedjpa);

        Optional<Ledger> result = ledgerRepository.findByID(new LedgerID(1));

        assertEquals(expected,result);
        assertEquals(expected.get().getTransactionCount(),result.get().getTransactionCount());
    }

    @Test
    void existsIdFalse(){
        Mockito.when(ledgerRepositoryJPA.existsById(new LedgerID(1))).thenReturn(false);

        assertFalse(ledgerRepository.existsID(new LedgerID(1)));
    }

    @Test
    void addPayment(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Collections.singletonList(ledgerJPA)));

        LedgerJPA expected = new LedgerJPA(1,email);
        expected.addPaymentVO(new TransactionID("1"),new AccountID("1"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        ledgerRepository.addPayment(personID,new TransactionID("1"),new AccountID("1"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expected);

    }

    @Test
    void addPaymentFail(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(Collections.emptyList());

        ledgerRepository.addPayment(personID,new TransactionID("1"),new AccountID("1"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPA);
    }

    @Test
    void addTransferFromFamily(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Arrays.asList(ledgerJPA,ledgerJPAFamily)));

        LedgerJPA expected = new LedgerJPA(1,email);
        expected.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        LedgerJPA expectedFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        expectedFamily.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        ledgerRepository.addTransferFromFamily(personID,personID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expected);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expectedFamily);
    }

    @Test
    void addTransferFromFamilyNotSameMember(){
        String email = "ze@isep.pt";
        String emailOther = "ana@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        PersonID otherID = new PersonID(new Email(emailOther));
        LedgerJPA ledgerJPAOther = new LedgerJPA(3,emailOther);
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Arrays.asList(ledgerJPA,ledgerJPAFamily)));
        Mockito.when(ledgerRepositoryJPA.findByPersonID(otherID)).thenReturn(new ArrayList<>(Arrays.asList(ledgerJPAOther)));

        LedgerJPA expected = new LedgerJPA(3,emailOther);
        expected.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        LedgerJPA expectedFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        expectedFamily.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        ledgerRepository.addTransferFromFamily(personID,otherID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expected);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expectedFamily);
    }

    @Test
    void addTransferFromFamilyFail(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(Collections.emptyList());

        ledgerRepository.addTransferFromFamily(personID,personID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPA);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPAFamily);
    }

    @Test
    void addTransferFromFamilyFailFamily(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(Arrays.asList(ledgerJPA));

        ledgerRepository.addTransferFromFamily(personID,personID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPA);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPAFamily);
    }


    @Test
    void addTransfer(){
        String email = "ze@isep.pt";
        String email2 = "ana@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        PersonID otherID = new PersonID(new Email(email2));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        LedgerJPA ledgerJPAAna = new LedgerJPA(3,email2);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Arrays.asList(ledgerJPA,ledgerJPAFamily)));
        Mockito.when(ledgerRepositoryJPA.findByPersonID(otherID)).thenReturn(new ArrayList<>(Arrays.asList(ledgerJPAAna)));

        LedgerJPA expected = new LedgerJPA(1,email);
        expected.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        LedgerJPA expectedOther = new LedgerJPA(3,email2);
        expectedOther.addTransfer(new TransactionID("1"),new AccountID("1"),new AccountID("2"),10,new CategoryID("1"),new TransactionDate("1990-01-01"));
        ledgerRepository.addTransfer(personID,otherID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expected);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(1)).save(expectedOther);
    }

    @Test
    void addTransferFail(){
        String email = "ze@isep.pt";
        String email2 = "ana@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        PersonID otherID = new PersonID(new Email(email2));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        LedgerJPA ledgerJPAAna = new LedgerJPA(3,email2);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Arrays.asList(ledgerJPA,ledgerJPAFamily)));
        Mockito.when(ledgerRepositoryJPA.findByPersonID(otherID)).thenReturn(Collections.emptyList());

        ledgerRepository.addTransfer(personID,otherID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPA);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPAAna);
    }

    @Test
    void addTransferFailOther(){
        String email = "ze@isep.pt";
        String email2 = "ana@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        PersonID otherID = new PersonID(new Email(email2));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        LedgerJPA ledgerJPAFamily = new LedgerJPA(2,email,ELedgerType.FAMILY);
        LedgerJPA ledgerJPAAna = new LedgerJPA(3,email2);
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(Collections.emptyList());
        Mockito.when(ledgerRepositoryJPA.findByPersonID(otherID)).thenReturn(Collections.emptyList());

        ledgerRepository.addTransfer(personID,otherID,new TransactionID("1"),new AccountID("1"), new AccountID("2"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));

        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPA);
        Mockito.verify(ledgerRepositoryJPA,Mockito.times(0)).save(ledgerJPAAna);
    }

    @Test
    void findTransaction(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        ledgerJPA.addPayment("1","1",10,"1",LocalDate.of(1990,1,1));
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Collections.singletonList(ledgerJPA)));
        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.of(ledgerJPA));

        ledgerRepository.addPayment(personID,new TransactionID("1"),new AccountID("1"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));


        Transactionable transactionable = ledgerRepository.findTransactionByID(new TransactionID("1"),new LedgerID(1));

        assertEquals(new AccountID("1"),transactionable.getAccountID());
        assertEquals(10,transactionable.getAmount());
        assertEquals(new CategoryID("1"),transactionable.getCategoryId());
    }

    @Test
    void findTransactionFailNoLedger(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        ledgerJPA.addPayment("1","1",10,"1",LocalDate.of(1990,1,1));
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Collections.singletonList(ledgerJPA)));
        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.empty());

        ledgerRepository.addPayment(personID,new TransactionID("1"),new AccountID("1"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));


        assertThrows(NullPointerException.class,()->{ ledgerRepository.findTransactionByID(new TransactionID("1"),new LedgerID(1));});

    }

    @Test
    void findTransactionByMovementFailNoLedger(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        ledgerJPA.addPayment("1","1",10,"1",LocalDate.of(1990,1,1));
        Mockito.when(ledgerRepositoryJPA.findByPersonID(personID)).thenReturn(new ArrayList<>(Collections.singletonList(ledgerJPA)));
        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.empty());

        ledgerRepository.addPayment(personID,new TransactionID("1"),new AccountID("1"),new MoneyValue(10),new CategoryID("1"),new TransactionDate("1990-01-01"));


        assertThrows(NullPointerException.class,()->{ ledgerRepository.findPossibleTransactionsByMovement(new LedgerID(1),new AccountID("1"),new MoneyValue(10));});

    }

    @Test
    void findTransactionsByMovement(){
        String email = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(email));
        LedgerJPA ledgerJPA = new LedgerJPA(1,email);
        ledgerJPA.addPayment("1","1",10,"1",LocalDate.of(1990,1,1));
        ledgerJPA.addTransfer(new TransactionID("2"),new AccountID("1"),new AccountID("3"),100,new CategoryID("1"),new TransactionDate(LocalDate.of(1980,10,2)));

        Mockito.when(ledgerRepositoryJPA.findById(new LedgerID(1))).thenReturn(Optional.of(ledgerJPA));

        List<Transactionable> expected = new ArrayList<>();
        expected.add(new Transfer(new TransactionID("2"),new AccountID("1"),new AccountID("3"),new MoneyValue(100),new CategoryID("1"),new TransactionDate(LocalDate.of(1980,10,2)),new Description("n/a")));

        List<Transactionable> list = ledgerRepository.findPossibleTransactionsByMovement(new LedgerID(1),new AccountID("3"),new MoneyValue(100));

        assertEquals(expected,list);
    }

}
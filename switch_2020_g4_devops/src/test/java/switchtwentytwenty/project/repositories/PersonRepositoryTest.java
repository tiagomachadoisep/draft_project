package switchtwentytwenty.project.repositories;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.assemblers.implementations.PersonAssemblerJPA;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


class PersonRepositoryTest {

    private final PersonAssemblerJPA personAssemblerJPAMock = Mockito.mock(PersonAssemblerJPA.class);
    private final IPersonRepositoryJPA iPersonRepositoryJPAMock = Mockito.mock(IPersonRepositoryJPA.class);
    private final PersonRepository personRepository = new PersonRepository(personAssemblerJPAMock, iPersonRepositoryJPAMock);



    @Test
    void whenTryToFindByID_personIsNotPresent() {
        PersonID personID = new PersonID(new Email("luis@isep.ipp.pt"));

        Mockito.when(iPersonRepositoryJPAMock.findById(personID)).thenReturn(Optional.empty());

        Optional<Person> result = personRepository.findByID(personID);

        assertEquals(Optional.empty(), result);

    }

    @Test
    void tryToAssembleFromPersonJPAList_toPersonList() {
        Person personOne = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setName(new PersonName("luis"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-1986")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        Person personTwo = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).setName(new PersonName("mimi"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-2016")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();


        PersonJPA personJPAOne = new PersonJPA("luis@isep.pt", "2", "luis"
                , 0, "Rua tal", "01-01-1986");

        PersonJPA personJPATwo = new PersonJPA("mimi@isep.pt", "2", "mimi"
                , 0, "Rua tal", "01-01-2016");


        List<PersonJPA> personJPAList = new ArrayList<>();
        personJPAList.add(personJPAOne);
        personJPAList.add(personJPATwo);


        List<Person> expected = new ArrayList<>();
        expected.add(personOne);
        expected.add(personTwo);

        List<Person> result = personRepository.assembleToPersonList(personJPAList);

        assertEquals(expected, result);
    }

    @Test
    void tryToGetAllPeople_OfGivenFamily() {
        FamilyID familyID = new FamilyID("20");
        Person personOne = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setName(new PersonName("luis"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-1986")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        Person personTwo = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).setName(new PersonName("mimi"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-2016")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        PersonJPA personJPAOne = new PersonJPA("luis@isep.pt", "2", "luis"
                , 0, "Rua tal", "01-01-1986");

        PersonJPA personJPATwo = new PersonJPA("mimi@isep.pt", "2", "mimi"
                , 0, "Rua tal", "01-01-2016");


        List<PersonJPA> personJPAList = new ArrayList<>();
        personJPAList.add(personJPAOne);
        personJPAList.add(personJPATwo);


        List<Person> expected = new ArrayList<>();
        expected.add(personOne);
        expected.add(personTwo);

        Mockito.when(iPersonRepositoryJPAMock.findAllByFamilyID(familyID)).thenReturn(personJPAList);

        List<Person> result = personRepository.findAllByFamilyID(familyID);

        assertEquals(expected, result);
    }

    @Test
    void hasSameFamily_False_BothMembersAreNotFound() {
        //arrange
        PersonID firstPerson = new PersonID(new Email("piece@iol.pt"));
        PersonID secondPerson = new PersonID(new Email("one@iol.pt"));
        Mockito.when(iPersonRepositoryJPAMock.findById(firstPerson)).thenReturn(Optional.empty(), Optional.empty());
        //act
        boolean result = personRepository.hasSameFamily(firstPerson, secondPerson);
        //assert
        assertFalse(result);
    }

    @Test
    void hasSameFamily_True() {
        //arrange
        PersonID firstPerson = new PersonID(new Email("piece@iol.pt"));
        PersonID secondPerson = new PersonID(new Email("one@iol.pt"));

        PersonJPA jpa = new PersonJPA();
        jpa.setPersonID(firstPerson);
        jpa.setName("Ben");
        jpa.setVat(0);
        jpa.setFamilyID(new FamilyID("12"));

        PersonJPA jpaSecond = new PersonJPA();
        jpaSecond.setPersonID(secondPerson);
        jpaSecond.setName("Benedite");
        jpaSecond.setVat(221094121);
        jpaSecond.setFamilyID(new FamilyID("12"));

        Mockito.when(iPersonRepositoryJPAMock.findById(firstPerson)).thenReturn(Optional.of(jpa));
        Mockito.when(iPersonRepositoryJPAMock.findById(secondPerson)).thenReturn(Optional.of(jpaSecond));
        Mockito.when(personAssemblerJPAMock.assemblePersonName(jpa)).thenReturn(new PersonName(jpa.getName()));
        Mockito.when(personAssemblerJPAMock.assemblePersonName(jpaSecond)).thenReturn(new PersonName(jpaSecond.getName()));
        Mockito.when(personAssemblerJPAMock.assembleVATNumber(jpaSecond)).thenReturn(new VATNumber(jpaSecond.getVat()));
        Mockito.when(personAssemblerJPAMock.assembleVATNumber(jpa)).thenReturn(new VATNumber(jpa.getVat()));
        //act
        boolean result = personRepository.hasSameFamily(firstPerson, secondPerson);
        //assert
        assertTrue(result);
    }

    @Test
    void hasSameFamily_False_DontBelongToSameFamily() {
        //arrange
        PersonID firstPerson = new PersonID(new Email("piece@iol.pt"));
        PersonID secondPerson = new PersonID(new Email("one@iol.pt"));

        PersonJPA jpa = new PersonJPA();
        jpa.setPersonID(firstPerson);
        jpa.setName("Ben");
        jpa.setVat(0);
        jpa.setFamilyID(new FamilyID("123"));

        PersonJPA jpaSecond = new PersonJPA();
        jpaSecond.setPersonID(secondPerson);
        jpaSecond.setName("Benedite");
        jpaSecond.setVat(221094121);
        jpaSecond.setFamilyID(new FamilyID("12"));

        Mockito.when(iPersonRepositoryJPAMock.findById(firstPerson)).thenReturn(Optional.of(jpa), Optional.of(jpaSecond));
        Mockito.when(personAssemblerJPAMock.assemblePersonName(jpa)).thenReturn(new PersonName(jpa.getName()), new PersonName(jpaSecond.getName()));
        Mockito.when(personAssemblerJPAMock.assembleVATNumber(jpaSecond)).thenReturn(new VATNumber(jpaSecond.getVat()), new VATNumber(jpa.getVat()));
        //act
        boolean result = personRepository.hasSameFamily(firstPerson, secondPerson);
        //assert
        assertFalse(result);
    }

    @Test
    void hasSameFamily_False_FirstPersonNotFound() {
        //arrange
        PersonID firstPerson = new PersonID(new Email("piece@iol.pt"));
        PersonID secondPerson = new PersonID(new Email("one@iol.pt"));

        PersonJPA jpaSecond = new PersonJPA();
        jpaSecond.setPersonID(secondPerson);
        jpaSecond.setName("Benedite");
        jpaSecond.setVat(221094121);
        jpaSecond.setFamilyID(new FamilyID("12"));

        Mockito.when(iPersonRepositoryJPAMock.findById(firstPerson)).thenReturn(Optional.empty(), Optional.of(jpaSecond));
        Mockito.when(personAssemblerJPAMock.assemblePersonName(jpaSecond)).thenReturn(new PersonName(jpaSecond.getName()));
        Mockito.when(personAssemblerJPAMock.assembleVATNumber(jpaSecond)).thenReturn(new VATNumber(jpaSecond.getVat()));
        //act
        boolean result = personRepository.hasSameFamily(firstPerson, secondPerson);
        //assert
        assertFalse(result);
    }

    @Test
    void findAll(){
        Mockito.when(iPersonRepositoryJPAMock.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new PersonJPA("ze@ipp.pt","1","ze",0,"outro"))));

        List<Person> expected = new ArrayList<>();
        Person person = new Person.Builder(new PersonID(new Email("ze@ipp.pt"))).setAddress(new Address("outro")).setFamilyId(new FamilyID("1")).setName(new PersonName("ze")).setVat(new VATNumber(0)).build();
        expected.add(person);
        List<Person> list = personRepository.findAll();
        assertEquals(expected,list);
    }

    @Test
    void deleteByPersonID() {
        personRepository.deleteByPersonID(new PersonID(new Email("teste@isep.pt")));

        Mockito.verify(iPersonRepositoryJPAMock,Mockito.times(1)).deleteByPersonID(new PersonID(new Email("teste@isep.pt")));
    }

    @Test
    void findFamilyByPersonId() {
        //arrange
        PersonID firstPerson = new PersonID(new Email("piece@iol.pt"));
        PersonID secondPerson = new PersonID(new Email("one@iol.pt"));

        PersonJPA jpa = new PersonJPA();
        jpa.setPersonID(firstPerson);
        jpa.setName("Ben");
        jpa.setVat(0);
        jpa.setFamilyID(new FamilyID("12"));

        PersonJPA jpaSecond = new PersonJPA();
        jpaSecond.setPersonID(secondPerson);
        jpaSecond.setName("Benedite");
        jpaSecond.setVat(221094121);
        jpaSecond.setFamilyID(new FamilyID("12"));

        Mockito.when(iPersonRepositoryJPAMock.findById(firstPerson)).thenReturn(Optional.of(jpa));
        Mockito.when(iPersonRepositoryJPAMock.findById(secondPerson)).thenReturn(Optional.of(jpaSecond));

        String result = personRepository.findFamilyByPersonId("piece@iol.pt");

        String expected = "12";
        assertEquals(expected,result);
    }

    @Test
    void findFamilyByPersonId_DoesntExist() {
        //arrange
        PersonID firstPerson = new PersonID(new Email("piece@iol.pt"));


        Mockito.when(iPersonRepositoryJPAMock.findById(firstPerson)).thenReturn(Optional.empty());


        String result = personRepository.findFamilyByPersonId("piece@iol.pt");

        String expected = "No family ID";
        assertEquals(expected,result);
    }

    /*

    @Test
    void addPersonTestSuccessUsingSameID() {
        PersonRepository personRepository = new PersonRepository();
        Email newEmail = new Email("luis@isep.ipp.pt");
        ID familyID = new FamilyID(1);

        ID expeted = new PersonID(newEmail);

        ID result = personRepository.addPerson("luis@isep.ipp.pt", "luis", 123456789, "Ruas", familyID);

        assertEquals(expeted, result);
    }

    @Test
    void addPersonTestFailDifferentID() {
        PersonRepository personRepository = new PersonRepository();
        Email newEmail = new Email("luis@isep.ipp.pt");
        ID familyID = new FamilyID(1);

        ID expeted = new PersonID(newEmail);

        ID result = personRepository.addPerson("mimi@isep.ipp.pt", "luis", 123456789, "Ruas", familyID);

        assertNotEquals(expeted, result);
    }

    @Test
    void getPersonByIDTestSuccessSamePersonID() {
        PersonRepository personRepository = new PersonRepository();
        ID familyID = new FamilyID(1);
        personRepository.addPerson("luis@isep.ipp.pt", "luis", 123456789, "Ruas", familyID);

        Person expectedPerson = new Person("luis@isep.ipp.pt", "luis", 123456789, "Ruas");
        Person resultPerson = personRepository.getPerson("luis@isep.ipp.pt");

        assertEquals(expectedPerson, resultPerson);
    }

    @Test
    void getPersonByIDTestFailDifferentPersonID() {
        PersonRepository personRepository = new PersonRepository();
        ID familyID = new FamilyID(1);
        personRepository.addPerson("luis@isep.ipp.pt", "luis", 123456789, "Ruas",familyID);

        assertThrows(IllegalArgumentException.class, () -> {
            Person resultPerson = personRepository.getPerson("mimi@isep.ipp.pt");
        });
    }

    @Test
    void getPersonIDByStringEmail_validCase() {
        PersonRepository personRepository = new PersonRepository();
        ID familyID = new FamilyID(1);
        personRepository.addPerson("luis@isep.ipp.pt", "luis", 123456789, "Ruas",familyID);
        personRepository.addPerson("pedro@isep.ipp.pt", "pedro", 289010969, "Ruas",familyID);
        String stringEmailToCompare = "pedro@isep.ipp.pt";

        ID expected = new PersonID(new Email(stringEmailToCompare));

        ID result = personRepository.getPersonIDByStringEmail(stringEmailToCompare).get();

        assertEquals(expected, result);
    }

    @Test
    void getPersonIDByStringEmail_invalidCase_IdNotFound() {
        PersonRepository personRepository = new PersonRepository();
        ID familyID = new FamilyID(1);
        personRepository.addPerson("luis@isep.ipp.pt", "luis", 123456789, "Ruas", familyID);
        personRepository.addPerson("pedro@isep.ipp.pt", "pedro", 289010969, "Ruas", familyID);
        String stringEmailToCompare = "maria@isep.ipp.pt";

        boolean result = personRepository.getPersonIDByStringEmail(stringEmailToCompare).isPresent();

        assertFalse(result);
    }

    @Test
    void checkIfEmailExists_True() {

        //Arrange
        PersonRepository personRepository = new PersonRepository();
        personRepository.addPerson("filipa.silva@gmail.com", "Filipa", 266301851, "Rua do Sol 13, Porto",
                new FamilyID(3));
        personRepository.addPerson("paulo.silva@gmail.com", "Paulo", 222371692, "Rua do Sol 13, Porto",
                new FamilyID(3));
        personRepository.addPerson("daniel.silva@gmail.com", "Daniel", 285869248, "Rua do Sol 13, Porto",
                new FamilyID(3));
        String email = "filipa.silva@gmail.com";

        //Act
        boolean result = personRepository.checkIfEmailExists(email);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfEmailExists_FalseBecauseNoEmailInExistingMembersList() {

        //Arrange
        PersonRepository personRepository = new PersonRepository();
        personRepository.addPerson("filipa.silva@gmail.com", "Filipa", 266301851, "Rua do Sol 13, Porto",
                new FamilyID(3));
        personRepository.addPerson("paulo.silva@gmail.com", "Paulo", 222371692, "Rua do Sol 13, Porto",
                new FamilyID(3));
        personRepository.addPerson("daniel.silva@gmail.com", "Daniel", 285869248, "Rua do Sol 13, Porto",
                new FamilyID(3));
        String email = "rui.silva@gmail.com";

        //Act
        boolean result = personRepository.checkIfEmailExists(email);

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfEmailExists_FalseBecauseMembersListIsEmpty() {

        //Arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "rui.silva@gmail.com";

        //Act
        boolean result = personRepository.checkIfEmailExists(email);

        //Assert
        assertFalse(result);

    }


    @Test
    void saveMemberTestSuccess() {
        //Arrange
        PersonRepository personRepository = new PersonRepository();
        PersonDTO personDTO = new PersonDTO("luis","luis@isep.ipp.pt",123456789);
        Person person = new Person(personDTO);

        //Act
        PersonID expectedID = new PersonID(new Email("luis@isep.ipp.pt"));
        PersonID resultID = personRepository.saveMember(person);

        //Assert
        assertEquals(expectedID,resultID);
    }

    @Test
    void saveMemberTestFail_MemberAlreadyRegistered() {
        //Arrange
        PersonRepository personRepository = new PersonRepository();
        PersonDTO personDTO = new PersonDTO("luis","luis@isep.ipp.pt",123456789);
        Person person = new Person(personDTO);

        //Act
        personRepository.saveMember(person);

        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            PersonID personID = personRepository.saveMember(person);
        });
    }

    */

}


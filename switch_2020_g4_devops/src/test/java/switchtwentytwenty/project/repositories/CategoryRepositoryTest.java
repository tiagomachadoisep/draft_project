package switchtwentytwenty.project.repositories;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.aggregates.category.CustomCategory;
import switchtwentytwenty.project.domain.aggregates.category.StandardCategory;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ICategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


class CategoryRepositoryTest {

    private ICategoryRepositoryJPA categoryRepositoryJPAMock = Mockito.mock(ICategoryRepositoryJPA.class);
    private ICategoryAssemblerJPA categoryAssemblerJPA = Mockito.mock(ICategoryAssemblerJPA.class);
    private CategoryRepository repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);


    @Test
    void hasSameNameTest() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA();
        jpa.setId(new CategoryID("1"));
        jpa.setName("Animals");
        jpa.setParentCategory(null);

        List<CategoryJPA> categories = new ArrayList<>();
        categories.add(jpa);

        Mockito.when(categoryRepositoryJPAMock.findByParentId(null)).thenReturn(categories);
        Mockito.when(categoryAssemblerJPA.toCategoryName("Animals")).thenReturn(new CategoryName("Animals"));
        //ACT
        boolean result = repository.existsCategory(null, new CategoryName("Animals"), true);

        //ASSERT
        assertTrue(result);
    }

    @Test
    void categoryNamesAreDifferent() {
        //ARRANGE
        CategoryJPA jpa = new CategoryJPA();
        jpa.setId(new CategoryID("1"));
        jpa.setName("Animals");
        jpa.setParentCategory(null);

        List<CategoryJPA> categories = new ArrayList<>();
        categories.add(jpa);

        Mockito.when(categoryRepositoryJPAMock.findByParentId(null)).thenReturn(categories);
        Mockito.when(categoryAssemblerJPA.toCategoryName("Animals")).thenReturn(new CategoryName("Animals"));
        //ACT
        boolean result = repository.existsCategory(null, new CategoryName("Dolls"), true);

        //ASSERT
        assertFalse(result);
    }

    @Test
    void categoryListEmpty_CategoryDoesNotExists() {
        //ARRANGE
        repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);

        List<CategoryJPA> categories = new ArrayList<>();

        Mockito.when(categoryRepositoryJPAMock.findByParentId(null)).thenReturn(categories);

        //ACT
        boolean result = repository.existsCategory(null, new CategoryName("Dolls"), true);

        //ASSERT
        assertFalse(result);
    }


    @Test
    void whenTheCategoryIsFound() {
        //ARRANGE
        Categorable expected = new StandardCategory.Builder(new CategoryID("129")).setName(new CategoryName("Cakes"))
                .setParent(null).build();

        CategoryJPA jpa = new CategoryJPA();
        jpa.setName("Cakes");
        jpa.setId(new CategoryID("129"));
        jpa.setParentCategory(null);

        Optional<CategoryJPA> category = Optional.of(jpa);

        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID("129"))).thenReturn(category);
        Mockito.when(categoryAssemblerJPA.toCategoryName("Cakes")).thenReturn(new CategoryName("Cakes"));
        //ACT
        Optional<Categorable> categoryRequired = repository.findByID(new CategoryID("129"));
        Categorable actual = categoryRequired.get();

        //ASSERT
        assertEquals(actual, expected);
    }

    @Test
    void whenTheCategoryIsNotFound() {
        //ARRANGE
        repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);

        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID("129"))).thenReturn(Optional.empty());
        //ACT
        Optional<Categorable> categoryRequired = repository.findByID(new CategoryID("129"));


        //ASSERT
        assertEquals(Optional.empty(), categoryRequired);
    }

    @Test
    void idExists() {
        //ARRANGE
        CategoryID id = new CategoryID("aish2");
        Mockito.when(categoryRepositoryJPAMock.existsById(id)).thenReturn(true);

        //ACT
        boolean result = repository.existsID(id);

        //ASSERT
        assertTrue(result);
    }

    @Test
    void idDoesntExists() {
        //ARRANGE
        CategoryID id = new CategoryID("aish2");
        Mockito.when(categoryRepositoryJPAMock.existsById(id)).thenReturn(false);

        //ACT
        boolean result = repository.existsID(id);

        //ASSERT
        assertFalse(result);
    }

    @Test
    void findsThreeCategories() {
        //ARRANGE
        CategoryJPA numberOne = new CategoryJPA();
        numberOne.setName("Clothes");
        numberOne.setId(new CategoryID("1i23"));
        numberOne.setParentCategory(null);

        CategoryJPA numberTwo = new CategoryJPA();
        numberTwo.setName("Socks");
        numberTwo.setId(new CategoryID("1itfj23"));
        numberTwo.setParentCategory("1i23");

        CategoryJPA numberThree = new CategoryJPA();
        numberThree.setName("Vegetables");
        numberThree.setId(new CategoryID("skfgeuigs"));
        numberThree.setParentCategory(null);

        List<CategoryJPA> allCategories = new ArrayList<>();
        allCategories.add(numberOne);
        allCategories.add(numberTwo);
        allCategories.add(numberThree);

        Mockito.when(categoryRepositoryJPAMock.findAll()).thenReturn(allCategories);
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberOne.getName())).thenReturn(new CategoryName(numberOne.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberTwo.getName())).thenReturn(new CategoryName(numberTwo.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryID(numberTwo.getParentId())).thenReturn(new CategoryID(numberTwo.getParentId()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberThree.getName())).thenReturn(new CategoryName(numberThree.getName()));

        Categorable standardOne = new StandardCategory.Builder(new CategoryID("1i23")).setName(new CategoryName(
                "Clothes"))
                .setParent(null).build();
        Categorable standardTwo = new StandardCategory.Builder(new CategoryID("1itfj23")).setName(new CategoryName(
                "Socks"))
                .setParent(new CategoryID("1i23")).build();
        Categorable standardThree =
                new StandardCategory.Builder(new CategoryID("skfgeuigs")).setName(new CategoryName("Vegetables"))
                        .setParent(null).build();
        List<Categorable> expectedList = new ArrayList<>();
        expectedList.add(standardOne);
        expectedList.add(standardTwo);
        expectedList.add(standardThree);

        //ACT
        List<Categorable> actualList = repository.findAll();

        //ASSERT
        assertEquals(expectedList, actualList);

    }

    @Test
    void transformsIntoCustom() {
        //Arrange
        CategoryJPA jpa = new CategoryJPA();
        jpa.setName("Hotel");
        jpa.setFamilyId(new FamilyID("123"));
        jpa.setId(new CategoryID("1"));

        CustomCategory expected = new CustomCategory.Builder(new CategoryID("1")).setFamily(new FamilyID("123"))
                .setParent(null).setName(new CategoryName("Hotel")).build();
        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID("1"))).thenReturn(Optional.of(jpa));
        Mockito.when(categoryAssemblerJPA.toCategoryName("Hotel")).thenReturn(new CategoryName("Hotel"));
        Mockito.when(categoryAssemblerJPA.toFamilyID(jpa.getFamilyId().toString())).thenReturn(new FamilyID("123"));
        //Act
        Optional<Categorable> actual = repository.findByID(new CategoryID("1"));
        //Assert
        assertEquals(expected, actual.get());
    }

    @Test
    void transformsIntoCustomSubcategory() {
        //Arrange
        CategoryJPA jpa = new CategoryJPA();
        jpa.setName("Hotel");
        jpa.setParentCategory("1234");
        jpa.setFamilyId(new FamilyID("123"));
        jpa.setId(new CategoryID("1"));

        CustomCategory expected = new CustomCategory.Builder(new CategoryID("1")).setFamily(new FamilyID("123"))
                .setParent(new CategoryID("1234")).setName(new CategoryName("Hotel")).build();
        Mockito.when(categoryRepositoryJPAMock.findById(new CategoryID("1"))).thenReturn(Optional.of(jpa));
        Mockito.when(categoryAssemblerJPA.toCategoryName("Hotel")).thenReturn(new CategoryName("Hotel"));
        Mockito.when(categoryAssemblerJPA.toCategoryID("1234")).thenReturn(new CategoryID("1234"));
        Mockito.when(categoryAssemblerJPA.toFamilyID(jpa.getFamilyId().toString())).thenReturn(new FamilyID("123"));
        //Act
        Optional<Categorable> actual = repository.findByID(new CategoryID("1"));
        //Assert
        assertEquals(expected, actual.get());
    }

    @Test
    @DisplayName("Success - get the list of family's categories including standard categories.")
    void getListOfCategoriesByFamilyId_Success() {
        //Arrange
        FamilyID familyId = new FamilyID("365");
        List<CategoryJPA> categoryJPAlist = new ArrayList<>();

        repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);

        CategoryJPA numberOne = new CategoryJPA();
        numberOne.setName("Clothes");
        numberOne.setId(new CategoryID("1i23"));
        numberOne.setParentCategory(null);

        CategoryJPA numberTwo = new CategoryJPA();
        numberTwo.setName("Socks");
        numberTwo.setId(new CategoryID("1itfj23"));
        numberTwo.setParentCategory("1i23");

        CategoryJPA numberThree = new CategoryJPA();
        numberThree.setName("Vegetables");
        numberThree.setId(new CategoryID("skfgeuigs"));
        numberThree.setParentCategory(null);

        categoryJPAlist.add(numberOne);
        categoryJPAlist.add(numberTwo);
        categoryJPAlist.add(numberThree);

        List<Categorable> familyCategories = new ArrayList<>();
        Categorable standardOne = new StandardCategory.Builder(new CategoryID("1i23")).setName(new CategoryName(
                "Clothes"))
                .setParent(null).build();
        Categorable standardTwo = new StandardCategory.Builder(new CategoryID("1itfj23")).setName(new CategoryName(
                "Socks"))
                .setParent(new CategoryID("1i23")).build();
        Categorable standardThree =
                new StandardCategory.Builder(new CategoryID("skfgeuigs")).setName(new CategoryName("Vegetables"))
                        .setParent(null).build();

        familyCategories.add(standardOne);
        familyCategories.add(standardTwo);
        familyCategories.add(standardThree);

        List<CategoryJPA> standardCategoryListJPA = new ArrayList<>();

        CategoryJPA jpa = new CategoryJPA();
        jpa.setName("Cakes");
        jpa.setId(new CategoryID("129"));
        jpa.setParentCategory(null);

        CategoryJPA jpaTwo = new CategoryJPA();
        jpaTwo.setName("Car");
        jpaTwo.setId(new CategoryID("149"));
        jpaTwo.setParentCategory(null);

        standardCategoryListJPA.add(jpa);
        standardCategoryListJPA.add(jpaTwo);

        List<Categorable> standardCategories = new ArrayList<>();

        Categorable one = new StandardCategory.Builder(new CategoryID("129")).setName(new CategoryName("Cakes"))
                .setParent(null).build();

        Categorable two = new StandardCategory.Builder(new CategoryID("149")).setName(new CategoryName("Car"))
                .setParent(null).build();
        standardCategories.add(one);
        standardCategories.add(two);
        familyCategories.addAll(standardCategories);

        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(familyId)).thenReturn(categoryJPAlist);
        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(null)).thenReturn(standardCategoryListJPA);
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberOne.getName())).thenReturn(new CategoryName(numberOne.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberTwo.getName())).thenReturn(new CategoryName(numberTwo.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryID(numberTwo.getParentId())).thenReturn(new CategoryID(numberTwo.getParentId()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberThree.getName())).thenReturn(new CategoryName(numberThree.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(jpa.getName())).thenReturn(new CategoryName(jpa.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(jpaTwo.getName())).thenReturn(new CategoryName(jpaTwo.getName()));


        //Act
        List<Categorable> actualList = repository.getListOfCategoriesByFamilyId(familyId);
        //Assert
        assertEquals(familyCategories, actualList);
    }

    @Test
    @DisplayName("Failure - the lists are empty.")
    void getListOfCategoriesByFamilyId_Failure() {
        //Arrange
        FamilyID familyId = new FamilyID("365");
        List<CategoryJPA> categoryJPAlist = new ArrayList<>();

        repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);

        List<Categorable> familyCategories = Collections.emptyList();

        List<CategoryJPA> standardCategoryListJPA = new ArrayList<>();

        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(familyId)).thenReturn(categoryJPAlist);
        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(null)).thenReturn(standardCategoryListJPA);
        //Act
        List<Categorable> actualList = repository.getListOfCategoriesByFamilyId(familyId);
        //Assert
        assertEquals(familyCategories, actualList);
    }

    @Test
    void getListOfCategoriesByFamilyId_EmptyFamilyCategoriesList() {
        //Arrange
        FamilyID familyId = new FamilyID("365");
        repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);

        CategoryJPA numberTwo = new CategoryJPA();
        numberTwo.setName("Socks");
        numberTwo.setId(new CategoryID("1itfj23"));
        numberTwo.setParentCategory(null);

        CategoryJPA numberThree = new CategoryJPA();
        numberThree.setName("Vegetables");
        numberThree.setId(new CategoryID("skfgeuigs"));
        numberThree.setParentCategory(null);

        List<CategoryJPA> standardCategoryListJPA = new ArrayList<>();
        standardCategoryListJPA.add(numberTwo);
        standardCategoryListJPA.add(numberThree);

        Categorable standardTwo = new StandardCategory.Builder(new CategoryID("1itfj23")).setName(new CategoryName(
                "Socks"))
                .setParent(null).build();
        Categorable standardThree =
                new StandardCategory.Builder(new CategoryID("skfgeuigs")).setName(new CategoryName("Vegetables"))
                        .setParent(null).build();

        List<Categorable> expected = new ArrayList<>();
        expected.add(standardTwo);
        expected.add(standardThree);

        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(familyId)).thenReturn(new ArrayList<>());
        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(null)).thenReturn(standardCategoryListJPA);
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberTwo.getName())).thenReturn(new CategoryName(numberTwo.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberThree.getName())).thenReturn(new CategoryName(numberThree.getName()));

        //Act
        List<Categorable> actualList = repository.getListOfCategoriesByFamilyId(familyId);
        //Assert
        assertEquals(expected, actualList);
    }

    @Test
    void getListOfCategoriesByFamilyId_EmptyStandardCategoriesList() {
        //Arrange
        FamilyID familyId = new FamilyID("009");
        repository = new CategoryRepository(categoryAssemblerJPA, categoryRepositoryJPAMock);

        CategoryJPA numberTwo = new CategoryJPA();
        numberTwo.setName("Socks");
        numberTwo.setId(new CategoryID("1itfj23"));
        numberTwo.setFamilyId(new FamilyID("009"));
        numberTwo.setParentCategory(null);

        CategoryJPA numberThree = new CategoryJPA();
        numberThree.setName("Vegetables");
        numberThree.setId(new CategoryID("skfgeuigs"));
        numberThree.setFamilyId(new FamilyID("009"));
        numberThree.setParentCategory(null);

        List<CategoryJPA> familyCategoryListJPA = new ArrayList<>();
        familyCategoryListJPA.add(numberTwo);
        familyCategoryListJPA.add(numberThree);

        Categorable standardTwo = new CustomCategory.Builder(new CategoryID("1itfj23")).setName(new CategoryName(
                "Socks"))
                .setParent(null).setFamily(new FamilyID("009")).build();
        Categorable standardThree =
                new CustomCategory.Builder(new CategoryID("skfgeuigs")).setName(new CategoryName("Vegetables"))
                        .setParent(null).setFamily(new FamilyID("009")).build();

        List<Categorable> expected = new ArrayList<>();
        expected.add(standardTwo);
        expected.add(standardThree);

        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(familyId)).thenReturn(familyCategoryListJPA);
        Mockito.when(categoryRepositoryJPAMock.findAllByFamilyId(null)).thenReturn(new ArrayList<>());
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberTwo.getName())).thenReturn(new CategoryName(numberTwo.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberThree.getName())).thenReturn(new CategoryName(numberThree.getName()));

        //Act
        List<Categorable> actualList = repository.getListOfCategoriesByFamilyId(familyId);
        //Assert
        assertEquals(expected, actualList);
    }

    @Test
    void findsTwoStandardCategories() {
        //ARRANGE
        CategoryJPA numberOne = new CategoryJPA();
        numberOne.setName("Clothes");
        numberOne.setId(new CategoryID("1i23"));
        numberOne.setParentCategory(null);

        CategoryJPA numberTwo = new CategoryJPA();
        numberTwo.setName("Socks");
        numberTwo.setId(new CategoryID("1itfj23"));
        numberTwo.setFamilyId(new FamilyID("1"));
        numberTwo.setParentCategory("1i23");

        CategoryJPA numberThree = new CategoryJPA();
        numberThree.setName("Vegetables");
        numberThree.setId(new CategoryID("skfgeuigs"));
        numberThree.setParentCategory(null);

        List<CategoryJPA> allCategories = new ArrayList<>();
        allCategories.add(numberOne);
        allCategories.add(numberTwo);
        allCategories.add(numberThree);

        Mockito.when(categoryRepositoryJPAMock.findAll()).thenReturn(allCategories);
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberOne.getName())).thenReturn(new CategoryName(numberOne.getName()));
        Mockito.when(categoryAssemblerJPA.toCategoryName(numberThree.getName())).thenReturn(new CategoryName(numberThree.getName()));

        Categorable standardOne = new StandardCategory.Builder(new CategoryID("1i23")).setName(new CategoryName(
                "Clothes"))
                .setParent(null).build();
        Categorable standardThree =
                new StandardCategory.Builder(new CategoryID("skfgeuigs")).setName(new CategoryName("Vegetables"))
                        .setParent(null).build();
        List<Categorable> expectedList = new ArrayList<>();
        expectedList.add(standardOne);
        expectedList.add(standardThree);

        //ACT
        List<Categorable> actualList = repository.getStandardCategoryTree();

        //ASSERT
        assertEquals(expectedList, actualList);

    }
}
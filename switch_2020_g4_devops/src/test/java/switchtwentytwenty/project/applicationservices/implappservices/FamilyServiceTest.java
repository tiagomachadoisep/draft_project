package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.PersonDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

//@SpringBootTest
class FamilyServiceTest     {


    private IFamilyRepository familyRepository = Mockito.mock(IFamilyRepository.class);
    private IPersonRepository personRepository = Mockito.mock(IPersonRepository.class);
    private AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
    private FamilyService service = new FamilyService(familyRepository, personRepository, assemblerToDTO);

    @Test
    void getFamilyById(){
        String name = "Ze";
        String adminEmail = "ze@isep.pt";
        String familyName = "Sousa";
        String date = LocalDate.now().toString();
        String familyID = "1";
        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName(familyName)).setAdminId(new PersonID(new Email(adminEmail)))
                .setRegistrationDate(new RegistrationDate(date)).build();

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));

        FamilyDTO dto = service.getFamilyById(familyID);

        FamilyDTO expected = new FamilyDTO(familyName, adminEmail, familyID);

        assertEquals(expected, dto);
    }

    @Test
    void getFamilyById_DoesntExist() {
        String name = "Ze";
        String adminEmail = "ze@isep.pt";
        String familyName = "Sousa";
        String date = LocalDate.now().toString();
        String familyID = "1";
        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName(familyName)).setAdminId(new PersonID(new Email(adminEmail)))
                .setRegistrationDate(new RegistrationDate(date)).build();

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.empty());

        FamilyDTO dto = service.getFamilyById(familyID);

        FamilyDTO expected = new FamilyDTO();

        assertEquals(expected, dto);
    }

    @Test
    void familyIdReturned() {
        PersonID martaEmail = new PersonID(new Email("marta@isep.pt"));

        Family family1 = new Family.Builder(new FamilyID("1")).setAdminId(martaEmail)
                .setName(new FamilyName("Santos")).setRegistrationDate(new RegistrationDate("11.09.20")).build();

        FamilyDTO family1Dto = new FamilyDTO();
        family1Dto.setFamilyId("1");

        Family family2 = new Family.Builder(new FamilyID("178")).setAdminId(new PersonID(new Email("ki@sapo.pt")))
                .setName(new FamilyName("Leite")).setRegistrationDate(new RegistrationDate("11.13.20")).build();

        FamilyDTO family2Dto = new FamilyDTO();
        family2Dto.setFamilyId("178");

        Family family3 = new Family.Builder(new FamilyID("1789")).setAdminId(new PersonID(new Email("yumi@sapo.pt")))
                .setName(new FamilyName("Reis")).setRegistrationDate(new RegistrationDate("11.01.20")).build();

        FamilyDTO family3Dto = new FamilyDTO();
        family3Dto.setFamilyId("1789");

        List<Family> allFamilies = new ArrayList<>();
        allFamilies.add(family1);
        allFamilies.add(family2);
        allFamilies.add(family3);

        Mockito.when(familyRepository.findAll()).thenReturn(allFamilies);

        //act
        List<FamilyDTO> actual = service.getFamilies();
        //Assert
        assertEquals(allFamilies.size(), actual.size());

    }

    @Test
    void getFamilyWithAdminEmail() {
        FamilyID expected = new FamilyID("1");
        Mockito.when(familyRepository.getFamilyIdByAdminMainEmail(new Email("disaster@email.com"))).thenReturn(expected);

        FamilyID actual = service.getFamilyIdByAdminMainEmail(new Email("disaster@email.com"));

        assertEquals(expected, actual);
    }

    @Test
    void createFamilyAdminAlreadyExists(){
        PersonID personID = new PersonID(new Email("admin@gmail.com"));
        FamilyDTO dto = new FamilyDTO("Silva","admin@gmail.com");
        //PersonDTO dtoAdmin = new PersonDTO("admin","admin@gmail.com",0);
        PersonDTO dtoAdmin = new PersonDTO("2","admin@gmail.com","Silva",0,"Rua tal", "01-01-1980");

        Mockito.when(personRepository.existsID(personID)).thenReturn(true);

        FamilyDTO result = service.createAndSaveFamily(dto,dtoAdmin);

        assertEquals(null,result);
    }

    @Test
    void createFamily() {
        FamilyDTO dto = new FamilyDTO("ilva", "admin@gmail.com");
        PersonDTO dtoAdmin = new PersonDTO("2","admin@gmail.com","Silva",0,"Rua tal", "01-01-1980");

        Mockito.when(personRepository.existsID(new PersonID(new Email("admin@gmail.com")))).thenReturn(false);

        FamilyDTO result = service.createAndSaveFamily(dto, dtoAdmin);

//        Family family = new Family("ilva","1","admin@gmail.com",LocalDate.now().toString());
        Person admin = new Person.Builder(new PersonID(new Email("admin@gmail.com"))).setName(new PersonName("admin"))
                .setVat(new VATNumber(0)).build();

        verify(familyRepository, times(1)).saveNew(any());
        verify(personRepository, times(1)).saveNew(admin);

        assertNotNull(result);
        assertEquals("ilva", result.getName());

    }

}
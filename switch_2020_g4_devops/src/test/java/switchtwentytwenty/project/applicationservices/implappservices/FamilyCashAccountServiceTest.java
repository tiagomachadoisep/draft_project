package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.dto.AccountDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FamilyCashAccountServiceTest {

    private final IFamilyService iFamilyServiceMock = Mockito.mock(IFamilyService.class);
    private final IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    FamilyCashAccountService familyCashAccountService = new FamilyCashAccountService(iFamilyServiceMock, iAccountServiceMock);

    @Test
    void testGetFamilyCashAccount() {
        AccountDTO accountDTO = new AccountDTO("luis@isep.pt", "FamilyCash", 200, "3548", EAccountType.FAMILY_CASH);

        Mockito.when(iAccountServiceMock.getAccountByID("3548")).thenReturn(Optional.of(accountDTO));

        Optional<AccountDTO> result = familyCashAccountService.getFamilyCashAccount("3548");

        assertTrue(result.isPresent());
    }


}
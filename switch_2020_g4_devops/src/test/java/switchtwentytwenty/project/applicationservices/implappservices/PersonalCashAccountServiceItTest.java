package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import switchtwentytwenty.project.applicationservices.implassemblers.AccountAssembler;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.exceptions.PersonAlreadyHasCashAccountException;
import switchtwentytwenty.project.persistence.assemblers.implementations.AccountAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.implementations.PersonAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;
import switchtwentytwenty.project.repositories.AccountRepository;
import switchtwentytwenty.project.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

//@SpringBootTest
public class PersonalCashAccountServiceItTest {
/*
    private static IPersonRepositoryJPA iPersonRepositoryJPAMock;
    private static IAccountRepositoryJPA iAccountRepositoryJPAMock;
    private static PersonService personService;
    private static AccountService accountService;
    private static IPersonRepository iPersonRepositoryMock;
    private static String ownerID;
    private static String description;
    private static double balance;
    private static AccountDTO personalCashAccountDTO;

    @BeforeEach
    private void setUp() {
        iPersonRepositoryJPAMock = Mockito.mock(IPersonRepositoryJPA.class);
        PersonAssemblerJPA personAssemblerJPA = new PersonAssemblerJPA();
        PersonRepository personRepository = new PersonRepository(personAssemblerJPA, iPersonRepositoryJPAMock);
        PersonAssembler personAssembler = new PersonAssembler();
        personService = new PersonService(personRepository, personAssembler);
        AccountAssembler accountAssembler = new AccountAssembler();
        //AccountConcreteFactory accountFactory = new AccountConcreteFactory();
        AccountAssemblerJPA accountAssemblerJPA = new AccountAssemblerJPA();
        iAccountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
        iPersonRepositoryMock = Mockito.mock(IPersonRepository.class);
        AccountRepository accountRepository = new AccountRepository(accountAssemblerJPA, iAccountRepositoryJPAMock);
        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO,iPersonRepositoryMock);

        ownerID = "catarina@gmail.com";
        description = "cash account";
        balance = -1;
        personalCashAccountDTO = new AccountDTO();
        personalCashAccountDTO.setOwnerId(ownerID);
        personalCashAccountDTO.setDescription(description);
        personalCashAccountDTO.setBalance(balance);

    }

    @Test
    void createPersonalCashAccount_ValidPersonAndAccount() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("15"));
        accountJPA.setPersonID(new PersonID(new Email("luisa@hotmail.com")));
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(balance);
        accountJPA.setDescription(description);
        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iPersonRepositoryJPAMock.existsById(new PersonID(new Email(ownerID)))).thenReturn(true);
        Mockito.when(iAccountRepositoryJPAMock.findAll()).thenReturn(accounts);

        PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(personService,
                accountService);

        //Act
        AccountDTO result = personalCashAccountService.createPersonalCashAccount(personalCashAccountDTO);

        //Assert
        assertNotNull(result);

    }

    @Test
    void createPersonalCashAccount_PersonDoesNotExist() {

        //Arrange
        Mockito.when(iPersonRepositoryJPAMock.existsById(new PersonID(new Email(ownerID)))).thenReturn(false);

        PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(personService,
                accountService);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> {
            personalCashAccountService.createPersonalCashAccount(personalCashAccountDTO);
        });
    }

    @Test
    void createPersonalCashAccount_PersonAlreadyHasACashAccount() {

        //Arrange
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("15"));
        accountJPA.setPersonID(new PersonID(new Email(ownerID)));
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(balance);
        accountJPA.setDescription(description);
        List<AccountJPA> accounts = new ArrayList<>();
        accounts.add(accountJPA);

        Mockito.when(iPersonRepositoryJPAMock.existsById(new PersonID(new Email(ownerID)))).thenReturn(true);
        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(new PersonID(new Email(ownerID)))).thenReturn(accounts);

        PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(personService,
                accountService);

        //Act+Assert
        assertThrows(PersonAlreadyHasCashAccountException.class, () -> {
            personalCashAccountService.createPersonalCashAccount(personalCashAccountDTO);
        });
    }

    @Test
    void getPersonalCashAccountByID_AccountExists() {

        //Arrange
        String accountID = "23AD4";
        AccountDTO expected = new AccountDTO();
        expected.setAccountId("23AD4");
        expected.setAccountType(EAccountType.PERSONAL_CASH);
        expected.setBalance(100);
        expected.setDescription("Cash account");
        expected.setOwnerId("luisa@gmail.com");

        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(new AccountID("23AD4"));
        accountJPA.setAccountType(EAccountType.PERSONAL_CASH);
        accountJPA.setBalance(100);
        accountJPA.setDescription("Cash account");
        accountJPA.setPersonID(new PersonID(new Email("luisa@gmail.com")));

        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(new AccountID(accountID))).thenReturn(accountJPA);

        PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(personService,
                accountService);

        //Act
        Optional<AccountDTO> result = personalCashAccountService.getPersonalCashAccountByID(accountID);

        //Assert
        assertTrue(result.isPresent());
        assertEquals(expected, result.get());

    }

    @Test
    void getPersonalCashAccountByID_AccountDoesNotExist() {

        //Arrange
        String accountID = "23AD4";

        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(new AccountID(accountID))).thenReturn(null);

        PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(personService,
                accountService);

        //Act
        Optional<AccountDTO> result = personalCashAccountService.getPersonalCashAccountByID(accountID);

        //Assert
        assertFalse(result.isPresent());

    }*/
}

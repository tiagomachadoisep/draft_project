package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.parameters.P;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.exceptions.NonExistentPersonException;
import switchtwentytwenty.project.utils.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

//@SpringBootTest
//@ExtendWith(SpringExtension.class)
class PersonServiceTest {

    // @Mock
    private final IPersonRepository personRepositoryMock = Mockito.mock(IPersonRepository.class);
    //@Mock
    //private IPersonAssembler personAssemblerMock;
    private final IPersonAssembler iPersonAssembler = new PersonAssembler();

    //@InjectMocks
    private final PersonService service = new PersonService(personRepositoryMock, iPersonAssembler);


    @Test
    void whenAddMember_thenRetrieveMemberID() {
        //Arrange
        String adminEmail = "ze@isep.pt";
        String familyID = "1";
        String personID = "luis@isep.ipp.pt";
        String name = "luis";
        int vat = 123456789;
        String address = "Rua de cima";
        String birthDate = "01-01-1986";
        Person admin = new Person.Builder(new PersonID(new Email(personID))).setVat(new VATNumber(0)).
                setName(new PersonName("ze")).setFamilyId(new FamilyID("1")).setBirthDate(new BirthDate("01-01-1900")).setAddress(new Address("Rua de cima")).
                setRoleAsFamilyAdministrator().build();

        PersonID expectedID = new PersonID(new Email(personID));
        PersonDTO personDTO = new PersonDTO(familyID, personID, name, vat, address, birthDate);

        Mockito.when(personRepositoryMock.findByID(new PersonID(new Email(personID)))).thenReturn(Optional.of(admin));

        //Act
        ID isNewMemberID = service.addMember(personDTO);
        Mockito.verify(personRepositoryMock,Mockito.times(1)).saveNew(admin);

        //Assert
        assertEquals(expectedID, isNewMemberID);
    }

    /*@Test
    void whenAddMember_notSameFamily() {
        //Arrange
        String adminEmail = "outroze@isep.pt";
        String familyID = "1";
        String personID = "luis@isep.ipp.pt";
        String name = "luis";
        int vat = 123456789;
        String address = "Rua de cima";
        String birthDate = "01-01-1986";
        Person admin = new Person.Builder(new PersonID(new Email(adminEmail))).setVat(new VATNumber(0)).
                setName(new PersonName("ze")).setFamilyId(new FamilyID("2")).setBirthDate(new BirthDate("01-01-1900")).setAddress(new Address("Rua de cima")).
                setRoleAsFamilyAdministrator().build();

        PersonID expectedID = new PersonID();
        PersonDTO personDTO = new PersonDTO(familyID, personID, name, vat, address, birthDate);

        Mockito.when(personRepositoryMock.findByID(new PersonID(new Email(adminEmail)))).thenReturn(Optional.of(admin));

        //Act
        ID isNewMemberID = service.addMember(personDTO);

        //Assert
        assertEquals(expectedID, isNewMemberID);
    }*/

    @Test
    void whenTryToAddNewMember_throwException(){
        //Arrange
        String adminEmail = "ze@isep.pt";
        String familyID = "1";
        String personID = "luis@isep.ipp.pt";
        String name = "luis";
        int vat = 123456789;
        String address = "Rua de cima";
        String birthDate = "01-01-1986";

        PersonID expectedID = new PersonID(new Email(personID));
        PersonDTO personDTO = new PersonDTO(familyID, personID, name, vat, address, birthDate);

        Mockito.when(personRepositoryMock.existsID(expectedID)).thenReturn(true);

        assertThrows(IllegalArgumentException.class, () -> service.addMember(personDTO));


    }

    @Test
    void tryToGetAllFamilyMembers() {
        PersonDTO personDTOOne = new PersonDTO("2", "luis@isep.pt", "luis"
                , 0, "Rua tal", "01-01-1986");
        PersonDTO personDTOTwo = new PersonDTO("2", "mimi@isep.pt", "mimi"
                , 0, "Rua tal", "01-01-2016");

        List<PersonDTO> expected = new ArrayList<>();
        expected.add(personDTOOne);
        expected.add(personDTOTwo);

        Person personOne = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setName(new PersonName("luis"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-1986")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        Person personTwo = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).setName(new PersonName("mimi"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-2016")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        List<Person> personList = new ArrayList<>();
        personList.add(personOne);
        personList.add(personTwo);

        Mockito.when(personRepositoryMock.findAllByFamilyID(new FamilyID("2"))).thenReturn(personList);

        List<PersonDTO> result = service.getFamilyMembers("2");

        assertEquals(expected, result);
    }

    @Test
    void tryToGetAllMembers(){
        PersonDTO personDTOOne = new PersonDTO("2", "luis@isep.pt", "luis"
                , 0, "Rua tal", "01-01-1986");
        PersonDTO personDTOTwo = new PersonDTO("2", "mimi@isep.pt", "mimi"
                , 0, "Rua tal", "01-01-2016");

        List<PersonDTO> expected = new ArrayList<>();
        expected.add(personDTOOne);
        expected.add(personDTOTwo);

        Person personOne = new Person.Builder(new PersonID(new Email("luis@isep.pt"))).setName(new PersonName("luis"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-1986")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        Person personTwo = new Person.Builder(new PersonID(new Email("mimi@isep.pt"))).setName(new PersonName("mimi"))
                .setAddress(new Address("Rua tal")).setBirthDate(new BirthDate("01-01-2016")).setFamilyId(new FamilyID("2")).setVat(new VATNumber(0)).build();

        List<Person> personList = new ArrayList<>();
        personList.add(personOne);
        personList.add(personTwo);

        Mockito.when(personRepositoryMock.findAll()).thenReturn(personList);

        List<PersonDTO> result = service.getAllMembers();

        assertEquals(expected,result);

    }



    @AfterEach
    void tearDown() {
    }

    @Test
    void checkIfEmailExists_ReturnsFalse() {

        //Arrange
        PersonID personID = new PersonID(new Email("carlos@gmail.com"));
        AccountDTO personalCashAccountDTO = new AccountDTO("carlos@gmail.com", "My cash " +
                "account", -2, EAccountType.PERSONAL_CASH);

        Mockito.when(personRepositoryMock.existsID(personID)).thenReturn(false);

        //Act
        boolean result = service.checkIfEmailExists(personalCashAccountDTO);

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfEmailExists_ReturnsTrue() {

        //Arrange
        PersonID personID = new PersonID(new Email("carlos@gmail.com"));
        AccountDTO personalCashAccountDTO = new AccountDTO("carlos@gmail.com", "My cash " +
                "account", -2, EAccountType.PERSONAL_CASH);

        Mockito.when(personRepositoryMock.existsID(personID)).thenReturn(true);

        //Act
        boolean result = service.checkIfEmailExists(personalCashAccountDTO);

        //Assert
        assertTrue(result);

    }

    //---------------------------------AddEmailToProfile Unit Tests--------------------------------------
    @Test
    @DisplayName("Success - unit test add valid email")
    void addEmailToProfile_validEmail() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO("juju50@outlook.com", "jurema1942@hotmail.com");
        String intFamilyID = "1";
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());

        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);

        //Act
        Result<String> result = service.addEmailToProfile(emailDTO);

        //Assert
        Mockito.verify(personRepositoryMock,Mockito.times(1)).saveNew(optionalPerson.get());
        assertTrue(result.isSuccess());
    }

    @Test
    @DisplayName("Failure - the email can't be added because the main email is not in the app.")
    void addEmailToProfile_personNotInTheApp() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO("juju50@outlook.com", "jurema1942@hotmail.com");

        String expected = "Failure. This email can't be added.";

        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.empty());

        //Act
        Result<String> result = service.addEmailToProfile(emailDTO);
        //Assert
        assertEquals(result.getContent(), expected);

    }

    @Test
    @DisplayName("Failure - unit test add the same email")
    void addEmailToProfile_sameEmail() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO("jurema1942@hotmail.com", "jurema1942@hotmail.com");
        String intFamilyID = "1";
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        String expected = "This email can't be added, because it was defined as your main email.";
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);

        //Act
        Result<String> result = service.addEmailToProfile(emailDTO);
        String finalResult = result.getContent();
        //Assert
        assertEquals(expected, finalResult);
    }

    @Test
    @DisplayName("Failure - unit test empty email")
    void addEmailToProfile_EmptyEmail() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO("", "jurema1942@hotmail.com");
        String intFamilyID = "1";
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);

        //Act
        //Assert
        assertThrows(IllegalArgumentException.class, () -> service.addEmailToProfile(emailDTO));
    }

    @Test
    @DisplayName("Failure - unit test null email")
    void addEmailToProfile_NullEmail() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO(null, "jurema1942@hotmail.com");
        String intFamilyID = "1";
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());

        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);

        //Act
        //Assert
        assertThrows(IllegalArgumentException.class, () -> service.addEmailToProfile(emailDTO));

    }

    @Test
    @DisplayName("Fail - unit test person not present")
    void addEmailToProfile_notPresent() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO("juju50@outlook.com", "jurema1942@hotmail.com");
        String intFamilyID = "1";
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());

        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.empty());

        //Act
        Result<String> result = service.addEmailToProfile(emailDTO);

        //Assert
        assertFalse(result.isSuccess());
        assertNotNull(result.getContent());
    }

    @Test
    @DisplayName("Failure - unit test add email already added")
    void addEmailToProfile_repeatedEmail() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        EmailDTO emailDTO = new EmailDTO("juju50@outlook.com", "jurema1942@hotmail.com");
        String intFamilyID = "1";

        Person person = new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        person.addEmailToProfile("juju50@outlook.com");

        Optional<Person> optionalPerson = Optional.of(person);

        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);

        //Act
        Result<String> result = service.addEmailToProfile(emailDTO);

        //Assert
        assertFalse(result.isSuccess());
        assertEquals("The email can't be added.", result.getContent());
    }

    //---------------------------------getPersonDTO Unit Tests--------------------------------------
    @Test
    @DisplayName("Success - get a personDTO")
    void getPersonDTO_success() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        String intFamilyID = "1";
        Person jurema = new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build();

        Optional<Person> optionalPerson = Optional.of(jurema);
        PersonDTO expected = new PersonDTO(intFamilyID, mainEmailJurema, "Jurema", 123456789,
                "Rua das Cristalinas,99", "01-01-1990");
        //Act
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);
        PersonDTO result = service.getPersonDTO(mainEmailJurema);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    @DisplayName("Failure - the person doesn't exist.")
    void getPersonDTO_failure() {
        //Arrange
        String mainEmailJurema = "jurema1942@hotmail.com";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        String intFamilyID = "1";
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        //Act
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);
        //Assert
        assertThrows(IllegalArgumentException.class, () -> service.getPersonDTO("juju@hotmail.com"));
    }


    //---------------------------------getOtherEmails Unit Tests--------------------------------------

    @Test
    @DisplayName("Success - get the email list")
    void getOtherEmailsList_success() {
        //Arrange
        List<String> emailList = new ArrayList<>();

        String mainEmailJurema = "jurema1942@hotmail.com";

        String intFamilyID = "1";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);
        Person jurema = optionalPerson.get();
        jurema.addEmailToProfile("juju@gmail.com");
        jurema.addEmailToProfile("juju123@gmail.com");

        emailList.add("juju@gmail.com");
        emailList.add("juju123@gmail.com");

        List<String> result = service.getOtherEmailsList(mainEmailJurema);
        //Assert
        assertEquals(emailList, result);
    }

    @Test
    @DisplayName("Success - the list is empty")
    void getOtherEmailsList_Success() {
        //Arrange
        List<String> emailList = new ArrayList<>();
        String mainEmailJurema = "jurema1942@hotmail.com";
        String intFamilyID = "1";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(optionalPerson);

        List<String> result = service.getOtherEmailsList(mainEmailJurema);
        //Assert
        assertEquals(emailList, result);
    }

    @Test
    @DisplayName("Failure - The person isn't in the app.")
    void getOtherEmailsList_Failure() {
        //Arrange
        List<String> emailList = new ArrayList<>();

        String mainEmailJurema = "jurema1942@hotmail.com";

        String intFamilyID = "1";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(null);
        Person jurema = optionalPerson.get();
        jurema.addEmailToProfile("juju@gmail.com");
        jurema.addEmailToProfile("juju123@gmail.com");

        emailList.add("juju@gmail.com");
        emailList.add("juju123@gmail.com");

        //Assert
        assertThrows(IllegalArgumentException.class, () -> service.getOtherEmailsList(mainEmailJurema));
    }

    @Test
    @DisplayName("Failure - not registered")
    void getOtherEmailsList_Fail() {
        //Arrange
        List<String> emailList = new ArrayList<>();
        String mainEmailJurema = "jurema1942@hotmail.com";
        String intFamilyID = "1";
        PersonID personID = new PersonID(new Email(mainEmailJurema));
        Optional<Person> optionalPerson = Optional.of(new Person.Builder(new PersonID(new Email(mainEmailJurema))).setFamilyId(new FamilyID(intFamilyID))
                .setName(new PersonName("Jurema")).setVat(new VATNumber(123456789)).setAddress(new Address("Rua das Cristalinas,99"))
                .setBirthDate(new BirthDate("01-01-1990")).build());
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.empty());

        List<String> result = service.getOtherEmailsList(mainEmailJurema);
        //Assert
        assertEquals(emailList, result);
    }

    //----------------------------------------------existsMember-----------------------------------------
    @Test
    void existsMember() {
        PersonID personID = new PersonID(new Email("jaciraA@gmail.com"));
        Mockito.when(personRepositoryMock.existsID(personID)).thenReturn(true);
        boolean result = service.existsMember(personID);
        assertTrue(result);
    }

    @Test
    void notExistsMember() {
        PersonID personID = new PersonID(new Email("jaciraA@gmail.com"));
        Mockito.when(personRepositoryMock.existsID(personID)).thenReturn(false);
        boolean result = service.existsMember(personID);
        assertFalse(result);
    }

    //--------------------------Delete Secondary Email--------------------------
    @Test
    void deleteSecondaryEmail_NonExistentPerson() {
        Email ownerID = new Email("carlos@isep.pt");
        Email emailToRemove = new Email ("carlosA@isep.pt");
        EmailDTO dto = new EmailDTO(emailToRemove.toString(), ownerID.toString());

        Mockito.when(personRepositoryMock.findByID(new PersonID(ownerID))).thenReturn(Optional.empty());

        assertThrows(NonExistentPersonException.class, () -> service.deleteSecondaryEmail(dto));
    }

    @Test
    void deleteSecondaryEmailSuccessfully() {
        Email ownerID = new Email("carlos@isep.pt");
        Email emailToRemove = new Email ("carlosA@isep.pt");
        EmailDTO dto = new EmailDTO(emailToRemove.toString(), ownerID.toString());

        List<Email> emailList = new ArrayList<>();
        emailList.add(new Email("carlosA@isep.pt"));
        emailList.add(new Email("carlosB@isep.pt"));
        emailList.add(new Email("carlosC@isep.pt"));

        Person person = new Person.Builder(new PersonID(ownerID))
                .setName(new PersonName("Carlos"))
                .setOtherEmails(emailList)
                .build();

        Mockito.when(personRepositoryMock.findByID(new PersonID(ownerID))).thenReturn(Optional.of(person));

        List<String> result = service.deleteSecondaryEmail(dto);

        Mockito.verify(personRepositoryMock,Mockito.times(1)).deleteByPersonID(new PersonID(ownerID));
        Mockito.verify(personRepositoryMock,Mockito.times(1)).saveNew(person);

        assertTrue(result.size() == 2);
    }

    @Test
    void getMemberNameById() {
        Email ownerID = new Email("carlos@isep.pt");
        Person person = new Person.Builder(new PersonID(ownerID))
                .setName(new PersonName("Carlos"))
                .build();

        Mockito.when(personRepositoryMock.findByID(new PersonID(ownerID))).thenReturn(Optional.of(person));

        String result = service.getMemberNameById("carlos@isep.pt");

        String expected = "Carlos";

        assertEquals(expected,result);
    }

    @Test
    void getMemberNameByIdDoesntExist() {

        Mockito.when(personRepositoryMock.findByID(new PersonID(new Email("carlos@isep.pt")))).thenReturn(Optional.empty());

        String result = service.getMemberNameById("carlos@isep.pt");

        String expected = "Person does not exist";

        assertEquals(expected,result);
    }
}
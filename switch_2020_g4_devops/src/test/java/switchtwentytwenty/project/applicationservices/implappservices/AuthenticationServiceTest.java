package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextImpl;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import static org.junit.jupiter.api.Assertions.*;

class AuthenticationServiceTest {

    private final ISecurityContextProvider provider = Mockito.mock(ISecurityContextProvider.class);

    private final IPersonService personService = Mockito.mock(IPersonService.class);

    private final AuthenticationService service= new AuthenticationService(provider,personService);

    @Test
    void isCurrentUser() {
        String userEmail = "test@isep.pt";

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(userEmail,"1234")));
        assertTrue(service.isCurrentUser(userEmail));
    }

    @Test
    void isCurrentUserNo() {
        String userEmail = "test@isep.pt";

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken("outro@isep.pt","1234")));
        assertFalse(service.isCurrentUser(userEmail));
    }

    @Test
    void isCurrentUserInFamily() {
        String userEmail = "test@isep.pt";

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(userEmail,"1234")));
        Mockito.when(personService.getMemberFamilyById(userEmail)).thenReturn("2");

        assertTrue(service.isCurrentUserInFamily("2"));

    }

    @Test
    void isCurrentUserInFamilyNo() {
        String userEmail = "test@isep.pt";

        Mockito.when(provider.getContext()).thenReturn(new SecurityContextImpl(new UsernamePasswordAuthenticationToken(userEmail,"1234")));
        Mockito.when(personService.getMemberFamilyById(userEmail)).thenReturn("1");

        assertFalse(service.isCurrentUserInFamily("2"));

    }
}
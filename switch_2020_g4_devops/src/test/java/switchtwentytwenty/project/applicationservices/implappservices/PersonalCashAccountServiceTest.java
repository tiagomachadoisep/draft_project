package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.dto.AccountDTO;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class PersonalCashAccountServiceTest {

    private final IPersonService iPersonServiceMock = Mockito.mock(IPersonService.class);
    private final IAccountService iAccountServiceMock = Mockito.mock(IAccountService.class);
    PersonalCashAccountService personalCashAccountService = new PersonalCashAccountService(iPersonServiceMock,
            iAccountServiceMock);

    @Test
    void createPersonalCashAccount_PersonExistsInRepository_ValidAccount() {

        //Arrange
        AccountDTO personalCashAccountDTO = new AccountDTO("sandra@gmail.com", "My account", 5,
                EAccountType.PERSONAL_CASH);
        AccountDTO outPersonalCashAccountDTO = new AccountDTO("sandra@gmail.com", "My account", 5, "asdf2",
                EAccountType.PERSONAL_CASH);

        Mockito.when(iPersonServiceMock.checkIfEmailExists(personalCashAccountDTO)).thenReturn(true);
        Mockito.when(iAccountServiceMock.createPersonalCashAccount(personalCashAccountDTO)).thenReturn(outPersonalCashAccountDTO);

        //Act
        AccountDTO result = personalCashAccountService.createPersonalCashAccount(personalCashAccountDTO);

        //Assert
        assertNotNull(result);

    }

    @Test
    void createPersonalCashAccount_PersonDoesNotExistInRepository() {

        //Arrange
        AccountDTO personalCashAccountDTO = new AccountDTO("sandra@gmail.com", "My account", 5,
                EAccountType.PERSONAL_CASH);

        Mockito.when(iPersonServiceMock.checkIfEmailExists(personalCashAccountDTO)).thenReturn(false);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> {
            personalCashAccountService.createPersonalCashAccount(personalCashAccountDTO);
        });

    }

    @Test
    void createPersonalCashAccount_PersonExistsInRepository_PersonAlreadyHasACashAccount() {

        //Arrange
        AccountDTO personalCashAccountDTO = new AccountDTO("sandra@gmail.com", "My account", 5,
                EAccountType.PERSONAL_CASH);

        Mockito.when(iPersonServiceMock.checkIfEmailExists(personalCashAccountDTO)).thenReturn(true);
        Mockito.when(iAccountServiceMock.createPersonalCashAccount(personalCashAccountDTO)).thenThrow(IllegalStateException.class);

        //Act+Assert
        assertThrows(IllegalStateException.class, () -> {
            personalCashAccountService.createPersonalCashAccount(personalCashAccountDTO);
        });

    }

    @Test
    void getPersonalCashAccountByID_AccountExists() {

        //Arrange
        String accountID = "23AD4";
        AccountDTO expected = new AccountDTO();
        expected.setAccountId("23AD4");
        expected.setAccountType(EAccountType.PERSONAL_CASH);
        expected.setBalance(100);
        expected.setDescription("Cash account");
        expected.setOwnerId("luisa@gmail.com");

        Mockito.when(iAccountServiceMock.getAccountByID(accountID)).thenReturn(Optional.of(expected));

        //Act
        Optional<AccountDTO> result = personalCashAccountService.getPersonalCashAccountByID(accountID);

        //Assert
        assertTrue(result.isPresent());
        assertEquals(expected, result.get());

    }

    @Test
    void getPersonalCashAccountByID_AccountDoesNotExist() {

        //Arrange
        String accountID = "23AD4";

        Mockito.when(iAccountServiceMock.getAccountByID(accountID)).thenReturn(Optional.empty());

        //Act
        Optional<AccountDTO> result = personalCashAccountService.getPersonalCashAccountByID(accountID);

        //Assert
        assertFalse(result.isPresent());

    }
}
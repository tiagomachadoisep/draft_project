package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;
import switchtwentytwenty.project.persistence.assemblers.implementations.PersonAssemblerJPA;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.repositories.PersonRepository;
import switchtwentytwenty.project.utils.Result;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


public class PersonServiceITTest {


    private static PersonAssemblerJPA personAssemblerJPA;
    private static IPersonAssembler personAssembler;
    private static String personEmail;
    private static PersonService personService;
    private static PersonDTO personDTO;
    private static PersonID personID;
    private static IPersonRepositoryJPA personRepositoryJPAMock;
    private static IPersonRepository personRepository;
    private String adminEmail;

    void setUp() {
        personEmail = "luis@isep.pt";
        personAssemblerJPA = new PersonAssemblerJPA();
        personAssembler = new PersonAssembler();
        personRepositoryJPAMock = Mockito.mock(IPersonRepositoryJPA.class);
        personRepository = new PersonRepository(personAssemblerJPA, personRepositoryJPAMock);
        personID = new PersonID(new Email(personEmail));
        personService = new PersonService(personRepository, personAssembler);
        personDTO = new PersonDTO("200",personEmail,"Luís", 123456789,"Rua tal","01-01-2000");
        personDTO.setFamilyId("fam1");
        adminEmail = "ze@isep.pt";
        String password = String.valueOf(218471742);
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(adminEmail,password));
    }

    @Test
    void whenUserIsAdded_ReturnPersonID() {
        setUp();
        Mockito.when(personRepositoryJPAMock.existsById(personID)).thenReturn(false);
        Mockito.when(personRepositoryJPAMock.findById(new PersonID(new Email(adminEmail)))).thenReturn(Optional.of(new PersonJPA(adminEmail,"fam1","ze",0,"other","01-01-2000")));
        PersonID resultID = personService.addMember(personDTO);
        assertEquals(personID, resultID);
    }

    @Test
    void checkIfEmailExists_ReturnTrue() {

        //Arrange
        setUp();
        String description = "Cash account";
        double balance = 10;
        AccountDTO personalCashAccountDTO = new AccountDTO(personEmail, description, balance,
                EAccountType.PERSONAL_CASH);

        Mockito.when(personRepositoryJPAMock.existsById(personID)).thenReturn(true);

        //Act
        boolean result = personService.checkIfEmailExists(personalCashAccountDTO);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfEmailExists_ReturnFalse() {

        //Arrange
        setUp();
        String description = "Cash account";
        double balance = 10;
        AccountDTO personalCashAccountDTO = new AccountDTO(personEmail, description, balance,
                EAccountType.PERSONAL_CASH);

        Mockito.when(personRepositoryJPAMock.existsById(personID)).thenReturn(false);

        //Act
        boolean result = personService.checkIfEmailExists(personalCashAccountDTO);

        //Assert
        assertFalse(result);
    }

    @Test
    void addEmailToProfile_IT() {
        setUp();
        Person person = new Person.Builder(personID).setAddress(new Address("rua de baixo")).setVat(new VATNumber(0)).setName(new PersonName("Miguel")).build();
        PersonJPA personJPA = new PersonJPA("luis@isep.pt"
                , "20", "luis", 123456789, "Rua tal","01-01-2020");

        EmailDTO emailDTO = new EmailDTO("novoEmail@isep.pt", "luis@isep.pt");
        //Mockito.doNothing().when(personRepositoryJPAMock).save(personJPA);
        //personRepository.saveNew(person);

        Mockito.when(personRepositoryJPAMock.existsById(personID)).thenReturn(true);
        Mockito.when(personRepositoryJPAMock.findById(personID)).thenReturn(Optional.of(personJPA));


        Result<String> expected = Result.success("novoEmail@isep.pt");

        Result<String> result = personService.addEmailToProfile(emailDTO);

        //assertEquals(expected, result);

        assertThat(result).isEqualTo(expected);
    }


}

package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.utils.Result;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FamilyRelationshipServiceItTest {

    private IFamilyRepository familyRepository = Mockito.mock(IFamilyRepository.class);
    private IPersonRepository personRepository = Mockito.mock(IPersonRepository.class);

    private AssemblerToDTO assembler = new AssemblerToDTO();

    private IFamilyRelationshipService familyRelationshipService =
            new FamilyRelationshipService(personRepository, familyRepository, assembler);

    private Family family;
    private Person personOneAna;
    private String anaEmail;
    private String joseEmail;
    private Person personTwoJose;

    private Family otherFamily;
    private Person personMaria;
    private Person personRoberto;
    private String mariaEmail;
    private String robertoEmail;

    @BeforeEach
    void setup() {
        family = new Family.Builder(new FamilyID("1")).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-04-1993"))
                .build();
        anaEmail = "ana@gmail.com";
        personOneAna = new Person.Builder(new PersonID(new Email(anaEmail))).setName(new PersonName("Ana")).setVat(new VATNumber(123456789))
                .setAddress(new Address("RuaDaAna")).setBirthDate(new BirthDate("25-03-1996")).setFamilyId(new FamilyID("1")).build();
        joseEmail = "jose@gmail.com";
        personTwoJose = new Person.Builder(new PersonID(new Email(joseEmail))).setFamilyId(new FamilyID("1"))
                .setName(new PersonName("Jose")).setVat(new VATNumber(502011378)).setBirthDate(new BirthDate("13-06-1992")).setAddress(new Address("RuaDoJose")).build();

        otherFamily = new Family.Builder(new FamilyID("2")).setName(new FamilyName("Dias")).setAdminId(new PersonID(new Email("admin2@gmail.com")))
                .setRegistrationDate(new RegistrationDate("25-04-1994")).build();

        mariaEmail = "maria@gmail.com";
        personMaria = new Person.Builder(new PersonID(new Email(mariaEmail))).setFamilyId(new FamilyID("2"))
                .setName(new PersonName("Maria")).setVat(new VATNumber(123456789)).setAddress(new Address("RuaDaMaria"))
                .setBirthDate(new BirthDate("25-03-1996")).build();

        robertoEmail = "roberto@gmail.com";
        personRoberto = new Person.Builder(new PersonID(new Email(robertoEmail))).setName(new PersonName("Roberto")).setFamilyId(new FamilyID("2"))
                .setVat(new VATNumber(502011378)).setBirthDate(new BirthDate("13-06-1992")).build();
    }

    @Test
    void createAndSaveFamilyRelationship_successCase() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(anaEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(joseEmail)))).
                thenReturn(Optional.of(personTwoJose));


        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setDesignation(designation);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertTrue(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_nullInputDTO() {

        //create argument DTO.
        RelationshipDTO relationshipDTO = null;

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_nullPersonOneEmail() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(null);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_invalidPersonOneEmail() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail("joana@@@@@@gmail.com");
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_nullPersonTwoEmail() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(null);
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_invalidPersonTwoEmail() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail("jose@@gmail.com");
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_nullDesignation() {

        //arrange
        String familyID = "1";
        String designation = null;

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_invalidDesignation() {

        //arrange
        String familyID = "1";
        String designation = "mama";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_nullFamilyID() {

        //arrange
        String familyID = null;
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_familyDoesNotExist() {

        //arrange
        String familyID = "notAnExistingFamily";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.empty());

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_PersonOneDoesNotExist() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail("eliza@gmail.com");
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email("eliza@gmail.com")))).
                thenReturn(Optional.empty());
        Mockito.when(personRepository.findByID(new PersonID(new Email(joseEmail)))).
                thenReturn(Optional.of(personTwoJose));

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_PersonTwoDoesNotExist() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail("eliza@gmail.com");
        relationshipDTO.setFamilyId(familyID);

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(anaEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email("eliza@gmail.com")))).
                thenReturn(Optional.empty());

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_PersonsDoNotBelongToTheSameFamily() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(robertoEmail);
        relationshipDTO.setFamilyId(familyID);

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(anaEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(robertoEmail)))).
                thenReturn(Optional.of(personRoberto));

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void createAndSaveFamilyRelationship_failCaseRest_relationshipBetweenPersonsAlreadyExists() {

        //arrange
        String familyID = "1";
        String designation = "sister";

        family.createRelationship(new PersonID(new Email(anaEmail)), new PersonID(new Email(joseEmail)),
                new FamilyRelationType("mother"), new FamilyRelationshipID("rel1"));

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setDesignation(designation);
        relationshipDTO.setFirstPersonEmail(anaEmail);
        relationshipDTO.setSecondPersonEmail(joseEmail);
        relationshipDTO.setFamilyId(familyID);

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));
        Mockito.when(personRepository.findByID(new PersonID(new Email(anaEmail)))).
                thenReturn(Optional.of(personOneAna));
        Mockito.when(personRepository.findByID(new PersonID(new Email(joseEmail)))).
                thenReturn(Optional.of(personTwoJose));

        //act
        Result result = familyRelationshipService.createAndSaveRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void getRelationshipDto_successCase() {
        //arrange
        String familyID = "1";
        String relationshipId = "rel1";

        family.createRelationship(new PersonID(new Email(anaEmail)), new PersonID(new Email(joseEmail)),
                new FamilyRelationType("mother"), new FamilyRelationshipID(relationshipId));

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));


        //act
        Result result = familyRelationshipService.getRelationshipDto(familyID, relationshipId);

        //assert
        assertTrue(result.isSuccess());
    }

    @Test
    void getRelationshipDto_failCase_RelationshipNotFound() {
        //arrange
        String familyID = "1";
        String relationshipId = "rel1";

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));

        //act
        Result result = familyRelationshipService.getRelationshipDto(familyID, relationshipId);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void getRelationshipDto_failCase_FamilyNotFound() {
        //arrange
        String familyID = "notFound";
        String relationshipId = "rel1";

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.empty());

        //act
        Result result = familyRelationshipService.getRelationshipDto(familyID, relationshipId);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void getRelationshipDto_failCase_InvalidRelationshipIdPassed() {
        //arrange
        String familyID = "1";
        String relationshipId = "";

        //act
        Result result = familyRelationshipService.getRelationshipDto(familyID, relationshipId);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void getRelationshipDto_failCase_InvalidFamilyId() {
        //arrange
        String familyID = "";
        String relationshipId = "rel1";

        //act
        Result result = familyRelationshipService.getRelationshipDto(familyID, relationshipId);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void getAllRelationshipsDto_successCase() {
        //arrange
        String familyID = "1";

        family.createRelationship(new PersonID(new Email(anaEmail)), new PersonID(new Email(joseEmail)),
                new FamilyRelationType("mother"), new FamilyRelationshipID("rel1"));

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));

        //act
        Result result = familyRelationshipService.getAllRelationshipsDto(familyID);

        //assert
        assertTrue(result.isSuccess());
    }

    @Test
    void getAllRelationshipsDto_failCase_FamilyDoesNotExist() {

        Mockito.when(familyRepository.findByID(new FamilyID("54"))).thenReturn(Optional.empty());

        Result result = familyRelationshipService.getAllRelationshipsDto("54");

        assertFalse(result.isSuccess());
    }

    @Test
    void getAllRelationshipsDto_failCase_nullFamilyId() {
        String stringFamilyId = null;

        Result result = familyRelationshipService.getAllRelationshipsDto(stringFamilyId);

        assertFalse(result.isSuccess());
    }


    @Test
    void changeRelationship_successCase_mother_to_sister() {
        //arrange
        String familyID = "1";
        String relId = "rel1";
        String newDesignation = "sister";

        family.createRelationship(new PersonID(new Email(anaEmail)), new PersonID(new Email(joseEmail)),
                new FamilyRelationType("mother"), new FamilyRelationshipID("rel1"));

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setDesignation(newDesignation);
        relationshipDTO.setRelationshipId(relId);

        //act
        Result result = familyRelationshipService.changeRelationship(relationshipDTO);

        //assert
        assertTrue(result.isSuccess());
    }

    @Test
    void changeRelationship_failCase_relationshipDoesNotExist() {
        //arrange
        String familyID = "1";
        String relId = "DoesNotExist";
        String newDesignation = "sister";

        family.createRelationship(new PersonID(new Email(anaEmail)), new PersonID(new Email(joseEmail)),
                new FamilyRelationType("mother"), new FamilyRelationshipID("rel1"));

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.of(family));

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setDesignation(newDesignation);
        relationshipDTO.setRelationshipId(relId);

        //act
        Result result = familyRelationshipService.changeRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

    @Test
    void changeRelationship_failCase_familyDoesNotExist() {
        //arrange
        String familyID = "DoesNotExist";
        String relId = "rel1";
        String newDesignation = "sister";

        Mockito.when(familyRepository.findByID(new FamilyID(familyID))).thenReturn(Optional.empty());

        //create argument DTO.
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setDesignation(newDesignation);
        relationshipDTO.setRelationshipId(relId);

        //act
        Result result = familyRelationshipService.changeRelationship(relationshipDTO);

        //assert
        assertFalse(result.isSuccess());
    }

}
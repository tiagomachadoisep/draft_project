package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import switchtwentytwenty.project.applicationservices.iassemblers.ICategoryAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepository;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.aggregates.category.CustomCategory;
import switchtwentytwenty.project.domain.aggregates.category.StandardCategory;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class CategoryServiceTest {

    private ICategoryRepository repositoryMock = Mockito.mock(ICategoryRepository.class);
    private ICategoryAssembler assembler = Mockito.mock(ICategoryAssembler.class);
    private AssemblerToDTO assemblerToDTO = Mockito.mock(AssemblerToDTO.class);
    private ApplicationContext applicationContext = Mockito.mock(ApplicationContext.class);
    private ICategoryRepositoryHttp standardCategoryAdapter = Mockito.mock(ICategoryRepositoryHttp.class);
    private CategoryService service = new CategoryService(repositoryMock, assembler, assemblerToDTO);

    @Test
    void addAValidStandardCategory() {
        //Arrange
        String categoryName = "Travel";
        String idExpected = "1";
        CategoryDTO categoryDto = new CategoryDTO();
        categoryDto.setName(categoryName);
        categoryDto.setId(idExpected);

        Categorable newCategory =
                new StandardCategory.Builder(new CategoryID("1")).setName(new CategoryName(categoryName))
                        .setParent(null).build();

        Mockito.when(repositoryMock.existsCategory(null, new CategoryName(categoryName), true)).thenReturn(false);
        Mockito.when(assembler.assembleCategoryId()).thenReturn(new CategoryID(idExpected));
        Mockito.when(assembler.assembleCategoryName(categoryDto)).thenReturn(new CategoryName(categoryName));
        Mockito.when(assemblerToDTO.toCategoryDTO(newCategory)).thenReturn(categoryDto);

        //ACT
        CategoryDTO realId = service.addStandardCategory(categoryDto);

        //assert
        assertNotNull(realId);
        Mockito.verify(repositoryMock, times(1)).saveNew(newCategory);

    }

    @Test
    void failAddingStandardCategory_AlreadyExists() {
        //Arrange
        String categoryName = "Travel";
        String idExpected = "1";
        CategoryDTO categoryDto = new CategoryDTO();
        categoryDto.setName(categoryName);
        categoryDto.setId(idExpected);


        Mockito.when(repositoryMock.existsCategory(null, new CategoryName(categoryName), true)).thenReturn(true);
        //assert
        assertThrows(DuplicatedValueException.class, () -> {
            service.addStandardCategory(categoryDto);
        });
    }

    @Test
    void addAValidStandardSubCategory() {
        //Arrange
        String subCategoryName = "Bus";
        String parentId = "1";

        CategoryDTO categoryDto = new CategoryDTO();
        categoryDto.setName(subCategoryName);
        categoryDto.setParentId(parentId);
        categoryDto.setId("2222");

        Categorable category =
                new StandardCategory.Builder(new CategoryID("2222")).setName(new CategoryName(subCategoryName)).setParent(new CategoryID(parentId)).build();

        Mockito.when(repositoryMock.existsID(new CategoryID(parentId))).thenReturn(true);
        Mockito.when(repositoryMock.existsCategory("1", new CategoryName(subCategoryName), true)).thenReturn(false);
        Mockito.when(assembler.assembleCategoryId()).thenReturn(new CategoryID("2222"));
        Mockito.when(assembler.assembleCategoryName(categoryDto)).thenReturn(new CategoryName(subCategoryName));
        Mockito.when(assembler.assembleParentCategoryId(categoryDto)).thenReturn(new CategoryID(parentId));
        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(categoryDto);

        //ACT
        CategoryDTO actual = service.addStandardSubCategory(categoryDto);

        assertNotNull(actual);
        Mockito.verify(repositoryMock, times(1)).saveNew(category);

    }


    @Test
    void failAddingSubcategoryParentCategoryDoesNotExist() {
        //Arrange
        String subCategoryName = "Bus";
        String parentId = "1";
        CategoryDTO categoryDto = new CategoryDTO();
        categoryDto.setName(subCategoryName);
        categoryDto.setParentId(parentId);

        //ACT + ASSERT
        assertThrows(NoSuchElementException.class, () -> {
            service.addStandardSubCategory(categoryDto);
        });
    }


    @Test
    void failAddingSubcategoryAlreadyExists() {
        //Arrange
        String subCategoryName = "Hull";
        String parentId = "1";

        CategoryDTO categoryDto = new CategoryDTO();
        categoryDto.setName(subCategoryName);
        categoryDto.setParentId(parentId);


        Mockito.when(repositoryMock.existsID(new CategoryID(parentId))).thenReturn(true);
        Mockito.when(repositoryMock.existsCategory(parentId, new CategoryName(subCategoryName), true)).thenReturn(true);

        //Assert
        assertThrows(DuplicatedValueException.class, () -> {
            service.addStandardSubCategory(categoryDto);
        });

    }

    @Test
    void findsCategoryById() {

        //ARRANGE
        CategoryDTO dto = new CategoryDTO();
        dto.setName("Sports");
        dto.setId("1");

        Categorable category =
                new StandardCategory.Builder(new CategoryID("1")).setName(new CategoryName("Sports")).build();
        Optional<Categorable> expected = Optional.of(category);

        Mockito.when(repositoryMock.findByID(new CategoryID("1"))).thenReturn(expected);
        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(dto);


        //ACT
        CategoryDTO actual = service.getCategoryById("1");
        //ASSERT
        assertEquals(dto, actual);
    }

    @Test
    void doesNotFindCategoryById() {

        //ARRANGE
        Mockito.when(repositoryMock.findByID(new CategoryID("1"))).thenReturn(Optional.empty());

        //ACT - assert
        assertThrows(NoSuchElementException.class, () -> {
            service.getCategoryById("1");
        });
    }

    @Test
    void addCustomCategory_ValidCategory() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setFamilyId("1111");
        categoryDTO.setId("2222");
        categoryDTO.setName("Groceries");
        categoryDTO.setParentId(null);
        Categorable category =
                new CustomCategory.Builder(new CategoryID("2222")).setFamily(new FamilyID("1111")).setName(new CategoryName("Groceries")).setParent(null).build();


        Mockito.when(repositoryMock.existsCategory(null, new CategoryName("Groceries"), false)).thenReturn(false);
        Mockito.when(assembler.assembleCategoryName(categoryDTO)).thenReturn(new CategoryName("Groceries"));
        Mockito.when(assembler.assembleCategoryId()).thenReturn(new CategoryID("2222"));
        Mockito.when(assembler.assembleFamilyId(categoryDTO)).thenReturn(new FamilyID("1111"));

        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(categoryDTO);

        //Act
        CategoryDTO result = service.addCustomCategory(categoryDTO);

        //Assert
        Mockito.verify(repositoryMock, times(1)).saveNew(category);
        assertNotNull(result);

    }

    @Test
    void addCustomCategory_CategoryAlreadyExists() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setFamilyId("1111");
        categoryDTO.setId("2222");
        categoryDTO.setName("Groceries");
        categoryDTO.setParentId(null);

        Mockito.when(repositoryMock.existsCategory(null, new CategoryName("Groceries"), false)).thenReturn(true);

        //Act+Assert
        assertThrows(DuplicatedValueException.class, () -> {
            service.addCustomCategory(categoryDTO);
        });

    }

    @Test
    void addCustomSubCategory_ValidCategory() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setFamilyId("1111");
        categoryDTO.setId("2222");
        categoryDTO.setName("Groceries");
        categoryDTO.setParentId("3333");
        Categorable category =
                new CustomCategory.Builder(new CategoryID("2222")).setFamily(new FamilyID("1111")).setName(new CategoryName("Groceries")).setParent(new CategoryID("3333")).build();

        Mockito.when(repositoryMock.existsID(new CategoryID("3333"))).thenReturn(true);
        Mockito.when(repositoryMock.existsCategory(null, new CategoryName("Groceries"), false)).thenReturn(false);
        Mockito.when(assembler.assembleCategoryName(categoryDTO)).thenReturn(new CategoryName("Groceries"));
        Mockito.when(assembler.assembleCategoryId()).thenReturn(new CategoryID("2222"));
        Mockito.when(assembler.assembleParentCategoryId(categoryDTO)).thenReturn(new CategoryID("3333"));
        Mockito.when(assembler.assembleFamilyId(categoryDTO)).thenReturn(new FamilyID("1111"));
        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(categoryDTO);

        //Act
        CategoryDTO result = service.checksParentIdAndAddsCustomSubcategory(categoryDTO);

        //Assert
        assertNotNull(result);
        Mockito.verify(repositoryMock, times(1)).saveNew(category);

    }

    @Test
    void addCustomSubCategory_CategoryAlreadyExists() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setFamilyId("1111");
        categoryDTO.setId("2222");
        categoryDTO.setName("Groceries");
        categoryDTO.setParentId("3333");

        Mockito.when(repositoryMock.existsID(new CategoryID("3333"))).thenReturn(true);
        Mockito.when(repositoryMock.existsCategory("3333", new CategoryName("Groceries"), false)).thenReturn(true);

        //Act+Assert
        assertThrows(DuplicatedValueException.class, () -> {
            service.addCustomSubCategory(categoryDTO);
        });

    }

    @Test
    void addCustomSubCategory_ParentIdDoesNotExist() {

        //Arrange
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setFamilyId("1111");
        categoryDTO.setId("2222");
        categoryDTO.setName("Groceries");
        categoryDTO.setParentId(null);

        Mockito.when(repositoryMock.existsID(new CategoryID("3333"))).thenReturn(false);


        //Act+Assert
        assertThrows(NoSuchElementException.class, () -> {
            service.checksParentIdAndAddsCustomSubcategory(categoryDTO);
        });
    }

    @Test
    void getFamilyCategoryByCategoryId_Successful() {

        //Arrange
        String familyID = "dsqq";
        String id = "123f";
        CategoryDTO expected = new CategoryDTO();
        expected.setFamilyId(familyID);
        expected.setId(id);
        expected.setName("Supermarket");
        expected.setParentId("111");

        Categorable category =
                new CustomCategory.Builder(new CategoryID(id)).setFamily(new FamilyID(familyID)).setName(new CategoryName("Supermarket")).setParent(new CategoryID("111")).build();

        Mockito.when(repositoryMock.findByID(new CategoryID(id))).thenReturn(Optional.of(category));
        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(expected);
        //Act
        CategoryDTO result = service.getFamilyCategoryByCategoryId(familyID, id);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void getFamilyCategoryByCategoryId_UnsuccessfulBecauseCategoryDoesNotExist() {

        //Arrange
        String familyID = "dsqq";
        String id = "123f";
        CategoryDTO expected = new CategoryDTO();
        expected.setFamilyId(familyID);
        expected.setId(id);
        expected.setName("Supermarket");
        expected.setParentId("111");

        Categorable category =
                new CustomCategory.Builder(new CategoryID(id)).setFamily(new FamilyID(familyID)).setName(new CategoryName("Supermarket")).setParent(new CategoryID("111")).build();

        Mockito.when(repositoryMock.findByID(new CategoryID(id))).thenReturn(Optional.empty());
        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(expected);

        //Act+Assert
        assertThrows(NoSuchElementException.class, () -> {
            service.getFamilyCategoryByCategoryId(familyID, id);
        });

    }

    @Test
    void getFamilyCategoryByCategoryId_UnsuccessfulBecauseCategoryDoesNotBelongToThisFamily() {

        //Arrange
        String familyID = "dsqq";
        String id = "123f";
        CategoryDTO expected = new CategoryDTO();
        expected.setFamilyId("xyz");
        expected.setId(id);
        expected.setName("Supermarket");
        expected.setParentId("111");
        Categorable category =
                new CustomCategory.Builder(new CategoryID(id)).setFamily(new FamilyID("xyz")).setName(new CategoryName("Supermarket")).setParent(new CategoryID("111")).build();


        Mockito.when(repositoryMock.findByID(new CategoryID(id))).thenReturn(Optional.of(category));
        Mockito.when(assemblerToDTO.toCategoryDTO(category)).thenReturn(expected);
        //Act+Assert
        assertThrows(IllegalStateException.class, () -> service.getFamilyCategoryByCategoryId(familyID, id));

    }

    @Test
    @DisplayName("Success - get the list of categories.")
    void getListOfCategories_success() {
        //Arrange
        FamilyID familyID = new FamilyID("123");
        String familyId = "12";
        CategoryDTO category1 = new CategoryDTO();
        category1.setId("1");
        category1.setName("Health");
        category1.setParentId(null);
        category1.setFamilyId(familyId);

        CategoryDTO category2 = new CategoryDTO();
        category2.setId("2");
        category2.setName("Supermarket");
        category2.setParentId(null);
        category2.setFamilyId(familyId);

        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        categoryDTOList.add(category1);
        categoryDTOList.add(category2);

        Categorable categoryTwo =
                new CustomCategory.Builder(new CategoryID("2")).setFamily(new FamilyID("12")).setName(new CategoryName("Supermarket")).setParent(new CategoryID(null)).build();

        Categorable categoryOne =
                new CustomCategory.Builder(new CategoryID("1")).setFamily(new FamilyID("12")).setName(new CategoryName("Health")).setParent(new CategoryID(null)).build();

        List<Categorable> categorableList = new ArrayList<>();
        categorableList.add(categoryOne);
        categorableList.add(categoryTwo);

        Mockito.when(repositoryMock.getListOfCategoriesByFamilyId(familyID)).thenReturn(categorableList);
        Mockito.when(assemblerToDTO.toCategoryDTO(categoryOne)).thenReturn(category1);
        Mockito.when(assemblerToDTO.toCategoryDTO(categoryTwo)).thenReturn(category2);

        //Act
        List<CategoryDTO> result = service.getListOfCategories(familyID.toString());
        //Assert
        assertEquals(categoryDTOList, result);

    }


/*
    @Test
    void importAll() {

        Mockito.when(applicationContext.getBean("repositoryhttp.xml", ICategoryRepositoryHttp.class)).thenReturn(new CategoryFromGitRepositoryHttp());
        Mockito.when(standardCategoryAdapter.importCategories()).thenReturn(new ArrayList<>());
        Mockito.when(repositoryMock.getStandardCategoryTree()).thenReturn(new ArrayList<>());
        List<CategoryDTO> expected = new ArrayList<>();

        //ACT
        List<CategoryDTO> actual = service.importAllCategories();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void importAllStandard() {

        CategoryDTO dto = new CategoryDTO();
        dto.setName("something");
        dto.setId("1");

        CategoryDTO dto2 = new CategoryDTO();
        dto2.setName("skhef");
        dto2.setId("2");

        List<CategoryDTO> external = new ArrayList<>();
        external.add(dto);
        external.add(dto2);

        Mockito.when(standardCategoryAdapter.importCategories()).thenReturn(external);
        Mockito.when(repositoryMock.getStandardCategoryTree()).thenReturn(new ArrayList<>());

        //ACT
        List<CategoryDTO> actual = service.importAllCategories();
        //Assert
        assertEquals(external, actual);
    }
*/
}




package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.implassemblers.AccountAssembler;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.BankSavingsAccount;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.exceptions.PersonAlreadyHasCashAccountException;
import switchtwentytwenty.project.persistence.assemblers.implementations.AccountAssemblerJPA;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IAccountAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.repositories.AccountRepository;
import switchtwentytwenty.project.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static switchtwentytwenty.project.domain.shared.EAccountType.BANK;

public class AccountServiceItTest {

    private static AccountRepository accountRepository;
    private static PersonRepository personRepository;
    private static AccountService accountService;
    private static AccountAssembler accountAssembler;
    private static IAccountAssemblerJPA accountAssemblerJPA;

    private static IAccountRepositoryJPA iAccountRepositoryJPAMock;

    private static AssemblerToDTO assemblerToDTO;
    private static AccountDTO accountDTOOne;
    private static AccountDTO accountDTOTwo;
    private static AccountDTO accountDTOThree;

    void setUpPersonalCashAccount() {
        assemblerToDTO = new AssemblerToDTO();
        accountAssembler = new AccountAssembler();
        accountAssemblerJPA = new AccountAssemblerJPA();
        iAccountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
        accountRepository = new AccountRepository(accountAssemblerJPA, iAccountRepositoryJPAMock);
        accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO,personRepository);
    }

    void setUpAccounts() {
        // iAccountFactory = new AccountConcreteFactory();
        accountAssemblerJPA = new AccountAssemblerJPA();
        iAccountRepositoryJPAMock = Mockito.mock(IAccountRepositoryJPA.class);
        accountRepository = new AccountRepository(accountAssemblerJPA, iAccountRepositoryJPAMock);

        accountAssembler = new AccountAssembler();
        assemblerToDTO = new AssemblerToDTO();
        accountService = new AccountService(accountRepository, accountAssembler, assemblerToDTO,personRepository);


        accountDTOOne = new AccountDTO("luis@isep.ipp.pt", "BankLuis", 150, "1", EAccountType.CREDIT_CARD);
        accountDTOTwo = new AccountDTO("mimi@epev.pt", "SavingsMimi", 200, "3", EAccountType.BANK_SAVINGS);
        accountDTOThree = new AccountDTO("luis@isep.ipp.pt", "CCLuis", 250, "2", EAccountType.CREDIT_CARD);

    }

    @Test
    void withPersonID_GetAllAccounts() {
        setUpAccounts();
        PersonID personID = new PersonID(new Email("luis@isep.ipp.pt"));

        Mockito.when(iAccountRepositoryJPAMock.count()).thenReturn(0L);

        accountService.addBankSavingsAccount(accountDTOOne);
        accountService.addBankSavingsAccount(accountDTOTwo);
        accountService.addCreditCardAccount(accountDTOThree);

        List<AccountDTO> expected = new ArrayList<>();
        expected.add(accountDTOOne);
        expected.add(accountDTOThree);

        List<AccountJPA> mockJPAlist = new ArrayList<>();
        AccountJPA accountJPAOne = new AccountJPA(new AccountID(accountDTOOne.getAccountId()), personID, "BankLuisJPA"
                , 20,
                EAccountType.CREDIT_CARD);
        AccountJPA accountJPATwo = new AccountJPA(new AccountID(accountDTOThree.getAccountId()), personID, "CCLuisJPA"
                , 50,
                EAccountType.CREDIT_CARD);
        mockJPAlist.add(accountJPAOne);
        mockJPAlist.add(accountJPATwo);

        Mockito.when(iAccountRepositoryJPAMock.findAllByPersonID(personID)).thenReturn(mockJPAlist);
        List<AccountDTO> result = accountService.findAllAccountsByOwner(personID);

        assertEquals(expected, result);
    }

    @Test
    void withAccountID_checkAccountBalance() {
        setUpAccounts();
        accountService.addBankSavingsAccount(accountDTOOne);
        AccountDTO output = accountService.addBankSavingsAccount(accountDTOTwo);
        accountService.addCreditCardAccount(accountDTOThree);

        Account newBankSavingsAccount = new BankSavingsAccount.Builder(new AccountID(output.getAccountId())).setBalance(new MoneyValue(output.getBalance())).setOwnerID(new PersonID(new Email(output.getOwnerId())))
                .setDescription(new Description(output.getDescription())).setAccountType().build();

        //Account bankSavings = accountAssembler.assembleBankSavingsAccount(accountDTOTwo);
        AccountJPA bankSavingsJPA = accountAssemblerJPA.accountToAccountJPA(newBankSavingsAccount);

        MoneyValue expected = new MoneyValue(200);

        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(new AccountID("2"))).thenReturn(bankSavingsJPA);
        MoneyValue result = accountService.checkAccountBalance(new AccountID("2"));

        assertEquals(expected, result);

    }

    @Test
    void withAccountID_getAccount() {
        setUpAccounts();
        accountService.addBankSavingsAccount(accountDTOOne);
        accountService.addBankSavingsAccount(accountDTOTwo);
        accountService.addCreditCardAccount(accountDTOThree);

        AccountDTO expected = accountDTOThree;

        Account creditCard = accountAssembler.assembleCreditCardAccount(accountDTOThree);
        AccountJPA creditCardJPA = accountAssemblerJPA.accountToAccountJPA(creditCard);
        Mockito.when(iAccountRepositoryJPAMock.findByAccountID(new AccountID("3"))).thenReturn(creditCardJPA);

        Optional<AccountDTO> result = accountService.getAccountByID("3");

        assertEquals(expected, result.get());

    }


}

package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.parameters.P;
import switchtwentytwenty.project.applicationservices.iassemblers.IAccountAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IAccountRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.account.*;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.exceptions.PersonAlreadyHasCashAccountException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class AccountServiceTest {

    private IAccountRepository iAccountRepositoryMock;
    private IPersonRepository iPersonRepositoryMock;
    private IAccountAssembler iAccountAssemblerMock;
    private AccountService accountService;
    private AssemblerToDTO assemblerToDTO;

    @BeforeEach
    void setUp() {
        iAccountRepositoryMock = Mockito.mock(IAccountRepository.class);
        iPersonRepositoryMock = Mockito.mock(IPersonRepository.class);
        iAccountAssemblerMock = Mockito.mock(IAccountAssembler.class);
        assemblerToDTO = Mockito.mock(AssemblerToDTO.class);
        accountService = new AccountService(iAccountRepositoryMock, iAccountAssemblerMock, assemblerToDTO,iPersonRepositoryMock);

    }

    @Test
    void createPersonalCashAccount_ValidAccount() {

        //Arrange
        PersonID personID = new PersonID(new Email("henrique@hotmail.com"));
        Description description = new Description("Cash account");
        MoneyValue balance = new MoneyValue(19.5);
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO();
        inputPersonalCashAccountDTO.setOwnerId("henrique@hotmail.com");
        inputPersonalCashAccountDTO.setDescription(description.toString());
        inputPersonalCashAccountDTO.setBalance(balance.getValue());
        AccountID accountID = new AccountID("id");
        PersonalCashAccount personalCashAccount =
                new PersonalCashAccount.Builder(accountID).setAccountType().setBalance(balance)
                        .setDescription(description).setOwnerID(personID).build();

        AccountDTO outputPersonalCashAccountDTO = new AccountDTO();
        outputPersonalCashAccountDTO.setAccountId("id");
        outputPersonalCashAccountDTO.setAccountType(EAccountType.PERSONAL_CASH);
        outputPersonalCashAccountDTO.setBalance(19.5);
        outputPersonalCashAccountDTO.setDescription("Cash account");
        outputPersonalCashAccountDTO.setOwnerId("henrique@hotmail.com");

        Mockito.when(iAccountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(false);
        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(new AccountID("id"));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(inputPersonalCashAccountDTO)).thenReturn(description);
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(inputPersonalCashAccountDTO)).thenReturn(balance);
        Mockito.when(assemblerToDTO.toPersonalCashAccountDTO(personalCashAccount)).thenReturn(outputPersonalCashAccountDTO);

        //Act
        AccountDTO result = accountService.createPersonalCashAccount(inputPersonalCashAccountDTO);

        //Assert
        assertNotNull(result);
        verify(iAccountRepositoryMock, times(1)).saveNew(personalCashAccount);

    }

    @Test
    void createPersonalCashAccount_PersonHasACashAccount() {

        //Arrange
        PersonID personID = new PersonID(new Email("henrique@hotmail.com"));
        AccountDTO inputPersonalCashAccountDTO = new AccountDTO();
        inputPersonalCashAccountDTO.setOwnerId("henrique@hotmail.com");
        inputPersonalCashAccountDTO.setDescription("Cash account");
        inputPersonalCashAccountDTO.setBalance(19.50);

        Mockito.when(iAccountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);

        //Act+Assert
        assertThrows(PersonAlreadyHasCashAccountException.class, () -> accountService.createPersonalCashAccount(inputPersonalCashAccountDTO));
    }

    @Test
    void withAccountID_checkAccountBalance_UT() {
        AccountID accountID = new AccountID("20");

        Account account = new CreditCardAccount.Builder(accountID).setOwnerID(new PersonID(new Email("howl@mc.sg"))).
                setDescription(new Description("CC howl")).setBalance(new MoneyValue(2000)).setAccountType().build();

        MoneyValue expected = new MoneyValue(2000);

        Mockito.when(iAccountRepositoryMock.findByID(accountID)).thenReturn(Optional.ofNullable(account));
        MoneyValue result = accountService.checkAccountBalance(accountID);

        assertEquals(expected, result);
    }

    @Test
    void withStringAccountID_getAccountByID_UT() {
        Account account = new CreditCardAccount.Builder(new AccountID("20")).setOwnerID(new PersonID(new Email("howl" +
                "@mc.sg"))).
                setDescription(new Description("CC howl")).setBalance(new MoneyValue(2000)).setAccountType().build();

        AccountDTO expected = new AccountDTO("howl@mc.sg", "CC howl", 2000, "20", EAccountType.CREDIT_CARD);

        Mockito.when(iAccountRepositoryMock.findByID(new AccountID("20"))).thenReturn(Optional.ofNullable(account));
        Mockito.when(assemblerToDTO.toAccountDTO(account)).thenReturn(expected);

        Optional<AccountDTO> result = accountService.getAccountByID("20");

        assertEquals(expected, result.get());
    }

    @Test
    void withStringAccountID_getAccountByID_ReturnsEmpty() {

        Mockito.when(iAccountRepositoryMock.findByID(new AccountID("20"))).thenReturn(Optional.empty());

        Optional<AccountDTO> result = accountService.getAccountByID("20");

        assertFalse(result.isPresent());
    }

    @Test
    void addCredit() {
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "CC howl", 2000, EAccountType.CREDIT_CARD);
        Account account =
                new CreditCardAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(2000)).setDescription(new Description("CC howl")).setOwnerID(new PersonID(new Email("howl@mc.sg"))).build();

        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(new AccountID("20"));
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(accountDTO)).thenReturn(new MoneyValue(2000));
        Mockito.when(iAccountAssemblerMock.getPersonIDFromAccountDTO(accountDTO)).thenReturn(new PersonID(new Email(
                "howl@mc.sg")));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(accountDTO)).thenReturn(new Description("CC " +
                "howl"));
        Mockito.when(assemblerToDTO.toAccountDTO(account)).thenReturn(accountDTO);

        AccountDTO accountDTO1 = accountService.addCreditCardAccount(accountDTO);

        assertNotNull(accountDTO1);
    }

    @Test
    void addBankAccount() {
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "CC howl", 2000, EAccountType.BANK);
        Account account =
                new BankAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(2000)).setDescription(new Description("CC howl")).setOwnerID(new PersonID(new Email("howl@mc.sg"))).build();

        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(new AccountID("20"));
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(accountDTO)).thenReturn(new MoneyValue(2000));
        Mockito.when(iAccountAssemblerMock.getPersonIDFromAccountDTO(accountDTO)).thenReturn(new PersonID(new Email(
                "howl@mc.sg")));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(accountDTO)).thenReturn(new Description("CC " +
                "howl"));
        Mockito.when(assemblerToDTO.toAccountDTO(account)).thenReturn(accountDTO);

        AccountDTO accountDTO1 = accountService.addBankAccount(accountDTO);

        assertNotNull(accountDTO1);
    }

    @Test
    void addBankSavingsAccount() {
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "CC howl", 2000, EAccountType.BANK);
        Account account =
                new BankSavingsAccount.Builder(new AccountID("290")).setAccountType().setBalance(new MoneyValue(2000)).setDescription(new Description("CC howl")).setOwnerID(new PersonID(new Email("howl@mc.sg"))).build();

        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(new AccountID("290"));
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(accountDTO)).thenReturn(new MoneyValue(2000));
        Mockito.when(iAccountAssemblerMock.getPersonIDFromAccountDTO(accountDTO)).thenReturn(new PersonID(new Email(
                "howl@mc.sg")));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(accountDTO)).thenReturn(new Description("CC " +
                "howl"));
        Mockito.when(assemblerToDTO.toAccountDTO(account)).thenReturn(accountDTO);

        AccountDTO accountDTO1 = accountService.addBankSavingsAccount(accountDTO);

        assertNotNull(accountDTO1);
    }

    @Test
    void checkAccountBalanceNotExists() {
        AccountID id = new AccountID("1");
        Mockito.when(iAccountRepositoryMock.findByID(id)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> {
            accountService.checkAccountBalance(id);
        });
    }

    @Test
    void whenAddCreditCardAccount_validateSaveNewCall() {
        //
        setUp();

        AccountID accountID = new AccountID("1");
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "CC howl", 2000, "1", EAccountType.CREDIT_CARD);

        Account account = new CreditCardAccount.Builder(accountID).setOwnerID(new PersonID(new Email("howl@mc.sg"))).
                setDescription(new Description("CC howl")).setBalance(new MoneyValue(2000)).setAccountType().build();


        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(accountID);
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(accountDTO)).thenReturn(new MoneyValue(2000));
        Mockito.when(iAccountAssemblerMock.getPersonIDFromAccountDTO(accountDTO)).thenReturn(new PersonID(new Email(
                "howl@mc.sg")));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(accountDTO)).thenReturn(new Description("CC " +
                "howl"));

        accountService.addCreditCardAccount(accountDTO);
        verify(iAccountRepositoryMock, times(1)).saveNew(account);
    }

    @Test
    void whenAddBankAccount_validateSaveNewCall() {
        //
        setUp();

        AccountID accountID = new AccountID("1");
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "CC howl", 2000, "1", EAccountType.BANK);

        Account account = new BankAccount.Builder(accountID).setOwnerID(new PersonID(new Email("howl@mc.sg"))).
                setDescription(new Description("CC howl")).setBalance(new MoneyValue(2000)).setAccountType().build();


        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(accountID);
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(accountDTO)).thenReturn(new MoneyValue(2000));
        Mockito.when(iAccountAssemblerMock.getPersonIDFromAccountDTO(accountDTO)).thenReturn(new PersonID(new Email(
                "howl@mc.sg")));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(accountDTO)).thenReturn(new Description("CC " +
                "howl"));

        accountService.addBankAccount(accountDTO);
        verify(iAccountRepositoryMock, times(1)).saveNew(account);
    }

    @Test
    void whenAddBankSavingsAccount_validateSaveNewCall() {
        //
        setUp();

        AccountID accountID = new AccountID("1");
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "CC howl", 2000, "1", EAccountType.BANK_SAVINGS);

        Account account = new BankSavingsAccount.Builder(accountID).setOwnerID(new PersonID(new Email("howl@mc.sg"))).
                setDescription(new Description("CC howl")).setBalance(new MoneyValue(2000)).setAccountType().build();


        Mockito.when(iAccountAssemblerMock.assembleAccountId()).thenReturn(accountID);
        Mockito.when(iAccountAssemblerMock.getBalanceFromAccountDTO(accountDTO)).thenReturn(new MoneyValue(2000));
        Mockito.when(iAccountAssemblerMock.getPersonIDFromAccountDTO(accountDTO)).thenReturn(new PersonID(new Email(
                "howl@mc.sg")));
        Mockito.when(iAccountAssemblerMock.getDescriptionFromAccountDTO(accountDTO)).thenReturn(new Description("CC " +
                "howl"));

        accountService.addBankSavingsAccount(accountDTO);
        verify(iAccountRepositoryMock, times(1)).saveNew(account);
    }

    @Test
    void whenCreateFamilyCashAccount_validateSaveNew() {
        setUp();
        PersonID personID = new PersonID(new Email("family@mail.pt"));
        AccountDTO accountDTO = new AccountDTO("howl@mc.sg", "Family Cash", 2000, EAccountType.FAMILY_CASH);
        Account account = new FamilyCashAccount.Builder(new AccountID("1")).setOwnerID(personID).
                setDescription(new Description("FamilyCash")).setBalance(new MoneyValue(2000)).setAccountType().build();

        Mockito.when(iAccountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(false);
        Mockito.when(iAccountAssemblerMock.assembleFamilyCashAccount(accountDTO)).thenReturn(account);
        accountService.createFamilyCashAccount(accountDTO);

        verify(iAccountRepositoryMock, times(1)).saveNew(account);
        assertNotNull(accountDTO.getAccountId());

    }

    @Test
    @DisplayName("Success - Gets AccountDTO that contains only the balance")
    void getFamilyOrMemberCashAccount_Success() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("claudia@gmail.com"));
        AccountDTO accountDTO = new AccountDTO("claudia@gmail.com", "Vacation Money", 25530, EAccountType.PERSONAL_CASH);
        //Act
        PersonalCashAccount personalCashAccount = new PersonalCashAccount.Builder(new AccountID("2")).setOwnerID(ownerId)
                .setDescription(new Description("Vacation Money")).setBalance(new MoneyValue(25530)).setAccountType().build();

        Mockito.when(iAccountRepositoryMock.getFamilyOrMemberCashAccount(ownerId)).thenReturn(personalCashAccount);
        Mockito.when(assemblerToDTO.getBalanceDTO(personalCashAccount)).thenReturn(accountDTO);
        AccountDTO result = accountService.getFamilyOrMemberCashAccount(ownerId);
        //Assert
        assertEquals(accountDTO, result);
    }

    @Test
    @DisplayName("Failure - The account doesn't exist.")
    void getFamilyOrMemberCashAccount_Failure() {
        //Arrange
        PersonID ownerId = new PersonID(new Email("claudia@gmail.com"));
        //Act
        Mockito.when(iAccountRepositoryMock.getFamilyOrMemberCashAccount(ownerId)).thenReturn(null);
        Mockito.when(assemblerToDTO.getBalanceDTO(null)).thenThrow(IllegalArgumentException.class);
        //Assert
        assertThrows(IllegalArgumentException.class, () -> accountService.getFamilyOrMemberCashAccount(ownerId));
    }

    @Test
    void getsTheChildsAccount() {
        //Arrange
        String childEmail = "mail@mail.com";
        PersonID childId = new PersonID(new Email(childEmail));
        Description description = new Description("baby account");

        AccountDTO expected = new AccountDTO();
        expected.setBalance(200);

        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType()
                .setBalance(new MoneyValue(200)).setOwnerID(childId).setDescription(description).build();
        Mockito.when(iAccountRepositoryMock.findCashByOwnerId(childId)).thenReturn(Optional.of(account));
        Mockito.when(assemblerToDTO.getBalanceDTO(account)).thenReturn(expected);

        //Act
        AccountDTO actual = accountService.checkChildsCashAccountBalance(childEmail);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void theChildsDoesNotHaveCashAccount() {
        //Arrange
        String childEmail = "mail@mail.com";
        PersonID childId = new PersonID(new Email(childEmail));

        Mockito.when(iAccountRepositoryMock.findCashByOwnerId(childId)).thenReturn(Optional.empty());
        //Act + Assert
        assertThrows(NoSuchElementException.class, () -> accountService.checkChildsCashAccountBalance(childEmail));
    }

    @Test
    void getAllAccounts() {
        AccountDTO accountDTO = new AccountDTO("ze@isep.pt","other",100,"1",EAccountType.CREDIT_CARD);
        CreditCardAccount creditCardAccount = new CreditCardAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(100)).
                setDescription(new Description("other")).setOwnerID(new PersonID(new Email("ze@isep.pt"))).setAccountType().build();
        List<Account> accounts = new ArrayList<>();
        accounts.add(creditCardAccount);

        Mockito.when(iAccountRepositoryMock.findAll()).thenReturn(accounts);
        Mockito.when(iAccountAssemblerMock.assembleCreditCardAccount(accountDTO)).thenReturn(creditCardAccount);

        List<AccountDTO> expected = new ArrayList<>();
        expected.add(accountDTO);

        List<AccountDTO> result = accountService.getAllAccounts();

        assertEquals(expected,result);
    }

    @Test
    void getFamilyAccounts() {
        String familyId = "1";
        AccountDTO accountDTO = new AccountDTO("ze@isep.pt","other",100,"1",EAccountType.CREDIT_CARD);
        CreditCardAccount creditCardAccount = new CreditCardAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(100)).
                setDescription(new Description("other")).setOwnerID(new PersonID(new Email("ze@isep.pt"))).setAccountType().build();
        List<Account> accounts = new ArrayList<>();
        accounts.add(creditCardAccount);
        Person person = new Person.Builder(new PersonID(new Email("ze@isep.pt"))).setAddress(new Address("other")).setBirthDate(new BirthDate("01-01-1900")).
                setFamilyId(new FamilyID(familyId)).setName(new PersonName("ze")).setVat(new VATNumber(0)).setRoleAsFamilyAdministrator().build();

        Mockito.when(iAccountRepositoryMock.findAll()).thenReturn(accounts);
        Mockito.when(iAccountAssemblerMock.assembleCreditCardAccount(accountDTO)).thenReturn(creditCardAccount);
        Mockito.when(iPersonRepositoryMock.findAllByFamilyID(new FamilyID(familyId))).thenReturn(Collections.singletonList(person));
        Mockito.when(iAccountRepositoryMock.findAllAccountsByOwner(new PersonID(new Email("ze@isep.pt")))).thenReturn(Collections.singletonList(creditCardAccount));

        List<AccountDTO> expected = new ArrayList<>();
        expected.add(accountDTO);

        List<AccountDTO> result = accountService.getFamilyAccounts(familyId);

        assertEquals(expected,result);
    }
}
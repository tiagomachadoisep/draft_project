package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.family.FamilyRelationship;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.utils.Result;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


class FamilyRelationshipServiceTest {

    private IPersonRepository personRepositoryMock = Mockito.mock(IPersonRepository.class);

    private IFamilyRepository familyRepositoryMock = Mockito.mock(IFamilyRepository.class);

    private AssemblerToDTO assemblerToDTO = Mockito.mock(AssemblerToDTO.class);

    private FamilyRelationshipService service = new FamilyRelationshipService(personRepositoryMock,
            familyRepositoryMock, assemblerToDTO);


    @Test
    void createAndSaveFamilyRelationshipRest_successCase() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        RelationshipDTO outputDto = new RelationshipDTO();
        outputDto.setFamilyId(familyID);
        outputDto.setFirstPersonEmail(joanaEmailString);
        outputDto.setSecondPersonEmail(ricardoEmailString);
        outputDto.setDesignation(designationSister);
        outputDto.setRelationshipId("123gf");

        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-07-2000")).build();
        Optional<Family> optionalFamily = Optional.of(family);
        Optional<Person> optionalPersonOne = Optional.of(personJoana);
        Optional<Person> optionalPersonTwo = Optional.of(personRicardo);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(personRepositoryMock.findByID(joanaID)).thenReturn(optionalPersonOne);
        Mockito.when(personRepositoryMock.findByID(ricardoID)).thenReturn(optionalPersonTwo);
        Mockito.when(assemblerToDTO.createFamilyRelationshipID()).thenReturn(new FamilyRelationshipID("123gf"));
        Mockito.doNothing().when(familyRepositoryMock).saveNew(family);
        Mockito.when(assemblerToDTO.toRelationshipDto(joanaEmailString, ricardoEmailString, familyID, designationSister,
                "123gf")).thenReturn(outputDto);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertTrue(result.isSuccess());
        verify(familyRepositoryMock, times(1)).saveNew(family);

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_relationshipAlreadyExistsBetweenMembers() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-07-2000")).build();
        family.createRelationship(joanaID, ricardoID, typeSister, new FamilyRelationshipID("232"));
        Optional<Family> optionalFamily = Optional.of(family);
        Optional<Person> optionalPersonOne = Optional.of(personJoana);
        Optional<Person> optionalPersonTwo = Optional.of(personRicardo);

        String responseMessage = "Relationship creation failed!" +
                " A relationship between the members already exists.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(personRepositoryMock.findByID(joanaID)).thenReturn(optionalPersonOne);
        Mockito.when(personRepositoryMock.findByID(ricardoID)).thenReturn(optionalPersonTwo);
        Mockito.when(assemblerToDTO.createFamilyRelationshipID()).thenReturn(new FamilyRelationshipID("123"));
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personsDoNotBelongToSameFamily_MemberTwoIsNot() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID("4")).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-07-2000")).build();
        Optional<Family> optionalFamily = Optional.of(family);
        Optional<Person> optionalPersonOne = Optional.of(personJoana);
        Optional<Person> optionalPersonTwo = Optional.of(personRicardo);

        String responseMessage = "Relationship creation failed!" +
                " The persons do not belong to the same family.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(personRepositoryMock.findByID(joanaID)).thenReturn(optionalPersonOne);
        Mockito.when(personRepositoryMock.findByID(ricardoID)).thenReturn(optionalPersonTwo);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personsDoNotBelongToSameFamily_MemberOneIsNot() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID("1")).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-07-2000")).build();
        Optional<Family> optionalFamily = Optional.of(family);
        Optional<Person> optionalPersonOne = Optional.of(personJoana);
        Optional<Person> optionalPersonTwo = Optional.of(personRicardo);

        String responseMessage = "Relationship creation failed!" +
                " The persons do not belong to the same family.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(personRepositoryMock.findByID(joanaID)).thenReturn(optionalPersonOne);
        Mockito.when(personRepositoryMock.findByID(ricardoID)).thenReturn(optionalPersonTwo);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personsOneDoesNotExist() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-07-2000")).build();
        Optional<Family> optionalFamily = Optional.of(family);
        Optional<Person> optionalPersonOne = Optional.empty();
        Optional<Person> optionalPersonTwo = Optional.of(personRicardo);

        String responseMessage = "Relationship creation failed!" +
                " The person(s) do not exist.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(personRepositoryMock.findByID(joanaID)).thenReturn(optionalPersonOne);
        Mockito.when(personRepositoryMock.findByID(ricardoID)).thenReturn(optionalPersonTwo);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personsTwoDoesNotExist() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);


        Family family = new Family.Builder(new FamilyID(familyID)).setName(new FamilyName("Santos"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).setRegistrationDate(new RegistrationDate("25-07-2000")).build();
        Optional<Family> optionalFamily = Optional.of(family);
        Optional<Person> optionalPersonOne = Optional.of(personJoana);
        Optional<Person> optionalPersonTwo = Optional.empty();

        String responseMessage = "Relationship creation failed!" +
                " The person(s) do not exist.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(personRepositoryMock.findByID(joanaID)).thenReturn(optionalPersonOne);
        Mockito.when(personRepositoryMock.findByID(ricardoID)).thenReturn(optionalPersonTwo);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_familyDoesNotExist() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        Person personJoana = new Person.Builder(new PersonID(new Email(joanaEmailString))).setName(new PersonName("Joana"))
                .setVat(new VATNumber(123456789)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        Person personRicardo = new Person.Builder(new PersonID(new Email(ricardoEmailString))).setName(new PersonName("Ricardo"))
                .setVat(new VATNumber(501964843)).setFamilyId(new FamilyID(familyID)).setAddress(new Address("RuaSantos"))
                .setBirthDate(new BirthDate("01-01-1990")).build();
        String designationSister = "sister";
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        Optional<Family> optionalFamily = Optional.empty();

        String responseMessage = "Relationship creation failed!" +
                " A family with the passed ID does not exist.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(familyID))).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_invalidDesignation() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID ricardoID = new PersonID(new Email(ricardoEmailString));
        String designationSister = "sssster";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        String responseMessage = "Relationship creation failed!" +
                " The relationship designation '" + designationSister + "' is invalid.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        InvalidDesignationException invalidDesignationException =
                new InvalidDesignationException("The relationship designation '" + designationSister + "' is invalid.");

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(ricardoID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenThrow(invalidDesignationException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);
        //act
        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personTwoHasInvalidID() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@@@gmail.com";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        String designationSister = "sister";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        String responseMessage = "Relationship creation failed!" +
                " Invalid Email";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Invalid Email");

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenThrow(illegalArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personOneHasInvalidID() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@@@@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        String designationSister = "sister";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        String responseMessage = "Relationship creation failed!" +
                " Invalid Email";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Invalid Email");

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenThrow(illegalArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_personsAreTheSame() {

        //arrange
        String familyID = "3";
        String joanaEmailString = "joana@gmail.com";
        String sameEmailString = "joana@gmail.com";
        String designationSister = "sister";
        PersonID joanaID = new PersonID(new Email(joanaEmailString));
        PersonID sameID = new PersonID(new Email(sameEmailString));
        FamilyRelationType typeSister = new FamilyRelationType(designationSister);

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(sameEmailString);
        relationshipDTO.setDesignation(designationSister);

        String responseMessage = "Relationship creation failed!" +
                " A self-relationship is not possible.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID(relationshipDTO.getFamilyId())).thenReturn(new FamilyID(familyID));
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getFirstPersonEmail())).thenReturn(joanaID);
        Mockito.when(assemblerToDTO.toPersonID(relationshipDTO.getSecondPersonEmail())).thenReturn(sameID);
        Mockito.when(assemblerToDTO.toFamilyRelationType(relationshipDTO.getDesignation())).thenReturn(typeSister);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_nullRelationshipDTO() {

        //arrange

        RelationshipDTO relationshipDTO = null;

        String responseMessage = "Relationship creation failed!" +
                " The passed argument cannot be null.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);


        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_nullFamilyId() {
        //arrange
        String familyID = null;
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        String designationSister = "sister";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        String responseMessage = "Relationship creation failed!" +
                " The family Id cannot be null.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        NullArgumentException nullArgumentException = new NullArgumentException("The family Id cannot be null.");

        Mockito.when(assemblerToDTO.toFamilyID(familyID)).thenThrow(nullArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void createAndSaveFamilyRelationshipRest_failCase_emptyFamilyId() {
        //arrange
        String familyID = "";
        String joanaEmailString = "joana@gmail.com";
        String ricardoEmailString = "ricardo@gmail.com";
        String designationSister = "sister";

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyID);
        relationshipDTO.setFirstPersonEmail(joanaEmailString);
        relationshipDTO.setSecondPersonEmail(ricardoEmailString);
        relationshipDTO.setDesignation(designationSister);

        String responseMessage = "Relationship creation failed!" +
                " The family Id cannot be null.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        EmptyArgumentException emptyArgumentException = new EmptyArgumentException("The family Id cannot be null.");

        Mockito.when(assemblerToDTO.toFamilyID(familyID)).thenThrow(emptyArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        Result result = service.createAndSaveRelationship(relationshipDTO);

        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }


    @Test
    void successChangingRelationship() {

        //ARRANGE
        String famId = "1";
        FamilyID familyID = new FamilyID(famId);

        Family requiredFamily = new Family.Builder(familyID).setName(new FamilyName("Martins"))
                .setAdminId(new PersonID(new Email("admin@gmail.com"))).build();

        requiredFamily.createRelationship(new PersonID(new Email("admin@gmail.com")),
                new PersonID(new Email("member@gmail.com")), new FamilyRelationType("mother"),
                new FamilyRelationshipID("rel1"));

        FamilyRelationship familyRelationship = requiredFamily.findRelationshipById(new FamilyRelationshipID("rel1")).get();

        Optional<Family> optionalFamily = Optional.of(requiredFamily);

        RelationshipDTO inputDto = new RelationshipDTO();
        inputDto.setFamilyId(famId);
        inputDto.setDesignation("cousin");
        inputDto.setRelationshipId("rel1");

        RelationshipDTO outputDto = new RelationshipDTO();
        outputDto.setRelationshipId("rel1");
        outputDto.setFamilyId(famId);
        outputDto.setDesignation("cousin");
        outputDto.setSecondPersonEmail("member@gmail.com");
        outputDto.setFirstPersonEmail("admin@gmail.com");

        Mockito.when(assemblerToDTO.toFamilyID(famId)).thenReturn(familyID);
        Mockito.when(assemblerToDTO.toFamilyRelationshipID("rel1")).thenReturn(new FamilyRelationshipID("rel1"));
        Mockito.when(assemblerToDTO.toFamilyRelationType("cousin")).thenReturn(new FamilyRelationType("cousin"));
        Mockito.when(familyRepositoryMock.findByID(familyID)).thenReturn(optionalFamily);
        Mockito.doNothing().when(familyRepositoryMock).saveNew(requiredFamily);
        Mockito.when(assemblerToDTO.toRelationshipDto(familyRelationship, familyID)).thenReturn(outputDto);

        Result expected = Result.success(outputDto);
        RelationshipDTO expectedDTO = (RelationshipDTO) expected.getContent();
        String expectedNewDesignation = expectedDTO.getDesignation();

        //ACT
        Result result = service.changeRelationship(inputDto);
        RelationshipDTO resultDTO = (RelationshipDTO) result.getContent();
        String resultNewDesignation = resultDTO.getDesignation();

        //ASSERT
        assertTrue(result.isSuccess());
        verify(familyRepositoryMock, times(1)).saveNew(requiredFamily);
        assertEquals(expected, result);
        assertEquals(expectedNewDesignation, resultNewDesignation);
    }

    @Test
    void cannotChangeRelationshipBecauseRelationshipDoesntExist() {

        //ARRANGE
        FamilyID id = new FamilyID("1");

        Family requiredFamily = new Family.Builder(id).setName(new FamilyName("Martins"))
                .setAdminId(new PersonID(new Email("1201@isep.ipp.pt"))).build();
        requiredFamily.createRelationship(new PersonID(new Email("1201@isep.ipp.pt")),
                new PersonID(new Email("133@gmail.com")), new FamilyRelationType("mother"),
                new FamilyRelationshipID("erew3"));

        Optional<Family> optionalFamily = Optional.of(requiredFamily);

        RelationshipDTO newRelationship = new RelationshipDTO();
        newRelationship.setFamilyId("1");
        newRelationship.setDesignation("cousin");
        newRelationship.setRelationshipId("esdd2asdsa");

        String responseMessage = "Could not change the relationship designation!" +
                " Relationship not found";

        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID("1")).thenReturn(new FamilyID("1"));
        Mockito.when(assemblerToDTO.toFamilyRelationshipID("esdd2asdsa")).thenReturn(new FamilyRelationshipID("esdd2asdsa"));
        Mockito.when(assemblerToDTO.toFamilyRelationType("cousin")).thenReturn(new FamilyRelationType("cousin"));
        Mockito.when(familyRepositoryMock.findByID(id)).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        //ACT
        Result result = service.changeRelationship(newRelationship);

        //ASSERT
        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void cannotChangeRelationshipDtoNull() {

        //ARRANGE
        String responseMessage = "Could not change the relationship designation!" +
                " The passed argument cannot be null.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);


        //ACT
        Result result = service.changeRelationship(null);

        //ASSERT
        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void cannotChangeRelationshipFamilyDoesNotExist() {

        //ARRANGE
        FamilyID id = new FamilyID("1");

        RelationshipDTO newRelationship = new RelationshipDTO();
        newRelationship.setFamilyId("100");
        newRelationship.setDesignation("cousin");
        newRelationship.setRelationshipId("rewrew3");

        String responseMessage = "Could not change the relationship designation!" +
                " A family with the passed ID does not exist.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        Mockito.when(assemblerToDTO.toFamilyID("1")).thenReturn(id);
        Mockito.when(assemblerToDTO.toFamilyRelationshipID("rewrew3")).thenReturn(new FamilyRelationshipID("rewrew3"));
        Mockito.when(assemblerToDTO.toFamilyRelationType("cousin")).thenReturn(new FamilyRelationType("cousin"));
        Mockito.when(familyRepositoryMock.findByID(id)).thenReturn(Optional.empty());
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        //ACT
        Result result = service.changeRelationship(newRelationship);

        //ASSERT
        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());
    }

    @Test
    void cannotChangeRelationship_FamilyIdIsNull() {

        //ARRANGE
        String idForFamily = null;

        RelationshipDTO newRelationship = new RelationshipDTO();
        newRelationship.setFamilyId(idForFamily);
        newRelationship.setDesignation("cousin");
        newRelationship.setRelationshipId("rewrew3");

        String responseMessage = "Could not change the relationship designation!" +
                " The family Id cannot be null.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        NullArgumentException nullArgumentException = new NullArgumentException("The family Id cannot be null.");

        Mockito.when(assemblerToDTO.toFamilyID(idForFamily)).thenThrow(nullArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        //ACT
        Result result = service.changeRelationship(newRelationship);

        //ASSERT
        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());

    }

    @Test
    void cannotChangeRelationship_FamilyIdIsEmpty() {

        //ARRANGE
        String idForFamily = "";

        RelationshipDTO newRelationship = new RelationshipDTO();
        newRelationship.setFamilyId(idForFamily);
        newRelationship.setDesignation("cousin");
        newRelationship.setRelationshipId("rewrew3");

        String responseMessage = "Could not change the relationship designation!" +
                " The family Id cannot be empty.";
        ExceptionDTO exceptionDTO = new ExceptionDTO(responseMessage);

        EmptyArgumentException emptyArgumentException = new EmptyArgumentException("The family Id cannot be empty.");

        Mockito.when(assemblerToDTO.toFamilyID(idForFamily)).thenThrow(emptyArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(responseMessage)).thenReturn(exceptionDTO);

        //ACT
        Result result = service.changeRelationship(newRelationship);

        //ASSERT
        assertFalse(result.isSuccess());
        ExceptionDTO exceptionDTO1finally = (ExceptionDTO) result.getContent();

        System.out.println("EXCEPTION MESSAGE: " + exceptionDTO1finally.getMessage());
    }

    @Test
    void getRelationshipDto_successCase() {
        String stringFamilyId = "famId123";
        FamilyID familyID = new FamilyID(stringFamilyId);
        String stringRelationshipId = "relId123";
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID(stringRelationshipId);

        Family family = new Family.Builder(familyID).build();
        family.createRelationship(new PersonID(new Email("one@gmail.com")),
                new PersonID(new Email("two@gmail.com")),
                new FamilyRelationType("sister"),
                familyRelationshipID);

        FamilyRelationship familyRelationship = new FamilyRelationship(
                new PersonID(new Email("one@gmail.com")),
                new PersonID(new Email("two@gmail.com")),
                new FamilyRelationType("sister"),
                familyRelationshipID
        );


        Optional<Family> optionalFamily = Optional.of(family);
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setRelationshipId(stringRelationshipId);
        relationshipDTO.setFamilyId(stringFamilyId);
        relationshipDTO.setFirstPersonEmail("one@gmail.com");
        relationshipDTO.setSecondPersonEmail("two@gmail.com");
        relationshipDTO.setDesignation("sister");

        Result expected = Result.success(relationshipDTO);

        Mockito.when(assemblerToDTO.toFamilyID(stringFamilyId)).thenReturn(familyID);
        Mockito.when(assemblerToDTO.toFamilyRelationshipID(stringRelationshipId)).thenReturn(familyRelationshipID);
        Mockito.when(familyRepositoryMock.findByID(familyID)).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toRelationshipDto(familyRelationship, familyID)).thenReturn(relationshipDTO);

        Result result = service.getRelationshipDto(stringFamilyId, stringRelationshipId);

        assertEquals(expected, result);
        assertEquals(expected.getContent(), result.getContent());
    }

    @Test
    void getRelationshipDto_failCase_RelationshipNotFound() {
        String stringFamilyId = "famId123";
        FamilyID familyID = new FamilyID(stringFamilyId);
        String stringRelationshipId = "relId123";
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID(stringRelationshipId);

        Family family = new Family.Builder(familyID).build();

        Optional<Family> optionalFamily = Optional.of(family);

        String expectedMessage = "Could not get relationship! There is no relationship with that id.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        Mockito.when(assemblerToDTO.toFamilyID(stringFamilyId)).thenReturn(familyID);
        Mockito.when(assemblerToDTO.toFamilyRelationshipID(stringRelationshipId)).thenReturn(familyRelationshipID);
        Mockito.when(familyRepositoryMock.findByID(familyID)).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);


        Result result = service.getRelationshipDto(stringFamilyId, stringRelationshipId);

        assertEquals(expected, result);
        assertEquals(expected.getContent(), result.getContent());
    }

    @Test
    void getRelationshipDto_failCase_FamilyNotFound() {
        String stringFamilyId = "famId123";
        FamilyID familyID = new FamilyID(stringFamilyId);
        String stringRelationshipId = "relId123";
        FamilyRelationshipID familyRelationshipID = new FamilyRelationshipID(stringRelationshipId);


        Optional<Family> optionalFamily = Optional.empty();

        String expectedMessage = "Could not get relationship! A family with the passed ID does not exist.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        Mockito.when(assemblerToDTO.toFamilyID(stringFamilyId)).thenReturn(familyID);
        Mockito.when(assemblerToDTO.toFamilyRelationshipID(stringRelationshipId)).thenReturn(familyRelationshipID);
        Mockito.when(familyRepositoryMock.findByID(familyID)).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);


        Result result = service.getRelationshipDto(stringFamilyId, stringRelationshipId);

        assertEquals(expected, result);
        assertEquals(expected.getContent(), result.getContent());
    }

    @Test
    void getRelationshipDto_failCase_InvalidRelationshipIdPassed() {
        String stringFamilyId = "famId123";
        FamilyID familyID = new FamilyID(stringFamilyId);
        String stringRelationshipId = null;

        String expectedMessage = "Could not get relationship! The relationship Id cannot be null.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        NullArgumentException nullArgumentException =
                new NullArgumentException("The relationship Id cannot be null.");

        Mockito.when(assemblerToDTO.toFamilyID(stringFamilyId)).thenReturn(familyID);
        Mockito.when(assemblerToDTO.toFamilyRelationshipID(stringRelationshipId)).thenThrow(nullArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);

        Result result = service.getRelationshipDto(stringFamilyId, stringRelationshipId);

        assertFalse(result.isSuccess());
        assertEquals(expected, result);
        assertEquals(expected.getContent(), result.getContent());
    }

    @Test
    void getRelationshipDto_failCase_nullFamilyId() {
        String stringFamilyId = null;
        String stringRelationshipId = "rel1";

        String expectedMessage = "Could not get relationship! The family Id cannot be null.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        NullArgumentException nullArgumentException =
                new NullArgumentException("The family Id cannot be null.");


        Mockito.when(assemblerToDTO.toFamilyID(stringFamilyId)).thenThrow(nullArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);


        Result result = service.getRelationshipDto(stringFamilyId, stringRelationshipId);

        assertFalse(result.isSuccess());
        assertEquals(expected, result);
        assertEquals(expected.getContent(), result.getContent());
    }

    @Test
    void getRelationshipDto_failCase_emptyFamilyId() {
        String stringFamilyId = "";
        String stringRelationshipId = "rel1";

        String expectedMessage = "Could not get relationship! The family Id cannot be empty.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        EmptyArgumentException emptyArgumentException =
                new EmptyArgumentException("The family Id cannot be empty.");

        Mockito.when(assemblerToDTO.toFamilyID(stringFamilyId)).thenThrow(emptyArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);


        Result result = service.getRelationshipDto(stringFamilyId, stringRelationshipId);

        assertFalse(result.isSuccess());
        assertEquals(expected, result);
        assertEquals(expected.getContent(), result.getContent());
    }

    @Test
    void getAllRelationshipsDto_successCase() {

        String famId = "1";
        FamilyID familyID = new FamilyID(famId);
        String adminEmail = "ze@isep.pt";
        String otherEmail = "ana@isep.pt";

        Family family = new Family.Builder(familyID).setName(new FamilyName("Santos")).
                setRegistrationDate(new RegistrationDate(LocalDate.now().toString())).
                setAdminId(new PersonID(new Email(adminEmail))).build();

        family.createRelationship(new PersonID(new Email(adminEmail)), new PersonID(new Email(otherEmail)),
                new FamilyRelationType("brother"), new FamilyRelationshipID("1"));

        Optional<Family> optionalFamily = Optional.of(family);

        RelationshipDTO dto = new RelationshipDTO();
        dto.setRelationshipId("1");
        dto.setFamilyId(famId);
        dto.setFirstPersonEmail(adminEmail);
        dto.setSecondPersonEmail(otherEmail);


        Mockito.when(assemblerToDTO.toFamilyID(famId)).thenReturn(familyID);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(famId))).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toRelationshipDto(new FamilyRelationship(new PersonID(new Email(adminEmail)),
                new PersonID(new Email(otherEmail)), new FamilyRelationType("brother"),
                new FamilyRelationshipID("1")), new FamilyID(famId))).thenReturn(dto);

        List<RelationshipDTO> expectedRelationshipDTOList = new ArrayList<>();
        expectedRelationshipDTOList.add(dto);

        Result result = service.getAllRelationshipsDto(famId);

        assertEquals(expectedRelationshipDTOList, result.getContent());
        assertTrue(result.isSuccess());

    }

    @Test
    void getAllRelationshipsDto_successCaseButNoRelationshipsPresent() {

        String famId = "1";
        FamilyID familyID = new FamilyID(famId);
        String adminEmail = "ze@isep.pt";

        Family family = new Family.Builder(familyID).setName(new FamilyName("Santos")).
                setRegistrationDate(new RegistrationDate(LocalDate.now().toString())).
                setAdminId(new PersonID(new Email(adminEmail))).build();

        Optional<Family> optionalFamily = Optional.of(family);


        Mockito.when(assemblerToDTO.toFamilyID(famId)).thenReturn(familyID);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(famId))).thenReturn(optionalFamily);

        List<RelationshipDTO> expectedRelationshipDTOList = new ArrayList<>();

        Result result = service.getAllRelationshipsDto(famId);

        assertEquals(expectedRelationshipDTOList, result.getContent());
        assertTrue(result.isSuccess());

    }

    @Test
    void getAllRelationshipsDto_failCase_FamilyDoesNotExist() {

        String famId = "1";
        FamilyID familyID = new FamilyID(famId);

        Optional<Family> optionalFamily = Optional.empty();

        String expectedMessage = "Could not get relationships! A family with the passed ID does not exist.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        Mockito.when(assemblerToDTO.toFamilyID(famId)).thenReturn(familyID);
        Mockito.when(familyRepositoryMock.findByID(new FamilyID(famId))).thenReturn(optionalFamily);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);

        Result result = service.getAllRelationshipsDto(famId);

        assertFalse(result.isSuccess());
        assertEquals(expected, result);

    }

    @Test
    void getAllRelationshipsDto_failCase_nullFamilyId() {

        String famId = null;

        String expectedMessage = "Could not get relationships! The family Id cannot be null.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        NullArgumentException nullArgumentException = new NullArgumentException("The family Id cannot be null.");

        Mockito.when(assemblerToDTO.toFamilyID(famId)).thenThrow(nullArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);

        Result result = service.getAllRelationshipsDto(famId);

        assertFalse(result.isSuccess());
        assertEquals(expected, result);

    }

    @Test
    void getAllRelationshipsDto_failCase_emptyFamilyId() {

        String famId = "";

        String expectedMessage = "Could not get relationships! The family Id cannot be empty.";

        ExceptionDTO exceptionDTO = new ExceptionDTO(expectedMessage);

        Result expected = Result.failure(exceptionDTO);

        EmptyArgumentException emptyArgumentException = new EmptyArgumentException("The family Id cannot be empty.");

        Mockito.when(assemblerToDTO.toFamilyID(famId)).thenThrow(emptyArgumentException);
        Mockito.when(assemblerToDTO.toExceptionDto(expectedMessage)).thenReturn(exceptionDTO);

        Result result = service.getAllRelationshipsDto(famId);

        assertFalse(result.isSuccess());
        assertEquals(expected, result);

    }

    @Test
    void memberIsTheChild() {
        //Arrange
        String idFamily = "189";
        String fatherEmail = "pai@iol.pt";
        String childEmail = "filha@yahoo.com";

        RelationshipDTO input = new RelationshipDTO();
        input.setFamilyId(idFamily);
        input.setFirstPersonEmail(fatherEmail);
        input.setSecondPersonEmail(childEmail);

        PersonID parent = new PersonID(new Email(fatherEmail));
        PersonID child = new PersonID(new Email(childEmail));
        FamilyID familyId = new FamilyID(idFamily);

        Family family = new Family.Builder(familyId).setAdminId(parent).setName(new FamilyName("Fonseca"))
                .setRegistrationDate(new RegistrationDate("21.07.2021")).build();
        family.createRelationship(parent, child, new FamilyRelationType("father"), new FamilyRelationshipID("009"));

        Mockito.when(assemblerToDTO.toFamilyID(idFamily)).thenReturn(familyId);
        Mockito.when(assemblerToDTO.toPersonID(fatherEmail)).thenReturn(parent);
        Mockito.when(assemblerToDTO.toPersonID(childEmail)).thenReturn(child);

        Mockito.when(familyRepositoryMock.findByID(familyId)).thenReturn(Optional.of(family));

        //Act
        boolean result = service.checkIfIsChild(input);

        //Assert
        assertTrue(result);

    }

    @Test
    void memberIsNotTheChild() {
        //Arrange
        String idFamily = "189";
        String cousinEmail = "pai@iol.pt";
        String childEmail = "filha@yahoo.com";

        RelationshipDTO input = new RelationshipDTO();
        input.setFamilyId(idFamily);
        input.setFirstPersonEmail(cousinEmail);
        input.setSecondPersonEmail(childEmail);

        PersonID notParent = new PersonID(new Email(cousinEmail));
        PersonID child = new PersonID(new Email(childEmail));
        FamilyID familyId = new FamilyID(idFamily);

        Family family = new Family.Builder(familyId).setAdminId(notParent).setName(new FamilyName("Fonseca"))
                .setRegistrationDate(new RegistrationDate("21.07.2021")).build();
        family.createRelationship(notParent, child, new FamilyRelationType("cousin"), new FamilyRelationshipID("009"));

        Mockito.when(assemblerToDTO.toFamilyID(idFamily)).thenReturn(familyId);
        Mockito.when(assemblerToDTO.toPersonID(cousinEmail)).thenReturn(notParent);
        Mockito.when(assemblerToDTO.toPersonID(childEmail)).thenReturn(child);

        Mockito.when(familyRepositoryMock.findByID(familyId)).thenReturn(Optional.of(family));

        //Act
        boolean result = service.checkIfIsChild(input);

        //Assert
        assertFalse(result);

    }


}
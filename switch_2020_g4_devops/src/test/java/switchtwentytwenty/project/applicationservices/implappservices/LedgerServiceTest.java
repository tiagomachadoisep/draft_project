package switchtwentytwenty.project.applicationservices.implappservices;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import switchtwentytwenty.project.applicationservices.irepositories.*;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.FamilyCashAccount;
import switchtwentytwenty.project.domain.aggregates.account.PersonalCashAccount;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.ledger.Payment;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.aggregates.ledger.Transfer;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.domainservices.ILedgerAccountService;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.dto.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

//@SpringBootTest
class LedgerServiceTest {

    //@Mock
    private final ILedgerRepository ledgerRepositoryMock = Mockito.mock(ILedgerRepository.class);
    //@Mock
    private final IAccountRepository accountRepositoryMock = Mockito.mock(IAccountRepository.class);
    //@Mock
    private final IPersonRepository personRepositoryMock = Mockito.mock(IPersonRepository.class);
    //@Mock
    private final ICategoryRepository categoryRepositoryMock = Mockito.mock(ICategoryRepository.class);
    //@Mock
    private final ILedgerAccountService ledgerAccountService = Mockito.mock(ILedgerAccountService.class);
    //@Mock
    private final IFamilyRepository familyRepositoryMock = Mockito.mock(IFamilyRepository.class);
    //@InjectMocks
    private final LedgerService ledgerService = new LedgerService(ledgerRepositoryMock, accountRepositoryMock, personRepositoryMock, categoryRepositoryMock, familyRepositoryMock, ledgerAccountService, new AssemblerToDTO());

    @Test
    void transferCashFromFamilyToMember() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);
        TransactionID transactionID = new TransactionID("test");

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(transactionID);

        TransferDTO expected = new TransferDTO(mainEmail, mainEmail, amount, categoryId, "1", "10", "test", date);
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
        assertNotNull(result);
        verify(accountRepositoryMock, times(1)).saveNew(memberAccount);
        verify(accountRepositoryMock, times(1)).saveNew(familyAccount);
        verify(ledgerRepositoryMock, times(1)).addTransferFromFamily(personID, personID, transactionID, memberAccount.getID(), familyAccount.getID(), new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date));

    }

    @Test
    void transferCashFromFamilyToMember_NoCategory() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(false);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Category doesn't exist");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_NoEmail() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.empty());
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Member doesn't exist");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_MemberNotInFamily() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);


        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(new FamilyID("2"));
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Persons are not in the same family");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_MemberHasNoCashAccount() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);


        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.empty());
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Member doesn't have cash account");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_FamilyHasNoCashAccount() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Family doesn't have cash account");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_NoLedger() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Ledger not created");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_NoLedgerFamily() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.empty());
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        TransferDTO expected = new TransferDTO("Ledger not created");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_NotEnoughBalance() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 1000;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);


        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("-1"));

        TransferDTO expected = new TransferDTO("Not enough balance");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCashFromFamilyToMember_FailInDomainService() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);


        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("-1"));

        TransferDTO expected = new TransferDTO("Not enough balance");
        //Act
        TransferDTO result = ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void registerPayment() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 10;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        TransactionDate date = new TransactionDate(LocalDate.now());
        TransactionID transactionID = new TransactionID("test");
        MoneyValue moneyAmount = new MoneyValue(amount);
        CategoryID categoryID = new CategoryID(categoryId);

        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, moneyAmount, categoryID, date)).thenReturn(transactionID);

        PaymentDTO expected = new PaymentDTO(mainEmail, amount, categoryId, transactionID.getId(), account.getID().getAccountIDValue(), date.getTransactionDate());
        //Act
        PaymentDTO result = ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        assertEquals(expected, result);
        verify(accountRepositoryMock, times(1)).saveNew(account);
        verify(ledgerRepositoryMock, times(1)).addPayment(personID, transactionID, account.getID(), moneyAmount, categoryID, date);
    }

    @Test
    void registerPayment_MemberHasNoAccount() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 10;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        TransactionDate date = new TransactionDate(LocalDate.now());

        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, new MoneyValue(amount), new CategoryID(categoryId), date)).thenReturn(new TransactionID("test"));

        PaymentDTO expected = new PaymentDTO("Member doesn't have cash account");
        //Act
        PaymentDTO result = ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void registerPayment_MemberHasNoLedger() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 10;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        TransactionDate date = new TransactionDate(LocalDate.now());

        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.empty());
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, new MoneyValue(amount), new CategoryID(categoryId), date)).thenReturn(new TransactionID("test"));

        PaymentDTO expected = new PaymentDTO("Ledger not created");
        //Act
        PaymentDTO result = ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        assertEquals(expected, result);
    }


    @Test
    void registerPayment_NoCategory() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 10;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        TransactionDate date = new TransactionDate(LocalDate.now());

        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(false);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, new MoneyValue(amount), new CategoryID(categoryId), date)).thenReturn(new TransactionID("test"));

        PaymentDTO expected = new PaymentDTO("Category doesn't exist");
        //Act
        PaymentDTO result = ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void registerPaymentInsufficientFunds() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 100;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        TransactionDate date = new TransactionDate(LocalDate.now());

        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, new MoneyValue(amount), new CategoryID(categoryId), date)).thenReturn(new TransactionID("-1"));


        PaymentDTO expected = new PaymentDTO("Not enough balance");
        //Act
        PaymentDTO result = ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        assertEquals(expected, result);


    }

    @Test
    void registerPaymentDS() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 10;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        MoneyValue moneyValue = new MoneyValue(amount);
        CategoryID categoryID = new CategoryID(categoryId);
        TransactionDate date = new TransactionDate(LocalDate.now());


        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, moneyValue, categoryID, date)).thenReturn(new TransactionID("test"));

        PaymentDTO expected = new PaymentDTO(mainEmail, amount, categoryId, "test", "1", date.getTransactionDate());

        //Act
        PaymentDTO result = ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        assertEquals(expected, result);
    }


    @Test
    void transferCash() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);


        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);

        TransferDTO expected = new TransferDTO(mainEmail, otherEmail, amount, categoryId, "10", "20", "1", date);
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
        verify(accountRepositoryMock, times(1)).saveNew(memberAccount);
        verify(accountRepositoryMock, times(1)).saveNew(otherAccount);
        verify(ledgerRepositoryMock, times(1)).addTransfer(otherID, personID, new TransactionID("1"), memberAccount.getID(), otherAccount.getID(), new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date));

    }

    @Test
    void transferCash_NoCategory() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(false);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);

        TransferDTO expected = new TransferDTO("Category doesn't exist");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCash_MembersNotInFamily() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();

        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(false);

        TransferDTO expected = new TransferDTO("Persons are not in the same family");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCash_MemberNoAccount() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        LocalDate date = LocalDate.of(2020, 1, 1);
        String categoryId = "1";
        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.empty());
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);

        TransferDTO expected = new TransferDTO("Member doesn't have cash account");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCash_MemberNoLedger() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        LocalDate date = LocalDate.of(2020, 1, 1);
        String categoryId = "1";
        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);
        TransferDTO expected = new TransferDTO("Ledger not created");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCash_OtherMemberNoAccount() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(false);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);

        TransferDTO expected = new TransferDTO("Member doesn't have cash account");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCash_NoFunds() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 1000;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);

        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("-1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);

        TransferDTO expected = new TransferDTO("Not enough balance");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void transferCash_DomainServiceFails() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        String otherEmail = "carlos@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        PersonID otherID = new PersonID(new Email(otherEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);

        TransferDTO transferDTO = new TransferDTO(mainEmail, otherEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Person other = new Person.Builder(new PersonID(new Email(otherEmail))).setName(new PersonName("Carlos")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account otherAccount = new PersonalCashAccount.Builder(new AccountID("20")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(otherID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger otherLedger = new Ledger(2, otherEmail);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.findByID(otherID)).thenReturn(Optional.of(other));
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(otherID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findCashByOwnerId(otherID)).thenReturn(Optional.of(otherAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(otherID)).thenReturn(Optional.of(otherLedger));
        Mockito.when(ledgerAccountService.addTransfer(otherLedger, ledger, memberAccount, otherAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("-1"));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, otherID)).thenReturn(true);
        TransferDTO expected = new TransferDTO("Not enough balance");
        //Act
        TransferDTO result = ledgerService.transferCash(transferDTO);

        //Assert
        assertEquals(result, expected);
    }

    @Test
    void findLedgerByOwnerID() {
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        Ledger ledger = new Ledger(1, mainEmail);
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));

        Optional<Ledger> optionalLedger = ledgerService.findLedgerByOwnerID(personID);

        assertEquals(ledger, optionalLedger.get());
    }

    @Test
    void whenTransferCshFromFamilyToMember_ValidateSaveNew() {

        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        FamilyID familyID = new FamilyID("1");
        double amount = 10;
        String categoryId = "1";
        LocalDate date = LocalDate.of(2020, 1, 1);
        TransferDTO transferDTO = new TransferDTO(mainEmail, mainEmail, amount, categoryId, date);
        Person member = new Person.Builder(new PersonID(new Email(mainEmail))).setName(new PersonName("Ze")).setVat(new VATNumber(0)).setFamilyId(new FamilyID("1")).build();
        Account memberAccount = new PersonalCashAccount.Builder(new AccountID("10")).setAccountType().setBalance(new MoneyValue(10))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Account familyAccount = new FamilyCashAccount.Builder(new AccountID("1")).setBalance(new MoneyValue(200))
                .setAccountType().setOwnerID(personID).setDescription(new Description("cash account")).build();
        Ledger ledger = new Ledger(1, mainEmail);
        Ledger familyLedger = new Ledger(2, mainEmail, ELedgerType.FAMILY);

        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(familyRepositoryMock.getFamilyIdByAdminMainEmail(new Email(mainEmail))).thenReturn(familyID);
        Mockito.when(personRepositoryMock.findByID(personID)).thenReturn(Optional.of(member));
        Mockito.when(personRepositoryMock.hasSameFamily(personID, personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.hasFamilyCashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(memberAccount));
        Mockito.when(accountRepositoryMock.findFamilyCashByOwnerId(personID)).thenReturn(Optional.of(familyAccount));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(ledgerRepositoryMock.findFamilyByOwnerID(personID)).thenReturn(Optional.of(familyLedger));
        Mockito.when(ledgerAccountService.addTransfer(familyLedger, ledger, familyAccount, memberAccount, new MoneyValue(amount), new CategoryID(categoryId), new TransactionDate(date))).thenReturn(new TransactionID("1"));

        //Act
        ledgerService.transferCashFromFamilyToMember(transferDTO);

        //Assert
        verify(accountRepositoryMock, times(1)).saveNew(memberAccount);
        verify(accountRepositoryMock, times(1)).saveNew(familyAccount);
    }

    @Test
    void whenRegisterPaymentDS_verifySaveNew() {
        //Arrange
        String mainEmail = "ze@isep.pt";
        PersonID personID = new PersonID(new Email(mainEmail));
        double amount = 10;
        String categoryId = "1";
        PaymentDTO paymentDTO = new PaymentDTO(mainEmail, amount, categoryId);
        Account account = new PersonalCashAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(20))
                .setDescription(new Description("cash account")).setOwnerID(personID).build();
        Ledger ledger = new Ledger(1, mainEmail);
        TransactionDate date = new TransactionDate(LocalDate.now());

        Mockito.when(accountRepositoryMock.hasPersonACashAccount(personID)).thenReturn(true);
        Mockito.when(accountRepositoryMock.findCashByOwnerId(personID)).thenReturn(Optional.of(account));
        Mockito.when(ledgerRepositoryMock.findByOwnerID(personID)).thenReturn(Optional.of(ledger));
        Mockito.when(categoryRepositoryMock.existsID(new CategoryID(categoryId))).thenReturn(true);
        Mockito.when(ledgerAccountService.addPayment(ledger, account, new MoneyValue(amount), new CategoryID(categoryId), date)).thenReturn(new TransactionID("test"));

        //Act
        ledgerService.registerPaymentDS(paymentDTO);

        //Assert
        verify(accountRepositoryMock, times(1)).saveNew(account);
    }

    @Test
    void getMovements() {
        LedgerID ledgerId = new LedgerID(1);
        AccountID accountID = new AccountID("1");
        TransactionDate startDate = new TransactionDate(LocalDate.of(2020, 12, 12).toString());
        TransactionDate endDate = new TransactionDate(LocalDate.of(2021, 12, 12).toString());

        List<Movement> movementList = new ArrayList<>();
        Movement movement = new Movement(accountID, new MoneyValue(10));
        Movement movementTwo = new Movement(accountID, new MoneyValue(25));
        movementList.add(movement);
        movementList.add(movementTwo);

        Mockito.when(ledgerRepositoryMock.getAccountMovementsBetweenDates(ledgerId, accountID, startDate, endDate)).thenReturn(movementList);

        List<MovementDTO> expected = new ArrayList<>();
        expected.add(new MovementDTO("1", 10));
        expected.add(new MovementDTO("1", 25));

        List<MovementDTO> result = ledgerService.getAccountMovementsBetweenDates(ledgerId, accountID, startDate, endDate);

        assertEquals(expected, result);
    }

    @Test
    void getTransfer() {
        LedgerID ledgerID = new LedgerID(1);
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID otherAccountID = new AccountID("3");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");

        Transactionable transactionable = new Transfer(transactionID, accountID, otherAccountID, amount, categoryID);

        Mockito.when(ledgerRepositoryMock.findTransactionByID(transactionID, ledgerID)).thenReturn(transactionable);

        TransactionDTO transactionDTO = new TransferDTO(transactionable);

        TransactionDTO result = ledgerService.findTransactionByID(transactionID, ledgerID);

        assertEquals(transactionDTO, result);
    }

    @Test
    void getTransferButItAPayment() {
        LedgerID ledgerID = new LedgerID(1);
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");


        Transactionable transactionable = new Payment(transactionID, accountID, amount, categoryID);

        Mockito.when(ledgerRepositoryMock.findTransactionByID(transactionID, ledgerID)).thenReturn(transactionable);

        PaymentDTO transactionDTO = new PaymentDTO(transactionable);

        TransactionDTO result = ledgerService.findTransactionByID(transactionID, ledgerID);

        assertEquals(transactionDTO, result);
    }

    @Test
    void findPossibleTransactions() {
        LedgerID ledgerID = new LedgerID(1);
        TransactionID transactionID = new TransactionID("1");
        AccountID accountID = new AccountID("1");
        AccountID otherAccountID = new AccountID("3");
        MoneyValue amount = new MoneyValue(10);
        CategoryID categoryID = new CategoryID("1");


        List<Transactionable> list = new ArrayList<>();
        Transactionable transactionable = new Transfer(transactionID, accountID, otherAccountID, amount, categoryID);
        list.add(transactionable);

        Mockito.when(ledgerRepositoryMock.findPossibleTransactionsByMovement(ledgerID, accountID, amount)).thenReturn(list);

        List<TransactionDTO> dtoList = new ArrayList<>();
        TransactionDTO transactionDTO = new TransferDTO(transactionable);
        dtoList.add(transactionDTO);

        List<TransactionDTO> result = ledgerService.findPossibleTransactionsByMovement(ledgerID, accountID, amount);

        assertEquals(dtoList, result);
    }


}


package switchtwentytwenty.project.applicationservices.implassemblers;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.CategoryDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CategoryAssemblerTest {

    private CategoryAssembler assembler = new CategoryAssembler();

    @Test
    void toCategoryName() {
        //ARRANGE
        String name = "Hotel";
        CategoryName expected = new CategoryName(name);
        CategoryDTO dto = new CategoryDTO();
        dto.setName(name);
        //ACT
        CategoryName actual = assembler.assembleCategoryName(dto);

        //ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void toCategoryId() {
        //ARRANGE

        //ACT
        CategoryID actual = assembler.assembleCategoryId();

        //ASSERT
        assertNotNull(actual);
    }

    @Test
    void toParentCategoryId() {
        //ARRANGE
        String id = "osur46t3";
        CategoryID expected = new CategoryID(id);

        CategoryDTO dto = new CategoryDTO();
        dto.setParentId(id);
        //ACT
        CategoryID actual = assembler.assembleParentCategoryId(dto);

        //ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void toFamilyId() {
        //ARRANGE
        String id = "osur46t3";
        FamilyID expected = new FamilyID(id);

        CategoryDTO dto = new CategoryDTO();
        dto.setFamilyId(id);
        //ACT
        FamilyID actual = assembler.assembleFamilyId(dto);

        //ASSERT
        assertEquals(expected, actual);
    }

}
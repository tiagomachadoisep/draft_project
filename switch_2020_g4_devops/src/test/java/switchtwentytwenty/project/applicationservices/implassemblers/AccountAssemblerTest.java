package switchtwentytwenty.project.applicationservices.implassemblers;

import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.domain.aggregates.account.*;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;

import static org.junit.jupiter.api.Assertions.*;

class AccountAssemblerTest {

    private final AccountAssembler accountAssembler = new AccountAssembler();
    private final AccountDTO accountDTO = new AccountDTO();

    @Test
    void getAccountIDFromAccountDTO() {

        //Arrange
        accountDTO.setAccountId("djsdjwo2");
        AccountID expected = new AccountID("djsdjwo2");

        //Act
        AccountID result = accountAssembler.getAccountIDFromAccountDTO(accountDTO);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void getPersonIDFromAccountDTO() {

        //Arrange
        accountDTO.setOwnerId("miguel@isep.pt");
        PersonID expected = new PersonID(new Email("miguel@isep.pt"));

        //Act
        PersonID result = accountAssembler.getPersonIDFromAccountDTO(accountDTO);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void getDescriptionFromAccountDTO() {

        //Arrange
        accountDTO.setDescription("miguel's credit account");
        Description expected = new Description("miguel's credit account");

        //Act
        Description result = accountAssembler.getDescriptionFromAccountDTO(accountDTO);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void getBalanceFromAccountDTO() {

        //Arrange
        accountDTO.setBalance(100);
        MoneyValue expected = new MoneyValue(100);

        //Act
        MoneyValue result = accountAssembler.getBalanceFromAccountDTO(accountDTO);

        //Assert
        assertEquals(expected, result);

    }

    @Test
    void assembleCreditCardAccount() {

        //Arrange
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountId("123");
        accountDTO.setOwnerId("filipa@gmail.com");
        accountDTO.setDescription("credit account");
        accountDTO.setBalance(0);
        Account expected = new CreditCardAccount.Builder(new AccountID("123")).setOwnerID(new PersonID(new Email(
                "filipa@gmail.com"))).setAccountType().setBalance(new MoneyValue(0)).setDescription(new Description(
                "credit account")).build();

        //Act
        Account result = accountAssembler.assembleCreditCardAccount(accountDTO);

        //Assert
        assertEquals(expected.getID().getAccountIDValue(), result.getID().getAccountIDValue());
        assertEquals(expected.getAccountType(), result.getAccountType());
        assertEquals(expected.getBalance().getValue(), result.getBalance().getValue());
        assertEquals(expected.getDescription(), result.getDescription());
        assertEquals(expected.getOwnerID(), result.getOwnerID());

    }

    @Test
    void assembleFamilyCashAccount() {

        //Arrange
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountId("123");
        accountDTO.setOwnerId("filipa@gmail.com");
        accountDTO.setDescription("family cash account");
        accountDTO.setBalance(500);
        Account expected =
                new FamilyCashAccount.Builder(new AccountID("123")).setAccountType().setBalance(new MoneyValue(500)).setDescription(new Description("family cash account")).setOwnerID(new PersonID(new Email("filipa@gmail.com"))).build();

        //Act
        Account result = accountAssembler.assembleFamilyCashAccount(accountDTO);

        //Assert
        assertEquals(expected.getID().getAccountIDValue(), result.getID().getAccountIDValue());
        assertEquals(expected.getAccountType(), result.getAccountType());
        assertEquals(expected.getBalance().getValue(), result.getBalance().getValue());
        assertEquals(expected.getDescription(), result.getDescription());
        assertEquals(expected.getOwnerID(), result.getOwnerID());
        assertNotNull(result);

    }

    @Test
    void assembleBuilder() {
        String email = "ze@isep.pt";
        String description = "conta teste";
        double balance = 10;
        EAccountType type = EAccountType.PERSONAL_CASH;
        AccountDTO accountDTO = new AccountDTO(email,description,balance,type);
        AccountBuilder builder = accountAssembler.assembleBuilder(accountDTO);

        Account account = builder.build();

        assertNotNull(builder);
        assertTrue(account instanceof PersonalCashAccount);
    }

    @Test
    void assembleBuilderException() {
        String email = "";
        String description = "conta teste";
        double balance = 10;
        EAccountType type = EAccountType.PERSONAL_CASH;
        AccountDTO accountDTO = new AccountDTO(email,description,balance,null,type);
        AccountBuilder builder = accountAssembler.assembleBuilder(accountDTO);

        assertNull(builder);
    }

    /*
    @Test
    void assembleCash() {
        //Arrange
        AccountDTO dto = new AccountDTO("miguel@isep.pt", "miguel's cash account", 100, "1",
                EAccountType.PERSONAL_CASH);

        //Act
        Account personalCashAccount = accountAssembler.assemble(dto);

        //Assert
        assertNotNull(personalCashAccount);

    }

    @Test
    void assembleCredit() {
        //Arrange
        AccountDTO dto = new AccountDTO("miguel@isep.pt", "miguel's credit account", 100, "1",
                EAccountType.CREDIT_CARD);
        Account expected =
                new CreditCardAccount.Builder(new AccountID("1")).setAccountType().setBalance(new MoneyValue(100))
                .setDescription(new Description("miguel's credit account")).setOwnerID(new PersonID(new Email
                ("miguel@isep.pt"))).build();

        //Act
        Account creditAccount = accountAssembler.assemble(dto);

        //Assert
        assertTrue(creditAccount.isID(expected.getID()));
    }
    */

}
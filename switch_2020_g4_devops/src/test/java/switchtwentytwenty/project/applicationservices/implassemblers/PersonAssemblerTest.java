package switchtwentytwenty.project.applicationservices.implassemblers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class PersonAssemblerTest {

    @Test
    void assembleAddress() {
        PersonAssembler personAssembler = new PersonAssembler();
        PersonDTO personDTO = new PersonDTO("1", "luis@isep.pt",
                "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");

        assertNotNull(personAssembler.assembleAddress(personDTO));

    }

    @Test
    void assembleFamilyID() {
        PersonAssembler personAssembler = new PersonAssembler();
        PersonDTO personDTO = new PersonDTO("1", "luis@isep.pt",
                "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");

        assertNotNull(personAssembler.assembleFamilyID(personDTO));

    }

    @Test
    void assembleBirthDate() {
        PersonAssembler personAssembler = new PersonAssembler();
        PersonDTO personDTO = new PersonDTO("1", "luis@isep.pt",
                "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");

        assertNotNull(personAssembler.assembleBirthDate(personDTO));
    }


    @Test
    void assembleBirthDateEmpty() {
        PersonAssembler personAssembler = new PersonAssembler();
        PersonDTO personDTO = new PersonDTO("1", "luis@isep.pt",
                "Maria", 123456789, "Rua das Coves, 22", "");

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            personAssembler.assembleBirthDate(personDTO);
        });
    }

    @Test
    void assembleBirthDateNull() {
        PersonAssembler personAssembler = new PersonAssembler();
        PersonDTO personDTO = new PersonDTO("1", "luis@isep.pt",
                "Maria", 123456789, "Rua das Coves, 22", null);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            personAssembler.assembleBirthDate(personDTO);
        });
    }

    @Test
    void assembleFamilyIDNull(){
        PersonAssembler personAssembler = new PersonAssembler();
        PersonDTO personDTO = new PersonDTO(null, "luis@isep.pt",
                "Maria", 123456789, "Rua das Coves, 22", "01-03-1990");


        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            personAssembler.assembleFamilyID(personDTO);
        });
    }
}
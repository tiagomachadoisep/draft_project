package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a family already has a cash account.
 */
public class FamilyAlreadyHasCashAccountException extends IllegalArgumentException {

    private static final long serialVersionUID = -1160574829305325433L;

    /**
     * Constructs a FamilyAlreadyHasCashAccountException with the specified detail message.
     *
     * @param s the detail message
     */
    public FamilyAlreadyHasCashAccountException(String s) {
        super(s);
    }
}

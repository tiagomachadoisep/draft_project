package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a method has been passed an empty argument (e.g. empty String).
 */

public class EmptyArgumentException extends IllegalArgumentException{

    private static final long serialVersionUID = -6755834819961550266L;

    /**
     * Constructs an EmptyArgumentException with the specified detail message.
     * @param s - the detail message.
     */
    public EmptyArgumentException(String s){
        super(s);
    }
}

package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a date introduced is invalid.
 */
public class InvalidDateExpressionException extends IllegalArgumentException {

    private static final long serialVersionUID = -7174686830836435019L;

    /**
     * Constructs an InvalidDateExpressionException with the specified detail message.
     * @param s - the detail message.
     */
    public InvalidDateExpressionException(String s){
        super(s);
    }
}

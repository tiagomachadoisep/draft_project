package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a person does not exist in the repository.
 */
public class NonExistentPersonException extends IllegalArgumentException {

    private static final long serialVersionUID = 399694843707209531L;

    /**
     * Constructs a NonExistentPersonException with the specified detail message.
     *
     * @param s the detail message
     */
    public NonExistentPersonException(String s) {
        super(s);
    }
}

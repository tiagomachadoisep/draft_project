package switchtwentytwenty.project.exceptions;

public class NoAccountsFoundException extends IllegalArgumentException {

    private static final long serialVersionUID = -7702033590640881627L;

    public NoAccountsFoundException(String s) {
        super(s);
    }
}

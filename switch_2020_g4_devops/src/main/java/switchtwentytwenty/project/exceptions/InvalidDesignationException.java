package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a method has been passed an invalid string designation.
 */

public class InvalidDesignationException extends IllegalArgumentException{

    private static final long serialVersionUID = 2530317585532507855L;

    /**
     * Constructs an InvalidDesignationException with the specified detail message.
     * @param s - the detail message.
     */
    public InvalidDesignationException(String s){
        super(s);
    }
}

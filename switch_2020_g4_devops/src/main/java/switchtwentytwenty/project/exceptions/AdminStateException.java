package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that person is not the family administrator
 */
public class AdminStateException extends IllegalArgumentException {
    private static final long serialVersionUID = 2625104495055661252L;

    /**
     * Constructs an AdminStateException with the specified detail message.
     * @param s - the detail message.
     */
    public AdminStateException(String s){
        super(s);
    }
}

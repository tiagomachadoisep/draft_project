package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a person already has a cash account.
 */
public class PersonAlreadyHasCashAccountException extends IllegalArgumentException {

    private static final long serialVersionUID = -1160777658388325433L;

    /**
     * Constructs a PersonAlreadyHasCashAccountException with the specified detail message.
     *
     * @param s the detail message
     */
    public PersonAlreadyHasCashAccountException(String s) {
        super(s);
    }
}

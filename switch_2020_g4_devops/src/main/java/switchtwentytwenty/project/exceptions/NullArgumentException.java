package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a method has been passed a null argument.
 */
public class NullArgumentException extends IllegalArgumentException{

    private static final long serialVersionUID = -6000707136956896287L;

    /**
     * Constructs a NullArgumentException with the specified detail message.
     * @param s - the detail message.
     */
    public NullArgumentException(String s){
        super(s);
    }
}

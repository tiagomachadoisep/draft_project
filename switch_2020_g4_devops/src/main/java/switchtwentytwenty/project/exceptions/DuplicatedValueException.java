package switchtwentytwenty.project.exceptions;

/**
 * Thrown to indicate that a duplicated value exists in the collection.
 */

public class DuplicatedValueException extends IllegalArgumentException{
    private static final long serialVersionUID = -3003379330318210253L;

    /**
     * Constructs an DuplicatedValueException with the specified detail message.
     * @param s - the detail message.
     */
    public DuplicatedValueException(String s){
        super(s);
    }
}

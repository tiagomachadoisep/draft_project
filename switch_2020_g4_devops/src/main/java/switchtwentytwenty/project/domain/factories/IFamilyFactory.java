package switchtwentytwenty.project.domain.factories;

import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.List;

public interface IFamilyFactory {
    List<Object> createFamilyAndAdmin(FamilyID id, FamilyName name, PersonID adminEmail, PersonName adminName, VATNumber adminVat, BirthDate adminBirthDate, Address address);
}

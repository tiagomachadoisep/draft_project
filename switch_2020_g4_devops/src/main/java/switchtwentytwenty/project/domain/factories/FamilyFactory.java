package switchtwentytwenty.project.domain.factories;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.ArrayList;
import java.util.List;

@Service
public class FamilyFactory implements IFamilyFactory{


    @Override
    public List<Object> createFamilyAndAdmin(FamilyID id, FamilyName name, PersonID adminEmail, PersonName adminName, VATNumber adminVat, BirthDate adminBirthDate, Address address) {
        List<Object> list = new ArrayList<>();
        Person admin = new Person.Builder(adminEmail).setFamilyId(id).setName(adminName).setBirthDate(adminBirthDate).
                setVat(adminVat).setAddress(address).setRoleAsFamilyAdministrator().build();
        Family family = new Family.Builder(id).setName(name).setAdminId(adminEmail).build();

        list.add(admin);
        list.add(family);

        return list;
    }
}

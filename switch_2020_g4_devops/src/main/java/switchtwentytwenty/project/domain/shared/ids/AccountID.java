package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.utils.GenerateID;

import java.io.Serializable;
import java.util.Objects;

public class AccountID implements ID, Serializable, GenerateID {

    private static final long serialVersionUID = 1930255355706226748L;
    private String accountIDValue;

    /**
     * Constructor for AccountID.
     *
     * @param id String
     */
    public AccountID(String id) {
        if (id == null || id.isEmpty())
            throw new IllegalArgumentException("Empty ID");
        this.accountIDValue = id;
    }

    /**
     * Empty constructor for AccountID that generates a UUID
     */
    public AccountID() {
        this.accountIDValue = generateID();
    }

    /**
     * Returns the accountID.
     *
     * @return String
     */
    public String getAccountIDValue() {
        return accountIDValue;
    }

    /**
     * Sets the accountID.
     *
     * @param accountID String
     */
    public void setAccountIDValue(String accountID) {
        this.accountIDValue = accountID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountID accountId1 = (AccountID) o;
        return accountIDValue.equals(accountId1.accountIDValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountIDValue);
    }

    @Override
    public String toString() {
        return accountIDValue;
    }
}

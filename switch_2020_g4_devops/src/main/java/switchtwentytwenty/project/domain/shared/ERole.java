package switchtwentytwenty.project.domain.shared;

public enum ERole {
    ADMIN,
    USER,
    SYSADMIN
}

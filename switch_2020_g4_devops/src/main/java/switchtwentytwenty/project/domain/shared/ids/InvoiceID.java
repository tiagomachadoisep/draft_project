package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.dddtypes.ID;
@SuppressWarnings("serial")
public class InvoiceID implements ID {
    public final int id;

    public InvoiceID(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}

package switchtwentytwenty.project.domain.shared;


import switchtwentytwenty.project.exceptions.InvalidDesignationException;
import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.Objects;
import java.util.regex.Pattern;

public class PersonName implements ValueObject {

    private final String name;

    /**
     * PersonName constructor
     * @param name String
     */
    public PersonName(String name) {
        if (validateName(name)) {
            this.name = name;
        } else {
            throw new InvalidDesignationException("Invalid name format");
        }
    }


    /**
     * check if a string is not null or empty
     *
     * @param name String
     * @return boolean
     */
    private boolean validateName(String name) {
        if (name == null || name.trim().isEmpty()) {
            return false;
        } else {
            return validateNameFormat(name);
        }
    }

    /**
     * Validates if name of person follows regex pattern.
     * Extracted from https://stackoverflow.com/questions/15805555/java-regex-to-validate-full-name-allow-only
     * -spaces-and-letters
     * @param name String
     * @return boolean
     */
    private boolean validateNameFormat(String name) {
        String nameRegex = "^[\\p{L} .'-]+$";

        Pattern pattern = Pattern.compile(nameRegex);
        return pattern.matcher(name).matches();
    }


    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonName that = (PersonName) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

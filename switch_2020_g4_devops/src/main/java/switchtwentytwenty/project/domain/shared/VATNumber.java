package switchtwentytwenty.project.domain.shared;


import lombok.Getter;
import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.Objects;


@Getter
public class VATNumber implements ValueObject {
    private int vat;

    /**
     * Vat Number's constructor
     * @param aVatNumber int
     */
    public VATNumber(int aVatNumber) {
        if(validateVat(aVatNumber)||aVatNumber==0) {
            this.vat = aVatNumber;
        } else {
            throw new IllegalArgumentException("Invalid VAT.");
        }

    }

    /**
     * Checks if valid vat. Only works on portuguese vat.
     * Method extracted from: https://pt.wikipedia.org/wiki/N%C3%BAmero_de_identifica%C3%A7%C3%A3o_fiscal
     * @param vat int
     * @return true if is valid or false if not.
     */
    private boolean validateVat(int vat){
        String vatString = String.valueOf(vat);
        int length = vatString.length();
        if(length!=9)
            return false;
        int controlDigit = Character.getNumericValue(vatString.charAt(length-1));
        int sum = 0;
        for (int digit = 0; digit < length - 1; digit++)
            sum += Character.getNumericValue(vatString.charAt(digit)) * (9 - digit);
        int rest = sum % 11;
        if (rest == 0 || rest == 1)
            return controlDigit == 0;
        else
            return controlDigit == (11 - rest);
    }

    @Override
    public String toString() {
        return String.valueOf(vat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VATNumber vatNumber = (VATNumber) o;
        return vat == vatNumber.vat;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vat);
    }
}

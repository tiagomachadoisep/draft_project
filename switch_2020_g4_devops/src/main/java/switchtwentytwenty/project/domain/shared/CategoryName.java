package switchtwentytwenty.project.domain.shared;


import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import java.util.Objects;
import java.util.regex.Pattern;

import static switchtwentytwenty.project.utils.StringUtils.capitalize;



public class CategoryName {
    private final String aCategoryName;

    public CategoryName(String name) {
        if(validateName(name)) {
            this.aCategoryName = capitalize(name);
        } else {
            throw new InvalidDesignationException("Invalid category name");
        }
    }

    /**
     * validates category name introduced.
     *
     * @param name String not null, empty or blank.
     * @return true if name is correct or false if not.
     */
    private boolean validateName(String name) {
        if (name == null || name.isEmpty() || name.trim().isEmpty() || name.trim().length() < 3) {
            return false;
        }

        return validateFormat(name);
    }

    /**
     * Validates format of name. Name cannot have numbers or other special characters.
     * Extracted from https://stackoverflow.com/questions/15805555/java-regex-to-validate-full-name-allow-only
     * -spaces-and-letters
     * @param name String
     * @return true if is correctly introduced or false if not.
     */
    private boolean validateFormat(String name) {
        //Extracted from https://stackoverflow.com/questions/15805555/java-regex-to-validate-full-name-allow-only
        // -spaces-and-letters
        String nameRegex = "^[\\p{L} .'-]+$";

        Pattern pattern = Pattern.compile(nameRegex);
        return pattern.matcher(name).matches();
    }

    /**
     * Retrieves category name in String format.
     * @return String
     */
    public String toString() {
        return this.aCategoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryName that = (CategoryName) o;
        return Objects.equals(aCategoryName, that.aCategoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aCategoryName);
    }
}

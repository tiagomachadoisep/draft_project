package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.utils.GenerateID;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class FamilyRelationshipID implements ID, Serializable, GenerateID {

    private static final long serialVersionUID = -2304103127613053424L;

    String relationshipId;

    public FamilyRelationshipID(String relationshipId) {

        setRelationshipId(relationshipId);
    }

    public FamilyRelationshipID() {

        setRelationshipId(generateID());
    }


    public String getRelationshipId() {
        return this.relationshipId;
    }

    public void setRelationshipId(String relationshipId) {

        validateId(relationshipId);

        this.relationshipId = relationshipId;
    }

    private void validateId(String relationshipId) {
        if (relationshipId == null) {
            throw new NullArgumentException("The relationship Id cannot be null");
        }

        if (relationshipId.trim().isEmpty()) {
            throw new EmptyArgumentException("The relationship Id cannot be empty");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyRelationshipID that = (FamilyRelationshipID) o;
        return relationshipId.equals(that.relationshipId);
    }

    @Override
    public String toString() {
        return this.relationshipId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(relationshipId);
    }
}

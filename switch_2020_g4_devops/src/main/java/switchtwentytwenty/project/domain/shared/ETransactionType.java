package switchtwentytwenty.project.domain.shared;

public enum ETransactionType {

    PAYMENT, TRANSFER;
}

package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.Objects;
import java.util.regex.Pattern;


public class PhoneNumber implements ValueObject {
    private final String aPhoneNumber;

    /**
     * Sole constructor of Phone Number
     * @param aPhoneNumber String
     */
    public PhoneNumber(String aPhoneNumber)
    {
        if (!validate(aPhoneNumber))
            throw new IllegalArgumentException("Invalid Phone Number.");
        this.aPhoneNumber = aPhoneNumber;
    }

    /**
     * Checks if is a valid phone number
     * @param phoneNumber
     * @return true if is valid and false if not.
     */
    private boolean validate(String phoneNumber) {
        if (phoneNumber == null)
            return false;
        if (phoneNumber.trim().isEmpty())
            return false;
        if(phoneNumber.length()!=9)
            return false;
        if(phoneNumber.charAt(0)!='9' && phoneNumber.charAt(0)!='2')
            return false;
        return validateFormat(phoneNumber);
    }

    /**
     * method to validate phone number format.
     *
     * @param phoneNumber String
     * @return true if is correctly defined and false if not.
     */
    private boolean validateFormat(String phoneNumber) {
        //Extracted from: "https://www.baeldung.com/java-regex-validate-phone-numbers"
        String phoneRegex = "^\\d{9}$";
        Pattern pattern = Pattern.compile(phoneRegex);
        return pattern.matcher(phoneNumber).matches();
    }
    /**
     * Transfer phone number object data into string format.
     *
     * @return String of phone number object data.
     */
    public String toString() {
        return this.aPhoneNumber;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber that = (PhoneNumber) o;
        return Objects.equals(aPhoneNumber, that.aPhoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aPhoneNumber);
    }
}

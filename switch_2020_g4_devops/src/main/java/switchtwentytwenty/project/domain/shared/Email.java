package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import javax.persistence.Embeddable;
import java.util.Objects;
import java.util.regex.Pattern;

@Embeddable
public class Email implements ValueObject {

    private String emailAddress;

    /**
     * E-mail constructor
     * @param emailAddress String
     */
    public Email(String emailAddress) {
        if(!validateEmailAddress(emailAddress))
            throw new IllegalArgumentException("Invalid Email");

        this.emailAddress = emailAddress;

    }

    public Email() {

    }

    /**
     * Checks if the email is null or empty
     *
     * @param emailAddress String
     * @return true or false
     */
    private boolean validateEmailAddress(String emailAddress) {
        if (emailAddress == null || emailAddress.trim().isEmpty())
            return false;
        return validateFormat(emailAddress);
    }

    /**
     * Validates whether the email format is valid
     * Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
     * @param email String
     * @return true or false
     */
    private boolean validateFormat(String email) {

        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }

    /**
     * Compares email with string entrance
     *
     * @param anEmail String
     * @return boolean if is the same or not
     */
    public boolean sameEmail(String anEmail) {
        return this.emailAddress.equals(anEmail);
    }

    /**
     * Checks if the object passed by parameter is the same as the EmailAddress instance     *
     *
     * @param o: object's name
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Email)) return false;
        Email that = (Email) o;
        if(this.emailAddress==null)
            return that.emailAddress==null;
        return this.emailAddress.equals(that.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailAddress);
    }

    /**
     * Transfer email address object data into string format.
     *
     * @return String of email address object data.
     */
    public String toString() {
        return this.emailAddress;
    }
}

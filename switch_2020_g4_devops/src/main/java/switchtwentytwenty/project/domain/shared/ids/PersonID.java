package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.dddtypes.OwnerID;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
@Embeddable
public class PersonID implements ID, Serializable, OwnerID {

    private final Email personIdentifier; //como tornar serializable?

    public PersonID(Email personIdentifier) {
        this.personIdentifier = personIdentifier;
    }

    public PersonID() {
        this.personIdentifier =new Email();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonID personID1 = (PersonID) o;
        return personIdentifier.equals(personID1.personIdentifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personIdentifier);
    }

    @Override
    public String toString() {
        if(personIdentifier ==null)
            return "";
        return personIdentifier.toString();
    }

    @Override
    public boolean isPerson() {
        return true;
    }
}

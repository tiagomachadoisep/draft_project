package switchtwentytwenty.project.domain.shared;

public enum EAccountType {

    PERSONAL_CASH, FAMILY_CASH, CREDIT_CARD, BANK_SAVINGS, BANK;

}

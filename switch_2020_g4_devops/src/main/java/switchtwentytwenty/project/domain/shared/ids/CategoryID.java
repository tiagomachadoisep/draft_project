package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.utils.GenerateID;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CategoryID implements ID, Serializable, GenerateID {

    private static final long serialVersionUID = 549498388370130459L;
    private final String identification;

    public CategoryID(String id) {
        this.identification = id;
    }

    public CategoryID() {
        this.identification = generateID();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryID that = (CategoryID) o;
        return identification.equals(that.identification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identification);
    }

    @Override
    public String toString() {
        return identification;

    }
}

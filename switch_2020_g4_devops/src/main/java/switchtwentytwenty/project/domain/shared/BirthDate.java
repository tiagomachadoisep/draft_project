package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.regex.Pattern;


public class BirthDate implements ValueObject {
    String theBirthDate;

    public BirthDate(String birthDate) {
        if (validateDate(birthDate) )
            this.theBirthDate = birthDate;
        else
            throw new IllegalArgumentException("Invalid date format, should be DD-MM-YYYY");

    }


    private boolean validateDate(String birthDate){
        if (birthDate == null || birthDate.trim().isEmpty()) {
            return false;
        }else {
            return validateDateFormat(birthDate);
        }
    }

    private boolean validateDateFormat(String birthDate) {
        String dateRegex = "^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4}$";
        Pattern pattern = Pattern.compile(dateRegex);
        return pattern.matcher(birthDate).matches();
    }


    @Override
    public String toString() {
        return theBirthDate;
    }
}

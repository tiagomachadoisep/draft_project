package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.domain.shared.ids.AccountID;

import java.util.Objects;

public class Movement {
    private final AccountID accountId;
    private final MoneyValue value;

    public Movement(AccountID accountId, MoneyValue value) {
        this.accountId = accountId;
        this.value = value;
    }

    public AccountID getAccountId() {
        return accountId;
    }

    public MoneyValue getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movement)) return false;
        Movement movement = (Movement) o;
        return accountId.equals(movement.accountId) && value.equals(movement.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, value);
    }
}

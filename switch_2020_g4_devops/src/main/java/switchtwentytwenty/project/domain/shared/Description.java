package switchtwentytwenty.project.domain.shared;


import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import java.util.Objects;


public class Description {

    private final String descriptionValue;

    /**
     * Description constructor
     * @param description String
     */
    public Description(String description) {
        if (validateDescription(description)) {
            this.descriptionValue = description;
        } else {
            throw new InvalidDesignationException("Invalid account description");
        }

    }

    /**
     * Validates account description. This description cannot be null, empty or less than 3 characters.
     * @param description String
     * @return true if valid and false if not
     */
    private boolean validateDescription(String description) {
        return description != null && !description.trim().isEmpty() && description.trim().length() >= 3;
    }

    /**
     * Returns the account description in String format.
     * @return String
     */
    @Override
    public String toString() {
        return descriptionValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Description that = (Description) o;
        return Objects.equals(descriptionValue, that.descriptionValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(descriptionValue);
    }
}

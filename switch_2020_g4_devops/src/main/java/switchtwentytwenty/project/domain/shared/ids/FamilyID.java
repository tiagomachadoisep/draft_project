package switchtwentytwenty.project.domain.shared.ids;


import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.dddtypes.OwnerID;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.utils.GenerateID;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@AttributeOverrides({
        @AttributeOverride(name = "familyId", column = @Column(name = "familyId")),
})
public class FamilyID implements ID, Serializable, OwnerID, GenerateID {

    private static final long serialVersionUID = -1895672102540050641L;
    private String familyIdValue;

    public FamilyID(String id) {
        setFamilyIdValue(id);
    }

    public FamilyID() {
        setFamilyIdValue(generateID());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FamilyID familyID = (FamilyID) o;
        return familyIdValue.equals(familyID.familyIdValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyIdValue);
    }

    public String getFamilyIdValue() {
        return familyIdValue;
    }

    public void setFamilyIdValue(String familyId) {

        validateId(familyId);

        this.familyIdValue = familyId;
    }

    private void validateId(String id) {
        if (id == null) {
            throw new NullArgumentException("The family Id cannot be null");
        }

        if (id.trim().isEmpty()) {
            throw new EmptyArgumentException("The family Id cannot be empty");
        }
    }

    @Override
    public String toString() {
        return familyIdValue;
    }

    @Override
    public boolean isPerson() {
        return false;
    }
}

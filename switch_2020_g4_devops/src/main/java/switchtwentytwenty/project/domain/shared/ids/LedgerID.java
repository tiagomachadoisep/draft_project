package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.dddtypes.ID;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
@Embeddable
public class LedgerID implements ID, Serializable {

    private int ledgerIdValue;

    public LedgerID(int ledgerID) {
        this.ledgerIdValue = ledgerID;
    }

    public LedgerID() {

    }

    public int getLedgerIdValue() {
        return ledgerIdValue;
    }

    public void setLedgerIdValue(int ledgerID) {
        this.ledgerIdValue = ledgerID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LedgerID)) return false;
        LedgerID ledgerID1 = (LedgerID) o;
        return ledgerIdValue == ledgerID1.ledgerIdValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ledgerIdValue);
    }


}

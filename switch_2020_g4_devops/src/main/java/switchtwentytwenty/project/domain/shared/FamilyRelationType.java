package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.Locale;
import java.util.Objects;

public class FamilyRelationType implements ValueObject {

    private String designation;

    /**
     * Constructor for FamilyRelationType. Makes use of the {@linkplain FamilyRelationType#setDesignation} method.
     *
     * @param designation String designation to caracterize the familyRelationType object.
     */
    public FamilyRelationType(String designation) {

        setDesignation(designation);
    }

    /**
     * This method sets the familyRelationType designation after validation through the
     * {@linkplain FamilyRelationType#validateDesignation} method.
     *
     * @param designation
     */
    private void setDesignation(String designation){
        String fixedDesignation;

        fixedDesignation = validateDesignation(designation);

        this.designation = fixedDesignation;
    }

    /**
     * This method validates the designation String passed as argument. If the designation is null or empty, an
     * {@linkplain Exception} is thrown.
     *
     * @param designation String designation passed as object to define the familyRelationType object.
     */
    private String validateDesignation(String designation) {

        boolean designationIsValid;
        String fixedDesignation;

        if (isObjectNull(designation)) {
            throw new NullArgumentException("The relationship designation cannot be null.");
        }
        if (designation.trim().isEmpty()) {
            throw new EmptyArgumentException("The relationship designation cannot be empty.");
        }

        fixedDesignation = designation.trim().toLowerCase(Locale.ROOT);

        designationIsValid = RelationType.isThereValueOfType(fixedDesignation);

        if (!designationIsValid) {
            throw new InvalidDesignationException("The relationship designation '" + designation +  "' is invalid.");
        } else {
            return fixedDesignation;
        }
    }

    /**
     * This method validates if an object passed as argument is null.
     *
     * @param object Object of any type passed as argument.
     * @return Boolean: true if the object is null; false if the object is not null.
     */
    private boolean isObjectNull(Object object) {

        return object == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyRelationType that = (FamilyRelationType) o;
        return designation.equals(that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }

    @Override
    public String toString() {
        return this.designation;
    }
}


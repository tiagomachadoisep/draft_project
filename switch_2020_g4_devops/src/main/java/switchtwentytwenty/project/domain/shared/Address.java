package switchtwentytwenty.project.domain.shared;


import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;


public class Address implements ValueObject {
    private final String addressValue;

    public Address(String address) {
        if (validateAddress(address)) {
            this.addressValue = address;
        } else {
           throw new IllegalArgumentException("Invalid address format");
        }

    }


    private boolean validateAddress(String address) {
        if (address == null || address.trim().isEmpty())
            return false;
        return true;
    }


    @Override
    public String toString() {
        return addressValue;
    }
}

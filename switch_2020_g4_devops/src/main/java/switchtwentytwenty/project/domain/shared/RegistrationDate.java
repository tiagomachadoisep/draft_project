package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.exceptions.InvalidDateExpressionException;
import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.Objects;

public class RegistrationDate implements ValueObject {

    private String theRegistrationDate;

    public RegistrationDate(String regDate) {

        if(validate(regDate)) {
            this.theRegistrationDate = regDate;
        } else {
            throw new InvalidDateExpressionException("Invalid date format");
        }
    }

    private boolean validate(String regDate) {
        return regDate != null && !regDate.trim().isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegistrationDate that = (RegistrationDate) o;
        return theRegistrationDate.equals(that.theRegistrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(theRegistrationDate);
    }

    @Override
    public String toString() {
        return theRegistrationDate;
    }
}

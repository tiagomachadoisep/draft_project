package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class TransactionDate implements ValueObject {
    LocalDate transactionDateValue;

    public TransactionDate(LocalDate transactionDateValue) {
        this.transactionDateValue = transactionDateValue;
    }

    public TransactionDate(String transactionDateValue) {
        if(transactionDateValue ==null)
            this.transactionDateValue =LocalDate.MIN;
        else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            this.transactionDateValue = LocalDate.parse(transactionDateValue, formatter);
        }

    }

    public LocalDate getTransactionDate() {
        return transactionDateValue;
    }

    public boolean isBetween(TransactionDate startDate, TransactionDate endDate) {
        try {
            return transactionDateValue.isAfter(startDate.getTransactionDate())
                    && transactionDateValue.isBefore(endDate.getTransactionDate());
        } catch (NullPointerException exception) {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionDate)) return false;
        TransactionDate date = (TransactionDate) o;
        return transactionDateValue.equals(date.transactionDateValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionDateValue);
    }
}
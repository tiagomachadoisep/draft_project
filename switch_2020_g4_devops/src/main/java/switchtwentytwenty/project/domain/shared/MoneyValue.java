package switchtwentytwenty.project.domain.shared;

import java.util.Currency;
import java.util.Objects;

public class MoneyValue {

    private final double value;
    private final Currency currency;

    /**
     * Constructor for MoneyValue for the default currency (EUR)
     *
     * @param value double
     */
    public MoneyValue(double value) {
        this.value = value;
        this.currency = Currency.getInstance("EUR");
    }

    /**
     * Constructor for MoneyValue for a different currency
     *
     * @param value    double
     * @param currency String
     */
    public MoneyValue(double value, String currency) {
        this.value = value;
        this.currency = Currency.getInstance(currency);
    }

    /**
     * Verifies if the balance is positive.
     * @return true if positive or false if not.
     */
    public boolean isPositive() {
        return value>0;
    }

    /**
     * Verifies if the balance is negative.
     *
     * @return true if negative or false if not.
     */
    public boolean isNegative() {
        return value < 0;
    }

    /**
     * Returns the value of the balance.
     *
     * @return double
     */
    public double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoneyValue that = (MoneyValue) o;
        return Double.compare(that.value, value) == 0 && Objects.equals(currency, that.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }

    public MoneyValue toNegative() {
        return new MoneyValue(-this.value);
    }

    public MoneyValue add(MoneyValue amount) {
        return new MoneyValue(this.value+ amount.value);
    }

    public MoneyValue subtract(MoneyValue amount) {
        return new MoneyValue(this.value - amount.value);
    }
}

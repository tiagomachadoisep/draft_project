package switchtwentytwenty.project.domain.shared.dddtypes;


public interface Entity<T extends ID> {

    T getID();

    boolean isID(T identity);
}

package switchtwentytwenty.project.domain.shared.dddtypes;


public interface OwnerID {

    boolean isPerson();
}

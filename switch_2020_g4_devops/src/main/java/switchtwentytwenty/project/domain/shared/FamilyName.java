package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.domain.shared.dddtypes.ValueObject;

import java.util.Objects;
import java.util.regex.Pattern;

public class FamilyName implements ValueObject {
    private String name;
    private static final String ERROR_MESSAGE = "Invalid name.";

    /**
     * FamilyName Constructor.
     * @param name String
     */
    public FamilyName(String name) {
        if (validateName(name)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException(ERROR_MESSAGE);
        }

    }

    /**
     * Validates name introduced.
     * @param name String
     * @return true if is valid and false if not.
     */
    private boolean validateName(String name) {
        if (name == null || name.trim().isEmpty()) {
            return false;
        }

        return validateFormat(name);
    }

    /**
     * Method that validates name format. E.g, it cannot have numbers.
     * Extracted from https://stackoverflow.com/questions/15805555/java-regex-to-validate-full-name-allow-only
     * -spaces-and-letters
     * @param name String
     * @return true if is valid or false if not.
     */
    private boolean validateFormat(String name) {

        String nameRegex = "^[a-zA-Z]*$";

        Pattern pattern = Pattern.compile(nameRegex);
        return pattern.matcher(name).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FamilyName that = (FamilyName) o;
        return name.equals(that.name);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

package switchtwentytwenty.project.domain.shared.ids;

import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.utils.GenerateID;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TransactionID implements ID, Serializable, GenerateID {

    private static final long serialVersionUID = 2581067520849121292L;
    private String id;

    public TransactionID(String id) {
        this.id = id;
    }

    public TransactionID() {
        this.id = generateID();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionID)) return false;
        TransactionID that = (TransactionID) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id;
    }
}

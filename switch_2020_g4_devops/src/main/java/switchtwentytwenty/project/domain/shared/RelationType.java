package switchtwentytwenty.project.domain.shared;

import switchtwentytwenty.project.exceptions.NullArgumentException;

public enum RelationType {

    MOTHER("mother"),
    FATHER("father"),
    SON("son"),
    DAUGHTER("daughter"),
    HUSBAND("husband"),
    WIFE("wife"),
    BROTHER("brother"),
    SISTER("sister"),
    COUSIN("cousin"),
    AUNT("aunt"),
    UNCLE("uncle"),
    NEPHEW("nephew"),
    NIECE("niece"),
    GRANDFATHER("grandfather"),
    GRANDMOTHER("grandmother"),
    GRANDSON("grandson"),
    GRANDDAUGHTER("granddaughter"),
    FRIEND("friend"),
    BOYFRIEND("boyfriend"),
    GIRLFRIEND("girlfriend"),
    SISTER_IN_LAW("sister-in-law"),
    BROTHER_IN_LAW("brother-in-law"),
    FATHER_IN_LAW("father-in-law"),
    MOTHER_IN_LAW("mother-in-law"),
    STEPFATHER("stepfather"),
    STEPMOTHER("stepmother"),
    STEPDAUGHTER("stepdaughter"),
    STEPSON("stepson"),
    STEPSISTER("stepsister"),
    STEPBROTHER("stepbrother");

    private final String designation;

    private RelationType(String designation) {
        this.designation = designation;
    }

    /**
     * This method checks if there is an existing ENUM field with value equal to some string passed as argument.
     * @param designation The String passed as argument.
     * @return Boolean: True if there is a valid ENUM. False if there is not valid ENUM with that string as designation.
     * @throws NullArgumentException To be thrown in case the passed string is null.
     */
    public static boolean isThereValueOfType(String designation){
        boolean typeMatches = false;

        if (designation == null){
            throw new NullArgumentException("Passed string for designation cannot be null.");
        }

        RelationType[] relationTypes = values();

        for (int index = 0; index < relationTypes.length && !typeMatches; index++) {
            if (designation.equals(relationTypes[index].designation)){
                typeMatches = true;
            }
        }

        return typeMatches;
    }

    @Override
    public String toString(){
        return this.designation;
    }
}

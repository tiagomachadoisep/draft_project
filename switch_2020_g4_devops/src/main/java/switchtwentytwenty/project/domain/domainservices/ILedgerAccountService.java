package switchtwentytwenty.project.domain.domainservices;


import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

public interface ILedgerAccountService {
    TransactionID addPayment(Ledger memberLedger, Account memberAccount, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date);

    TransactionID addTransfer(Ledger familyLedger, Ledger memberLedger, Account memberAccount, Account familyAccount, MoneyValue moneyAmount, CategoryID categoryId,TransactionDate date);

}

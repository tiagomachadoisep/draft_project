package switchtwentytwenty.project.domain.domainservices;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

@Service
public class LedgerAccountService implements ILedgerAccountService{

    @Override
    public TransactionID addPayment(Ledger memberLedger, Account memberAccount, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date) {
        if(memberAccount.getBalance().subtract(moneyAmount).isNegative())
            return new TransactionID("-1");
        TransactionID transactionID = new TransactionID();
        memberLedger.addPayment(transactionID,memberAccount.getID(), moneyAmount,categoryId,date);
        memberAccount.removeFunds(moneyAmount);
        return transactionID;
    }

    @Override
    public TransactionID addTransfer(Ledger familyLedger, Ledger memberLedger, Account memberAccount, Account otherAccount, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date) {
        if(memberAccount.getBalance().subtract(moneyAmount).isNegative())
            return new TransactionID("-1");
        TransactionID transactionID = new TransactionID();
        familyLedger.addTransfer(transactionID,memberAccount.getID(),otherAccount.getID(),moneyAmount,categoryId,date);
        memberLedger.addTransfer(transactionID,memberAccount.getID(),otherAccount.getID(),moneyAmount,categoryId,date);

        memberAccount.removeFunds(moneyAmount);
        otherAccount.addFunds(moneyAmount);

        return transactionID;

    }
}

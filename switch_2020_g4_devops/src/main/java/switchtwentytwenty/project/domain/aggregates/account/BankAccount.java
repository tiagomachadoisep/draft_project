package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;


public class BankAccount extends ElectronicAccount {

    /**
     * Builder class for a bank account.
     */
    public static class Builder implements AccountBuilder {
        private final AccountID accountID;
        private PersonID personID;
        private EAccountType accountType;
        private Description description;
        private MoneyValue balance;

        /**
         * Builder initializer for a bank account with account id.
         *
         * @param accountID AccountID
         */
        public Builder(AccountID accountID) {
            this.accountID = accountID;
        }

        /**
         * Sets the account type for a bank account.
         *
         * @return Builder
         */
        public Builder setAccountType() {
            this.accountType = EAccountType.BANK;
            return this;
        }

        /**
         * Sets the owner ID for a bank account
         *
         * @param ownerID ID
         * @return Builder
         */
        public Builder setOwnerID(ID ownerID) {
            this.personID = (PersonID) ownerID;
            return this;
        }

        /**
         * Sets the balance for a bank account.
         *
         * @param balance MoneyValue
         * @return Builder
         */
        public Builder setBalance(MoneyValue balance) {
            this.balance = balance;
            return this;
        }

        /**
         * Sets the description for a bank account.
         *
         * @param description Description
         * @return Builder
         */
        public Builder setDescription(Description description) {
            this.description = description;
            return this;
        }

        /**
         * Builds a bank account.
         *
         * @return BankAccount
         */
        public BankAccount build() {
            BankAccount bankAccount = new BankAccount();
            bankAccount.accountId = this.accountID;
            bankAccount.accountType = this.accountType;
            bankAccount.description = this.description;
            bankAccount.balance = this.balance;
            bankAccount.personId = this.personID;

            return bankAccount;
        }

    }

    protected BankAccount() {
    }
}

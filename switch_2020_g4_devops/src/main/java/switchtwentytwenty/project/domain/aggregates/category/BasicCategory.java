package switchtwentytwenty.project.domain.aggregates.category;


import lombok.Setter;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;

import java.util.Objects;


@Setter
public abstract class BasicCategory implements Categorable {

    protected CategoryID id;
    protected CategoryName name;
    protected CategoryID parentId;


    public CategoryID getID() {
        return this.id;
    }

    public boolean isID(CategoryID identity) {
        return this.id.equals(identity);
    }

    /**
     * Method that checks if a category has the same name of another
     *
     * @param name String
     * @return boolean
     */
    public boolean hasSameName(CategoryName name) {
        return this.name.equals(name);
    }


    /**
     * Method that checks if a category is at the root level
     *
     * @return boolean
     */
    public boolean isAtRoot() {
        return this.parentId == null;
    }


    public CategoryName getName() {
        return this.name;
    }

    public CategoryID getParentId() {
        return this.parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicCategory that = (BasicCategory) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

package switchtwentytwenty.project.domain.aggregates.category;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;


public interface Categorable extends AggregateRoot<CategoryID> {

    /**
     * Method that checks if a category has the same name of another
     *
     * @param name String
     * @return boolean
     */
    boolean hasSameName(CategoryName name);


    /**
     * Method that checks if a category is standard
     *
     * @return boolean
     */
    boolean isStandard();

    /**
     * Method that checks if a category is at the root level
     *
     * @return boolean
     */
    boolean isAtRoot();

    /**
     * Method that checks if a category can be used by a family
     *
     * @param familyId FamilyID
     * @return boolean
     */
    boolean canBeUsedByFamily(FamilyID familyId);

    CategoryName getName();
    CategoryID getParentId();

    FamilyID getFamilyId();

}

package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;

public interface AccountBuilder {
    Account build();
    AccountBuilder setAccountType();
    AccountBuilder setDescription(Description description);
    AccountBuilder setOwnerID(ID ownerID);
    AccountBuilder setBalance(MoneyValue balance);
}

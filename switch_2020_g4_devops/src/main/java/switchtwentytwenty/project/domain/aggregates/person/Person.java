package switchtwentytwenty.project.domain.aggregates.person;

import lombok.Getter;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
public class Person implements AggregateRoot<PersonID> {
    private PersonID mainEmail;
    private PersonName name;
    private VATNumber vatNumber;
    private Address address;
    private FamilyID familyID;
    private List<Email> otherEmails;
    private List<PhoneNumber> phoneNumbers;
    private BirthDate birthDate;

    private ERole role;

    protected Person() {
    }

    /**
     * Checks if two persons have the same family.
     *
     * @param otherMember other person
     * @return true if they have and false if not.
     */
    public boolean hasSameFamily(Person otherMember) {
        return this.familyID.equals(otherMember.getFamilyID());
    }

    /**
     * Add a secondary email to the list of other emails
     *
     * @param email a secondary email
     * @return the created object
     */
    public boolean addEmailToProfile(String email) {
        if (email.trim().isEmpty()) {
            throw new IllegalArgumentException("Impossible to add this email.");
        }
        try {
            Email newEmail = new Email(email);
            if (this.mainEmail.equals(new PersonID(newEmail)) || this.otherEmails.contains(newEmail)) {
                return false;
            } else {
                otherEmails.add(newEmail);
                return true;
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Impossible to add this email.");
        }
    }

    /**
     * Returns person id.
     *
     * @return person id.
     */
    @Override
    public PersonID getID() {
        return mainEmail;
    }

    public ERole getRole() {
        return role;
    }

    /**
     * Checks if the id matches the person id.
     *
     * @param identity id
     * @return true if it's the same and false if not.
     */
    @Override
    public boolean isID(PersonID identity) {
        return identity.equals(mainEmail);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return mainEmail.equals(person.mainEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mainEmail);
    }

    public static class Builder {
        private final PersonID mainEmail;
        private List<PhoneNumber> phoneNumbers = new ArrayList<>();
        private PersonName name;
        private VATNumber vatNumber;
        private Address address;
        private FamilyID familyID;
        private List<Email> otherEmails = new ArrayList<>();
        private BirthDate birthDate;
        private ERole role;

        public Builder(PersonID mainEmail) {
            this.mainEmail = mainEmail;
        }

        public Builder setName(PersonName name) {
            this.name = name;
            return this;
        }

        public Builder setVat(VATNumber vat) {
            this.vatNumber = vat;
            return this;
        }

        public Builder setAddress(Address address) {
            this.address = address;
            return this;
        }

        public Builder setFamilyId(FamilyID familyId) {
            this.familyID = familyId;
            return this;
        }

        public Builder setOtherEmails(List<Email> otherEmails) {
            this.otherEmails = new ArrayList<>(otherEmails);
            return this;
        }

        public Builder setPhoneNumbers(List<PhoneNumber> numbers) {
            this.phoneNumbers = new ArrayList<>(numbers);
            return this;
        }

        public Builder setBirthDate(BirthDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder setRoleAsFamilyAdministrator(){
            this.role = ERole.ADMIN;
            return this;
        }

        public Builder setRoleAsUser(){
            this.role=ERole.USER;
            return this;
        }

        public Builder setRoleAsSystemManager(){
            this.role=ERole.SYSADMIN;
            return this;
        }

        public Builder setRole(ERole role){
            this.role=role;
            return this;
        }

        public Person build() {
            Person person = new Person();
            person.mainEmail = this.mainEmail;
            person.address = this.address;
            person.familyID = this.familyID;
            person.vatNumber = this.vatNumber;
            person.name = this.name;
            person.otherEmails = this.otherEmails;
            person.phoneNumbers = this.phoneNumbers;
            person.birthDate = this.birthDate;
            person.role = this.role;
            return person;
        }
    }
}

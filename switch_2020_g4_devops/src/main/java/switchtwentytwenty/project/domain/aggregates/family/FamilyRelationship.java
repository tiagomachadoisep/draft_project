package switchtwentytwenty.project.domain.aggregates.family;

import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.dddtypes.Entity;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.NullArgumentException;

public class FamilyRelationship implements Entity<FamilyRelationshipID> {

    private PersonID personOneID;
    private PersonID personTwoID;
    private FamilyRelationshipID familyRelationshipID;
    private FamilyRelationType relationType;

    /**
     * Constructor of FamilyRelationship. Uses {@linkplain FamilyRelationship#setRelationship} method to
     * set the attributes.
     *
     * @param personOneID  PersonID object of the first family member passed as argument.
     * @param personTwoID  PersonID object of the second family member passed as argument.
     * @param relationType String of the relation designation.
     * @param relationshipId id of relation
     */
    public FamilyRelationship(PersonID personOneID, PersonID personTwoID, FamilyRelationType relationType, FamilyRelationshipID relationshipId) {

        setRelationship(personOneID, personTwoID, relationType, relationshipId);
    }

    public FamilyRelationship() {
    }

    /**
     * This method sets the attributes of a familyRelation object by the passed arguments after validation through
     * the {@linkplain FamilyRelationship#validateRelationship} method.
     *
     * @param personOneID  Email object of the first family member passed as argument.
     * @param personTwoID  Email object of the second family member passed as argument.
     * @param relationType String of the relation designation.
     */
    private void setRelationship(PersonID personOneID, PersonID personTwoID,
                                 FamilyRelationType relationType, FamilyRelationshipID relationshipID) {

        validateRelationship(personOneID, personTwoID, relationType, relationshipID);

        this.personOneID = personOneID;
        this.personTwoID = personTwoID;
        this.relationType = relationType;
        this.familyRelationshipID = relationshipID;
    }

    /**
     * This method validates if the two emails passed as argument are null. If any of them is null, a {@linkplain
     * NullPointerException} is thrown.
     *
     * @param personOneID
     * @param personTwoID
     */
    private void validateRelationship(PersonID personOneID, PersonID personTwoID,
                                      FamilyRelationType relationType, FamilyRelationshipID relationshipID) {

        validatePersonOneID(personOneID);

        validatePersonTwoID(personTwoID);

        validateFamilyRelationType(relationType);

        validateFamilyRelationshipID(relationshipID);

    }

    private void validateFamilyRelationshipID(FamilyRelationshipID relationshipID) {
        if (isObjectNull(relationshipID)) {
            throw new NullArgumentException("The relationship id passed as argument cannot be null.");
        }
    }

    private void validateFamilyRelationType(FamilyRelationType relationType) {
        if (isObjectNull(relationType)) {
            throw new NullArgumentException("The relation type passed as argument cannot be null.");
        }
    }

    private void validatePersonTwoID(PersonID personTwoID) {
        if (isObjectNull(personTwoID)) {
            throw new NullArgumentException("The email of the personTwo passed as argument cannot be null.");
        }
    }

    private void validatePersonOneID(PersonID personOneID) {
        if (isObjectNull(personOneID)) {
            throw new NullArgumentException("The email of the personOne passed as argument cannot be null.");
        }
    }

    /**
     * This method validates if an object passed as argument is null.
     *
     * @param object Object of any type passed as argument.
     * @return Boolean: true if the object is null; false if the object is not null.
     */
    private boolean isObjectNull(Object object) {

        return object == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyRelationship that = (FamilyRelationship) o;
        return (personOneID.equals(that.personOneID) && personTwoID.equals(that.personTwoID)) ||
                (personTwoID.equals(that.personOneID) && personOneID.equals(that.personTwoID));
    }

    public void setFamilyRelationshipID(FamilyRelationshipID familyRelationshipID) {

        validateFamilyRelationshipID(familyRelationshipID);

        this.familyRelationshipID = familyRelationshipID;
    }

    public PersonID getPersonOneID() {
        return personOneID;
    }

    public void setPersonOneID(PersonID personOneID) {

        validatePersonOneID(personOneID);

        this.personOneID = personOneID;
    }

    public PersonID getPersonTwoID() {
        return personTwoID;
    }

    public void setPersonTwoID(PersonID personTwoID) {

        validatePersonTwoID(personTwoID);

        this.personTwoID = personTwoID;
    }

    @Override
    public int hashCode() {
        return personOneID.hashCode() * personTwoID.hashCode();
    }

    @Override
    public FamilyRelationshipID getID() {
        return familyRelationshipID;
    }

    @Override
    public boolean isID(FamilyRelationshipID identity) {
        return familyRelationshipID.equals(identity);
    }

    public FamilyRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(FamilyRelationType newRelation) {

        validateFamilyRelationType(newRelation);

        this.relationType = newRelation;
    }

    /**
     * Confirms if the relation is parent type.
     *
     * @return true if it is and false if not.
     */
    public boolean isParent() {
        return this.relationType.toString().equals("father") || this.relationType.toString().equals("mother");
    }

    /**
     * Checks if given members are the same persons that identifies the relationship.
     *
     * @param personOneID first member
     * @param personTwoID second member
     * @return true if it is and false otherwise.
     */
    public boolean areTheSamePersons(PersonID personOneID, PersonID personTwoID) {
        return this.personOneID.equals(personOneID) && this.personTwoID.equals(personTwoID);
    }
}

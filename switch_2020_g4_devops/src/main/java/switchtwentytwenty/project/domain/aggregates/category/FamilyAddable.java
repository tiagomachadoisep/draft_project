package switchtwentytwenty.project.domain.aggregates.category;

import switchtwentytwenty.project.domain.aggregates.family.Family;

public interface FamilyAddable {

    Family getFamily();

    boolean isCustom();


}

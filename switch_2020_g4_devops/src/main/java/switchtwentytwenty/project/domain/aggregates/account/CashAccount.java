package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.Objects;

public abstract class CashAccount implements Account {


    protected PersonID ownerID;
    protected AccountID accountId;
    protected EAccountType accountType;
    protected Description description;
    protected MoneyValue balance;


    /**
     * Overrides getID() method from Entity interface
     *
     * @return ID
     */
    @Override
    public AccountID getID() {
        return this.accountId;
    }

    /**
     * Verify if the account ids are equals.
     *
     * @param identity account id
     * @return boolean
     */
    @Override
    public boolean isID(AccountID identity) {
        return this.accountId.equals(identity);
    }

    /**
     * Overrides getAccountType() method from Account interface
     *
     * @return AccountType
     */
    @Override
    public EAccountType getAccountType() {
        return this.accountType;
    }

    /**
     * Confirm that the account is a Cash account
     *
     * @return boolean
     */
    @Override
    public boolean isCashAccount() {
        return true;
    }


    /**
     * Verify if the Id is the owner's account id
     *
     * @param id id, person or family
     * @return boolean
     */
    @Override
    public boolean isAccountOwner(ID id) {
        return this.ownerID.equals(id);
    }

    /**
     * Method to get the owner id.
     *
     * @return ID
     */
    @Override
    public PersonID getOwnerID() {
        return this.ownerID;
    }

    /**
     * Method to get the description.
     *
     * @return Description
     */
    @Override
    public Description getDescription() {
        return description;
    }

    /**
     * Get the balance
     *
     * @return a money value
     */
    @Override
    public MoneyValue getBalance() {
        return this.balance;
    }

    /**
     * Add funds in the balance
     *
     * @param amount a value
     */
    public void addFunds(MoneyValue amount) {
        this.balance = balance.add(amount);
    }

    /**
     * Remove funds from the balance
     *
     * @param amount a value
     */
    public void removeFunds(MoneyValue amount) {
        this.balance = balance.subtract(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CashAccount that = (CashAccount) o;
        return Objects.equals(accountId, that.accountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId);
    }


}

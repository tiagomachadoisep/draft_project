package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

public interface Account extends AggregateRoot<AccountID> {

    /**
     * Checks if an account is a cash account
     *
     * @return boolean
     */
    boolean isCashAccount();

    /**
     * Checks if a given ID is the ID of the owner of an account
     *
     * @param id ID
     * @return boolean
     */
    boolean isAccountOwner(ID id);

    /**
     * Method to get the owner id.
     *
     * @return ID
     */
    PersonID getOwnerID();

    /**
     * Method to get the description.
     *
     * @return Description
     */
    Description getDescription();

    /**
     * Method to get balance.
     *
     * @return MoneyValue
     */
    MoneyValue getBalance();

    /**
     * Gets the account type of an account (Cash or Electronic)
     *
     * @return AccountType
     */
    EAccountType getAccountType();


    void removeFunds(MoneyValue moneyAmount);
    void addFunds(MoneyValue moneyAmount);
}

package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

public class PersonalCashAccount extends CashAccount {


    /**
     * Builder class for a personal cash account.
     */
    public static class Builder implements AccountBuilder {

        private final AccountID accountID;
        private EAccountType accountType;
        private Description description;
        private MoneyValue balance;
        private PersonID ownerID;

        /**
         * Builder initializer for a personal cash account.
         *
         * @param accountID AccountID
         */
        public Builder(AccountID accountID) {
            this.accountID = accountID;
        }

        /**
         * Sets the account type for a personal cash account.
         *
         * @return Builder
         */
        public Builder setAccountType() {
            this.accountType = EAccountType.PERSONAL_CASH;
            return this;
        }

        /**
         * Sets the description for a personal cash account.
         *
         * @param description Description
         * @return Builder
         */
        public Builder setDescription(Description description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the balance for a personal cash account.
         *
         * @param balance MoneyValue
         * @return Builder
         */
        public Builder setBalance(MoneyValue balance) {
            this.balance = balance;
            return this;
        }

        /**
         * Sets the owner ID for a personal cash account
         *
         * @param ownerID ID
         * @return Builder
         */
        public Builder setOwnerID(ID ownerID) {
            this.ownerID = (PersonID) ownerID;
            return this;
        }

        /**
         * Builds a personal cash account.
         *
         * @return PersonalCashAccount
         */
        public PersonalCashAccount build() {
            PersonalCashAccount personalCashAccount = new PersonalCashAccount();
            personalCashAccount.accountId = this.accountID;
            personalCashAccount.accountType = this.accountType;
            personalCashAccount.description = this.description;
            personalCashAccount.balance = this.balance;
            personalCashAccount.ownerID = this.ownerID;
            return personalCashAccount;
        }
    }

    /**
     * Empty personal cash account constructor so as to enable the builder
     */
    protected PersonalCashAccount() {
    }



}

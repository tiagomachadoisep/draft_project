package switchtwentytwenty.project.domain.aggregates.ledger;

import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import java.time.LocalDate;
import java.util.Objects;

public class Payment implements Transactionable {

    private final CategoryID categoryId;
    private final TransactionID transactionID;
    private Description description;
    private final Movement debit;
    private TransactionDate transactionDate = new TransactionDate(LocalDate.now().toString());

    public Payment(TransactionID transactionID, AccountID id, MoneyValue amount, CategoryID categoryId) {
        this.transactionID = transactionID;
        this.categoryId = categoryId;
        this.debit = new Movement(id, amount.toNegative());
    }
    public Payment(TransactionID transactionID, AccountID id, MoneyValue amount, CategoryID categoryId, Description description, TransactionDate transactionDate) {
        this.transactionID = transactionID;
        this.description = description;
        this.categoryId = categoryId;
        this.transactionDate = transactionDate;
        this.debit = new Movement(id, amount.toNegative());
    }

    public Description getDescription() {
        return this.description;
    }

    public CategoryID getCategoryId() {
        return categoryId;
    }

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.PAYMENT;
    }

    @Override
    public AccountID getAccountID() {
        return debit.getAccountId();
    }

    @Override
    public double getAmount() {
        return debit.getValue().getValue();
    }

    @Override
    public AccountID getDestinationId() {
        return null;
    }

    @Override
    public TransactionID getID() {
        return transactionID;
    }

    @Override
    public boolean isID(TransactionID identity) {
        return transactionID.equals(identity);
    }

    @Override
    public Movement getMovementDebit() {
        return debit;
    }

    @Override
    public boolean isPayment() {
        return true;
    }

    @Override
    public boolean isTransfer() {
        return false;
    }

    @Override
    public TransactionDate getTransactionDate() {
        return transactionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;
        Payment payment = (Payment) o;
        return transactionID.equals(payment.transactionID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionID);
    }
}

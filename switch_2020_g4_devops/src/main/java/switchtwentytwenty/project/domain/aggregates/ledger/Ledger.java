package switchtwentytwenty.project.domain.aggregates.ledger;


import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.ids.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Ledger implements AggregateRoot<LedgerID> {

    private final LedgerID ledgerID;
    private final PersonID personID;
    private final Transactions transactions;
    private ELedgerType ledgerType = ELedgerType.PERSONAL;

    public Ledger(int ledgerID, String ownerID) {
        this.ledgerID = new LedgerID(ledgerID);
        this.personID = new PersonID(new Email(ownerID));
        this.transactions = new Transactions();
    }

    public Ledger(int ledgerID, String ownerID, ELedgerType ledgerType) {
        this.ledgerID = new LedgerID(ledgerID);
        this.personID = new PersonID(new Email(ownerID));
        this.transactions = new Transactions();
        this.ledgerType = ledgerType;
    }

    public ELedgerType getLedgerType() {
        return ledgerType;
    }

    @Override
    public LedgerID getID() {
        return ledgerID;
    }


    public PersonID getOwnerID() {
        return personID;
    }

    @Override
    public boolean isID(LedgerID identity) {
        return ledgerID.equals(identity);
    }


    public void addTransfer(TransactionID id, AccountID accountID, AccountID destinationID, MoneyValue moneyValue, CategoryID categoryID) {
        Transfer transfer = new Transfer(id, accountID, destinationID, moneyValue, categoryID);
        transactions.addTransaction(transfer);
    }

    public void addTransfer(TransactionID id, AccountID accountID, AccountID destinationID, MoneyValue moneyValue, CategoryID categoryID, TransactionDate transactionDate) {
        Transfer transfer = new Transfer(id, accountID, destinationID, moneyValue, categoryID, transactionDate, new Description("n/a"));
        transactions.addTransaction(transfer);
    }

    public void addPayment(TransactionID id, AccountID accountID, MoneyValue moneyValue, CategoryID categoryID) {
        Payment payment = new Payment(id, accountID, moneyValue, categoryID);
        transactions.addTransaction(payment);
    }

    public void addPayment(TransactionID id, AccountID accountID, MoneyValue moneyValue, CategoryID categoryID, TransactionDate transactionDate) {
        Payment payment = new Payment(id, accountID, moneyValue, categoryID, new Description("n/a"), transactionDate);
        transactions.addTransaction(payment);
    }

    public List<Transactionable> getTransactions() {
        return Collections.unmodifiableList(transactions.transactionList);
    }

    public int getTransactionCount() {
        return transactions.size();
    }

    public List<Transactionable> getTransactionsBetweenDates(List<Transactionable> transactions, TransactionDate startDate, TransactionDate endDate) {
        List<Transactionable> transactionsBetweenDates = new ArrayList<>();
        for (Transactionable transaction : transactions) {
            if (transaction.getTransactionDate().isBetween(startDate, endDate)) {
                transactionsBetweenDates.add(transaction);
            }
        }
        return transactionsBetweenDates;
    }

    public List<Movement> getMovements(List<Transactionable> transactions) {
        List<Movement> movementsBetweenDates = new ArrayList<>();
        for (Transactionable transaction : transactions) {
            if (transaction.isPayment()) {
                Payment payment = (Payment) transaction;
                movementsBetweenDates.add(payment.getMovementDebit());
            } else {
                Transfer transfer = (Transfer) transaction;
                movementsBetweenDates.add(transfer.getMovementDebit());
                movementsBetweenDates.add(transfer.getMovementCredit());
            }
        }
        return movementsBetweenDates;
    }

    public List<Movement> getMovementsByAccountID(List<Movement> movements, AccountID accountID) {
        List<Movement> movementsByAccountID = new ArrayList<>();
        for (Movement movement : movements) {
            if (movement.getAccountId().equals(accountID)) {
                movementsByAccountID.add(movement);
            }
        }
        return movementsByAccountID;
    }

    public List<Movement> getAccountMovementsBetweenDates(AccountID accountID, TransactionDate startDate, TransactionDate endDate) {
        List<Transactionable> transactionsBetweenDates = getTransactionsBetweenDates(getTransactions(), startDate, endDate);
        List<Movement> movementsBetweenDates = getMovements(transactionsBetweenDates);
        return getMovementsByAccountID(movementsBetweenDates, accountID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ledger)) return false;
        Ledger ledger = (Ledger) o;
        return Objects.equals(ledgerID, ledger.ledgerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ledgerID);
    }

    private class Transactions {
        private final List<Transactionable> transactionList;

        protected Transactions() {
            this.transactionList = new ArrayList<>();
        }

        public void addTransaction(Transactionable transaction) {
            this.transactionList.add(transaction);
        }

        public int size() {
            return transactionList.size();
        }


    }

    public Transactionable findTransactionByID(TransactionID transactionID) {
        for (Transactionable transaction : getTransactions()) {
            if (transaction.getID().toString().equals(transactionID.toString())) {
                return transaction;
            }
        }
        throw new NullPointerException("No transaction with that ID.");
    }

    public List<Transactionable> findPossibleTransactionsByMovement(AccountID accountID, MoneyValue amount) {
        List<Transactionable> possibleTransactions= new ArrayList<>();
        for (Transactionable transaction : getTransactions()) {
            if (transaction.getMovementDebit().getAccountId().toString().equals(accountID.toString()) &&
                    transaction.getMovementDebit().getValue().equals(amount)) {
                possibleTransactions.add(transaction);
            }
            else if (transaction.isTransfer()) {
                Transfer transfer = (Transfer) transaction;
                if (transfer.getMovementCredit().getAccountId().toString().equals(accountID.toString()) &&
                        transfer.getMovementCredit().getValue().equals(amount)) {
                    possibleTransactions.add(transaction);
                }
            }
        }
        if (possibleTransactions.isEmpty()) {
            throw new NullPointerException("No transaction that includes that movement.");
        }
        return possibleTransactions;
    }
}

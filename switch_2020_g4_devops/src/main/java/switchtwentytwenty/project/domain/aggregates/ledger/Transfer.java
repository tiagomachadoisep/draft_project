package switchtwentytwenty.project.domain.aggregates.ledger;

import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import java.time.LocalDate;
import java.util.Objects;

public class Transfer implements Transactionable {

    private final TransactionID transactionID;
    private Description description;
    private final CategoryID categoryId;
    private final Movement debit;
    private final Movement credit;
    private TransactionDate transactionDate = new TransactionDate(LocalDate.now().toString());

    public Transfer(TransactionID transactionID, AccountID debitAccount, AccountID creditAccount,
                    MoneyValue value, CategoryID categoryId) {
        this.transactionID = transactionID;
        this.categoryId = categoryId;
        this.debit = new Movement(debitAccount, value.toNegative());
        this.credit = new Movement(creditAccount, value);
    }

    public Transfer(TransactionID transactionID, AccountID id,AccountID destinationId,MoneyValue amount,
                    CategoryID categoryId, TransactionDate transactionDate, Description description){
        this.transactionID=transactionID;
        this.categoryId=categoryId;
        this.debit=new Movement(id,amount.toNegative());
        this.credit =new Movement(destinationId,amount);
        this.transactionDate = transactionDate;
        this.description=description;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transfer)) return false;
        Transfer transfer = (Transfer) o;
        return transactionID.equals(transfer.transactionID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionID);
    }


    @Override
    public TransactionID getID() {
        return transactionID;
    }

    @Override
    public boolean isID(TransactionID identity) {
        return transactionID.equals(identity);
    }

    @Override
    public Description getDescription() {
        return description;
    }

    @Override
    public CategoryID getCategoryId() {
        return categoryId;
    }

    @Override
    public ETransactionType getTransactionType() {
        return ETransactionType.TRANSFER;
    }

    @Override
    public AccountID getAccountID() {
        return debit.getAccountId();
    }

    @Override
    public double getAmount() {
        return credit.getValue().getValue();
    }

    @Override
    public AccountID getDestinationId() {
        return credit.getAccountId();
    }

    @Override
    public TransactionDate getTransactionDate() {
        return transactionDate;
    }

    @Override
    public Movement getMovementDebit() {
        return debit;
    }

    public Movement getMovementCredit() {
        return credit;
    }

    @Override
    public boolean isTransfer() {
        return  true;
    }

    @Override
    public boolean isPayment() {
        return false;
    }

}

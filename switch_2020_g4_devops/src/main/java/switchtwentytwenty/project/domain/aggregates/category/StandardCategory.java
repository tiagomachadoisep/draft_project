package switchtwentytwenty.project.domain.aggregates.category;

import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

public class StandardCategory extends BasicCategory {

    /**
     * Protected constructor, only accessible to Builder class.
     */
    protected StandardCategory() {
        //it's empty because builder is setting the custom category's attributes
    }

    /**
     * Method that checks if a category is standard
     *
     * @return boolean
     */
    public boolean isStandard() {
        return true;
    }

    /**
     * Method that checks if a category can be used by a family
     *
     * @param familyId FamilyID
     * @return boolean
     */
    @Override
    public boolean canBeUsedByFamily(FamilyID familyId) {
        return true;
    }

    @Override
    public FamilyID getFamilyId() {
        return null;
    }

    /**
     * Builder class for a standard category;
     */
    public static class Builder {
        private CategoryID id;
        private CategoryName name;
        private CategoryID parentId;

        /**
         * Builder constructor for a standard category, including the id.
         *
         * @param id CategoryID
         */
        public Builder(CategoryID id) {
            this.id = id;

        }

        /**
         * Sets the standard category name.
         *
         * @param name CategoryName
         * @return StandardCategory.Builder
         */
        public Builder setName(CategoryName name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the standard category parent category id.
         *
         * @param parent CategoryID
         * @return StandardCategory.Builder
         */
        public Builder setParent(CategoryID parent) {
            this.parentId = parent;
            return this;
        }

        /**
         * Builds the standard category.
         *
         * @return StandardCategory
         */
        public StandardCategory build() {
            StandardCategory category = new StandardCategory();
            category.id = this.id;
            category.name = this.name;
            category.parentId = this.parentId;

            return category;
        }
    }


}

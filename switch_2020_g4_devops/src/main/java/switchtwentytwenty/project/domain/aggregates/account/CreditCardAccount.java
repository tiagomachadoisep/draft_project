package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

public class CreditCardAccount extends ElectronicAccount {

    private CreditCardAccount() {
    }

    /**
     * Builder class for a credit card account.
     */
    public static class Builder implements AccountBuilder {

        private final AccountID accountID;
        private PersonID personID;
        private EAccountType accountType;
        private Description description;
        private MoneyValue balance;

        /**
         * Builder initializer for a credit card account with account id.
         *
         * @param accountID AccountID
         */
        public Builder(AccountID accountID) {
            this.accountID = accountID;
        }


        /**
         * Sets the account type for a credit card account.
         *
         * @return Builder
         */
        public Builder setAccountType() {
            this.accountType = EAccountType.CREDIT_CARD;
            return this;
        }

        /**
         * Sets the description for a credit card account.
         *
         * @param description Description
         * @return Builder
         */
        public Builder setDescription(Description description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the balance for a credit card account.
         *
         * @param balance MoneyValue
         * @return Builder
         */
        public Builder setBalance(MoneyValue balance) {
            this.balance = balance;
            return this;
        }

        /**
         * Sets the owner ID for a credit card account
         *
         * @param ownerID ID
         * @return Builder
         */
        public Builder setOwnerID(ID ownerID) {
            this.personID = (PersonID) ownerID;
            return this;
        }

        /**
         * Builds a credit card account.
         *
         * @return CreditCardAccount
         */
        public CreditCardAccount build() {
            CreditCardAccount creditCardAccount = new CreditCardAccount();
            creditCardAccount.accountId = this.accountID;
            creditCardAccount.accountType = this.accountType;
            creditCardAccount.description = this.description;
            creditCardAccount.balance = this.balance;
            creditCardAccount.personId = this.personID;

            return creditCardAccount;
        }

    }


}

package switchtwentytwenty.project.domain.aggregates.ledger;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.ETransactionType;
import switchtwentytwenty.project.domain.shared.Movement;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.dddtypes.Entity;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

public interface Transactionable extends Entity<TransactionID> {

    Description getDescription();
    CategoryID getCategoryId();
    ETransactionType getTransactionType();

    AccountID getAccountID();

    double getAmount();

    AccountID getDestinationId();

    TransactionDate getTransactionDate();

    Movement getMovementDebit();

    boolean isTransfer();

    boolean isPayment();
}

package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;


public class BankSavingsAccount extends ElectronicAccount {

    private BankSavingsAccount() {
    }

    /**
     * Builder class for a bank savings account.
     */
    public static class Builder implements AccountBuilder {
        private PersonID personID;
        private final AccountID accountID;
        private EAccountType accountType;
        private Description description;
        private MoneyValue balance;

        /**
         * Builder initializer for a bank savings account with account id.
         *
         * @param accountID AccountID
         */
        public Builder(AccountID accountID) {
            this.accountID = accountID;
        }


        /**
         * Sets the owner ID for a bank savings account
         *
         * @param ownerID ID
         * @return Builder
         */
        public Builder setOwnerID(ID ownerID) {
            this.personID = (PersonID) ownerID;
            return this;
        }

        /**
         * Sets the description for a bank savings account.
         *
         * @param description Description
         * @return Builder
         */
        public Builder setDescription(Description description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the balance for a bank savings account.
         *
         * @param balance MoneyValue
         * @return Builder
         */
        public Builder setBalance(MoneyValue balance) {
            this.balance = balance;
            return this;
        }

        /**
         * Sets the account type for a bank savings account.
         *
         * @return Builder
         */
        public Builder setAccountType() {
            this.accountType = EAccountType.BANK_SAVINGS;
            return this;
        }


        /**
         * Builds a bank savings account.
         *
         * @return BankSavingsAccount
         */
        public BankSavingsAccount build() {
            BankSavingsAccount bankSavingsAccount = new BankSavingsAccount();
            bankSavingsAccount.accountId = this.accountID;
            bankSavingsAccount.accountType = this.accountType;
            bankSavingsAccount.description = this.description;
            bankSavingsAccount.balance = this.balance;
            bankSavingsAccount.personId = this.personID;

            return bankSavingsAccount;
        }
    }
}

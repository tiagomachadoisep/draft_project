package switchtwentytwenty.project.domain.aggregates.category;

import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

public class CustomCategory extends BasicCategory {

    private FamilyID familyId;

    /**
     * Builder class for a custom category;
     */
    public static class Builder {
        private CategoryID id;
        private CategoryName name;
        private CategoryID parentId;
        private FamilyID familyId;

        /**
         * Builder constructor for a custom category.
         *
         * @param id CategoryID
         */
        public Builder(CategoryID id) {
            this.id = id;
        }

        /**
         * Sets the custom category name.
         *
         * @param name CategoryName
         * @return CustomCategory.Builder
         */
        public CustomCategory.Builder setName(CategoryName name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the custom category parent category id.
         *
         * @param parent CategoryID
         * @return CustomCategory.Builder
         */
        public CustomCategory.Builder setParent(CategoryID parent) {
            this.parentId = parent;
            return this;
        }

        /**
         * Sets the custom category family id.
         *
         * @param familyId FamilyID
         * @return CustomCategory.Builder
         */
        public CustomCategory.Builder setFamily(FamilyID familyId) {
            this.familyId = familyId;
            return this;
        }

        /**
         * Builds the customer category.
         *
         * @return CustomCategory
         */
        public CustomCategory build() {
            CustomCategory category = new CustomCategory();
            category.id = this.id;
            category.name = this.name;
            category.parentId = this.parentId;
            category.familyId = this.familyId;
            return category;
        }
    }

    /**
     * Protected constructor, only accessible to Builder class.
     */
    protected CustomCategory() {
        //it's empty because builder is setting the custom category's attributes
    }

    /**
     * Method that checks if a category is standard
     *
     * @return boolean
     */
    @Override
    public boolean isStandard() {
        return false;
    }

    /**
     * Method that checks if a category can be used by a family
     *
     * @param familyId FamilyID
     * @return boolean
     */
    @Override
    public boolean canBeUsedByFamily(FamilyID familyId) {
        return this.familyId.equals(familyId);
    }

    /**
     * Returns the id of the family selected
     *
     * @return family id.
     */
    @Override
    public FamilyID getFamilyId() {
        return this.familyId;
    }

}


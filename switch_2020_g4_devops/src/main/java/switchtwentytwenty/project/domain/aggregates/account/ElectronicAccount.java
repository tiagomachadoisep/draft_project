package switchtwentytwenty.project.domain.aggregates.account;

import lombok.Getter;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.Objects;

@Getter
public abstract class ElectronicAccount implements Account {

    protected EAccountType accountType;
    protected Description description;
    protected PersonID personId;
    protected AccountID accountId;
    protected MoneyValue balance;

    @Override
    public EAccountType getAccountType() {
        return accountType;
    }

    @Override
    public boolean isCashAccount() {
        return false;
    }

    @Override
    public boolean isAccountOwner(ID email) {
        return this.personId.equals(email);
    }

    @Override
    public AccountID getID() {
        if (accountId == null) {
            throw new IllegalArgumentException("AccountID is null");
        }
        return accountId;
    }

    @Override
    public boolean isID(AccountID identity) {
        return identity.equals(accountId);
    }

    @Override
    public PersonID getOwnerID() {
        return personId;
    }

    @Override
    public void removeFunds(MoneyValue amount) {
    }

    @Override
    public void addFunds(MoneyValue amount) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElectronicAccount that = (ElectronicAccount) o;
        return accountId.equals(that.accountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId);
    }
}

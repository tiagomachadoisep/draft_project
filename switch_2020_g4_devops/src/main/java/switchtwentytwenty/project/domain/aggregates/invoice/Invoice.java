package switchtwentytwenty.project.domain.aggregates.invoice;

import lombok.AllArgsConstructor;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.VATNumber;
import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.dddtypes.Entity;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.InvoiceID;

@AllArgsConstructor
public class Invoice implements Entity<InvoiceID>, AggregateRoot<InvoiceID> {

    private final InvoiceID invoiceID;
    private final Description invoiceDescription;
    private final VATNumber sellerVat;
    private final CategoryID categoryId;


    @Override
    public InvoiceID getID() {
        return invoiceID;
    }

    @Override
    public boolean isID(InvoiceID identity) {
        return invoiceID.getId() == identity.getId();
    }
}

package switchtwentytwenty.project.domain.aggregates.family;


import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import java.time.LocalDate;
import java.util.*;

public class Family implements AggregateRoot<FamilyID> {

    private PersonID adminMainEmail;
    private RegistrationDate registrationDate;
    private FamilyID familyID;
    private FamilyName familyName;
    private FamilyRelationships membersRelationships;

    protected Family() {
    }

    public String getName() {
        return familyName.toString();
    }

    public PersonID getAdminMainEmail() {
        return adminMainEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Family family = (Family) o;
        return this.familyID.equals(family.familyID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registrationDate, familyName, familyID);
    }

    /**
     * This method redirects the creation and addition of a familyRelationship to the list of relationships with the
     * given values passed as argument.
     *
     * @param emailPersonOne Email object of the first family member passed as argument.
     * @param emailPersonTwo Email object of the second family member passed as argument.
     * @param relationType   String of the relation designation.
     * @param relationshipID id of relation
     *
     */
    public void createRelationship(PersonID emailPersonOne, PersonID emailPersonTwo,
                                   FamilyRelationType relationType, FamilyRelationshipID relationshipID) {
        membersRelationships.createRelationship(emailPersonOne, emailPersonTwo, relationType, relationshipID);
    }

    /**
     * Finds the relationship between two members by its id.
     *
     * @param familyRelationshipID id
     * @return Optional of the relationship or empty if the relation does not exist.
     */
    public Optional<FamilyRelationship> findRelationshipById(FamilyRelationshipID familyRelationshipID) {
        return membersRelationships.findById(familyRelationshipID);
    }

    /**
     * Returns the registration date of the family
     *
     * @return registration date
     */
    public RegistrationDate getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Returns family Id
     *
     * @return family id
     */
    public FamilyID getID() {
        return familyID;
    }

    /**
     * Checks if it is the id of the family
     *
     * @param identity Family Id
     * @return true if it is and false if not.
     */
    public boolean isID(FamilyID identity) {
        return identity.equals(familyID);
    }

    /**
     * If the relationship exists, it changes it's relation type.
     *
     * @param relationshipID ID
     * @param newRelation    new relation type.
     */
    public void changeRelationship(FamilyRelationshipID relationshipID, FamilyRelationType newRelation) {
        if (!membersRelationships.findAndChangeRelationship(relationshipID, newRelation)) {
            throw new NoSuchElementException("Relationship not found");
        }
    }

    /**
     * Checks if a relationship between two members is a parent type.
     *
     * @param parentId PersonOne
     * @param childId  PersonTwo
     * @return true if it is and false if not.
     */
    public boolean isChild(PersonID parentId, PersonID childId) {
        return this.membersRelationships.isChild(parentId, childId);
    }

    public List<FamilyRelationship> getFamilyRelationships() {
        return membersRelationships.getRelationships();
    }


    public static class Builder {

        private RegistrationDate registrationDate = new RegistrationDate(LocalDate.now().toString());
        private final FamilyRelationships membersRelationships = new FamilyRelationships();
        private PersonID adminMainEmail;
        private final FamilyID familyID;
        private FamilyName familyName;

        public Builder(FamilyID familyID) {
            this.familyID = familyID;
        }

        public Builder setName(FamilyName name) {
            this.familyName = name;
            return this;
        }

        public Builder setAdminId(PersonID adminId) {
            this.adminMainEmail = adminId;
            return this;
        }

        public Builder setRegistrationDate(RegistrationDate registrationDate) {
            this.registrationDate = registrationDate;
            return this;
        }

        public Family build() {
            Family family = new Family();
            family.familyID = this.familyID;
            family.familyName = this.familyName;
            family.adminMainEmail = this.adminMainEmail;
            family.membersRelationships = this.membersRelationships;
            family.registrationDate = this.registrationDate;

            return family;
        }


    }

    /**
     * This class holds familyRelationship objects inside a List.
     */
    private static class FamilyRelationships {

        private final List<FamilyRelationship> relationships;

        /**
         * Constructor for FamilyRelationships.
         */
        public FamilyRelationships() {

            relationships = new ArrayList<>();
        }

        public List<FamilyRelationship> getRelationships() {
            return new ArrayList<>(relationships);
        }


        /**
         * This method creates a familyRelationship object with the values passed as argument. It also calls another
         * method {@link FamilyRelationships#addRelationship(FamilyRelationship)} that will add the created
         * familyRelationship to the list of relationships.
         *
         * @param personOneID  PersonID object of the first family member passed as argument.
         * @param personTwoID  PersonID object of the second family member passed as argument.
         * @param relationType String of the relation designation.
         */
        public void createRelationship(PersonID personOneID, PersonID personTwoID,
                                       FamilyRelationType relationType, FamilyRelationshipID relationshipID) {

            if (personOneID == null || personTwoID == null || relationType == null || relationshipID == null) {
                throw new NullArgumentException("The passed argument(s) cannot be null.");
            }

            FamilyRelationship familyRelationship = new FamilyRelationship(personOneID, personTwoID, relationType, relationshipID);

            addRelationship(familyRelationship);

        }

        /**
         * This method adds an object familyRelationship passed as argument to the list of relationships if it does not
         * exist already a relationship between two members.
         *
         * @param familyRelationship FamilyRelationship object passed as argument.
         */
        private void addRelationship(FamilyRelationship familyRelationship) {

            for (FamilyRelationship relationshipIterated : relationships) {
                if (familyRelationship.equals(relationshipIterated)) {
                    throw new DuplicatedValueException("A relationship between the members already exists.");
                }
            }

            relationships.add(familyRelationship);
        }

        /**
         * Searches and changes a relationship.
         *
         * @param relationshipID id
         * @param newRelation    relation type
         * @return true if the relationship is changed and false if not.
         */
        public boolean findAndChangeRelationship(FamilyRelationshipID relationshipID, FamilyRelationType newRelation) {
            boolean relationTypeChanged = false;

            for (int relationIndex = 0; relationIndex < this.relationships.size() && !relationTypeChanged; relationIndex++) {
                FamilyRelationship relationshipIterated = this.relationships.get(relationIndex);

                if (relationshipIterated.isID(relationshipID)) {

                    relationshipIterated.setRelationType(newRelation);
                    relationTypeChanged = true;
                }
            }

            return relationTypeChanged;
        }

        /**
         * Finds the relation by id
         *
         * @param familyRelationshipID id
         * @return Optional of the relationship if it exists and false if not.
         */
        public Optional<FamilyRelationship> findById(FamilyRelationshipID familyRelationshipID) {

            if (familyRelationshipID == null) {
                throw new NullArgumentException("The familyRelationshipID cannot be null.");
            }

            for (FamilyRelationship relationshipIterated : this.relationships) {
                if (relationshipIterated.isID(familyRelationshipID)) {
                    return Optional.of(relationshipIterated);
                }
            }

            return Optional.empty();

        }

        /**
         * Find the relationship between those persons.
         *
         * @param person    PersonOne from the relationship
         * @param personTwo PersonTwo that completes the relationship
         * @return FamilyRelationship
         */
        private FamilyRelationship findRelationshipByMembers(PersonID person, PersonID personTwo) {
            for (FamilyRelationship relation :
                    this.relationships) {
                if (relation.areTheSamePersons(person, personTwo)) {
                    return relation;
                }
            }
            throw new NoSuchElementException("The relationship does not exist");
        }

        /**
         * Checks if child (personTwo) is actually child from parent (personOne)
         *
         * @param parent PersonOne
         * @param child  PersonTwo
         * @return true if is child and false if not.
         */
        public boolean isChild(PersonID parent, PersonID child) {
            FamilyRelationship relation = findRelationshipByMembers(parent, child);
            return relation.isParent();

        }

    }


}

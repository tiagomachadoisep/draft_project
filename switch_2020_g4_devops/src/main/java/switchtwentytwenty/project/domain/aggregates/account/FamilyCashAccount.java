package switchtwentytwenty.project.domain.aggregates.account;

import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

public class FamilyCashAccount extends CashAccount {


    /**
     * Empty family cash account constructor so as to enable the builder
     */
    private FamilyCashAccount() {
    }

    /**
     * Builder class for a family cash account.
     */
    public static class Builder implements AccountBuilder {

        private final AccountID accountIdentifier;
        private EAccountType accountType;
        private MoneyValue balance;
        private Description description;
        private PersonID personID;

        /**
         * Builder initializer for a family cash account.
         *
         * @param accountID account id
         */
        public Builder(AccountID accountID) {
            this.accountIdentifier = accountID;
        }

        /**
         * Sets the account type for a family cash account.
         *
         * @return Builder
         */
        public Builder setAccountType() {
            this.accountType = EAccountType.FAMILY_CASH;
            return this;
        }

        /**
         * Sets the description for a family cash account.
         *
         * @param description Description
         * @return Builder
         */
        public Builder setDescription(Description description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the balance for a family cash account.
         *
         * @param balance money value
         * @return Builder
         */
        public Builder setBalance(MoneyValue balance) {
            this.balance = balance;
            return this;
        }

        /**
         * Sets the owner id for a family cash account.
         *
         * @param ownerID owner id
         * @return Builder
         */
        public Builder setOwnerID(ID ownerID) {
            this.personID = (PersonID) ownerID;
            return this;
        }

        /**
         * Builds a family cash account.
         *
         * @return Family cash account
         */
        public FamilyCashAccount build() {
            FamilyCashAccount familyCashAccount = new FamilyCashAccount();

            familyCashAccount.accountId = this.accountIdentifier;
            familyCashAccount.accountType = this.accountType;
            familyCashAccount.balance = this.balance;
            familyCashAccount.description = this.description;
            familyCashAccount.ownerID = this.personID;

            return familyCashAccount;
        }
    }
}

package switchtwentytwenty.project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.controllers.icontrollers.*;
import switchtwentytwenty.project.controllers.implcontrollers.AddEmailController;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.controllers.implcontrollers.GetAccountMovementsBetweenDatesController;
import switchtwentytwenty.project.controllers.implcontrollers.TransferCashBetweenMembersController;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.dto.*;
import switchtwentytwenty.project.persistence.data.*;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ILedgerRepositoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import java.time.LocalDate;
import java.util.UUID;


@SpringBootApplication
public class MainApplication implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(MainApplication.class);
    private static final String EMAIL_ZE = "ze@isep.pt";
    private static final String EMAIL_CARLOS = "carlos@isep.pt";
    private static final String EMAIL_ANA = "ana@isep.pt";
    private static final String EMAIL_DANIEL = "daniel@isep.pt";
    private static final String FAMILYONE_ADDRESS = "Rua de Cima";
    private static final String FAMILYTWO_ADDRESS = "Rua de Baixo";

    @Autowired
    private IPersonRepositoryJPA iPersonRepositoryJPA;
    @Autowired
    private ILedgerRepositoryJPA iLedgerRepositoryJPA;
    @Autowired
    private ICreatePersonalCashAccountController createPersonalCashAccountController;
    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private ICategoryRepositoryJPA categoryRepoJpa;
    @Autowired
    private IAddCustomCategoryController customCategoryController;

    @Autowired
    private AddFamilyController addFamilyController;

    @Autowired
    private IRegisterPaymentUsingCashController registerPaymentUsingCashController;
    @Autowired
    private ITransferFromFamilyCashToMemberCashController transferFromFamilyCashToMemberCashController;
    @Autowired
    private TransferCashBetweenMembersController transferCashBetweenMembersController;
    @Autowired
    private ICreateFamilyCashAccountController createFamilyCashAccountController;
    @Autowired
    private IAccountService iAccountService;
    @Autowired
    private IFamilyRepository familyRepository;
    @Autowired
    private IAccountRepositoryJPA iAccountRepositoryJPA;
    @Autowired
    private GetAccountMovementsBetweenDatesController getAccountMovementsBetweenDatesController;
    @Autowired
    private AddEmailController addEmailController;

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    private void bootstrapping() {


        Family zeFamily = new Family.Builder(new FamilyID("1"))
                .setName(new FamilyName("Martins"))
                .setAdminId(new PersonID(new Email(EMAIL_ZE))).build();

        iPersonRepositoryJPA.save(new PersonJPA(EMAIL_ZE, "1", "Ze", 218471742, FAMILYONE_ADDRESS, "01-03-1970",
                ERole.ADMIN));
        iPersonRepositoryJPA.save(new PersonJPA(EMAIL_ANA, "1", "Ana", 297243314, FAMILYONE_ADDRESS, "01-03-1974"));
        iPersonRepositoryJPA.save(new PersonJPA(EMAIL_DANIEL, "1", "Daniel", 297243314, FAMILYONE_ADDRESS, "01-08" +
                "-1990"));
        iPersonRepositoryJPA.save(new PersonJPA("sysAdmin@isep.pt", "admin", "System Manager", 219188017
                , "Address", "15-07" +
                "-1968", ERole.SYSADMIN));

        PersonJPA personJPA = new PersonJPA(EMAIL_CARLOS, "1", "Carlos", 252231716, FAMILYONE_ADDRESS, "21-07-1993");
        iPersonRepositoryJPA.save(personJPA);
        personJPA.setName("Carlitos");

        personJPA.addEmail("carlosA@isep.pt");
        personJPA.addEmail("carlosB@isep.pt");
        personJPA.addEmail("carlosC@isep.pt");
        personJPA.addEmail("carlosD@isep.pt");

        iPersonRepositoryJPA.save(personJPA);



        zeFamily.createRelationship(new PersonID(new Email(EMAIL_ZE)),
                new PersonID(new Email(EMAIL_CARLOS)),
                new FamilyRelationType("father"),
                new FamilyRelationshipID());

        zeFamily.createRelationship(new PersonID(new Email(EMAIL_ZE)),
                new PersonID(new Email(EMAIL_DANIEL)),
                new FamilyRelationType("uncle"),
                new FamilyRelationshipID());

        familyRepository.saveNew(zeFamily);

        //Family 2
        String luisMail = "luis@isep.pt";
        Family rodriguesFamily = new Family.Builder((new FamilyID("2")))
                .setName(new FamilyName("Rodrigues")).setAdminId(new PersonID(new Email(luisMail))).build();


        iPersonRepositoryJPA.save(new PersonJPA("margarida@isep.pt", "2", "Margarida", 297243314, FAMILYTWO_ADDRESS,
                "30-06-1950"));
        PersonJPA luisJPA = new PersonJPA(luisMail, "2"
                , "Luis", 202433919, FAMILYTWO_ADDRESS, "14-11-1956",ERole.ADMIN);
        iPersonRepositoryJPA.save(luisJPA);
        luisJPA.addPhoneNumber(new PhoneNumberJPA("911111111"));
        luisJPA.setBirthDate("01-01-1986");
        iPersonRepositoryJPA.save(new PersonJPA("martim@isep.ipp.pt", "2", "Martim", 268959145, FAMILYTWO_ADDRESS,
                "08-03-1985"));
        iPersonRepositoryJPA.save(new PersonJPA("marta@isep.pt", "2", "Marta", 284430048, FAMILYTWO_ADDRESS, "21-03" +
                "-2016"));

        rodriguesFamily.createRelationship(new PersonID(new Email(luisMail)),
                new PersonID(new Email("margarida@isep.pt")),
                new FamilyRelationType("son"),
                new FamilyRelationshipID());

        familyRepository.saveNew(rodriguesFamily);

        addFamilyController.addFamily(new FamilyDTO("Silva","jjs@isep.pt","Jose",145644707,"12-01-1987"));

        ///////////////////////////

        LedgerJPA ledgerZeJPA = new LedgerJPA(1, EMAIL_ZE);
        ledgerZeJPA.addPayment("1", "1", 10, "1", LocalDate.of(2020, 1, 11));
        ledgerZeJPA.addPayment("2", "1", 15, "2", LocalDate.of(2020, 1, 11));
        ledgerZeJPA.addPayment("4", "2", 1000, "7", LocalDate.of(2020, 1, 11));
        iLedgerRepositoryJPA.save(ledgerZeJPA);
        log.info("ledger ze created");
        LedgerJPA ledgerZeJPANew = new LedgerJPA(new LedgerID(1), ledgerZeJPA.getPersonID(),
                ledgerZeJPA.getTransactionJPAList());
        ledgerZeJPANew.addPayment("7", "1", 12, "1", LocalDate.of(2020, 1, 1));
        iLedgerRepositoryJPA.save(ledgerZeJPANew);
        ledgerZeJPA.addPayment("9", "1", 20, "1", LocalDate.MIN);
        iLedgerRepositoryJPA.save(ledgerZeJPA);
        if (iLedgerRepositoryJPA.findById(new LedgerID(1)).isPresent()) {
            LedgerJPA ledgerJPA = iLedgerRepositoryJPA.findById(new LedgerID(1)).orElse(new LedgerJPA());
            log.info("ledger ze found");
            ledgerJPA.addPayment("6", "1", 23, "1", LocalDate.of(2021, 3, 1));
            iLedgerRepositoryJPA.save(ledgerJPA);
            log.info("ledger ze updated");
        }

        LedgerJPA ledgerCarlosJPA = new LedgerJPA(3, EMAIL_CARLOS);
        iLedgerRepositoryJPA.save(ledgerCarlosJPA);

        LedgerJPA ledgerAnaJPA = new LedgerJPA(2, EMAIL_ANA);
        ledgerAnaJPA.addPayment("3", "4", 2, "2", LocalDate.now());
        ledgerAnaJPA.addPayment("5", "12", 50, "2", LocalDate.of(2020, 12, 31));
        ledgerAnaJPA.addPayment("8", "4", 79, "5", LocalDate.of(2020, 1, 8));
        ledgerAnaJPA.addPayment("9", "4", 32, "5", LocalDate.of(2018, 9, 1));
        iLedgerRepositoryJPA.save(ledgerAnaJPA);


        AccountJPA accountJPAOne = new AccountJPA(new AccountID(UUID.randomUUID().toString()),
                new PersonID(new Email("howl@mc.sg")), "CC Howl", 150, EAccountType.CREDIT_CARD);
        iAccountRepositoryJPA.save(accountJPAOne);
        AccountJPA accountJPATwo = new AccountJPA(new AccountID(UUID.randomUUID().toString()),
                new PersonID(new Email("sophie@mc.sg")), "CC Sophie", 200, EAccountType.CREDIT_CARD);
        iAccountRepositoryJPA.save(accountJPATwo);
        AccountJPA accountJPAThree = new AccountJPA(new AccountID(UUID.randomUUID().toString()),
                new PersonID(new Email("calcifer@mc.sg")), "CC Calcifer", 1000, EAccountType.CREDIT_CARD);
        iAccountRepositoryJPA.save(accountJPAThree);
        AccountJPA accountJPAFour = new AccountJPA(new AccountID(UUID.randomUUID().toString()),
                new PersonID(new Email("howl@mc.sg")), "PC Howl", 350, EAccountType.PERSONAL_CASH);
        iAccountRepositoryJPA.save(accountJPAFour);

        /////////////////////////////////CATEGORIES//////////////////////////////////////////

        CategoryDTO dto = new CategoryDTO();
        dto.setName("GroupFour Health");
        CategoryDTO outputDto = categoryService.addStandardCategory(dto);

        CategoryDTO standardCategoryDTO = new CategoryDTO();
        standardCategoryDTO.setName("GroupFour Television");
        CategoryDTO output = categoryService.addStandardCategory(standardCategoryDTO);

        CategoryDTO standardSubCategoryDTO = new CategoryDTO();
        standardSubCategoryDTO.setName("GroupFour Gym");
        standardSubCategoryDTO.setParentId(outputDto.getId());
        categoryService.addStandardSubCategory(standardSubCategoryDTO);

        CategoryDTO customCategoryDTO = new CategoryDTO();
        customCategoryDTO.setName("Netflix");
        customCategoryDTO.setParentId(output.getId());
        customCategoryController.addCustomCategory("1", customCategoryDTO);

        CategoryDTO customCategoryDTO2 = new CategoryDTO();
        customCategoryDTO2.setName("HBO");
        customCategoryDTO2.setParentId(output.getId());
        customCategoryController.addCustomCategory("1", customCategoryDTO2);

        CategoryJPA jpa = new CategoryJPA(new CategoryID("1"), "GroupFour Games", null, null);
        categoryRepoJpa.save(jpa);

        /////////////////////////////////ACCOUNTS//////////////////////////////////////////

        iAccountService.findAllAccountsByOwner(new PersonID(new Email("luis@isep.ipp.pt")));

        AccountDTO personalCashAccountDTO = new AccountDTO("cash do ze", 2000, EAccountType.PERSONAL_CASH);
        createPersonalCashAccountController.createPersonalCashAccount(EMAIL_ZE, personalCashAccountDTO);
        registerPaymentUsingCashController.registerPayment(EMAIL_ZE, 100, "1");
        registerPaymentUsingCashController.registerPayment(EMAIL_ZE, 3.5, "1");
        registerPaymentUsingCashController.registerPayment(EMAIL_ZE, 100, "1");

        AccountDTO personalCashAccountDTO2 = new AccountDTO("cash do carlos", 0, EAccountType.PERSONAL_CASH);
        createPersonalCashAccountController.createPersonalCashAccount(EMAIL_CARLOS, personalCashAccountDTO2);

        iLedgerRepositoryJPA.save(new LedgerJPA(10, EMAIL_ZE, ELedgerType.FAMILY));

        AccountDTO inputDTO = new AccountDTO("cash", EMAIL_ZE, 1000, EAccountType.FAMILY_CASH);
        createFamilyCashAccountController.createFamilyCashAccount( EMAIL_ZE, inputDTO);

        iAccountRepositoryJPA.save(new AccountJPA(new AccountID("teste"), new PersonID(new Email(EMAIL_ZE)),
                "cash", 9000, EAccountType.FAMILY_CASH));
        transferFromFamilyCashToMemberCashController.transferCashFromFamilyToMember(EMAIL_ZE, EMAIL_ZE, 17,
                "1", LocalDate.of(2000, 1, 1));

        transferFromFamilyCashToMemberCashController.transferCashFromFamilyToMember(EMAIL_ZE, EMAIL_ZE, 12.5,
                "1", LocalDate.of(2000, 1, 1));

        transferCashBetweenMembersController.transferCash(new TransferDTO(EMAIL_ZE, EMAIL_CARLOS, 35, "1",
                LocalDate.of(1999, 1, 1)));

        getAccountMovementsBetweenDatesController.getAccountMovementsBetweenDates(2, "4", "2021-01-01", "2021-12-31");

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setRelationshipId("test");
        relationshipDTO.setDesignation("father");
        relationshipDTO.setFamilyId("1");
        relationshipDTO.setFirstPersonEmail(EMAIL_ZE);
        relationshipDTO.setSecondPersonEmail(EMAIL_ANA);


    }


    @Override
    public void run(ApplicationArguments args) throws Exception {

      //bootstrapping();

    }
}

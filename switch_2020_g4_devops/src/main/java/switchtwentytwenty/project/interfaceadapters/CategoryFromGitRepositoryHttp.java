package switchtwentytwenty.project.interfaceadapters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class CategoryFromGitRepositoryHttp implements ICategoryRepositoryHttp {

    private static final String API_URL = "https://gist.githubusercontent.com/dagilleland/4191556/raw/1d1517860ad110bde47fbf0b4702e2c426f1932c/fiddle.response.json";

    private static final String ID_HEADER = "HTTPGIST/";

    private RestTemplate restTemplate = (new RestTemplateBuilder()).build();

    @Override
    public List<CategoryDTO> importCategories() {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String response = restTemplate.exchange(API_URL, HttpMethod.GET, entity, String.class).getBody();

        try {
            JSONArray array = new JSONArray(response);
            List<CategoryDTO> list = new ArrayList<>();

            for(int i = 0; i< array.length();i++){
                JSONObject object = array.getJSONObject(i);
                list.addAll(jsonToDtoList(object));
            }

            return list;
        }
        catch (Exception e){return Collections.emptyList();}
    }

    private List<CategoryDTO> jsonToDtoList(JSONObject object){
        String name = "name";
        String categoryId = "categoryId";
        String subcategories = "subCategories";
        String subCategoryId = "subCategoryId";

        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        categoryDTOList.add(new CategoryDTO(object.optString(name), ID_HEADER +object.optString(categoryId),null,null));

        if(object.has(subcategories)){
            JSONArray subCategoriesArray = object.optJSONArray(subcategories);
            int size = subCategoriesArray.length();
            for(int i=0;i< size;i++){
                try {
                    JSONObject subcategory = subCategoriesArray.getJSONObject(i);
                    CategoryDTO dto = new CategoryDTO(subcategory.optString(name), ID_HEADER +subcategory.optString(subCategoryId), ID_HEADER +object.optString(categoryId),null );
                    categoryDTOList.add(dto);
                } catch (JSONException e) {
                    categoryDTOList = Collections.emptyList();
                }
            }
        }
        return categoryDTOList;
    }
}

package switchtwentytwenty.project.interfaceadapters;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CategoriesFromGroupOne implements ICategoryRepositoryHttp {

    private RestTemplate restTemplate = (new RestTemplateBuilder()).build();

    @Override
    public List<CategoryDTO> importCategories() {


        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String apiURL = "http://vs249.dei.isep.ipp.pt:8080/categories/standard/list";
        String response = restTemplate.exchange(apiURL, HttpMethod.GET, entity, String.class).getBody();

        try {
            JSONObject responseObject = new JSONObject(response);
            JSONArray jsonArray = responseObject.getJSONArray("categoryDTOs");
            List<CategoryDTO> categoryDTOList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                categoryDTOList.addAll(jsonToDTOList(object));
            }
            return categoryDTOList;
        } catch (Exception exception) {
            throw new IllegalArgumentException(exception);
        }

    }

    public List<CategoryDTO> jsonToDTOList(JSONObject object) {
        String categoryName = "name";
        String categoryID = "id";
        String parentID = "parentId";

        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        String idHeader = "G1/";
        if(!object.isNull(parentID)) {
            categoryDTOList.add(new CategoryDTO(object.optString(categoryName)
                    , idHeader + object.optInt(categoryID)
                    , idHeader+object.optInt(parentID)
                    , null));
        }
        else{
            categoryDTOList.add(new CategoryDTO(object.optString(categoryName)
                    , idHeader + object.optInt(categoryID)
                    , null
                    , null));
        }

        return categoryDTOList;
    }
}

package switchtwentytwenty.project.interfaceadapters;


import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriesFromGroupThree implements ICategoryRepositoryHttp {

    private RestTemplate restTemplate = (new RestTemplateBuilder()).build();

    @Override
    public List<CategoryDTO> importCategories() {


        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String apiURL = "http://vs116.dei.isep.ipp.pt:8080/categories";
        String response = restTemplate.exchange(apiURL, HttpMethod.GET, entity, String.class).getBody();

        try {
            JSONObject responseObject = new JSONObject(response);
            JSONArray jsonArray = responseObject.getJSONArray("outputCategoryDTOList");
            List<CategoryDTO> categoryDTOList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                categoryDTOList.addAll(jsonToDTOList(object));
            }
            return categoryDTOList;
        } catch (Exception exception) {
           throw new IllegalArgumentException(exception);
        }

    }

    public List<CategoryDTO> jsonToDTOList(JSONObject object) {
        String categoryName = "categoryName";
        String categoryID = "categoryID";
        String parentID = "parentID";
        String familyID = "familyID";

        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        String idHeader = "G3/";
        categoryDTOList.add(new CategoryDTO(object.optString(categoryName)
                , idHeader +object.optString(categoryID)
                , object.optString(parentID)
                , object.optString(familyID)));

        return categoryDTOList;
    }
}

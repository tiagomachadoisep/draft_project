package switchtwentytwenty.project.interfaceadapters;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CategoriesFromGroupTwo implements ICategoryRepositoryHttp {

    private RestTemplate restTemplate = (new RestTemplateBuilder()).build();

    @Override
    public List<CategoryDTO> importCategories() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String apiURL = "http://vs118.dei.isep.ipp.pt:8080/categories/standard";
        String response = restTemplate.exchange(apiURL, HttpMethod.GET, entity, String.class).getBody();

        try {
            JSONArray jsonArray = new JSONArray(response);
            List<CategoryDTO> categoryDTOList = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                categoryDTOList.addAll(jsonToDTOList(object));
            }
            return categoryDTOList;
        } catch (Exception exception) {
            throw new IllegalArgumentException(exception);
        }

    }

    public List<CategoryDTO> jsonToDTOList(JSONObject object) {
        String categoryName = "designation";
        String categoryID = "id";
        String parentID = "parentID";

        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        String idHeader = "G2/";
        if(!object.isNull(parentID)) {
            categoryDTOList.add(new CategoryDTO(object.optString(categoryName)
                    , idHeader + object.optString(categoryID)
                    ,idHeader +  object.optString(parentID), null));
        }
        else{
            categoryDTOList.add(new CategoryDTO(object.optString(categoryName)
                    , idHeader + object.optString(categoryID)
                    , null, null));
        }

        return categoryDTOList;
    }
}

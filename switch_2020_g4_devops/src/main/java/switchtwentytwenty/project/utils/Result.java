package switchtwentytwenty.project.utils;

import java.util.Objects;

public class Result<T> {
    private final boolean status;
    private final T content;

    private Result(boolean status, T content) {
        this.status = status;
        this.content = content;
    }

    public static <T> Result<T> success() {
        return new Result(true, null);
    }

    public static <T> Result<T> success(T content) {
        return new Result(true, content);
    }

    public static <T> Result<T> failure(T content) {
        return new Result(false, content);
    }

    public boolean isSuccess() {
        return status;
    }

    public T getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result<?> result = (Result<?>) o;

        if (status != result.status) return false;
        if (content == null) return result.content == null;
        return content.equals(result.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, content);
    }
}


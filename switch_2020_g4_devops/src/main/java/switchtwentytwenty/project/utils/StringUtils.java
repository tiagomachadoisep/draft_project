package switchtwentytwenty.project.utils;

public class StringUtils {

    private StringUtils() {
    }

    public static String toCamelCase(String s) {
        if (s == null) return null;
        String[] parts = s.split("_");
        String camelCaseString = "";
        for (String part : parts) {
            camelCaseString = camelCaseString + capitalize(part);
        }
        return camelCaseString;
    }

    /**
     * Method that receives a String and changes the first letter to upper case
     * and the other ones to lower case.
     *
     * @param s String
     * @return formatted string
     */
    public static String capitalize(String s) {
        if (s == null) return null;
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }
}

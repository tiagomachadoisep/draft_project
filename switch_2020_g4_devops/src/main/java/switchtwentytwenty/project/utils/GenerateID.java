package switchtwentytwenty.project.utils;


import java.util.UUID;

public interface GenerateID {

    default String generateID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}

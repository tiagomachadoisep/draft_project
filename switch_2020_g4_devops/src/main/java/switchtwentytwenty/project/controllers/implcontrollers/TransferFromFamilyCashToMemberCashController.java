package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.controllers.icontrollers.ITransferFromFamilyCashToMemberCashController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.TransferDTO;

import java.time.LocalDate;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api")
public class TransferFromFamilyCashToMemberCashController implements ITransferFromFamilyCashToMemberCashController {

    @Autowired
    private final ILedgerService ledgerService;
    @Autowired
    private final AssemblerToDTO assemblerToDTO;

    public TransferFromFamilyCashToMemberCashController(ILedgerService ledgerService, AssemblerToDTO assemblerToDTO) {
        this.ledgerService = ledgerService;
        this.assemblerToDTO = assemblerToDTO;
    }

    @Override
    public void transferCashFromFamilyToMember(String  adminEmail, String memberEmail, double amount, String categoryId, LocalDate date) {
        TransferDTO transferDTO = assemblerToDTO.toTransferDTO(adminEmail, memberEmail, amount, categoryId,date);
        ledgerService.transferCashFromFamilyToMember(transferDTO);
    }

    @Override
    @PostMapping(value = "/ledgers/transfers/")
    public ResponseEntity<Object> transferCashFromFamily(@RequestBody TransferDTO transferDTO) {

        TransferDTO transactionDTO = ledgerService.transferCashFromFamilyToMember(transferDTO);

        Link accountLink = linkTo(methodOn(CheckAccountBalanceController.class).checkAccountBalance(transactionDTO.getAccountId()))
                .withRel("Account balance");

        transactionDTO.add(accountLink);

        if(transactionDTO.getBalance()==0)
            return new ResponseEntity<>(transactionDTO.getTransactionID(),HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(transactionDTO, HttpStatus.CREATED);
    }
}

package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAuthenticationService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.controllers.icontrollers.IAddFamilyMemberController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class AddMemberController implements IAddFamilyMemberController {

    @Autowired
    private final IPersonService personService;

    @Autowired
    private final AssemblerToDTO assemblerToDTO;

    @Autowired
    private final ISecurityContextProvider securityContextProvider;

    @Autowired
    private final IAuthenticationService authenticationService;

    @PostMapping(value = "/families/{id}/members")
    public ResponseEntity<Object> addPerson(@RequestBody PersonDTO personDTO, @PathVariable("id") String familyId) {
        personDTO.setFamilyId(familyId);
        try {
            if(!authenticationService.isCurrentUserInFamily(familyId)){
                String responseMessage = "Forbidden - not your family";
                ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto(responseMessage);
                exceptionDTO.add(linkToFamily(personDTO));
                return new ResponseEntity<>(exceptionDTO, HttpStatus.FORBIDDEN);
            }
            personService.addMember(personDTO);

            personDTO.add(linkToFamily(personDTO));
            personDTO.add(linkToAddEmail(personDTO));
            return new ResponseEntity<>(personDTO, HttpStatus.CREATED);
        } catch (Exception exception) {
            String responseMessage = "Invalid parameters - " + exception.getMessage();
            ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto(responseMessage);
            exceptionDTO.add(linkToFamily(personDTO));
            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/families/{id}/members")
    public ResponseEntity<Object> getFamilyMembers(@PathVariable("id") String familyID) {
        List<PersonDTO> membersList = personService.getFamilyMembers(familyID);

        return new ResponseEntity<>(membersList, HttpStatus.OK);
    }

    private Link linkToFamily(PersonDTO personDTO) {
        return linkTo(methodOn(AddFamilyController.class)
                .getFamily(personDTO.getFamilyID())).withRel("Family");
    }

    private Link linkToAddEmail(PersonDTO personDTO) {
        return linkTo((methodOn(AddEmailController.class).showOtherEmails(personDTO.getEmailAddress()))).withRel(
                "Other emails");
    }

    @GetMapping(value = "/members")
    public ResponseEntity<Object> getAllMembers() {
        List<PersonDTO> personDTOList = personService.getAllMembers();

        return new ResponseEntity<>(personDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/members2/{id}")
    public ResponseEntity<Object> getMemberFamilyById(@PathVariable String id) {

        String familyId = personService.getMemberFamilyById(id);
        return new ResponseEntity<>(familyId, HttpStatus.OK);

    }


}

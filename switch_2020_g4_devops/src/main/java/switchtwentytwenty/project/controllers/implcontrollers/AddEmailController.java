package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.controllers.icontrollers.IAddEmailController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.utils.Result;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class AddEmailController implements IAddEmailController {

    @Autowired
    IPersonService personService;
    @Autowired
    AssemblerToDTO assemblerToDTO;

    /**
     * Add other email to profile
     * @param inputDTO- the new email
     * @param mainEmail - person id
     * @return Response Entity
     */
    @PostMapping("/members/{id}/emails")
    public ResponseEntity<Object> addEmailAccountToProfile(@RequestBody EmailDTO inputDTO, @PathVariable("id") String mainEmail) {
        try {
            EmailDTO emailDTO = assemblerToDTO.toEmailDTO(inputDTO.email, mainEmail);
            PersonDTO personDTO = personService.getPersonDTO(mainEmail);
            //link to add more emails - self link
            Link selfLink = linkTo(methodOn(AddEmailController.class).addEmailAccountToProfile((new EmailDTO(emailDTO.email)), emailDTO.mainEmail)).withSelfRel();
            //link to show all emails
            Link showEmailsLink = linkTo(methodOn(AddEmailController.class).showOtherEmails(mainEmail)).withSelfRel();

            personDTO.add(selfLink);
            personDTO.add(showEmailsLink);

            Result<String> output = personService.addEmailToProfile(emailDTO);
            if (output.isSuccess()) {
                EmailDTO outputDTO = assemblerToDTO.toEmailDTO(output.getContent());
                return new ResponseEntity<>(outputDTO, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(output.getContent(), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Invalid parameters.", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method that shows the list of other person's emails
     * @param mainEmail main email
     * @return entity response
     */
    @GetMapping(path = "/members/{id}/emails")
    public ResponseEntity<Object> showOtherEmails(@PathVariable("id") String mainEmail) {
        try {
            List<String> otherEmails =  personService.getOtherEmailsList(mainEmail);
            EmailDTO emailDTO = new EmailDTO(mainEmail);
            emailDTO.setEmailsList(otherEmails);
            return new ResponseEntity<>(emailDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Invalid id",HttpStatus.BAD_REQUEST);
        }
    }

}

package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.controllers.icontrollers.IAddBankSavingsAccountController;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class AddBankSavingsAccountController implements IAddBankSavingsAccountController {
    @Autowired
    private final AssemblerToDTO assemblerToDTO;

    @Autowired
    private final IAccountService iAccountService;

    /**
     * Method that adds a bank savings account to our persistence.
     *
     * @param id    personId
     * @param input data needed to create a bank account
     * @return returns either the bank savings account DTO created or an error if occurred.
     */
    @PostMapping(value = "members/{id}/accounts/savings")
    public ResponseEntity<Object> addBankSavingsAccount(@PathVariable String id, @RequestBody AccountDTO input) {
        AccountDTO accountDTO = assemblerToDTO.inputToAccountDTO(id, input);
        try {
            AccountDTO output = iAccountService.addBankSavingsAccount(accountDTO);
            output.add(linkToAllMembersAccounts(output));
            output.add(linkToNewAccountInfo(output));
            return new ResponseEntity<>(output, HttpStatus.OK);

        } catch (IllegalArgumentException exception) {
            String responseMessage = "Cannot add Bank Savings Account - " + exception.getMessage();
            ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto(responseMessage);
            exceptionDTO.add(linkToAllMembersAccounts(accountDTO));

            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Link that allows to get access to the new account created.
     *
     * @param accountDTO AccountDTO
     * @return final output AccountDTO.
     */
    private Link linkToNewAccountInfo(AccountDTO accountDTO) {
        return linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountByID(accountDTO.getAccountId())).withRel("New Account Info");
    }

    /**
     * Link that allows to see the other accounts associated to the same person.
     *
     * @param accountDTO AccountDTO
     * @return final output AccountDTO.
     */
    private Link linkToAllMembersAccounts(AccountDTO accountDTO) {
        return linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(accountDTO.getOwnerId())).withRel("All Member accounts");
    }
}

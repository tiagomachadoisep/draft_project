package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.controllers.icontrollers.ICreateFamilyRelationshipController;
import switchtwentytwenty.project.controllers.icontrollers.LinkAddable;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.utils.Result;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class CreateFamilyRelationshipController implements ICreateFamilyRelationshipController, LinkAddable {

    @Autowired
    private final IFamilyRelationshipService familyRelationshipService;
    @Autowired
    private AssemblerToDTO assemblerToDTO;

    @Autowired
    public CreateFamilyRelationshipController(IFamilyRelationshipService familyRelationshipService) {
        this.familyRelationshipService = familyRelationshipService;
    }


    @PostMapping(value = "/families/{id}/relations/")
    public ResponseEntity<Object> createRelationship(@RequestBody RelationshipDTO relationshipDTO,
                                                     @PathVariable("id") String familyId) {

        RelationshipDTO newRelationshipDTO = assemblerToDTO.toRelationshipDto(relationshipDTO.getFirstPersonEmail(),
                relationshipDTO.getSecondPersonEmail(), familyId, relationshipDTO.getDesignation());

        Result result = familyRelationshipService.createAndSaveRelationship(newRelationshipDTO);

        if (result.isSuccess()) {

            RelationshipDTO outputDto = (RelationshipDTO) result.getContent();

            addRelationshipLinksAll(outputDto, familyId);

            return new ResponseEntity<>(outputDto, HttpStatus.CREATED);

        } else {
            ExceptionDTO exceptionDTO = (ExceptionDTO) result.getContent();

            addRelationshipFamilyLink(exceptionDTO, familyId);
            addRelationshipsLink(exceptionDTO, familyId);

            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping(value = "/families/{id}/relations/{relationshipId}")
    public ResponseEntity<Object> getRelationship(@PathVariable("id") String familyId,
                                                  @PathVariable("relationshipId") String relationshipId) {

        Result result = familyRelationshipService.getRelationshipDto(familyId, relationshipId);

        if (result.isSuccess()) {

            RelationshipDTO relationshipDTO = (RelationshipDTO) result.getContent();

            addRelationshipLinksAll(relationshipDTO, familyId);

            return new ResponseEntity<>(relationshipDTO, HttpStatus.OK);

        } else {

            ExceptionDTO exceptionDTO = (ExceptionDTO) result.getContent();

            addRelationshipFamilyLink(exceptionDTO, familyId);
            addRelationshipsLink(exceptionDTO, familyId);


            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }


    }

    //TODO:Pedro. output links are arranged differently here. why?
    @GetMapping(value = "/families/{id}/relations/")
    public ResponseEntity<Object> getAllRelationships(@PathVariable("id") String familyId) {

        Result result = familyRelationshipService.getAllRelationshipsDto(familyId);

        if (result.isSuccess()) {

            List<RelationshipDTO> relationshipDTOList = (List<RelationshipDTO>) result.getContent();

            for (RelationshipDTO dto : relationshipDTOList) {

                addRelationshipLinksAll(dto, familyId);
            }

            return new ResponseEntity<>(relationshipDTOList, HttpStatus.OK);

        } else {

            ExceptionDTO exceptionDTO = (ExceptionDTO) result.getContent();

            addRelationshipFamilyLink(exceptionDTO, familyId);
            addRelationshipsLink(exceptionDTO, familyId);

            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }


    }
}

package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.controllers.icontrollers.IGetStandardCategoryTreeController;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(value = "*")
public class GetStandardCategoryTreeController implements IGetStandardCategoryTreeController {

    @Autowired
    private final ICategoryService iCategoryService;


    /**
     * Get Standard Categories created previously
     *
     * @return ResponseEntity Object
     */
    @GetMapping(value = "/categories")
    public ResponseEntity<Object> getStandardCategoryTree() {

        List<CategoryDTO> result = iCategoryService.getStandardCategoryTree();

        if (result.isEmpty())
            return new ResponseEntity<>("Empty category tree", HttpStatus.OK);

        for (CategoryDTO dto : result) {
            Link selfLink =
                    linkTo(methodOn(GetFamilyCategoryListController.class).getCategoryInfo(dto.getId())).withRel("Category Info");
            dto.add(selfLink);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @GetMapping(value = "/allcategories")
    public ResponseEntity<Object> getFullStandardCategoryTree() {
        List<CategoryDTO> allCategories = iCategoryService.importAllCategories();
        return new ResponseEntity<>(allCategories, HttpStatus.OK);

    }
}

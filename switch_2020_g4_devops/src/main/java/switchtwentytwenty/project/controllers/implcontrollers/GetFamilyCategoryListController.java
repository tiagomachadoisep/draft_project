package switchtwentytwenty.project.controllers.implcontrollers;


import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.controllers.icontrollers.IGetFamilyCategoryListController;
import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class GetFamilyCategoryListController implements IGetFamilyCategoryListController {

    @Autowired
    ICategoryService categoryService;

    @GetMapping(path = "families/{id}/categories")
    public ResponseEntity<Object> getListOfCategories(@PathVariable("id") String familyID) {
        try {
            List<CategoryDTO> categoryDTOList = categoryService.getListOfCategories(familyID);

            for (CategoryDTO dto :
                    categoryDTOList) {
                Link selfLink = linkTo(methodOn(GetFamilyCategoryListController.class).getCategoryInfo(dto.getId())).withSelfRel();

                dto.add(selfLink);

            }
            return new ResponseEntity<>(categoryDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Invalid parameter.", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * This method gets the category id and return its info
     * @param categoryID category id
     * @return Category DTO
     */
    @GetMapping(value = "categories/{id}")
    public ResponseEntity<Object> getCategoryInfo(@PathVariable("id") String categoryID){
        try {
            return new ResponseEntity<>(categoryService.getCategoryById(categoryID),
                    HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Category does not exist", HttpStatus.BAD_REQUEST);
        }
    }
}

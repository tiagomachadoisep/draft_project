package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.controllers.icontrollers.IAddFamilyController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.dto.visitor.LinkAddVisitor;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class AddFamilyController implements IAddFamilyController {

    private static final String FAMILIES = "families";
    private static final String MEMBERS = "members";
    private static final String ADMIN = "admin";

    @Autowired
    private final AssemblerToDTO assemblerToDTO;
    @Autowired
    private final IFamilyService familyService;

    @Autowired
    public AddFamilyController(AssemblerToDTO assemblerToDTO, IFamilyService familyService) {
        this.assemblerToDTO = assemblerToDTO;
        this.familyService = familyService;
    }

    @PostMapping(value = "/families")
    public ResponseEntity<Object> addFamily(@RequestBody FamilyDTO dto) {
        PersonDTO personDTO = assemblerToDTO.toPersonDTO(dto.getAdminName(), dto.getAdminEmail(), dto.getAdminVat(),
                dto.getAdminBirthDate());
        FamilyDTO familyDTO = assemblerToDTO.toFamilyDTO(dto.getName(), dto.getAdminEmail());

        try {
            FamilyDTO familyDTOout = familyService.createAndSaveFamily(familyDTO, personDTO);

            if (familyDTOout == null) {
                ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto("Member already registered");
                Link tryAgainLink = linkTo(methodOn(AddFamilyController.class).addFamily(dto)).withRel("try again");
                exceptionDTO.add(tryAgainLink);
                return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
            }

            familyDTOout.accept(new LinkAddVisitor());

            return new ResponseEntity<>(familyDTOout, HttpStatus.CREATED);
        } catch (Exception e) {
            String responseMessage = e.getMessage();
            ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto(responseMessage);

            Link tryAgainLink = linkTo(methodOn(AddFamilyController.class).addFamily(dto)).withRel("try again");

            exceptionDTO.add(tryAgainLink);

            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }
    }

    private void populateWithLinks(FamilyDTO familyDTO) {
        Link selfLink = linkTo(methodOn(AddFamilyController.class).getFamily(familyDTO.getId())).withSelfRel();
        Link membersLink =
                linkTo(AddFamilyController.class).slash(FAMILIES).slash(familyDTO.getId()).slash(MEMBERS).withRel(MEMBERS);
        Link adminLink =
                linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(familyDTO.getAdminEmail())).withRel(ADMIN);
        Link relationsLink =
                linkTo(methodOn(CreateFamilyRelationshipController.class).getAllRelationships(familyDTO.getId())).withRel("relations");
        Link accountLink =
                linkTo(CreateFamilyCashAccountController.class).slash(FAMILIES).slash(familyDTO.getId()).slash(
                        "accounts").withRel("account");

        familyDTO.add(selfLink);
        familyDTO.add(membersLink);
        familyDTO.add(adminLink);
        familyDTO.add(relationsLink);
        familyDTO.add(accountLink);
    }

    @GetMapping(value = "/families/{id}")
    public ResponseEntity<Object> getFamily(@PathVariable("id") String id) {
        FamilyDTO familyDTO = familyService.getFamilyById(id);

        populateWithLinks(familyDTO);

        return new ResponseEntity<>(familyDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/families")
    public ResponseEntity<Object> getFamilies() {
        List<FamilyDTO> familyDTOList = familyService.getFamilies();

        for (FamilyDTO dto :
                familyDTOList) {
            Link selfLink = linkTo(methodOn(AddFamilyController.class).getFamily(dto.getId())).withSelfRel();

            Link adminLink =
                    linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(dto.getAdminEmail())).withRel(ADMIN);

            dto.add(selfLink);
            dto.add(adminLink);
        }

        return new ResponseEntity<>(familyDTOList, HttpStatus.OK);
    }

}

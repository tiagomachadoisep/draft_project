package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.dto.TransferDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/")
public class TransferCashBetweenMembersController {

    @Autowired
    private final ILedgerService ledgerService;

    public TransferCashBetweenMembersController(ILedgerService ledgerService) {
        this.ledgerService = ledgerService;
    }

    @PostMapping(value = "/ledgers/transfers/cash")
    public ResponseEntity<Object> transferCash(@RequestBody TransferDTO transferDTO) {

        TransferDTO transactionDTO = ledgerService.transferCash(transferDTO);

        Link accountLink = linkTo(methodOn(CheckAccountBalanceController.class).checkAccountBalance(transactionDTO.getAccountId()))
                .withRel("Account balance");

        transactionDTO.add(accountLink);

        if(transactionDTO.getBalance()==0)
            return new ResponseEntity<>("Insufficient funds", HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(transactionDTO, HttpStatus.CREATED);
    }
}

package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonalCashAccountService;
import switchtwentytwenty.project.controllers.icontrollers.ICreatePersonalCashAccountController;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.NonExistentPersonException;
import switchtwentytwenty.project.exceptions.PersonAlreadyHasCashAccountException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/")
public class CreatePersonalCashAccountController implements ICreatePersonalCashAccountController {

    private static final String MEMBERS = "members";
    private static final String ACCOUNTS = "accounts";

    @Autowired
    private AssemblerToDTO assemblerToDTO;
    @Autowired
    private IPersonalCashAccountService iPersonalCashAccountService;

    /**
     * Creates a personal cash account for a family member.
     *
     * @param inputPersonalCashAccountDTO AccountDTO
     * @return ResponseEntity Object
     */
    @Override
    @PostMapping(value = "members/{id}/accounts/cash")
    public ResponseEntity<Object> createPersonalCashAccount(@PathVariable String id,
                                                            @RequestBody AccountDTO inputPersonalCashAccountDTO) {
        String errorMessage = "";

        try {
            AccountDTO assembledDTO = assemblerToDTO.inputToPersonalCashAccountDTO(id,
                    inputPersonalCashAccountDTO);

            AccountDTO personalCashAccountDTOBeforeLinks =
                    iPersonalCashAccountService.createPersonalCashAccount(assembledDTO);

            AccountDTO outputPersonalCashAccountDTO = createAndAddLinksToDTO(personalCashAccountDTOBeforeLinks);

            return new ResponseEntity<>(outputPersonalCashAccountDTO,
                    HttpStatus.CREATED);

        } catch (NonExistentPersonException exception) {
            errorMessage = "Person does not exist";
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(errorMessage, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (PersonAlreadyHasCashAccountException exception) {
            errorMessage = "Person already has a cash account";
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(errorMessage, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException exception) {
            errorMessage = "Wrong info";
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(errorMessage, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Method that gets a personal cash account by its account ID.
     *
     * @param accountId String
     * @param id member id
     * @return AccountDTO
     *
     */
    @GetMapping(value = "/members/{id}/accounts/cash/{accountId}")
    public ResponseEntity<Object> getPersonalCashAccountByAccountId(@PathVariable String id,
                                                                    @PathVariable String accountId) {

        String errorMessage = "";

        try {
            return new ResponseEntity<>(iPersonalCashAccountService.getPersonalCashAccountByID(accountId),
                    HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            errorMessage = "Account does not exist";
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(errorMessage, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Method that creates hypermedia links for US170 REST controller and adds them to the account DTO
     *
     * @param personalCashAccountDTOBeforeLinks AccountDTO
     * @return AccountDTO
     */
    public AccountDTO createAndAddLinksToDTO(AccountDTO personalCashAccountDTOBeforeLinks) {

        Link selfLink =
                linkTo(methodOn(CreatePersonalCashAccountController.class).getPersonalCashAccountByAccountId(personalCashAccountDTOBeforeLinks.getOwnerId(), personalCashAccountDTOBeforeLinks.getAccountId())).withRel("New Account Info");

        Link allMemberAccountsLink =
                linkTo(methodOn(CheckAccountBalanceController.class).getAccountsByOwnerID(personalCashAccountDTOBeforeLinks.getOwnerId())).withRel("All Member accounts");

        Link memberLink =
                linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(personalCashAccountDTOBeforeLinks.getOwnerId())).withRel("Account Owner Info");

        Link registerPaymentLink =
                linkTo(RegisterPaymentUsingCashController.class).slash("ledgers").slash("payments").withRel(
                        "Register cash payment");

        Link accountMovementsLink =
                linkTo(GetAccountMovementsBetweenDatesController.class).slash(MEMBERS).slash(personalCashAccountDTOBeforeLinks.getOwnerId()).slash("accountMovementsLink").withRel("Get Account Movements Between Dates");

        Link addCreditCardAccountLink =
                linkTo(AddCreditCardAccountController.class).slash(MEMBERS).slash(personalCashAccountDTOBeforeLinks.getOwnerId()).slash(ACCOUNTS).slash("credit").withRel("Add " +
                        "Credit Card Account");

        Link addBankAccountLink =
                linkTo(AddBankAccountController.class).slash(MEMBERS).slash(personalCashAccountDTOBeforeLinks.getOwnerId()).slash(ACCOUNTS).slash("bank").withRel("Add" +
                        " Bank Account");

        Link addBankSavingsAccountLink =
                linkTo(AddBankSavingsAccountController.class).slash(MEMBERS).slash(personalCashAccountDTOBeforeLinks.getOwnerId()).slash(ACCOUNTS).slash("savings").withRel("Add" +
                        " Bank Savings Account");

        personalCashAccountDTOBeforeLinks.add(selfLink);
        personalCashAccountDTOBeforeLinks.add(allMemberAccountsLink);
        personalCashAccountDTOBeforeLinks.add(memberLink);
        personalCashAccountDTOBeforeLinks.add(registerPaymentLink);
        personalCashAccountDTOBeforeLinks.add(accountMovementsLink);
        personalCashAccountDTOBeforeLinks.add(addCreditCardAccountLink);
        personalCashAccountDTOBeforeLinks.add(addBankAccountLink);
        personalCashAccountDTOBeforeLinks.add(addBankSavingsAccountLink);

        return personalCashAccountDTOBeforeLinks;
    }

    /**
     * Method that creates hypermedia links for US170 REST controller and adds them to the exception DTO
     *
     * @param errorMessage String
     * @param ownerId      String
     * @return ExceptionDTO
     */
    public ExceptionDTO createAndAddLinksToExceptionDTO(String errorMessage, String ownerId) {

        if (ownerId == null || ownerId.equals("") || errorMessage.equals("Person does not " +
                "exist")) {
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            Link homePage = linkTo(CreatePersonalCashAccountController.class).slash(MEMBERS).withRel("See members");
            exceptionDTO.add(homePage);
            return exceptionDTO;
        }

        ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
        Link createPersonalCashAccountLink =
                linkTo(CreatePersonalCashAccountController.class).slash(MEMBERS).slash(ownerId).slash(ACCOUNTS).slash("cash").withRel("Create a personal cash account");
        exceptionDTO.add(createPersonalCashAccountLink);

        return exceptionDTO;

    }

}

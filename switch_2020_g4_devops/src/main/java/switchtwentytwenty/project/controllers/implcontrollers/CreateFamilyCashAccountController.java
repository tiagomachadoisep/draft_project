package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyCashAccountService;
import switchtwentytwenty.project.controllers.icontrollers.*;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.AdminStateException;
import switchtwentytwenty.project.exceptions.FamilyAlreadyHasCashAccountException;

import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/")
public class CreateFamilyCashAccountController implements ICreateFamilyCashAccountController {

    @Autowired
    private final AssemblerToDTO assembler;
    @Autowired
    private final IFamilyCashAccountService iFamilyCashAccountService;


    @PostMapping(value = "/families/{id}/account")
    public ResponseEntity<Object> createFamilyCashAccount (@PathVariable String id,
                                                           @RequestBody AccountDTO inputDTO){
        AccountDTO assembledDTO = assembler.toFamilyCashAccountDTO(inputDTO.getOwnerId(), inputDTO.getDescription(), inputDTO.getBalance(), "-1");

        try {
            AccountDTO outputDTO = iFamilyCashAccountService.createFamilyCashAccount(assembledDTO);

            Link accountInfoLink = linkTo(methodOn(CreateFamilyCashAccountController.class).getAccountByID(outputDTO.getOwnerId(), outputDTO.getAccountId())).withRel("New account information");

            Link transferMoneyLink = linkTo(TransferFromFamilyCashToMemberCashController.class).slash("ledgers").slash("transfer").withRel("Transfer money");

            outputDTO.add(accountInfoLink);
            outputDTO.add(transferMoneyLink);

            return new ResponseEntity<>(outputDTO, HttpStatus.CREATED);

        }
        catch (FamilyAlreadyHasCashAccountException e){
            return new ResponseEntity<>("Family already has a cash account", HttpStatus.BAD_REQUEST);
        }
        catch (AdminStateException e ){
            return new ResponseEntity<>("Person is not the family administrator of this family",HttpStatus.BAD_REQUEST);
        }
        catch (Exception e){
            return new ResponseEntity<>("Information Error",HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Retrieves family cash account information
     *
     * @param ownerId String
     * @param accountId String
     * @return cash account
     */
    @GetMapping(value = "/families/{ownerId}/account")
    public ResponseEntity<Object> getAccountByID (@PathVariable String ownerId, String accountId) {
        Optional<AccountDTO> output = iFamilyCashAccountService.getFamilyCashAccount(accountId);

        if(output.isPresent())
            return new ResponseEntity<>(output, HttpStatus.OK);


        else {
            String errorMessage = "Family doesn't have a cash account";
            ExceptionDTO outputExceptionDTO = new ExceptionDTO(errorMessage);

            Link createAccountLink = linkTo(CreateFamilyCashAccountController.class).slash("families").slash(ownerId).slash("accounts").withRel("Create a family cash account");

            outputExceptionDTO.add(createAccountLink);

            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        }

    }
}

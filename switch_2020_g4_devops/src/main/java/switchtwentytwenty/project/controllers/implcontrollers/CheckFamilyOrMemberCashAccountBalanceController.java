package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.controllers.icontrollers.ICheckFamilyOrMemberCashAccountBalanceController;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
public class CheckFamilyOrMemberCashAccountBalanceController implements ICheckFamilyOrMemberCashAccountBalanceController {

    @Autowired
    private IAccountService iAccountService;

    @GetMapping(value = "accounts/{id}/cash/balance")
    public ResponseEntity<Object> checkFamilyOrMemberCashAccountBalance(@PathVariable("id") String ownerId) {
        try {
            PersonID personID = new PersonID(new Email(ownerId));
            AccountDTO accountDTO = iAccountService.getFamilyOrMemberCashAccount(personID);
            Link self = linkTo(methodOn(CheckFamilyOrMemberCashAccountBalanceController.class).
                    checkFamilyOrMemberCashAccountBalance(ownerId)).withSelfRel();
            accountDTO.add(self);

            return new ResponseEntity<>(accountDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Invalid parameter", HttpStatus.BAD_REQUEST);
        }

    }


}

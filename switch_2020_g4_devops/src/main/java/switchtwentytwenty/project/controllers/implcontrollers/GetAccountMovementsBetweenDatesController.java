package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.controllers.icontrollers.IGetAccountMovementsBetweenDatesController;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;
import switchtwentytwenty.project.dto.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/")
public class GetAccountMovementsBetweenDatesController implements IGetAccountMovementsBetweenDatesController {
    @Autowired
    private final ILedgerService iLedgerService;

    /**
     * GetAccountMovementsBetweenDatesController constructor with all arguments.
     * @param iLedgerService ledgerService
     */
    public GetAccountMovementsBetweenDatesController(ILedgerService iLedgerService) {
        this.iLedgerService = iLedgerService;
    }

    /**
     * Method to get the account movements between two certain dates
     * @param ledgerID ledger ID
     * @param accountID account ID
     * @param startDate start Date parameter
     * @param endDate end date parameter
     * @return list of movement DTO's between two dates
     */
    @GetMapping(value = "/ledgers/{ledgerID}/accounts/{accountID}/movements")
    public ResponseEntity<Object> getAccountMovementsBetweenDates(@PathVariable("ledgerID") int ledgerID,
                                                                  @PathVariable("accountID") String accountID,
                                                                  @RequestParam(value = "startDate", defaultValue = "2000-01-01") String startDate,
                                                                  @RequestParam(value = "endDate", defaultValue = "2021-12-12") String endDate) {
        try {
            LedgerID idLedger = new LedgerID(ledgerID);
            AccountID idAccount = new AccountID(accountID);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            TransactionDate dateStart = new TransactionDate(LocalDate.parse(startDate, formatter));
            TransactionDate dateEnd = new TransactionDate(LocalDate.parse(endDate, formatter));
            List<MovementDTO> movementsDTO = iLedgerService.getAccountMovementsBetweenDates
                    (idLedger, idAccount, dateStart, dateEnd);
            List<MovementDTO> movementsDTOWithLink = new ArrayList<>();
            for (MovementDTO movementDTO : movementsDTO) {
                MoneyValue value = new MoneyValue(movementDTO.getAmount());
                List<TransactionDTO> possibleTransactions = iLedgerService.findPossibleTransactionsByMovement(idLedger, idAccount, value);
                for (TransactionDTO transaction : possibleTransactions) {
                    TransactionID transactionID = new TransactionID(transaction.getTransactionID());
                    Link transactionInfo = linkTo(methodOn(GetAccountMovementsBetweenDatesController.class).
                            getTransaction(ledgerID, transactionID.toString())).withRel("Possible Transactions Info");
                    movementDTO.add(transactionInfo);
                }
                movementsDTOWithLink.add(movementDTO);
            }
            return new ResponseEntity<>(movementsDTOWithLink, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>("Wrong info", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method to get a transaction from a ledger
     * @param ledgerID ledger id
     * @param transactionID transaction id
     * @return transaction DTO according to the transaction id
     */
    @GetMapping(value = "ledgers/{ledgerID}/transactions/{transactionID}")
    public ResponseEntity<Object> getTransaction(@PathVariable("ledgerID") int ledgerID,
                                                 @PathVariable("transactionID") String transactionID) {
        try {
            TransactionID idTransaction = new TransactionID(transactionID);
            LedgerID idLedger = new LedgerID(ledgerID);
            TransactionDTO transactionDTO = iLedgerService.findTransactionByID(idTransaction, idLedger);
            return new ResponseEntity<>(transactionDTO, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Wrong info", HttpStatus.BAD_REQUEST);
        }
    }
}

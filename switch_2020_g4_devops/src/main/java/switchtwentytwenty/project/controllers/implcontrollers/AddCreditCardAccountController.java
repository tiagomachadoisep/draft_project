package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.controllers.icontrollers.IAddCreditCardAccountControllerREST;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AccountDTOOUT;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class AddCreditCardAccountController implements IAddCreditCardAccountControllerREST {

    @Autowired
    private final IAccountService iAccountService;
    @Autowired
    private final AssemblerToDTO assemblerToDTO;


    public AddCreditCardAccountController(IAccountService accountService, AssemblerToDTO assemblerToDTO) {
        this.iAccountService = accountService;
        this.assemblerToDTO = assemblerToDTO;
    }

    @PostMapping(value = "members/{id}/accounts/credit")
    public ResponseEntity<Object> addCreditCardAccount(@PathVariable String id, @RequestBody AccountDTO accountDTO) {
        try {
            AccountDTO accountDTOExit = iAccountService.addCreditCardAccount(accountDTO);
            /*
            AccountDTOOUT accountDTOOUT = assemblerToDTO.toAccountDtoOut(accountDTO);
            accountDTOOUT.add(linkToNewAccountInfo(accountDTO));
            accountDTOOUT.add(linkToAllMembersAccounts(accountDTO));
            */
            accountDTOExit.add(linkToNewAccountInfo(accountDTO));
            accountDTOExit.add(linkToAllMembersAccounts(accountDTO));
            return new ResponseEntity<>(accountDTOExit, HttpStatus.OK);

        } catch (Exception exception) {
            String responseMessage = "Can't add Credit Card Account - " + exception.getMessage();
            ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto(responseMessage);
            exceptionDTO.add(linkToAllMembersAccounts(accountDTO));

            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }
    }

    private Link linkToNewAccountInfo(AccountDTO accountDTO) {
        return linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountByID(accountDTO.getAccountId())).withRel("New Account Info");
    }

    private Link linkToAllMembersAccounts(AccountDTO accountDTO) {
        return linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(accountDTO.getOwnerId())).withRel("All Member accounts");
    }
}

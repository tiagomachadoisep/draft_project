package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.controllers.icontrollers.IAddCustomCategoryControllerV2;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;
import switchtwentytwenty.project.exceptions.InvalidDesignationException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class AddCustomCategoryControllerV2 implements IAddCustomCategoryControllerV2 {

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private AssemblerToDTO assemblerToDTO;

    @PostMapping(value = "families/{id}/categoriesV2")
    public ResponseEntity<Object> addCustomSubcategoryV2(@PathVariable String id,
                                                         @RequestBody CategoryDTO inputCategoryDTO) {

        try {
            if (isSelectedParentCategoryAvailable(inputCategoryDTO)) {
                CategoryDTO assembledCategoryDTO = assemblerToDTO.inputToCustomCategoryDTO(id, inputCategoryDTO);
                CategoryDTO categoryDTOBeforeLinks = categoryService.addCustomSubCategory(assembledCategoryDTO);
                CategoryDTO outputCategoryDTO = createAndAddLinksToDTO(categoryDTOBeforeLinks);
                return new ResponseEntity<>(outputCategoryDTO, HttpStatus.CREATED);
            }
            String errorMessage = "Category creation failed! You must select a parent category.";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (DuplicatedValueException exception) {
            String errorMessage = "Category creation failed! A category with that name already exists.";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (InvalidDesignationException exception) {
            String errorMessage = "Category creation failed! Invalid category name. Spaces and numbers are not " +
                    "allowed.";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Method that verifies if the new custom category is the child of any of the imported standard categories.
     *
     * @param inputCategoryDTO CategoryDTO
     * @return boolean
     */

    public boolean isSelectedParentCategoryAvailable(CategoryDTO inputCategoryDTO) {

        try {
            List<CategoryDTO> availableStandardCategoriesList = categoryService.importAllCategories();
            for (CategoryDTO categoryDTO : availableStandardCategoriesList
            ) {
                if (categoryDTO.getId().equals(inputCategoryDTO.getParentId())) {
                    return true;
                }
            }
            return false;
        } catch (IllegalStateException exception) {
            return false;
        }

    }

    /**
     * Method that creates hypermedia links for US111v2 REST controller and adds them to the category DTO
     *
     * @param categoryDTOBeforeLinks CategoryDTO
     * @return CategoryDTO
     */
    public CategoryDTO createAndAddLinksToDTO(CategoryDTO categoryDTOBeforeLinks) {

        Link selfLink =
                linkTo(methodOn(AddCustomCategoryController.class).getFamilyCategoryByCategoryId(categoryDTOBeforeLinks.getFamilyId(), categoryDTOBeforeLinks.getId())).withRel("New CategoryInfo");

        Link standardCategoriesListLink =
                linkTo(methodOn(GetStandardCategoryTreeController.class).getFullStandardCategoryTree()).withRel(
                        "Standard Category List");

        Link familyCategoriesListLink =
                linkTo(methodOn(GetFamilyCategoryListController.class).getListOfCategories(categoryDTOBeforeLinks.getFamilyId())).withRel("Family Category List");

        Link addCustomCategoryLink =
                linkTo(AddCustomCategoryControllerV2.class).slash("families").slash(categoryDTOBeforeLinks.getFamilyId()).slash("categoriesV2").withRel("Add another category to the family");

        categoryDTOBeforeLinks.add(selfLink);
        categoryDTOBeforeLinks.add(standardCategoriesListLink);
        categoryDTOBeforeLinks.add(familyCategoriesListLink);
        categoryDTOBeforeLinks.add(addCustomCategoryLink);

        return categoryDTOBeforeLinks;

    }

    /**
     * Method that creates hypermedia links for US111v2 REST controller and adds them to the exception DTO
     *
     * @param exceptionDTOBeforeLinks ExceptionDTO
     * @param familyId                String
     * @return ExceptionDTO
     */
    public ExceptionDTO createAndAddLinksToExceptionDTO(ExceptionDTO exceptionDTOBeforeLinks, String familyId) {

        Link addCustomCategoryLink =
                linkTo(AddCustomCategoryControllerV2.class).slash("families").slash(familyId).slash("categoriesV2").withRel("Add a custom category");

        Link standardCategoriesListLink =
                linkTo(methodOn(GetStandardCategoryTreeController.class).getFullStandardCategoryTree()).withRel(
                        "Standard Category List");

        Link getFamilyCategoryList =
                linkTo(methodOn(GetFamilyCategoryListController.class).getListOfCategories(familyId)).withRel("Family" +
                        " Category List");

        exceptionDTOBeforeLinks.add(addCustomCategoryLink);
        exceptionDTOBeforeLinks.add(standardCategoriesListLink);
        exceptionDTOBeforeLinks.add(getFamilyCategoryList);

        return exceptionDTOBeforeLinks;

    }


}

package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.controllers.icontrollers.IRegisterPaymentUsingCashController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.PaymentDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value="api")
public class RegisterPaymentUsingCashController implements IRegisterPaymentUsingCashController {

    private final ILedgerService ledgerService;
    private final AssemblerToDTO mapper;

    @Autowired
    public RegisterPaymentUsingCashController(ILedgerService ledgerService, AssemblerToDTO map){
        this.ledgerService=ledgerService;
        this.mapper=map;
    }

    @Override
    public PaymentDTO registerPayment(String mainEmail, double amount, String categoryId) {
        PaymentDTO paymentDTO = mapper.toPaymentDTO(mainEmail,amount,categoryId);
        return ledgerService.registerPaymentDS(paymentDTO);
    }


    @PostMapping(value = "/ledgers/payments")
    public ResponseEntity<Object> registerPaymentRest(@RequestBody PaymentDTO paymentDTO) {
        PaymentDTO transactionDTO = ledgerService.registerPaymentDS(paymentDTO);

        if(transactionDTO.getBalance()==0)
            return new ResponseEntity<>("Insufficient funds",HttpStatus.BAD_REQUEST);

        Link accountLink = linkTo(methodOn(CheckAccountBalanceController.class).checkAccountBalance(transactionDTO.getAccountID()))
                .withRel("Account balance");

        transactionDTO.add(accountLink);

        return new ResponseEntity<>(transactionDTO, HttpStatus.CREATED);
    }
}

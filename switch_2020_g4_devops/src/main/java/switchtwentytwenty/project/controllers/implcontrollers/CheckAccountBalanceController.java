package switchtwentytwenty.project.controllers.implcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.controllers.icontrollers.ICheckAccountBalanceController;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AccountDTOOUT;
import switchtwentytwenty.project.dto.AssemblerToDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class CheckAccountBalanceController implements ICheckAccountBalanceController {

    @Autowired
    private final IAccountService iAccountService;
    private final ILedgerService iLedgerService;

    @Autowired
    private final AssemblerToDTO assembler;

    private static final String WRONG_INFORMATION = "Wrong Information";

    public CheckAccountBalanceController(IAccountService accountService, ILedgerService ledgerService, AssemblerToDTO assemblerToDTO) {
        this.iAccountService = accountService;
        this.iLedgerService = ledgerService;
        this.assembler = assemblerToDTO;
    }

    @GetMapping(value = "/members/{memberID}/ledger")
    public ResponseEntity<Object> findLedgerIDByOwnerID(@PathVariable("memberID") String memberID) {
        try {
            PersonID personID = new PersonID(new Email(memberID));
            Optional<Ledger> ledger = iLedgerService.findLedgerByOwnerID(personID);
            LedgerID ledgerID;
            if (ledger.isPresent()) {
                ledgerID = ledger.get().getID();
            } else {
                throw new NullPointerException("No ledger found.");
            }
            return new ResponseEntity<>(ledgerID, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>("MemberID not valid", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/members/{id}/accounts")
    public ResponseEntity<Object> getAccountsByOwnerID(@PathVariable("id") String id) {

        try {
            PersonID personID = new PersonID(new Email(id));
            List<AccountDTO> accountDTOList = iAccountService.findAllAccountsByOwner(personID);
            List<AccountDTOOUT> accountsDescriptionList = new ArrayList<>();
            LedgerID ledgerID = (LedgerID) findLedgerIDByOwnerID(id).getBody();
            for (AccountDTO accountDTO : accountDTOList) {
                AccountDTOOUT accountDTOOUT = assembler.toAccountDtoOut(accountDTO);

                Link accountInfoLink = linkTo(methodOn(CheckAccountBalanceController.class).
                        getAccountByID(accountDTO.getAccountId())).withRel("Account Info");

                assert ledgerID != null;
                Link movementsInfo =
                        linkTo(GetAccountMovementsBetweenDatesController.class).slash("ledgers").slash(ledgerID.getLedgerIdValue()).
                        slash("accounts").slash(accountDTO.getAccountId()).slash("movements").withRel("MovementsInfo");

                accountDTOOUT.add(movementsInfo);
                accountDTOOUT.add(accountInfoLink);
                accountsDescriptionList.add(accountDTOOUT);

            }
            return new ResponseEntity<>(accountsDescriptionList, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(WRONG_INFORMATION, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/accounts/{id}/balance")
    public ResponseEntity<Object> checkAccountBalance(@PathVariable("id") String id) {


        try {
            AccountID accountID = new AccountID(id);
            return new ResponseEntity<>(iAccountService.checkAccountBalance(accountID).getValue(), HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(WRONG_INFORMATION, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/accounts/{id}")
    public  ResponseEntity<Object> getAccountByID(@PathVariable("id") String id){

        try {
            return new ResponseEntity<>(iAccountService.getAccountByID(id),HttpStatus.OK);
        }catch ( IllegalArgumentException exception){
            return  new ResponseEntity<>(WRONG_INFORMATION,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/accounts")
    public ResponseEntity<Object> getAllAccounts(){
        List<AccountDTO> accountDTOList = iAccountService.getAllAccounts();

        return new ResponseEntity<>(accountDTOList,HttpStatus.OK);
    }

    @GetMapping(value="/families/{id}/accounts")
    public ResponseEntity<Object> getFamilyAccounts(@PathVariable("id") String familyID){
        List<AccountDTO> accountDTOList = iAccountService.getFamilyAccounts(familyID);

        return new ResponseEntity<>(accountDTOList,HttpStatus.OK);
    }

    @GetMapping(value="/members/{id}/allaccounts")
    public ResponseEntity<Object> getMemberAccounts(@PathVariable("id") String ownerId){
        List<AccountDTO> accountDTOList = iAccountService.findAllAccountsByOwner(new PersonID(new Email(ownerId)));

        return new ResponseEntity<>(accountDTOList,HttpStatus.OK);
    }

}



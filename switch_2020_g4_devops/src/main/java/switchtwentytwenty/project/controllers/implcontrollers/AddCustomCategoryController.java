package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.controllers.icontrollers.IAddCustomCategoryController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;

import java.util.NoSuchElementException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class AddCustomCategoryController implements IAddCustomCategoryController {

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private AssemblerToDTO assemblerToDTO;

    /**
     * Method that adds a custom category.
     *
     * @param inputCategoryDTO CategoryDTO
     * @param id               String
     * @return ResponseEntity Object
     */
    @Override
    @PostMapping(value = "/families/{id}/categories")
    public ResponseEntity<Object> addCustomCategory(@PathVariable String id,
                                                    @RequestBody CategoryDTO inputCategoryDTO) {

        CategoryDTO assembledCategoryDTO = assemblerToDTO.inputToCustomCategoryDTO(id, inputCategoryDTO);
        String parentID = inputCategoryDTO.getParentId();
        CategoryDTO categoryDTOBeforeLinks;

        try {
            if (parentID.equals("")) {
                categoryDTOBeforeLinks = categoryService.addCustomCategory(assembledCategoryDTO);
            } else {
                categoryDTOBeforeLinks = categoryService.checksParentIdAndAddsCustomSubcategory(assembledCategoryDTO);
            }
            CategoryDTO outputCategoryDTO = createAndAddLinksToDTO(categoryDTOBeforeLinks);
            return new ResponseEntity<>(outputCategoryDTO, HttpStatus.CREATED);
        } catch (NoSuchElementException exception) {
            String errorMessage = "ParentID not found";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (DuplicatedValueException exception) {
            String errorMessage = "Category already exists";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException exception) {
            String errorMessage = "Invalid parameters";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method that gets a family category by its category ID.
     *
     * @param id         String
     * @param categoryId String
     * @return ResponseEntity Object
     */
    @GetMapping(value = "/families/{id}/categories/{categoryId}")
    public ResponseEntity<Object> getFamilyCategoryByCategoryId(@PathVariable String id,
                                                                @PathVariable String categoryId) {

        try {
            return new ResponseEntity<>(categoryService.getFamilyCategoryByCategoryId(id, categoryId),
                    HttpStatus.OK);
        } catch (NoSuchElementException exception) {
            String errorMessage = "Category does not exist";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        } catch (IllegalStateException exception) {
            String errorMessage = "Category does not belong to this family";
            ExceptionDTO exceptionDTO = new ExceptionDTO(errorMessage);
            ExceptionDTO outputExceptionDTO = createAndAddLinksToExceptionDTO(exceptionDTO, id);
            return new ResponseEntity<>(outputExceptionDTO, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Method that creates hypermedia links for US111 REST controller and adds them to the category DTO
     *
     * @param categoryDTOBeforeLinks CategoryDTO
     * @return CategoryDTO
     */
    public CategoryDTO createAndAddLinksToDTO(CategoryDTO categoryDTOBeforeLinks) {

        Link selfLink =
                linkTo(methodOn(AddCustomCategoryController.class).getFamilyCategoryByCategoryId(categoryDTOBeforeLinks.getFamilyId(), categoryDTOBeforeLinks.getId())).withRel("New CategoryInfo");


        Link familyCategoriesListLink =
                linkTo(methodOn(GetFamilyCategoryListController.class).getListOfCategories(categoryDTOBeforeLinks.getFamilyId())).withRel("Family Category List");

        Link addCustomCategoryLink =
                linkTo(AddCustomCategoryController.class).slash("families").slash(categoryDTOBeforeLinks.getFamilyId()).slash("categories").withRel("Add another category to the family");

        categoryDTOBeforeLinks.add(selfLink);
        categoryDTOBeforeLinks.add(familyCategoriesListLink);
        categoryDTOBeforeLinks.add(addCustomCategoryLink);

        return categoryDTOBeforeLinks;

    }

    /**
     * Method that creates hypermedia links for US111v2 REST controller and adds them to the exception DTO
     *
     * @param exceptionDTOBeforeLinks ExceptionDTO
     * @param familyId                String
     * @return ExceptionDTO
     */
    public ExceptionDTO createAndAddLinksToExceptionDTO(ExceptionDTO exceptionDTOBeforeLinks, String familyId) {

        Link addCustomCategoryLink =
                linkTo(AddCustomCategoryControllerV2.class).slash("families").slash(familyId).slash("categoriesV2").withRel("Add a custom category");

        Link getFamilyCategoryList =
                linkTo(methodOn(GetFamilyCategoryListController.class).getListOfCategories(familyId)).withRel("Family" +
                        " Category List");

        exceptionDTOBeforeLinks.add(addCustomCategoryLink);
        exceptionDTOBeforeLinks.add(getFamilyCategoryList);

        return exceptionDTOBeforeLinks;

    }

}



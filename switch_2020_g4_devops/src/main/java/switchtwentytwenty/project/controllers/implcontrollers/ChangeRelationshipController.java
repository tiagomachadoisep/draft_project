package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.controllers.icontrollers.IChangeRelationshipController;
import switchtwentytwenty.project.controllers.icontrollers.LinkAddable;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.utils.Result;


@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class ChangeRelationshipController implements IChangeRelationshipController, LinkAddable {

    @Autowired
    private final AssemblerToDTO mapper;

    @Autowired
    private final IFamilyRelationshipService familyRelationshipService;

    @PutMapping(value = "/families/{id}/relations/{relationId}")
    public ResponseEntity<Object> changeRelationship(@PathVariable("id") String familyId,
                                                     @PathVariable("relationId") String relationId,
                                                     @RequestBody RelationshipDTO input) {

        RelationshipDTO relationshipDTO = mapper.inputToRelationshipDTO(familyId, relationId, input.getDesignation());

        Result output = familyRelationshipService.changeRelationship(relationshipDTO);

        if (output.isSuccess()) {

            RelationshipDTO outDto = (RelationshipDTO) output.getContent();

            addRelationshipLinksAll(outDto, familyId);

            return new ResponseEntity<>(outDto, HttpStatus.ACCEPTED);

        } else {

            ExceptionDTO exceptionDTO = (ExceptionDTO) output.getContent();

            addRelationshipFamilyLink(exceptionDTO, familyId);
            addRelationshipsLink(exceptionDTO, familyId);

            return new ResponseEntity<>(exceptionDTO, HttpStatus.BAD_REQUEST);
        }
    }
}

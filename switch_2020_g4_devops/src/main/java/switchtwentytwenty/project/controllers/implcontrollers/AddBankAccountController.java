package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.controllers.icontrollers.IAddBankAccountController;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class AddBankAccountController implements IAddBankAccountController {
    @Autowired
    private AssemblerToDTO assemblerToDTO;

    @Autowired
    private final IAccountService iAccountService;

    /**
     * Method that adds a bank account to our persistence.
     *
     * @param id    personId
     * @param input data needed to create a bank account
     * @return returns either the bank account DTO created or an error if occurred.
     */
    @PostMapping(value = "members/{id}/accounts/bank")
    public ResponseEntity<Object> addBankAccount(@PathVariable String id, @RequestBody AccountDTO input) {
        try {
            AccountDTO accountDTO = assemblerToDTO.inputToAccountDTO(id, input);
            AccountDTO output = iAccountService.addBankAccount(accountDTO);
            AccountDTO finalOutput = addLinksToTheOutput(output);

            return new ResponseEntity<>(finalOutput, HttpStatus.OK);
        } catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("Invalid parameters", HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * Links that allows to get access to other account functionalities.
     *
     * @param output AccountDTO
     * @return final output AccountDTO.
     */
    private AccountDTO addLinksToTheOutput(AccountDTO output) {
        Link accountInfo = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountByID(output.getAccountId())).withRel("New Account Info");
        output.add(accountInfo);

        Link allMemberAccountsLink = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(output.getOwnerId())).withRel("All Member accounts");
        output.add(allMemberAccountsLink);
        return output;
    }
}

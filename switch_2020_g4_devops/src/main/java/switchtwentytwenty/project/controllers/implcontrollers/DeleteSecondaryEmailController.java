package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.controllers.icontrollers.IDeleteSecondaryEmailController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class DeleteSecondaryEmailController implements IDeleteSecondaryEmailController {

    @Autowired
    IPersonService personService;
    @Autowired
    AssemblerToDTO assemblerToDTO;

    /**
     * Deletes the given email from the secondary email list
     *
     * @param id    String
     * @param emailToRemove String
     * @return ResponseEntity Object
     */
    @PutMapping("/members/{id}/emails/{emailToRemove}") //
    public ResponseEntity<Object> deleteSecondaryEmail(@PathVariable String id, @PathVariable String emailToRemove) {
        EmailDTO emailDTO = assemblerToDTO.toEmailDTO(emailToRemove, id);
        try {
            List<String> emailList = personService.deleteSecondaryEmail(emailDTO);

            EmailDTO outputDTO = assemblerToDTO.toEmailDTO(emailToRemove, id);
            //Sets new secondary email list
            outputDTO.setEmailsList(emailList);

            // Create and add link here
            Link deleteOtherLink = linkTo(methodOn(DeleteSecondaryEmailController.class).deleteSecondaryEmail(emailDTO.getEmail(), emailDTO.getMainEmail())).withSelfRel();
            Link profileInfoLink = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(emailDTO.getMainEmail())).withRel("Get profile information");

            outputDTO.add(deleteOtherLink);
            outputDTO.add(profileInfoLink);

            return new ResponseEntity<>(outputDTO, HttpStatus.OK);
        } catch (IllegalArgumentException e) { //"Cannot remove main email" / "Person does not exist"
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}

package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAuthenticationService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.controllers.icontrollers.IGetProfileInfoController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;


@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
@CrossOrigin(origins = "*")
public class GetProfileInfoController implements IGetProfileInfoController {

    @Autowired
    private final ISecurityContextProvider securityContextProvider;
    @Autowired
    private final IAuthenticationService authenticationService;
    @Autowired
    IPersonService personService;
    @Autowired
    AssemblerToDTO assemblerToDTO;

    /**
     * This method gets the member's profile
     *
     * @param mainEmail - person ID
     * @return String
     */
    @GetMapping(path = "/members/{id}")
    public ResponseEntity<Object> getProfileInfo(@PathVariable("id") String mainEmail) {

        try {
            Link selfLInk = linkTo(GetProfileInfoController.class).slash("members").slash("profile").withSelfRel();
            if(!authenticationService.isCurrentUser(mainEmail)){
                String responseMessage = "Forbidden - this is not you";
                ExceptionDTO exceptionDTO = assemblerToDTO.toExceptionDto(responseMessage);
                exceptionDTO.add(selfLInk);
                return new ResponseEntity<>(exceptionDTO, HttpStatus.FORBIDDEN);
            }


            PersonDTO personDTO = personService.getPersonDTO(mainEmail);

            personDTO.add(selfLInk);

            return new ResponseEntity<>(personDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Invalid parameter.", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/members/profile")
    public ResponseEntity<Object> getCurrentUserProfileInfo(){
        String userEmail = authenticationService.getCurrentUser();
        return getProfileInfo(userEmail);
    }
}


package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.controllers.icontrollers.IAddStandardCategoryController;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;

import java.util.NoSuchElementException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/")
@AllArgsConstructor
public class AddStandardCategoryController implements IAddStandardCategoryController {

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private AssemblerToDTO mapper;


    @PostMapping(value = "/categories")
    public ResponseEntity<Object> addStandardCategory(@RequestBody CategoryDTO input) {

        String parentID = input.getParentId();
        CategoryDTO output;
        CategoryDTO categoryDTO = mapper.inputToCategoryDTO(input);
        try {
            if (parentID == null) {
                output = categoryService.addStandardCategory(categoryDTO);
            } else {
                output = categoryService.addStandardSubCategory(categoryDTO);
            }
            CategoryDTO finalOutput = addDTOLinks(output);
            return new ResponseEntity<>(finalOutput, HttpStatus.CREATED);
        } catch (NoSuchElementException exception) {
            return new ResponseEntity<>("ParentID not found", HttpStatus.BAD_REQUEST);
        } catch (DuplicatedValueException e) {
            return new ResponseEntity<>("Category already exists", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("Invalid parameters", HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "/categories/{categoryId}")
    public ResponseEntity<Object> getCategoryById(@PathVariable String categoryId) {
        CategoryDTO output;
        try {
            output = categoryService.getCategoryById(categoryId);
            return new ResponseEntity<>(output, HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity<>("Category not found", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Links that allows to get access to other category functionalities.
     *
     * @param output CategoryDTO
     * @return final output categorydto.
     */
    private CategoryDTO addDTOLinks(CategoryDTO output) {
        Link standardCategoriesLink = linkTo(methodOn(GetStandardCategoryTreeController.class).getStandardCategoryTree()).withRel("standardCategories");
        Link selfLink = linkTo(methodOn(AddStandardCategoryController.class).getCategoryById(output.getId())).withSelfRel();
        output.add(standardCategoriesLink);
        output.add(selfLink);
        return output;
    }

}

package switchtwentytwenty.project.controllers.implcontrollers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/")
public class CheckChildsCashAccountBalance {

    @Autowired
    private final IAccountService accountService;

    @Autowired
    private final IFamilyRelationshipService familyRelationService;

    @Autowired
    private final AssemblerToDTO assembler;


    @GetMapping(value = "/members/{id}/accounts/cash")
    public ResponseEntity<Object> checkChildsCashAccount(@PathVariable("id") String parentId, @RequestBody RelationshipDTO childDto) {
        RelationshipDTO input = assembler.inputSearchForRelationshipDTO(childDto.getFamilyId(), parentId, childDto.getFirstPersonEmail());
        try {
            if (familyRelationService.checkIfIsChild(input)) {
                AccountDTO childBalance = accountService.checkChildsCashAccountBalance(childDto.getFirstPersonEmail());
                AccountDTO finalResponse = addLinksToTheOutput(parentId, childBalance);
                return new ResponseEntity<>(finalResponse, HttpStatus.OK);
            } else {
                throw new IllegalArgumentException("The member is not the parent");

            }
        } catch (RuntimeException ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);

        }
    }

    /**
     * Links that allows to get access to other account functionalities.
     *
     * @param parentId parent e-mail
     * @return final output AccountDTO.
     */
    private AccountDTO addLinksToTheOutput(String parentId, AccountDTO childOutput) {


        Link allMemberAccountsLink = linkTo(methodOn(CheckAccountBalanceController.class).
                getAccountsByOwnerID(parentId)).withRel("All Member accounts");
        childOutput.add(allMemberAccountsLink);
        return childOutput;
    }

}

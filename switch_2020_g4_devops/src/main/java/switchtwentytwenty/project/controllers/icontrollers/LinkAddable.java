package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.controllers.implcontrollers.ChangeRelationshipController;
import switchtwentytwenty.project.controllers.implcontrollers.CreateFamilyRelationshipController;
import switchtwentytwenty.project.controllers.implcontrollers.GetProfileInfoController;
import switchtwentytwenty.project.dto.RelationshipDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public interface LinkAddable {

    default void addRelationshipSelfLink(RelationshipDTO relationshipDTO, String familyId) {

        relationshipDTO.add(linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getRelationship(familyId, relationshipDTO.getRelationshipId()))
                .withSelfRel());
    }

    default void addRelationshipChangeRelLink(RelationshipDTO relationshipDTO, String familyId) {

        relationshipDTO.add(linkTo(ChangeRelationshipController.class).slash("families").slash(familyId).
                slash("relations").slash(relationshipDTO.getRelationshipId()).withRel("changeRel"));
    }

    default void addRelationshipFamilyLink(RepresentationModel dto, String familyId) {

        dto.add(linkTo(methodOn(AddFamilyController.class).getFamily(familyId)).
                withRel("family"));
    }

    default void addRelationshipPersonOneLink(RelationshipDTO relationshipDTO) {

        relationshipDTO.add(linkTo(methodOn(GetProfileInfoController.class)
                .getProfileInfo(relationshipDTO.getFirstPersonEmail())).withRel("first person"));
    }

    default void addRelationshipPersonTwoLink(RelationshipDTO relationshipDTO) {

        relationshipDTO.add(linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(relationshipDTO.
                getSecondPersonEmail())).withRel("second person"));
    }

    default void addRelationshipsLink(RepresentationModel dto, String familyId) {

        dto.add(linkTo(methodOn(CreateFamilyRelationshipController.class)
                .getAllRelationships(familyId)).withRel("relationships"));
    }

    default void addRelationshipLinksAll(RelationshipDTO relationshipDTO, String familyId) {
        addRelationshipSelfLink(relationshipDTO, familyId);
        addRelationshipChangeRelLink(relationshipDTO, familyId);
        addRelationshipFamilyLink(relationshipDTO, familyId);
        addRelationshipPersonOneLink(relationshipDTO);
        addRelationshipPersonTwoLink(relationshipDTO);
        addRelationshipsLink(relationshipDTO, familyId);
    }


}

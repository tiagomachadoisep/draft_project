package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;

public interface IGetProfileInfoController {
    ResponseEntity<Object> getProfileInfo(String mainEmail);
}

package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;


public interface ICheckFamilyOrMemberCashAccountBalanceController {

    ResponseEntity<Object> checkFamilyOrMemberCashAccountBalance(String ownerId);
}

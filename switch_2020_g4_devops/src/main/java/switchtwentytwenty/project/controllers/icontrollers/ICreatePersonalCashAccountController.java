package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.AccountDTO;

public interface ICreatePersonalCashAccountController {

    /**
     * Creates a personal cash account for a family member.
     *
     * @param personalCashAccountDTO AccountDTO
     * @param id member id
     * @return ResponseEntityObject
     */
    ResponseEntity<Object> createPersonalCashAccount(String id, AccountDTO personalCashAccountDTO);
}

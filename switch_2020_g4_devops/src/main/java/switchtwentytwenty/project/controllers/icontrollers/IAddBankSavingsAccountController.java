package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.AccountDTO;

public interface IAddBankSavingsAccountController {

    ResponseEntity<Object> addBankSavingsAccount(String id, AccountDTO input);
}

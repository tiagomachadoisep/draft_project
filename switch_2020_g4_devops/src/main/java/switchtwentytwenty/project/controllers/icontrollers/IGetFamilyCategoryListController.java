package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;

public interface IGetFamilyCategoryListController {

    ResponseEntity<Object> getListOfCategories(String familyID);

    ResponseEntity<Object> getCategoryInfo(String id);
}

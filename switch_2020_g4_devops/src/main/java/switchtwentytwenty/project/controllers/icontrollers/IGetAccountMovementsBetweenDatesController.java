package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;

public interface IGetAccountMovementsBetweenDatesController {

    ResponseEntity<Object> getAccountMovementsBetweenDates(int ledgerID, String accountID, String startDate, String endDate);

    ResponseEntity<Object> getTransaction(int ledgerID, String transactionID);

}

package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.CategoryDTO;

public interface IAddCustomCategoryController {

    /**
     * Method that adds a custom category.
     *
     * @param inputCategoryDTO CategoryDTO
     * @param id               String
     * @return ResponseEntity Object
     */
    ResponseEntity<Object> addCustomCategory(String id, CategoryDTO inputCategoryDTO);
}

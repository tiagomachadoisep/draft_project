package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.AccountDTO;

public interface ICreateFamilyCashAccountController {

    /**
     * Creates a Family cash account
     * @param inputDTO - DTO with information to create Cash Account (id, description, balance)
     * @param id id
     * @return ResponseEntity Object
     */
    ResponseEntity<Object> createFamilyCashAccount(String id, AccountDTO inputDTO);

    /**
     * Retrieves family cash account information
     *
     * @param ownerId String
     * @param accountId String
     * @return the account
     */
    ResponseEntity<Object> getAccountByID (String ownerId, String accountId);
}

package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.AccountDTO;

public interface IAddBankAccountController {

    ResponseEntity<Object> addBankAccount(String id, AccountDTO input);
}

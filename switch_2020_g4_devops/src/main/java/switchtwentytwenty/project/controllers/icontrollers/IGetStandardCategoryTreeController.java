package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;

public interface IGetStandardCategoryTreeController {
    /**
     * Get Standard Categories created previously
     *
     * @return ResponseEntity Object
     */
    ResponseEntity<Object> getStandardCategoryTree();
}

package switchtwentytwenty.project.controllers.icontrollers;


import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.EmailDTO;

public interface IAddEmailController {

    ResponseEntity<Object> addEmailAccountToProfile(EmailDTO inputemail, String mainEmail) ;
    ResponseEntity<Object> showOtherEmails(String mainEmail);
}

package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import switchtwentytwenty.project.dto.TransferDTO;

import java.time.LocalDate;

public interface ITransferFromFamilyCashToMemberCashController {

    void transferCashFromFamilyToMember(String  familyId , String memberEmail, double amount, String categoryId, LocalDate date);

    ResponseEntity<Object> transferCashFromFamily(TransferDTO transferDTO);
}

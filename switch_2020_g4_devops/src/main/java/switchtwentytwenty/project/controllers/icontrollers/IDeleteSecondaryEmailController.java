package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;

public interface IDeleteSecondaryEmailController {
    /**
     * Deletes the given email from the secondary email list
     *
     * @param id String
     * @param email String
     * @return ResponseEntity Object
     */
    ResponseEntity<Object> deleteSecondaryEmail(String id, String email);
}

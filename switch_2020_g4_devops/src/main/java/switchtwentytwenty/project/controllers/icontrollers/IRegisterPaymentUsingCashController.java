package switchtwentytwenty.project.controllers.icontrollers;

import switchtwentytwenty.project.dto.PaymentDTO;

public interface IRegisterPaymentUsingCashController {
    PaymentDTO registerPayment(String mainEmail, double amount, String categoryId);
}

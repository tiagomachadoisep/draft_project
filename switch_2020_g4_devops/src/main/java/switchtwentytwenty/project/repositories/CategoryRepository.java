package switchtwentytwenty.project.repositories;


import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepository;
import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.aggregates.category.CustomCategory;
import switchtwentytwenty.project.domain.aggregates.category.StandardCategory;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ICategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ICategoryRepositoryJPA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class CategoryRepository implements ICategoryRepository {

    @Autowired
    private final ICategoryAssemblerJPA categoryAssemblerJPA;

    @Autowired
    private final ICategoryRepositoryJPA categoryRepositoryJPA;


    /**
     * Method that saves a new category on data base.
     *
     * @param newCategory Categorable
     */
    @Override
    public void saveNew(Categorable newCategory) {
        this.categoryRepositoryJPA.save(categoryAssemblerJPA.toJPA(newCategory));
    }

    /**
     * Search a category by ID.
     *
     * @param id id
     * @return the category if is present.
     */
    @Override
    public Optional<Categorable> findByID(CategoryID id) {
        Optional<CategoryJPA> categoryJpa = this.categoryRepositoryJPA.findById(id);
        if (categoryJpa.isPresent()) {
            Categorable category = toEntity(categoryJpa.get());
            return Optional.of(category);
        } else {
            return Optional.empty();
        }

    }

    /**
     * Returns all categories saved.
     *
     * @return a list of categories.
     */
    @Override
    public List<Categorable> findAll() {
        List<CategoryJPA> categoriesJpa = this.categoryRepositoryJPA.findAll();
        List<Categorable> categoriesInSystem = new ArrayList<>();
        for (CategoryJPA category :
                categoriesJpa) {
            categoriesInSystem.add(toEntity(category));
        }
        return categoriesInSystem;
    }

    /**
     * Checks if the ID already exists on system.
     *
     * @param id CategoryID
     * @return true if exists and false otherwise.
     */
    @Override
    public boolean existsID(CategoryID id) {
        return this.categoryRepositoryJPA.existsById(id);
    }


    /**
     * Searches and return a list of categories with same parent category.
     *
     * @param parent StandardCategory
     * @return List
     */
    private List<Categorable> categoriesWithSameParent(String parent) {

        List<Categorable> categories = new ArrayList<>();
        List<CategoryJPA> categoriesJPA = categoryRepositoryJPA.findByParentId(parent);
        for (CategoryJPA catJPA :
                categoriesJPA) {
            categories.add(toEntity(catJPA));
        }
        return categories;
    }

    /**
     * Checks if there is a category with same name at same level.
     * If there is, but categories are different (custom and standard) it is possible to add.
     *
     * @param parent parent category
     * @param name   CategoryName
     * @return boolean
     */
    public boolean existsCategory(String parent, CategoryName name, boolean isStandard) {
        boolean exists = false;

        List<Categorable> categoriesWithSameParent = categoriesWithSameParent(parent);
        if (!categoriesWithSameParent.isEmpty()) {
            for (int categoryIndex = 0; categoryIndex < categoriesWithSameParent.size() && !exists; categoryIndex++) {
                Categorable categorable = categoriesWithSameParent.get(categoryIndex);
                if (categorable.hasSameName(name) && ((categorable.isStandard() && isStandard) || (!categorable.isStandard() && !isStandard))) {
                    exists = true;
                }
            }
        }
        return exists;

    }

    /**
     * returns the standard categories tree
     *
     * @return List Categorable
     */
    @Override
    public List<Categorable> getStandardCategoryTree() {

        List<CategoryJPA> categoryTreeJPA = categoryRepositoryJPA.findAll();

        List<CategoryJPA> standardCategoryTreeJPA = new ArrayList<>();

        for (CategoryJPA categoryJPA : categoryTreeJPA) {
            if (categoryJPA.getFamilyId() == null) {
                standardCategoryTreeJPA.add(categoryJPA);
            }
        }

        return categoryJPAListToCategorableList(standardCategoryTreeJPA);

    }

    /**
     * Transforms a List of Categories JPA into a Categorable List
     *
     * @param categoryTreeJPA jpa
     * @return Categorable list
     */
    private List<Categorable> categoryJPAListToCategorableList(List<CategoryJPA> categoryTreeJPA) {
        List<Categorable> categoryTree = new ArrayList<>();

        for (CategoryJPA categoryJPA : categoryTreeJPA) {
            Categorable standard = toEntity(categoryJPA);
            categoryTree.add(standard);
        }
        return categoryTree;
    }

    /**
     * This method returns a category list of family
     *
     * @param familyId - Family id
     * @return categorable list
     */
    public List<Categorable> getListOfCategoriesByFamilyId(FamilyID familyId) {
        List<CategoryJPA> familyCategoriesJPA = categoryRepositoryJPA.findAllByFamilyId(familyId);
        List<Categorable> familyCategories = categoryJPAListToCategorableList(familyCategoriesJPA);

        List<CategoryJPA> standardCategoriesJPA = categoryRepositoryJPA.findAllByFamilyId(null);
        List<Categorable> standardCategories = categoryJPAListToCategorableList(standardCategoriesJPA);
        familyCategories.addAll(standardCategories);
        if(standardCategoriesJPA.isEmpty() && familyCategoriesJPA.isEmpty()){
            return Collections.emptyList();
        }else{
            return familyCategories;
        }
    }

    /**
     * Transforms CategoryJPA to a Categorable
     *
     * @param categoryJPA data model (JPA)
     * @return domain object
     */
    private Categorable toEntity(CategoryJPA categoryJPA) {

        if (categoryJPA.getFamilyId() == null) {
            return transformsToStandard(categoryJPA);
        } else {
            return transformsToCustom(categoryJPA);
        }

    }

    /**
     * Transforms JPA into Standard Category
     *
     * @param categoryJPA persistence
     * @return Categorable
     */
    private Categorable transformsToStandard(CategoryJPA categoryJPA) {
        if (categoryJPA.getParentId() == null) {
            return new StandardCategory.Builder(categoryJPA.getId())
                    .setName(categoryAssemblerJPA.toCategoryName(categoryJPA.getName()))
                    .setParent(null)
                    .build();
        } else {
            return new StandardCategory.Builder(categoryJPA.getId())
                    .setName(categoryAssemblerJPA.toCategoryName(categoryJPA.getName()))
                    .setParent(categoryAssemblerJPA.toCategoryID(categoryJPA.getParentId()))
                    .build();
        }
    }

    /**
     * Transforms JPA into Custom Category
     *
     * @param categoryJPA persistence
     * @return Categorable
     */
    private Categorable transformsToCustom(CategoryJPA categoryJPA) {
        if (categoryJPA.getParentId() == null) {
            return new CustomCategory.Builder(categoryJPA.getId())
                    .setName(categoryAssemblerJPA.toCategoryName(categoryJPA.getName()))
                    .setParent(null)
                    .setFamily(categoryAssemblerJPA.toFamilyID(categoryJPA.getFamilyId().toString()))
                    .build();
        } else {
            return new CustomCategory.Builder(categoryJPA.getId())
                    .setName(categoryAssemblerJPA.toCategoryName(categoryJPA.getName()))
                    .setParent(categoryAssemblerJPA.toCategoryID(categoryJPA.getParentId()))
                    .setFamily(categoryAssemblerJPA.toFamilyID(categoryJPA.getFamilyId().toString()))
                    .build();
        }
    }

}

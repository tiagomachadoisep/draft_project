package switchtwentytwenty.project.repositories;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.AdminStateException;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IFamilyIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.FamilyJPA;
import switchtwentytwenty.project.persistence.data.FamilyRelationshipJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IFamilyRepositoryJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class FamilyRepository implements IFamilyRepository {

    @Autowired
    private IFamilyRepositoryJPA familyRepositoryJPA;
    @Autowired
    private IFamilyIAssemblerJPA familyIAssemblerJPA;

    /**
     * Gets the family ID with corresponding admin main email
     *
     * @param email Email
     * @return FamilyID
     */
    @Override
    public FamilyID getFamilyIdByAdminMainEmail(Email email) {
        List<FamilyJPA> families = familyRepositoryJPA.findAll();

        for (FamilyJPA famJPA : families) {
            if (famJPA.isAdmin(email.toString())) {
                return famJPA.getID();
            }
        }
        throw new AdminStateException("Person is not the family administrator of this family");
    }

    /**
     * Searches for all families saved in the system.
     *
     * @return List of families.
     */
    @Override
    public List<Family> findAll() {
        List<FamilyJPA> familyJPAList = familyRepositoryJPA.findAll();
        List<Family> families = new ArrayList<>();
        for (FamilyJPA jpa :
                familyJPAList) {
            families.add(familyJPAToDomain(jpa));
        }
        return families;
    }

    /**
     * Saves the family on repository.
     *
     * @param family family
     */
    public void saveNew(Family family) {
        familyRepositoryJPA.save(familyIAssemblerJPA.toJPA(family));
    }

    /**
     * Searches for the family with a given id.
     *
     * @param id id
     * @return Optional: returns a family or empty if the id does not exist.
     */
    @Override
    public Optional<Family> findByID(FamilyID id) {
        Family family = null;

        Optional<FamilyJPA> optionalFamilyJPA = familyRepositoryJPA.findById(id);

        if (optionalFamilyJPA.isPresent()) {
            FamilyJPA familyJPA = optionalFamilyJPA.get();
            family = familyJPAToDomain(familyJPA);
        }

        return Optional.ofNullable(family);
    }

    /**
     * Checks if familyId exists.
     *
     * @param id id
     * @return true if is present and false if not.
     */
    @Override
    public boolean existsID(FamilyID id) {

        return familyRepositoryJPA.existsById(id);
    }

    /**
     * Transforms FamilyJPA to the Entity Family
     *
     * @param familyJPA familyJpa
     * @return Family
     */
    private Family familyJPAToDomain(FamilyJPA familyJPA) {

        FamilyName familyName = familyIAssemblerJPA.toFamilyName(familyJPA.getFamilyName());
        RegistrationDate registrationDate = familyIAssemblerJPA.toRegistrationDate(familyJPA.getRegistrationDate());

        Family family = new Family.Builder(familyJPA.getFamilyID()).setName(familyName).
                setAdminId(familyJPA.getAdminEmail()).setRegistrationDate(registrationDate).build();

        List<FamilyRelationshipJPA> familyRelationshipJPAList = familyJPA.getFamilyRelationshipJPAList();

        for (int index = 0; index < familyRelationshipJPAList.size(); index++) {
            FamilyRelationshipJPA relationshipJPAIterated = familyRelationshipJPAList.get(index);

            PersonID personOneID = familyIAssemblerJPA.toPersonID(relationshipJPAIterated.getPersonOneID());
            PersonID personTwoID = familyIAssemblerJPA.toPersonID(relationshipJPAIterated.getPersonTwoID());
            FamilyRelationType familyRelationType = familyIAssemblerJPA.toFamilyRelationType(relationshipJPAIterated.getDesignation());
            FamilyRelationshipID familyRelationshipID = familyIAssemblerJPA.toFamilyRelationshipID(relationshipJPAIterated.getFamilyRelationshipID());

            family.createRelationship(personOneID, personTwoID, familyRelationType, familyRelationshipID);
        }

        return family;
    }

    @Override
    public boolean isAdmin(PersonID email) {
        List<FamilyJPA> families = familyRepositoryJPA.findAll();
        for (FamilyJPA famJPA : families) {
            if (famJPA.isAdmin(email.toString())) {
                return true;
            }
        }
        return false;
    }

}

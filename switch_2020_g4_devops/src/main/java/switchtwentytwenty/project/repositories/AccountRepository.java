package switchtwentytwenty.project.repositories;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.applicationservices.irepositories.IAccountRepository;
import switchtwentytwenty.project.domain.aggregates.account.*;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IAccountAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IAccountRepositoryJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@NoArgsConstructor
public class AccountRepository implements IAccountRepository {

    @Autowired
    private IAccountAssemblerJPA iAccountAssemblerJPA;

    @Autowired
    private IAccountRepositoryJPA iAccountRepositoryJPA;

    /**
     * Constructor for AccountRepository.
     *
     * @param iAccountAssemblerJPA  IAccountAssemblerJPA
     * @param iAccountRepositoryJPA IAccountRepositoryJPA
     */
    public AccountRepository(IAccountAssemblerJPA iAccountAssemblerJPA, IAccountRepositoryJPA iAccountRepositoryJPA) {
        this.iAccountAssemblerJPA = iAccountAssemblerJPA;
        this.iAccountRepositoryJPA = iAccountRepositoryJPA;
    }

    /**
     * Method that checks if a person already has a cash account.
     *
     * @param personID PersonID
     * @return boolean
     */
    @Override
    public boolean hasPersonACashAccount(PersonID personID) {

        List<AccountJPA> accounts = iAccountRepositoryJPA.findAllByPersonID(personID);

        for (AccountJPA accountJPA : accounts) {
            if (accountJPA.getAccountType().equals(EAccountType.PERSONAL_CASH)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method that checks if a family already has a cash account.
     *
     * @param adminId PersonID
     * @return boolean
     */
    @Override
    public boolean hasFamilyCashAccount(PersonID adminId) {
        List<AccountJPA> accountsJPA = iAccountRepositoryJPA.findAllByPersonID(adminId);
        for (AccountJPA account :
                accountsJPA) {
            if (account.getAccountType().equals(EAccountType.FAMILY_CASH))
                return true;
        }
        return false;
    }

    /**
     * Save the new account in AccountJPA
     * @param newAccount Account
     */
    @Override
    public void saveNew(Account newAccount) {
        AccountJPA accountJPA = iAccountAssemblerJPA.accountToAccountJPA(newAccount);
        iAccountRepositoryJPA.save(accountJPA);
    }

    /**
     * Method that returns a personal cash account by a person ID.
     *
     * @param id PersonID
     * @return Optional Account
     */
    @Override
    public Optional<Account> findCashByOwnerId(PersonID id) {
        List<AccountJPA> jpaList = iAccountRepositoryJPA.findAllByPersonID(id);
        for (AccountJPA jpa :
                jpaList) {
            if (jpa.getAccountType().equals(EAccountType.PERSONAL_CASH)) {
                Account account = toEntity(jpa);
                return Optional.of(account);
            }
        }
        return Optional.empty();
    }

    /**
     * Method that returns a family cash account by a person ID.
     *
     * @param personID PersonID
     * @return Optional Account
     */
    @Override
    public Optional<Account> findFamilyCashByOwnerId(PersonID personID) {
        List<AccountJPA> accountJPAS = iAccountRepositoryJPA.findAllByPersonID(personID);
        for (AccountJPA account : accountJPAS) {
            if (account.getAccountType().equals(EAccountType.FAMILY_CASH))
                return Optional.of(toEntity(account));
        }
        return Optional.empty();
    }

    /**
     * Checks if an account with a given accountID exists.
     *
     * @param id AccountID
     * @return boolean
     */
    @Override
    public boolean existsID(AccountID id) {
        return iAccountRepositoryJPA.existsById(id);
    }

    /**
     * Finds all accounts of an owner given the personID.
     *
     * @param personID PersonID
     * @return List Account
     */
    @Override
    public List<Account> findAllAccountsByOwner(PersonID personID) {
        List<AccountJPA> allAccountsJPA = iAccountRepositoryJPA.findAllByPersonID(personID);
        return assembleToAccountList(allAccountsJPA);
    }

    /**
     * Finds an account by its accountID.
     *
     * @param accountID AccountID
     * @return Optional Account
     */
    public Optional<Account> findByID(AccountID accountID) {
        Optional<AccountJPA> accountJPA = Optional.ofNullable(iAccountRepositoryJPA.findByAccountID(accountID));
        if (accountJPA.isPresent()) {
            return Optional.of(toEntity(accountJPA.get()));
        }
        return Optional.empty();
    }

    /**
     * Transforms an accountJPA into an account domain object.
     *
     * @param accountJPA AccountJPA
     * @return Account
     */
    public Account toEntity(AccountJPA accountJPA) {

        AccountID accountID = iAccountAssemblerJPA.getAccountIDFromAccountJPA(accountJPA);
        Description description = iAccountAssemblerJPA.getDescriptionFromAccountJPA(accountJPA);
        MoneyValue balance = iAccountAssemblerJPA.getBalanceFromAccountJPA(accountJPA);
        PersonID personID = iAccountAssemblerJPA.getPersonIDFromAccountJPA(accountJPA);
        EAccountType accountType = iAccountAssemblerJPA.getAccountTypeFromAccountJPA(accountJPA);

        if (accountType == EAccountType.CREDIT_CARD)
            return new CreditCardAccount.Builder(accountID).
                    setDescription(description).
                    setBalance(balance).
                    setOwnerID(personID).
                    setAccountType().build();

        if (accountJPA.getAccountType() == EAccountType.PERSONAL_CASH)
            return new PersonalCashAccount.Builder(accountID).
                    setDescription(description).
                    setBalance(balance).
                    setOwnerID(personID).
                    setAccountType().build();

        if (accountJPA.getAccountType() == EAccountType.BANK)
            return new BankAccount.Builder(accountID).
                    setDescription(description).
                    setBalance(balance).
                    setOwnerID(personID).
                    setAccountType().build();

        if (accountJPA.getAccountType() == EAccountType.BANK_SAVINGS)
            return new BankSavingsAccount.Builder(accountID).
                    setDescription(description).
                    setBalance(balance).
                    setOwnerID(personID).
                    setAccountType().build();

        if (accountJPA.getAccountType() == EAccountType.FAMILY_CASH)
            return new FamilyCashAccount.Builder(accountID).
                    setDescription(description).
                    setBalance(balance).
                    setOwnerID(personID).
                    setAccountType().build();

        throw new IllegalArgumentException("Wrong Data, non existent account type");

    }

    /**
     * Returns a list of account domain objects given a list of accountJPA objects.
     *
     * @param accountsJPA List AccountJPA
     * @return List Account
     */
    @Override
    public List<Account> assembleToAccountList(List<AccountJPA> accountsJPA) {
        List<Account> accountList = new ArrayList<>();
        for (AccountJPA accountJPA : accountsJPA) {
            accountList.add(toEntity(accountJPA));
        }
        return accountList;
    }

    /**
     * Get a family's or Member's cash account
     *
     * @param ownerId person id -email
     * @return account
     */
    @Override
    public Account getFamilyOrMemberCashAccount(PersonID ownerId) {
        List<AccountJPA> accountJPA = iAccountRepositoryJPA.findAllByPersonID(ownerId);
        for (AccountJPA account : accountJPA) {
            if (account.getAccountType().equals(EAccountType.PERSONAL_CASH)  || account.getAccountType().equals(EAccountType.FAMILY_CASH)) {
                return toEntity(account);
            }else{
                throw new IllegalArgumentException("This account is not a cash account.");
            }
        }
        throw new IllegalArgumentException("Account not found.");
    }

    @Override
    public List<Account> findAll(){
        List<AccountJPA> accountJPAList = iAccountRepositoryJPA.findAll();
        return assembleToAccountList(accountJPAList);
    }



}


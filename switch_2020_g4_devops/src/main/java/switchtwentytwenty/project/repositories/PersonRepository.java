package switchtwentytwenty.project.repositories;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IPersonIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Repository
public class PersonRepository implements IPersonRepository {


    @Autowired
    private IPersonIAssemblerJPA personAssemblerJPA;

    @Autowired
    private IPersonRepositoryJPA personRepositoryJPA;


    /**
     * Saves the person created
     *
     * @param person Person
     */
    @Override
    public void saveNew(Person person) {

        PersonJPA personJPA = personAssemblerJPA.toJPA(person);
        personRepositoryJPA.save(personJPA);
    }

    /**
     * Checks if two persons have the same family
     *
     * @param personOneId Person one
     * @param personTwoId Person two
     * @return true if they have and false if they belong to a different family.
     */
    @Override
    public boolean hasSameFamily(PersonID personOneId, PersonID personTwoId) {
        Person personOne = null;

        Optional<PersonJPA> personJPAOptional = personRepositoryJPA.findById(personOneId);
        if (personJPAOptional.isPresent()) {
            PersonJPA personJPA = personJPAOptional.get();
            personOne = toEntity(personJPA);
        }

        Person personTwo = null;

        Optional<PersonJPA> personJPAOptionalTwo = personRepositoryJPA.findById(personTwoId);
        if (personJPAOptional.isPresent()) {
            PersonJPA personJPA = personJPAOptional.get();
            personTwo = toEntity(personJPA);
        }

        return (personJPAOptional.isPresent() && personJPAOptionalTwo.isPresent()) && (personOne.hasSameFamily(personTwo));
    }

    /**
     * Searches for the Person with a given id.
     *
     * @param id id
     * @return Optional of Person
     */
    @Override
    public Optional<Person> findByID(PersonID id) {
        Person person = null;

        Optional<PersonJPA> personJPAOptional = personRepositoryJPA.findById(id);
        if (personJPAOptional.isPresent()) {
            PersonJPA personJPA = personJPAOptional.get();
            person = toEntity(personJPA);
        }

        return Optional.ofNullable(person);
    }

    /**
     * Transforms the personJpa to a Person entity.
     *
     * @param personJPA person JPA
     * @return Person
     */
    private Person toEntity(PersonJPA personJPA) {
        return new Person.Builder(personJPA.getPersonID())
                .setName(personAssemblerJPA.assemblePersonName(personJPA))
                .setAddress(personAssemblerJPA.assembleAddress(personJPA))
                .setBirthDate(personAssemblerJPA.assembleBirthDate(personJPA))
                .setFamilyId(personJPA.getFamilyID())
                .setVat(personAssemblerJPA.assembleVATNumber(personJPA))
                .setOtherEmails(personAssemblerJPA.assembleEmail(personJPA))
                .setRole(personJPA.getRole()).build();
    }

    /**
     * Converts a list of personJPA to a list of Person
     *
     * @param personJPAList List PersonJPA
     * @return List Person
     */
    public List<Person> assembleToPersonList(List<PersonJPA> personJPAList) {
        List<Person> personList = new ArrayList<>();
        for (PersonJPA personJPA : personJPAList) {
            personList.add(toEntity(personJPA));
        }
        return personList;
    }


    /**
     * Checks if a person id exists.
     *
     * @param id id
     * @return true if exists and false if not.
     */
    @Override
    public boolean existsID(PersonID id) {
        return personRepositoryJPA.existsById(id);
    }

    /**
     * Searches for all members of a family.
     *
     * @param familyID family Id.
     * @return a List of all members.
     */
    public List<Person> findAllByFamilyID(FamilyID familyID) {
        List<PersonJPA> personJPAList = personRepositoryJPA.findAllByFamilyID(familyID);
        return assembleToPersonList(personJPAList);
    }

    @Override
    public List<Person> findAll() {
        List<PersonJPA> personJPAList = personRepositoryJPA.findAll();
        return assembleToPersonList(personJPAList);
    }

    @Transactional
    public void deleteByPersonID(PersonID personID) {
        personRepositoryJPA.deleteByPersonID(personID);
    }

    @Override
    public String findFamilyByPersonId(String personId) {
        Optional<PersonJPA> personJPA = personRepositoryJPA.findById(new PersonID(new Email(personId)));
        if (personJPA.isPresent()) {
            return personJPA.get().getFamilyID().toString();
        }
        return "No family ID";
    }


}
package switchtwentytwenty.project.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.applicationservices.irepositories.ILedgerRepository;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.shared.ELedgerType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.Movement;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ILedgerIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.LedgerJPA;
import switchtwentytwenty.project.persistence.data.TransactionJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.ILedgerRepositoryJPA;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class LedgerRepository implements ILedgerRepository {

    @Autowired
    private final ILedgerRepositoryJPA ledgerRepositoryJPA;
    @Autowired
    private final ILedgerIAssemblerJPA ledgerAssemblerJPA;

    public LedgerRepository(ILedgerRepositoryJPA ledgerRepositoryJPA, ILedgerIAssemblerJPA ledgerAssemblerJPA) {
        this.ledgerRepositoryJPA = ledgerRepositoryJPA;
        this.ledgerAssemblerJPA = ledgerAssemblerJPA;
    }

    @Override
    public void saveNew(Ledger object) {
        LedgerJPA jpa = ledgerAssemblerJPA.toJPA(object);
        ledgerRepositoryJPA.save(jpa);
    }

    /**
     * @param jpa jpa
     * @return ledger
     */
    private Ledger toEntity(LedgerJPA jpa) {
        Ledger ledger = new Ledger(jpa.getLedgerID().getLedgerIdValue(), jpa.getPersonID().toString(),
                jpa.getLedgerType());
        List<TransactionJPA> transactionJPAList = jpa.getTransactionJPAList();
        for (TransactionJPA transactionJPA :
                transactionJPAList) {
            if (transactionJPA.getDestinationID() == null)
                ledger.addPayment(transactionJPA.getTransactionID(), transactionJPA.getAccountID(), new MoneyValue(transactionJPA.getAmount()), transactionJPA.getCategoryID(), new TransactionDate(transactionJPA.getTransactionDate()));
            else
                ledger.addTransfer(transactionJPA.getTransactionID(), transactionJPA.getAccountID(), transactionJPA.getDestinationID(), new MoneyValue(transactionJPA.getAmount()), transactionJPA.getCategoryID(), new TransactionDate(transactionJPA.getTransactionDate()));
        }
        return ledger;
    }

    @Override
    public Optional<Ledger> findByID(LedgerID id) {
        Optional<LedgerJPA> ledgerJPA = ledgerRepositoryJPA.findById(id);
        if (!ledgerJPA.isPresent())
            return Optional.empty();
        return Optional.of(toEntity(ledgerJPA.get()));
    }


    public List<Ledger> findAll() {
        Iterable<LedgerJPA> ledgerJPAList = ledgerRepositoryJPA.findAll();
        List<Ledger> ledgers = new ArrayList<>();
        for (LedgerJPA jpa :
                ledgerJPAList) {
            Ledger ledger = toEntity(jpa);
            ledgers.add(ledger);
        }
        return ledgers;
    }

    @Override
    public boolean existsID(LedgerID id) {
        return ledgerRepositoryJPA.existsById(id);
    }

    @Override
    public Optional<Ledger> findByOwnerID(PersonID ownerID) {
        List<LedgerJPA> ledgerJPA = ledgerRepositoryJPA.findByPersonID(ownerID);
        for (LedgerJPA ledger :
                ledgerJPA) {
            if (ledger.getLedgerType().equals(ELedgerType.PERSONAL)) {
                Ledger ledger1 = toEntity(ledger);
                return Optional.of(ledger1);
            }
        }
        return Optional.empty();

    }

    @Override
    public void addPayment(PersonID personID, TransactionID transactionID, AccountID id, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date) {
        Optional<Ledger> optionalLedger = findByOwnerID(personID);
        if(optionalLedger.isPresent()) {
            Ledger memberLedger = optionalLedger.get();
            LedgerJPA ledgerJPA = ledgerAssemblerJPA.toJPA(memberLedger);
            ledgerJPA.addPaymentVO(transactionID, id, moneyAmount.getValue(), categoryId, date);
            ledgerRepositoryJPA.save(ledgerJPA);
        }
    }

    @Override
    public void addTransferFromFamily(PersonID adminID, PersonID personID, TransactionID transactionID, AccountID id, AccountID destinationId, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date) {
        Optional<Ledger> optionalLedger = findByOwnerID(personID);
        Optional<Ledger> optionalLedgerFamily = findFamilyByOwnerID(adminID);
        if(optionalLedger.isPresent() && optionalLedgerFamily.isPresent()) {

            Ledger memberLedger = optionalLedger.get();
            Ledger familyLedger = optionalLedgerFamily.get();

            LedgerJPA ledgerJPA = ledgerAssemblerJPA.toJPA(memberLedger);
            LedgerJPA familyLedgerJPA = ledgerAssemblerJPA.toJPA(familyLedger);

            ledgerJPA.addTransfer(transactionID, id, destinationId, moneyAmount.getValue(), categoryId, date);
            familyLedgerJPA.addTransfer(transactionID, id, destinationId, moneyAmount.getValue(), categoryId, date);

            ledgerRepositoryJPA.save(ledgerJPA);
            ledgerRepositoryJPA.save(familyLedgerJPA);
        }
    }

    @Override
    public Optional<Ledger> findFamilyByOwnerID(PersonID personID) {
        List<LedgerJPA> ledgerJPA = ledgerRepositoryJPA.findByPersonID(personID);
        for (LedgerJPA ledger :
                ledgerJPA) {
            if (ledger.getLedgerType().equals(ELedgerType.FAMILY)) {
                Ledger ledgerEntity = toEntity(ledger);
                return Optional.of(ledgerEntity);
            }
        }
        return Optional.empty();
    }

    @Override
    public void addTransfer(PersonID personID, PersonID otherID, TransactionID transactionID, AccountID id, AccountID destinationId, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date) {
        Optional<Ledger> optionalLedger = findByOwnerID(personID);
        Optional<Ledger> optionalLedgerOther = findByOwnerID(otherID);
        if(optionalLedger.isPresent() && optionalLedgerOther.isPresent()) {
            Ledger memberLedger = optionalLedger.get();
            Ledger otherMemberLedger = optionalLedgerOther.get();

            LedgerJPA ledgerJPA = ledgerAssemblerJPA.toJPA(memberLedger);
            LedgerJPA otherLedgerJPA = ledgerAssemblerJPA.toJPA(otherMemberLedger);

            ledgerJPA.addTransfer(transactionID, id, destinationId, moneyAmount.getValue(), categoryId, date);
            otherLedgerJPA.addTransfer(transactionID, id, destinationId, moneyAmount.getValue(), categoryId, date);

            ledgerRepositoryJPA.save(ledgerJPA);
            ledgerRepositoryJPA.save(otherLedgerJPA);
        }
    }

    @Override
    public List<Movement> getAccountMovementsBetweenDates(LedgerID ledgerID, AccountID accountID,
                                                          TransactionDate startDate, TransactionDate endDate) {
        Optional<LedgerJPA> ledgerJPA = ledgerRepositoryJPA.findById(ledgerID);
        if(ledgerJPA.isPresent()) {
            Ledger ledger = toEntity(ledgerJPA.get());
            return ledger.getAccountMovementsBetweenDates(accountID, startDate, endDate);
        }
        return Collections.emptyList();
    }

    @Override
    public Transactionable findTransactionByID(TransactionID transactionID, LedgerID ledgerID) {
        Optional<LedgerJPA> ledgerJPA = ledgerRepositoryJPA.findById(ledgerID);
        if(ledgerJPA.isPresent()) {
            Ledger ledger = toEntity(ledgerJPA.get());
            return ledger.findTransactionByID(transactionID);
        }
        throw new NullPointerException("No ledger with that ID.");
    }

    /**
     * Method to find the possible transactions of a given movement
     * @param ledgerID ledger id
     * @param accountID account id
     * @param moneyValue amount of money in the movement
     * @return possible transactions DTO
     */
    @Override
    public List<Transactionable> findPossibleTransactionsByMovement(LedgerID ledgerID, AccountID accountID, MoneyValue moneyValue) {
        Optional<LedgerJPA> ledgerJPA = ledgerRepositoryJPA.findById(ledgerID);
        if(ledgerJPA.isPresent()) {
            Ledger ledger = toEntity(ledgerJPA.get());
            return ledger.findPossibleTransactionsByMovement(accountID, moneyValue);
        }
        throw new NullPointerException("No ledger with that ID.");
    }
}

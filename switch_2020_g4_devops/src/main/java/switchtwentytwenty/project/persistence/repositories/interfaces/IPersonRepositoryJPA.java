package switchtwentytwenty.project.persistence.repositories.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.PersonJPA;

import java.util.List;
import java.util.Optional;

@Repository
public interface IPersonRepositoryJPA extends CrudRepository<PersonJPA, PersonID> {

    Optional<PersonJPA> findById(PersonID personID);

    List<PersonJPA> findAll();

    List<PersonJPA> findAllByFamilyID(FamilyID familyID);

    void deleteByPersonID(PersonID personID);

  }

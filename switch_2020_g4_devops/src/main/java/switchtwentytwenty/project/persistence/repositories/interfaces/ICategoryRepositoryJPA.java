package switchtwentytwenty.project.persistence.repositories.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.data.CategoryJPA;

import java.util.List;
import java.util.Optional;

@Repository
public interface ICategoryRepositoryJPA extends CrudRepository<CategoryJPA, CategoryID> {
    Optional<CategoryJPA> findById(CategoryID categoryID);

    List<CategoryJPA> findAll();

    List<CategoryJPA> findByParentId(String parentId);

    List<CategoryJPA> findAllByFamilyId(FamilyID familyId);

}

package switchtwentytwenty.project.persistence.repositories.interfaces;

import org.springframework.data.repository.CrudRepository;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.data.FamilyJPA;

import java.util.List;

public interface IFamilyRepositoryJPA extends CrudRepository<FamilyJPA, FamilyID> {

    List<FamilyJPA> findAll();
}

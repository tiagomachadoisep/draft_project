package switchtwentytwenty.project.persistence.repositories.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.LedgerJPA;

import java.util.List;

@Repository
public interface ILedgerRepositoryJPA extends CrudRepository<LedgerJPA, LedgerID> {
    List<LedgerJPA> findByPersonID(PersonID personID);
}

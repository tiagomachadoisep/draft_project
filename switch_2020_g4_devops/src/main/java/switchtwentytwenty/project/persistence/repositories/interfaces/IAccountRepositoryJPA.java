package switchtwentytwenty.project.persistence.repositories.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.AccountJPA;

import java.util.List;

@Repository
public interface IAccountRepositoryJPA extends CrudRepository<AccountJPA, AccountID> {

    List<AccountJPA> findAllByPersonID(PersonID personID);

    AccountJPA findByAccountID(AccountID accountID);

    List<AccountJPA> findAll();

}

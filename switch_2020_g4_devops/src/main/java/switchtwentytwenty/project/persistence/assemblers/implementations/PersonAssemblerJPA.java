package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IPersonIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.EmailJPA;
import switchtwentytwenty.project.persistence.data.PersonJPA;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonAssemblerJPA implements IPersonIAssemblerJPA {
    @Override
    public PersonJPA toJPA(Person person) {
        //findPerson in repository
        PersonJPA personJPA = new PersonJPA();
        personJPA.setPersonID(person.getID());
        if(person.getAddress()!=null)
            personJPA.setAddress(person.getAddress().toString());
        personJPA.setFamilyID(person.getFamilyID());
        personJPA.setName(person.getName().toString());
        personJPA.setVat(person.getVatNumber().getVat());
        if(person.getBirthDate()!=null)
            personJPA.setBirthDate(person.getBirthDate().toString());
        List<Email> emails = person.getOtherEmails();
        List<EmailJPA> emailsJPA = new ArrayList<>();
        for (Email email : emails) {
            EmailJPA emailJPA = new EmailJPA(email.toString(), personJPA);
            emailsJPA.add(emailJPA);
        }
        personJPA.setOtherEmails(emailsJPA);
        personJPA.setRole(person.getRole());
        return personJPA;
    }

    @Override
    public PersonName assemblePersonName(PersonJPA personJpa) {
        return new PersonName(personJpa.getName());
    }

    @Override
    public VATNumber assembleVATNumber(PersonJPA personJpa) {
        return new VATNumber(personJpa.getVat());
    }

    @Override
    public Address assembleAddress(PersonJPA personJpa) {
        return new Address(personJpa.getAddress());
    }

    @Override
    public BirthDate assembleBirthDate(PersonJPA personJpa) {
        return new BirthDate(personJpa.getBirthDate());
    }

    @Override
    public List<Email> assembleEmail(PersonJPA personJpa) {
        List<Email> otherEmails = new ArrayList<>();
        for (EmailJPA email : personJpa.getOtherEmails()) {
                Email emails = new Email(email.getEmail());
                otherEmails.add(emails);
        }
        return otherEmails;
    }


}

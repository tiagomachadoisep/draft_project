package switchtwentytwenty.project.persistence.assemblers.interfaces;

import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.data.CategoryJPA;

public interface ICategoryAssemblerJPA extends IAssemblerJPA<Categorable, CategoryJPA> {
    CategoryName toCategoryName(String name);

    CategoryID toCategoryID(String id);

    FamilyID toFamilyID(String familyID);

}

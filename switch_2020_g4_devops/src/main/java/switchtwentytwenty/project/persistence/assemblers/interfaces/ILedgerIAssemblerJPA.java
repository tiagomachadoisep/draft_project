package switchtwentytwenty.project.persistence.assemblers.interfaces;

import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.persistence.data.LedgerJPA;

public interface ILedgerIAssemblerJPA extends IAssemblerJPA<Ledger, LedgerJPA> {
}

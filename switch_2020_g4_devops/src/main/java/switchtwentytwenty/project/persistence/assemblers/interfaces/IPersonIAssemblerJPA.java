package switchtwentytwenty.project.persistence.assemblers.interfaces;

import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.persistence.data.PersonJPA;

import java.util.List;

public interface IPersonIAssemblerJPA extends IAssemblerJPA<Person, PersonJPA> {

    PersonName assemblePersonName(PersonJPA personJpa);

    VATNumber assembleVATNumber(PersonJPA personJpa);

    Address assembleAddress(PersonJPA personJpa);

    BirthDate assembleBirthDate(PersonJPA personJpa);

    List<Email> assembleEmail(PersonJPA personJpa);
}

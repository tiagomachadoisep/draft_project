package switchtwentytwenty.project.persistence.assemblers.interfaces;

import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.FamilyJPA;

public interface IFamilyIAssemblerJPA extends IAssemblerJPA<Family, FamilyJPA> {

    FamilyName toFamilyName(String familyName);
    RegistrationDate toRegistrationDate(String date);
    PersonID toPersonID(String personEmail);
    FamilyRelationType toFamilyRelationType(String relationDesignation);
    FamilyRelationshipID toFamilyRelationshipID(String relationshipID);
}

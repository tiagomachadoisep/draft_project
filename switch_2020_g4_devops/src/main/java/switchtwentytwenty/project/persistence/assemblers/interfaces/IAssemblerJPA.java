package switchtwentytwenty.project.persistence.assemblers.interfaces;

import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.persistence.data.JPA;

public interface IAssemblerJPA<T extends AggregateRoot, K extends JPA> {
    K toJPA(T entity);
}

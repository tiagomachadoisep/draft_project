package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ICategoryAssemblerJPA;
import switchtwentytwenty.project.persistence.data.CategoryJPA;

@Service
public class CategoryAssemblerJPA implements ICategoryAssemblerJPA {

    /**
     * Transforms a standard or custom category in a CategoryJPA
     *
     * @param category domain object
     * @return data model (JPA)
     */
    @Override
    public CategoryJPA toJPA(Categorable category) {
        CategoryJPA categoryJPA = new CategoryJPA();
        categoryJPA.setId(category.getID());
        categoryJPA.setName(category.getName().toString());
        if (category.getFamilyId() != null) {
            categoryJPA.setFamilyId(new FamilyID(category.getFamilyId().toString()));
        }
        if (category.getParentId() != null) {
            categoryJPA.setParentCategory(category.getParentId().toString());
        }

        return categoryJPA;
    }

    @Override
    public CategoryName toCategoryName(String name) {
        return new CategoryName(name);
    }

    @Override
    public CategoryID toCategoryID(String id) {
        return new CategoryID(id);
    }

    @Override
    public FamilyID toFamilyID(String familyID) {
        return new FamilyID(familyID);
    }

}

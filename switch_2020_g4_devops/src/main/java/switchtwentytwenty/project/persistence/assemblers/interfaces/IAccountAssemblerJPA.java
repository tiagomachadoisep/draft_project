package switchtwentytwenty.project.persistence.assemblers.interfaces;

import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.AccountJPA;

public interface IAccountAssemblerJPA extends IAssemblerJPA<Account, AccountJPA> {

    /**
     * Returns the accountID from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return AccountID
     */
    AccountID getAccountIDFromAccountJPA(AccountJPA accountJPA);

    /**
     * Returns the personID from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return PersonID
     */
    PersonID getPersonIDFromAccountJPA(AccountJPA accountJPA);

    /**
     * Returns the description from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return Description
     */
    Description getDescriptionFromAccountJPA(AccountJPA accountJPA);

    /**
     * Returns the balance from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return MoneyValue
     */
    MoneyValue getBalanceFromAccountJPA(AccountJPA accountJPA);

    /**
     * Returns the account type from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return EAccountType
     */
    EAccountType getAccountTypeFromAccountJPA(AccountJPA accountJPA);


    /**
     * Transforms an account domain object into an account JPA object.
     *
     * @param account Account
     * @return AccountJPA
     */
    AccountJPA accountToAccountJPA(Account account);


}

package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.shared.ETransactionType;
import switchtwentytwenty.project.persistence.assemblers.interfaces.ILedgerIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.LedgerJPA;

import java.util.List;

@Service
public class LedgerAssemblerJPA implements ILedgerIAssemblerJPA {
    @Override
    public LedgerJPA toJPA(Ledger ledger) {
        LedgerJPA ledgerJPA = new LedgerJPA(ledger.getID().getLedgerIdValue(), ledger.getOwnerID().toString(),
                ledger.getLedgerType());
        List<Transactionable> transactionables = ledger.getTransactions();
        for (int transactionNumber = 0; transactionNumber< ledger.getTransactionCount(); transactionNumber++) {
            Transactionable transaction = transactionables.get(transactionNumber);
            if(transaction.getTransactionType().equals(ETransactionType.PAYMENT))
                ledgerJPA.addPaymentVO(transaction.getID(), transaction.getAccountID(), transaction.getAmount(), transaction.getCategoryId(), transaction.getTransactionDate()); //use date
            else
                ledgerJPA.addTransfer(transaction.getID(), transaction.getAccountID(), transaction.getDestinationId(), transaction.getAmount(), transaction.getCategoryId(), transaction.getTransactionDate());
        }
        return ledgerJPA;

    }
}

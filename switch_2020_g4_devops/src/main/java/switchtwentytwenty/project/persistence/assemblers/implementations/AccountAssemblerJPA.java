package switchtwentytwenty.project.persistence.assemblers.implementations;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IAccountAssemblerJPA;
import switchtwentytwenty.project.persistence.data.AccountJPA;

@Service
@NoArgsConstructor
public class AccountAssemblerJPA implements IAccountAssemblerJPA {

    /**
     * Returns the accountID from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return AccountID
     */
    public AccountID getAccountIDFromAccountJPA(AccountJPA accountJPA) {
        return accountJPA.getAccountID();
    }

    /**
     * Returns the personID from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return PersonID
     */
    public PersonID getPersonIDFromAccountJPA(AccountJPA accountJPA) {
        return accountJPA.getPersonID();
    }

    /**
     * Returns the description from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return Description
     */
    public Description getDescriptionFromAccountJPA(AccountJPA accountJPA) {
        return new Description(accountJPA.getDescription());
    }

    /**
     * Returns the balance from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return MoneyValue
     */
    @Override
    public MoneyValue getBalanceFromAccountJPA(AccountJPA accountJPA) {
        return new MoneyValue(accountJPA.getBalance());
    }

    /**
     * Returns the account type from an accountJPA.
     *
     * @param accountJPA AccountJPA
     * @return EAccountType
     */
    @Override
    public EAccountType getAccountTypeFromAccountJPA(AccountJPA accountJPA) {
        return accountJPA.getAccountType();
    }


    /**
     * Transforms an account domain object into an account JPA object.
     *
     * @param account Account
     * @return AccountJPA
     */
    public AccountJPA accountToAccountJPA(Account account) {
        AccountJPA accountJPA = new AccountJPA();
        accountJPA.setAccountID(account.getID());
        accountJPA.setPersonID(account.getOwnerID());
        accountJPA.setDescription(account.getDescription().toString());
        accountJPA.setBalance(account.getBalance().getValue());
        accountJPA.setAccountType(account.getAccountType());
        return accountJPA;

    }


    @Override
    public AccountJPA toJPA(Account entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Null account");
        }
        return accountToAccountJPA(entity);
    }
}

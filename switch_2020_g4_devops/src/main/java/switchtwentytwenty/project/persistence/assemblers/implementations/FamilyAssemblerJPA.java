package switchtwentytwenty.project.persistence.assemblers.implementations;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.family.FamilyRelationship;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.FamilyName;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.RegistrationDate;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.persistence.assemblers.interfaces.IFamilyIAssemblerJPA;
import switchtwentytwenty.project.persistence.data.FamilyJPA;

import java.util.List;

@Service
public class FamilyAssemblerJPA implements IFamilyIAssemblerJPA {
    @Override
    public FamilyJPA toJPA(Family entity) {

        if (entity == null) {
            throw new NullArgumentException("The passed object cannot be null.");
        }

        FamilyJPA familyJPA = new FamilyJPA();
        familyJPA.setFamilyID(entity.getID());
        familyJPA.setFamilyName(entity.getName());
        familyJPA.setAdminEmail(entity.getAdminMainEmail());
        familyJPA.setRegistrationDate(entity.getRegistrationDate().toString());

        List<FamilyRelationship> familyRelationships = entity.getFamilyRelationships();

        for (int index = 0; index < familyRelationships.size(); index++) {
            FamilyRelationship relationshipIterated = familyRelationships.get(index);

            String personOneID = relationshipIterated.getPersonOneID().toString();
            String personTwoID = relationshipIterated.getPersonTwoID().toString();
            String relationName = relationshipIterated.getRelationType().toString();
            String relationshipId = relationshipIterated.getID().toString();

            familyJPA.addFamilyRelationship(relationshipId, personOneID, personTwoID, relationName);

        }

        return familyJPA;
    }

    public FamilyName toFamilyName(String familyName) {
        return new FamilyName(familyName);
    }

    public RegistrationDate toRegistrationDate(String date) {
        return new RegistrationDate(date);
    }

    public PersonID toPersonID(String personEmail) {
        return new PersonID(new Email(personEmail));
    }

    public FamilyRelationType toFamilyRelationType(String relationDesignation) {
        return new FamilyRelationType(relationDesignation);
    }

    public FamilyRelationshipID toFamilyRelationshipID(String relationshipID) {
        return new FamilyRelationshipID(relationshipID);
    }


}

package switchtwentytwenty.project.persistence.data;

import switchtwentytwenty.project.domain.shared.ELedgerType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ledgers")
public class LedgerJPA implements JPA {
    @Id
    private LedgerID ledgerID;
    @Embedded
    private PersonID personID;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = /*"idJPA.ledgerJPA"*/ "ledgerJPA", cascade = CascadeType.ALL)
    private List<TransactionJPA> transactionJPAList = new ArrayList<>();
    @Enumerated(EnumType.STRING)
    private ELedgerType ledgerType = ELedgerType.PERSONAL;


    public LedgerJPA(int ledgerID, String personId) {
        this.ledgerID = new LedgerID(ledgerID);
        this.personID = new PersonID(new Email(personId));
        this.transactionJPAList = new ArrayList<>();
    }

    public LedgerJPA(LedgerID ledgerID, PersonID personID, List<TransactionJPA> transactionJPAList) {
        this.ledgerID = ledgerID;
        this.personID = personID;
        List<TransactionJPA> list = new ArrayList<>();
        list.addAll(transactionJPAList);
        this.transactionJPAList = list;
    }

    public LedgerJPA(int ledgerID, String personId, ELedgerType ledgerType) {
        this.ledgerID = new LedgerID(ledgerID);
        this.personID = new PersonID(new Email(personId));
        this.transactionJPAList = new ArrayList<>();
        this.ledgerType = ledgerType;
    }

    public LedgerJPA() {
    }

    public void addPaymentVO(TransactionID transactionID, AccountID accountID, double amount, CategoryID categoryID, TransactionDate transactionDate) {
        TransactionJPA transactionJPA = new TransactionJPA(this, transactionID, accountID, -amount, categoryID, transactionDate);
        transactionJPAList.add(transactionJPA);
    }

    public void addTransfer(TransactionID transactionID, AccountID accountID, AccountID destinationId, double amount, CategoryID categoryID, TransactionDate transactionDate) {
        TransactionJPA transferJPA = new TransactionJPA(this, transactionID, accountID, destinationId, amount, categoryID, transactionDate);
        transactionJPAList.add(transferJPA);
    }

    public void addPayment(String transactionID, String accountID, double amount, String categoryID, LocalDate date) {
        TransactionJPA transactionJPA = new TransactionJPA(this, transactionID, accountID, -amount, categoryID, new TransactionDate(date));
        transactionJPAList.add(transactionJPA);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LedgerJPA)) return false;
        LedgerJPA ledgerJPA = (LedgerJPA) o;
        return Objects.equals(ledgerID, ledgerJPA.ledgerID)&& this.transactionJPAList.size()==ledgerJPA.transactionJPAList.size();
    }

    @Override
    public int hashCode() {
        return Objects.hash(ledgerID);
    }

    public LedgerID getLedgerID() {
        return ledgerID;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public List<TransactionJPA> getTransactionJPAList() {
        return Collections.unmodifiableList(transactionJPAList);
    }

    public ELedgerType getLedgerType() {
        return ledgerType;
    }
}

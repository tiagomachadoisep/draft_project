package switchtwentytwenty.project.persistence.data;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import switchtwentytwenty.project.domain.shared.ERole;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "members")
public class PersonJPA implements JPA {
    @Id
    private PersonID personID;
    @Embedded
    private FamilyID familyID;
    private String name;
    private int vat;
    private String address;
    private String birthDate;
    private String encodedPassword;
    @Enumerated(EnumType.STRING)
    private ERole role;
    @OneToMany(mappedBy = "personJPA", cascade = CascadeType.ALL)
    private final List<PhoneNumberJPA> phoneNumbers = new ArrayList<>();
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "personJPA", cascade = CascadeType.ALL)
    private List<EmailJPA> otherEmails = new ArrayList<>();

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public PersonJPA() {
    }

    public PersonJPA(String personID, String familyID, String name, int vat, String address) {
        this.personID = new PersonID(new Email(personID));
        this.familyID = new FamilyID(familyID);
        this.name = name;
        this.vat = vat;
        this.address = address;
        this.role= ERole.USER;
        this.encodedPassword=new BCryptPasswordEncoder().encode(String.valueOf(vat));
    }

    public PersonJPA(String personID, String familyID, String name, int vat, String address, String birthDate) {
        this.personID = new PersonID(new Email(personID));
        this.familyID = new FamilyID(familyID);
        this.name = name;
        this.vat = vat;
        this.address = address;
        this.birthDate = birthDate;
        this.role = ERole.USER;
        this.encodedPassword=new BCryptPasswordEncoder().encode(String.valueOf(vat));
    }

    public PersonJPA(String personID, String familyID, String name, int vat, String address, String birthDate, ERole role) {
        this.personID = new PersonID(new Email(personID));
        this.familyID = new FamilyID(familyID);
        this.name = name;
        this.vat = vat;
        this.address = address;
        this.birthDate = birthDate;
        this.role = role;
        this.encodedPassword=new BCryptPasswordEncoder().encode(String.valueOf(vat));
    }


    //Getters and Setters

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public FamilyID getFamilyID() {
        return familyID;
    }

    public void setFamilyID(FamilyID familyID) {
        this.familyID = familyID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVat() {
        return vat;
    }

    public void setVat(int vat) {
        this.vat = vat;
        this.encodedPassword=new BCryptPasswordEncoder().encode(String.valueOf(vat));
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void addPhoneNumber(PhoneNumberJPA phoneNumber) {
        this.phoneNumbers.add(phoneNumber);
    }

    public void addEmail(String email) {
        this.otherEmails.add(new EmailJPA(email,this));
    }

    public List<EmailJPA> getOtherEmails() {
        return new ArrayList<>(otherEmails);
    }

    public void setOtherEmails(List<EmailJPA> otherEmails) {
        this.otherEmails = new ArrayList<>(otherEmails);
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public ERole getRole() {
        return role;
    }

    public void setRole(ERole role) {
        this.role = role;
    }
}

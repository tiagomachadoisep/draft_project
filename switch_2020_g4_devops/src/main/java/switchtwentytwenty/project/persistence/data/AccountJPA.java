package switchtwentytwenty.project.persistence.data;

import lombok.*;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Accounts")

public class AccountJPA implements JPA {

    @Id
    private AccountID accountID;
    @Embedded
    private PersonID personID;
    private String description;
    private double balance;
    @Enumerated(EnumType.STRING)
    private EAccountType accountType;

    /**
     * Overrides equals method for AccountJPA
     *
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountJPA that = (AccountJPA) o;
        return accountID.equals(that.accountID);
    }

    /**
     * Overrides hashCode method for AccountJPA
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(accountID);
    }
}

package switchtwentytwenty.project.persistence.data;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "categories")
public class CategoryJPA implements JPA {

    @Id
    private CategoryID id; //TODO: CATEGORYIDJPA
    private String name;
    private String parentId;
    @Embedded
    private FamilyID familyId;

    public CategoryID getId() {
        return id;
    }

    public void setId(CategoryID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentCategory(String parentCategoryId) {
        this.parentId = parentCategoryId;
    }

    public FamilyID getFamilyId() {
        return familyId;
    }

    public void setFamilyId(FamilyID familyId) {
        this.familyId = familyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryJPA that = (CategoryJPA) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, parentId, familyId);
    }
}

package switchtwentytwenty.project.persistence.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="families")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FamilyJPA implements JPA {
    @Id
    //@GeneratedValue
    private FamilyID familyID;
    @Embedded
    private PersonID adminEmail;
    private String familyName;
    private String registrationDate;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "familyRelationshipIdJPA.familyJPA", cascade = CascadeType.ALL)
    private List<FamilyRelationshipJPA> familyRelationshipJPAList = new ArrayList<>();

    public boolean addFamilyRelationship(String familyRelationshipID, String personOneID, String personTwoID,
                                         String designation){

        if (isObjectNull(personOneID) || isObjectNull(personTwoID) || isObjectNull(designation) || isObjectNull(familyRelationshipID)){
            throw new NullArgumentException("The passed object cannot be null");
        }

        FamilyRelationshipJPA familyRelationshipJPA = new FamilyRelationshipJPA(familyRelationshipID, this,
                personOneID, personTwoID, designation);

        return familyRelationshipJPAList.add(familyRelationshipJPA);
    }

    public FamilyJPA(FamilyID familyID, PersonID adminEmail, String familyName, String registrationDate) {
        this.familyID = familyID;
        this.adminEmail = adminEmail;
        this.familyName = familyName;
        this.registrationDate = registrationDate;
    }

    private boolean isObjectNull(Object o){
        return o == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyJPA familyJPA = (FamilyJPA) o;
        return familyID.equals(familyJPA.familyID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyID);
    }

    public boolean isAdmin (String email){
        PersonID personID = new PersonID(new Email(email));
        return personID.equals(this.adminEmail);
    }

    public FamilyID getID() {
        return familyID;
    }
}

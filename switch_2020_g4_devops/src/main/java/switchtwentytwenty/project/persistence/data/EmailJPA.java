package switchtwentytwenty.project.persistence.data;

import lombok.NoArgsConstructor;

import javax.persistence.*;


@NoArgsConstructor
@Entity
@Table(name = "emails")
public class EmailJPA implements JPA {

    @Id
    private String email;
    @ManyToOne
    @JoinColumn(name = "personID", nullable = false)
    private PersonJPA personJPA;

    public EmailJPA(String email) {
        this.email = email;
    }

    public EmailJPA(String email, PersonJPA personJPA) {
        this.email = email;
        this.personJPA = personJPA;
    }

    public String getEmail(){
        return this.email;
    }

}

package switchtwentytwenty.project.persistence.data;


import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "transactions")
public class TransactionJPA implements JPA {
    //@EmbeddedId
    private AccountID destinationID;
    private AccountID accountID;
    private double amount;
    private LocalDate transactionDate;
    @Embedded
    private CategoryID categoryID;

    @Id
    @Column(nullable = false)
    private TransactionID transactionID;
    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private LedgerJPA ledgerJPA;

    public TransactionJPA() {

    }


/*
    @Embeddable
    static class LedgerPaymentIdJPA implements Serializable {
        @ManyToOne
        @JoinColumn(name = "ledger_id")
        private LedgerJPA ledgerJPA;
        @Column(nullable = false, updatable = false)
        private TransactionID transactionID;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof LedgerPaymentIdJPA)) return false;
            LedgerPaymentIdJPA that = (LedgerPaymentIdJPA) o;
            return Objects.equals(transactionID, that.transactionID);
        }

        public LedgerPaymentIdJPA(LedgerJPA ledgerJPA, TransactionID transactionID) {
            this.ledgerJPA = ledgerJPA;
            this.transactionID = transactionID;
        }

        public TransactionID getTransactionID() {
            return transactionID;
        }

        public LedgerPaymentIdJPA() {
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }

        @Override
        public String toString() {
            return "transactionID=" + transactionID;
        }
    }

 */

    public AccountID getDestinationID() {
        return destinationID;
    }

    public TransactionID getTransactionID() {
        return transactionID;
    }

    public LedgerJPA getLedgerJPA() {
        return ledgerJPA;
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public double getAmount() {
        return amount;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public CategoryID getCategoryID() {
        return categoryID;
    }

    public TransactionJPA(LedgerJPA ledgerJPA, TransactionID transactionID, AccountID accountID, double amount, CategoryID categoryID, TransactionDate transactionDate){
        this.accountID=accountID;
        this.amount=amount;
        this.categoryID=categoryID;

        this.ledgerJPA=ledgerJPA;
        this.transactionID=transactionID;
        this.transactionDate = transactionDate.getTransactionDate();
    }

    public TransactionJPA(LedgerJPA ledgerJPA, String transactionID, String accountID, double amount, String categoryID, TransactionDate transactionDate){
        this.accountID = new AccountID(accountID);
        this.amount = amount;
        this.categoryID = new CategoryID(categoryID);
        this.ledgerJPA =ledgerJPA;
        this.transactionID = new TransactionID(transactionID);
        this.transactionDate = transactionDate.getTransactionDate();
    }

    public TransactionJPA(LedgerJPA ledgerJPA, TransactionID transactionID, AccountID accountID, AccountID destinationID, double amount, CategoryID categoryID,TransactionDate transactionDate){
        this.accountID=accountID;
        this.destinationID=destinationID;
        this.amount=amount;
        this.categoryID=categoryID;

        this.ledgerJPA=ledgerJPA;
        this.transactionID=transactionID;
        this.transactionDate = transactionDate.getTransactionDate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionJPA)) return false;
        TransactionJPA that = (TransactionJPA) o;
        return transactionID.equals(that.transactionID);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

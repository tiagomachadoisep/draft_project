package switchtwentytwenty.project.persistence.data;

import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "telephone")
@NoArgsConstructor

public class PhoneNumberJPA {
    @Id
    String number;
    @ManyToOne
    @JoinColumn(name = "personID")
    private PersonJPA personJPA;

    public PhoneNumberJPA(String number) {
        this.number = number;
    }

    //Setters and Getters
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PersonJPA getPersonJPA() {
        return personJPA;
    }

    public void setPersonJPA(PersonJPA personJPA) {
        this.personJPA = personJPA;
    }
}

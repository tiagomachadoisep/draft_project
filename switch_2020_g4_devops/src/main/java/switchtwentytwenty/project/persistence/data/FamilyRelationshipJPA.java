package switchtwentytwenty.project.persistence.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "relationships")
@NoArgsConstructor
public class FamilyRelationshipJPA {

    @EmbeddedId
    private FamilyRelationshipIdJPA familyRelationshipIdJPA;
    private String personOneID;
    private String personTwoID;
    private String designation;


    public FamilyRelationshipJPA(String familyRelationshipID, FamilyJPA familyJPA, String personOneID, String personTwoID,
                                 String designation) {

        validateArgumentsForConstruction(familyRelationshipID, familyJPA, personOneID, personTwoID, designation);

        this.familyRelationshipIdJPA = new FamilyRelationshipIdJPA(familyRelationshipID, familyJPA);
        this.personOneID = personOneID;
        this.personTwoID = personTwoID;
        this.designation = designation;
    }

    public String getFamilyRelationshipID() {
        return this.familyRelationshipIdJPA.getFamilyRelationshipID();
    }

    public String getPersonOneID() {
        return personOneID;
    }

    public String getPersonTwoID() {
        return personTwoID;
    }

    public String getDesignation() {
        return designation;
    }

    private void validateArgumentsForConstruction(String familyRelationshipID, FamilyJPA familyJPA, String personOneID,
                                                  String personTwoID, String designation) {

        validateFamilyRelationshipID(familyRelationshipID);
        validateFamilyJPA(familyJPA);
        validatePersonOneID(personOneID);
        validatePersonTwoID(personTwoID);
        validateDesignation(designation);
    }

    private void validateFamilyRelationshipID(String relationshipId) {
        if (relationshipId == null) {
            throw new NullArgumentException("The relationshipId cannot be null.");
        }

        if (relationshipId.trim().isEmpty()) {
            throw new EmptyArgumentException("The relationshipId cannot be empty");
        }
    }

    private void validatePersonOneID(String personOneID) {
        if (personOneID == null) {
            throw new NullArgumentException("The first person's ID cannot be null.");
        }

        if (personOneID.trim().isEmpty()) {
            throw new EmptyArgumentException("The first person's ID cannot be empty.");
        }
    }

    private void validatePersonTwoID(String personTwoID) {
        if (personTwoID == null) {
            throw new NullArgumentException("The second person's ID cannot be null.");
        }

        if (personTwoID.trim().isEmpty()) {
            throw new EmptyArgumentException("The second person's ID cannot be empty.");
        }
    }

    private void validateDesignation(String designation) {
        if (designation == null) {
            throw new NullArgumentException("The relationship designation cannot be null.");
        }

        if (designation.trim().isEmpty()) {
            throw new EmptyArgumentException("The relationship designation cannot be empty.");
        }
    }

    private void validateFamilyJPA(FamilyJPA familyJPA) {
        if (familyJPA == null) {
            throw new NullArgumentException("The familyJPA to which this relationship belongs cannot be null.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyRelationshipJPA that = (FamilyRelationshipJPA) o;
        return familyRelationshipIdJPA.equals(that.familyRelationshipIdJPA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyRelationshipIdJPA);
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Embeddable
    static class FamilyRelationshipIdJPA implements Serializable {
        private static final long serialVersionUID = 1769335947401492934L;

        @Getter
        String familyRelationshipID;

        @ManyToOne
        @JoinColumn(name = "family_id")
        FamilyJPA familyJPA;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FamilyRelationshipIdJPA that = (FamilyRelationshipIdJPA) o;
            return Objects.equals(familyRelationshipID, that.familyRelationshipID) && Objects.equals(familyJPA, that.familyJPA);
        }

        @Override
        public int hashCode() {
            return Objects.hash(familyRelationshipID, familyJPA);
        }
    }
}

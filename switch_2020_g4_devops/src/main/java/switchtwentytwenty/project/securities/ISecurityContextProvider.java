package switchtwentytwenty.project.securities;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;

@Component
public interface ISecurityContextProvider {

    SecurityContext getContext();

    void setContext(SecurityContext securityContext);
}

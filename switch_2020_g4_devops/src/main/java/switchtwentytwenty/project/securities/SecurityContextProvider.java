package switchtwentytwenty.project.securities;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityContextProvider implements  ISecurityContextProvider{
    @Override
    public SecurityContext getContext() {
        return SecurityContextHolder.getContext();
    }

    @Override
    public void setContext(SecurityContext securityContext) {
        SecurityContextHolder.setContext(securityContext);
    }
}

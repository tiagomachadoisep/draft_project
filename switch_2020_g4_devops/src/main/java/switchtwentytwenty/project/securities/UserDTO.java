package switchtwentytwenty.project.securities;

import java.util.*;

public class UserDTO {
    private final String username;
    private final List<String> roles;

    public UserDTO(String username, List<String> roles) {
        this.username = username;
        this.roles = new ArrayList<>(roles);
    }

    public String getUsername() {
        return username;
    }

    public List<String> getRoles() {
        return Collections.unmodifiableList(roles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDTO)) return false;
        UserDTO userDTO = (UserDTO) o;
        return username.equals(userDTO.username) && roles.equals(userDTO.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, roles);
    }
}

package switchtwentytwenty.project.securities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import javax.xml.bind.ValidationException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@RestController
public class UserController {


    @Autowired
    private final IPersonRepositoryJPA userInfoRepository;

    public UserController(IPersonRepositoryJPA userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }


    @PostMapping("/user")
    public Boolean create(@RequestBody Map<String, String> body) throws NoSuchAlgorithmException, ValidationException {
        String username = body.get("username");
        if (userInfoRepository.existsById(new PersonID(new Email(username)))){

            throw new ValidationException("Username already existed");

        }
        String fullname = body.get("fullname");
        userInfoRepository.save(new PersonJPA(username,"0",fullname,0, "rua sul","01-01-1900"));
        return true;
    }

}

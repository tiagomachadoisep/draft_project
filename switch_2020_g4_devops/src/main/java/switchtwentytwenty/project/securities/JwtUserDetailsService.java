package switchtwentytwenty.project.securities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.PersonJPA;
import switchtwentytwenty.project.persistence.repositories.interfaces.IPersonRepositoryJPA;

import java.util.Collections;

@Component
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private IPersonRepositoryJPA userInfoRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        PersonJPA user = userInfoRepository.findById(new PersonID(new Email(username))).orElse(null);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

        return new org.springframework.security.core.userdetails.User(user.getPersonID().toString(), user.getEncodedPassword(),
                Collections.singletonList(new SimpleGrantedAuthority(user.getRole().toString())));
    }
}

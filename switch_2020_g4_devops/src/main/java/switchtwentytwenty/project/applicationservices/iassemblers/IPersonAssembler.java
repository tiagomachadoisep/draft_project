package switchtwentytwenty.project.applicationservices.iassemblers;


import switchtwentytwenty.project.domain.shared.Address;
import switchtwentytwenty.project.domain.shared.BirthDate;
import switchtwentytwenty.project.domain.shared.PersonName;
import switchtwentytwenty.project.domain.shared.VATNumber;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.PersonDTO;

public interface IPersonAssembler extends Assembler {

    PersonName assemblePersonName(PersonDTO personDTO);

    FamilyID assembleFamilyID(PersonDTO personDTO);

    PersonID assemblePersonID(PersonDTO personDTO);

    VATNumber assembleVATNumber(PersonDTO personDTO);

    Address assembleAddress(PersonDTO personDTO);

    BirthDate assembleBirthDate(PersonDTO personDTO);

}

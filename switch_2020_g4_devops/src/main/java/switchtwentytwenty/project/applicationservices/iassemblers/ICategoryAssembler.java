package switchtwentytwenty.project.applicationservices.iassemblers;

import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.CategoryDTO;

public interface ICategoryAssembler extends Assembler {

    CategoryID assembleCategoryId();

    CategoryName assembleCategoryName(CategoryDTO dto);

    CategoryID assembleParentCategoryId(CategoryDTO dto);

    FamilyID assembleFamilyId(CategoryDTO dto);
}

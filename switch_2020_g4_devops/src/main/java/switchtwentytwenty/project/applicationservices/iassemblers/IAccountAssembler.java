package switchtwentytwenty.project.applicationservices.iassemblers;

import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;

public interface IAccountAssembler extends Assembler {

    /**
     * Creates value object AccountID from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return AccountID
     */
    AccountID getAccountIDFromAccountDTO(AccountDTO accountDTO);

    /**
     * Creates value object AccountID from an account DTO.
     *
     * @return AccountID
     */
    AccountID assembleAccountId();

    /**
     * Creates value object PersonID from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return PersonID
     */
    PersonID getPersonIDFromAccountDTO(AccountDTO accountDTO);

    /**
     * Creates value object Description from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return Description
     */
    Description getDescriptionFromAccountDTO(AccountDTO accountDTO);

    /**
     * Creates value object MoneyValue from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return MoneyValue
     */
    MoneyValue getBalanceFromAccountDTO(AccountDTO accountDTO);

    /**
     * Assembles a family cash account
     *
     * @param accountDTO AccountDTO
     * @return Account
     */
    Account assembleFamilyCashAccount(AccountDTO accountDTO);

    Account assembleCreditCardAccount(AccountDTO accountDTO);

}

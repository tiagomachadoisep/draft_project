package switchtwentytwenty.project.applicationservices.irepositories;

import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.persistence.data.AccountJPA;

import java.util.List;
import java.util.Optional;

public interface IAccountRepository extends IRepository<Account, AccountID> {

    /**
     * Method that checks if a person already has a cash account.
     *
     * @param personID PersonID
     * @return boolean
     */
    boolean hasPersonACashAccount(PersonID personID);

    /**
     * Method that checks if a family already has a cash account.
     *
     * @param personID PersonID
     * @return boolean
     */
    boolean hasFamilyCashAccount(PersonID personID);

    /**
     * Method that returns a personal cash account by a person ID.
     *
     * @param id PersonID
     * @return Optional Account
     */
    Optional<Account> findCashByOwnerId(PersonID id);


    /**
     * Checks if an account with a given accountID exists.
     *
     * @param accountID AccountID
     * @return boolean
     */
    boolean existsID(AccountID accountID);

    /**
     * Finds all accounts of an owner given the personID.
     *
     * @param personID PersonID
     * @return List Account
     */
    List<Account> findAllAccountsByOwner(PersonID personID);

    /**
     * Finds an account by its accountID.
     *
     * @param accountID AccountID
     * @return Optional Account
     */
    Optional<Account> findByID(AccountID accountID);

    /**
     * Method that returns a family cash account by a person ID.
     *
     * @param personID PersonID
     * @return Optional Account
     */
    Optional<Account> findFamilyCashByOwnerId(PersonID personID);

    /**
     * Returns a list of account domain objects given a list of accountJPA objects.
     *
     * @param accountsJPA List AccountJPA
     * @return List Account
     */
    List<Account> assembleToAccountList(List<AccountJPA> accountsJPA);

    /**
     * Get a family's or Member's cash account
     * @param ownerId person id -email
     * @return account
     */
    Account getFamilyOrMemberCashAccount(PersonID ownerId);

    List<Account> findAll();

//    List <Account> findByFamilyID(String familyID);
}

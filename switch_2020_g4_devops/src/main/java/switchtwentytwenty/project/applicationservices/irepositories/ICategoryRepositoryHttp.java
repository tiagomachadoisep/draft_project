package switchtwentytwenty.project.applicationservices.irepositories;

import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.List;


public interface ICategoryRepositoryHttp {

    List<CategoryDTO> importCategories();
}

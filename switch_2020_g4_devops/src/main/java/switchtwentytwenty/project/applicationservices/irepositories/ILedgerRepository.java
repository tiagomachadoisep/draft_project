package switchtwentytwenty.project.applicationservices.irepositories;

import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.Movement;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.*;

import java.util.List;
import java.util.Optional;

public interface ILedgerRepository extends IRepository<Ledger, LedgerID> {
    Optional<Ledger> findByOwnerID(PersonID personID);

    void addPayment(PersonID personID, TransactionID transactionID, AccountID id, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date);

    void addTransferFromFamily(PersonID adminID, PersonID personID, TransactionID transactionID, AccountID id, AccountID destinationId, MoneyValue moneyAmount, CategoryID categoryId, TransactionDate date);

    Optional<Ledger> findFamilyByOwnerID(PersonID personID);

    void addTransfer(PersonID otherID, PersonID personID, TransactionID transactionID, AccountID id, AccountID id1, MoneyValue moneyAmount, CategoryID categoryId,TransactionDate date);

    List<Movement> getAccountMovementsBetweenDates(LedgerID ledgerID, AccountID accountID,
                                                   TransactionDate startDate, TransactionDate endDate);

    Transactionable findTransactionByID(TransactionID transactionID, LedgerID ledgerID);

    List<Transactionable> findPossibleTransactionsByMovement(LedgerID ledgerID, AccountID accountID, MoneyValue amount);
}

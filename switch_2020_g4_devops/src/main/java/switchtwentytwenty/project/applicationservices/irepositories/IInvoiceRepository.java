package switchtwentytwenty.project.applicationservices.irepositories;

import switchtwentytwenty.project.domain.aggregates.invoice.Invoice;
import switchtwentytwenty.project.domain.shared.ids.InvoiceID;

public interface IInvoiceRepository extends IRepository<Invoice, InvoiceID>{
}

package switchtwentytwenty.project.applicationservices.irepositories;


import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;

import java.util.List;


public interface ICategoryRepository extends IRepository<Categorable, CategoryID> {


    /**
     * Returns all Standard Categories saved.
     *
     * @return a list of standard categories.
     */
    List<Categorable> findAll();

    /**
     * Checks if there is a category with same name at same level.
     * If there is, but categories are different (custom and standard) it is possible to add.
     *
     * @param parentId parent category
     * @param name     CategoryName
     * @param isStandard if its a standard category or custom
     * @return boolean
     */
    boolean existsCategory(String parentId, CategoryName name, boolean isStandard);

    /**
     * returns the standard categories tree
     *
     * @return List Categorable
     */
    List<Categorable> getStandardCategoryTree();

    /**
     * This method returns a category list of family
     *
     * @param familyId - Family id
     * @return categorable list
     */
    List<Categorable> getListOfCategoriesByFamilyId(FamilyID familyId);


}

package switchtwentytwenty.project.applicationservices.irepositories;

import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.List;

@Repository
public interface IPersonRepository extends IRepository<Person, PersonID> {

    boolean hasSameFamily(PersonID personOne, PersonID personTwo);

    List<Person> findAllByFamilyID(FamilyID familyID);

    List<Person> findAll();

    void deleteByPersonID(PersonID personID);

    String findFamilyByPersonId(String personId);
}

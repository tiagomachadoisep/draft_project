package switchtwentytwenty.project.applicationservices.irepositories;

import switchtwentytwenty.project.domain.shared.dddtypes.AggregateRoot;
import switchtwentytwenty.project.domain.shared.dddtypes.ID;

import java.util.Optional;

public interface IRepository<T extends AggregateRoot, K extends ID> {

    void saveNew(T entity);
    Optional<T> findByID(K id);
    boolean existsID(K id);

}

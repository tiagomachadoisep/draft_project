package switchtwentytwenty.project.applicationservices.irepositories;

import org.springframework.stereotype.Repository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;

import java.util.List;

@Repository
public interface IFamilyRepository extends IRepository<Family, FamilyID> {

    /**
     * Gets the family ID with corresponding admin main email
     *
     * @param email Email
     * @return FamilyID
     */
    FamilyID getFamilyIdByAdminMainEmail (Email email);

    List<Family> findAll();

    boolean isAdmin(PersonID email);
}

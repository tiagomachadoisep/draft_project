package switchtwentytwenty.project.applicationservices.implassemblers;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.PersonDTO;

@Service
public class PersonAssembler implements IPersonAssembler {

    @Override
    public PersonName assemblePersonName(PersonDTO personDTO) {
        return new PersonName(personDTO.getName());
    }

    @Override
    public FamilyID assembleFamilyID(PersonDTO personDTO) {
        return new FamilyID(personDTO.getFamilyID());
    }

    @Override
    public PersonID assemblePersonID(PersonDTO personDTO) {
        return new PersonID(new Email(personDTO.getEmailAddress()));
    }

    @Override
    public VATNumber assembleVATNumber(PersonDTO personDTO) {
        return new VATNumber(personDTO.getVatNumber());
    }

    @Override
    public Address assembleAddress(PersonDTO personDTO) {
        return new Address(personDTO.getAddress());
    }

    @Override
    public BirthDate assembleBirthDate(PersonDTO personDTO) {

        return new BirthDate(personDTO.getBirthDate());
    }
}

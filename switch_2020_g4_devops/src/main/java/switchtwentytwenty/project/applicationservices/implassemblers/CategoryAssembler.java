package switchtwentytwenty.project.applicationservices.implassemblers;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iassemblers.ICategoryAssembler;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.CategoryDTO;

@Service
public class CategoryAssembler implements ICategoryAssembler {

    public CategoryName assembleCategoryName(CategoryDTO dto) {
        return new CategoryName(dto.getName());
    }

    public CategoryID assembleCategoryId() {
        return new CategoryID();
    }

    public CategoryID assembleParentCategoryId(CategoryDTO dto) {
        return new CategoryID(dto.getParentId());
    }

    public FamilyID assembleFamilyId(CategoryDTO dto) {
        return new FamilyID(dto.getFamilyId());
    }
}

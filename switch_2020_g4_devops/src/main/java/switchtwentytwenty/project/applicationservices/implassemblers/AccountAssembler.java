package switchtwentytwenty.project.applicationservices.implassemblers;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iassemblers.IAccountAssembler;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.AccountBuilder;
import switchtwentytwenty.project.domain.aggregates.account.CreditCardAccount;
import switchtwentytwenty.project.domain.aggregates.account.FamilyCashAccount;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;

import java.lang.reflect.Constructor;

import static switchtwentytwenty.project.utils.StringUtils.toCamelCase;

@Service
public class AccountAssembler implements IAccountAssembler {

    /**
     * Creates value object AccountID from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return AccountID
     */
    @Override
    public AccountID getAccountIDFromAccountDTO(AccountDTO accountDTO) {
        return new AccountID(accountDTO.getAccountId());
    }

    /**
     * Creates value object PersonID from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return PersonID
     */
    @Override
    public PersonID getPersonIDFromAccountDTO(AccountDTO accountDTO) {
        return new PersonID(new Email(accountDTO.getOwnerId()));
    }

    /**
     * Creates value object Description from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return Description
     */
    @Override
    public Description getDescriptionFromAccountDTO(AccountDTO accountDTO) {
        return new Description(accountDTO.getDescription());
    }

    /**
     * Creates value object MoneyValue from an account DTO.
     *
     * @param accountDTO AccountDTO
     * @return MoneyValue
     */
    @Override
    public MoneyValue getBalanceFromAccountDTO(AccountDTO accountDTO) {
        return new MoneyValue(accountDTO.getBalance());
    }

    /**
     * Assembles a family cash account
     *
     * @param accountDTO AccountDTO
     * @return Account
     */
    @Override
    public Account assembleFamilyCashAccount(AccountDTO accountDTO) {
        AccountID accountID = new AccountID(accountDTO.getAccountId());
        PersonID personID = new PersonID(new Email(accountDTO.getOwnerId()));
        Description description = new Description(accountDTO.getDescription());
        MoneyValue balance = new MoneyValue(accountDTO.getBalance());

        return new FamilyCashAccount.Builder(accountID).setAccountType().setDescription(description).setBalance(balance).setOwnerID(personID).build();
    }


    /**
     * Assembles a credit card account.
     *
     * @param accountDTO AccountDTO
     * @return Account
     */
    public Account assembleCreditCardAccount(AccountDTO accountDTO) {
        AccountID accountID = new AccountID(accountDTO.getAccountId());
        PersonID personID = new PersonID(new Email(accountDTO.getOwnerId()));
        Description description = new Description(accountDTO.getDescription());
        MoneyValue balance = new MoneyValue(accountDTO.getBalance());

        return new CreditCardAccount.Builder(accountID).setAccountType().setDescription(description).setBalance(balance).setOwnerID(personID).build();
    }

    /**
     * Generates a new Id to the new account
     *
     * @return AccountID
     */
    public AccountID assembleAccountId() {
        return new AccountID();
    }


    public AccountBuilder assembleBuilder(AccountDTO dto) {
        try {
            String accountType = dto.getAccountType().toString() ;
            String accountTypeFormatted = toCamelCase(accountType)+"Account$Builder";
            Class<?> accountClass = Class.forName("switchtwentytwenty.project.domain.aggregates.account."+accountTypeFormatted);
            Constructor<?> classConstructor = accountClass.getConstructor(AccountID.class);
            Object builder=classConstructor.newInstance(new AccountID(dto.getAccountId()));
            return (AccountBuilder) builder;
        } catch (Exception e) {
            return null;
        }
    }

}

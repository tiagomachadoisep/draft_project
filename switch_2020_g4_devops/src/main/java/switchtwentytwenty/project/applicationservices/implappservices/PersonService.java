package switchtwentytwenty.project.applicationservices.implappservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.exceptions.NonExistentPersonException;
import switchtwentytwenty.project.utils.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService implements IPersonService {

    @Autowired
    private final IPersonRepository personRepository;

    @Autowired
    private final IPersonAssembler personAssembler;


    public PersonService(IPersonRepository personRepository, IPersonAssembler personAssembler) {
        this.personRepository = personRepository;
        this.personAssembler = personAssembler;

    }

    /**
     * Method to call PersonRepository to saveMember
     *
     * @param personDTO personDTO
     * @return PersonID
     */
    public PersonID addMember(PersonDTO personDTO) {

        PersonName name = personAssembler.assemblePersonName(personDTO);
        FamilyID familyID = personAssembler.assembleFamilyID(personDTO);
        PersonID personID = personAssembler.assemblePersonID(personDTO);
        VATNumber vatNumber = personAssembler.assembleVATNumber(personDTO);
        Address address = personAssembler.assembleAddress(personDTO);
        BirthDate birthDate= personAssembler.assembleBirthDate(personDTO);

        Person person = new Person.Builder(personID).setName(name)
                .setFamilyId(familyID).setVat(vatNumber).setAddress(address).setBirthDate(birthDate).setRoleAsUser().build();

        if (personRepository.existsID(person.getID())){
            throw new IllegalArgumentException("Member already registered");}

        personRepository.saveNew(person);

        return personID;

    }

    /**
     * This method verify if a member exist
     *
     * @param personID main email
     * @return boolean
     */
    @Override
    public boolean existsMember(PersonID personID) {
        return personRepository.existsID(personID);
    }

    /**
     * Method that checks if an email exists as the main email of a person in the person repository.
     *
     * @param personalCashAccountDTO PersonalCashAccountDTO
     * @return boolean
     */
    @Override
    public boolean checkIfEmailExists(AccountDTO personalCashAccountDTO) {
        String emailAddress = personalCashAccountDTO.getOwnerId();
        Email email = new Email(emailAddress);
        PersonID personID = new PersonID(email);
        return personRepository.existsID(personID);
    }

    /**
     * This method adds an email to member's profile
     *
     * @param emailDTO new email, main email
     * @return boolean
     */
    @Override
    public Result<String> addEmailToProfile(EmailDTO emailDTO) {
        try {
            Email mainEmail = new Email(emailDTO.getMainEmail());
            Email otherEmail = new Email(emailDTO.getEmail());
            if (mainEmail.equals(otherEmail)) {
                return Result.failure("This email can't be added, because it was defined as your main email.");
            }
            PersonID personID = new PersonID(mainEmail);
            Optional<Person> person = personRepository.findByID(personID);
            if (person.isPresent()) {
                Person personOne = person.get();
                if (!personOne.addEmailToProfile(otherEmail.toString())) {
                    return Result.failure("The email can't be added.");
                }
                personRepository.saveNew(personOne);
                return Result.success(emailDTO.getEmail());
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("The email can't be null or empty.");
        }
        return Result.failure("Failure. This email can't be added.");
    }

    /**
     * This method get a person id and return the personDTO
     *
     * @param id main email
     * @return PersonDTO
     */
    @Override
    public PersonDTO getPersonDTO(String id) {
        Optional<Person> person = personRepository.findByID(new PersonID(new Email(id)));
        if (person.isPresent()) {
            AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
            return assemblerToDTO.toPersonDTO(person.get());
        }
        throw new IllegalArgumentException("This person doesn't exist.");
    }


    /**
     * This method get a list of other emails of a member
     *
     * @param id main email
     * @return List of emails in String format
     */
    @Override
    public List<String> getOtherEmailsList(String id) {
        try {
            List<String> emailList = new ArrayList<>();
            Optional<Person> person = personRepository.findByID(new PersonID(new Email(id)));
            if (person.isPresent()) {
                Person personA = person.get();
                List<Email> emails = personA.getOtherEmails();
                for (Email email : emails) {
                    emailList.add(email.toString());
                }
            }
            return emailList;
        } catch (Exception e) {
            throw new IllegalArgumentException("This person is not in the app");
        }
    }

    /**
     * Searches and finds for every family's members.
     *
     * @param familyID family id
     * @return list of all members
     */
    public List<PersonDTO> getFamilyMembers(String familyID) {
        List<Person> membersList = personRepository.findAllByFamilyID(new FamilyID(familyID));

        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        return assemblerToDTO.toPersonDTOList(membersList);

    }

    @Override
    public List<PersonDTO> getAllMembers() {
        List<Person> personList = personRepository.findAll();

        AssemblerToDTO assemblerToDTO = new AssemblerToDTO();
        return assemblerToDTO.toPersonDTOList(personList);
    }

    /**
     * Removes secondary email
     *
     * @param emailDTO email
     * @return Retrieves a list of strings secondary emails
     */
    @Override
    public List<String> deleteSecondaryEmail (EmailDTO emailDTO){
        Email ownerID = new Email (emailDTO.getMainEmail());
        Email emailToRemove = new Email(emailDTO.getEmail());

        if (ownerID.equals(emailToRemove))
            throw new IllegalArgumentException("Cannot remove main email");

        PersonID personID = new PersonID(ownerID);
        Optional<Person> person = personRepository.findByID(personID);

        //Necessary?
        if(!person.isPresent())
             throw new NonExistentPersonException("Person does not exist");

        Person newPerson = person.get();
        newPerson.getOtherEmails().remove(emailToRemove);//remove 1st occurrence from the list if present returns true, else list is unchanged returns false

        personRepository.deleteByPersonID(personID);
        personRepository.saveNew(newPerson);

        List<String> emailsList = new ArrayList<>();

        for (Email email : newPerson.getOtherEmails()) {
            emailsList.add(email.toString());
        }

        return emailsList;
    }

    @Override
    public String getMemberFamilyById(String personId) {
        return personRepository.findFamilyByPersonId(personId);
    }

    @Override
    public String getMemberNameById(String personId) {
        Optional<Person> personJPA = personRepository.findByID(new PersonID(new Email(personId)));
        if (personJPA.isPresent()) {
            return personJPA.get().getName().toString();
        }
        return "Person does not exist";
    }
}

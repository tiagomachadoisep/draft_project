package switchtwentytwenty.project.applicationservices.implappservices;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.ICategoryService;
import switchtwentytwenty.project.applicationservices.iassemblers.ICategoryAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepository;
import switchtwentytwenty.project.applicationservices.irepositories.ICategoryRepositoryHttp;
import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.aggregates.category.CustomCategory;
import switchtwentytwenty.project.domain.aggregates.category.StandardCategory;
import switchtwentytwenty.project.domain.shared.CategoryName;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.CategoryDTO;
import switchtwentytwenty.project.exceptions.DuplicatedValueException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CategoryService implements ICategoryService {

    private static final String CATEGORY_EXISTS = "Category already exists";
    private static final String PARENTID_NOTFOUND = "Parent ID not found";
    private static final String CATEGORY_DOES_NOT_EXIST = "Category does not exist";

    @Autowired
    private final ICategoryRepository categoryRepository;
    @Autowired
    private final ICategoryAssembler assembler;
    @Autowired
    private final AssemblerToDTO assemblerToDTO;


    /**
     * Method that creates a standard category.
     *
     * @param categoryDTO CategoryDTO
     * @return String
     */
    @Override
    public CategoryDTO addStandardCategory(CategoryDTO categoryDTO) {
        CategoryDTO output;
        if (!existsCategory(categoryDTO.getName(), null, true)) {
            CategoryID id = assembler.assembleCategoryId();
            CategoryName name = assembler.assembleCategoryName(categoryDTO);
            Categorable newCategory = new StandardCategory.Builder(id).setName(name).setParent(null).build();
            categoryRepository.saveNew(newCategory);
            output = assemblerToDTO.toCategoryDTO(newCategory);
        } else {
            throw new DuplicatedValueException(CATEGORY_EXISTS);
        }
        return output;
    }


    /**
     * Method that creates a standard subcategory.
     *
     * @param categoryDTO CategoryDTO
     * @return CategoryDTO
     */
    @Override
    public CategoryDTO addStandardSubCategory(CategoryDTO categoryDTO) {
        CategoryDTO output;
        validateId(categoryDTO.getParentId());
        if (!existsCategory(categoryDTO.getName(), categoryDTO.getParentId(), true)) {
            CategoryID id = assembler.assembleCategoryId();
            CategoryName name = assembler.assembleCategoryName(categoryDTO);
            CategoryID parentId = assembler.assembleParentCategoryId(categoryDTO);
            Categorable newSubcategory = new StandardCategory.Builder(id).setName(name).setParent(parentId).build();
            categoryRepository.saveNew(newSubcategory);
            output = assemblerToDTO.toCategoryDTO(newSubcategory);
        } else {
            throw new DuplicatedValueException(CATEGORY_EXISTS);

        }
        return output;
    }

    /**
     * Method that creates a custom category.
     *
     * @param categoryDTO CategoryDTO
     * @return CategoryDTO
     */
    @Override
    public CategoryDTO addCustomCategory(CategoryDTO categoryDTO) {
        CategoryDTO output;
        if (!existsCategory(categoryDTO.getName(), null, false)) {
            CategoryID id = assembler.assembleCategoryId();
            CategoryName name = assembler.assembleCategoryName(categoryDTO);
            FamilyID familyID = assembler.assembleFamilyId(categoryDTO);
            Categorable newCategory =
                    new CustomCategory.Builder(id).setName(name).setParent(null).setFamily(familyID).build();
            categoryRepository.saveNew(newCategory);
            output = assemblerToDTO.toCategoryDTO(newCategory);
        } else {
            throw new DuplicatedValueException(CATEGORY_EXISTS);
        }

        return output;
    }

    /**
     * Method that checks if parent Id is present and then creates a custom subcategory.
     *
     * @param categoryDTO CategoryDTO
     * @return CategoryDTO
     */
    @Override
    public CategoryDTO checksParentIdAndAddsCustomSubcategory(CategoryDTO categoryDTO) {
        validateId(categoryDTO.getParentId());
        return addCustomSubCategory(categoryDTO);
    }

    /**
     * Method that creates a custom subcategory.
     *
     * @param categoryDTO CategoryDTO
     * @return CategoryDTO
     */
    @Override
    public CategoryDTO addCustomSubCategory(CategoryDTO categoryDTO) {
        CategoryDTO output;
        if (!existsCategory(categoryDTO.getName(), categoryDTO.getParentId(), false)) {
            CategoryID id = assembler.assembleCategoryId();
            CategoryName name = assembler.assembleCategoryName(categoryDTO);
            CategoryID parentId = assembler.assembleParentCategoryId(categoryDTO);
            FamilyID familyID = assembler.assembleFamilyId(categoryDTO);
            Categorable newSubcategory =
                    new CustomCategory.Builder(id).setName(name).setParent(parentId).setFamily(familyID).build();
            categoryRepository.saveNew(newSubcategory);
            output = assemblerToDTO.toCategoryDTO(newSubcategory);
        } else {
            throw new DuplicatedValueException(CATEGORY_EXISTS);

        }
        return output;
    }

    /**
     * Checks if Category ID exists
     *
     * @param parent ID
     */
    private void validateId(String parent) {
        if (!categoryRepository.existsID(new CategoryID(parent))) {
            throw new NoSuchElementException(PARENTID_NOTFOUND);
        }
    }

    /**
     * Checks if a category at same level has the same name.
     *
     * @param name   String
     * @param parent String
     * @return true if exists false otherwise
     */
    private boolean existsCategory(String name, String parent, boolean isStandard) {
        return categoryRepository.existsCategory(parent, new CategoryName(name), isStandard);
    }

    /**
     * Retrieves list of standard categoryDTO
     *
     * @return List categoryDTO
     */
    @Override
    public List<CategoryDTO> getStandardCategoryTree() {
        List<Categorable> categoryTree = categoryRepository.getStandardCategoryTree();

        return standardCategoryListToCategoryDTOList(categoryTree);
    }

    /**
     * Auxiliary method to get the standard category tree.
     *
     * @param categoryTree List<Categorable>
     * @return List<CategoryDTO>
     */
    private List<CategoryDTO> standardCategoryListToCategoryDTOList(List<Categorable> categoryTree) {
        List<CategoryDTO> categoryTreeDTO = new ArrayList<>();

        for (Categorable category : categoryTree) {
            CategoryDTO dto = assemblerToDTO.toCategoryDTO(category);
            categoryTreeDTO.add(dto);
        }
        return categoryTreeDTO;
    }

    /**
     * Gets Category By Id.
     *
     * @param categoryID CategoryId
     * @return Category DTO
     */
    public CategoryDTO getCategoryById(String categoryID) {
        Optional<Categorable> category = categoryRepository.findByID(new CategoryID(categoryID));
        if (category.isPresent()) {
            return assemblerToDTO.toCategoryDTO(category.get());
        } else {
            throw new NoSuchElementException(CATEGORY_DOES_NOT_EXIST);
        }
    }

    /**
     * Returns a category given its familyID and categoryID.
     *
     * @param familyId   String
     * @param categoryID String
     * @return CategoryDTO
     */
    public CategoryDTO getFamilyCategoryByCategoryId(String familyId, String categoryID) {
        CategoryDTO familyCategory = getCategoryById(categoryID);
        if (!familyCategory.getFamilyId().equals(familyId)) {
            throw new IllegalStateException("Category does not belong to this family");
        } else {
            return familyCategory;
        }
    }

    /**
     * This method return the list of family's categories
     *
     * @param familyId string family id
     * @return list of DTO categories
     */
    public List<CategoryDTO> getListOfCategories(String familyId) {
        FamilyID idFamily = new FamilyID(familyId);
        List<CategoryDTO> allCategories = new ArrayList<>();
        List<Categorable> categorableList = categoryRepository.getListOfCategoriesByFamilyId(idFamily);
        for (Categorable category :
                categorableList) {
            CategoryDTO dto = assemblerToDTO.toCategoryDTO(category);
            allCategories.add(dto);
        }
        return allCategories;
    }

    /**
     * import all categories from external API and the ones present on the app.
     *
     * @return List of Categories
     */
    public List<CategoryDTO> importAllCategories() {

        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("repositoryhttp.xml");
            ICategoryRepositoryHttp httpRepository = context.getBean("repositoryGroup",
                    ICategoryRepositoryHttp.class);

            List<CategoryDTO> standardCategoryTree = getStandardCategoryTree();
            List<CategoryDTO> externalCategories = httpRepository.importCategories();
            if (!externalCategories.isEmpty()) {
                standardCategoryTree.addAll(externalCategories);
            }
            return standardCategoryTree;
        } catch (Exception exception) {
            throw new IllegalStateException("Error");
        }

    }


}

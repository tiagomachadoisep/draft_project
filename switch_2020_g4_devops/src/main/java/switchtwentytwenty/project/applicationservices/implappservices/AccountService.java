package switchtwentytwenty.project.applicationservices.implappservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iassemblers.IAccountAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IAccountRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.account.*;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.Description;
import switchtwentytwenty.project.domain.shared.EAccountType;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.exceptions.FamilyAlreadyHasCashAccountException;
import switchtwentytwenty.project.exceptions.PersonAlreadyHasCashAccountException;

import java.util.*;


@Service
public class AccountService implements IAccountService {

    @Autowired
    private final IAccountRepository iAccountRepository;

    @Autowired
    private final IAccountAssembler iAccountAssembler;

    @Autowired
    private final IPersonRepository personRepository;

    @Autowired
    private final AssemblerToDTO assemblerToDTO;

    public AccountService(IAccountRepository iAccountRepository, IAccountAssembler iAccountAssembler, AssemblerToDTO
            assemblerToDTO, IPersonRepository personRepository) {
        this.iAccountRepository = iAccountRepository;
        this.iAccountAssembler = iAccountAssembler;
        this.assemblerToDTO = assemblerToDTO;
        this.personRepository = personRepository;

    }


    /**
     * Method that creates a personal cash account for a family member.
     *
     * @param personalCashAccountDTO PersonalCashAccountDTO
     * @return int
     */
    @Override
    public AccountDTO createPersonalCashAccount(AccountDTO personalCashAccountDTO) {
        String email = personalCashAccountDTO.getOwnerId();
        PersonID personID = new PersonID(new Email(email));

        if (iAccountRepository.hasPersonACashAccount(personID)) {
            throw new PersonAlreadyHasCashAccountException("Person already has a cash account.");
        }

        AccountID accountID = iAccountAssembler.assembleAccountId();
        Description description = iAccountAssembler.getDescriptionFromAccountDTO(personalCashAccountDTO);
        MoneyValue balance = iAccountAssembler.getBalanceFromAccountDTO(personalCashAccountDTO);

        Account personalCashAccount = new PersonalCashAccount.Builder(accountID).setAccountType().setDescription
                (description).setBalance(balance).setOwnerID(personID).build();

        iAccountRepository.saveNew(personalCashAccount);

        return assemblerToDTO.toPersonalCashAccountDTO(personalCashAccount);

    }

    /**
     * Creates and saves a new credit card account
     *
     * @param accountDTO accountDTO
     * @return output AccountDTO
     */
    @Override
    public AccountDTO addCreditCardAccount(AccountDTO accountDTO) {
        if (!accountDTO.getAccountType().equals(EAccountType.CREDIT_CARD)) {
            throw new IllegalArgumentException("Wrong account type");
        }

        AccountID accountID = iAccountAssembler.assembleAccountId();
        Description accountDescription = iAccountAssembler.getDescriptionFromAccountDTO(accountDTO);
        PersonID personID = iAccountAssembler.getPersonIDFromAccountDTO(accountDTO);
        MoneyValue balance = iAccountAssembler.getBalanceFromAccountDTO(accountDTO);
        Account newCreditCardAccount = new CreditCardAccount.Builder(accountID).setDescription(accountDescription)
                .setOwnerID(personID).setBalance(balance).setAccountType().build();
        iAccountRepository.saveNew(newCreditCardAccount);
        return assemblerToDTO.toAccountDTO(newCreditCardAccount);


    }

    /**
     * Creates and saves a new bank savings account
     *
     * @param accountDTO accountDTO
     * @return output AccountDTO
     */
    @Override
    public AccountDTO addBankSavingsAccount(AccountDTO accountDTO) {
     /*   if (!accountDTO.getAccountType().equals(EAccountType.BANK_SAVINGS)) {
            throw new IllegalArgumentException("Wrong account type");
        }*/
        AccountDTO output;
        AccountID accountID = iAccountAssembler.assembleAccountId();
        Description accountDescription = iAccountAssembler.getDescriptionFromAccountDTO(accountDTO);
        PersonID personID = iAccountAssembler.getPersonIDFromAccountDTO(accountDTO);
        MoneyValue balance = iAccountAssembler.getBalanceFromAccountDTO(accountDTO);
        Account newBankSavingsAccount = new BankSavingsAccount.Builder(accountID).setBalance(balance).setOwnerID(personID)
                .setDescription(accountDescription).setAccountType().build();
        iAccountRepository.saveNew(newBankSavingsAccount);
        output = assemblerToDTO.toAccountDTO(newBankSavingsAccount);
        return output;
    }

    /**
     * Creates and saves a new bank account
     *
     * @param accountDTO accountDTO
     * @return output AccountDTO
     */
    @Override
    public AccountDTO addBankAccount(AccountDTO accountDTO) {
      /*  if (!accountDTO.getAccountType().equals(EAccountType.BANK)) {
            throw new IllegalArgumentException("Wrong account type");
        }*/
        AccountDTO output;
        AccountID accountID = iAccountAssembler.assembleAccountId();
        Description accountDescription = iAccountAssembler.getDescriptionFromAccountDTO(accountDTO);
        PersonID personID = iAccountAssembler.getPersonIDFromAccountDTO(accountDTO);
        MoneyValue balance = iAccountAssembler.getBalanceFromAccountDTO(accountDTO);
        Account newBankAccount = new BankAccount.Builder(accountID).setBalance(balance).setOwnerID(personID)
                .setDescription(accountDescription).setAccountType().build();
        iAccountRepository.saveNew(newBankAccount);
        output = assemblerToDTO.toAccountDTO(newBankAccount);
        return output;
    }

    /**
     * Creates and saves a new family cash account
     *
     * @param familyCashAccountDTO accountDTO
     * @return true if account is created and false if not.
     */
    @Override
    public AccountDTO createFamilyCashAccount(AccountDTO familyCashAccountDTO) {
        if (iAccountRepository.hasFamilyCashAccount(new PersonID(new Email(familyCashAccountDTO.getOwnerId())))) {
            throw new FamilyAlreadyHasCashAccountException("Family already has a cash account");
        }

        AccountID accountID = new AccountID(UUID.randomUUID().toString());

        familyCashAccountDTO.setAccountId(accountID.getAccountIDValue()); // it is created by default with value -1
        Account familyCashAccount = iAccountAssembler.assembleFamilyCashAccount(familyCashAccountDTO);

        iAccountRepository.saveNew(familyCashAccount);

        return assemblerToDTO.toFamilyCashAccountDTO(familyCashAccountDTO.getOwnerId(), familyCashAccountDTO.getDescription(), familyCashAccountDTO.getBalance(), familyCashAccountDTO.getAccountId());

    }

    /**
     * Method that finds all account that belongs to a person.
     *
     * @param personID PersonID
     * @return accountDTO list
     */
    public List<AccountDTO> findAllAccountsByOwner(PersonID personID) {
        List<Account> accountList = iAccountRepository.findAllAccountsByOwner(personID);
        return assemblerToDTO.toAccountDTOList(accountList);
    }

    /**
     * Searches for the account and return its balance
     *
     * @param accountID accountID
     * @return MoneyValue
     */
    public MoneyValue checkAccountBalance(AccountID accountID) {
        Optional<Account> account = iAccountRepository.findByID(accountID);
        if (account.isPresent()) {
            return account.get().getBalance();
        } else {
            throw new IllegalArgumentException("Account doesn't exist");
        }
    }

    /**
     * Get an account DTO by providing its account ID.
     *
     * @param accountID String
     * @return Account DTO
     */
    @Override
    public Optional<AccountDTO> getAccountByID(String accountID) {
        Optional<Account> account = iAccountRepository.findByID(new AccountID(accountID));
        return account.map(assemblerToDTO::toAccountDTO);

    }

    public AccountDTO getFamilyOrMemberCashAccount(PersonID ownerID) {
        try {
            Account account = iAccountRepository.getFamilyOrMemberCashAccount(ownerID);
            return assemblerToDTO.getBalanceDTO(account);
        } catch (Exception e) {
            throw new IllegalArgumentException("The account doesn't exist.");
        }
    }

    /**
     * Searches for the owner cash account and returns its balance
     *
     * @param childId owner Id
     * @return AccountDTO
     */
    public AccountDTO checkChildsCashAccountBalance(String childId) {

        Optional<Account> childAccount = iAccountRepository.findCashByOwnerId(new PersonID(new Email(childId)));
        if (childAccount.isPresent()) {
            Account account = childAccount.get();
            return assemblerToDTO.getBalanceDTO(account);
        } else {
            throw new NoSuchElementException("Your child does not have cash account");
        }
    }

    @Override
    public List<AccountDTO> getAllAccounts() {
        List<Account> accountList = iAccountRepository.findAll();

        AssemblerToDTO newAssemblerToDTO = new AssemblerToDTO();
        return newAssemblerToDTO.toAccountDTOList(accountList);
    }

    @Override
    public List<AccountDTO> getFamilyAccounts(String familyID) {
        List<AccountDTO> accountDTOList = new ArrayList<>();
        AssemblerToDTO newAssemblerToDTO = new AssemblerToDTO();

        List<Person> personList = personRepository.findAllByFamilyID(new FamilyID(familyID));

        for (Person person : personList) {
            PersonID personID = person.getID();
            List<Account> accountList = iAccountRepository.findAllAccountsByOwner(personID);
            accountDTOList.addAll(newAssemblerToDTO.toAccountDTOList(accountList));
        }
        return accountDTOList;
    }


}

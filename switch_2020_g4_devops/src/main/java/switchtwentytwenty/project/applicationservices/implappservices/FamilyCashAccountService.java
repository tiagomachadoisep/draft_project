package switchtwentytwenty.project.applicationservices.implappservices;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyCashAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.dto.AccountDTO;

import java.util.Optional;

@Service
@AllArgsConstructor
public class FamilyCashAccountService implements IFamilyCashAccountService {

    @Qualifier("familyService")
    @Autowired
    private final IFamilyService iFamilyService;
    @Autowired
    private final IAccountService iAccountService;

    /**Method that creates a family cash account
     *
     * @param familyCashAccountDTO AccountDTO
     * @return accountDTO
     */
    @Override
    public AccountDTO createFamilyCashAccount(AccountDTO familyCashAccountDTO) {

            Email newEmail = new Email(familyCashAccountDTO.getOwnerId());

            iFamilyService.getFamilyIdByAdminMainEmail(newEmail);

            return iAccountService.createFamilyCashAccount(familyCashAccountDTO);

    }

    @Override
    public Optional<AccountDTO> getFamilyCashAccount(String accountId) {
        return iAccountService.getAccountByID(accountId);
    }
}

package switchtwentytwenty.project.applicationservices.implappservices;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IAccountService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonalCashAccountService;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.exceptions.NonExistentPersonException;

import java.util.Optional;

@Service
@AllArgsConstructor
public class PersonalCashAccountService implements IPersonalCashAccountService {

    @Autowired
    private final IPersonService iPersonService;
    @Autowired
    private final IAccountService iAccountService;

    /**
     * Method that creates a personal cash account for a family member.
     *
     * @param personalCashAccountDTO PersonalCashAccountDTO
     * @return int
     */
    @Override
    public AccountDTO createPersonalCashAccount(AccountDTO personalCashAccountDTO) {
        if (!iPersonService.checkIfEmailExists(personalCashAccountDTO)) {
            throw new NonExistentPersonException("Person does not exist.");
        }
        return iAccountService.createPersonalCashAccount(personalCashAccountDTO);
    }

    /**
     * Method that returns a personal cash account given its account ID.
     *
     * @param accountID String
     * @return AccountDTO
     */
    @Override
    public Optional<AccountDTO> getPersonalCashAccountByID(String accountID) {
        return iAccountService.getAccountByID(accountID);
    }
}

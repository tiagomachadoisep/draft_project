package switchtwentytwenty.project.applicationservices.implappservices;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyRelationshipService;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.family.FamilyRelationship;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.FamilyRelationType;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.ExceptionDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.exceptions.NullArgumentException;
import switchtwentytwenty.project.utils.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * The service responsible for dealing with family relationships. It catches exceptions thrown on the repositories
 * or domain and returns a {@linkplain Result} object to the controllers.
 */
@Service
@AllArgsConstructor
public class FamilyRelationshipService implements IFamilyRelationshipService {

    /**
     * The {@linkplain IPersonRepository} interface.
     */
    @Autowired
    private IPersonRepository personRepository;
    /**
     * The {@linkplain IFamilyRepository} interface.
     */
    @Autowired
    private IFamilyRepository familyRepository;
    /**
     * The {@linkplain AssemblerToDTO} assembler.
     */
    @Autowired
    private AssemblerToDTO assembler;

    /**
     * This methods issues the creation of a new family relationship to the {@linkplain Family} domain class after making use
     * of an {@linkplain AssemblerToDTO} to build value objects and communicating with the repositories
     * ({@linkplain IPersonRepository} and {@linkplain IFamilyRepository}) to validate data.
     * @param relationshipDTO The input Dto containing the relevant data necessary to create a new family relationship.
     * @return {@linkplain Result} object containing a Dto with either the information of the new relationship
     * created or the message of an exception if some was thrown and the relationship creation failed.
     */
    @Override
    public Result createAndSaveRelationship(RelationshipDTO relationshipDTO) {

        try {

            invalidateNullArgument(relationshipDTO);

            String personOneEmail = relationshipDTO.getFirstPersonEmail();
            String personTwoEmail = relationshipDTO.getSecondPersonEmail();
            String designation = relationshipDTO.getDesignation();
            String stringFamilyID = relationshipDTO.getFamilyId();

            FamilyID familyID = assembler.toFamilyID(stringFamilyID);
            PersonID personOneID = assembler.toPersonID(personOneEmail);
            PersonID personTwoID = assembler.toPersonID(personTwoEmail);
            FamilyRelationType familyRelationType = assembler.toFamilyRelationType(designation);

            invalidateSelfRelationship(personOneID, personTwoID);

            Family family = validateFamily(familyID);

            Optional<Person> optionalPersonOne = personRepository.findByID(personOneID);
            Optional<Person> optionalPersonTwo = personRepository.findByID(personTwoID);

            validatePersons(optionalPersonOne, optionalPersonTwo);

            FamilyRelationshipID relationshipID = assembler.createFamilyRelationshipID();

            family.createRelationship(personOneID, personTwoID, familyRelationType, relationshipID);

            familyRepository.saveNew(family);

            RelationshipDTO outputDto = assembler.toRelationshipDto(personOneEmail, personTwoEmail, stringFamilyID,
                    designation, relationshipID.getRelationshipId());

            return Result.success(outputDto);

        } catch (Exception e) {

            String responseMessage = "Relationship creation failed! " + e.getMessage();
            ExceptionDTO exceptionDTO = assembler.toExceptionDto(responseMessage);

            return Result.failure(exceptionDTO);
        }

    }

    /**
     * This method validates an input object. If it is null, a {@linkplain NullArgumentException} is thrown.
     * @param o The input object.
     */
    private void invalidateNullArgument(Object o) {
        if (o == null) {
            throw new NullArgumentException("The passed argument cannot be null.");
        }
    }

    /**
     * This method validates if two Persons are present in each Optional object. If one of them is not, a
     * {@linkplain NoSuchElementException} is thrown. If both are present, it is checked if both Persons belong to
     * the same Family. If not, an {@linkplain IllegalArgumentException} is thrown.
     * @param optionalPersonOne The first Optional passed as argument.
     * @param optionalPersonTwo The second Optional passed as argument.
     */
    private void validatePersons(Optional<Person> optionalPersonOne, Optional<Person> optionalPersonTwo) {

        if (!optionalPersonOne.isPresent() || !optionalPersonTwo.isPresent()) {
            throw new NoSuchElementException("The person(s) do not exist.");
        }

        Person personOne = optionalPersonOne.get();
        Person personTwo = optionalPersonTwo.get();

        boolean personsBelongToFamily = personOne.hasSameFamily(personTwo);

        if (!personsBelongToFamily) {
            throw new IllegalArgumentException("The persons do not belong to the same family.");
        }

    }

    /**
     * This method verifies if a Family exists in the database with the passed FamilyID. If the Family does not exist,
     * a {@linkplain NoSuchElementException} is thrown. If the Family exists, it is returned.
     * @param familyID The {@linkplain FamilyID} passed as argument.
     * @return {@linkplain Family} object.
     */
    private Family validateFamily(FamilyID familyID) {
        Optional<Family> optionalFamily = familyRepository.findByID(familyID);
        if (optionalFamily.isPresent()) {
            return optionalFamily.get();
        } else {
            throw new NoSuchElementException("A family with the passed ID does not exist.");
        }


    }

    /**
     * This method verifies if two PersonIDs are the same. If they are, an {@linkplain IllegalArgumentException} is
     * thrown.
     * @param personOneID The {@linkplain PersonID} of the first Person.
     * @param personTwoID The {@linkplain PersonID} of the second Person.
     */
    private void invalidateSelfRelationship(PersonID personOneID, PersonID personTwoID) {
        if (personOneID.equals(personTwoID)) {
            throw new IllegalArgumentException("A self-relationship is not possible.");
        }
    }

    /**
     * This method searches and returns a FamilyRelationship in a RelationshipDTO inside a {@linkplain Result} object.
     * If the FamilyRelationship with the passed relationship id does not exist or the input arguments are invalid,
     * the {@linkplain Result} object will contain the message of the exception thrown.
     * @param familyId String familyId.
     * @param relationshipId String relationshipId
     * @return {@linkplain Result} object.
     */
    @Override
    public Result getRelationshipDto(String familyId, String relationshipId) {

        try {

            FamilyID familyID = assembler.toFamilyID(familyId);
            FamilyRelationshipID familyRelationshipID = assembler.toFamilyRelationshipID(relationshipId);

            Family family = validateFamily(familyID);

            Optional<FamilyRelationship> optionalFamilyRelationship = family.findRelationshipById(familyRelationshipID);

            FamilyRelationship familyRelationship = validateFamilyRelationship(optionalFamilyRelationship);

            RelationshipDTO relationshipDTO = assembler.toRelationshipDto(familyRelationship, familyID);

            return Result.success(relationshipDTO);

        } catch (Exception e) {

            String responseMessage = "Could not get relationship! " + e.getMessage();
            ExceptionDTO exceptionDTO = assembler.toExceptionDto(responseMessage);

            return Result.failure(exceptionDTO);
        }
    }

    /**
     * This method verifies if a {@linkplain FamilyRelationship} exists inside and {@linkplain Optional} object.
     * If it exists, the FamilyRelationship is returned. If not, a {@linkplain NoSuchElementException} is thrown.
     * @param optionalFamilyRelationship The {@linkplain Optional} object.
     * @return {@linkplain FamilyRelationship} object.
     */
    private FamilyRelationship validateFamilyRelationship(Optional<FamilyRelationship> optionalFamilyRelationship) {
        if (!optionalFamilyRelationship.isPresent()) {
            throw new NoSuchElementException("There is no relationship with that id.");
        }

        return optionalFamilyRelationship.get();
    }

    /**
     * This method will change the designation of a {@linkplain FamilyRelationship} of an existing {@linkplain Family}.
     * If it is successful, a {@linkplain Result} object is returned containing a {@linkplain RelationshipDTO} with
     * the information of the relationship after the designation change. If it fails, the {@linkplain Result} object
     * will contain the message of the exception thrown.
     * @param relationshipDTO The {@linkplain RelationshipDTO} containing the familyId, the relationshipId and the new
     *                        designation.
     * @return {@linkplain Result} object.
     */
    @Override
    public Result changeRelationship(RelationshipDTO relationshipDTO) {

        try {

            invalidateNullArgument(relationshipDTO);

            FamilyID familyID = assembler.toFamilyID(relationshipDTO.getFamilyId());
            FamilyRelationshipID familyRelationshipID = assembler.toFamilyRelationshipID(relationshipDTO.getRelationshipId());
            FamilyRelationType newRelationType = assembler.toFamilyRelationType(relationshipDTO.getDesignation());

            Family family = validateFamily(familyID);

            family.changeRelationship(familyRelationshipID, newRelationType);

            familyRepository.saveNew(family);

            Optional<FamilyRelationship> optionalFamilyRelationship = family.findRelationshipById(familyRelationshipID);

            FamilyRelationship familyRelationship = validateFamilyRelationship(optionalFamilyRelationship);

            RelationshipDTO outputDto = assembler.toRelationshipDto(familyRelationship, familyID);

            return Result.success(outputDto);

        } catch (Exception e) {

            String responseMessage = "Could not change the relationship designation! " + e.getMessage();
            ExceptionDTO exceptionDTO = assembler.toExceptionDto(responseMessage);

            return Result.failure(exceptionDTO);
        }


    }

    /**
     * This method returns all relationships existing in a family.
     * @param familyId String with the family id of the family.
     * @return {@linkplain Result} object. Can contain a list of the family relationships of the family or the message
     * of a thrown exception.
     */
    @Override
    public Result getAllRelationshipsDto(String familyId) {
        try {
            FamilyID familyID = assembler.toFamilyID(familyId);
            Family family = validateFamily(familyID);

            List<FamilyRelationship> relationshipList = family.getFamilyRelationships();

            List<RelationshipDTO> relationshipDtoList = new ArrayList<>();

            for (FamilyRelationship relationship : relationshipList) {

                RelationshipDTO relationshipDto = assembler.toRelationshipDto(relationship, familyID);

                relationshipDtoList.add(relationshipDto);
            }

            return Result.success(relationshipDtoList);

        } catch (Exception e) {

            String responseMessage = "Could not get relationships! " + e.getMessage();
            ExceptionDTO exceptionDTO = assembler.toExceptionDto(responseMessage);

            return Result.failure(exceptionDTO);
        }
    }

    /**
     * Verifies if a certain PersonOne is parent of the PersonTwo of the relationship.
     *
     * @param relationship {@linkplain RelationshipDTO}
     * @return Boolean: true if is the parent and false if not.
     */
    public boolean checkIfIsChild(RelationshipDTO relationship) {
        FamilyID familyId = assembler.toFamilyID(relationship.getFamilyId());
        PersonID fatherId = assembler.toPersonID(relationship.getFirstPersonEmail());
        PersonID childId = assembler.toPersonID(relationship.getSecondPersonEmail());

        Family family = validateFamily(familyId);
        return family.isChild(fatherId, childId);
    }
}


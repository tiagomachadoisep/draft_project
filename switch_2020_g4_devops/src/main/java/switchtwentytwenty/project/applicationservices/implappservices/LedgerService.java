package switchtwentytwenty.project.applicationservices.implappservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.ILedgerService;
import switchtwentytwenty.project.applicationservices.irepositories.*;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.account.BankAccount;
import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.domainservices.ILedgerAccountService;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.Movement;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.*;
import switchtwentytwenty.project.dto.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LedgerService implements ILedgerService {

    private static final String EMAIL_NOT_REGISTERED = "Member doesn't exist";
    private static final String CATEGORY_NOT_REGISTERED = "Category doesn't exist";
    private static final String MEMBER_NO_CASH_ACCOUNT = "Member doesn't have cash account";
    private static final String FAMILY_NO_CASH_ACCOUNT = "Family doesn't have cash account";
    private static final String NO_LEDGER = "Ledger not created";
    private static final String NOT_IN_SAME_FAMILY = "Persons are not in the same family";
    private static final String NO_BALANCE = "Not enough balance";
    private static final String OTHER_ERROR = "An error has occurred";
    @Autowired
    private final ILedgerRepository ledgerRepository;
    @Autowired
    private final IAccountRepository accountRepository;
    @Autowired
    private final IPersonRepository personRepository;
    @Autowired
    private final ICategoryRepository categoryRepository;
    @Autowired
    private final IFamilyRepository familyRepository;
    @Autowired
    private final ILedgerAccountService ledgerAccountService; //Domain Service
    @Autowired
    private final AssemblerToDTO assemblerToDTO;

    public LedgerService(ILedgerRepository ledgerRepository, IAccountRepository accountRepository, IPersonRepository personRepository, ICategoryRepository categoryRepository,
                         IFamilyRepository familyRepository, ILedgerAccountService ledgerAccountService, AssemblerToDTO assemblerToDTO) {
        this.ledgerRepository = ledgerRepository;
        this.accountRepository = accountRepository;
        this.personRepository = personRepository;
        this.categoryRepository = categoryRepository;
        this.familyRepository = familyRepository;
        this.ledgerAccountService = ledgerAccountService;
        this.assemblerToDTO = assemblerToDTO;
    }

    @Override
    public TransferDTO transferCashFromFamilyToMember(TransferDTO transferDTO) {

        PersonID personID = assemblerToDTO.toPersonID(transferDTO.getMemberId());
        PersonID adminID = assemblerToDTO.toPersonID(transferDTO.getAdminId());
        CategoryID categoryId = assemblerToDTO.toCategoryId(transferDTO.getCategoryId());

        Optional<Account> optionalAccount = accountRepository.findCashByOwnerId(personID);
        Optional<Account> optionalAccountFamily = accountRepository.findFamilyCashByOwnerId(adminID);
        Optional<Ledger> optionalLedger = ledgerRepository.findByOwnerID(personID);
        Optional<Ledger> optionalLedgerFamily = ledgerRepository.findFamilyByOwnerID(adminID);

        String check = checkTransferConditions(personID, adminID, categoryId);
        if (check != null)
            return new TransferDTO(check);

        MoneyValue moneyAmount = new MoneyValue(transferDTO.getBalance());
        TransactionDate transactionDate = new TransactionDate(transferDTO.getTransactionDate());

        if (!optionalAccount.isPresent() || !optionalAccountFamily.isPresent() || !optionalLedgerFamily.isPresent() || !optionalLedger.isPresent())
            return new TransferDTO(OTHER_ERROR);

        Account memberAccount = optionalAccount.get();
        Account familyAccount = optionalAccountFamily.get();

        Ledger memberLedger = optionalLedger.get();
        Ledger familyLedger = optionalLedgerFamily.get();

        TransactionID transactionID = ledgerAccountService.addTransfer(familyLedger, memberLedger, familyAccount, memberAccount, moneyAmount, categoryId, transactionDate);

        if (transactionID.getId().equals("-1"))
            return new TransferDTO(NO_BALANCE);

        ledgerRepository.addTransferFromFamily(adminID, personID, transactionID, memberAccount.getID(), familyAccount.getID(), moneyAmount, categoryId, transactionDate);

        accountRepository.saveNew(memberAccount);
        accountRepository.saveNew(familyAccount);

        return new TransferDTO(transferDTO.getAdminId(), transferDTO.getMemberId(), transferDTO.getBalance(), transferDTO.getCategoryId(), familyAccount.getID().getAccountIDValue(), memberAccount.getID().getAccountIDValue(), transactionID.getId(), transferDTO.getTransactionDate());

    }



    private String checkTransferConditions(PersonID memberId, PersonID adminId, CategoryID categoryID) {
        if (!personRepository.findByID(memberId).isPresent())
            return EMAIL_NOT_REGISTERED;
        if (!categoryRepository.existsID(categoryID))
            return CATEGORY_NOT_REGISTERED;
        if (!accountRepository.findCashByOwnerId(memberId).isPresent())
            return MEMBER_NO_CASH_ACCOUNT;
        if (!accountRepository.findFamilyCashByOwnerId(adminId).isPresent())
            return FAMILY_NO_CASH_ACCOUNT;
        if (!ledgerRepository.findFamilyByOwnerID(adminId).isPresent() || !ledgerRepository.findByOwnerID(memberId).isPresent())
            return NO_LEDGER;
        if (!personRepository.hasSameFamily(memberId, adminId))
            return NOT_IN_SAME_FAMILY;
        return null;
    }


    @Override
    public PaymentDTO registerPaymentDS(PaymentDTO paymentDTO) {
        PersonID personID = new PersonID(new Email(paymentDTO.getMemberId()));
        CategoryID categoryId = new CategoryID(paymentDTO.getCategoryId());

        MoneyValue moneyAmount = new MoneyValue(paymentDTO.getBalance());
        TransactionDate date = new TransactionDate(paymentDTO.getTransactionDate());
        Account memberAccount = accountRepository.findCashByOwnerId(personID).orElse(new BankAccount.Builder(new AccountID("1")).build());

        String check = checkPaymentConditions(personID, categoryId);
        if (check != null)
            return new PaymentDTO(check);

        Ledger memberLedger = ledgerRepository.findByOwnerID(personID).orElse(null);
        TransactionID transactionID = ledgerAccountService.addPayment(memberLedger, memberAccount, moneyAmount, categoryId, date);

        if (transactionID.getId().equals("-1"))
            return new PaymentDTO(NO_BALANCE);

            ledgerRepository.addPayment(personID, transactionID, memberAccount.getID(), moneyAmount, categoryId, date);//updates entry in DB
            accountRepository.saveNew(memberAccount);

            return new PaymentDTO(paymentDTO.getMemberId(), paymentDTO.getBalance(), paymentDTO.getCategoryId(), transactionID.getId(), memberAccount.getID().getAccountIDValue(), date.getTransactionDate());


    }

    private String checkPaymentConditions(PersonID personID, CategoryID categoryID) {
        if (!categoryRepository.existsID(categoryID))
            return CATEGORY_NOT_REGISTERED;
        if (!accountRepository.findCashByOwnerId(personID).isPresent())
            return MEMBER_NO_CASH_ACCOUNT;
        if (!ledgerRepository.findByOwnerID(personID).isPresent())
            return NO_LEDGER;
        return null;
    }

    @Override
    public TransferDTO transferCash(TransferDTO transferDTO) {
        PersonID personID = new PersonID(new Email(transferDTO.getAdminId()));
        PersonID otherID = new PersonID(new Email(transferDTO.getMemberId()));
        CategoryID categoryId = new CategoryID(transferDTO.getCategoryId());

        String check = checkMemberTransferConditions(personID, otherID, categoryId);
        if (check != null)
            return new TransferDTO(check);

        MoneyValue moneyAmount = new MoneyValue(transferDTO.getBalance());
        TransactionDate date = new TransactionDate(transferDTO.getTransactionDate());
        Account memberAccount = accountRepository.findCashByOwnerId(personID).orElse(null);
        Account otherMemberAccount = accountRepository.findCashByOwnerId(otherID).orElse(null);

        Ledger memberLedger = ledgerRepository.findByOwnerID(personID).orElse(null);
        Ledger otherMemberLedger = ledgerRepository.findByOwnerID(otherID).orElse(null);

        TransactionID transactionID = ledgerAccountService.addTransfer(otherMemberLedger, memberLedger, memberAccount, otherMemberAccount, moneyAmount, categoryId, date);

        if (transactionID.getId().equals("-1"))
            return new TransferDTO(NO_BALANCE);

        if (memberAccount != null && otherMemberAccount != null) {
            ledgerRepository.addTransfer(otherID, personID, transactionID, memberAccount.getID(), otherMemberAccount.getID(), moneyAmount, categoryId, date);

            accountRepository.saveNew(memberAccount);
            accountRepository.saveNew(otherMemberAccount);


            return new TransferDTO(transferDTO.getAdminId(), transferDTO.getMemberId(), transferDTO.getBalance(), transferDTO.getCategoryId(), memberAccount.getID().getAccountIDValue(), otherMemberAccount.getID().getAccountIDValue(), transactionID.getId(), transferDTO.getTransactionDate());
        }
        return null;

    }

    private String checkMemberTransferConditions(PersonID senderID, PersonID receiverID, CategoryID categoryId) {
        if (!categoryRepository.existsID(categoryId))
            return CATEGORY_NOT_REGISTERED;
        if (!personRepository.hasSameFamily(senderID, receiverID))
            return NOT_IN_SAME_FAMILY;
        if (!accountRepository.findCashByOwnerId(senderID).isPresent() || !accountRepository.findCashByOwnerId(receiverID).isPresent())
            return MEMBER_NO_CASH_ACCOUNT;
        if (!ledgerRepository.findByOwnerID(senderID).isPresent() || !ledgerRepository.findByOwnerID(receiverID).isPresent())
            return NO_LEDGER;
        return null;
    }

    @Override
    public List<MovementDTO> getAccountMovementsBetweenDates(LedgerID ledgerID, AccountID accountID,
                                                             TransactionDate startDate, TransactionDate endDate) {
        List<Movement> movements = ledgerRepository.getAccountMovementsBetweenDates(ledgerID, accountID, startDate, endDate);
        return assemblerToDTO.toMovementDTOList(movements);
    }

    @Override
    public Optional<Ledger> findLedgerByOwnerID(PersonID personID) {
        return ledgerRepository.findByOwnerID(personID);
    }

    @Override
    public TransactionDTO findTransactionByID(TransactionID transactionID, LedgerID ledgerID) {
        Transactionable transaction = ledgerRepository.findTransactionByID(transactionID, ledgerID);
        if (transaction.isTransfer()) {
            return assemblerToDTO.toTransferDTO(transaction);
        } else {
            return assemblerToDTO.toPaymentDTO(transaction);
        }
    }

    /**
     * Method to find the possible transactions of a given movement
     * @param ledgerID ledger id
     * @param accountID account id
     * @param amount amount of money in the movement
     * @return possible transactions DTO
     */
    @Override
    public List<TransactionDTO> findPossibleTransactionsByMovement(LedgerID ledgerID, AccountID accountID, MoneyValue amount) {
        List<Transactionable> possibleTransactions = ledgerRepository.findPossibleTransactionsByMovement(ledgerID, accountID, amount);
        List<TransactionDTO> possibleTransactionsDTO = new ArrayList<>();
        for (Transactionable transaction : possibleTransactions) {
            if (transaction.isTransfer()) {
                possibleTransactionsDTO.add(assemblerToDTO.toTransferDTO(transaction));
            } else {
                possibleTransactionsDTO.add(assemblerToDTO.toPaymentDTO(transaction));
            }
        }
        return possibleTransactionsDTO;
    }
}

package switchtwentytwenty.project.applicationservices.implappservices;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IAuthenticationService;
import switchtwentytwenty.project.applicationservices.iappservices.IPersonService;
import switchtwentytwenty.project.securities.ISecurityContextProvider;

@Service
@AllArgsConstructor
public class AuthenticationService implements IAuthenticationService {

    @Autowired
    private final ISecurityContextProvider securityContextProvider;
    @Autowired
    private final IPersonService personService;

    @Override
    public boolean isCurrentUser(String mainEmail) {
        Authentication auth = securityContextProvider.getContext().getAuthentication();
        String username = auth.getName();
        return mainEmail.equals(username);
    }

    @Override
    public boolean isCurrentUserInFamily(String familyId) {
        Authentication auth = securityContextProvider.getContext().getAuthentication();
        String username = auth.getName();
        return personService.getMemberFamilyById(username).equals(familyId);
    }

    @Override
    public String getCurrentUser() {
        Authentication auth = securityContextProvider.getContext().getAuthentication();
        return auth.getName();
    }


}

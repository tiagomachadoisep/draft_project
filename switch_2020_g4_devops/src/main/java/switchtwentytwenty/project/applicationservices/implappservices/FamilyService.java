package switchtwentytwenty.project.applicationservices.implappservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchtwentytwenty.project.applicationservices.iappservices.IFamilyService;
import switchtwentytwenty.project.applicationservices.iassemblers.IPersonAssembler;
import switchtwentytwenty.project.applicationservices.implassemblers.PersonAssembler;
import switchtwentytwenty.project.applicationservices.irepositories.IFamilyRepository;
import switchtwentytwenty.project.applicationservices.irepositories.IPersonRepository;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.factories.FamilyFactory;
import switchtwentytwenty.project.domain.factories.IFamilyFactory;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AssemblerToDTO;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.PersonDTO;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FamilyService implements IFamilyService {

    @Autowired
    private final IPersonRepository personRepository;
    @Autowired
    private final IFamilyRepository familyRepository;
    @Autowired
    private final IPersonAssembler personAssembler;
    @Autowired
    private final AssemblerToDTO assemblerToDTO;
    @Autowired
    private final IFamilyFactory familyFactory;

    public FamilyService(IFamilyRepository familyRepository, IPersonRepository personRepository, AssemblerToDTO assembler) {
        this.familyRepository = familyRepository;
        this.personRepository = personRepository;
        this.personAssembler = new PersonAssembler();
        this.assemblerToDTO = assembler;
        this.familyFactory = new FamilyFactory();
    }


    /**
     * Creates and saves family and its administrator.
     *
     * @param familyDTO family data
     * @param personDTO admin data
     * @return FamilyDTO
     */
    @Override
    @Transactional(rollbackOn = Exception.class)
    public FamilyDTO createAndSaveFamily(FamilyDTO familyDTO, PersonDTO personDTO) {

        FamilyName familyName = assemblerToDTO.toFamilyName(familyDTO.getName());
        PersonID adminId = assemblerToDTO.toPersonID(familyDTO.getAdminEmail());
        VATNumber vat = assemblerToDTO.toVAT(personDTO.getVatNumber());
        PersonName adminName = assemblerToDTO.toPersonName(personDTO.getName());
        BirthDate birthDate = assemblerToDTO.toBirthDate(personDTO.getBirthDate());
        Address address = assemblerToDTO.toAddress(personDTO.getAddress());
        FamilyID familyID = assemblerToDTO.toFamilyID();

        List<Object> list = familyFactory.createFamilyAndAdmin(familyID,familyName,adminId,adminName,vat,birthDate, address);

        Person admin = (Person) list.get(0);
        Family family = (Family) list.get(1);

        if (personRepository.existsID(adminId))
            return null;

        personRepository.saveNew(admin);
        familyRepository.saveNew(family);
        return assemblerToDTO.toFamilyDTONew(family);
    }

    /**
     * Searches for familyID by administrator id.
     *
     * @param email admin id
     * @return family id
     */
    public FamilyID getFamilyIdByAdminMainEmail(Email email) {
        return familyRepository.getFamilyIdByAdminMainEmail(email);
    }

    /**
     * Finds family by id and returns the respective family.
     *
     * @param id family id
     * @return Family
     */
    @Override
    public FamilyDTO getFamilyById(String id) {
        Optional<Family> optional = familyRepository.findByID(new FamilyID(id));
        if (optional.isPresent()) {
            Family family = optional.get();
            return assemblerToDTO.toFamilyDTONew(family);
        }
        return new FamilyDTO();
    }

    /**
     * Searches and returns all families included in system
     *
     * @return List of Families.
     */
    @Override
    public List<FamilyDTO> getFamilies() {
        List<FamilyDTO> familyDTOList = new ArrayList<>();
        List<Family> families = familyRepository.findAll();
        for (Family family :
                families) {
            familyDTOList.add(assemblerToDTO.toFamilyDTONew(family));
        }
        return familyDTOList;
    }

}

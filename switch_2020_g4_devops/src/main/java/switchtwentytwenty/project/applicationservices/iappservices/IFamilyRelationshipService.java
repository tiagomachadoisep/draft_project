package switchtwentytwenty.project.applicationservices.iappservices;


import switchtwentytwenty.project.dto.RelationshipDTO;
import switchtwentytwenty.project.utils.Result;


public interface IFamilyRelationshipService {

    Result createAndSaveRelationship(RelationshipDTO relationshipDTO);

    Result getRelationshipDto(String familyId, String relationshipId);

    Result changeRelationship(RelationshipDTO relationshipDTO);

    Result getAllRelationshipsDto(String familyId);

    boolean checkIfIsChild(RelationshipDTO relationship);
}

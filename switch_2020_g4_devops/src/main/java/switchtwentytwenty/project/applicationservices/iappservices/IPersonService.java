package switchtwentytwenty.project.applicationservices.iappservices;

import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;
import switchtwentytwenty.project.dto.EmailDTO;
import switchtwentytwenty.project.dto.PersonDTO;
import switchtwentytwenty.project.utils.Result;

import java.util.List;

public interface IPersonService extends IService {

    /**
     * Method that checks if an email exists as the main email of a person in the person repository.
     *
     * @param personalCashAccountDTO PersonalCashAccountDTO
     * @return boolean
     */
    boolean checkIfEmailExists(AccountDTO personalCashAccountDTO);
    
    /**
     * This method add a new email in the member's list of emails
     * @param emailDTO new email, main email
     * @return boolean
     */
    Result<String> addEmailToProfile(EmailDTO emailDTO);

    /**
     * Removes secondary email
     *
     * @param emailDTO email to remove
     * @return Retrieves a list of strings secondary emails
     */
    List<String> deleteSecondaryEmail(EmailDTO emailDTO);

    PersonDTO getPersonDTO(String id);

    PersonID addMember(PersonDTO personDTO);


    boolean existsMember(PersonID personID);

    List<String> getOtherEmailsList(String id);

    List<PersonDTO> getFamilyMembers(String familyID);

    List<PersonDTO> getAllMembers();

    String getMemberFamilyById(String personId);

    String getMemberNameById(String personId);
}

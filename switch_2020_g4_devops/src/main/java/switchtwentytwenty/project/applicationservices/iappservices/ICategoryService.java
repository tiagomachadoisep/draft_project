package switchtwentytwenty.project.applicationservices.iappservices;

import switchtwentytwenty.project.dto.CategoryDTO;

import java.util.List;


public interface ICategoryService {
    CategoryDTO addStandardCategory(CategoryDTO categoryDto);

    CategoryDTO addStandardSubCategory(CategoryDTO categoryDTO);

    /**
     * Method that creates a custom category.
     *
     * @param categoryDTO CategoryDTO
     * @return String
     */
    CategoryDTO addCustomCategory(CategoryDTO categoryDTO);

    /**
     * Method that creates a custom subcategory.
     *
     * @param categoryDTO CategoryDTO
     * @return String
     */
    CategoryDTO addCustomSubCategory(CategoryDTO categoryDTO);

    /**
     * Method that checks if parent Id is present and then creates a custom subcategory.
     *
     * @param categoryDTO CategoryDTO
     * @return CategoryDTO
     */
    CategoryDTO checksParentIdAndAddsCustomSubcategory(CategoryDTO categoryDTO);

    /**
     * Retrieves list of standard categoryDTO
     *
     * @return List categoryDTO
     */
    List<CategoryDTO> getStandardCategoryTree();

    /**
     * Returns a category given its familyId and categoryID.
     *
     * @param familyId   String
     * @param categoryID String
     * @return CategoryDTO
     */
    CategoryDTO getFamilyCategoryByCategoryId(String familyId, String categoryID);

    CategoryDTO getCategoryById(String categoryID);

    /**
     * This method return the list of family's categories
     *
     * @param familyId string family id
     * @return list of DTO categories
     */
    List<CategoryDTO> getListOfCategories(String familyId);

    /**
     * Imports all external and the app categories available.
     *
     * @return List of Categories.
     */
    List<CategoryDTO> importAllCategories();
}

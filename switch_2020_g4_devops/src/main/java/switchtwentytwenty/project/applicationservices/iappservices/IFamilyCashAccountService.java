package switchtwentytwenty.project.applicationservices.iappservices;

import switchtwentytwenty.project.dto.AccountDTO;

import java.util.Optional;

public interface IFamilyCashAccountService extends IService {
    /**
     * Method that creates a family cash account
     *
     * @param familyCashAccountDTO AccountDTO
     * @return boolean
     */
    AccountDTO createFamilyCashAccount(AccountDTO familyCashAccountDTO);

    Optional<AccountDTO> getFamilyCashAccount(String accountId);

}

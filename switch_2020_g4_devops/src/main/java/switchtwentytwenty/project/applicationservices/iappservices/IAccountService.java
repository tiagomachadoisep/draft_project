package switchtwentytwenty.project.applicationservices.iappservices;

import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.dto.AccountDTO;

import java.util.List;
import java.util.Optional;


public interface IAccountService extends IService {

    /**
     * Method that creates a personal cash account for a family member.
     *
     * @param personalCashAccountDTO AccountDTO
     * @return int
     */
    AccountDTO createPersonalCashAccount(AccountDTO personalCashAccountDTO);

    AccountDTO addBankAccount(AccountDTO accountDTO);

    AccountDTO addBankSavingsAccount(AccountDTO accountDTO);

    AccountDTO addCreditCardAccount(AccountDTO accountDTO);

    AccountDTO createFamilyCashAccount(AccountDTO familyCashAccountDTO);

    List<AccountDTO> findAllAccountsByOwner(PersonID personID);

    MoneyValue checkAccountBalance(AccountID accountID);

    /**
     * Get an account DTO by providing its account ID.
     *
     * @param accountID String
     * @return Account DTO
     */
    Optional<AccountDTO> getAccountByID(String accountID);

    AccountDTO getFamilyOrMemberCashAccount(PersonID ownerID);

    AccountDTO checkChildsCashAccountBalance(String childId);

    List<AccountDTO> getAllAccounts();

    List<AccountDTO> getFamilyAccounts(String familyID);

}

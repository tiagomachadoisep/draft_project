package switchtwentytwenty.project.applicationservices.iappservices;

import switchtwentytwenty.project.domain.aggregates.ledger.Ledger;
import switchtwentytwenty.project.domain.shared.MoneyValue;
import switchtwentytwenty.project.domain.shared.TransactionDate;
import switchtwentytwenty.project.domain.shared.ids.AccountID;
import switchtwentytwenty.project.domain.shared.ids.LedgerID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.domain.shared.ids.TransactionID;
import switchtwentytwenty.project.dto.MovementDTO;
import switchtwentytwenty.project.dto.PaymentDTO;
import switchtwentytwenty.project.dto.TransactionDTO;
import switchtwentytwenty.project.dto.TransferDTO;

import java.util.List;
import java.util.Optional;

public interface ILedgerService extends IService{
    TransferDTO transferCashFromFamilyToMember(TransferDTO transferDTO);

    PaymentDTO registerPaymentDS(PaymentDTO paymentDTO);

    TransferDTO transferCash(TransferDTO transferDTO);

    List<MovementDTO> getAccountMovementsBetweenDates(LedgerID ledgerID, AccountID accountID,
                                                      TransactionDate startDate, TransactionDate endDate);

    Optional<Ledger> findLedgerByOwnerID(PersonID personID);

    TransactionDTO findTransactionByID(TransactionID transactionID, LedgerID ledgerID);

    List<TransactionDTO> findPossibleTransactionsByMovement(LedgerID ledgerID, AccountID accountID, MoneyValue amount);
}

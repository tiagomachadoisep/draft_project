package switchtwentytwenty.project.applicationservices.iappservices;

import switchtwentytwenty.project.dto.AccountDTO;

import java.util.Optional;

public interface IPersonalCashAccountService extends IService {

    /**
     * Method that creates a personal cash account for a family member.
     *
     * @param personalCashAccountDTO PersonalCashAccountDTO
     * @return int
     */
    AccountDTO createPersonalCashAccount(AccountDTO personalCashAccountDTO);

    /**
     * Method that returns a personal cash account given its account ID.
     *
     * @param accountID String
     * @return AccountDTO
     */
    Optional<AccountDTO> getPersonalCashAccountByID(String accountID);
}

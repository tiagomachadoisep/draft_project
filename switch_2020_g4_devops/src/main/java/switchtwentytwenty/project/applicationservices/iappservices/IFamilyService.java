package switchtwentytwenty.project.applicationservices.iappservices;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.shared.Email;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.PersonDTO;

import java.util.List;

@Service
public interface IFamilyService extends IService {

    FamilyDTO createAndSaveFamily(FamilyDTO familyDTO, PersonDTO personDTO);

    FamilyID getFamilyIdByAdminMainEmail(Email newEmail);

    FamilyDTO getFamilyById(String id);

    List<FamilyDTO> getFamilies();

}

package switchtwentytwenty.project.applicationservices.iappservices;

public interface IAuthenticationService extends IService{

    boolean isCurrentUser(String mainEmail);

    boolean isCurrentUserInFamily(String familyId);

    String getCurrentUser();
}

package switchtwentytwenty.project.dto;

import switchtwentytwenty.project.dto.visitor.Visitor;

public interface LinkAddableDTO {

    void accept(Visitor visitor);
}

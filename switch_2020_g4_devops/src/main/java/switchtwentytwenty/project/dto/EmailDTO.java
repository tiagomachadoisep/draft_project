package switchtwentytwenty.project.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmailDTO extends RepresentationModel<EmailDTO> implements DTO {
    @Getter
    public String email;
    @Getter
    public String mainEmail;
    @Setter
    public List<String> emailsList;

    public EmailDTO(String email, String mainEmail) {
        this.email = email;
        this.mainEmail = mainEmail;
        this.emailsList = new ArrayList<>();
    }

    public EmailDTO(String email) {
        this.email = email;
        this.emailsList = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        EmailDTO emailDTO = (EmailDTO) o;
        return email.equals(emailDTO.email) && mainEmail.equals(emailDTO.mainEmail) && emailsList.equals(emailDTO.emailsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, mainEmail, emailsList);
    }
}

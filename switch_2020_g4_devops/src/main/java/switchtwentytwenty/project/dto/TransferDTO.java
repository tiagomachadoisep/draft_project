package switchtwentytwenty.project.dto;

import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;

import java.time.LocalDate;
import java.util.Objects;

public class TransferDTO extends RepresentationModel<TransferDTO> implements DTO, TransactionDTO {
    private String accountId;
    private String destinationId;
    private String adminId;
    private String memberId;
    private final double balance;
    private String categoryId;
    private String transactionId;
    private LocalDate transactionDate;

    public String getAccountId() {
        return accountId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    @Override
    public String getTransactionID() {
        return transactionId;
    }

    public TransferDTO(String adminId, String memberId, double balance, String categoryId, LocalDate date) {
        this.adminId = adminId;
        this.memberId = memberId;
        this.balance = balance;
        this.categoryId = categoryId;
        this.transactionDate = date;
    }

    public TransferDTO(String transactionId){
        this.transactionId=transactionId;
        this.balance=0;
    }

    public TransferDTO(String adminId,String memberId,double balance, String categoryId, String accountId,String destinationId,String transactionId, LocalDate transactionDate){
        this.adminId = adminId;
        this.memberId = memberId;
        this.balance = balance;
        this.categoryId = categoryId;
        this.accountId=accountId;
        this.destinationId=destinationId;
        this.transactionId=transactionId;
        this.transactionDate = transactionDate;
    }

    public TransferDTO(Transactionable transfer) {
        this.accountId = transfer.getAccountID().toString();
        this.destinationId = transfer.getDestinationId().toString();
        this.transactionId = transfer.getID().toString();
        this.transactionDate = transfer.getTransactionDate().getTransactionDate();
        this.balance = transfer.getAmount();
    }

    public String  getAdminId() {
        return adminId;
    }

    public String getMemberId() {
        return memberId;
    }

    public double getBalance() {
        return balance;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransferDTO)) return false;
        if (!super.equals(o)) return false;
        TransferDTO that = (TransferDTO) o;
        return Objects.equals(transactionId, that.transactionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), transactionId);
    }

}

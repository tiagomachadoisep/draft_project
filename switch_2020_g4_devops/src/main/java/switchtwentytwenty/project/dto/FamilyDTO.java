package switchtwentytwenty.project.dto;

import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.dto.visitor.Visitor;


public class FamilyDTO extends RepresentationModel<FamilyDTO> implements DTO, LinkAddableDTO{
    private String name;
    private String adminEmail;
    private String familyId;
    private String adminName;
    private int adminVat;
    private String adminBirthDate;

    public FamilyDTO(){}

    public FamilyDTO(String name, String adminEmail) {
        this.name = name;
        this.adminEmail = adminEmail;
    }
    public FamilyDTO(String name, String adminEmail, String familyId) {
        this.name = name;
        this.adminEmail = adminEmail;
        this.familyId = familyId;
    }

    public FamilyDTO(String name, String adminEmail, String adminName, int adminVat, String adminBirthDate) {
        this.name = name;
        this.adminEmail = adminEmail;
        this.adminName = adminName;
        this.adminVat = adminVat;
        this.adminBirthDate = adminBirthDate;
    }

    public String getName() {
        return name;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setFamilyId(String value) {
        this.familyId=value;
    }

    public String getId() {
        return familyId;
    }

    public String getAdminName() {
        return adminName;
    }

    public int getAdminVat() {
        return adminVat;
    }

    public String getAdminBirthDate() {
        return adminBirthDate;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}

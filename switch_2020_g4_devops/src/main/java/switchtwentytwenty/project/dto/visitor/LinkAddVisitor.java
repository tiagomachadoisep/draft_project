package switchtwentytwenty.project.dto.visitor;

import org.springframework.hateoas.Link;
import switchtwentytwenty.project.controllers.implcontrollers.AddFamilyController;
import switchtwentytwenty.project.controllers.implcontrollers.CreateFamilyCashAccountController;
import switchtwentytwenty.project.controllers.implcontrollers.CreateFamilyRelationshipController;
import switchtwentytwenty.project.controllers.implcontrollers.GetProfileInfoController;
import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class LinkAddVisitor implements Visitor {


    @Override
    public void visit(FamilyDTO familyDTO) {
        Link selfLink = linkTo(methodOn(AddFamilyController.class).getFamily(familyDTO.getId())).withSelfRel();
        Link membersLink = linkTo(AddFamilyController.class).slash("families").slash(familyDTO.getId()).slash("members").withRel("members");
        Link adminLink = linkTo(methodOn(GetProfileInfoController.class).getProfileInfo(familyDTO.getAdminEmail())).withRel("admin");
        Link relationsLink = linkTo(methodOn(CreateFamilyRelationshipController.class).getAllRelationships(familyDTO.getId())).withRel("relations");
        Link accountLink = linkTo(CreateFamilyCashAccountController.class).slash("families").slash(familyDTO.getId()).slash("accounts").withRel("account");

        familyDTO.add(selfLink);
        familyDTO.add(membersLink);
        familyDTO.add(adminLink);
        familyDTO.add(relationsLink);
        familyDTO.add(accountLink);
    }

    @Override
    public void visit(RelationshipDTO controller) {

    }


}

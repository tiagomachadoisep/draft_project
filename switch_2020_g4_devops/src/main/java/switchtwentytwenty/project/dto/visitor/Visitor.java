package switchtwentytwenty.project.dto.visitor;

import switchtwentytwenty.project.dto.FamilyDTO;
import switchtwentytwenty.project.dto.RelationshipDTO;

public interface Visitor {

    void visit(FamilyDTO dto);
    void visit(RelationshipDTO dto);

}

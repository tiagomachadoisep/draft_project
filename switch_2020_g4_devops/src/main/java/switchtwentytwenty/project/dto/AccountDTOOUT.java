package switchtwentytwenty.project.dto;

import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.exceptions.EmptyArgumentException;

import java.util.Objects;

@Getter
public class AccountDTOOUT extends RepresentationModel<AccountDTOOUT> implements DTO {

    private final String description;

    public AccountDTOOUT(String description) {
        if (description == null) {
            throw new NullPointerException("Description can't be null");
        } else if (description.isEmpty()) {
            throw new EmptyArgumentException("Description can't be empty");
        } else
            this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AccountDTOOUT that = (AccountDTOOUT) o;

        return description.equals(that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), description);
    }
}

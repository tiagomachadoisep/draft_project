package switchtwentytwenty.project.dto;

import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;

import java.time.LocalDate;
import java.util.Objects;

public class PaymentDTO extends RepresentationModel<PaymentDTO> implements DTO, TransactionDTO {
    private final double balance;
    private String memberId;
    private String categoryId;
    private String transactionID;
    private String accountID;
    private LocalDate transactionDate;


    public PaymentDTO(String memberId, double balance, String categoryId) {
        this.memberId = memberId;
        this.balance = balance;
        this.categoryId = categoryId;
        this.transactionDate = LocalDate.now();
    }

    public PaymentDTO(String memberId, double balance, String categoryId, String transactionID, String accountID, LocalDate transactionDate) {
        this.memberId = memberId;
        this.balance = balance;
        this.categoryId = categoryId;
        this.transactionID = transactionID;
        this.accountID = accountID;
        this.transactionDate = transactionDate;
    }

    public PaymentDTO(Transactionable payment) {
        this.accountID = payment.getAccountID().toString();
        this.transactionID = payment.getID().toString();
        this.transactionDate = payment.getTransactionDate().getTransactionDate();
        this.balance = payment.getAmount();
    }

    public PaymentDTO(String error) {
        this.transactionID = error;
        this.balance = 0;
    }

    public String getMemberId() {
        return memberId;
    }

    public double getBalance() {
        return balance;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public String getAccountID() {
        return accountID;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentDTO)) return false;
        if (!super.equals(o)) return false;
        PaymentDTO that = (PaymentDTO) o;
        return Objects.equals(transactionID, that.transactionID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), transactionID);
    }
}

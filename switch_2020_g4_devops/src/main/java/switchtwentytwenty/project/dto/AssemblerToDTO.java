package switchtwentytwenty.project.dto;

import org.springframework.stereotype.Service;
import switchtwentytwenty.project.domain.aggregates.account.Account;
import switchtwentytwenty.project.domain.aggregates.category.Categorable;
import switchtwentytwenty.project.domain.aggregates.family.Family;
import switchtwentytwenty.project.domain.aggregates.family.FamilyRelationship;
import switchtwentytwenty.project.domain.aggregates.ledger.Transactionable;
import switchtwentytwenty.project.domain.aggregates.person.Person;
import switchtwentytwenty.project.domain.shared.*;
import switchtwentytwenty.project.domain.shared.ids.CategoryID;
import switchtwentytwenty.project.domain.shared.ids.FamilyID;
import switchtwentytwenty.project.domain.shared.ids.FamilyRelationshipID;
import switchtwentytwenty.project.domain.shared.ids.PersonID;
import switchtwentytwenty.project.exceptions.NullArgumentException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Service
public class AssemblerToDTO {

    public PersonDTO toPersonDTO(String name, String email, int vatNumber, String birthDate) {
        return new PersonDTO( email,name, vatNumber, birthDate);
    }

    public PersonDTO toPersonDTO(Person person) {
        return new PersonDTO(person.getFamilyID().getFamilyIdValue(), person.getMainEmail().toString(),
                person.getName().toString()
                , person.getVatNumber().getVat(), person.getAddress().toString(), person.getBirthDate().toString());
    }


    public FamilyDTO toFamilyDTO(String name, String adminEmail) {
        return new FamilyDTO(name, adminEmail);
    }

    public FamilyDTO toFamilyDTONew(Family fam) {
        return new FamilyDTO(fam.getName(), fam.getAdminMainEmail().toString(), fam.getID().getFamilyIdValue());
    }

    /**
     * Categorable transforms into CategoryDTO
     *
     * @param category Categorable
     * @return CategoryDTO
     */
    public CategoryDTO toCategoryDTO(Categorable category) {
        CategoryDTO newCategoryDTO = new CategoryDTO();
        newCategoryDTO.setName(category.getName().toString());
        newCategoryDTO.setId(category.getID().toString());
        if (category.getParentId() == null) {
            newCategoryDTO.setParentId(null);
        } else {
            newCategoryDTO.setParentId(category.getParentId().toString());
        }
        if (category.getFamilyId() == null) {
            newCategoryDTO.setFamilyId(null);
        } else {
            newCategoryDTO.setFamilyId(category.getFamilyId().toString());
        }
        return newCategoryDTO;
    }

    /**
     * Transforms external data to a proper categoryDTO.
     *
     * @param input external data
     * @return CategryDTO
     */
    public CategoryDTO inputToCategoryDTO(CategoryDTO input) {
        CategoryDTO dto = new CategoryDTO();
        dto.setName(input.getName());
        dto.setParentId(input.getParentId());
        return dto;
    }

    /**
     * Method that assembles an input custom category DTO with the familyID.
     *
     * @param familyId         String
     * @param inputCategoryDTO CategoryDTO
     * @return CategoryDTO
     */
    public CategoryDTO inputToCustomCategoryDTO(String familyId, CategoryDTO inputCategoryDTO) {
        inputCategoryDTO.setFamilyId(familyId);
        return inputCategoryDTO;
    }

    /**
     * Creates a personal cash account DTO from domain object.
     *
     * @param personalCashAccount Account
     * @return AccountDTO
     */
    public AccountDTO toPersonalCashAccountDTO(Account personalCashAccount) {

        return new AccountDTO(personalCashAccount.getOwnerID().toString(),
                personalCashAccount.getDescription().toString(), personalCashAccount.getBalance().getValue(),
                personalCashAccount.getID().toString(), EAccountType.PERSONAL_CASH);
    }

    /**
     * Creates an account DTO from the input parameters of the REST controller.
     *
     * @param id              String
     * @param inputAccountDTO AccountDTO
     * @return AccountDTO
     */
    public AccountDTO inputToAccountDTO(String id, AccountDTO inputAccountDTO) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setBalance(inputAccountDTO.getBalance());
        accountDTO.setOwnerId(id);
        accountDTO.setDescription(inputAccountDTO.getDescription());
        return accountDTO;
    }

    /**
     * Creates a personal cash account DTO from the input parameters of the REST controller.
     *
     * @param id                          String
     * @param inputPersonalCashAccountDTO AccountDTO
     * @return AccountDTO
     */
    public AccountDTO inputToPersonalCashAccountDTO(String id, AccountDTO inputPersonalCashAccountDTO) {
        if (id == null || inputPersonalCashAccountDTO == null || id.isEmpty()) {
            throw new IllegalArgumentException("Wrong input parameters");
        }
        inputPersonalCashAccountDTO.setOwnerId(id);
        return inputPersonalCashAccountDTO;
    }

    public AccountDTO toFamilyCashAccountDTO(String ownerId, String description, double balance, String accountId) {
        return new AccountDTO(ownerId, description, balance, accountId, EAccountType.FAMILY_CASH);
    }

    public RelationshipDTO toRelationshipDto(String firstPersonEmail, String secondPersonEmail, String familyId,
                                             String description) {
        RelationshipDTO aRelationshipDTO = new RelationshipDTO();
        aRelationshipDTO.setFamilyId(familyId);
        aRelationshipDTO.setDesignation(description);
        aRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        aRelationshipDTO.setSecondPersonEmail(secondPersonEmail);

        return aRelationshipDTO;
    }

    public RelationshipDTO toRelationshipDto(String firstPersonEmail, String secondPersonEmail, String familyId,
                                             String description, String relationshipId) {
        RelationshipDTO aRelationshipDTO = new RelationshipDTO();
        aRelationshipDTO.setFamilyId(familyId);
        aRelationshipDTO.setDesignation(description);
        aRelationshipDTO.setFirstPersonEmail(firstPersonEmail);
        aRelationshipDTO.setSecondPersonEmail(secondPersonEmail);
        aRelationshipDTO.setRelationshipId(relationshipId);

        return aRelationshipDTO;
    }

    public RelationshipDTO toRelationshipDto(FamilyRelationship familyRelationship, FamilyID familyID) {

        if (familyRelationship == null) {
            throw new NullArgumentException("The passed relationship cannot be null.");
        }

        if (familyID == null) {
            throw new NullArgumentException("The passed family id cannot be null.");
        }

        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFirstPersonEmail(familyRelationship.getPersonOneID().toString());
        relationshipDTO.setSecondPersonEmail(familyRelationship.getPersonTwoID().toString());
        relationshipDTO.setDesignation(familyRelationship.getRelationType().toString());
        relationshipDTO.setRelationshipId(familyRelationship.getID().getRelationshipId());
        relationshipDTO.setFamilyId(familyID.toString());

        return relationshipDTO;
    }

    public RelationshipDTO inputToRelationshipDTO(String familyId, String relationId, String newDesignation) {
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyId);
        relationshipDTO.setRelationshipId(relationId);
        relationshipDTO.setDesignation(newDesignation);

        return relationshipDTO;
    }

    /**
     * Transforms all parameters into RelationshipDTO
     *
     * @param familyId  familyId
     * @param personOne personOne email
     * @param personTwo personTwo email
     * @return RelatonshipDTO
     */
    public RelationshipDTO inputSearchForRelationshipDTO(String familyId, String personOne, String personTwo) {
        RelationshipDTO relationshipDTO = new RelationshipDTO();
        relationshipDTO.setFamilyId(familyId);
        relationshipDTO.setFirstPersonEmail(personOne);
        relationshipDTO.setSecondPersonEmail(personTwo);
        return relationshipDTO;
    }

    public AccountDTO toAccountDTO(String ownerID, String description, EAccountType accountType) {
        return new AccountDTO(ownerID, description, accountType);
    }

    /**
     * Get only the balance family or member account
     *
     * @param account account
     * @return accountdto- balance
     */
    public AccountDTO getBalanceDTO(Account account) {
        return new AccountDTO(account.getBalance().getValue());
    }

    public AccountDTO toAccountDTO(Account account) {
        return new AccountDTO(account.getOwnerID().toString(), account.getDescription().toString(),
                account.getBalance().getValue(), account.getID().getAccountIDValue(), account.getAccountType());
    }

    /**
     * This method transform Strings in EmailDTO objects
     *
     * @param secondEmail member secondary email
     * @param mainEmail   member main email
     * @return EmailDTO object
     */
    public EmailDTO toEmailDTO(String secondEmail, String mainEmail) {
        return new EmailDTO(secondEmail, mainEmail);
    }

    public EmailDTO toEmailDTO(String secondEmail){
        return new EmailDTO(secondEmail);
    }

    public TransferDTO toTransferDTO(String familyId, String memberEmail, double amount, String categoryId,
                                     LocalDate date) {
        return new TransferDTO(familyId, memberEmail, amount, categoryId, date);
    }

    public TransferDTO toTransferDTO(Transactionable transfer) {
        return new TransferDTO(transfer);
    }

    public PaymentDTO toPaymentDTO(String memberEmail, double amount, String categoryId) {
        return new PaymentDTO(memberEmail, amount, categoryId);
    }

    public PaymentDTO toPaymentDTO(Transactionable payment) {
        return new PaymentDTO(payment);
    }

    public List<AccountDTO> toAccountDTOList(List<Account> accountList) {
        List<AccountDTO> accountDTOList = new ArrayList<>();
        for (Account account : accountList) {
            accountDTOList.add(toAccountDTO(account));
        }
        return accountDTOList;
    }

    public AccountDTOOUT toAccountDtoOut(AccountDTO accountDTO) {
        return new AccountDTOOUT(accountDTO.getDescription());
    }

    public PersonID toPersonID(String personEmail) {
        return new PersonID(new Email(personEmail));
    }

    public FamilyID toFamilyID(String id) {
        return new FamilyID(id);
    }

    public FamilyID toFamilyID() {
        return new FamilyID();
    }

    public FamilyName toFamilyName(String name) {
        return new FamilyName(name);
    }

    public PersonName toPersonName(String name) {
        return new PersonName(name);
    }

    public VATNumber toVAT(int number) {
        return new VATNumber(number);
    }

    public BirthDate toBirthDate(String date) {
        return new BirthDate(date);
    }

    public CategoryID toCategoryId(String id) {
        return new CategoryID(id);
    }

    public FamilyRelationshipID toFamilyRelationshipID(String relationId) {
        return new FamilyRelationshipID(relationId);
    }

    public FamilyRelationshipID createFamilyRelationshipID() {
        return new FamilyRelationshipID();
    }

    public FamilyRelationType toFamilyRelationType(String relationDesignation) {
        return new FamilyRelationType(relationDesignation);
    }

    public MovementDTO toMovementDTO(Movement movement) {
        return new MovementDTO(movement.getAccountId().toString(), movement.getValue().getValue());
    }

    public List<MovementDTO> toMovementDTOList(List<Movement> movementList) {
        List<MovementDTO> movementDTOList = new ArrayList<>();
        for (Movement movement : movementList) {
            movementDTOList.add(toMovementDTO(movement));
        }
        return movementDTOList;
    }

    public List<PersonDTO> toPersonDTOList(List<Person> membersList) {
        List<PersonDTO> membersListDTO = new ArrayList<>();

        for (Person person : membersList) {
            membersListDTO.add(toPersonDTO(person));
        }
        return membersListDTO;

    }

    public ExceptionDTO toExceptionDto(String message) {
        return new ExceptionDTO(message);
    }

    public Address toAddress(String address) {
        return new Address(address);
    }
}

package switchtwentytwenty.project.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.domain.shared.EAccountType;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTO extends RepresentationModel<AccountDTO> implements DTO {

    private String ownerId;
    private String description;
    private double balance;
    private String accountId;
    private EAccountType accountType;

    /**
     * Constructor for AccountDTO (without AccountID).
     *
     * @param ownerId     String
     * @param description String
     * @param balance     double
     * @param accountType EAccountType
     */
    public AccountDTO(String ownerId, String description, double balance, EAccountType accountType) {
        this.ownerId = ownerId;
        this.description = description;
        this.balance = balance;
        this.accountType = accountType;
        this.accountId = "-1";
    }

    /**
     * this is a constructor without balance argument
     *
     * @param ownerId     String
     * @param description String
     * @param accountType enum
     */

    public AccountDTO(String ownerId, String description, EAccountType accountType) {
        this.ownerId = ownerId;
        this.description = description;
        this.balance = 0;
        this.accountId = "-1";
        this.accountType = accountType;
    }

    /**
     * Constructor for a cash account DTO.
     *
     * @param ownerId     String
     * @param description String
     * @param balance     double
     * @param accountID   int
     * @param accountType type of the account (enum)
     */
    public AccountDTO(String ownerId, String description, double balance, String accountID, EAccountType accountType) {
        this.ownerId = ownerId;
        this.description = description;
        this.balance = balance;
        this.accountId = accountID;
        this.accountType = accountType;
    }

    /**
     * Constructor for an input cash account DTO (for REST controller).
     *
     * @param description String
     * @param balance     double
     * @param accountType EAccountType
     */
    public AccountDTO(String description, double balance, EAccountType accountType) {
        this.ownerId = "";
        this.description = description;
        this.balance = balance;
        this.accountId = "-1";
        this.accountType = accountType;
    }

    /**
     * Constructor that allows to build an account DTO only with the balance
     * @param balance value
     */
    public AccountDTO(double balance) {
        this.balance = balance;
    }

    /**
     * Sets the AccountID for an AccountDTO.
     *
     * @param accountID String
     */
    public void setAccountId(String accountID) {
        this.accountId = accountID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return accountId.equals(that.accountId) && Objects.equals(ownerId, that.ownerId) && accountType == that.accountType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ownerId, accountId, accountType);
    }
}

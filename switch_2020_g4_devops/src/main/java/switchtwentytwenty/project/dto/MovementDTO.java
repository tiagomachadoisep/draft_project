package switchtwentytwenty.project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class MovementDTO extends RepresentationModel<MovementDTO> implements DTO{
    private final String accountID;
    private final double amount;

    public MovementDTO(String accountID, double amount) {
        this.accountID = accountID;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public String getAccountID() {
        return accountID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovementDTO)) return false;
        MovementDTO movementDTO = (MovementDTO) o;
        return accountID.equals(movementDTO.accountID) && amount == movementDTO.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountID, amount);
    }
}

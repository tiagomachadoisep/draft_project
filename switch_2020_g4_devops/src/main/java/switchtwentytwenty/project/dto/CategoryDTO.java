package switchtwentytwenty.project.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class CategoryDTO extends RepresentationModel<CategoryDTO> implements DTO {

    @Setter
    @Getter
    private String name;
    @Setter
    @Getter
    private String id;
    @Setter
    @Getter
    private String parentId;
    @Setter
    @Getter
    private String familyId;


    public CategoryDTO() {
        //setters are defined above
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDTO that = (CategoryDTO) o;
        return Objects.equals(name, that.name) && Objects.equals(id, that.id) && Objects.equals(parentId,
                that.parentId) && Objects.equals(familyId, that.familyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, parentId, familyId);
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", parentId='" + parentId + '\'' +
                ", familyId='" + familyId + '\'' +
                "}";
    }
}

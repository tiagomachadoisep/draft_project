package switchtwentytwenty.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


@Getter
@NoArgsConstructor
public class PersonDTO extends RepresentationModel<PersonDTO> implements DTO {
    private String emailAddress;
    private String name;
    private int vatNumber;
    private String address;
    private String familyID;
    private String birthDate;

    public PersonDTO(String emailAddress, String name, int vatNumber, String birthDate) {
        this.emailAddress = emailAddress;
        this.name = name;
        this.vatNumber = vatNumber;
        this.birthDate = birthDate;
        this.address = "other";
    }

    public PersonDTO(String name, String emailAddress, int vatNumber) {
        this.name = name;
        this.emailAddress = emailAddress;
        this.vatNumber = vatNumber;
        this.birthDate= LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-y"));
    }

    public PersonDTO(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public PersonDTO(String familyID, String personID, String name, int vatNumber, String address, String birthDate) {
        this.name = name;
        this.emailAddress = personID;
        this.vatNumber = vatNumber;
        this.address = address;
        this.familyID = familyID;
        this.birthDate = birthDate;
    }

    public void setFamilyId(String value) {
        this.familyID = value;
    }

    public String getFamilyID() {
        return familyID;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return emailAddress.equals(personDTO.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), emailAddress);
    }
}

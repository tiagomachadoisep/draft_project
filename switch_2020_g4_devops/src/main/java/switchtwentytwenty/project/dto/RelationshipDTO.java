package switchtwentytwenty.project.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RelationshipDTO extends RepresentationModel<RelationshipDTO> implements DTO {
    @Setter
    @Getter
    private String firstPersonEmail;
    @Setter
    @Getter
    private String secondPersonEmail;

    @Setter
    @Getter
    private String designation;
    @Setter
    @Getter
    private String familyId;
    @Setter
    @Getter
    private String relationshipId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RelationshipDTO that = (RelationshipDTO) o;
        return Objects.equals(firstPersonEmail, that.firstPersonEmail) && Objects.equals(secondPersonEmail,
                that.secondPersonEmail) && Objects.equals(designation, that.designation) &&
                Objects.equals(familyId, that.familyId) && Objects.equals(relationshipId, that.relationshipId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), firstPersonEmail, secondPersonEmail, designation, familyId, relationshipId);
    }

}

import React, {useContext, useState} from "react";
import AppContext from "../../context/AppContext";
import {
    addCustomCategory, fetchAccounts,
    fetchCategories,
    fetchMembers,
    fetchRelationships, fetchStandardCategories, toggleAccounts, toggleCategories,
    toggleFamilies,
    toggleMembers,
    toggleRelationships,
    ungetFamily
} from "../../context/Actions";
import {Button} from "react-bootstrap";
import Alert from "react-bootstrap/Alert";

function FamilySelector() {
    const {state, dispatch} = useContext(AppContext);
    const {family} = state;
    const {familyid, name, adminEmail} = family;

    const handleClickMembers = () => {
        fetchMembers(dispatch, familyid, state.token);
        toggleMembers(dispatch, state.members.on);
    }

    const handleClickRelations = () => {
        fetchRelationships(dispatch, familyid, state.token)
        fetchMembers(dispatch, familyid, state.token)
        toggleRelationships(dispatch, state.relationships.on);
    }

    const handleClickCategories = () => {
        fetchCategories(dispatch, familyid, state.token)
        toggleCategories(dispatch, state.categories.on);
    }

    const handleClickAccounts = () => {
        fetchAccounts(dispatch, familyid, state.token)
        toggleAccounts(dispatch, state.accounts.on);
    }

    const familyIdReset = () => {
        ungetFamily(dispatch)
        toggleFamilies(dispatch, state.families.on)
    }

    if (familyid === undefined || familyid === 0) {
        return null;
    }

    else{
        return (
            <div>
                {/*<p>Family {name} selected</p>*/}
                <h4>
                    <Alert variant={"primary"} style={{display: "inline-block"}}>Family '{name}' selected
                        {/*<Alert.Heading>Family '{name}' selected</Alert.Heading>*/}
                    </Alert>
                </h4>
                <Button variant={"info"} onClick={handleClickMembers} style={{marginLeft: "7px"}}>Members</Button>{' '}
                <Button variant={"info"} onClick={handleClickRelations}>Relationships</Button>{' '}
                <Button variant={"info"} onClick={handleClickCategories}>Family Categories</Button>{' '}
                <Button variant={"info"} onClick={handleClickAccounts}>Family Accounts</Button>{' '}
                <Button variant={"warning"} onClick={familyIdReset}>Reset</Button>
                {/*<input type="button" onClick={handleClickMembers} value="Members"/>
                <input type="button" onClick={handleClickRelations} value="Relationships"/>
                <input type="button" onClick={familyIdReset} value="Reset"/>*/}
            </div>
        )
    }
}

export default FamilySelector;
import React, {useContext, useEffect, useState} from "react";
import AppContext from "../../context/AppContext";
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import {addFamily, getFamilies, getFamily, ungetFamily} from "../../context/Actions";
import Spinner from "react-bootstrap/Spinner";
import AlertDismissable from "../relationships/AlertDismissable";
import {validEmailRegex, validFamilyNameRegex, validPersonNameRegex, validPtVatRegex} from "../content/ConstantsUtils";
import {TextField} from "@material-ui/core";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {format} from "date-fns";


function Table() {
    const {state, dispatch} = useContext(AppContext);
    const {families, family} = state;
    const {loading, error, data} = families;
    const {familyid} = family;
    /*const addFamilyError = family.error;*/
    const [selectedRow, setSelectedRow] = useState(null);
    const [selectedDate, handleDateChange] = useState(null);

    useEffect(() => {
        getFamilies(dispatch, state.token);
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleClick = (id) => {
        setSelectedRow(id)
        if (familyid !== id) {
            console.log("clicked on")
            console.log(id)
            getFamily(dispatch, id, state.token);
        } else {
            console.log("ungetting")
            ungetFamily(dispatch);
            console.log(family)
            setSelectedRow(null)
        }
    }

    const handleReloadClick = () => {
        getFamilies(dispatch,state.token)
    }

    const tablefamilies = (<div>
        <MaterialTable
            onRowClick={(event, rowData) => handleClick(rowData.id)}
            icons={tableIcons}
            actions={[
                {
                    icon: tableIcons.Refresh,
                    tooltip: 'Refresh families',
                    isFreeAction: true,
                    onClick: handleReloadClick,
                }
            ]}
            localization={{
                body: {
                    emptyDataSourceMessage: 'No families to display.'
                }
            }}
            options={{
                /*tableLayout: "fixed",*/
                pageSize: 7,
                pageSizeOptions: [], // e.g. [5, 10, 20]
                showEmptyDataSourceMessage: true,
                padding: "default", //dense or default
                searchFieldAlignment: "left",
                searchFieldVariant: "outlined", //standard, outlined, filled
                toolbarButtonAlignment: "left",
                paging: false,
                search: true,
                filtering: false,
                sorting: true,
                showTitle: true,
                toolbar: true,
                thirdSortClick: false,
                grouping: true,
                addRowPosition: "first",
                headerStyle: {
                    position: "sticky",
                    top: "0",
                    backgroundColor: '#41506b',
                    color: '#FFF'
                },
                maxBodyHeight: "350px",
                actionsColumnIndex: -1,
                rowStyle: rowData => ({
                    backgroundColor: (selectedRow === rowData.id) ? '#EEE' : '#FFF'
                })
            }}
            editable={{
                onRowAdd: newFamily =>
                    new Promise((resolve, reject) => {
                        console.log(newFamily)
                        addFamily(dispatch, newFamily, state.token);
                        resolve();
                    })
            }}
            columns={[
                {
                    title: "ID",
                    field: "id",
                    editable: "never"
                },
                {
                    title: "Name",
                    field: "name",
                    validate: rowData => rowData.name === undefined ?
                        false : (rowData.name === '' ?
                            false : (!!rowData.name.match(validFamilyNameRegex))),
                    type: "string",
                    editComponent: props => (
                        <TextField
                            type="text"
                            value={props.value}
                            onChange={e => props.onChange(e.target.value)}
                            error={props.value === undefined ? false : (props.value === '' ?
                                true : (!props.value.match(validFamilyNameRegex)))}
                            helperText={props.value === undefined ?
                                '' : (props.value === '' ?
                                    'Cannot be empty.' : (props.value.match(validFamilyNameRegex)
                                        ? '' : 'Letters only.'))}
                            required={true}
                            InputProps={{'style': {fontSize: "13px"}}}
                            placeholder='e.g. Mendes'
                            label="Name"
                        />
                    )
                },
                {
                    title: "Admin Email",
                    field: "adminEmail",
                    validate: rowData => rowData.adminEmail !== undefined ?
                        ((!(rowData.adminEmail === '' || !rowData.adminEmail.match(validEmailRegex)))) : false,
                    editComponent: props => (
                        <TextField
                            type="email"
                            label="Admin Email"
                            value={props.value}
                            onChange={e => props.onChange(e.target.value)}
                            error={props.value !== undefined ?
                                ((props.value === '' || !props.value.match(validEmailRegex))) : false}
                            helperText={props.value === undefined ? '' :
                                (props.value === '' ? 'Cannot be empty.' :
                                    (props.value.match(validEmailRegex) ? '' : "Invalid format."))}
                            required={true}
                            placeholder="e.g. ex@gmail.com"
                            InputProps={{'style': {fontSize: "13px"}}}
                        />
                    )
                },
                {
                    title: "Admin VAT",
                    field: "adminVat",
                    validate: rowData => rowData.adminVat === undefined ?
                        false : (rowData.adminVat === '' ?
                            false : (!!rowData.adminVat.match(validPtVatRegex))),
                    editComponent: props => (
                        <TextField
                            type="number"
                            value={props.value}
                            onChange={e => props.onChange(e.target.value)}
                            /*error={props.value !== undefined ? (props.value === '') : false}*/
                            error={props.value === undefined ?
                                false : (props.value === '' ?
                                    true : (!props.value.match(validPtVatRegex)))}
                            /*error={props.value === undefined ? false : (props.value === '' ? true : (!validateNIF(props.value)))}*/
                            helperText={props.value === undefined ?
                                '' : (props.value === '' ?
                                    'Cannot be empty.' : (!props.value.match(validPtVatRegex) ?
                                        'Not a VAT number.' : ''))}
                            required={true}
                            InputProps={{'style': {fontSize: "13px"}}}
                            placeholder='e.g. 123456789'
                            label="Admin VAT"
                            /*margin="dense"*/
                            /*variant*/
                        />
                    ),
                    //hidden: hideVat
                    cellStyle:{
                        color: "#FFF"
                    },
                    headerStyle:{
                        color: "#41506b"
                    }
                },
                {
                    title: "Admin Name",
                    field: "adminName",
                    validate: rowData => rowData.adminName === undefined ?
                        false : (rowData.adminName === '' ?
                            false : (!!rowData.adminName.match(validPersonNameRegex))),
                    type: "string",
                    editComponent: props => (
                        <TextField
                            type="text"
                            value={props.value}
                            onChange={e => props.onChange(e.target.value)}
                            error={props.value === undefined ? false : (props.value === '' ?
                                true : (!props.value.match(validPersonNameRegex)))}
                            helperText={props.value === undefined ?
                                '' : (props.value === '' ?
                                    'Cannot be empty.' : (props.value.match(validPersonNameRegex)
                                        ? '' : 'Letters & space only.'))}
                            required={true}
                            InputProps={{'style': {fontSize: "13px"}}}
                            placeholder='e.g. Hell Boy'
                            label="Admin Name"
                        />
                    ),
                    //hidden: hideVat
                    cellStyle:{
                        color: "#FFF"
                    },
                    headerStyle:{
                        color: "#41506b"
                    }
                },
                {
                    title: "Admin Birthdate",
                    field: "adminBirthDate",
                    initialEditValue: '',
                    validate: rowData => rowData.adminBirthDate !== undefined ? (rowData.adminBirthDate !== null) : false,
                    editComponent: props => (
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                                error={props.value !== undefined ? (props.value === null) : false}
                                helperText={props.value !== undefined ?
                                    (props.value === null ?
                                        'Cannot be empty.' : '') : ''}
                                clearable
                                value={selectedDate}
                                placeholder="dd-MM-yyyy"
                                label="Admin Birthdate"
                                onChange={(date) => {
                                    handleDateChange(date);
                                    console.log(date);
                                    console.log(date !== null ? format(date, 'dd-MM-yyyy') : "null")
                                    props.onChange(date !== null ?
                                        format(date, 'dd-MM-yyyy') : null)
                                }
                                }
                                minDate="01-01-1890"
                                maxDate={new Date()}
                                format="dd-MM-yyyy"
                                required
                                InputProps={{'style': {fontSize: "13px"}}}

                            />
                        </MuiPickersUtilsProvider>
                    ),
                    //hidden: hideVat
                    cellStyle: {
                        color: "#FFF"
                    },
                    headerStyle: {
                        color: "#41506b"
                    }
                }
            ]} data={Array.from(data)}
            title="Families"/>
        <hr/>
        <AlertDismissable variant="danger" error={error}/>
    </div>)

    if (!families.on) {
        return null;
    } else {
        if (loading === true) {
            console.log("chegou à table families loading")
            console.log(data)
            return (
                /*<h1>Loading ......</h1>*/
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            );
        } else {
            /*if (error !== null) {*/
                return (
                    /*<>{tablefamilies}
                        <h1>{error}</h1>
                    </>*/
                    tablefamilies
                )

            /*} else {
                if (data.length > 0) {
                    return tablefamilies;
                } else {
                    return (<h1>No data.... </h1>);
                }
            }*/
        }
    }
}

export default Table;
import React, {useContext} from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PaymentIcon from '@material-ui/icons/Payment';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import GroupIcon from '@material-ui/icons/Group';
import CategoryIcon from '@material-ui/icons/Category';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import AppContext from "../../context/AppContext";
import {
    fetchAccounts,
    fetchCategories,
    fetchMembers,
    fetchRelationships,
    fetchStandardCategories,
    toggleAccounts,
    toggleCategories,
    toggleFamilies,
    toggleMembers,
    toggleRelationships,
    toggleStandardCategories,
    ungetFamily
} from "../../context/Actions";

export default function ListItems() {

    const {state, dispatch} = useContext(AppContext);
    const handleFamilyClick = () => {
        toggleFamilies(dispatch, state.families.on);

    }

    //SYS ADMIN
    const handleMemberClick = () => {
        ungetFamily(dispatch)
        fetchMembers(dispatch, 0, state.token)
        toggleMembers(dispatch, state.members.on);

    }
    const handleStandardClick = () => {
        ungetFamily(dispatch)
        fetchStandardCategories(dispatch, state.token)
        toggleStandardCategories(dispatch, state.categories.on);

    }

    const handleAccountsClick = () => {
        fetchAccounts(dispatch, 0, state.token,state.loggedUser,state.role)
        toggleAccounts(dispatch, state.accounts.on)

    }

    //FAMILY ADMIN
    const handleAdminFamilyMembersClick = () => {
        console.log(state.userFamilyId)
        fetchMembers(dispatch, state.userFamilyId, state.token);
        toggleMembers(dispatch, state.members.on);

    }

    const handleAdminFamilyCategoriesClick = () => {
        console.log(state.userFamilyId)
        fetchCategories(dispatch, state.userFamilyId, state.token);
        toggleCategories(dispatch, state.categories.on);

    }

    const handleAdminFamilyRelationshipsClick = () => {
        console.log(state.userFamilyId)
        fetchMembers(dispatch, state.userFamilyId, state.token)
        fetchRelationships(dispatch, state.userFamilyId, state.token);
        toggleRelationships(dispatch, state.relationships.on);


    }

    const handleAdminFamilyAccountsClick = () => {
        console.log(state.userFamilyId)
        fetchAccounts(dispatch, state.userFamilyId, state.token,state.loggedUser,state.role);
        toggleAccounts(dispatch, state.accounts.on)

    }

    //FAMILY MEMBER
    const handleUserFamilyCategoriesClick = () => {
        console.log(state.userFamilyId)
        fetchCategories(dispatch, state.userFamilyId, state.token);
        toggleCategories(dispatch, state.categories.on);

    }

    const handleUserFamilyAccountsClick = () => {
        console.log(state.userFamilyId)
        fetchAccounts(dispatch, state.userFamilyId, state.token,state.loggedUser,state.role);
        toggleAccounts(dispatch, state.accounts.on)

    }

    const mainListItems = (
        <div>
            {/*  <ListItem button>
                <ListItemIcon>
                    <DashboardIcon/>
                </ListItemIcon>
                <ListItemText primary="Home"/>
            </ListItem>*/}
            <ListItem button onClick={handleFamilyClick}>
                <ListItemIcon>
                    <GroupIcon style={{color: '#ff656c'}}/>
                </ListItemIcon>
                <ListItemText primary="Families"/>
            </ListItem>
            <ListItem button onClick={handleMemberClick}>
                <ListItemIcon>
                    <EmojiPeopleIcon style={{color: '#ff656c'}}/>
                </ListItemIcon>
                <ListItemText primary="Members"/>
            </ListItem>
            <ListItem button onClick={handleStandardClick}>
                <ListItemIcon>
                    <CategoryIcon style={{color: '#ff656c'}}/>
                </ListItemIcon>
                <ListItemText primary="Categories"/>
            </ListItem>
            <ListItem button onClick={handleAccountsClick}>
                <ListItemIcon>
                    <PaymentIcon style={{color: '#ff656c'}}/>
                </ListItemIcon>
                <ListItemText primary="Accounts"/>
            </ListItem>
        </div>
    );


    if (state.role === "ADMIN") {
        console.log("ROLE IS ADMIN");
        return (
            <div>
                {/*<ListItem button>
                    <ListItemIcon>
                        <DashboardIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Home"/>
                </ListItem>*/}
                <ListItem button onClick={handleAdminFamilyMembersClick}>
                    <ListItemIcon>
                        <EmojiPeopleIcon style={{color: '#ff656c'}}/>
                    </ListItemIcon>
                    <ListItemText primary="Family Members"/>
                </ListItem>
                <ListItem button onClick={handleAdminFamilyCategoriesClick}>
                    <ListItemIcon>
                        <CategoryIcon style={{color: '#ff656c'}}/>
                    </ListItemIcon>
                    <ListItemText primary="Family Categories"/>
                </ListItem>
                <ListItem button onClick={handleAdminFamilyRelationshipsClick}>
                    <ListItemIcon>
                        <GroupAddIcon style={{color: '#ff656c'}}/>
                    </ListItemIcon>
                    <ListItemText primary="Family Relationships"/>
                </ListItem>
                <ListItem button onClick={handleAdminFamilyAccountsClick}>
                    <ListItemIcon>
                        <PaymentIcon style={{color: '#ff656c'}}/>
                    </ListItemIcon>
                    <ListItemText primary="Family Accounts"/>
                </ListItem>
            </div>
        );
    }

    if (state.role === "USER") {
        console.log("ROLE IS USER");
        return (
            <div>
                {/*<ListItem button>
                    <ListItemIcon>
                        <DashboardIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Home"/>
                </ListItem>*/}
                <ListItem button onClick={handleUserFamilyCategoriesClick}>
                    <ListItemIcon>
                        <CategoryIcon style={{color: '#ff656c'}}/>
                    </ListItemIcon>
                    <ListItemText primary="Family Categories"/>
                </ListItem>
                <ListItem button onClick={handleUserFamilyAccountsClick}>
                    <ListItemIcon>
                        <PaymentIcon style={{color: '#ff656c'}}/>
                    </ListItemIcon>
                    <ListItemText primary="My Ledger"/>
                </ListItem>
            </div>
        );

    }

    return mainListItems;

}

/*export const validEmailRegex =  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;*/
export const validEmailRegex = /^[\w-]+@([\w-]+\.)+[\w-]{2,4}$/;
/*export const validEmailRegex =
/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])
|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;*/
export const validVatRegex = /^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/;
export const validPersonNameRegex = /^[a-zA-Z\s]*$/;
export const validDateRegex =  /^\s*(3[01]|[12][0-9]|0?[1-9])(1[012]|0?[1-9])((?:19|20)\d{2})\s*$/;
export const validPtVatRegex = /*/^(((([1-3]|5|6)\d)|(45)|(7([0-2]|[4-5]|[7-9]))|(9[0|1|8|9]))(\d{7}$))/;*/ /^([0-9]{9})$/;
export const validCategoryName = /^[A-Za-z]+$/;
export const validBalanceRegex = /[+-]?([0-9]*[.])?[0-9]+/;
export const validFamilyNameRegex = /^[a-zA-Z]*$/;

/*
export function validateNIF(nif){
    let added, control, mod;
    console.log(nif)

    if(nif.length === 9){
        added = ((nif[7]*2)+(nif[6]*3)+(nif[5]*4)+(nif[4]*5)+(nif[3]*6)+(nif[2]*7)+(nif[1]*8)+(nif[0]*9));
        mod = added % 11;
        if(mod === 0 || mod === 1){
            control = 0;
        } else {
            control = 11 - mod;
        }

        return nif[8] === control;
    } else {
        return false;
    }
}*/

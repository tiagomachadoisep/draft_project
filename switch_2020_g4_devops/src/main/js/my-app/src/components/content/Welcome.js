import React, {useContext, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AppContext from "../../context/AppContext";
import {fetchInfo, getFamily} from "../../context/Actions";
import image from '../../images/Moneytree_Flatline.png';

const useStyles = makeStyles({
    welcomeWord: {
        fontWeight: 'bold',
        textShadow: '2px 1px 2px gray',
        textAlign: 'center',
        margin:'0 auto',
        marginTop:'70px',
        marginRight:'400px'
    },
    welcome: {
       blockSize:'120px',
        display:'flex'
    },
    familyName: {
        color: '#ff656c',
        fontSize: '18px',
        fontWeight: 'bold',
        textShadow: 'none',
        marginLeft:'20px',

    },
    left: {
        display:'inline-block',
        position:'relative'
    }
});

function Welcome() {

    const {state, dispatch} = useContext(AppContext);
    const {info, loggedUser, userFamilyId, family} = state;
    const {data} = info;

    useEffect(() => {
        console.log("MEMBRO" + loggedUser)
        fetchInfo(dispatch, loggedUser, state.token);
        console.log(data)
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        console.log(userFamilyId);
        getFamily(dispatch, userFamilyId, state.token)
    }, [userFamilyId]); // eslint-disable-line react-hooks/exhaustive-deps


    const classes = useStyles();
    if (data.length === 0) {
        return (
            <React.Fragment>
                <Typography className={classes.welcome} component="h1" variant="h4">
                    Welcome
                </Typography>
            </React.Fragment>
        );
    } else if (userFamilyId==="admin" || userFamilyId===undefined) {
        console.log(family.name);
        console.log("family id: "+userFamilyId)
        return (<div className={classes.welcome}>
                <React.Fragment>
                    <div className={classes.left}>
                        <img align={'left'} src={image} alt={image}
                             style={{width: '180px', height: '180px'}}/>

                    </div>
                    <Typography className={classes.welcomeWord} component="h1" variant="h4">
                        Welcome {data[0].name}
                    </Typography>

                </React.Fragment>
            </div>
        );
    } else {
        console.log("current family is: "+userFamilyId)
        return (<div className={classes.welcome}>
            <React.Fragment>
                <div className={classes.left}>
                    <img align={'left'} src={image} alt={image}
                         style={{width: '180px', height: '180px'}}/>
                <p className={classes.familyName}> Family {family.name}</p></div>
                <Typography className={classes.welcomeWord} component="h1" variant="h4">
                    Welcome {data[0].name}
                </Typography>

            </React.Fragment>
        </div>);
    }
}

export default Welcome;
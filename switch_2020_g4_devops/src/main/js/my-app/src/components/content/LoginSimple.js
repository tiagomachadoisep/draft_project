import React, {useContext, useState} from "react";
import AppContext from "../../context/AppContext";
import "../../LoginSimple.css";
import {loginSimple} from "../../context/Actions";
import Spinner from "react-bootstrap/Spinner";
import {Copyright} from "./Dashboard";

function LoginSimple() {
    const {state, dispatch} = useContext(AppContext);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleChangeName = (event) => {
        const {value} = event.target;
        setUsername(value);
    }
    const handleChangePassword = (event) =>{
        const {value} = event.target;
        setPassword(value);
    }

    const handleOnClick = (name, adminEmail) => {
        loginSimple(dispatch, name, adminEmail);
        console.log("state.token in LoginSimple: " + state.token);
        setUsername("");
        setPassword("");
    }

    if (state.loginMessage === 'Loading...') {
        return (<div>
                <div className="login-block">
                    <form>
                        {/*<h1>LOG IN</h1>*/}
                        <label>E-mail</label>
                        <input type="email" placeholder="Enter e-mail" value={username}
                               onChange={(event) => handleChangeName(event)}/>
                        <label>Password</label>
                        <input type="password" placeholder="Enter password" value={password}
                               onChange={(event) => handleChangePassword(event)}/>
                        {/*<input type="button" className="singin" value="Sign in" onClick={() => handleOnClick(username, password)}/>*/}
                    </form>
                    {/*<div style={{display: "inline-block"}} >*/}
                    <p>{state.loginMessage}</p>
                    {/*</div>*/}
                    {/*<div style={{display: "inline-block"}} >*/}
                    <Spinner animation="border" size="lg"/>
                    {/*</div>*/}

                </div>
            </div>
        );
    } else {
        return (<div className="container">
                <h1 className="sws-title">
                    SWS Family Finance App
                </h1>
                <div className="login-block">
                    <form>
                        {/*<h1>LOG IN</h1>*/}
                        <label>E-mail</label>
                        <input type="email" placeholder="Enter e-mail" value={username}
                               onChange={(event) => handleChangeName(event)}/>
                        <label>Password</label>
                        <input type="password" placeholder="Enter password" value={password}
                               onChange={(event) => handleChangePassword(event)}/>
                        <input type="button" className="singin" value="Sign in"
                               onClick={() => handleOnClick(username, password)}/>
                    </form>
                    <p>{state.loginMessage}</p>

                </div>
                <Copyright/>
            </div>
        );
    }


}
export default LoginSimple;
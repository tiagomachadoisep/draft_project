import {ListItem} from "@material-ui/core";
import {fetchInfo, toggleInfo} from "../../context/Actions";
import React, {useContext} from "react";
import AppContext from "../../context/AppContext";
import FaceIcon from '@material-ui/icons/Face';
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

/*const useStyles = makeStyles((theme) => ({
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
        border: "3px solid #ff656c"
    },

}));*/

function UserProfileButton() {

    const {state, dispatch} = useContext(AppContext);
    const {loggedUser} = state;

    const handleClick = () => {
       // ungetFamily(dispatch)
        fetchInfo(dispatch, loggedUser, state.token);
        //fetchInfo(dispatch, id, state.token)
        toggleInfo(dispatch, state.info.on);
        console.log();
    }
    return (
        <div>
            {/*<Avatar onClick={handleClick} alt="Piggy" src="" className={classes.large}/>*/}
            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                    <FaceIcon style={{color: '#ff656c'}}/>
                </ListItemIcon>
                <ListItemText primary="My Profile"/>
            </ListItem>
        </div>
    );

}

export default UserProfileButton;
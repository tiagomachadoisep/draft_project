import React, {useContext, useEffect} from "react";
import AppContext from "../../context/AppContext";
import {addAccount, fetchAccounts, fetchLedgerId, fetchMovements} from "../../context/Actions";
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import AlertDismissable from "../relationships/AlertDismissable";
import {MenuItem, Select, TextField} from "@material-ui/core";
import Spinner from "react-bootstrap/Spinner";
import {validBalanceRegex} from "../content/ConstantsUtils";

function AccountsTable() {

    const {state, dispatch} = useContext(AppContext);
    const {accounts, family, loggedUser, userFamilyId, role} = state;
    const {loading, error, data} = accounts;
    const {familyid} = family;

    const accountTypes = ['PERSONAL_CASH', 'FAMILY_CASH', 'CREDIT_CARD', 'BANK_SAVINGS', 'BANK'];

    /*const accountTypesEntries = accountTypes.map((account, index) => {

        return {accountType: account, label: "valid designation"}
    });*/


    useEffect(() => {
        console.log('USE EFFFFFECT ACCOUNTS')
        console.log("role: "+ role)
        console.log("logged user: "+loggedUser)
        console.log("token: "+state.token)
        console.log(familyid)
        fetchAccounts(dispatch, userFamilyId, state.token,loggedUser,role)
        fetchLedgerId(dispatch, loggedUser, state.token)
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    const handleClick = (accountId) => {
        console.log(state.movements.ledgerId)
        fetchMovements(dispatch, state.movements.ledgerId, accountId, state.token)
        console.log("AccountID:" + accountId)
        console.log("LedgerID:" + state.movements.ledgerId)
    }

    const handleReloadClick = () => {
        fetchAccounts(dispatch, userFamilyId, state.token, loggedUser,role)
    }


    const tableAccounts = (<div>

        <div style={{maxWidth: "100%"}}>
            <MaterialTable
                //  onRowClick={(event, rowData)=>handleClick(rowData.emailAddress)}
                onRowClick={(event, rowData) => handleClick(rowData.accountId)}
                icons={tableIcons}
                actions={[
                    {
                        icon: tableIcons.Refresh,
                        tooltip: 'Refresh',
                        isFreeAction: true,
                        onClick: handleReloadClick,

                    }
                ]}
                localization={{
                    body: {
                        emptyDataSourceMessage: 'No accounts to display.'
                    }
                }}
                options={{
                    rowStyle: {
                        backgroundColor: '#EEE',
                    },
                    pageSize: 7,
                    pageSizeOptions: [], // e.g. [5, 10, 20]
                    showEmptyDataSourceMessage: true,
                    padding: "default", //dense or default
                    searchFieldAlignment: "left",
                    searchFieldVariant: "outlined", //standard, outlined, filled
                    toolbarButtonAlignment: "left",
                    paging: false,
                    search: true,
                    filtering: false,
                    sorting: true,
                    showTitle: true,
                    toolbar: true,
                    thirdSortClick: false,
                    grouping: true,
                    addRowPosition: "first",
                    headerStyle: {
                        position: "sticky",
                        top: "0",
                        backgroundColor: '#41506b',
                        color: '#FFF'
                    },
                    maxBodyHeight: "350px",
                    actionsColumnIndex: -1
                }}
                editable={{
                    onRowAdd: newAccount =>
                        new Promise((resolve, reject) => {
                            console.log("newAccountTable", newAccount)
                            console.log("ownerId -> ", newAccount.ownerId)
                            addAccount(dispatch, newAccount, state.loggedUser, state.token);
                            resolve();

                        })
                }}
                columns={[
                    {
                        title: "Account ID",
                        field: "accountId",
                        hidden: true,
                    },
                    {
                        title: "Person Email",
                        field: "ownerId",
                        editable: "never"
                    },
                    {
                        title: "Balance",
                        field: "balance",
                        validate: rowData =>
                            rowData.balance === undefined ?
                                true : (rowData.balance === '' ?
                                true : (!!rowData.balance.match(validBalanceRegex))),
                        editComponent: props => (
                            <TextField
                                type="number"
                                label="Balance"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value === undefined ?
                                    false : (props.value === '' ?
                                        false : (!props.value.match(validBalanceRegex)))}

                                helperText={props.value === undefined ?
                                    '' : (props.value === '' ? '' : (props.value.match(validBalanceRegex) ?
                                        '' : 'Only numbers and one dot.'))}
                                style={{width: "100%"}}
                            />
                        )
                    },
                    {
                        title: "Description",
                        field: "description",
                        validate: rowData => rowData.description !== undefined ?
                            ((!(rowData.description === ''))) : false ,
                        editComponent: props => (
                            <TextField
                                label="Description"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value !== undefined ? (props.value === '') : false}
                                helperText={props.value === undefined ? '' :
                                    (props.value === '' ? 'Cannot be empty.': '')}
                                required={true}
                                style={{width: "100%"}}
                            />
                        )
                    },
                    {
                        title: "Account Type",
                        field: "accountType",
                        validate: rowData => rowData.accountType !== undefined,
                        editComponent: props => (
                            <Select
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                style={{width: "100%"}}
                            >
                                {accountTypes.map((type) =>
                                    (
                                        <MenuItem value={type}>{type}</MenuItem>
                                    )
                                )}
                            </Select>
                        )
                    }
                ]
                }
                data={data}
                title="Accounts"
            />
            <hr/>


            <AlertDismissable variant="danger" error={error}/>
        </div>
    </div>)

    if (!accounts.on) {
        return null;
    } else {
        if (loading === true) {
            return (
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            );
        } else {
            return tableAccounts;
        }
    }
}

export default AccountsTable;
import React, {useContext} from 'react';
import AppContext from "../../context/AppContext";
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import Spinner from "react-bootstrap/Spinner";
import AlertDismissable from "../relationships/AlertDismissable";

function MovementsTable() {
    const {state} = useContext(AppContext);
    const {movements} = state;
    const {loading, error, data, on} = movements;

    /*const handleReloadClick = () => {
        /!*fetchMovements(dispatch, state.token)*!/
        fetchMovements(dispatch, state.movements.ledgerId, state.accounts.accountId, state.token)
    }*/

    const tableMovements = (<div>
        <MaterialTable
            icons={tableIcons}
            /*actions={[
                {
                    icon: tableIcons.Refresh,
                    tooltip: 'Refresh',
                    isFreeAction: true,
                    onClick: handleReloadClick,
                }
            ]}*/
            localization={{
                body: {
                    emptyDataSourceMessage: 'No info to display.'
                }
            }}
            options={{
                rowStyle: {
                    backgroundColor: '#EEE',
                },
                pageSize: 7,
                pageSizeOptions: [], // e.g. [5, 10, 20]
                showEmptyDataSourceMessage: true,
                padding: "default", //dense or default
                toolbarButtonAlignment: "left",
                paging: false,
                search: false,
                filtering: false,
                sorting: true,
                showTitle: true,
                toolbar: true,
                thirdSortClick: false,
                grouping: false,
                addRowPosition: "first",
                headerStyle: {
                    position: "sticky",
                    top: "0",
                    backgroundColor: '#41506b',
                    color: '#FFF'
                },
                maxBodyHeight: "350px"
            }}
            columns={[
                {
                    title: "Amount",
                    field: "amount",
                },
            ]} data={data}
            title="Movements"/>
        <hr/>

        <AlertDismissable variant="danger" error={error}/>
    </div>)

    if (on === false || state.accounts.on === false) {
        return null;
    } else {
        if (loading === true) {
            return (
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            );
        } else {
            if (error !== null) {
                return (<h1>Error ....</h1>);
            } else {
                return (
                    tableMovements
                );
            }
        }
    }
}

export default MovementsTable;
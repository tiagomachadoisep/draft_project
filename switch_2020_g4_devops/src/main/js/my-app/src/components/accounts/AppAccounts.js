import React from "react";
import AccountsTable from './AccountsTable';
import MovementsTable from "./MovementsTable";

function AppAccounts(){

    return(
        <div>
            <AccountsTable />
            <MovementsTable />
        </div>
    );
}

export default AppAccounts;
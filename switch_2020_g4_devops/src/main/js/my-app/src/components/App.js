import React, {useContext} from 'react';
import AppContext from "../context/AppContext";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import LoginSimple from "./content/LoginSimple";
import Dashboard from "./content/Dashboard";


export function App() {
    const {state} = useContext(AppContext);
    if (state.token === undefined) {
        return (
            <div>

                <LoginSimple/>

            </div>
        );
    } else {
        return (
            <Dashboard/>
        )
    }
}

export default App;
import React, {useContext, useEffect, useState} from 'react';

import AppContext from '../../context/AppContext.js';
import {addMember, fetchMembers, genericRequest} from '../../context/Actions'

import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import Spinner from "react-bootstrap/Spinner";
import AlertDismissable from "../relationships/AlertDismissable";
import {TextField} from "@material-ui/core";
import {validEmailRegex, validPersonNameRegex, validPtVatRegex} from "../content/ConstantsUtils";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {format} from "date-fns";


function Table() {
    const {state, dispatch} = useContext(AppContext);
    const {members, userFamilyId, memberAdded, memberAddedLinks} = state;
    const {loading, error, data} = members;
    const [selectedDate, handleDateChange] = useState(null);
    const [emailAdd,setEmailAdd] = useState("");

    useEffect(() => {
        console.log("FAMILY ID IN MEMBERS USE EFFECT " + userFamilyId)
        fetchMembers(dispatch, userFamilyId, state.token);
        //console.log("USEEFFECT")
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    /*useEffect(() =>{
        handleDateChange(null);
    }, [data])*/

    useEffect(() => {
        console.log("members have changed!" + userFamilyId)
        console.log(state.memberAddedLinks)
        console.log(Object.keys(state.memberAddedLinks)[1])
        console.log(memberAddedLinks[ 'Other emails' ])
        console.log(memberAddedLinks.Family)
    }, [memberAdded]); // eslint-disable-line react-hooks/exhaustive-deps

    const handleReloadClick = () => {
        console.log("FAMILY ID IN MEMBERS TABLE " + userFamilyId)
        fetchMembers(dispatch, userFamilyId, state.token);
        /*handleDateChange(null);*/
    }

    /*const handleClick = (id) => {
        fetchInfo(dispatch, id, state.token)
        toggleInfo(dispatch, state.info.on)
    }*/

    const handleClickHateoas = (event) => {
        console.log("handling click hateoas")
        event.preventDefault()
        genericRequest(dispatch,memberAddedLinks[ 'Other emails' ],"POST",state.token, emailAdd)
        setEmailAdd("")
    }

    const tableMembers = (<div>
        <div style={{maxWidth: "100%"}}>
            <MaterialTable
                // onRowClick={(event, rowData) => handleClick(rowData.emailAddress)}
                icons={tableIcons}
                actions={[
                    {
                        icon: tableIcons.Refresh,
                        tooltip: 'Refresh',
                        isFreeAction: true,
                        onClick: handleReloadClick,
                    },
                ]}
                localization={{
                    body: {
                        emptyDataSourceMessage: 'No members to display.'
                    }
                }}
                options={{
                    rowStyle: {
                        backgroundColor: '#EEE',
                    },
                    pageSize: 7,
                    pageSizeOptions: [], // e.g. [5, 10, 20]
                    showEmptyDataSourceMessage: true,
                    padding: "default", //dense or default
                    searchFieldAlignment: "left",
                    searchFieldVariant: "outlined", //standard, outlined, filled
                    toolbarButtonAlignment: "left",
                    paging: false,
                    search: true,
                    filtering: false,
                    sorting: true,
                    showTitle: true,
                    toolbar: true,
                    thirdSortClick: false,
                    grouping: true,
                    addRowPosition: "first",
                    headerStyle: {
                        position: "sticky",
                        top: "0",
                        backgroundColor: '#41506b',
                        color: '#FFF'
                    },
                    maxBodyHeight: "350px",
                    actionsColumnIndex: -1
                }}
                editable={{
                    onRowAdd: userFamilyId !== 0 ? newMember =>
                        new Promise((resolve, reject) => {

                            console.log(newMember)
                            console.log(newMember.emailAddress)
                            if (newMember.emailAddress === '') {
                                console.log(newMember)
                                console.log('going to get rejected')
                                reject();
                            } else {

                                addMember(dispatch, newMember.emailAddress, newMember.address
                                    , newMember.birthDate, userFamilyId, newMember.name, newMember.vatNumber, state.token);
                                resolve();

                            }

                            handleDateChange(" ");
                        }) : undefined
                }}
                columns={[
                    {
                        title: "Email",
                        field: "emailAddress",

                        validate: rowData => rowData.emailAddress !== undefined ?
                            ((!(rowData.emailAddress === '' || !rowData.emailAddress.match(validEmailRegex)))) : false,
                        editComponent: props => (
                            <TextField
                                type="email"
                                label="Email"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value !== undefined ?
                                    ((props.value === '' || !props.value.match(validEmailRegex))) : false}
                                helperText={props.value === undefined ? '' :
                                    (props.value === '' ? 'Cannot be empty.' :
                                        (props.value.match(validEmailRegex) ? '' : "Invalid format."))}
                                required={true}
                                placeholder="e.g. ex@gmail.com"
                                InputProps={{'style': {fontSize: "13px"}}}
                            />
                        )

                    },
                    {
                        title: "Address",
                        field: "address",
                        validate: rowData => rowData.address !== undefined ?
                            (rowData.address !== '') : false,
                        editComponent: props => (
                            <TextField
                                type="text"
                                label="Address"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value !== undefined ? (props.value === '') : false}
                                helperText={props.value !== undefined ? ((props.value === '') ?
                                    'Cannot be empty.' : '') : ''}
                                placeholder='e.g. Rua 50'
                                required={true}
                                InputProps={{'style': {fontSize: "13px"}}}
                            />
                        )

                    },
                    {
                        title: "Birth Date",
                        field: "birthDate",
                        initialEditValue: '',
                        validate: rowData => rowData.birthDate !== undefined ? (rowData.birthDate !== null) : false,
                        editComponent: props => (
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    error={props.value !== undefined ? (props.value === null) : false}
                                    helperText={props.value !== undefined ?
                                        (props.value === null ?
                                            'Cannot be empty.' : '') : ''}
                                    clearable
                                    value={selectedDate}
                                    placeholder="dd-MM-yyyy"
                                    label="Birthdate"
                                    onChange={(date) => {
                                        handleDateChange(date);
                                        console.log(date);
                                        console.log(date !== null ? format(date, 'dd-MM-yyyy') : "null")
                                        props.onChange(date !== null ?
                                            format(date, 'dd-MM-yyyy') : null)
                                    }
                                    }
                                    minDate="01-01-1890"
                                    maxDate={new Date()}
                                    format="dd-MM-yyyy"
                                    required
                                    InputProps={{'style': {fontSize: "13px"}}}

                                />
                            </MuiPickersUtilsProvider>
                        ),

                    },
                    {
                        title: "FamilyID",
                        field: "familyID",
                        editable: "never",
                        //hidden: true,
                    },
                    {
                        title: "Name",
                        field: "name",
                        validate: rowData => rowData.name === undefined ?
                            false : (rowData.name === '' ?
                                false : (!!rowData.name.match(validPersonNameRegex))),
                        type: "string",
                        editComponent: props => (
                            <TextField
                                type="text"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value === undefined ? false : (props.value === '' ?
                                    true : (!props.value.match(validPersonNameRegex)))}
                                helperText={props.value === undefined ?
                                    '' : (props.value === '' ?
                                        'Cannot be empty.' : (props.value.match(validPersonNameRegex)
                                            ? '' : 'Letters & space only.'))}
                                required={true}
                                InputProps={{'style': {fontSize: "13px"}}}
                                placeholder='e.g. Hell Boy'
                                label="Name"
                            />
                        )
                    },
                    {
                        title: "VAT",
                        field: "vatNumber",
                        validate: rowData => rowData.vatNumber === undefined ?
                            false : (rowData.vatNumber === '' ?
                                false : (!!rowData.vatNumber.match(validPtVatRegex))),
                        editComponent: props => (
                            <TextField
                                type="number"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                /*error={props.value !== undefined ? (props.value === '') : false}*/
                                error={props.value === undefined ?
                                    false : (props.value === '' ?
                                        true : (!props.value.match(validPtVatRegex)))}
                                /*error={props.value === undefined ? false : (props.value === '' ? true : (!validateNIF(props.value)))}*/
                                helperText={props.value === undefined ?
                                    '' : (props.value === '' ?
                                        'Cannot be empty.' : (!props.value.match(validPtVatRegex) ?
                                            'Not a VAT number.' : ''))}
                                required={true}
                                InputProps={{'style': {fontSize: "13px"}}}
                                placeholder='e.g. 123456789'
                                label="VAT"
                                /*margin="dense"*/
                                /*variant*/
                            />
                        )
                    }
                ]}
                data={data}
                title="Members"
            />
            <hr/>
            {/*<ErrorAlert variant="danger"/>*/}
            <AlertDismissable variant="danger" error={error}/>
            {/*<AlertDismissable variant="info" error={memberAdded} text={"available operations: "}/>*/}
        </div>
    </div>)

    const hateoasForm = (<div>

        <form onSubmit={handleClickHateoas}>
            <label>
                {memberAdded} : {Object.keys(memberAddedLinks)[1]}
                <p></p><input type="text" value={emailAdd} onChange={e => setEmailAdd(e.target.value)}/>
            </label>
            <input type="submit" value="Submit" style={{backgroundColor:'#41506b', marginTop:'20px',marginLeft:'5px', color:"white"}}/>
        </form>


    </div>)

    if (!members.on) {
        return null;
    } else {
        if (loading === true) {
            return (
                /*<h1>Loading ......</h1>*/
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            );
        } else {
            /*if (data.length > 0) {*/
            if(memberAdded===null) {
                return (
                    <div>
                        {tableMembers}
                    </div>);
                /*} else {
                    return (<h1>No data.... </h1>);
                }*/
            }
            else{
                return (
                    <div>
                        {tableMembers}
                        {hateoasForm}
                        {/*<Button onClick={handleClickHateoas(memberAddedLinks[ 'Other emails' ])}>{memberAdded} : {Object.keys(memberAddedLinks)[1]} </Button>*/}
                    </div>);
            }
        }
    }

}

export default Table;
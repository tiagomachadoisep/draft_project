import React, {useContext, useEffect} from 'react';
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import {addCustomCategory, fetchCategories} from "../../context/Actions";
import AppContext from "../../context/AppContext";
import Spinner from "react-bootstrap/Spinner";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import AlertDismissible from "../relationships/AlertDismissable";
import {validCategoryName} from "../content/ConstantsUtils";

function FamilyCategoriesTable() {

    const {state, dispatch} = useContext(AppContext);
    const {familyCategories, userFamilyId, categories, role} = state;
    const {loading, error, data} = familyCategories;

    useEffect(() => {
        fetchCategories(dispatch, userFamilyId, state.token);

    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleReloadClick = () => {
        fetchCategories(dispatch, userFamilyId, state.token)
    }

    const familyCategoriesTable = (
        <div style={{margin: "10px"}}>
            <MaterialTable
                title="Family categories table"
                icons={tableIcons}
                localization={{
                    body: {
                        emptyDataSourceMessage: 'No categories to display.'
                    }
                }}
                actions={[
                    {
                        icon: tableIcons.Refresh,
                        tooltip: 'Refresh family categories',
                        isFreeAction: true,
                        onClick: handleReloadClick,
                    }
                ]}
                columns={[
                    {
                        title: "Category Name",
                        field: "name",
                        validate: rowData => rowData.name === undefined ?
                            false : (rowData.name === '' ?
                                false : (!!rowData.name.match(validCategoryName))),
                        editComponent: props => (
                            <TextField
                                type="text"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value === undefined ? false : (props.value === '' ?
                                    true : (!props.value.match(validCategoryName)))}
                                helperText={props.value === undefined ?
                                    '' : (props.value === '' ?
                                        'Cannot be empty.' : (props.value.match(validCategoryName)
                                            ? '' : 'Letters only.'))}
                                required={true}
                                InputProps={{'style': {fontSize: "13px"}}}
                                placeholder='e.g. Duties'
                                label="Category name"
                                />
                        )
                    },
                    {
                        title: "Parent Category",
                        field: "parentId",
                        validate: rowData => rowData.parentId === undefined ?
                            false : (categoriesIDsList.indexOf(rowData.parentId) !== -1),
                        editComponent: props => (
                            <Autocomplete
                                clearOnEscape
                                freeSolo
                                disableClearable
                                options={categoriesIDs}
                                getOptionLabel={(option) => option.id}
                                renderInput={(params) =>
                                    <TextField
                                        required
                                        {...params}
                                        value={props.value}
                                        onChange={e => props.onChange(e.target.value)}
                                        error={props.value === undefined ?
                                            false : (props.value === null ?
                                                true : (categoriesIDsList.indexOf(props.value) === -1))}
                                        helperText={props.value === undefined ?
                                            '' : (props.value === '' ?
                                                'Cannot be empty.' : (categoriesIDsList.indexOf(props.value) === -1) ?
                                                    'Non-existent category ID' : '')}
                                        label="Parent category"
                                        placeholder="e.g. 91610f9c-1de9-4b1c-980d-84615c06ccef"
                                        inputProps={{
                                            ...params.inputProps
                                        }}/>}
                                onChange={(event, value) => props.onChange(value?.id)}
                                renderOption={(option) => {
                                    return `${option.name} (${option.id})`
                                }}
                            />
                        )
                    }
                ]}
                data={data}
                editable={{
                    onRowAdd: newCustomCategory =>
                        new Promise((resolve, reject) => {
                            addCustomCategory(dispatch, newCustomCategory, userFamilyId, state.token);
                            resolve();
                        })
                }}
                options={{
                    rowStyle: {
                        backgroundColor: '#EEE',
                    },
                    actionsColumnIndex: -1,
                    pageSize: 7,
                    pageSizeOptions: [], // e.g. [5, 10, 20]
                    showEmptyDataSourceMessage: true,
                    padding: "default", //dense or default
                    searchFieldAlignment: "left",
                    searchFieldVariant: "outlined", //standard, outlined, filled
                    toolbarButtonAlignment: "left",
                    paging: false,
                    search: true,
                    filtering: false,
                    sorting: true,
                    showTitle: true,
                    toolbar: true,
                    thirdSortClick: false,
                    grouping: true,
                    addRowPosition: "first",
                    headerStyle: {
                        position: "sticky",
                        top: "0",
                        backgroundColor: '#41506b',
                        color: '#FFF'
                    },
                    maxBodyHeight: "350px"

                }}

            />
            <br/>
            <AlertDismissible variant="danger" error={error}/>
        </div>
    );

    if (role === "USER" && familyCategories.on) {
        return (
            <div style={{margin: "10px"}}>
                <MaterialTable
                    title="Family categories table"
                    icons={tableIcons}
                    localization={{
                        body: {
                            emptyDataSourceMessage: 'No categories to display.'
                        }
                    }}
                    actions={[
                        {
                            icon: tableIcons.Refresh,
                            tooltip: 'Refresh family categories',
                            isFreeAction: true,
                            onClick: handleReloadClick,
                        }
                    ]}
                    columns={[
                        {
                            title: "Category Name",
                            field: "name"
                        },
                        {
                            title: "Parent Category",
                            field: "parentId"
                        }
                    ]}
                    data={data}
                    options={{
                        rowStyle: {
                            backgroundColor: '#EEE',
                        },
                        actionsColumnIndex: -1,
                        pageSize: 7,
                        pageSizeOptions: [], // e.g. [5, 10, 20]
                        showEmptyDataSourceMessage: true,
                        padding: "default", //dense or default
                        searchFieldAlignment: "left",
                        searchFieldVariant: "outlined", //standard, outlined, filled
                        toolbarButtonAlignment: "left",
                        paging: false,
                        search: true,
                        filtering: false,
                        sorting: true,
                        showTitle: true,
                        toolbar: true,
                        thirdSortClick: false,
                        grouping: true,
                        addRowPosition: "first",
                        headerStyle: {
                            position: "sticky",
                            top: "0",
                            backgroundColor: '#41506b',
                            color: '#FFF'
                        },
                        maxBodyHeight: "350px"

                    }}

                />
                <br/>
                <AlertDismissible variant="danger" error={error}/>
            </div>
        );
    }

    const categoriesInfo = categories.data.map((category) => {
        return [category.id, category.name];
    });

    const categoriesIDs = categoriesInfo.map((categoryInfo) => {
        return {id: categoryInfo[0], name: categoryInfo[1]}
    });

    const categoriesIDsList = categories.data.map((row) => {
        return row.id
    })

    if (!familyCategories.on) {
        return null;
    } else {
        if (loading === true) {
            return (
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            );
        } else {
            return familyCategoriesTable;
        }
    }
}

export default FamilyCategoriesTable;
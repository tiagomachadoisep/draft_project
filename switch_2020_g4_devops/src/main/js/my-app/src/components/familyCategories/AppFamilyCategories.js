import React from 'react';
import FamilyCategoriesTable from "./FamilyCategoriesTable";

function AppFamilyCategories() {
    return (
        <div>
            <FamilyCategoriesTable/>
        </div>
    );
}

export default AppFamilyCategories;

import React, {useState, useEffect} from "react";
import Alert from 'react-bootstrap/Alert';


function AlertDismissible({variant, error, text}) {
    const [show, setShow] = useState(false);

    useEffect(() => {
        /*console.log(error)
        console.log(data)
        console.log('effect before setShow')*/

        setShow(true)
        /*console.log('effect after setShow')
        console.log(error)
        console.log(data)*/

    }, [error]);

    if (show && error !== null) {
        return (
            <div style={{display: "inline-block"}}>
                {/*<br/>*/}
                <Alert variant={variant} onClose={() => setShow(false)} dismissible>
                    {error} - {text}
                </Alert>
            </div>
        )
    } /*else if (show && error === null && data.length === 0) {
        return (
            <>
                <Alert variant="info">
                    There are no relationships in this family yet.
                </Alert>
            </>
        )
    }*/ /*else if (show && error === null && data.length > 0){
        console.log('rendering success alert')
        return (
            <>
                <Alert variant="success">
                    Relationship successfully added!
                </Alert>
            </>
        )
    }*/ else {
        return null;
    }

}

export default AlertDismissible;
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import RelationshipsTable from './RelationshipsTable';

function AppRelationships() {
    return (
        <div>
            <RelationshipsTable/>
        </div>
    );
}

export default AppRelationships;

import React, {useContext, useEffect} from 'react';
import AlertDismissible from './AlertDismissable'
import AppContext from '../../context/AppContext';
import {addRelationship, changeRelationship, fetchMembers, fetchRelationships} from '../../context/Actions';
import Spinner from 'react-bootstrap/Spinner';
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';


function RelationshipsTable() {
    const {state, dispatch} = useContext(AppContext);
    const {relationships, members, family, userFamilyId} = state;
    const {loading, on, data, error} = relationships;
    const membersData = members.data;

    useEffect(() => {
        console.log("use effect id " + family.id + '' + userFamilyId)
        fetchRelationships(dispatch, userFamilyId, state.token);
        fetchMembers(dispatch, userFamilyId, state.token);
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const transformMembersDataIntoObjectInfo = (membersData) => {
        const obj = {};
        membersData.forEach(member => obj[member.emailAddress] = member.name);

        return obj;
    }

    const membersObjectInfo = transformMembersDataIntoObjectInfo(membersData);

    const materialData = data.map((row) => {
        return (
            {

                firstPersonEmail: `${membersObjectInfo[row.firstPersonEmail]} (${row.firstPersonEmail})`,
                secondPersonEmail: `${membersObjectInfo[row.secondPersonEmail]} (${row.secondPersonEmail})`,
                designation: row.designation,
                relationshipId: row.relationshipId
            }
        )
    });

    const membersInfo = membersData.map((member) => {
        return [member.name, member.emailAddress];
    });

    const membersIDs = membersInfo.map((memberInfo) => {

        return {name: memberInfo[0], email: memberInfo[1]}
    });

    const existingMembersEmails = membersData.map((row) => {
        return row.emailAddress
    });

    const existingMembers = membersData.map((row) => {
        return `${row.name} (${row.emailAddress})`
    });

    const materialHeaders = [
        {
            title: "First member", field: "firstPersonEmail",
            editable: 'onAdd',
            validate: rowData => rowData.firstPersonEmail === undefined ?
                false : ((existingMembersEmails.indexOf(rowData.firstPersonEmail) !== -1) ||
                    (existingMembers.indexOf(rowData.firstPersonEmail) !== -1)),
            editComponent: props => (
                <Autocomplete
                    disableClearable
                    freeSolo
                    clearOnEscape
                    options={membersIDs}
                    getOptionLabel={(option) => option.email}
                    renderInput={(params) =>
                        <TextField {...params}
                                   required
                                   label="First member"
                                   value={props.value}
                                   onChange={e => props.onChange(e.target.value)}
                                   placeholder="e.g. ex@example.com"
                                   error={props.value === undefined ?
                                       false : (props.value === null ?
                                           true : (existingMembersEmails.indexOf(props.value) === -1))}
                                   helperText={props.value === undefined ?
                                       '' : (props.value === '' ?
                                           'Cannot be empty.' : (existingMembersEmails.indexOf(props.value) === -1)
                                               ? 'Non-existent member.' : '')}
                        />}
                    onChange={(event, value) =>
                        props.onChange(value?.email)}
                    renderOption={(option) => {
                        return `${option.name} (${option.email})`
                    }}
                />
            )
        },
        {
            title: "Second member", field: "secondPersonEmail",
            editable: 'onAdd',
            validate: rowData => rowData.secondPersonEmail === undefined ?
                false : ((existingMembersEmails.indexOf(rowData.secondPersonEmail) !== -1) ||
                    (existingMembers.indexOf(rowData.secondPersonEmail) !== -1)),
            editComponent: props => (
                <Autocomplete
                    clearOnEscape
                    freeSolo
                    disableClearable
                    options={membersIDs}
                    getOptionLabel={(option) => option.email}
                    renderInput={(params) =>
                        <TextField {...params}
                                   required
                                   label="Second member"
                                   value={props.value}
                                   onChange={e => props.onChange(e.target.value)}
                                   placeholder="e.g. ex@example.com"
                                   error={props.value === undefined ?
                                       false : (props.value === null ?
                                           true : (existingMembersEmails.indexOf(props.value) === -1))}
                                   helperText={props.value === undefined ?
                                       '' : (props.value === '' ?
                                           'Cannot be empty.' : (existingMembersEmails.indexOf(props.value) === -1) ?
                                               'Non-existent member.' : '')}
                        />}
                    onChange={(event, newValue) =>
                        props.onChange(newValue?.email)}
                    renderOption={(option) => {
                        return `${option.name} (${option.email})`
                    }}
                />
            )
        },
        {
            title: "Designation", field: "designation",
            validate: rowData => rowData.designation === undefined ?
                false : (relationTypes.indexOf(rowData.designation) !== -1),
            editComponent: props => (
                <Autocomplete
                    options={relationTypesEntries}
                    clearOnEscape
                    freeSolo
                    disableClearable
                    getOptionLabel={(option) => option.relationType}
                    renderInput={(params) =>
                        <TextField required {...params}
                                   value={props.value}
                                   onChange={e => props.onChange(e.target.value)}
                                   error={props.value === undefined ?
                                       false : (props.value === null ?
                                           true : (relationTypes.indexOf(props.value) === -1))}
                                   helperText={props.value === undefined ?
                                       '' : (props.value === '' ?
                                           'Cannot be empty.' : (relationTypes.indexOf(props.value) === -1) ?
                                               'Invalid designation.' : '')}
                                   label="Designation" /*variant="outlined"*/
                                   placeholder="e.g. mother"
                        />}
                    onChange={e => props.onChange(e.target.innerText)}

                />
            )
            /*editComponent: props => (
                <Select
                    style={{ width: "100%" }}
                    value={props.value}
                    onChange={e => props.onChange(e.target.value)}
                >
                    {/!*{membersIDs.map((memberID) => (
                        <MenuItem  value={memberID.email} >
                            {`${memberID.name} (${memberID.email})`}
                        </MenuItem>
                    ))}*!/}
                    {relationTypes.map((type) => (
                        <MenuItem value={type} >
                            {type}
                        </MenuItem>
                    ))}
                </Select>
            )*/
        },
        {
            title: "Id", field: "relationshipId", hidden: true
        }
    ]
    const relationTypes = ['mother', 'father', 'son', 'daughter', 'husband', 'wife', 'brother', 'sister', 'cousin',
        'aunt', 'uncle', 'nephew', 'niece', 'grandfather', 'grandmother', 'grandson',
        'granddaughter', 'friend', 'boyfriend', 'girlfriend', 'sister-in-law', 'brother-in-law',
        'father-in-law', 'mother-in-law', 'stepfather', 'stepmother', 'stepdaughter', 'stepson',
        'stepsister', 'stepbrother'];

    const relationTypesEntries = relationTypes.map((relation, index) => {

        return {relationType: relation, label: "valid designation"}
    });

    const handleReloadClick = () => {
        console.log("use effect id " + family.id + ' userFamId: ' + userFamilyId)
        fetchMembers(dispatch, userFamilyId, state.token)
        fetchRelationships(dispatch, userFamilyId, state.token)
        console.log(membersIDs);
        console.log(membersInfo);
    }


    const tableRelationships = (
        <div>
            <div>
                {/*<table border="1">
                    <RelationshipsTableHeader/>
                    <RelationshipsTableBody/>
                </table>*/}
                <MaterialTable
                    title="Family relationships table"
                    icons={tableIcons}
                    /*components={{
                        Toolbar: props => (
                            <div style={{ backgroundColor: '#6895CF' }}>
                                <MTableToolbar {...props} />
                            </div>

                                ),
                                }}*/
                    actions={[
                        {
                            icon: tableIcons.Refresh,
                            tooltip: 'Refresh relationships',
                            isFreeAction: true,
                            onClick: handleReloadClick,
                        }
                    ]}
                    localization={{
                        body: {
                            emptyDataSourceMessage: 'No relationships to display.',
                            addTooltip: "Create new relationship"
                        }
                    }}
                    options={{
                        rowStyle: {
                            backgroundColor: '#EEE',
                        },
                        pageSize: 7,
                        pageSizeOptions: [], // e.g. [5, 10, 20]
                        showEmptyDataSourceMessage: true,
                        padding: "default", //dense or default
                        searchFieldAlignment: "left",
                        searchFieldVariant: "outlined", //standard, outlined, filled
                        toolbarButtonAlignment: "left",
                        paging: false,
                        search: true,
                        filtering: false,
                        sorting: true,
                        showTitle: true,
                        toolbar: true,
                        thirdSortClick: false,
                        grouping: true,
                        addRowPosition: "first",
                        actionsColumnIndex: -1,
                        headerStyle: {
                            position: "sticky",
                            top: "0",
                            backgroundColor: '#41506b',
                            color: '#FFF'
                        },
                        maxBodyHeight: "350px"
                    }}
                    editable={{
                        onRowAdd: newRelationship =>
                            new Promise((resolve, reject) => {
                                /*setTimeout(() => {*/
                                /*setData([...data, newData]);*/
                                console.log(newRelationship)
                                console.log(family)
                                addRelationship(dispatch, newRelationship, userFamilyId, state.token);

                                resolve();
                                /*}, 3000)*/
                            }),
                        onRowUpdate: (newDesignation, oldDesignation) =>
                            new Promise((resolve, reject) => {
                                console.log(newDesignation, oldDesignation)
                                const index = oldDesignation.tableData.id;
                                changeRelationship(dispatch, newDesignation, userFamilyId, state.token, index);

                                resolve();
                            })
                    }}
                    data={materialData}
                    columns={materialHeaders}
                />
            </div>
            <hr/>
            {/*<RelationshipAdder/>*/}
            {/*<br/>*/}
            <AlertDismissible variant="danger" error={error}/>
        </div>
    );

    const submissionSpinner = (
        <Spinner animation="border" role="status" size="lg">
        </Spinner>
    )

    if (!on) {
        return null;
    } else {
        if (loading === true) {
            return submissionSpinner;
        } else {
            return tableRelationships;
            /* if (error !== null) {
                return tableAlertError;
            } else {
                if (relationships.length > 0) {
                    return tableWithRelationships;
                } else {
                    return tableWithoutRelationships;
                }
            } */
        }
    }


}

export default RelationshipsTable;
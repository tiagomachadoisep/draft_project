import React, {useContext, useEffect} from 'react';
import AppContext from "../../context/AppContext";
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import Spinner from "react-bootstrap/Spinner";
import AlertDismissable from "../relationships/AlertDismissable";
import {addEmail, deleteEmail, showEmail} from "../../context/Actions";
import {validEmailRegex} from "../content/ConstantsUtils";
import {TextField} from "@material-ui/core";


function TableEmail() {
    const {state, dispatch} = useContext(AppContext);
    const {emails, info, loggedUser} = state;
    const {loading, error, data, on} = emails;
    const {id} = info;

    const handleReloadClick = () => {
        showEmail(dispatch, state.token, id)
    }

    useEffect(() => {
        console.log(loggedUser);
        showEmail(dispatch, state.token, loggedUser);
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const emailsList = data.map((row) =>{
        return (
            {
                otherEmails: row
            }
        )
    });

    const tableEmail = (<div>
        <MaterialTable
            icons={tableIcons}
            actions={[
                {
                    icon: tableIcons.Refresh,
                    tooltip: 'Refresh',
                    isFreeAction: true,
                    onClick: handleReloadClick,
                }
            ]}
            localization={{
                body: {
                    emptyDataSourceMessage: 'No info to display.'
                }
            }}
            options={{
                selection: false,
                actionsColumnIndex: -1,
                rowStyle: {
                    backgroundColor: '#EEE',
                },
                pageSize: 5,
                pageSizeOptions: [5,10], // e.g. [5, 10, 20]
                showEmptyDataSourceMessage: true,
                padding: "default", //dense or default
                toolbarButtonAlignment: "left",
                paging: true,
                search: true,
                searchFieldAlignment: "left",
                filtering: false,
                sorting: false,
                showTitle: true,
                toolbar: true,
                thirdSortClick:false,
                grouping: false,
                addRowPosition: "first",
                headerStyle: {
                    position: "sticky",
                    top: "0",
                    backgroundColor: '#41506b', //estava #01579b
                    color: '#FFF'
                },
                maxBodyHeight: "350px"
            }}
            editable={{
                onRowDelete: selectedRow =>
                    new Promise((resolve, reject) => {
                        console.log("TableEmail - on delete row")
                        console.log("\tDeleting email -> ", selectedRow.otherEmails) //Undefine
                        console.log("\tID -> ", id)
                        deleteEmail(dispatch, state.token, id, selectedRow.otherEmails)
                        resolve();

                    }),
                onRowAdd: newEmail =>
                    new Promise((resolve, reject) => {
                        addEmail(dispatch, newEmail, state.token, id);
                        resolve();

                    })
            }}
            columns={[
                {
                    title: "Other Emails",
                    field: "otherEmails",
                    validate: rowData =>
                        rowData.otherEmails !== undefined ?
                            ((!(rowData.otherEmails === '' || !rowData.otherEmails.match(validEmailRegex)))) : false,
                    editComponent: props => (
                        <TextField
                            type="email"
                            label="Email"
                            value={props.value}
                            onChange={e => props.onChange(e.target.value)}
                            error={props.value !== undefined ?
                                ((props.value === '' || !props.value.match(validEmailRegex))) : false}
                            helperText={props.value === undefined ? '' :
                                (props.value === '' ? 'Cannot be empty.' :
                                    (props.value.match(validEmailRegex) ? '' : "Invalid format."))}
                            required={true}
                            placeholder="e.g. ex@gmail.com"
                            InputProps={{'style': {fontSize: "13px"}}}
                        />
                    )
                }
            ]} data={emailsList}
            title="Other Emails"/>
        <hr/>
        <AlertDismissable variant="danger" error={error}/>
    </div>)

    if(on === false || info.on === false){
        return null;
    }
    if (loading === true) {
        return (
            <>
                <h1>Loading ....</h1>
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            </>
        );
    } else {
       /* if (error !== null) {
            return (
                <h1>Error ....</h1>,
                tableEmail
            );
        } else {
            if (data.length > 0) {*/
                return tableEmail;
         /*   } else {
                return (
                    <h1>No data ....</h1>,
                    tableEmail
                )

            };
        }*/
    }

};

export default TableEmail;
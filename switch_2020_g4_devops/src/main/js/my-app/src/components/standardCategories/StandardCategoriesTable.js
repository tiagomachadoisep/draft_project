import React, {useContext, useEffect} from 'react';
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import {addStandardCategory, fetchStandardCategories, selectParentCategory} from "../../context/Actions";
import AppContext from "../../context/AppContext";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import {validCategoryName} from "../content/ConstantsUtils";
import AlertDismissable from "../relationships/AlertDismissable";

function StandardCategoriesTable() {

    const {state, dispatch} = useContext(AppContext);
    const {categories, role} = state;
    const {loading, error, data} = categories;

    useEffect(() => {
        fetchStandardCategories(dispatch, state.token);
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const handleReloadClick = () => {
        fetchStandardCategories(dispatch, state.token)
    }

    function handleOnClickRow(id) {
        selectParentCategory(dispatch, id);
    }
    const categoriesIDsList = categories.data.map((row) => {
        return row.id
    })

    const standardCategoriesTable = (
        <div style={{margin: "10px"}}>
            <MaterialTable
                title="Standard categories table"
                icons={tableIcons}
                onRowClick={(event, rowData) => handleOnClickRow(rowData.id)}
                actions={[
                    {
                        icon: tableIcons.Refresh,
                        tooltip: 'Refresh standard categories',
                        isFreeAction: true,
                        onClick: handleReloadClick,
                    }
                ]}
                editable={{
                    onRowAdd: newStandardCategory =>
                        new Promise((resolve, reject) => {
                            addStandardCategory(dispatch, newStandardCategory, state.token);
                            resolve();
                            handleReloadClick();
                        })
                }}
                columns={[
                    {
                        title: "Category ID",
                        field: "id",
                        editable: 'never'
                    },
                    {
                        title: "Category Name",
                        field: "name",
                        validate: rowData => rowData.name === undefined ?
                            false : (rowData.name === '' ?
                                false : (!!rowData.name.match(validCategoryName))),
                        editComponent: props => (
                            <TextField
                                type="text"
                                value={props.value}
                                onChange={e => props.onChange(e.target.value)}
                                error={props.value === undefined ? false : (props.value === '' ?
                                    true : (!props.value.match(validCategoryName)))}
                                helperText={props.value === undefined ?
                                    '' : (props.value === '' ?
                                        'Cannot be empty.' : (props.value.match(validCategoryName)
                                            ? '' : 'Letters only.'))}
                                required={true}
                                InputProps={{'style': {fontSize: "13px"}}}
                                placeholder='e.g. Duties'
                                label="Category name"
                            />
                        )
                    },
                    {
                        title: "Parent ID",
                        field: "parentId",
                        editComponent: props => (
                            <Autocomplete
                                clearOnEscape
                                freeSolo
                                disableClearable
                                options={categoriesIDs}
                                getOptionLabel={(option) => option.id}
                                renderInput={(params) =>
                                    <TextField
                                        {...params}
                                        value={props.value}
                                        onChange={e => props.onChange(e.target.value)}
                                        error={props.value === undefined ?
                                            false : (props.value === '' ?
                                                false : (categoriesIDsList.indexOf(props.value) === -1))}
                                        helperText={props.value === undefined ?
                                            '' : (props.value === '' ?
                                                '' : (categoriesIDsList.indexOf(props.value) === -1) ?
                                                    'Non-existent category ID' : '')}
                                        label="Parent category"
                                        placeholder="e.g. 91610f9c-1de9-4b1c-980d-84615c06ccef"
                                        inputProps={{
                                            ...params.inputProps
                                        }}/>}
                                onChange={(event, value) => props.onChange(value?.id)}
                                renderOption={(option) => {
                                    return `${option.id} (${option.name})`
                                }}
                            />
                        )
                        /*editComponent: props => (
                            <Autocomplete
                                clearOnEscape
                                options={categoriesIDs}
                                getOptionLabel={(option) => option.name}
                                renderInput={(params) =>
                                    <TextField
                                        required
                                        {...params}
                                        label="Select parent category"
                                        inputProps={{
                                            ...params.inputProps
                                        }}/>}
                                onChange={(event, value) => props.onChange(value?.id)}
                                renderOption={(option) => {
                                    return `${option.id} (${option.name})`
                                }}
                            />
                        )*/
                    },

                ]}
                data={data}
                options={{
                    rowStyle: {
                        backgroundColor: '#EEE',
                    },
                    pageSize: 7,
                    pageSizeOptions: [], // e.g. [5, 10, 20]
                    showEmptyDataSourceMessage: true,
                    padding: "default", //dense or default
                    searchFieldAlignment: "left",
                    searchFieldVariant: "outlined", //standard, outlined, filled
                    toolbarButtonAlignment: "left",
                    actionsColumnIndex: -1,
                    paging: false,
                    search: true,
                    filtering: false,
                    sorting: true,
                    showTitle: true,
                    toolbar: true,
                    thirdSortClick: false,
                    grouping: true,
                    addRowPosition: "first",
                    headerStyle: {
                        position: "sticky",
                        top: "0",
                        backgroundColor: '#41506b',
                        color: '#FFF'
                    },
                    maxBodyHeight: "350px"

                }}

            />
            <AlertDismissable variant="danger" error={error} />
        </div>
    );

    const categoriesInfo = categories.data.map((category) => {
        return [category.id, category.name];
    });

    const categoriesIDs = categoriesInfo.map((categoryInfo) => {
        return {id: categoryInfo[0], name: categoryInfo[1]}
    });

    if (!categories.on) {
        return null;
    } else {
        if (role === undefined) {
            console.log("ROLE UNDEFINED")
            return null
        } else {
            console.log("ROLE IS DEFINED!")
            console.log(role);
            if (loading === true) {
                return (<h1>Loading ......</h1>);
            } else {
                /*if (error !== null) {*/
                    return (
                        /*<>{standardCategoriesTable}
                            <h1>{error}</h1>
                        </>*/
                        standardCategoriesTable
                    )
                /*} else {
                    if (data.length > 0) {
                        return standardCategoriesTable;
                    } else {
                        return (<h1>No data.... </h1>);
                    }
                }*/
            }
        }
    }
}

export default StandardCategoriesTable;
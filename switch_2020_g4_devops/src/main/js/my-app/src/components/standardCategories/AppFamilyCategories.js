import React from 'react';
import StandardCategoriesTable from "./StandardCategoriesTable";

function AppStandardCategories() {
    return (
        <div>
            <StandardCategoriesTable/>
        </div>
    );
}

export default AppStandardCategories;

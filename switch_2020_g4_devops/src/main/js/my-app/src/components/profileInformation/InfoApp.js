import React from 'react';
import Table from './Table';
import TableEmail from '../otherEmails/TableEmail';

function InfoApp() {
  return (
    <div>
        <Table/>
        <TableEmail/>
    </div>
  );
}
export default InfoApp;

import React, {useContext} from 'react';
import AppContext from "../../context/AppContext";
import {fetchInfo, toggleOtherEmails} from "../../context/Actions";
import MaterialTable from "material-table";
import {tableIcons} from "../content/MaterialTableIcons";
import Spinner from "react-bootstrap/Spinner";
import AlertDismissable from "../relationships/AlertDismissable";
import {Button} from "react-bootstrap";

function Table() {
    const { state, dispatch } = useContext(AppContext);
    const { info } = state;
    const { id,loading, error, data, on } = info;

    const handleReloadClick = () => {
        fetchInfo(dispatch,id,state.token)
    }
    const handleOnClick = () => {
        toggleOtherEmails(dispatch, state.emails.on)
    }

    const tableProfile = (<div>
        <MaterialTable
            icons={tableIcons}
            actions={[
                {
                    icon: tableIcons.Refresh,
                    tooltip: 'Refresh',
                    isFreeAction: true,
                    onClick: handleReloadClick,
                }
            ]}
            localization={{
                body: {
                    emptyDataSourceMessage: 'No info to display.'
                }
            }}
            options={{
                rowStyle: {
                    backgroundColor: '#EEE',
                },
                pageSize: 7,
                pageSizeOptions: [], // e.g. [5, 10, 20]
                showEmptyDataSourceMessage: true,
                padding: "default", //dense or default
                toolbarButtonAlignment: "left",
                paging: false,
                search: false,
                filtering: false,
                sorting: false,
                showTitle: true,
                toolbar: true,
                thirdSortClick: false,
                grouping: false,
                addRowPosition: "first",
                headerStyle: {
                    position: "sticky",
                    top: "0",
                    backgroundColor: '#41506b',
                    color: '#FFF'
                },
                maxBodyHeight: "350px"
            }}
            columns={[
                {
                    title: "Name",
                    field: "name",
                    editable: "never"
                },
                {
                    title: "VAT Number",
                    field: "vatNumber",
                },
                {
                    title: "Email Address",
                    field: "emailAddress",
                },
                {
                    title: "Address",
                    field: "address",
                },
                {
                    title: "Family ID",
                    field: "familyID",
                },
                {
                    title: "Birth Date",
                    field: "birthDate",
                },
            ]} data={data}
            title="Profile Information"/>
        <Button variant={'primary'} style={{backgroundColor:'#41506b', marginTop:'20px'}} onClick={handleOnClick}>Other emails</Button>
        <hr/>

        <AlertDismissable variant="danger" error={error}/>
    </div>)

    if(on===false){
        return null;
    }
    else {
        if (loading === true) {
            return (
                <Spinner animation="border" role="status" size="lg">
                </Spinner>
            );
        } else {
            if (error !== null) {
                return (<h1>Error ....</h1>);
            } else {
                if (data.length > 0) {
            return (
                tableProfile

            );
            } else {
                return (<h1>No data ....</h1>);
            }
        }
        }
    }
}
export default Table;
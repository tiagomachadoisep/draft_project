import {
    ACCOUNTS_TOGGLE_OFF,
    ACCOUNTS_TOGGLE_ON,
    ADD_ACCOUNT_FAILURE,
    ADD_ACCOUNT_STARTED,
    ADD_ACCOUNT_SUCCESS,
    ADD_CATEGORY_FAILURE,
    ADD_CATEGORY_STARTED,
    ADD_CATEGORY_SUCCESS,
    ADD_EMAIL_FAILURE,
    ADD_EMAIL_STARTED,
    ADD_EMAIL_SUCCESS,
    ADD_FAMILY_FAILURE,
    ADD_FAMILY_STARTED,
    ADD_FAMILY_SUCCESS,
    ADD_MEMBER_FAILURE,
    ADD_MEMBER_STARTED,
    ADD_MEMBER_SUCCESS,
    ADD_RELATIONSHIP_FAILURE,
    ADD_RELATIONSHIP_STARTED,
    ADD_RELATIONSHIP_SUCCESS,
    ADD_STANDARD_CATEGORY_FAILURE,
    ADD_STANDARD_CATEGORY_STARTED,
    ADD_STANDARD_CATEGORY_SUCCESS,
    CATEGORIES_TOGGLE_OFF,
    CATEGORIES_TOGGLE_ON,
    CHANGE_RELATIONSHIP_FAILURE,
    CHANGE_RELATIONSHIP_STARTED,
    CHANGE_RELATIONSHIP_SUCCESS,
    DELETE_EMAIL_FAILURE,
    DELETE_EMAIL_STARTED,
    DELETE_EMAIL_SUCCESS,
    FAMILIES_TOGGLE_OFF,
    FAMILIES_TOGGLE_ON,
    FETCH_ACCOUNTS_FAILURE,
    FETCH_ACCOUNTS_STARTED,
    FETCH_ACCOUNTS_SUCCESS,
    FETCH_CATEGORIES_FAILURE,
    FETCH_CATEGORIES_STARTED,
    FETCH_CATEGORIES_SUCCESS,
    FETCH_INFO_FAILURE,
    FETCH_INFO_STARTED,
    FETCH_INFO_SUCCESS,
    FETCH_LEDGERID_FAILURE,
    FETCH_LEDGERID_STARTED,
    FETCH_LEDGERID_SUCCESS,
    FETCH_MEMBERS_FAILURE,
    FETCH_MEMBERS_STARTED,
    FETCH_MEMBERS_SUCCESS,
    FETCH_MOVEMENTS_FAILURE,
    FETCH_MOVEMENTS_STARTED,
    FETCH_MOVEMENTS_SUCCESS,
    FETCH_RELATIONSHIPS_FAILURE,
    FETCH_RELATIONSHIPS_STARTED,
    FETCH_RELATIONSHIPS_SUCCESS,
    FETCH_STANDARD_CATEGORIES_FAILURE,
    FETCH_STANDARD_CATEGORIES_STARTED,
    FETCH_STANDARD_CATEGORIES_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_USER_SUCCESS,
    GET_FAMILIES_FAILURE,
    GET_FAMILIES_STARTED,
    GET_FAMILIES_SUCCESS,
    GET_FAMILY_FAILURE,
    GET_FAMILY_STARTED,
    GET_FAMILY_SUCCESS,
    GET_FAMILY_USER_FAILURE,
    GET_FAMILY_USER_STARTED,
    GET_FAMILY_USER_SUCCESS,
    GET_USER_ROLE_FAILURE,
    GET_USER_ROLE_STARTED,
    GET_USER_ROLE_SUCCESS,
    INFO_TOGGLE_OFF,
    INFO_TOGGLE_ON,
    LOGGING_OUT_DONE,
    LOGIN,
    LOGIN_SIMPLE_FAILURE,
    LOGIN_SIMPLE_STARTED,
    LOGIN_SIMPLE_SUCCESS,
    LOGOUT,
    MEMBERS_TOGGLE_OFF,
    MEMBERS_TOGGLE_ON,
    OTHER_EMAILS_TOGGLE_OFF,
    OTHER_EMAILS_TOGGLE_ON,
    RELATIONSHIPS_TOGGLE_OFF,
    RELATIONSHIPS_TOGGLE_ON,
    SELECT_PARENT_CATEGORY_SUCCESS,
    SHOW_EMAIL_FAILURE,
    SHOW_EMAIL_STARTED,
    SHOW_EMAIL_SUCCESS,
    STANDARD_CATEGORIES_TOGGLE_OFF,
    STANDARD_CATEGORIES_TOGGLE_ON,
    UNGET_FAMILY,
    WELCOME_TOGGLE_OFF,
} from "./Actions"

function reducer(state, action) {
    switch (action.type) {

        case WELCOME_TOGGLE_OFF:
            return {
                ...state,
                welcome: {
                    on: false,
                }
            }

//ADD OTHER EMAILS----------------------------------------------------------------------------------------------
        case ADD_EMAIL_STARTED:
            return {
                ...state,
                emails: {
                    on: state.emails.on,
                    loading: true,
                    error: null,
                    data: state.emails.data,
                }
            }
        case ADD_EMAIL_SUCCESS:
            return {
                ...state,
                emails:{
                    on: state.emails.on,
                    loading: false,
                    error: null,
                    data: [action.payload.success, ...state.emails.data],
                }

            }
        case ADD_EMAIL_FAILURE:
            return{
                ...state,
                emails:{
                    on: state.emails.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }

            }
        case  OTHER_EMAILS_TOGGLE_ON:
            return{
                ...state,
                emails:{
                    on: true,
                    loading: false,
                    error: null,
                    data: state.emails.data,
                }
            }
        case  OTHER_EMAILS_TOGGLE_OFF:
            return{
                ...state,
                emails:{
                    on: false,
                    loading: false,
                    error: null,
                    data: state.emails.data,
                }
            }


//SHOW Emails-------------------------------------------------------------------------------
        case SHOW_EMAIL_STARTED:
            console.log('SHOW_EMAIL_STARTED');
            return{
                ...state,
                emails : {
                    on: state.emails.on,
                    loading: true,
                    error: null,
                    data: [],
                }

            }
        case SHOW_EMAIL_SUCCESS:
            console.log('SHOW_EMAIL_SUCCESS');
            return {
                ...state,
                emails: {
                    on: state.emails.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }
        case SHOW_EMAIL_FAILURE:
            console.log('SHOW_EMAIL_FAILURE');
            return{
                ...state,
                emails: {
                    on: state.emails.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }

//DELETE EMAIL
        case DELETE_EMAIL_STARTED:
            return {
                ...state,
                loading: true,
                error: false,
                data: [],
            }
        case DELETE_EMAIL_SUCCESS:
            return {
                ...state,
                emails: {
                    on: state.emails.on,
                    loading: false,
                    error: null,
                    data: [
                        ...state.emails.data.filter(emails => emails !== action.payload.data),
                    ],
                },
            }
        case DELETE_EMAIL_FAILURE:
            return {
                ...state,
                emails: {
                    on: state.emails.on,
                    loading: false,
                    error: action.payload.error,
                    data: [...state.emails.data],
                },
            }

// LOGINSIMPLE -------------------------------------------------------------------------------------------------------
        case LOGIN_SIMPLE_STARTED:
            return {
                ...state,
                loggedUser: action.payload.email,
                loginMessage: "Loading..."
            }

        case LOGIN_SIMPLE_SUCCESS:
            return {
                ...state,
                token: action.payload.token,
                loginMessage: undefined
            }

        case LOGIN_SIMPLE_FAILURE:
            return {
                ...state,
                token: undefined,
                loginMessage: action.payload.error
            }

        case LOGGING_OUT_DONE:
            return {
                ...state,
                token: undefined,
                loggedUser: undefined,
                loginMessage: undefined,
                role: undefined,
                userFamilyId: undefined,
                username: undefined,
                welcome: {
                    on: true
                },
//FAMILIES --------------------------------------------------------------------------------------------------------
                families: {
                    on: false,
                    loading: true,
                    error: null,
                    data: [],
                },
                family: {
                    familyid: 0,
                    name: "",
                    adminEmail: "",
                    loading: false,
                    error: null,
//        data: [],
                },
//----------------------------------------------------------------------------------------------------------------
// MEMBERS
                members: {
                    on: false,
                    loading: true,
                    error: null,
                    data: [],
                },
//----------------------------------------------------------------------------------------------------------------
//CATEGORIES -----------------------------------------------------------------------------------------------------
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [],
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [],
                },
                form: {
                    id: "Click on the desired parent category from the standard categories table",
                },
//----------------------------------------------------------------------------------------------------------
// RELATIONSHIPS -------------------------------------------------------------------------------------------
                relationships: {
                    on: false,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: []
                },


//---------------------------------------------------------------------------------------------------------------
// PROFILE INFORMATION ------------------------------------------------------------------------------------------
                info: {
                    id: "",
                    on: false,
                    loading: true,
                    error: null,
                    data: [],
                },
//---------------------------------------------------------------------------------------------------------------
//ACCOUNTS------------------------------------------------------------------------------------------------------------
                accounts: {
                    on: false,
                    loading: true,
                    error: null,
                    data: [],
                },

//---------------------------------------------------------------------------------------------------------------
//LOGIN------------------------------------------------------------------------------------------------------------
                user: {
                    isLogged: false,
                    error: null,
                    data: [],
                },
//-------------------------------------------------------------------------------------------------------------------
//EMAILS------------------------------------------------------------------------------------------------------------
                emails: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [],
                }
            };

// USER ROLES ------------------------------------------------------------------------------------------------------

        case GET_USER_ROLE_STARTED:
            return {
                ...state
            }

        case GET_USER_ROLE_SUCCESS:
            return {
                ...state,
                role: action.payload.role,
            }

        case GET_USER_ROLE_FAILURE:
            return {
                ...state,
                role: state.role,
                loginMessage: action.payload.error
            }

        case GET_FAMILY_USER_STARTED:
            return {
                ...state
            }

        case GET_FAMILY_USER_SUCCESS:
            return {
                ...state,
                userFamilyId: action.payload.userFamilyId,
            }

        case GET_FAMILY_USER_FAILURE:
            return {
                ...state,
                userFamilyId: undefined,
            }

//FAMILIES -----------------------------------------------------------------------------------------------------------
        case GET_FAMILIES_STARTED:
            return {
                ...state,
                families: {
                    on: state.families.on,
                    loading: true,
                    error: null,
                    data: [],
                }
            }

        case GET_FAMILIES_SUCCESS:
            return {
                ...state,
                families: {
                    on: state.families.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data],
                }
            }

        case GET_FAMILIES_FAILURE:
            return {
                ...state,
                families: {
                    on: state.families.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }

        case GET_FAMILY_STARTED:
            console.log("previous family")
            console.log(state.family.familyid)
            console.log("trying to get new")
            return {
                ...state,
                family: {
                    familyid: action.payload.familyid,
                    loading: true,
                    error: null,
                    //data : [],
                    name: "",
                    adminEmail: "",
                }
            }

        case GET_FAMILY_SUCCESS:
            console.log("success getting family")
            console.log(state.family.familyid)
            return {
                ...state,
                family: {
                    familyid: state.family.familyid,
                    name: action.payload.name,
                    adminEmail: action.payload.adminEmail,
                    loading: false,
                    error: null,
                    //data : [...action.payload.data],
                }
            }

        case GET_FAMILY_FAILURE:
            console.log("failure getting family")
            return {
                ...state,
                family: {
                    familyid: 0,
                    loading: false,
                    error: action.payload.error,
                    //data : [],
                    name: "",
                    adminEmail: ""
                }
            }

        case UNGET_FAMILY:
            return {
                ...state,
                family: {
                    loading: false,
                    error: null,
                    //data: [],
                    familyid: 0,
                    name: "",
                    adminEmail: "",
                }
            }

        case ADD_FAMILY_STARTED:
            console.log("start adding family")
            return {
                ...state,
                /*family: {
                    familyid: 0,
                    loading: true,
                    error: null,
                    name: action.payload.name,
                    adminEmail: action.payload.adminEmail,
                    //data: []
                }*/
                families: {
                    on: state.families.on,
                    loading: true,
                    error: state.families.error,
                    data: state.families.data
                }
            }

        case ADD_FAMILY_SUCCESS:
            console.log("success adding family")
            console.log(state.family.name)

            return {
                ...state,
                /*family: {
                    familyid: action.payload.familyid,
                    loading: false,
                    error: null,
                    name: state.family.name,
                    adminEmail: state.family.adminEmail,
                    data: [...action.payload.data]
                }*/
                families: {
                    on: state.families.on,
                    loading: false,
                    error: null,
                    data: [action.payload.data, ...state.families.data]
                }
                //update families too (with family from response), maybe this will re-render
            }

        case ADD_FAMILY_FAILURE:
            console.log("fail adding family")
            console.log(action.payload.error)
            return {
                ...state,
                /*family: {
                    familyid: state.family.familyid,
                    loading: false,
                    error: action.payload.error,
                    name: "",
                    adminEmail: ""
                    data: []
                }*/
                families: {
                    on: state.families.on,
                    loading: false,
                    error: action.payload.error,
                    data: state.families.data
                }
            }

        case FAMILIES_TOGGLE_ON:
            return {
                ...state,
                families: {
                    on: true,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                relationships: {
                    on: false,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: [...state.relationships.data]
                },
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                },
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case FAMILIES_TOGGLE_OFF:
            return {
                ...state,
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                }
            }
//--------------------------------------------------------------------------------------------------------------------
// MEMBERS -----------------------------------------------------------------------------------------------------------
        case FETCH_MEMBERS_STARTED:
            return {
                ...state,
                members: {
                    on: state.members.on,
                    loading: true,
                    error: null,
                    data: []
                }
            }
        case FETCH_MEMBERS_SUCCESS:
            return {
                ...state,
                members: {
                    on: state.members.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }
        case FETCH_MEMBERS_FAILURE:
            return {
                ...state,
                members: {
                    on: state.members.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }
        case ADD_MEMBER_STARTED:
            return {
                ...state,
                members: {
                    on: state.members.on,
                    loading: true,
                    error: null,
                    data: state.members.data
                },
                memberAdded: null,
                memberAddedLinks : []
            }
        case ADD_MEMBER_SUCCESS:
            console.log("chegou ao reducer success")
            return {
                ...state,
                members: {
                    on: state.members.on,
                    loading: false,
                    error: null,
                    data: [...state.members.data, action.payload.data],
                },
                memberAdded: action.payload.data.name,
                memberAddedLinks : action.payload.data._links,
            }
        case ADD_MEMBER_FAILURE:
            console.log("chegou ao reducer failure")
            return {
                ...state,
                members: {
                    on: state.members.on,
                    loading: false,
                    error: action.payload.error,
                    data: state.members.data,
                },
                memberAdded: null,
                memberAddedLinks : []
            }
        case MEMBERS_TOGGLE_ON:
            return {
                ...state,
                members: {
                    on: true,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                relationships: {
                    on: false,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: [...state.relationships.data]
                },
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                },
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case MEMBERS_TOGGLE_OFF:
            return {
                ...state,
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                }
            }
//--------------------------------------------------------------------------------------------------------------------

//CATEGORIES ---------------------------------------------------------------------------------------------------------

        case FETCH_STANDARD_CATEGORIES_STARTED:
            return {
                ...state,
                categories: {
                    on: state.categories.on,
                    loading: true,
                    error: null,
                    data: []
                }
            }

        case FETCH_STANDARD_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: {
                    on: state.categories.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }

        case FETCH_STANDARD_CATEGORIES_FAILURE:
            return {
                ...state,
                categories: {
                    on: state.categories.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }

        case SELECT_PARENT_CATEGORY_SUCCESS:
            return {
                ...state,
                form: {
                    id: action.payload.id,
                }
            }

        case FETCH_CATEGORIES_STARTED:
            return {
                ...state,
                familyCategories: {
                    on: state.familyCategories.on,
                    loading: true,
                    error: null,
                    data: []
                }
            }

        case FETCH_CATEGORIES_SUCCESS:
            return {
                ...state,
                familyCategories: {
                    on: state.familyCategories.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }

        case FETCH_CATEGORIES_FAILURE:
            return {
                ...state,
                familyCategories: {
                    on: state.familyCategories.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }

        case ADD_CATEGORY_STARTED:
            return {
                ...state,
                familyCategories: {
                    on: state.familyCategories.on,
                    loading: true,
                    error: null,
                    data: state.familyCategories.data
                }
            }

        case ADD_CATEGORY_SUCCESS:
            return {
                ...state,
                familyCategories: {
                    on: state.familyCategories.on,
                    loading: false,
                    error: null,
                    data: [action.payload.data, ...state.familyCategories.data]
                }
            }

        case ADD_CATEGORY_FAILURE:
            return {
                ...state,
                familyCategories: {
                    on: state.familyCategories.on,
                    loading: false,
                    error: action.payload.error,
                    data: state.familyCategories.data
                }
            }

        case ADD_STANDARD_CATEGORY_STARTED:
            return {
                ...state,
                categories: {
                    on: state.categories.on,
                    loading: true,
                    error: null,
                    data: state.categories.data
                }
            }

        case ADD_STANDARD_CATEGORY_SUCCESS:
            return {
                ...state,
                categories: {
                    on: state.categories.on,
                    loading: false,
                    error: null,
                    data: [action.payload.data, ...state.categories.data]
                }
            }

        case ADD_STANDARD_CATEGORY_FAILURE:
            return {
                ...state,
                categories: {
                    on: state.categories.on,
                    loading: false,
                    error: action.payload.error,
                    data: state.categories.data
                }
            }

        case STANDARD_CATEGORIES_TOGGLE_ON:
            return {
                ...state,
                categories: {
                    on: true,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                relationships: {
                    on: false,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: [...state.relationships.data]
                },
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case STANDARD_CATEGORIES_TOGGLE_OFF:
            return {
                ...state,
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case CATEGORIES_TOGGLE_ON:
            return {
                ...state,
                familyCategories: {
                    on: true,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                relationships: {
                    on: false,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: [...state.relationships.data]
                },
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case CATEGORIES_TOGGLE_OFF:
            return {
                ...state,
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                }
            }

//--------------------------------------------------------------------------------------------------------------------
// RELATIONSHIPS -----------------------------------------------------------------------------------------------------

        case FETCH_RELATIONSHIPS_STARTED:
            /* return {
              ...state,
              loading: true,
              error: null
            } */
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: state.relationships.relSubmitSpinner,
                    loading: true,
                    error: null,
                    data: []
                }
            }
        case FETCH_RELATIONSHIPS_SUCCESS:
            /* return {
              ...state,
              loading: false,
              error: null,
              relationships: [...action.payload.data]
            } */
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: state.relationships.relSubmitSpinner,
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }
        case FETCH_RELATIONSHIPS_FAILURE:
            /* return {
              ...state,
              loading: false,
              error: action.payload.error
            } */
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: state.relationships.relSubmitSpinner,
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            }
        case CHANGE_RELATIONSHIP_STARTED:
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: true,
                    loading: false,
                    error: null,
                    data: state.relationships.data
                }
            }
        case CHANGE_RELATIONSHIP_SUCCESS:
            const newData = [...state.relationships.data];
            newData[action.payload.index] = action.payload.data;
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: newData,
                }
            }
        case CHANGE_RELATIONSHIP_FAILURE:
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: false,
                    loading: false,
                    error: action.payload.error,
                    data: state.relationships.data
                }
            }
        case ADD_RELATIONSHIP_STARTED:
            /* return {
              ...state,
              relSubmitSpinner: true,
              loading: false,
              error: null
            } */
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: true,
                    loading: false,
                    error: null,
                    data: state.relationships.data
                }
            }
        case ADD_RELATIONSHIP_SUCCESS:
            /* return {
              ...state,
              relSubmitSpinner: false,
              loading: false,
              error: null,
              relationships: [ ...state.relationships, action.payload.data]
            } */
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: [action.payload.data, ...state.relationships.data]
                }
            }
        case ADD_RELATIONSHIP_FAILURE:
            /* return {
              ...state,
              relSubmitSpinner: false,
              loading: false,
              error: action.payload.error
            } */
            return {
                ...state,
                relationships: {
                    on: state.relationships.on,
                    relSubmitSpinner: false,
                    loading: false,
                    error: action.payload.error,
                    data: state.relationships.data
                }
            }

        case RELATIONSHIPS_TOGGLE_ON:
            return {
                ...state,
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                relationships: {
                    on: true,
                    relSubmitSpinner: state.relationships.relSubmitSpinner,
                    loading: state.relationships.loading,
                    error: state.relationships.error,
                    data: state.relationships.data
                },
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                },
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case RELATIONSHIPS_TOGGLE_OFF:
            return {
                ...state,
                relationships: {
                    on: false,
                    relSubmitSpinner: state.relationships.relSubmitSpinner,
                    loading: state.relationships.loading,
                    error: state.relationships.error,
                    data: state.relationships.data
                }
            }
        //------------------------------------------------------------------------------------------------------
        //PROFILE INFORMATION ----------------------------------------------------------------------------------
        case FETCH_INFO_STARTED:
            return {
                ...state,
                info: {
                    on: state.info.on,
                    loading: true,
                    error: null,
                    data: [],
                    id: state.loggedUser
                }
            }
        case FETCH_INFO_SUCCESS:
            return {
                ...state,
                info: {
                    on: state.info.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data],
                    id: state.loggedUser
                }
            }
        case FETCH_INFO_FAILURE:
            return {
                ...state,
                info: {
                    on: state.info.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    id: state.loggedUser
                }
            }
        case INFO_TOGGLE_ON:
            return {
                ...state,
                info: {
                    on: true,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                relationships: {
                    on: false,
                    relSubmitSpinner: state.relationships.relSubmitSpinner,
                    loading: state.relationships.loading,
                    error: state.relationships.error,
                    data: state.relationships.data
                },
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                },
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }
        case INFO_TOGGLE_OFF:
            return {
                ...state,
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                }
            }
//---------------------------------------------------------------------------------------------------------------------
//ACCOUNTS-----------------------------------------------------------------------------------------------------------------
        case FETCH_ACCOUNTS_STARTED:
            return {
                ...state,
                accounts: {
                    on: state.accounts.on,
                    loading: true,
                    error: null,
                    data: []
                }
            }
        case  FETCH_ACCOUNTS_SUCCESS:
            return {
                ...state,
                accounts: {
                    on: state.accounts.on,
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            }
        case FETCH_ACCOUNTS_FAILURE:
            return {
                ...state,
                accounts: {
                    on: state.accounts.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            }

        case ADD_ACCOUNT_STARTED:
            return {
                ...state,
                accounts: {
                    on: state.accounts.on,
                    loading: true,
                    error: null,
                    data: [...state.accounts.data]
                }
            }

        case ADD_ACCOUNT_SUCCESS:
            return {
                ...state,
                accounts:{
                    on: state.accounts.on,
                    loading:false,
                    error: null,
                    data: [action.payload.data,...state.accounts.data]
                }
            }

        case ADD_ACCOUNT_FAILURE:
            return{
                ...state,
                accounts:{
                    on: state.accounts.on,
                    loading: false,
                    error: action.payload.error,
                    data: state.accounts.data,
                }
            }

        case ACCOUNTS_TOGGLE_ON:
            return {
                ...state,
                members: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.members.data]
                },
                familyCategories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.familyCategories.data]
                },
                families: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.families.data]
                },
                relationships: {
                    on: false,
                    relSubmitSpinner: false,
                    loading: false,
                    error: null,
                    data: [...state.relationships.data]
                },
                info: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.info.data],
                    id: state.loggedUser
                },
                categories: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                },
                accounts: {
                    on: true,
                    loading: false,
                    error: null,
                    data: [...state.categories.data]
                }
            }

        case ACCOUNTS_TOGGLE_OFF:
            return {
                ...state,
                accounts: {
                    on: false,
                    loading: false,
                    error: null,
                    data: [...state.accounts.data]
                }
            }

//---------------------------------------------------------------------------------------------------------------------
//LOGIN-----------------------------------------------------------------------------------------------------------------
        case LOGIN:
            return {
                ...state,
                user: {
                    isLogged: false,
                    error: null,
                    data: []
                }
            }
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                user: {
                    isLogged: true,
                    error: null,
                    data: action.payload.user
                }
            }
        case FETCH_USER_FAILURE:
            return {
                ...state,
                user: {
                    isLogged: false,
                    error: action.payload.error,
                    data: [],
                }
            }
        case LOGOUT:
            return {
                ...state,
                user: {
                    isLogged: false,
                    error: null,
                    data: [],
                }
            }
        default:
            return state;
//MOVEMENTS---------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
        case FETCH_MOVEMENTS_STARTED:
            return {
                ...state,
                movements: {
                    on: state.movements.on,
                    loading: true,
                    error: null,
                    data: [],
                    ledgerId: state.movements.ledgerId,
                }
            }
        case FETCH_MOVEMENTS_SUCCESS:
            return {
                ...state,
                movements: {
                    on: true,
                    loading: false,
                    error: null,
                    data: action.payload.data,
                    ledgerId: state.movements.ledgerId,
                }
            }
        case FETCH_MOVEMENTS_FAILURE:
            return {
                ...state,
                movements: {
                    on: false,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    ledgerId: state.movements.ledgerId,
                }
            }

        case FETCH_LEDGERID_STARTED:
            return {
                ...state,
                movements: {
                    on: state.movements.on,
                    loading: true,
                    error: null,
                    data: [],
                    ledgerId: state.movements.ledgerId,
                }
            }
        case FETCH_LEDGERID_SUCCESS:
            return {
                ...state,
                movements: {
                    on: state.movements.on,
                    loading: false,
                    error: null,
                    data: state.movements.data,
                    ledgerId: action.payload.data.ledgerIdValue,
                }
            }
        case FETCH_LEDGERID_FAILURE:
            return {
                ...state,
                movements: {
                    on: state.movements.on,
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    ledgerId: state.movements.ledgerId,
                }
            }
    }
}

export default reducer;
import {addFamilyToWeb, getFamiliesFromWeb, getFamilyFromWeb} from './services/FamilyService'
import {addMemberToH2, fetchMembersFromH2} from './services/MembersService'
import {
    addRelationshipOnBackEnd,
    changeRelationshipOnBackEnd,
    fetchFamOneRelationships
} from './services/RelationshipsService'; //RELATIONSHIPS
import {fetchInfoFromWeb} from './services/InfoService';
import {
    addCustomCategoryToSWSAPI,
    addStandardCategoryToSWSAPI,
    fetchCategoriesFromSWSAPI,
    fetchStandardCategoriesFromSWSAPI
} from './services/CategoryService';
import {loginSimpleWeb} from "./services/LoginSimpleService";
import {addAccountToWS, fetchAccountsFromWS} from "./services/AccountService";
import {deleteSecondaryEmail, fetchEmailWeb} from "./services/EmailService";
import {getUserFamilyWeb, getUserRoleWeb} from "./services/UserRoleService";
import {fetchLedgerIdFromWeb, fetchMovementsFromWeb} from "./services/MovementsService";
import {genericRequestWeb} from "./services/GenericService";


export const WELCOME_TOGGLE_OFF = "WELCOME_TOGGLE_OFF"

export function toggleWelcomeOff() {
    return {
        type: WELCOME_TOGGLE_OFF
    }
}

export function toggleWelcome(dispatch, bool) {
    if (bool) {
        dispatch(toggleWelcomeOff())
    }
}

export function genericRequest(dispatch,url,httpverb,token,object){
    console.log("generic request started")
    console.log(url.href)
    console.log(object)
    dispatch(genericRequestStarted());
    const data = {
        email: object,
    }
    const req = {
        method: httpverb,
        headers: {
            "Content-Type": "Application/Json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(data)
    };
    console.log(req);
    genericRequestWeb(url.href,(res) => dispatch(genericRequestSuccess(res)), (err) => dispatch(genericRequestFailure(err)), req);
}

export function genericRequestStarted() {
    return {

    }
}

export function genericRequestSuccess(res) {
    return {

    }
}

export function genericRequestFailure(message) {
    return {
        payload: {
            error: message
        }
    }
}

//----Add Email to Profile-------------------------------------------------------------------------------------------------

export const ADD_EMAIL_STARTED = "ADD_EMAIL_STARTED";
export const ADD_EMAIL_SUCCESS = "ADD_EMAIL_SUCCESS";
export const ADD_EMAIL_FAILURE = "ADD_EMAIL_FAILURE";
export const SHOW_EMAIL_STARTED = "SHOW_EMAIL_STARTED";
export const SHOW_EMAIL_SUCCESS = "SHOW_EMAIL_SUCCESS";
export const SHOW_EMAIL_FAILURE = "SHOW_EMAIL_FAILURE";

export function addEmailStarted() {
    return {
        type: ADD_EMAIL_STARTED,
    }
}

export function addEmailSuccess(email) {
    return {
        type: ADD_EMAIL_SUCCESS,
        payload: {
            success: email.email
        }
    }
}

export function addEmailFailure(message) {
    return {
        type: ADD_EMAIL_FAILURE,
        payload: {
            error: message
        }
    }
}

export function addEmail(dispatch, newEmail, token, id) {
    dispatch(addEmailStarted());
console.log(newEmail);
    const data = {
        email: newEmail.otherEmails,
    }
    const req = {
        method: "POST",
        headers: {
            "Content-Type": "Application/Json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(data)
    };
    console.log(req);
    fetchEmailWeb((res) => dispatch(addEmailSuccess(res)), (err) => dispatch(addEmailFailure(err)), req, id);

}


export function toggleOtherEmails(dispatch, bool) {
    if (bool) {
        dispatch(toggleOtherEmailsOff())
    } else {
        dispatch(toggleOtherEmailsOn())
    }
}

export const OTHER_EMAILS_TOGGLE_ON = "OTHER_EMAILS_TOGGLE_ON";
export const OTHER_EMAILS_TOGGLE_OFF = "OTHER_EMAILS_TOGGLE_OFF";


export function toggleOtherEmailsOn() {
    return {
        type:  OTHER_EMAILS_TOGGLE_ON
    }
}

export function toggleOtherEmailsOff() {
    return {
        type:OTHER_EMAILS_TOGGLE_OFF
    }
}

//SHOW EMAILS -------------------------------------------------------------------------------------------------------
export function showEmailStarted() {
    console.log('started')
    return {
        type: SHOW_EMAIL_STARTED,
    }
}

export function showEmailSuccess(emails) {
    console.log('success');
    return {
        type: SHOW_EMAIL_SUCCESS,
        payload: {
            data: [...emails.emailsList],
        }
    }
}

export function showEmailFailure(message) {
    return {
        type: SHOW_EMAIL_FAILURE,
        payload: {
            error: message
        }
    }
}

export function showEmail(dispatch, token, id) {
    dispatch(showEmailStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
            "Access-Control-Allow-Origin": "*",
        },
    };
    fetchEmailWeb((res) => dispatch(showEmailSuccess(res)), (err) => dispatch(showEmailFailure(err.message)), req, id);
}

//DELETE EMAIL ------------------------------------------------------------------------------------------------------

export const DELETE_EMAIL_STARTED = "DELETE_EMAIL_STARTED";
export const DELETE_EMAIL_SUCCESS = "DELETE_EMAIL_SUCCESS";
export const DELETE_EMAIL_FAILURE = "DELETE_EMAIL_FAILURE";

export function deleteEmailStarted(){
    return {
        type: DELETE_EMAIL_STARTED,
    }
}

export function deleteEmailSuccess(email){ //confirmar
    return {
        type: DELETE_EMAIL_SUCCESS,
        payload: {
            data: email.email
        }
    }
}

export function deleteEmailFailure(message){
    return {
        type: DELETE_EMAIL_FAILURE,
        payload: {
            error: message
        }
    }
}

export function deleteEmail(dispatch, token, id, emailToRemove) {
    console.log("Action - Delete Email");
    console.log("\tID -> " + id);
    console.log("\tEmail to remove -> " + emailToRemove);
    dispatch(deleteEmailStarted());
    const req = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
    };
    deleteSecondaryEmail((res) => dispatch(deleteEmailSuccess(res)), (err) => dispatch(deleteEmailFailure(err.message)), id, req, emailToRemove);
}

// LOGINSIMPLE ------------------------------------------------------------------------------------------------------

export const LOGIN_SIMPLE_STARTED = "LOGIN_SIMPLE_STARTED";
export const LOGIN_SIMPLE_SUCCESS = "LOGIN_SIMPLE_SUCCESS";
export const LOGIN_SIMPLE_FAILURE = "LOGIN_SIMPLE_FAILURE";
export const LOGGING_OUT_DONE = "LOGGING_OUT_DONE";

export function loginSimple(dispatch, username, password) {
    dispatch(loginSimpleStarted(username));
    const data = {
        username: username,
        password: password,
    };
    const req = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data)
    };

    loginSimpleWeb((res) => dispatch(loginSimpleSuccess(res)), (err) => dispatch(loginSimpleFailure(err)), req);
}

export function loginSimpleStarted(username) {
    console.log("started login");
    return {
        type: LOGIN_SIMPLE_STARTED,
        payload: {
            email: username,
        }
    }
}

export function loginSimpleSuccess(token) {
    console.log("login successful");
    console.log("token in loginSimpleSuccess: " + token.token)
    return {
        type: LOGIN_SIMPLE_SUCCESS,
        payload: {
            token: token.token
        }
    }
}

export function loginSimpleFailure(message) {
    console.log("login failed");
    console.log(message)
    return {
        type: LOGIN_SIMPLE_FAILURE,
        payload: {
            error: message
        }
    }
}

export function loggingout(dispatch) {
    dispatch(logoutDone())
}

export function logoutDone() {
    return {
        type: LOGGING_OUT_DONE
    }
}

// USER ROLES ------------------------------------------------------------------------------------------------------

export const GET_USER_ROLE_STARTED = "GET_USER_ROLE_STARTED";
export const GET_USER_ROLE_SUCCESS = "GET_USER_ROLE_SUCCESS";
export const GET_USER_ROLE_FAILURE = "GET_USER_ROLE_FAILURE";
export const GET_FAMILY_ADMIN_STARTED = "GET_FAMILY_ADMIN_STARTED";
export const GET_FAMILY_ADMIN_SUCCESS = "GET_FAMILY_ADMIN_SUCCESS";
export const GET_FAMILY_ADMIN_FAILURE = "GET_FAMILY_ADMIN_FAILURE";
export const GET_FAMILY_USER_STARTED = "GET_FAMILY_USER_STARTED";
export const GET_FAMILY_USER_SUCCESS = "GET_FAMILY_USER_SUCCESS";
export const GET_FAMILY_USER_FAILURE = "GET_FAMILY_USER_FAILURE";

export function getUserRole(dispatch, token) {
        dispatch(getUserRoleStarted())
    const req = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        }
    };
    getUserRoleWeb((res) => dispatch(getUserRoleSuccess(res)), (err) => dispatch(getUserRoleFailure(err)), req);
}

export function getUserRoleStarted() {
    console.log("Get User Role Started");
    return {
        type: GET_USER_ROLE_STARTED,

    }
}

export function getUserRoleSuccess(userInfo) {
    console.log("Get User Role Success");
    console.log("Action role: ");
    console.log(userInfo);
    console.log(userInfo.roles[0])
    return {
        type: GET_USER_ROLE_SUCCESS,
        payload: {
            username: userInfo.username,
            role: userInfo.roles[0],
        }
    }
}

export function getUserRoleFailure(message) {
    console.log("get user role failed");
    console.log(message)
    return {
        type: GET_USER_ROLE_FAILURE,
        payload: {
            error: message
        }
    }
}

export function getUserFamily(dispatch, loggedUser, token) {
    dispatch(getUserFamilyStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
            "Access-Control-Allow-Origin": "*",
        },
    };
    getUserFamilyWeb((res) => dispatch(getUserFamilySuccess(res)), (err) => dispatch(getUserFamilyFailure(err)), req, loggedUser);

}

export function getUserFamilyStarted() {
    console.log("started getting user family")
    return {
        type: GET_FAMILY_USER_STARTED
    }
}

export function getUserFamilySuccess(familyId) {
    console.log("success getting user family")
    return {
        type: GET_FAMILY_USER_SUCCESS,
        payload: {
            userFamilyId: familyId,
        }
    }
}

export function getUserFamilyFailure(message) {
    console.log("failure getting user family")
    return {
        type: GET_FAMILY_USER_FAILURE,
        payload: {
            error: message
        }
    }
}

// FAMILIES ---------------------------------------------------------------------------------------------------------

export const GET_FAMILIES_STARTED = "GET_FAMILIES_STARTED";
export const GET_FAMILIES_SUCCESS = "GET_FAMILIES_SUCCESS";
export const GET_FAMILIES_FAILURE = "GET_FAMILIES_FAILURE";

export function getFamilies(dispatch, token) {
    dispatch(getFamiliesStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    getFamiliesFromWeb((res) => dispatch(getFamiliesSuccess(res)), (err) => dispatch(getFamiliesFailure(err)), req);
}

export function getFamiliesStarted() {
    console.log("started getting families")
    return {
        type: GET_FAMILIES_STARTED,
    }
}

export function getFamiliesSuccess(families) {
    console.log("success getting families")
    return {
        type: GET_FAMILIES_SUCCESS,
        payload: {
            data: [...families],
        }
    }
}

export function getFamiliesFailure(message) {
    return {
        type: GET_FAMILIES_FAILURE,
        payload: {
            error: message
        }
    }
}

export function getFamily(dispatch, id, token) {
    dispatch(getFamilyStarted(id));
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    getFamilyFromWeb((res) => dispatch(getFamilySuccess(res)), (err) => dispatch(getFamilyFailure(err.message)), id, req);
}

export function ungetFamily(dispatch) {
    dispatch(ungetFamilyDone());
}

export const GET_FAMILY_STARTED = "GET_FAMILY_STARTED";
export const GET_FAMILY_SUCCESS = "GET_FAMILY_SUCCESS";
export const GET_FAMILY_FAILURE = "GET_FAMILY_FAILURE";
export const UNGET_FAMILY = "UNGET_FAMILY";

export function getFamilyStarted(id) {
    return {
        type: GET_FAMILY_STARTED,
        payload: {
            familyid: id,
        }
    }
}

export function getFamilySuccess(family) {
    return {
        type: GET_FAMILY_SUCCESS,
        payload: {
            familyid: family.id,
            name: family.name,
            adminEmail: family.adminEmail,
            //data: [...family],
        }
    }
}

export function getFamilyFailure(message) {
    return {
        type: GET_FAMILY_FAILURE,
        payload: {
            error: message
        }
    }
}

export function ungetFamilyDone() {
    return {
        type: UNGET_FAMILY,
    }
}

//export function addFamily(dispatch, name, adminEmail) {
export function addFamily(dispatch, newFamily, token) {
    dispatch(addFamilyStarted(newFamily));
    const data = {
        name: newFamily.name,
        adminEmail: newFamily.adminEmail,
        adminVat: newFamily.adminVat,
        adminName: newFamily.adminName,
        adminBirthDate: newFamily.adminBirthDate,
    };
    const req = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(data)
    };
    addFamilyToWeb((res) => dispatch(addFamilySuccess(res)), (err) => dispatch(addFamilyFailure(err.message)), req);
    //addFamilyToWeb2((res) => dispatch(addFamilySuccess(res)),(err) => dispatch(addFamilyFailure(err.message)),req,name,adminEmail);
}

export const ADD_FAMILY_STARTED = "ADD_FAMILY_STARTED";
export const ADD_FAMILY_SUCCESS = "ADD_FAMILY_SUCCESS";
export const ADD_FAMILY_FAILURE = "ADD_FAMILY_FAILURE";

/*export function addFamilyStarted(name,adminEmail) {
    return {
        type: ADD_FAMILY_STARTED,
        payload: {
            name : name,
            adminEmail: adminEmail,
        }
    }
}*/

export function addFamilyStarted(newFamily) {
    return {
        type: ADD_FAMILY_STARTED,
        payload: {
            name: newFamily.name,
            adminEmail: newFamily.adminEmail,
            adminVat: newFamily.adminVat,
            adminName: newFamily.adminName,
            adminBirthDate: newFamily.adminBirthDate,
        }
    }
}

export function addFamilySuccess(family) {
    return {
        type: ADD_FAMILY_SUCCESS,
        payload: {
            data: family
        }
    }
}

export function addFamilyFailure(message) {
    return {
        type: ADD_FAMILY_FAILURE,
        payload: {
            error: message
        }
    }
}

export function toggleFamilies(dispatch, bool) {
    if (bool) {
        dispatch(toggleFamiliesOff())
    } else {
        dispatch(toggleFamiliesOn())
    }
}

export const FAMILIES_TOGGLE_ON = "FAMILIES_TOGGLE_ON";
export const FAMILIES_TOGGLE_OFF = "FAMILIES_TOGGLE_OFF";


export function toggleFamiliesOn() {
    return {
        type: FAMILIES_TOGGLE_ON
    }
}

export function toggleFamiliesOff() {
    return {
        type: FAMILIES_TOGGLE_OFF
    }
}

// -----------------------------------------------------------------------------------------------------------------

// MEMBERS ---------------------------------------------------------------------------------------------------------


export const FETCH_MEMBERS_STARTED = 'FETCH_MEMBERS_STARTED';
export const FETCH_MEMBERS_SUCCESS = 'FETCH_MEMBERS_SUCCESS';
export const FETCH_MEMBERS_FAILURE = 'FETCH_MEMBERS_FAILURE';

export function fetchMembers(dispatch, id, token) {
    dispatch(fetchMembersStarted());
    console.log("trying to fetch admin members.")
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchMembersFromH2((res) => dispatch(fetchMembersSuccess(res)), (err) => dispatch(fetchMemberFailure(err.message)), id, req);
}

export function fetchMembersStarted() {
    return {
        type: FETCH_MEMBERS_STARTED
    }
}

export function fetchMembersSuccess(members) {
    return {
        type: FETCH_MEMBERS_SUCCESS,
        payload: {
            data:
                [...members]
        }
    }
}

export function fetchMemberFailure(message) {
    return {
        type: FETCH_MEMBERS_FAILURE,
        payload: {
            error: message
        }
    }
}

export const ADD_MEMBER_STARTED = 'ADD_MEMBER_STARTED';
export const ADD_MEMBER_SUCCESS = 'ADD_MEMBER_SUCCESS';
export const ADD_MEMBER_FAILURE = 'ADD_MEMBER_FAILURE';

export function addMember(dispatch, emailAddress, address, birthDate, familyID, name, vatNumber, token) {
    dispatch(addMemberStarted())

    const data = {
        emailAddress: emailAddress,
        address: address,
        birthDate: birthDate,
        familyID: familyID,
        name: name,
        vatNumber: vatNumber,
    };

    const req = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(data)
    };

    addMemberToH2((res) => dispatch(addMemberSuccess(res)),
        (err) => dispatch(addMemberFailure(err.message)),familyID, req);
}

export function addMemberStarted() {
    return {
        type: ADD_MEMBER_STARTED
    }
}

export function addMemberSuccess(member) {
    console.log("membro adicionado")
    console.log(member)
    console.log("links")
    console.log(member._links)
    console.log("nome dos links")
    console.log(Object.keys(member._links))
    console.log("link 1 nome")
    console.log(Object.keys(member._links)[1])
    return {
        type: ADD_MEMBER_SUCCESS,
        payload: {
            data: member
        }
    }
}

export function addMemberFailure(message) {
    return {
        type: ADD_MEMBER_FAILURE,
        payload: {
            error: message
        }
    }
}

export function toggleMembers(dispatch, bool) {
    if (bool) {
        dispatch(toggleMembersOff())
    } else {
        dispatch(toggleMembersOn())
    }
}

export const MEMBERS_TOGGLE_ON = "MEMBERS_TOGGLE_ON";
export const MEMBERS_TOGGLE_OFF = "MEMBERS_TOGGLE_OFF";


export function toggleMembersOn() {
    return {
        type: MEMBERS_TOGGLE_ON
    }
}

export function toggleMembersOff() {
    return {
        type: MEMBERS_TOGGLE_OFF
    }
}

// CATEGORIES ------------------------------------------------------------------------------------------------------

export const FETCH_STANDARD_CATEGORIES_STARTED = 'FETCH_STANDARD_CATEGORIES_STARTED';
export const FETCH_STANDARD_CATEGORIES_SUCCESS = 'FETCH_STANDARD_CATEGORIES_SUCCESS';
export const FETCH_STANDARD_CATEGORIES_FAILURE = 'FETCH_STANDARD_CATEGORIES_FAILURE';
export const ADD_STANDARD_CATEGORY_STARTED = 'ADD_STANDARD_CATEGORY_STARTED';
export const ADD_STANDARD_CATEGORY_SUCCESS = 'ADD_STANDARD_CATEGORY_SUCCESS';
export const ADD_STANDARD_CATEGORY_FAILURE = 'ADD_STANDARD_CATEGORY_FAILURE';
export const SELECT_PARENT_CATEGORY_SUCCESS = 'SELECTED_PARENT_CATEGORY_SUCCESS';
export const FETCH_CATEGORIES_STARTED = 'FETCH_CATEGORIES_STARTED';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORIES_FAILURE = 'FETCH_CATEGORIES_FAILURE';
export const ADD_CATEGORY_STARTED = 'ADD_CATEGORY_STARTED';
export const ADD_CATEGORY_SUCCESS = 'ADD_CATEGORY_SUCCESS';
export const ADD_CATEGORY_FAILURE = 'ADD_CATEGORY_FAILURE';


export function addStandardCategory(dispatch, newStandardCategory, token) {
    console.log("new standard category: " + newStandardCategory)
    dispatch(addStandardCategoryStarted());
    const postRequest = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(newStandardCategory)
    };
    addStandardCategoryToSWSAPI((res) => dispatch(addStandardCategorySuccess(res)), (err) => dispatch(addStandardCategoryFailure(err.message)), postRequest);
}

export function addStandardCategoryStarted() {
    return {
        type: ADD_STANDARD_CATEGORY_STARTED,
    }
}

export function addStandardCategorySuccess(category) {
    console.log("add standard category success");
    return {
        type: ADD_STANDARD_CATEGORY_SUCCESS,
        payload: {
            data: category
        }
    }
}

export function addStandardCategoryFailure(message) {
    console.log("add standard category failure");
    return {
        type: ADD_STANDARD_CATEGORY_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchStandardCategories(dispatch, token) {
    dispatch(fetchStandardCategoriesStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchStandardCategoriesFromSWSAPI((res) => dispatch(fetchStandardCategoriesSuccess(res)), (err) => dispatch(fetchStandardCategoriesFailure(err.message)), req);
}

export function fetchStandardCategoriesStarted() {
    console.log("fetch standard categories started");
    return {
        type: FETCH_STANDARD_CATEGORIES_STARTED,
    }
}

export function fetchStandardCategoriesSuccess(categories) {
    return {
        type: FETCH_STANDARD_CATEGORIES_SUCCESS,
        payload: {
            data:
                [...categories]
        }
    }
}

export function fetchStandardCategoriesFailure(message) {
    return {
        type: FETCH_STANDARD_CATEGORIES_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchCategories(dispatch, id, token) {
    console.log("actions id " + id);
    dispatch(fetchCategoriesStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchCategoriesFromSWSAPI((res) => dispatch(fetchCategoriesSuccess(res)), (err) => dispatch(fetchCategoriesFailure(err.message)), id, req);
}

export function fetchCategoriesStarted() {
    return {
        type: FETCH_CATEGORIES_STARTED,

    }
}

export function fetchCategoriesSuccess(familyCategories) {
    return {
        type: FETCH_CATEGORIES_SUCCESS,
        payload: {
            data:
                [...familyCategories]
        }
    }
}

export function fetchCategoriesFailure(message) {
    return {
        type: FETCH_CATEGORIES_FAILURE,
        payload: {
            error: message
        }
    }
}

export function selectParentCategory(dispatch, id) {
    dispatch(selectParentCategoryStarted(id));
}

export function selectParentCategoryStarted(id) {
    return {
        type: SELECT_PARENT_CATEGORY_SUCCESS,
        payload: {
            id: id
        }
    }
}

export function addCustomCategory(dispatch, newCustomCategory, id, token) {
    dispatch(addCustomCategoryStarted());
    const postRequest = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(newCustomCategory)
    };
    addCustomCategoryToSWSAPI((res) => dispatch(addCustomCategorySuccess(res)), (err) => dispatch(addCustomCategoryFailure(err.message)), id, postRequest);
}

export function addCustomCategoryStarted() {
    return {
        type: ADD_CATEGORY_STARTED,
    }
}

export function addCustomCategorySuccess(familyCategory) {
    return {
        type: ADD_CATEGORY_SUCCESS,
        payload: {
            data: familyCategory
        }
    }
}

export function addCustomCategoryFailure(message) {
    return {
        type: ADD_CATEGORY_FAILURE,
        payload: {
            error: message
        }
    }
}

// RELATIONSHIPS ---------------------------------------------------------------------------------------------------
export const FETCH_RELATIONSHIPS_STARTED = 'FETCH_RELATIONSHIPS_STARTED';
export const FETCH_RELATIONSHIPS_SUCCESS = 'FETCH_RELATIONSHIPS_SUCCESS';
export const FETCH_RELATIONSHIPS_FAILURE = 'FETCH_RELATIONSHIPS_FAILURE';

export function fetchRelationships(dispatch, id, token) {
    console.log("relationship action id " + id)
    dispatch(fetchRelationshipsStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchFamOneRelationships((res) => dispatch(fetchRelationshipsSuccess(res)), (err) => dispatch(fetchRelationshipsFailure(err)), id, req);
}

export function fetchRelationshipsStarted() {
    return {
        type: FETCH_RELATIONSHIPS_STARTED,

    }
}

export function fetchRelationshipsSuccess(relationships) {
    return {
        type: FETCH_RELATIONSHIPS_SUCCESS,
        payload: {
            data:
                [...relationships]
        }

    }
}

export function fetchRelationshipsFailure(message) {
    return {
        type: FETCH_RELATIONSHIPS_FAILURE,
        payload: {
            error: message
        }
    }
}

export const CHANGE_RELATIONSHIP_STARTED = "CHANGE_RELATIONSHIP_STARTED";
export const CHANGE_RELATIONSHIP_SUCCESS = "CHANGE_RELATIONSHIP_SUCCESS";
export const CHANGE_RELATIONSHIP_FAILURE = "CHANGE_RELATIONSHIP_FAILURE";
export const ADD_RELATIONSHIP_STARTED = "ADD_RELATIONSHIP_STARTED";
export const ADD_RELATIONSHIP_SUCCESS = "ADD_RELATIONSHIP_SUCCESS";
export const ADD_RELATIONSHIP_FAILURE = "ADD_RELATIONSHIP_FAILURE";

export function changeRelationship(dispatch, relationship, id, token, index) {
    dispatch(changeRelationshipStarted());
    console.log('Actions CHAAAANGE');
    console.log(id)
    console.log(token);
    console.log(relationship.relationshipId);
    const request = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(relationship)
    };

    changeRelationshipOnBackEnd((res) => dispatch(changeRelationshipSuccess(res, index)), (err) => dispatch(changeRelationshipFailure(err.message)), request, id, relationship.relationshipId)

}

export function changeRelationshipStarted() {
    return {
        type: CHANGE_RELATIONSHIP_STARTED
    }
}

export function changeRelationshipSuccess(relationship, index) {
    return {
        type: CHANGE_RELATIONSHIP_SUCCESS,
        payload: {
            data: relationship,
            index: index
        }
    }
}

export function changeRelationshipFailure(message) {
    return {
        type: CHANGE_RELATIONSHIP_FAILURE,
        payload: {
            error: message
        }
    }
}


export function addRelationship(dispatch, relationship, id, token) {
    dispatch(addRelationshipStarted());

    const request = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(relationship)
    };

    addRelationshipOnBackEnd((res) => dispatch(addRelationshipSuccess(res)), (err) => dispatch(addRelationshipFailure(err.message)), request, id)

}

export function addRelationshipStarted() {
    return {
        type: ADD_RELATIONSHIP_STARTED
    }
}

export function addRelationshipSuccess(relationship) {
    return {
        type: ADD_RELATIONSHIP_SUCCESS,
        payload: {
            data: relationship
        }
    }
}

export function addRelationshipFailure(message) {
    return {
        type: ADD_RELATIONSHIP_FAILURE,
        payload: {
            error: message
        }
    }
}

export function toggleRelationships(dispatch, bool) {
    if (bool) {
        dispatch(toggleRelationshipsOff())
    } else {
        dispatch(toggleRelationshipsOn())
    }
}

export const RELATIONSHIPS_TOGGLE_ON = "RELATIONSHIPS_TOGGLE_ON";
export const RELATIONSHIPS_TOGGLE_OFF = "RELATIONSHIPS_TOGGLE_OFF";


export function toggleRelationshipsOn() {
    return {
        type: RELATIONSHIPS_TOGGLE_ON
    }
}

export function toggleRelationshipsOff() {
    return {
        type: RELATIONSHIPS_TOGGLE_OFF
    }
}

export function toggleStandardCategories(dispatch, bool) {
    if (bool) {
        dispatch(toggleStandardCategoriesOff())
    } else {
        console.log("toggle standard on");
        dispatch(toggleStandardCategoriesOn())
    }
}

export const STANDARD_CATEGORIES_TOGGLE_ON = "STANDARD_CATEGORIES_TOGGLE_ON";
export const STANDARD_CATEGORIES_TOGGLE_OFF = "STANDARD_CATEGORIES_TOGGLE_OFF";


export function toggleStandardCategoriesOn() {
    return {
        type: STANDARD_CATEGORIES_TOGGLE_ON
    }
}

export function toggleStandardCategoriesOff() {
    return {
        type: STANDARD_CATEGORIES_TOGGLE_OFF
    }
}


export function toggleCategories(dispatch, bool) {
    if (bool) {
        dispatch(toggleCategoriesOff())
    } else {
        dispatch(toggleCategoriesOn())
    }
}

export const CATEGORIES_TOGGLE_ON = "CATEGORIES_TOGGLE_ON";
export const CATEGORIES_TOGGLE_OFF = "CATEGORIES_TOGGLE_OFF";


export function toggleCategoriesOn() {
    return {
        type: CATEGORIES_TOGGLE_ON
    }
}

export function toggleCategoriesOff() {
    return {
        type: CATEGORIES_TOGGLE_OFF
    }
}

// -----------------------------------------------------------------------------------------------------------------
// PROFILE INFORMATION----------------------------------------------------------------------------------------------

export const FETCH_INFO_STARTED = 'FETCH_INFO_STARTED';
export const FETCH_INFO_SUCCESS = 'FETCH_INFO_SUCCESS';
export const FETCH_INFO_FAILURE = 'FETCH_INFO_FAILURE';
export const INFO_TOGGLE_ON = 'INFO_TOGGLE_ON';
export const INFO_TOGGLE_OFF = 'INFO_TOGGLE_OFF';

export function fetchInfo(dispatch, id, token) {
    dispatch(fetchInfoStarted());
    console.log("A BUSCAR..." + id)
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchInfoFromWeb((res) => dispatch(fetchInfoSuccess(res)), (err) => dispatch(fetchInfoFailure(err.message)), id, req);
}

export function fetchInfoStarted() {
    return {
        type: FETCH_INFO_STARTED,
    }
}

export function fetchInfoSuccess(info) {
    console.log("Success INFO USER")
    console.log(info)
    return {
        type: FETCH_INFO_SUCCESS,
        payload: {
            data:
                [info]
        }
    }
}

export function fetchInfoFailure(message) {
    return {
        type: FETCH_INFO_FAILURE,
        payload: {
            error: message
        }
    }
}

export function toggleInfo(dispatch, bool) {
    if (bool) {
        dispatch(toggleInfoOff())
    } else {
        dispatch(toggleInfoOn())
    }
}

export function toggleInfoOn() {
    return {
        type: INFO_TOGGLE_ON
    }
}

export function toggleInfoOff() {
    return {
        type: INFO_TOGGLE_OFF
    }
}

//---------------------------------------LOGIN ----------------------------------


export const LOGIN = 'LOGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const LOGOUT = 'LOGOUT';


export function login() {
    return {
        type: LOGIN,
    }
}

export function fetchUserSuccess(user) {
    return {
        type: FETCH_USER_SUCCESS,
        payload: {
            user: user
        }
    }
}

export function fetchUserFailure(message) {
    return {
        type: FETCH_USER_FAILURE,
        payload: {
            error: message
        }
    }
}

export function logout() {
    return {
        type: LOGOUT,
    }
}

// ACCOUNTS ---------------------------------------------------------------------------------------------------

export const FETCH_ACCOUNTS_STARTED = 'FETCH_ACCOUNTS_STARTED';
export const FETCH_ACCOUNTS_SUCCESS = 'FETCH_ACCOUNTS_SUCCESS';
export const FETCH_ACCOUNTS_FAILURE = 'FETCH_ACCOUNTS_FAILURE';

export function fetchAccounts(dispatch, familyid, token,userId,role) {
    dispatch(fetchAccountsStarted());
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchAccountsFromWS((res) => dispatch(fetchAccountsSuccess(res)), (err) => dispatch(fetchAccountsFailure(err.message)), familyid, req,userId,role);
}

export function fetchAccountsStarted() {
    return {
        type: FETCH_ACCOUNTS_STARTED
    }

}

export function fetchAccountsSuccess(accounts) {
    return {
        type: FETCH_ACCOUNTS_SUCCESS,
        payload: {
            data:
                [...accounts]
        }
    }
}

export function fetchAccountsFailure(message) {
    return {
        type: FETCH_ACCOUNTS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function toggleAccounts(dispatch, bool) {
    if (bool) {
        dispatch(toggleAccountsOff())
    } else {
        dispatch(toggleAccountsOn())
    }
}

export const ACCOUNTS_TOGGLE_ON = "ACCOUNTS_TOGGLE_ON";
export const ACCOUNTS_TOGGLE_OFF = "ACCOUNTS_TOGGLE_OFF";

export function toggleAccountsOn() {
    return {
        type: ACCOUNTS_TOGGLE_ON
    }
}

export function toggleAccountsOff() {
    return {
        type: ACCOUNTS_TOGGLE_OFF
    }
}

export const ADD_ACCOUNT_STARTED = 'ADD_ACCOUNT_STARTED'
export const ADD_ACCOUNT_SUCCESS = 'ADD_ACCOUNT_SUCCESS'
export const ADD_ACCOUNT_FAILURE = 'ADD_ACCOUNT_FAILURE'

export function addAccount(dispatch, newAccount, id, token) {
    dispatch(addAccountStarted());

    const data = {
        accountId: newAccount.accountId,
        ownerId: id,
        balance: newAccount.balance,
        description: newAccount.description,
        accountType: newAccount.accountType,
    };

    const req = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token,
        },
        body: JSON.stringify(data)
    };
    console.log("actions antes da call ao add account id ->", id)
    console.log("actions antes da call ao add account token ->", token)
    console.log("actions antes da call ao add account req ->", req)
    addAccountToWS((res) => dispatch(addAccountSuccess(res)),
        (err) => dispatch(addAccountFailure(err.message)), id, req);
}

export function addAccountStarted() {
    return {
        type: ADD_ACCOUNT_STARTED
    }
}

export function addAccountSuccess(account) {
    return {
        type: ADD_ACCOUNT_SUCCESS,
        payload: {
            data: account
        }

    }
}

export function addAccountFailure(message) {
    return {
        type: ADD_ACCOUNT_FAILURE,
        payload: {
            error: message
        }
    }
}

//MOVEMENTS----------------------------------------------------------------------------------------------------------
export const FETCH_MOVEMENTS_STARTED = 'FETCH_MOVEMENTS_STARTED';
export const FETCH_MOVEMENTS_SUCCESS = 'FETCH_MOVEMENTS_SUCCESS';
export const FETCH_MOVEMENTS_FAILURE = 'FETCH_MOVEMENTS_FAILURE';

export function fetchMovements(dispatch, ledgerId, accountId, token) {
    dispatch(fetchMovementsStarted());
    console.log("A BUSCAR..." + accountId)
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchMovementsFromWeb((res) => dispatch(fetchMovementsSuccess(res)), (err) => dispatch(fetchMovementsFailure(err.message)), ledgerId, accountId, req);
}

export function fetchMovementsStarted() {
    console.log("Movements started")
    return {

        type: FETCH_MOVEMENTS_STARTED,
    }
}

export function fetchMovementsSuccess(movements) {
    console.log("Success Movements")
    console.log(movements)
    return {
        type: FETCH_MOVEMENTS_SUCCESS,
        payload: {
            data:
                movements
        }
    }
}

export function fetchMovementsFailure(message) {
    console.log("Failure Movements")
    return {
        type: FETCH_MOVEMENTS_FAILURE,
        payload: {
            error: message
        }
    }
}


export const FETCH_LEDGERID_STARTED = 'FETCH_LEDGERID_STARTED';
export const FETCH_LEDGERID_SUCCESS = 'FETCH_LEDGERID_SUCCESS';
export const FETCH_LEDGERID_FAILURE = 'FETCH_LEDGERID_FAILURE';

export function fetchLedgerId(dispatch, id, token) {
    dispatch(fetchLedgerIdStarted());
    console.log("A BUSCAR LEDGER ID..." + id)
    const req = {
        method: "GET",
        headers: {
            "Authorization": "Bearer " + token,
        },
    };
    fetchLedgerIdFromWeb((res) => dispatch(fetchLedgerIdSuccess(res)), (err) => dispatch(fetchLedgerIdFailure(err.message)), id, req);
}

export function fetchLedgerIdStarted() {
    return {
        type: FETCH_LEDGERID_STARTED,
    }
}

export function fetchLedgerIdSuccess(response) {
    console.log("Success Ledger ID")
    console.log(response)
    return {
        type: FETCH_LEDGERID_SUCCESS,
        payload: {
            data:
                response
        }
    }
}

export function fetchLedgerIdFailure(message) {
    return {
        type: FETCH_LEDGERID_FAILURE,
        payload: {
            error: message
        }
    }
}



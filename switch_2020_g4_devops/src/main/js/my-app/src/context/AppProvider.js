import React, {useReducer} from 'react';
import PropTypes from "prop-types";
import {Provider} from './AppContext';
import reducer from './Reducer';

const initialState = {
    token: undefined,
    loggedUser: undefined,
    loginMessage: undefined,
    role: undefined,
    userFamilyId: undefined,
    username: undefined,
    memberAdded: null,
    memberAddedLinks : [],
//FAMILIES --------------------------------------------------------------------------------------------------------
    families: {
        on: false,
        loading: true,
        error: null,
        data: [],
    },
    family: {
        familyid: 0,
        name: "",
        adminEmail: "",
        loading: false,
        error: null,
//        data: [],
    },
//----------------------------------------------------------------------------------------------------------------
// MEMBERS
    members: {
        on: false,
        loading: true,
        error: null,
        data: [],
    },
//----------------------------------------------------------------------------------------------------------------
//CATEGORIES -----------------------------------------------------------------------------------------------------
    categories: {
        on: false,
        loading: true,
        error: null,
        data: [],
    },
    familyCategories: {
        on: false,
        loading: true,
        error: null,
        data: [],
    },
    form: {
        id: "Click on the desired parent category from the standard categories table",
    },
//----------------------------------------------------------------------------------------------------------
// RELATIONSHIPS -------------------------------------------------------------------------------------------
    relationships: {
        on: false,
        relSubmitSpinner: false,
        loading: false,
        error: null,
        data: []
    },


//---------------------------------------------------------------------------------------------------------------
// PROFILE INFORMATION ------------------------------------------------------------------------------------------
    info: {
        id: "",
        on: false,
        loading: true,
        error: null,
        data: [],
    },
//---------------------------------------------------------------------------------------------------------------
//ACCOUNTS------------------------------------------------------------------------------------------------------------
    accounts: {
        on: false,
        loading: true,
        error: null,
        data: [],
    },

//---------------------------------------------------------------------------------------------------------------
//LOGIN------------------------------------------------------------------------------------------------------------
    user: {
        isLogged: false,
        error: null,
        data: [],
    },
//-------------------------------------------------------------------------------------------------------------------
//EMAILS------------------------------------------------------------------------------------------------------------
    emails:{
        on: false,
        loading: false,
        error: null,
        data: [],
    },
//------------------------------------------------------------------------------------------------------------------
//MOVEMENTS------------------------------------------------------------------------------------------------------------
    movements: {
        on: false,
        loading: false,
        error: null,
        ledgerId: undefined,
        data: [],
    },
    welcome: {
        on: true,
    },
};

const emailHeaders = {
    emailAddress: "Email",
}

const familyHeaders = {
    id: "ID",
    name: "name",
    adminEmail: "Admin Email",
};

const userHeaders = {
    emailAddress: "Email",
    name: "Name",
    vat: "VAT Number",
    birthDate: "Birth Date",
};

const membersHeaders = {
    email: "E-mail",
    address: "Address",
    birthdate: "Birthdate",
    familyID: "Family",
    name: "Name",
    vat: "VAT",
};

const categoryHeaders = {
    id: "ID",
    name: "Name",
    choose: "Choose parent category"
};

const relationshipsHeaders = {
    personOneID: "First member",
    personTwoID: "Second member",
    designation: "Designation",
};

const infoHeaders = {
    name: "name",
    vatNumber: "VAT Number",
    emailAddress: "Email",
    address: "Address",
    familyID: "Family ID",
    birthDate: "Birth Date",
    emails: "Emails",
};

const accountHeaders = {
    accountId: "Account ID",
    personId: "Person ID",
    balance: "Balance",
    description: "Description",
    accountType: "Account Type",
};


const AppProvider = (props) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
        <Provider value={{
            state,
            familyHeaders,
            userHeaders,
            membersHeaders,
            categoryHeaders,
            relationshipsHeaders,
            infoHeaders,
            accountHeaders,
            emailHeaders,
            dispatch
        }}>
            {props.children}
        </Provider>
    );
};

AppProvider.propTypes = {
    children: PropTypes.node,
};

export default AppProvider;


import {URL_API} from "./URL_API";

//export const URL_API_ROLE = 'http://localhost:8080/project-1.0-SNAPSHOT';
export const URL_API_ROLE = 'http://vs469.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT';
//export const URL_API_ROLE = 'http://localhost:8080/switch_2020_g4_devops-0.0.1-SNAPSHOT';
//export const URL_API_ROLE = 'http://vs448.dei.isep.ipp.pt:8080/switch_2020_g4_devops-0.0.1-SNAPSHOT';
//export const URL_API_ROLE = 'http://vs195.dei.isep.ipp.pt:8080/switch_2020_g4_devops-0.0.1-SNAPSHOT';

export function getUserRoleWeb(success, failure, request) {
    fetch(`${URL_API_ROLE}/userinfo`, request)
        .then(res => {
            if (res.ok) {
                return res.json()
            } else {
                throw new Error("Invalid token")
            }
        })
        .then(res => success(res))
        .catch(err => failure(err.message))
    ;
}

export function getUserFamilyWeb(success, failure, request, id) {
    fetch(`${URL_API}/members2/${id}`, request)
        .then(res => {
            if (res.ok) {
                return res.json()
            } else {
                throw new Error("Invalid user")
            }
        })
        .then(res => success(res))
        .catch(err => failure(err.message))
    ;
}
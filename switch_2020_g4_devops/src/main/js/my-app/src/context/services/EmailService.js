import {URL_API} from "./URL_API";

export function fetchEmailWeb(success, failure, request, id){

    console.log(id);
    fetch(`${URL_API}/members/${id}/emails`,request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err))
    ;
}

export function deleteSecondaryEmail(success, failure, id, request, emailToRemove) {
    console.log("Email Service - delete function");
    console.log("\tID -> " + id);
    console.log("\tEmail to remove -> " + emailToRemove)

    fetch(`${URL_API}/members/${id}/emails/${emailToRemove}`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err));

}

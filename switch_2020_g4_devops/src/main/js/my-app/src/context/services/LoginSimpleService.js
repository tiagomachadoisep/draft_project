//export const URL_API = 'http://vs195.dei.isep.ipp.pt:8080/switch_2020_g4_devops-0.0.1-SNAPSHOT';
//export const URL_API = 'http://localhost:8080/project-1.0-SNAPSHOT';
export const URL_API = 'http://vs469.dei.isep.ipp.pt:8080/project-1.0-SNAPSHOT';

export function loginSimpleWeb(success, failure, request) {
    fetch(`${URL_API}/authenticate`, request)
        .then(res => {
            if (res.ok) {
                return res.json()
            } else {
                throw new Error("Invalid credentials")
            }
        })
        .then(res => success(res))
        .catch(err => failure(err.message))
    ;
}


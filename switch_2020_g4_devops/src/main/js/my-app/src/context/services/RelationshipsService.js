import {URL_API} from "./URL_API";

export const RELATIONSHIPSFAM1 = '/families/2/relations/';

export function fetchFamOneRelationships(success, failure, id,request) {
    /*if(id===undefined || id===0) {
        fetch(`${URL_API}${RELATIONSHIPSFAM1}`,request) //get all relations?
            .then(res => res.json())
            .then(res => success(res))
            .catch(err => {
                console.log("partiu");
                failure(err.message)
            })
        ;
    }*/
    /*else{*/
    fetch(`${URL_API}/families/${id}/relations/`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => {
            console.log("partiu com id" + id);
            failure(err.message)
        })
    ;
    /*}*/
}

export async function addRelationshipOnBackEnd(success, failure, request,id) {

    try {

        const response = await fetch(`${URL_API}/families/${id}/relations/`, request);

        const responseCode = response.status;

        if (response.ok) {
            const responseData = await response.json();
            success(responseData);

        } else if (responseCode >= 400 && responseCode <= 499) {
            const responseData = await response.json(); /* response.text() */
            failure(responseData);

        } else {
            console.log("Service: response is broken.")
            throw new Error('partiu')
        }

    } catch (err) {
        console.log("Service: catch was called")
        failure(err);
    }
}

export async function changeRelationshipOnBackEnd(success, failure, request, id, relationId) {

    try {

        const response = await fetch(`${URL_API}/families/${id}/relations/${relationId}`, request);

        const responseCode = response.status;

        if (response.ok) {
            const responseData = await response.json();
            success(responseData);

        } else if (responseCode >= 400 && responseCode <= 499) {
            const responseData = await response.json(); /* response.text() */
            failure(responseData);

        } else {
            console.log("Service: response is broken.")
            throw new Error('partiu')
        }

    } catch (err) {
        console.log("Service: catch was called")
        failure(err);
    }
}


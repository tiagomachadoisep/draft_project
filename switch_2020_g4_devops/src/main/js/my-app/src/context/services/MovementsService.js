import {URL_API} from "./URL_API";

export function fetchMovementsFromWeb(success, failure, ledgerId, accountId, request){
    fetch(`${URL_API}/ledgers/${ledgerId}/accounts/${accountId}/movements`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err))
    ;
}

export function fetchLedgerIdFromWeb(success, failure, memberId, request) {
    fetch(`${URL_API}/members/${memberId}/ledger`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err))
    ;
}
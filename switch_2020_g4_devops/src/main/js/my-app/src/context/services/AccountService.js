import {URL_API} from "./URL_API";

export function fetchAccountsFromWS(success, failure, id, request,userId,role) {
    console.log("Accounts user id is: "+userId)
    console.log("Accounts role is: "+role)
    if (id === undefined || id === 0) {

        fetch(`${URL_API}/accounts`, request)
            .then(res => res.json())
            .then(res => success(res))
            .catch(err => failure(err));
    } else {
        if(role==="ADMIN" || role==="SYSADMIN") {
            fetch(`${URL_API}/families/${id}/accounts`, request)
                .then(res => res.json())
                .then(res => success(res))
                .catch(err => failure(err));
        }
        else{
            fetch(`${URL_API}/members/${userId}/allaccounts`, request)
                .then(res => res.json())
                .then(res => success(res))
                .catch(err => failure(err));
        }
    }
}

export async function addAccountToWS(success, failure, id,request) {

    console.log("chegou ao account service")
    console.log(id)

    try {
        const response = await fetch(`${URL_API}/members/${id}/accounts/credit`, request);
        const responseCode = response.status;

        if (response.ok) {
            console.log("Account Service: response is ok, making json...")
            const responseData = await response.json();
            success(responseData);

        } else if (responseCode >= 400 && responseCode <= 499) {
            const responseData = await response.json(); /* response.text() */

            console.log("falhou no account service", responseData);
            failure(responseData);

        } else {
            console.log("Account Service: response is broken.")
            throw new Error('partiu')
        }

    } catch (err) {
        console.log("Service: catch was called")
        failure(err);
    }

}





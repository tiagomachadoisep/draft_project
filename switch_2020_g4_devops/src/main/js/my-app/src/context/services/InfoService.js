import {URL_API} from "./URL_API";

export function fetchInfoFromWeb(success, failure,id,request){
    fetch(`${URL_API}/members/${id}`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err))
    ;
}
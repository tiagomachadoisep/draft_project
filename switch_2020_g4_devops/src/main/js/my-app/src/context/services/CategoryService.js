import {URL_API} from "./URL_API";

export const URL_API_FAMILY_1 = 'http://localhost:8080/api/families/1';

export function fetchStandardCategoriesFromSWSAPI(success, failure, request) {
    fetch(`${URL_API}/allcategories`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message))
    ;
}

export function fetchCategoriesFromSWSAPI(success, failure, id, request) {
    if (id === undefined || id === 0) {
        fetch(`${URL_API_FAMILY_1}/categories`, request)
            .then(res => res.json())
            .then(res => {
                success(res)
            })
            .catch(err => {
                failure(err.message)
            })

        ;
    } else {
        fetch(`${URL_API}/families/${id}/categories`, request)
            .then(res => res.json())
            .then(res => {
                success(res)
            })
            .catch(err => {
                failure(err.message)
            })
        ;

    }

}

export function addStandardCategoryToSWSAPI(success, failure, postRequest) {
    fetch(`${URL_API}/categories`, postRequest)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message));
}

export async function addCustomCategoryToSWSAPI(success, failure, id, postRequest) {
    try {

        const response = await fetch(`${URL_API}/families/${id}/categoriesV2`, postRequest);
        console.log(response);
        const responseCode = response.status;
        console.log(responseCode)

        if (response.ok) {
            console.log("RESPONSE OK")
            const responseData = await response.json();
            success(responseData);

        } else if (responseCode >= 400 && responseCode <= 499) {
            console.log("RESPONSE NOT OK")
            const responseData = await response.json();
            failure(responseData);

        } else {
            console.log("Service: response is broken.")
            throw new Error('partiu')
        }

    } catch (err) {
        console.log("Service: catch was called")
        failure(err);
    }

}


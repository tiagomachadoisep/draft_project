import {URL_API} from "./URL_API";

export function fetchMembersFromH2(success, failure, id, request) {
    if (id === undefined || id === 0) {
        //fetch(`${URL_API}/families/2/members`)
        fetch(`${URL_API}/members`, request)
            .then(res => res.json())
            .then(res => success(res))
            .catch(err => failure(err));
    }
    else{
        console.log("getting it.")
        fetch(`${URL_API}/families/${id}/members`, request)
            .then(res => res.json())
            .then(res => success(res))
            .catch(err => failure(err));
    }
}
/*
export function addMemberToH2(success, failure, request) {
    console.log("chegou ao service")
    fetch(`${URL_API}/families/2/members`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message));
}
*/

export async function addMemberToH2(success, failure,familyId, request) {

    try {

        const response = await fetch(`${URL_API}/families/${familyId}/members`, request);

        const responseCode = response.status;

        if (response.ok) {
            console.log("Service: response is ok, making json...")
            const responseData = await response.json();
            success(responseData);

        } else if (responseCode >= 400 && responseCode <= 499) {
            const responseData = await response.json(); /* response.text() */

            console.log("falhou no serviço",responseData);
            failure(responseData);

        } else {
            console.log("Service: response is broken.")
            throw new Error('partiu')
        }

    } catch (err) {
        console.log("Service: catch was called")
        failure(err);
    }

}

/*
export function fetchMembersFromH2(success, failure, id) {
    fetch(`${URL_API}/families/${id}/members`)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message));
}

*/

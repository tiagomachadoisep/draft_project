

export function genericRequestWeb(url,success,failure,request){
    fetch(url,request)
        .then(res =>  res.json())
        .then(res =>success(res))
        .catch(err => failure(err.message))
    ;
}
import {URL_API} from "./URL_API";

export function getFamiliesFromWeb(success, failure,request){
    fetch(`${URL_API}/families`,request)
        .then(res =>  res.json())
        .then(res =>success(res))
        .catch(err => failure(err.message))
    ;
}

export function getFamilyFromWeb(success, failure, id,request){
    fetch(`${URL_API}/families/${id}`,request)
        .then(res => { if(res.ok) {return res.json()}
        else{
            throw new Error("id doesn't exist")
        }})
        .then(res =>success(res))
        .catch(err => failure(err.message))
    ;
}

/*export function addFamilyToWeb(success, failure, request){
    fetch(`${URL_API}/families`,request)
        .then(res => res.json())
        .then(res => { if(res.ok) {return success(res)}
        else{
            return failure(res);
        }
        })
        .catch(err => failure(err.message))
    ;
}*/

export async function addFamilyToWeb(success, failure, request) {

    try {
        const response = await fetch(`${URL_API}/families`, request);
        const responseCode = response.status;

        if (response.ok) {
            const responseData = await response.json();
            success(responseData);
        } else if (responseCode >= 400 && responseCode <= 499) {
            const responseData = await response.json();
            failure(responseData);
        } else {
            throw new Error('partiu')
        }
    } catch (err) {
        failure(err);
    }
}

export function addFamilyToWeb2(success, failure, request, name, adminEmail) {
    fetch(`${URL_API}/families?name=${name}&email=${adminEmail}&vat=0`, request)
        .then(res => res.json())
        .then(res => success(res))
        .catch(err => failure(err.message))
    ;
}
# Class Assignment 5 Report - Part Two -

### CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

#### 1.1

To start this CA5 Part Two, I've created a new Jenkinsfile in ./ca2/PartTwo/Gradle/.

To run a first "test" I copied the Jenkins file used in CA5 part one, and just corrected path's and some echos,
just to try if a simple Jenkins is working well. For that I used a pipeline script directly on Jenkins not from SCM.

I got a minor error related to archiving, because my ./dist folder with the .jar file was on gitignore, so I needed to add this folder to my remote repository otherwise i wouldn't be able to archive it.

For this CA5 PArt Two there were two major different jobs first the javadoc publish on jenkins, and the docker image with Tomcat publish in the docker hub.

For both of this new jobs it was necessary to install new plugins, javadoc and docker plugins.

First i try to publish a HTML related to my javadoc, I starded by add a new task to my build.gradle file, responsible for creating the javadoc and save it in ./build/docs/javadoc/.

With the task on gradle ready we just need to create a new job on Jenkins to publish the HTML and for that I created a new stage where first the command to generateJavadoc was called, this generates the files related to javadoc

![](images/image1.PNG)

then a post if returned success where the published HTML is executed in the respective folder.

After everything working well with the published HTML, I started to build and publish the docker image.

First thing was to create a Dockerfile in the ca2P2 folder, this new Dockerfile will be the responsible to create a new docker image in this case with the tomcat and the war file.

After that I just need to create a new job on Jenkins to build and publish the created docker image in docker hub.

For that I first create a object called app which takes the result of the docker.build command, meaning the docker image.

I used two arguments to make the docker build one with the {username}/{imagename}, another with the -f option to indicate the path for the Dockerfile I want to be used.

After that we need to login with my docker hub account, and I needed to add a new set of credentials to my Jenkins account this one for the docker hub.

To create new credentials, I go Dashboard -> Manage credentials -> Jenkins (stores scoped) -> Global credentials -> Add Credentials, fill the form with the username and password for my docker hub account.

![](images/image2.PNG)

With the login resolved, it was only left to push the image to the hub, for that I used the command app.push, with one argument '%BUILD_NUMBER' with this command (for Windows) the push is tagged with the build number.

After all changes have been done I tried to build my Jenkinsfile from SCM, and everything work smoothly.

![](images/image3.PNG)

and the test report (I've done only a really simple test just to check if it was everything ok with the Jenkins job related)

![](images/image4.PNG)

--------------------------------------------------------------------------------


## 2. Analysis of an Alternative

**Jenkins vs Buddy (Theoretical)**

![](images/image01.PNG)

![](images/image02.PNG)

**Jenkins vs Buddy (Personal Experience)**

For me Jenkins was the most fun technology I've learned throughout all the class assignments, but Buddy was a wonderful surprise, it's very intuitive, I've made all the implementation without the need of install anything it works perfectly in the web application.
Has it can be synchronized with both bitbucket and docker, it made both of these jobs/actions really easy to implement.
Another positive aspect of buddy is the UI which is much appealing than Jenkins.

As a downsizes to buddy is that is paid, is free with limited condition. Also, Jenkins has a gigantic community compared to buddy, which makes really easy to solve problems.
At last it felt to me that the way that Jenkins works with plugins, makes it much more customizable, although buddy it's easier to work, it's less expandable.

As conclusion I liked both Jenkins and Buddy, and I prefer to use Buddy but even for a small project like ours it feels a bit limeted compared with Jenkins.

--------------------------------------------------------------------------------


## 3. Implementation of the Alternative

Has an alternative to Jenkins to complete the CA5P2 I choose buddy.

With buddy, I'll use the web application, to register I synchronized with my bitbucket account, this process makes all my repositories available, and I choose to create a new repository [new repository](https://bitbucket.org/SwitchLR/ca5p2buddy/src/master/) equal to the ca2P2, but only to use with Buddy.

So after this minor setup I started to replicate the jobs I've already executed with Jenkins:

#### Checkout

Since buddy is already synchronized with my bitbucket account, I just needed to choose the correct repository, I didn't a specific task for this job. I just start my Buddy project in the ca5P2Buddy repository that I just created.


#### Assemble

Buddy works with actions instead of jobs so assemble was the first job I've created. Is incredibly easy to create actions with buddy, so:

First we choose 'Add the first action'

![](images/image5.PNG)

Then Gradle

![](images/image6.PNG)

Now we have bash window and can write the 'gradle assemble' command, but first we need to change the gradle version, selecting [change] to the gradle version in our project (6.8.3).

![](images/image7.PNG)

![](images/image8.PNG)


#### Tests

First we need another gradle action with the command 'gradle test', then since Buddy didn't has a specific plugin/action to make/publish test reports we need to go to the file system and check if it's ok.

![](images/image9.PNG)

#### Javadoc

We have a problem just like we the point before, theres no specific plugin/action to publish HTML so what we can do is go to file system and check if Javadoc was created.

![](images/image10.PNG)

#### Archiving

Once again unlike Jenkins, with buddy we don't have a specific plugin to deal when we want to archiveArtifacts in this case the war file so, once again we need to check in the file sytem.

![](images/image11.PNG)

#### Docker

This was one of the things that amazed me the most using buddy, it was incredibly easy to build and push an image to docker hub.

We need two actions one to build docker image, and another to push the created image to dockerhub.

For the first, since the Dockerfile is the directory root, we  literally just select the 'Build Docker Image' action and save. And it's done.

![](images/imageX1.PNG)

For the second part, we just choose the 'Push Docker image' action, then we fill the form.

Docker registry: DockerHub, because is the one where I have an account.

DockerHub account: We just need to synchronize with dockerHub account, using our username and password.

Repository: is the same I used for the Jenkins implementation

Tags: is the one we choose.

![](images/imageX2.PNG)

After all that we can run the pipeline I checke if it's everything ok

![](images/image12.PNG)

And if the image was efectivelly pushed to dockerHub

![](images/image13.PNG)

To finish I pushed the changes to my repository and tag the last commit.
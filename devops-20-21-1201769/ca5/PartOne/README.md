# Class Assignment 5 Report - Part One -

### CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

#### 1.1 Setup

I started by install Jenkins, I got minor problems, first because I have a Windows Home edition I needed to enable
secPol.msc, and then I was able to resolve the problem with invalid service logon credentials described in the Jenkins tutorial.

While in the Jenkins installation it was also needed to add a password to my machine.

Besides the explained before I followed the [official tutorial](https://www.jenkins.io/doc/book/installing/windows/) for Jenkins installation in windows.


My first step was create a Jenkinsfile in the CA2/PartOne/gradle_basic_demo folder and add new credentials to my jenkins account.

After the "setup" was complete, I focus my attention to my Jenkinsfile.

#### 1.2 First stage (Checkout)

In the first stage I use as git credentialsID the one that I have created in the previous step, and add the URL to my remote repository

![](images/jenkins1.PNG)


#### 1.3 Second Stage and Third Stage (Assemble + Test)

Since my jenkinsfile is not in the root project and using windows I need to use the command

	bat 'call ca2/PartOne/gradle_basic_demo/gradlew.bat -p ca2/PartOne/gradle_basic_demo assemble'

the call command it's needed to make the call to the folder were gradlew.bat is present the -p flag is to pass the folder were the gradle will run.

In this point it was where I got some problems:

- in the steps I used the command *sh* which does not work in windows

- problems related to the path, since at first that the steps command worked conditionally, 
  so I was trying to do something that do not work like:

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd ca2/PartOne/gradle_basic_demo/'    
                bat 'gradlew.bat assemble'
            }
        }


![](images/jenkins2.PNG)

#### 1.4 Fourth Stage (Archiving)

In this stage I just needed to add the build/distributions path to the archiveArtifacts feature.

![](images/jenkins3.PNG)

#### 1.5 Pipeline Creation

In jenkins dashboard, I've created a new item with name ca5_PartOne + Pipeline, these are the changes I've done:

- *Pipeline Script from SCM* since I want to get my Jenkinsfile from my remote repository.

- GIT, because it's the SCM

- Repository URL: https://bitbucket.org/SwitchLR/devops-20-21-1201769/

- Credentials: SwitchLR/****** (SwitchLR Bitbucket)

- Script Path: ca2/PartOne/gradle_basic_demo/Jenkinsfile since as said before my Jenkinsfile is not in the repository root

![](images/jenkins4.PNG)

#### 1.6 Build

Finally, I tried to build and everything went perfectly

![](images/jenkins5.PNG) 



--------------------------------------------------------------------------------


## 2. Analysis of an Alternative




--------------------------------------------------------------------------------


## 3. Implementation of the Alternative


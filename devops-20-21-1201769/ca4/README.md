# Class Assignment 4 Report

### Containers and Docker

## 1. Analysis, Design and Implementation

#### 1.1 and 1.2 Use docker-compose to produce 2 services/containers

To start this implementation, first I downloaded the code in this [repository](https://bitbucket.org/atb/docker-compose-spring-tut-demo/) suggested by our professor.

Both files, docker-compose.yml and the Dockerfiler for db were ok, I didn't need to change anything.

The web Dockerfile, need changes so it work with my own [repository](https://SwitchLR@bitbucket.org/SwitchLR/tut-basic-gradle.git)

Changes needed:

RUN git clone need to point to my repo:

	RUN git clone https://SwitchLR@bitbucket.org/SwitchLR/tut-basic-gradle.git

The name of my .war file is different in my project:

	RUN cp build/libs/devOpsCA2-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

After that I tried to

	docker-compose build

And got the next error 127:

![](images/docker1.PNG)

to correct this error I changed the name of my workdir after git clone command in Dockerfile(web), to match the Path created after git clone.

	WORKDIR /tmp/build/tut-basic-gradle

After this change i tried again to build and got a new error 126:

![](images/docker2.PNG)

so now I have a permission problem and to resolve I simply add the next command in my Dockerfile before the ./gradlew clean build

	RUN chmod u+x gradlew

Now afer try again the build command, it was successful

![](images/docker3.PNG)

Now lets check if docker-compose up command works and run this composition

![](images/springRun.PNG)

Everything was, lets just verify if the url's are also working

First the DB

	http://localhost:8080/devOpsCA2-0.0.1-SNAPSHOT/h2-console

![](images/docker4.PNG)

Now the Web

	http://localhost:8080/devOpsCA2-0.0.1-SNAPSHOT/

![](images/docker5.PNG)

With the command

	docker images

we can list our local docker images and check if both db and web images were created

![](images/docker6.PNG)

#### 1.3 Publish the images (db and web) to Docker Hub

For this part of the assignment I've created an account and a repository in docker.com

![](images/docker7.PNG)

//////////////////////////////////////////

to login using the terminal

	docker login -u switchlr

To tag both images

	docker tag ca4_db:db_v1 switchlr/devops:dbpush	

	docker tag ca4_web:web_v1 switchlr/devops:webpush

Now to push both image to docker hub

	docker push switchlr/devops:dbpush

	docker push switchlr/devops:webpush

Now lets see if my repository in docker hub has both images:

![](images/docker8.PNG)

Thankfully everything went as expected.

Now in our terminal again I went to check docker images:

![](images/docker9.PNG)

Now as we can see in the image above we have both images repeated, so lets delete the ones we don't need.

	docker rmi ca4_web:web_v1

	docker rmi ca4_db:db_v1

![](images/docker10.PNG)

Now we only have the ones we send to repository on docker hub.

#### 1.4 Use a volume with the db container to get a copy of the database file

(
At this point some things weren't working properly so reset my docker using the command

	docker system prune -a

so some names could be different from the ones I used until this point
)

First we can check which containers are ready to run

	docker ps -a

as expected we have two containers one for db and another for web

![](images/docker11.PNG)

now let's start the db container with

	docker start ca4_db_1

and let's go inside the container

	docker exec -it 8385086ada1b bash

we can in the next image the container running, and a shell started inside the container

![](images/docker12.PNG)

Now to make a copy of the database file in our host we use the command

	cp ./jpadb.mv.db /usr/src/data

we can see that jpadb.mv.db file is now in the \data\ folder as we want to.

![](images/docker13.PNG)


--------------------------------------------------------------------------------


## 2. Analysis of an Alternative

**Docker vs Kubernetes (Theorethical)**

![](imagesKubernetes/dockerVSkube.PNG)



**Docker vs Kubernetes (Personal Experience)**

As in the previows alternatives for each class assignment, the kubernetes implementation was way more difiicult than docker. The main problems I got were related with minikube start, and until I found find Kompose I was not able to correctly setup .

In this CA we use kubernetes essencially as an alternative to Docker while Kubernetes is more a complement used to scale multiple containers deployed across multiple sources, for better control and implementation of contentorized applications.

Since our application is quite small and simple, we can't see the true potencial of kubernetes. In fact Docker Swarm is the real alternative to Kubernetes.

In our case, we use kubernetes to deploy and build from the images on our docker hub, but in my reaserch I found that usually we use docker to build and deploy and kubernetes to run, monitor and scale.

![](imagesKubernetes/kube11.PNG)



## 3. Implementation of the Alternative


I started by installing kubernetes and minikube

After the installation I made the deployment of both db and web from dockerHub images used in Docker implementation


	kubectl create deployment k8sdb --image=switchlr/devops:dbpush

	kubectl create deployment k8web --image=switchlr/devops:webpush

To check if everything went well in the deployment

	kubectl get deployment

To check if both pods are running

	kubectl get pods

![](imagesKubernetes/kube5.PNG)

Know to convert from a docker-compose.yaml to kubernetes objects I used kompose

After install kompose in the folder where I have my docker-compose.yaml I run the command 

	kompose convert

this commands generates several files known by kubernetes

![](imagesKubernetes/kube6.PNG)

And some changes were needed in these files:

1. In db-deployment.yaml and web-deployment.yaml I needed to add the images from my dockerhub

	 spec:
      containers:
      - image: "switchlr/devops:dbpush"

2. In the db/service.yaml and web/service. yaml I needed

	spec:
	  type: NodePort
	
3. In the default network policy

	apiVersion: networking.k8s.io/v1

at this point I started minikube with 

	minikube start

with this command the vm created is based in docker, not hyperv or virtualbox.

I tried with virtualbox and got conflict problems related with hyperv.

Also tried with hyperv, and got memory problems, i could use 2/3/4/5 Gb RAM memory, and always got the same problem, with the "minikube start", i got no errors.

After this changes with need to apply them with the command 

	kubectl apply -f .

Now we can check each pod IP

	kubectl get pod -o wide

![](imagesKubernetes/kube7.PNG)

With the db pod IP *172.17.0.3* we go to application.properties file and change the 

So let's enter the web pod

	kubectl exec -ti web-54b7d88fd8-rz5bk -- bash

(install nano)

and make the changes

![](imagesKubernetes/kube8.PNG)

Now has I've done in the docker implementation I need to

	./gradlew clean build

and

	cp build/libs/devOpsCA2-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/


After exiting the pod we can try to access db for that first we need to start the service

	minikube service db

![](imagesKubernetes/kube9.PNG)

And automatically open a browser window with the h2 console login, in the login we need to change the URL to use the correct IP address

![](imagesKubernetes/kube4.PNG)

and then we can finally access h2 database

![](imagesKubernetes/kube3.PNG)

Now let's run the web and for that

	minikube service web

![](imagesKubernetes/kube10.PNG)

And everything went well as we can see the expected table in the browser

![](imagesKubernetes/kube1.PNG)

At last, I've done the usual procedure of commit to remote repository(with tag) and 
close the issue related to the alternative implementation 





package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void validateFirstNameTestSuccess() {
        Employee employee = new Employee("Luis", "Rodrigues", "IT",
                "chief", "luis@isep.ipp.pt");


        assertNotNull(employee);
    }


    @Test
    void validateStringTestNull() {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Luis", "Rodrigues", "IT",
                    "chief", "");
        });
    }


}
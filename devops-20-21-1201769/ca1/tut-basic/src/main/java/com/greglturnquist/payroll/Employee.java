/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String email;

    private Employee() {
    }

    public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
        if (validateFirstName(firstName)){
        this.firstName = firstName;
        }
        if (validateLastName(lastName)){
        this.lastName = lastName;
        }
        if(validateDescription(description)) {
            this.description = description;
        }
        if (validateJobTitle(jobTitle)) {
            this.jobTitle = jobTitle;
        }
        if (validateEmailFormat(email) ) {
            this.email = email;
        }else throw new IllegalArgumentException("Invalid Data");
    }


    public boolean validateFirstName(String firstName){
        if (firstName.isEmpty() || firstName.equals(null))
            throw new IllegalArgumentException("Data can't be null or empty");;
        return true;
    }


    public boolean validateLastName(String lastName){
        if (lastName.isEmpty() || lastName.equals(null))
            throw new IllegalArgumentException("Data can't be null or empty");;
       return true;
    }


    public boolean validateDescription(String description){
        if (description.isEmpty() || description.equals(null))
            throw new IllegalArgumentException("Data can't be null or empty");;
        return true;
    }


    public boolean validateJobTitle(String jobTitle){
        if (jobTitle.isEmpty() || jobTitle.equals(null))
            throw new IllegalArgumentException("Data can't be null or empty");;
        return true;
    }


    public boolean validateEmail(String email){
        if (email.isEmpty() || email.equals(null))
            throw new IllegalArgumentException("Data can't be null or empty");;
        return true;
    }

    public boolean validateEmailFormat(String email){
        // Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobTitle, employee.jobTitle) &&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
// end::code[]

package com.greglturnquist.payroll.test;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void validateEmployeeTestSuccess() {
        Employee employee = new Employee("Luis", "Rodrigues", "IT",
                "chief", "luis@isep.ipp.pt");


        assertNotNull(employee);
    }



    @Test
    void validateFirstNameTestSuccess() {
        Employee employee = new Employee("Luis", "Rodrigues", "IT",
                "chief", "luis@isep.ipp.pt");


        assertNotNull(employee);
    }

    ////////////////////////////////////////////////////////////////


    @Test
    void validateFirstNameTestNull() {

        assertThrows(NullPointerException.class, () -> {
            Employee employee = new Employee(null, "Rodrigues", "IT",
                    "chief", "luis.isep.ipp.pt");
        });
    }

    @Test
    void validateFirstNameTestEmpty() {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Rodrigues", "IT",
                    "chief", "luis.isep.ipp.pt");
        });
    }

    /////////////////////////////////////////////



    @Test
    void validateLastNameTestNull() {

        assertThrows(NullPointerException.class, () -> {
            Employee employee = new Employee("luis", null, "IT",
                    "chief", "luis.isep.ipp.pt");
        });
    }

    @Test
    void validateLastNameTestEmpty() {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("luis", "" , "IT",
                    "chief", "luis.isep.ipp.pt");
        });
    }

    ////////////////////////////////////////////////////////


    @Test
    void validateDescriptionTestNull() {

        assertThrows(NullPointerException.class, () -> {
            Employee employee = new Employee(null, "Rodrigues", null,
                    "chief", "luis.isep.ipp.pt");
        });
    }

    @Test
    void validateDescriptionTestEmpty() {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Rodrigues", "",
                    "chief", "luis.isep.ipp.pt");
        });
    }

    /////////////////////////////////////////////////////////////

    @Test
    void validateJobTitleTestNull() {

        assertThrows(NullPointerException.class, () -> {
            Employee employee = new Employee(null, "Rodrigues", "IT",
                    null, "luis.isep.ipp.pt");
        });
    }

    @Test
    void validateJobTitleTestEmpty() {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Rodrigues", "IT",
                    "", "luis.isep.ipp.pt");
        });
    }

    ///////////////////////////////////////////////////////////////

    @Test
    void validateEmailTestNull() {

        assertThrows(NullPointerException.class, () -> {
            Employee employee = new Employee(null, "Rodrigues", "IT",
                    "chief", null);
        });
    }

    @Test
    void validateEmailTestEmpty() {

        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Rodrigues", "IT",
                    "chief", "");
        });
    }

    @Test
    @DisplayName("Try to input an email without @")
    void validateEmailFormatTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Rodrigues", "IT",
                    "chief", "luis.isep");
        });

    }
}
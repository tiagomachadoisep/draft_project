# Class Assignment 1 Report

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

## 1. Analysis, Design and Implementation

### 1.1 First Class 11/03/2021

I started by creating a copy of the professor example repository in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

for duplicate the example repository I used the next command in gitBash

**git clone --bare https://bitbucket.org/atb/devops-21-rep-template.git**

the push it into my new repository

**git push -- mirror https://SwitchLR@bitbucket.org/SwitchLR/devops-20-21-1201769.git**

With my new repository ready I add the job title to the Employee as requested in the last page
of the first class presentation. Try it in the browser and everything went perfectly.

### 1.2 Second Class 18/03/2021

The five Requirements for CA1 were presented:

### 1.2.1

To see the current status I used in gitBash:

**git status**

(shows the state of your working directory and helps you see all the files which are untracked by Git, staged or unstaged)

There were some files that were not in staging area, to add every file to staging area I used

**git add .**

then commit with

**git commit -m "first CA1 commit"**

push to the remote repository

**git push -u origin master**

(the -u makes push unambiguous and that way git pull can be done with no more arguments
because knows that for origin email-field)

to see if everything was ok

**git status**


### 1.2.2

to see the name of each commit

**git log**

(git log command is really useful not only show the recent commits but also each commit ID, date, time, author, and some additional details)

to tag the initial version

**git tag -a v1.2.0 -m "initial version" 6d310244b5730932d584e8fa2b3affc48cae068f**

to push all tags to the remote repository:

**git push --tags**

### 1.3.1

to create a branch called email-field and checkout to new branch

**git checkout -b email-field**

(-b stands for creating the branch and automatically checking out to the new branch, it's equivalent to 

git branch email-field

git checkout email-field)


### 1.3.2

after adding support for email in the project

(in email-field branch)

to add all files to staging area

**git add .**

to commit

**git commit -m "add email support"**

to push into remote repository

**git push origin email-field**

### 1.3.3

after adding unit test for testing the creation of Employees
and the validation of their attributes.

**git add .**

**git commit -m "add unit test for Employee and their attributes"**

**git push -u origin email-field**

(the -u makes push unambiguous and that way git pull can be done with no more arguments
because knows that for origin email-field)

now I decided to update my README.md file

so after the update in IntelliJ, I went back to gitBash and use

**git add README.md**

**git commit -m "update README.md"**

**git push**

### 1.3.4

To run the project I used, in the project folder the command

**./mvnw spring-boot:run**

**Backend Debug**

In intelliJ I've put a break point in the Employee constructor, to see if everything is ok:

![](tut-basic/imagesProject/imagem1.PNG)

To debug, right click on ReactAndSpringDataRestApplication, and select Debug

![](tut-basic/imagesProject/imagem2.png)

then we try to instantiate two Employees from the database to see if everything is ok

![](tut-basic/imagesProject/imagem3.PNG)

as we can see next everything went well for both employees

![](tut-basic/imagesProject/imagem4r.PNG)

![](tut-basic/imagesProject/imagem5.PNG)

![](tut-basic/imagesProject/imagem6.PNG)

![](tut-basic/imagesProject/imagem7.PNG)


Now I tried to save an Employee with an empty first name:

![](tut-basic/imagesProject/imagem8.PNG)

After trying to run the application we get an error, "Data can't be null or empty":

![](tut-basic/imagesProject/imagem9.PNG)

**FrontEndDebug**

On Google Chrome developer tools we can also use the debug tool

![](tut-basic/imagesProject/imagem10.PNG)

After some changes to tests, and to the README.md file I made another commit

**git add . **

**git commit -m "README.md update and tests update"

**git push origin email-field

### 1.3.5

Now it's time to merge the email-field branch into master

first I need to leave email-field branch to master, for that I used the command

**git checkout master**

now on the master

**git merge email-field**

**git push**

to tag this commit

**git tag -a v1.3.0 -m "v1.3.0" 6060f76a9fd96c3664f7fd832e085a097a5f6bd4**

to push all tags to the remote repository

git push --tags

### 1.4

For this part of the CA1 I created a issue in bitbucket

I used this as a test, so I created a simple issue for the fourth part of the assignment
Issue #1 called fix invalid email

### 1.4.1

create a new branch called fix-invalid-email

**git checkout -b fix-invalid-email**

after adding the method validateEmailFormat(String email) another commit

**git add .**

**git commit -m "add validation to email format"**

**git push origin fix-invalid-email**

### 1.4.2

**Backend Debug**

With the new method validateEmailFormat, if every data is inputed as expected everything went great

![](tut-basic/imagesProject/imagem11.PNG)

but if we input an invalid email (without '@') we can't execute our app

![](tut-basic/imagesProject/imagem12.PNG)



### 1.4.3

to check all branches

git branch -v

now we want to merge fix-invalid-email branch into master and for that we use

**git checkout master**

**git merge fix-invalid-email**

**git push**

to add a tag to this version

**git log**

**git tag -a v1.3.1 -m "v1.3.1" cdba868ba21bdb2c232b25a9732450a20f5a9126**

**git push --tags**

### 1.5

update README.md

**git add .**

**git commit -m "update README.md resolve issue #1"**



************************************************************************************************************
## 2. Analysis of an Alternative

As an alternative to git, after some research I choose to analyse Mercurial.

Mercurial is a free distributed source control management tool,known for its efficiency in handling projects of all sizes.

Both git and mercurial are DVCS(Distributed Version Control Systems) and very similar to each other.

In a DVCS system the user, in addition to the central repository, has one copy of the repository locally.

![](tut-basic/imagesProject/CVCS-vs-DVCS.png)

**GIT vs Mercurial (theoretical)**

![](tut-basic/imagesProject/gitVSmercurial.png)

**GIT vs Mercurial (personal experience)**

After implementing both git and mercurial to the same assignments, I feel that they are very similar.

The process and the logic behind it is almost the same and there were only two points that make this experience different from the one where I use GIT:

1 - **TortoiseHG** - as expected using a GUI as made the process a bit more pleasant and clear.

2 - **Support in Google** - because GIT has something like 80% market share, and Mercurial around 2%, it's way more easy to solve
any problem using a simple google search in GIT. Also, it seems that after 2016/2017 almost no one cares about mercurial, a lot of tutorials where made about 10 years ago.




************************************************************************************************************
## 3. Implementation of the Alternative

In my research I got the feeling that git and mercurial if used in the terminal are very similar,
having a handful of commands almost equal to each other.

Because of that for this part of the CA1 I choose to make the implementation using a GUI for mercurial,
in this case TortoiseHG.


**Initialize**

First I create a new folder, and copy inside the project from [here](https://github.com/spring-guides/tut-react-and-spring-data-rest)


Right click on the new folder -> TortoiseHG -> crete new repository here

**Commit and Branching**


In intelliJ I add the jobTitle and email field.

In the next image we can see how to make a commit and how to create a new branch

![](tut-basic/imagesProject/hg1.png)

In red, we can see how to do a commit:

1 - the files that have been modified

2 - the commit message

3 - button to commit

In green, we can see how to do a commit and create a new branch

1 - button to change the branch

2 - message box to create new branch


**TAG**

To tag a commit we just need to right click on the commit we want to tag, select the tag, and choose the name.

**Merge Branches**

first we need to go back to default branch

![](tut-basic/imagesProject/hg2.PNG)

then right click on the branch we want to merge with default and choose "Merge with Local"

![](tut-basic/imagesProject/hg3.PNG)

For the last part of the assignment I created a new branch called fix-invalid-email
made the changes in intelliJ as explained before merge the new branch with the default one, and tag this new version

![](tut-basic/imagesProject/hg4.PNG)

Finally, I created a remote repository on [perforce](https://www.perforce.com/hth/mercurial-hosting) 

![](tut-basic/imagesProject/hg5.PNG)

and then made a push:

![](tut-basic/imagesProject/hg6.PNG)

1 - url for the repository

2 - Paths in repository settings

3 - button to push


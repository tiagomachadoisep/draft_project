# Class Assignment 2 Report Part One

### Build Tools with Gradle

## 1. Analysis, Design and Implementation

#### 1.1 assignment preparation


I started by creating as request a new folder for the Part One of CA2.

Then download the content of the repository [here](https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/)
to the new folder

After this I did my first commit to the remote

#### 1.2 experiment with the application

First I installed gradle, following the instructions from [gradle.org](https://gradle.org/install/)

It seemed to me that everything was ok after instalation, but still I could not get gradle to build. I needed to edit system environment variables to make everything work somethly.

After that rectification I was able to fully experiment the application, and everything worked as expected.

#### 1.3 Add a new task to execute the server.

To add a new task in case the runServer task, I went to build.gradle file on my IDE and added the next code

    task runServer(type:JavaExec, dependsOn: classes){

    group = "DevOps"

    description = "Launches a server"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
    }

then to see if everything was ok, on terminal I use the command

    ./gradlew runServer

and as we can see in the next image everything was ok.

![](imagesCA2P1/gradle1.PNG)

A new commit was made to close this part of the assignment.

#### 1.4 Add a unit test

First I created the folder to match the path indicated for the assignment.

Then I use the next code to create a test

    package basic_demo;
    import org.junit.jupiter.api.Test;
    import static org.testng.AssertJUnit.assertNotNull;

    public class AppTest {
    @Test
    public void testAppHasAGreeting() {
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
    }

Now I checked if everything was ok:

![](imagesCA2P1/gradle2.PNG)
![](imagesCA2P1/gradle3.PNG)

I finish this task with a new commit and resolving the issue #5

#### 1.5 Add a new task of type Copy

I created a new task in gradle.build

    task backup(type:Copy){
    from 'src'
    into 'backup'
    }

In terminal run

    ./gradle backup

![](imagesCA2P1/gradle4.PNG)

After that I checked if the backup was really made, everything was correct

![](imagesCA2P1/gradle5.PNG)

Finally, commit the changes and resolve the issue #6

#### 1.6 Add a new task of type Zip

I created a new task in gradle.build

    task zipBackup(type: Zip) {
    archiveFileName = "srcZipBackup.zip"
    destinationDirectory = file("backup/zips")
    from "src"
    }

In terminal run

    ./gradle zipBackup

![](imagesCA2P1/gradle6.PNG)

Like in last topic I checked if everything was correct

![](imagesCA2P1/gradle7.PNG)

and now I commit the changes made and resolve issue #7




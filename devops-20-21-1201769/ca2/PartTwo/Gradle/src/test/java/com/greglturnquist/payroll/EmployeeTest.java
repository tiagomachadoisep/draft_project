package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getFirstName() {

        Employee employee = new Employee("luis","rodrigues","aluno");

        assertEquals(employee.getFirstName(),"luis");
    }
}
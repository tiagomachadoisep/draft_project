# Class Assignment 3 Report Part Two

### Build Tools with Gradle

## 1. Analysis, Design and Implementation

#### 1.1 Create a VM as described in the lecture

I choose to create a [new repository](https://SwitchLR@bitbucket.org/SwitchLR/tut-basic-gradle.git) to implement Vagrant/VirtualBox for CA3 PartTwo, this way I don't need to change any of my previous work.

In the Vagrantfile, I just neede to change the git clone command to

	git clone https://SwitchLR@bitbucket.org/SwitchLR/tut-basic-gradle.git

and the name of the .war file, because in gradle build the .jar file created is called devOpsCA2-0.0.1-SNAPSHOT.jar

	devOpsCA2-0.0.1-SNAPSHOT.war


After I change the tut-basic-gradle project as instructed by the next repository

https://bitbucket.org/atb/tut-basic-gradle/commits/

Now to create and configure the guest machines I used

	vagrant up

(I add to gitignore the .vagrant folder)

To check if both machines where running

	vagrant status

![](images/vagrantStatusOk.PNG)

Now lets check if everything is working in each machine

Starting with db

	vagrant ssh db

we can use

	ps -xa

to see which processes are running

![](images/dbPS.PNG)

and we can see that h2 is running as expected

Now let's check with the url's if everything is working

	http://localhost:8080/devOpsCA2-0.0.1-SNAPSHOT/h2-console

	http://192.168.33.10:8080/devOpsCA2-0.0.1-SNAPSHOT/h2-console/

And we can check that everything went as expected

![](images/h2ConsoleOk.PNG) h2ConsoleOk

Now let's see how it's going with the web machine

	vagrant ssh web
	ps -xa

![](images/webPS.PNG) webPS

we can see tomcat8 running

Now, as I did for the db VM, let's check the url's

http://localhost:8080/devOpsCA2-0.0.1-SNAPSHOT/

http://192.168.33.10:8080/devOpsCA2-0.0.1-SNAPSHOT/

![](images/frontendOK.PNG) frontendOK

And, thankfully everything is working.

The last step for this part of the assignment is to commit all the changes.

## 2. Analysis of an Alternative

I choose to try implement Hyper-V, because it's free, has good reviews, and is a native tool for windows.

**Virtual Box vs Hyper-V (Theorethical)**

#### 2.1 Hypervisor types

There are two types of hypervisors:

Type 1 (hyper-V) - the virtual machine runs directly on a computer's hardware

Type 2 (VirtualBox) - is an application that runs on the operating system(OS) and is already installed on a host.

![](images/hypervisorsTypes.PNG) hypervisorsTypes

#### 2.2 Platforms

VirtualBox is much more flexible since supports different OS as guest, like MacOS, Windows, Linux,Solaris etc.
On the other hand Hyper-V can host VMs with windows, Linus and FreeBSD. 

#### 2.3 Networking

When working with Vagrant this does not know how to configure networks for Hyper-V, configurations in 
Vagrantfile are ignored. It as to be configured after *vagrant up*


**Virtual Box vs Hyper-V (Personal Experience)**

As my personal experience, I feel like that for this CA3 the implementation of an alternative was easier 
than in the previous CAs.
Very little changes were needed on the Vagrantfile. At first, I tried several boxes that where not compatible, after
choosing the right one everything went smoothly, and for the specific assign I did not feel much difference using
Hyper-v instead of VirtualBox.

## 3. Implementation of the Alternative

In my case has I have the Home version of Windows 10, I needed to run the next script to download Hyper-v

 	pushd "%~dp0"
 	dir /b %SystemRoot%\servicing\Packages\*Hyper-V*.mum >hv.txt
 	for /f %%i in ('findstr /i . hv.txt 2^>nul') do dism /online /norestart /add-package:"%SystemRoot%\servicing\Packages\%%i"add-package:"%SystemRoot%\servicing\Packages\%%i"
 	del hv.txt
 	Dism /online /enable-feature /featurename:Microsoft-Hyper-V -All /LimitAccess /ALL
 	pause

After run this script as a .bat file I was able to activate Hyper-v in my machine

To implement hyper-v I used the Vagrantfile that was already working with virtual box, with some few changes:

- config.vm.box = "generic/ubuntu1604"
- web.vm.provider "hyperv"

disclaimer: I tried different boxes, like hashicorp/precise64, centos/7, etc, but until I found generic/ubuntu1604, I was having a lot of trouble. That's because it's necessary a 16.04 version of ubuntu.

After this changes on Vagrantfile, on Windows command prompt (as administrator) I tried to initialize both VM

	vagrant up --provider=hyperv

after this both machines were running as expected

![](images/hypervBothVMRunning.PNG) 

At this point none of the URL's were working, so I needed to enter the web VM

	vagrant ssh web

and now on the application.properties i need to change the IP address to the one of the db VM

	spring.datasource.url=jdbc:h2:tcp://172.22.65.221:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

![](images/dbVMIP.PNG) 

Now using de web VM IP address we can check if everything is working

![](images/hyperVfrontendOK.PNG) 












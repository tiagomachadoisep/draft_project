# Class Assignment 3 Report Part One

### Build Tools with Gradle

## 1. Analysis, Design and Implementation

#### 1.1 Create a VM as described in the lecture

To create my own VM as follow all the lecture steps. 
Thing is when I tried to use ssh to connect to the VM, I get the error 

"ssh: connect to host 192.168.56.5 port 22: Connection timeout"

So I try to ping 192.168.56.5, and I got 100% loss.

So after some research, I found out that if my host was 192.168.1.76, my VM ip needed to be something like 192.168.1.X,
so I change my guest IP address to 192.168.1.5.

Another problem that I got was that i was not able to install any of the suggested applications, I needed to go in VM VirtualBox and change my Network Adapter 1 from NAT to Bridged Adapter.

![](images/bridgeAdapter.PNG) 

After this changes I was able, in the host terminal, to use ssh to connect to the VM using:

    ssh luis@192.168.1.5

![](images/connectionSSH.PNG) 

#### 1.2 clone your individual repository inside the VM

to clone the application I just needed to use the command

git clone https://SwitchLR@bitbucket.org/SwitchLR/devops-20-21-1201769.git

![](images/cloneRepo.PNG) 
#### 1.3 build and execute the spring boot tutorial basic project and the gradle_basic_demo project

Now it's time to clone the application Spring Tutorial, and build the aplication with the command

    ./mvnw spring-boot:run

![](images/VM_springBootRun.PNG) 

In the host computer with the url http://192.168.1.5:8080/ I was able to see that everything was going smoothly

![](images/localHostOK.PNG) 

With everything working as expected, now it's time to install maven 

    sudo apt install maven

and gradle

    sudo apt install gradle

Now it's time to check if everything is ok and working when running project from previous assignments:

For CA1 

    running ./mvnw spring-boot:run 

works perfectly:

![](images/ca1running.PNG) 

Now for the CA2

    ./gradlew build

this time i got permission issues:

![](images/gradlePermissionDenied.PNG) 

so with

    chmod u+x gradlew

I add the permission for the user to execute the gradle command build

    ./gradlew build

![](images/gradleOkBuild.PNG)

For the PartOne of CA2, in my VM, I was able 

    ./gradlew runServer

![](images/CA1P1runServer.PNG)  

In another terminal on my VM I tried

    ./gradlew runClient

it didn't work

![](images/VMrunClientFail.PNG)  

So I tried the same thing in windows command prompt using ssh and get the exact same error.

That's because I'm trying to run code that is dependent on display, in this case the windows from the chat room server.

To solve this problem I've created a new task in gradle:

    task runClientVM(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on 192.168.1.5 "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.1.5', '59001'
    }

This way we launch a chat client from a server on 192.168.1.5 my VM.

On Windows command prompt I use 

    gradlew.bat runClientVM

And, as expected I got a new chat window

![](images/chatWindow.PNG)  chatWindow


For the CA2 Part two

First I try to build it and was ok

![](images/CA2P2OkGradleBuild.PNG)  

Has we can see after 

    ./gradlew build

the folder built has two files, in case, bundle.js and bundle.js.map

![](images/fullBuilt.PNG)  fullBuilt

if we try 

    ./gradlew clean

we can see in the next image that as expected the built folder is now empty:

![](images/emptyBuilt.PNG)  emptyBuilt

























